#--------------------------------------------------------------------
# Version 20230309-110107 checked into repository
# (c) CopyRight 2023 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#--------------------------------------------------------------------
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/include/loadcheck.py
# Python module to throttle a script by checking CPU load
#
# How to use it:
# import sys
# sys.path.append('/usr/local/include')
# from loadcheck import LoadCheck
# LoadCheck(0,900)

# Necessary modules
import os
import sys
import time
from subprocess import Popen, PIPE, STDOUT

#--------------------------------------------------------------------
# The max. load is dependent on the number of CPUs
LC_MaxLoad = 1
LC_NoCPU = 0
for line in open('/proc/cpuinfo', 'r'):
    if 'processor' in line:
        LC_NoCPU += 1

if LC_NoCPU> 16:
    LC_MaxLoad = LC_NoCPU/ 4
elif LC_NoCPU > 1:
    LC_MaxLoad = LC_NoCPU / 2

#--------------------------------------------------------------------
def LoadCheck(LC_Debug=0, LC_MaxTime=900):

    if LC_Debug:
        sys.stderr.write(
            "DEBUG: Max. CPU load is %f\nDEBUG: Max. runtime is %d second(s)\n"
            % (LC_MaxLoad, LC_MaxTime)
        )
    LC_CurrentLoad = 9999
    StartTime = time.time()
    LC_Wait = 0
    while LC_CurrentLoad > LC_MaxLoad:
        with open("/proc/loadavg") as f:
            LoadAVG = f.readline().split(" ", 1)
            LC_CurrentLoad = float(LoadAVG[0])
        if LC_Debug:
            sys.stderr.write(
                "DEBUG: LC_CurrentLoad = %f, MaxLoad = %f\n" % (LC_CurrentLoad, LC_MaxLoad)
            )
        if LC_CurrentLoad > LC_MaxLoad:
            # Wait a bit
            time.sleep(2)
            LC_Wait += 2
        Now = time.time()
        Elapsed = time.time() - StartTime
        if Elapsed > LC_MaxTime:
            if LC_Debug:
                sys.stderr.write("DEBUG: Max. runtime has elapsed\n")
            break
    if LC_Debug and LC_Wait:
        sys.stderr.write(
            "DEBUG: CPU load after waiting %d seconds is %f (max. allowed load is %f)\n"
            % (LC_Wait, LC_CurrentLoad, LC_MaxLoad)
        )
