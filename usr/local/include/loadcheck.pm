#--------------------------------------------------------------------
# (c) CopyRight 2023 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#--------------------------------------------------------------------
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/include/loadcheck.pm
# Perl module to throttle a script by checking CPU load

# The max. CPU load to check against
my $LC_MaxLoad = -1;

# Calculate the max. CPU load (for throttling)
if ( open( my $LC_ML, '<', '/proc/cpuinfo' ) )
{
    $LC_MaxLoad = 0;
    while (<$LC_ML>)
    {
        $LC_MaxLoad++ if (/^processor\s+:/o);
    } ## end while (<$LC_ML>)
    close($LC_ML);
} ## end if ( open( my $LC_ML, ...))
if ( $LC_MaxLoad > 1 )
{
    $LC_MaxLoad = ( $LC_MaxLoad > 16 ) ? $LC_MaxLoad / 4 : $LC_MaxLoad / 2;
} ## end if ( $LC_MaxLoad > -1 ...)

sub LoadCheck()
{
    # How to use it:
    #require '/usr/local/include/loadcheck.pm';
    #LoadCheck(0|1,<max. runtime in seconds|900>);
    # By default, no debugging and run for max. 15 minutes
    my $LC_Debug      = shift || 0;
    my $LC_MaxRunTime = shift || 60 * 15;
    warn
        "DEBUG: Max. CPU load is $LC_MaxLoad\nDEBUG: Max. runtime is $LC_MaxRunTime second(s)\n"
        if ($LC_Debug);

    # Local variables
    my $LC_Wait = 0;
    if ( $LC_MaxLoad > -1 )
    {
        my $LC_StartTime = time();
        # Sleep a bit if the current CPU load is higher than the max.
        my $LC_CurrentLoad = -1;
        while (( $LC_CurrentLoad == -1 )
            or ( $LC_CurrentLoad > $LC_MaxLoad ) )
        {
            if ( open( my $LC_LA, '<', '/proc/loadavg' ) )
            {
                ($LC_CurrentLoad) = split( /\s+/, <$LC_LA> );
                close($LC_LA);
            } ## end if ( open( my $LC_LA, ...))
            if ( $LC_CurrentLoad > -1 )
            {
                if ($LC_Debug)
                {
                    my $LC_Now = time();
                    warn 'DEBUG: Current CPU load after '
                        . ( $LC_Now - $LC_StartTime )
                        . " second(s) is $LC_CurrentLoad (max. allowed load is $LC_MaxLoad)\n";
                } ## end if ($LC_Debug)
                last if ( $LC_CurrentLoad < $LC_MaxLoad );
                $LC_Wait += 2 if ($LC_Debug);
                sleep(2);
            } ## end if ( $LC_CurrentLoad >...)
            my $LC_Now = time();
            if ( ( $LC_Now - $LC_StartTime ) > $LC_MaxRunTime )
            {
                warn "DEBUG: Max. runtime has elapsed\n" if ($LC_Debug);
                last;
            } ## end if ( ( $LC_Now - $LC_StartTime...))
        } ## end while ( ( $LC_CurrentLoad...))
    } else
    {
        # By default sleep half a minute
        warn "DEBUG: No max. CPU load defined - waiting 30 seconds\n"
            if ($LC_Debug);
        $LC_Wait = 30 if ($LC_Debug);
        sleep(30);
    } ## end else [ if ( $LC_MaxLoad > -1 ...)]
    if ( ($LC_Debug) and ($LC_Wait) )
    {
        my $LC_CurrentLoad = -1;
        while ( $LC_CurrentLoad == -1 )
        {
            if ( open( my $LC_LA, '<', '/proc/loadavg' ) )
            {
                ($LC_CurrentLoad) = split( /\s+/, <$LC_LA> );
                close($LC_LA);
            } ## end if ( open( my $LC_LA, ...))
        } ## end while ( $LC_CurrentLoad ==...)
        warn
            "DEBUG: CPU load after waiting $LC_Wait seconds is $LC_CurrentLoad (max. allowed load is $LC_MaxLoad)\n";
    } ## end if ( ($LC_Debug) and (...))
} ## end sub LoadCheck

sub ThrottleProcess()
{
    # How to use it:
    #require '/usr/local/include/loadcheck.pm';
    #my $pid = fork();
    #die "ERROR: Can't fork: $!\n" unless (defined $pid);
    #if( $pid > 0)
    #{
    #   # Throttle child process until it ends
    #   ThrottleProcess($pid, [0|1]);
    #} elsif ($pid == 0)
    #{
    #   #  Do whatever is needed in child process
    #   ...
    #}
    my $LC_Pid   = shift || 0;
    my $LC_Debug = shift || 0;

    # Do nothing unless we have a valid process ID
    #  or are asked to stop ourself
    return if ( ( $LC_Pid == 0 ) or ( $LC_Pid == $$ ) );

    # Local variables
    my $Throttle = 0;

    # This keeps the CPU load in check
    # It requires the process to run in the background, e.g. via "fork"
    warn "DEBUG: Starting process $LC_Pid at full speed\n" if ($LC_Debug);
    while (1)
    {
        kill( 0, $LC_Pid ) or last;

        my $CurrentLoad = -1;
        if ( open( my $LC_LA, '<', '/proc/loadavg' ) )
        {
            ($CurrentLoad) = split( /\s+/, <$LC_LA> );
            close($LC_LA);
        } ## end if ( open( my $LC_LA, ...))
        next unless ( $CurrentLoad > -1 );
        warn
            "DEBUG: Current CPU is $CurrentLoad (max. allowed load is $LC_MaxLoad)\n"
            if ($LC_Debug);
        if ( $CurrentLoad < $LC_MaxLoad )
        {
            # Transition to full speed
            warn "DEBUG: Run process $LC_Pid at full speed\n" if ($LC_Debug);
            $Throttle = 0;
            sleep(0.5);
        } else
        {
            # Transition to throttle
            warn "DEBUG Throttle process $LC_Pid\n" if ($LC_Debug);
            $Throttle = 1;
            sleep(1.5);    # Let it run for a while
            kill( 'STOP', $LC_Pid ) or last;
            sleep(1);      # Pause it for a bit
            kill( 'CONT', $LC_Pid ) or last;
        } ## end else [ if ( $CurrentLoad < $LC_MaxLoad...)]

    } ## end while (1)
} ## end sub ThrottleProcess

1;
