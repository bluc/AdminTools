#!/bin/bash
# Version 20240202-233943 checked into repository
################################################################
# (c) Copyright 2017 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/UpdateBlocklists.sh

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE

# Global variables
THISHOST=$(hostname)
[[ $THISHOST == *.* ]] || THISHOST=$(hostname -f)
DEBUG='-q'
LC_DEBUG=0
if [[ $- ==  *x* ]]
then
    DEBUG='-v'
    LC_DEBUG=1
fi

# The level of blocking
# 1 - minimum
# 2 - medium
# 3 - all
BLK_LEVEL=1

# Force update
FORCE=0

TFILE=$(mktemp /tmp/$$XXXXXXXX)

# Get possible program options
while getopts fL: OPTION
do
    case ${OPTION} in
        L)  BLK_LEVEL=$(printf "%d" $OPTARG 2> /dev/null)
            [ $BLK_LEVEL -lt 1 ] && BLK_LEVEL=1
            [ $BLK_LEVEL -gt 3 ] && BLK_LEVEL=3
            ;;
        f)  FORCE=1
            ;;
        *)  echo "Usage: $0 [-F|-L [1|2|3]] (default=$BLK_LEVEL)"
            ;;
    esac
done
shift $((OPTIND - 1))

#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Bail unless the last update was more than 24 hours ago
#  - or the FORCE flag is set
if [ $FORCE -eq 0 ]
then
    if [ -s /tmp/Blocklist.lastupdate ]
    then
        if [ $(date +%s -r /tmp/Blocklist.lastupdate) -gt $((EPOCHSECONDS - 86400)) ]
        then
            logger -p info -t ${PROG}[$$] -- Last update was less than 24 hours ago
            exit 0
        fi
    fi
fi

#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Install the CIDR aggregate program
[ -x /usr/bin/iprange ] || apt-get -y install iprange
if [ ! -x /usr/bin/iprange ]
then
    [ -x /usr/bin/aggregate ] || apt-get -y install aggregate
fi
# Install the "prips" utility
[ -x /usr/bin/prips ] || apt-get -y install prips
# Install the "ipset" utility
[ -x /sbin/ipset ] || apt-get -y install ipset

#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Enable throttling
source /usr/local/include/loadcheck.sm

#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Get IP addresses and network for bad actors
USER_AGENT=${PROG}'/'$(awk '/^# Version 2/{print $3}' $(command -v UpdateBlocklists.sh))
WGET_OPTS='-q --no-check-certificate -t 3 -T 60 --user-agent='"$USER_AGENT"
[[ "$(wget --help)" =~ compression= ]] && WGET_OPTS="$WGET_OPTS --compression=auto"

# Get the private blocklist
CL1='cl1.btoy1.net'
[ -n "$(pidof tailscaled)" ] && CL1='cl1-btoy1-net'
wget $DEBUG $WGET_OPTS --connect-timeout=30 \
  --header "Content-Type: application/json" -O $TFILE.bluc-bl-blocks \
  --post-data='{"action":"list","s_fqdn":"'${THISHOST}'","s_process":"UpdateBlocklists.sh"}' \
  https://${CL1}/blapi.php
if [ -s $TFILE.bluc-bl-blocks ]
then
    # Extract just the IP addresses
    awk '/^[0-9]/{print $1}' $TFILE.bluc-bl-blocks | sort -u | sed 's@/32@@' > $TFILE.bluc-bl-blocks.txt
    [ "T$DEBUG" = 'T-v' ] && less $TFILE.bluc-bl-blocks.txt
    awk '{print $1" BLUC-BL"}' $TFILE.bluc-bl-blocks.txt > /var/tmp/BL_bluc-bl-blocks.txt
fi

# Get the "DShield" list of netblocks
logger -p info -t ${PROG}[$$] -- Getting DShield blocks
wget $DEBUG $WGET_OPTS http://feeds.dshield.org/block.txt -O $TFILE.dshield-blocks
if [ -s $TFILE.dshield-blocks ]
then
    # Extract just the IP addresses
    awk '/^[0-9]/{print $1"/"$3}' $TFILE.dshield-blocks | sort -u > $TFILE.dshield-blocks.txt
    [ "T$DEBUG" = 'T-v' ] && less $TFILE.dshield-blocks.txt
    awk '{print $1" DShield"}' $TFILE.dshield-blocks.txt > /var/tmp/BL_dshield-blocks.txt
fi

# Get the "SSL blacklist"
logger -p info -t ${PROG}[$$] -- Getting SSL blacklist
wget $DEBUG $WGET_OPTS http://sslbl.abuse.ch/blacklist/sslipblacklist.csv -O $TFILE.sslipblacklist.csv
if [ -s $TFILE.sslipblacklist.csv ]
then
    # Extract just the IP addresses
    awk -F, '{print $2}' $TFILE.sslipblacklist.csv | sort -u > $TFILE.sslipblacklist.txt
    [ "T$DEBUG" = 'T-v' ] && less $TFILE.sslipblacklist.txt
    awk '{print $1" SSLIPBacklist"}' $TFILE.sslipblacklist.txt > /var/tmp/BL_sslipblacklist.txt
fi

# Get the "Malc0de" list
logger -p info -t ${PROG}[$$] -- Getting Malc0de list
wget $DEBUG $WGET_OPTS http://malc0de.com/bl/IP_Blacklist.txt -O $TFILE.malc0de_ip_blacklist
if [ -s $TFILE.malc0de_ip_blacklist ]
then
    # Extract just the IP addresses
    awk '/^[0-9]/{print $1}' $TFILE.malc0de_ip_blacklist | sort -u > $TFILE.malc0de_ip_blacklist.txt
    [ "T$DEBUG" = 'T-v' ] && less $TFILE.malc0de_ip_blacklist.txt
    awk '{print $1" MalC0de"}' $TFILE.malc0de_ip_blacklist.txt > /var/tmp/BL_malc0de_ip_blacklist.txt
fi

# Get the DROP and EDROP lists from spamhaus (based on block level)
logger -p info -t ${PROG}[$$] -- Getting Spamhaus lists
wget $DEBUG $WGET_OPTS 'http://www.spamhaus.org/drop/drop.lasso' -O $TFILE.spamhaus.drop
[ $BLK_LEVEL -gt 1 ] && wget $DEBUG $WGET_OPTS 'http://www.spamhaus.org/drop/edrop.lasso' -O $TFILE.spamhaus.edrop
if [ -s $TFILE.spamhaus.drop ] || [ -s $TFILE.spamhaus.edrop ]
then
    # Extract just the IP addresses
    awk '/^[0-9]/{print $1}' $TFILE.spamhaus*drop | sort -u > $TFILE.spamhaus.txt
    [ "T$DEBUG" = 'T-v' ] && less $TFILE.spamhaus.txt
    awk '{print $1" spamhaus"}' $TFILE.spamhaus.txt > /var/tmp/BL_spamhaus.txt
fi

# Get the OTX malicious hosts lists (based on reputation.alienvault.com)
logger -p info -t ${PROG}[$$] -- Getting OTX Malicious Hosts list
wget $DEBUG $WGET_OPTS --no-check-certificate 'http://consult.btoy1.net/OTX.Malicious.txt' -O $TFILE.OTX.Malicious.txt
if [ -s $TFILE.OTX.Malicious.txt ]
then
    # Extract just the IP addresses
    awk '/^[0-9]/{print $1}' $TFILE.OTX.Malicious.txt | sort -u > $TFILE.OTX.txt
    [ "T$DEBUG" = 'T-v' ] && less $TFILE.OTX.txt
    awk '{print $1" OTX"}' $TFILE.OTX.txt > /var/tmp/BL_OTX.txt
fi

# Get the list of active attackers (from CyberCure)
logger -p info -t ${PROG}[$$] -- Getting CyberCure IP list
wget $DEBUG $WGET_OPTS -H 'Accept: application/json' 'http://api.cybercure.ai/feed/get_ips?type=list' -O $TFILE.CyberCure.txt
if [ -s $TFILE.CyberCure.txt ]
then
    # Extract just the IP addresses
    awk '/^[0-9]/{print $1}' $TFILE.CyberCure.txt | sort -u > $TFILE.CC.txt
    [ "T$DEBUG" = 'T-v' ] && less $TFILE.CC.txt
    awk '{print $1" CC"}' $TFILE.CC.txt > /var/tmp/BL_CC.txt
fi

# Get the list of TOR exit nodes (once ever 35 minutes)
[ -s /var/tmp/BL_torlist.txt ] || touch -d $(date -d '35 min ago') /var/tmp/BL_torlist.txt
if [ $(date +%s -r /var/tmp/BL_torlist.txt) -lt $((EPOCHSECONDS - 2100)) ]
then
    logger -p info -t ${PROG}[$$] -- Getting list of TOR exit nodes
    wget $DEBUG $WGET_OPTS --no-check-certificate 'https://www.dan.me.uk/torlist/' -O $TFILE.torlist
    if [ -s $TFILE.torlist ]
    then
        # Extract just the IPv4 addresses
        awk '/^[0-9]/ && !/:/{print $1}' $TFILE.torlist | sort -u > $TFILE.torlist.txt
        [ "T$DEBUG" = 'T-v' ] && less $TFILE.torlist.txt
        awk '{print $1" TOR"}' $TFILE.torlist.txt > /var/tmp/BL_torlist.txt
    fi
fi

# Get the IPSUM list
[ -s /var/tmp/BL_ipsum.txt ] || touch -d "$(date -d '25 hour ago')" /var/tmp/BL_ipsum.txt
if [ $(date +%s -r /var/tmp/BL_ipsum.txt) -lt $((EPOCHSECONDS - 86400)) ]
then
    logger -p info -t ${PROG}[$$] -- Getting IPSUM list
    wget $DEBUG $WGET_OPTS --no-check-certificate 'https://raw.githubusercontent.com/stamparm/ipsum/master/ipsum.txt' -O $TFILE.ipsumlist
    if [ -s $TFILE.ipsumlist ]
    then
        # Extract just the IP addresses
        awk '!/^#/ && $NF > 2 {print $1}' $TFILE.ipsumlist | sort -u > $TFILE.ipsumlist.txt
        [ "T$DEBUG" = 'T-v' ] && less $TFILE.ipsumlist.txt
        awk '{print $1" IPSUM"}' $TFILE.ipsumlist.txt > /var/tmp/BL_ipsumlist.txt
    fi
fi

if [ $BLK_LEVEL -gt 1 ]
then
    # Get the "Emerging Threads" list of IP addresses (for block level 2 or above)
    logger -p info -t ${PROG}[$$] -- Getting Emerging Threads list
    wget $DEBUG $WGET_OPTS http://rules.emergingthreats.net/blockrules/compromised-ips.txt -O $TFILE.ETBL.txt
    if [ -s $TFILE.ETBL.txt ]
    then
        [ "T$DEBUG" = 'T-v' ] && less $TFILE.ETBL.txt
        awk '{print $1" ETBL"}' $TFILE.ETBL.txt > /var/tmp/BL_ETBL.txt
    fi

    # Get the "Rutgers DROP" list of IP addresses (for block level 2 or above)
    logger -p info -t ${PROG}[$$] -- Getting Rutgers DROP list
    wget $DEBUG $WGET_OPTS https://report.rutgers.edu/DROP/attackers -O $TFILE.RDrop.txt
    if [ -s $TFILE.RDrop.txt ]
    then
        [ "T$DEBUG" = 'T-v' ] && less $TFILE.RDrop.txt
        awk '{print $1" RDrop"}' $TFILE.RDrop.txt > /var/tmp/BL_RDrop.txt
    fi

    # Get the "Binary Defense" list (for block level 2 or above)
    logger -p info -t ${PROG}[$$] -- Getting Binary Defense list
    wget $DEBUG $WGET_OPTS http://www.binarydefense.com/banlist.txt -O $TFILE.bindefense-list
    if [ -s $TFILE.bindefense-list ]
    then
        # Extract just the IP addresses
        awk '/^[0-9]/{print $1}' $TFILE.bindefense-list | sort -u > $TFILE.bindefense.txt
        [ "T$DEBUG" = 'T-v' ] && less $TFILE.bindefense.txt
        awk '{print $1" BinDefense"}' $TFILE.bindefense.txt > /var/tmp/BL_bindefense.txt
    fi

    # Get "fail2ban based" lists of IP addresses (for block level 2 or above)
    logger -p info -t ${PROG}[$$] -- Getting blocklist.de
    wget $DEBUG $WGET_OPTS https://www.blocklist.de/downloads/export-ips_all.txt -O $TFILE.BL_DE.txt
    if [ -s $TFILE.BL_DE.txt ]
    then
        [ "T$DEBUG" = 'T-v' ] && less $TFILE.BL_DE.txt
        awk '{print $1" BL_DE"}' $TFILE.BL_DE.txt > /var/tmp/BL_BL_DE.txt
    fi

    if [ $BLK_LEVEL -gt 2 ]
    then
        # Get the Firehol list of netblocks (for block level 3 or above)
        BO=''
        [[ $THISHOST =~ gw.btoy1* ]] && BO="--no-proxy --bind-address $(ifconfig eth1 | sed -n -e 's/\(.*\)inet addr:\(.*\)B.*$/\2/p')"
        logger -p info -t ${PROG}[$$] -- Getting Firehol list
        wget $DEBUG $WGET_OPTS --no-check-certificate $BO \
            https://raw.githubusercontent.com/firehol/blocklist-ipsets/master/firehol_level1.netset -O $TFILE.firehol_level1
        if [ -s $TFILE.firehol_level1 ]
        then
            # Extract just the IP addresses
            grep -Eav '^(0|127|10|192\.168|172\.16|224)\.' $TFILE.firehol_level1 | \
                awk '/^[0-9]/{print $1}' > $TFILE.firehol_level1.txt
            [ "T$DEBUG" = 'T-v' ] && less $TFILE.firehol_level1.txt
            awk '{print $1" Firehol_L1"}' $TFILE.firehol_level1.txt > /var/tmp/BL_firehol_level1.txt
        fi

        # Get the IpBlockLists (for block level 3 or above)
        logger -p info -t ${PROG}[$$] -- Getting IpBlock list
        wget $DEBUG $WGET_OPTS http://lists.blocklist.de/lists/all.txt -O $TFILE.ipblocklist.txt
        if [ -s $TFILE.ipblocklist.txt ]
        then
            [ "T$DEBUG" = 'T-v' ] && less $TFILE.ipblocklist.txt
            awk '{print $1" IPBlocklist"}' $TFILE.ipblocklist.txt > /var/tmp/BL_ipblocklist.txt
        fi

        # Get IP addresses from various other sources, sort them,
        #  aggregate them into subnets and weed out local subnets
        logger -p info -t ${PROG}[$$] -- Getting BotScout list
        wget $DEBUG $WGET_OPTS http://raw.githubusercontent.com/firehol/blocklist-ipsets/master/botscout_1d.ipset -O $TFILE.other.botscout
        if [ -s $TFILE.other.botscout ]
        then
            [ "T$DEBUG" = 'T-v' ] && less $TFILE.other.botscout
            awk '/^[0-9]/ {print $1" botscout"}' $TFILE.other.botscout | grep -Eav '^(127\.|192\.168.\0|10\.0)' > /var/tmp/BL_botscout
        fi

        logger -p info -t ${PROG}[$$] -- Getting BiAny list
        wget $DEBUG $WGET_OPTS http://raw.githubusercontent.com/firehol/blocklist-ipsets/master/bi_any_2_30d.ipset -O $TFILE.other.bi_any
        if [ -s $TFILE.other.bi_any ]
        then
            [ "T$DEBUG" = 'T-v' ] && less $TFILE.other.bi_any
            awk '/^[0-9]/ {print $1" bi_any"}' $TFILE.other.bi_any | grep -Eav '^(127\.|192\.168.\0|10\.0)' > /var/tmp/BL_bi_any
        fi

        logger -p info -t ${PROG}[$$] -- Getting Snort list
        wget $DEBUG $WGET_OPTS http://raw.githubusercontent.com/firehol/blocklist-ipsets/master/snort_ipfilter.ipset -O $TFILE.other.snort
        if [ -s $TFILE.other.snort ]
        then
            [ "T$DEBUG" = 'T-v' ] && less $TFILE.other.snort
            awk '/^[0-9]/ {print $1" snort"}' $TFILE.other.snort | grep -Eav '^(127\.|192\.168.\0|10\.0)'> /var/tmp/BL_snort
        fi

        logger -p info -t ${PROG}[$$] -- Getting Bambenek C+C list
        wget $DEBUG $WGET_OPTS http://osint.bambenekconsulting.com/feeds/c2-ipmasterlist.txt -O $TFILE.other.bambenek
        if [ -s TFILE.other.bambenek ]
        then
            [ "T$DEBUG" = 'T-v' ] && less $TFILE.other.bambenek
            awk -F, '/^[0-9]/ {print $1" Bambenek"}' $TFILE.other.bambenek | grep -Eav '^(127\.|192\.168.\0|10\.0)'> /var/tmp/BL_bambenek
        fi
    fi
fi

# Collect some IP addresses to exclude from blocking
logger -p info -t ${PROG}[$$] -- Collecting IP addresses to exclude
if [ -x /usr/bin/dig ]
then
    while true
    do
        dig cl1.btoy1.net +short 2> /dev/null >> $TFILE.exclude
        [ $? -eq 0 ] && break
    done
    while true
    do
        dig cl2.btoy1.net +short 2> /dev/null >> $TFILE.exclude
        [ $? -eq 0 ] && break
    done
    while true
    do
        dig btoy1.mooo.com +short 2> /dev/null >> $TFILE.exclude
        [ $? -eq 0 ] && break
    done
    while true
    do
        dig raw.githubusercontent.com +short 2> /dev/null >> $TFILE.exclude
        [ $? -eq 0 ] && break
    done
else
    while true
    do
        (dig +short cl1.btoy1.net 2> /dev/null) >> $TFILE.exclude
        [ $? -eq 0 ] && break
    done
    while true
    do
        (dig +short cl2.btoy1.net 2> /dev/null) >> $TFILE.exclude
        [ $? -eq 0 ] && break
    done
    while true
    do
        (dig +short btoy1.mooo.com 2> /dev/null) >> $TFILE.exclude
        [ $? -eq 0 ] && break
    done
    while true
    do
        (dig +short raw.githubusercontent.com 2> /dev/null) >> $TFILE.exclude
        [ $? -eq 0 ] && break
    done
fi
# Exclude the IP addresses for UptimeRobot
wget $DEBUG $WGET_OPTS -O /tmp/UT.txt https://uptimerobot.com/inc/files/ips/IPv4.txt
if [ -s /tmp/UT.txt ]
then
    diff -wu /tmp/UT.txt /var/tmp/UT.txt &> /dev/null
    [ $? -ne 0 ] && cp /tmp/UT.txt /var/tmp
fi
cat /var/tmp/UT.txt >> $TFILE.exclude
[ "T$DEBUG" = 'T-v' ] && less $TFILE.exclude

# Get the IP addresses, sort them and weed out any duplicates
LoadCheck $LC_DEBUG 900
logger -p info -t ${PROG}[$$] -- Weed out duplicate IP addresses
grep -h '^[0-9]' /var/tmp/BL_*.txt | grep -Fv ':' | sort -u -k1 | \
    grep -Eavf $TFILE.exclude > $TFILE.AllBlacklistedIPAddresses.txt
LoadCheck $LC_DEBUG 900
perl -ani -e 'print unless $seen{$F[0]}++' $TFILE.AllBlacklistedIPAddresses.txt

# Separate hosts from networks
logger -p info -t ${PROG}[$$] -- Create separate lists for single addresses and networks
if [ -x /usr/bin/iprange ]
then
    IDEBUG=$DEBUG
    [ "T$DEBUG" = 'T-q' ] && IDEBUG=''
    LoadCheck $LC_DEBUG 900
    grep -Fv '/' $TFILE.AllBlacklistedIPAddresses.txt | cut -d' ' -f1 | \
        iprange --ipset-reduce 20 --ipset-reduce-entries 80000 $IDEBUG > /tmp/AllBlacklistedHosts.txt
    LoadCheck $LC_DEBUG 900
    grep -F '/' $TFILE.AllBlacklistedIPAddresses.txt | cut -d' ' -f1 | \
        iprange --ipset-reduce 20 --ipset-reduce-entries 80000 $IDEBUG > /tmp/AllBlacklistedNets.txt
elif [ -x /usr/bin/aggregate ]
then
    LoadCheck $LC_DEBUG 900
    grep -Fv '/' $TFILE.AllBlacklistedIPAddresses.txt | \
        aggregate -t $DEBUG > /tmp/AllBlacklistedHosts.txt
    LoadCheck $LC_DEBUG 900
    grep -F '/' $TFILE.AllBlacklistedIPAddresses.txt > /tmp/AllBlacklistedNets.txt
else
    LoadCheck $LC_DEBUG 900
    grep -Fv '/' $TFILE.AllBlacklistedIPAddresses.txt > /tmp/AllBlacklistedHosts.txt
    LoadCheck $LC_DEBUG 900
    grep -F '/' $TFILE.AllBlacklistedIPAddresses.txt > /tmp/AllBlacklistedNets.txt
fi

# Download geo databases
mkdir -p /var/tmp/countrydb
logger -p info -t ${PROG}[$$] -- Updating geo databases
wget $DEBUG $WGET_OPTS -O /tmp/all-zones.tar.gz \
    http://www.ipdeny.com/ipblocks/data/countries/all-zones.tar.gz
if [ $? -eq 0 ] && [ -s /tmp/all-zones.tar.gz ]
then
    tar -C /var/tmp/countrydb -xzf /tmp/all-zones.tar.gz &
    ThrottleProcess $!

    if [ -x /usr/bin/iprange ]
    then
        IDEBUG=$DEBUG
        [ "T$DEBUG" = 'T-q' ] && IDEBUG=''
        for ZF in /var/tmp/countrydb/*.zone
        do
            LoadCheck $LC_DEBUG 900
            iprange --ipset-reduce 20 --ipset-reduce-entries 80000 $IDEBUG $ZF > ${ZF%.zone}.txt 2> /dev/null
        done
    fi

    [ "T$DEBUG" = 'T-v' ] && cat << EOT > /var/tmp/countrydb/README
To activate/use the databases in firehol, add these lines
 to "/etc/firehol/firehol.conf" (example is for Russia):

# One set for the single IPs
ipv4 ipset create C_IP_ru hash:ip
ipv4 ipset addfile C_IP_ru /var/tmp/countrydb/ru.zone
# One set for the CIDR addresses
ipv4 ipset create C_NETS_ru hash:nets
ipv4 ipset addfile C_NETS_ru /var/tmp/countrydb/ru.zone

# Enable the next line if you want to stop traffic to/from this country
# ipv4 blacklist full ipset:C_NETS_ru ipset:C_IP_ru log "Geo rejection: Russia"

Note: The above commands only work with firehol version 3 and above!
EOT
fi

# We are done
date > /tmp/Blocklist.lastupdate
logger -p info -t ${PROG}[$$] -- Done
exit 0
