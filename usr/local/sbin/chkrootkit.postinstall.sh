#!/bin/bash
################################################################
# (c) Copyright 2018 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/chkrootkit.postinstall.sh

# Run this code once a night to keep chkrootkit updated:
## DEBUG='-q'
## [[ $- ==  *x* ]] && DEBUG='-v'
## # Update chkrootkit
## wget -q --tries=2 --timeout=60 \
##   https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/chkrootkit.postinstall.sh \
##   -O /usr/local/sbin/chkrootkit.postinstall.sh
## if [ $? -eq 0 -a -s /usr/local/sbin/chkrootkit.postinstall.sh ]
## then
##     chmod 0744 /usr/local/sbin/chkrootkit.postinstall.sh
##     /usr/local/sbin/chkrootkit.postinstall.sh
##     [ "T$DEBUG" = 'T-v' ] && echo -n 'Currently installed '; chkrootkit -V
## fi

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Should we force updates
FORCE=0
if [ $# -ge 1 ]
then
    [ "T${1^^}" == 'TFORCE' ] && FORCE=1
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE

DEBUG='-q'
[[ $- ==  *x* ]] && DEBUG='-v'

# We need the standard Debian/Ubuntu package first
dpkg-query -W chkrootkit &> /dev/null || exit 1

#--------------------------------------------------------------------
# Get the source code
WGET_OPTS='--tries=2 --timeout=60'
wget $DEBUG $WGET_OPTS 'http://cl1.btoy1.net:81/chkrootkit.tar.gz' -O /tmp/chkrootkit.tar.gz 
if [ $? -ne 0 -o ! -s /tmp/chkrootkit.tar.gz ]
then
    wget $DEBUG $WGET_OPTS 'ftp://ftp.pangeia.com.br/pub/seg/pac/chkrootkit.tar.gz' -O /tmp/chkrootkit.tar.gz
fi
if [ $? -eq 0 -a -s /tmp/chkrootkit.tar.gz ]
then
    cmp /tmp/chkrootkit.tar.gz /usr/local/src/chkrootkit.tar.gz &> /dev/null
    [ $? -ne 0 ] && cat /tmp/chkrootkit.tar.gz > /usr/local/src/chkrootkit.tar.gz

    # Create the executables
    rm -rf /tmp/chkrootkit*
    cd /tmp
    tar xzf /usr/local/src/chkrootkit.tar.gz
    cd chkrootkit*
    REBUILD=0
    if [ $FORCE -eq 0 ]
    then
        /usr/sbin/chkrootkit -V &> /tmp/$$.ov
        ./chkrootkit -V &> /tmp/$$.nv
        diff /tmp/$$.ov /tmp/$$.nv &> /dev/null
        REBUILD=$?
    else
        REBUILD=1
    fi
    if [ $REBUILD -ne 0 ]
    then
        [ -x /usr/bin/make ] || apt-get install make
        # Let chkproc default to procps version 3
        sed -i 's/pv = verbose = 0;/pv = 3; verbose = 0;/' chkproc.c
        # Log files are in /var/log
        sed -i 's@/var/adm@/var/log@g' *
        make
        # Install them
        for F in /usr/lib/chkrootkit/*
        do
            [ -x ${F##*/} ] && cat ${F##*/} > $F
        done
        sed 's@# Workaround@# Use the correct utility directory\ncd /usr/lib/chkrootkit\n\n# Workaround@;s@### chkrootkit@exit 0\n### chkrootkit@' chkrootkit > /usr/sbin/chkrootkit
        chmod 0755 /usr/sbin/chkrootkit
    fi

    # Get rid of the evidence :)
    cd /tmp; rm -rf chkrootkit*
fi

# We are done
exit 0
