#!/bin/bash
################################################################
# (c) Copyright 2020 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/DailyServerStats.sh

# This is a companion script to SysMon.pl which summarizes
#  and visualizes the data collected by SysMon through a day

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin

PROG=${0##*/}
ionice -c2 -n7 -p $$

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE

# Define the email address for the resulting image
EMAIL_TO=''

# Define the day to get stats for
STATS_DAY='yesterday'

THISHOST=$(hostname)
[[ $THISHOST == *.* ]] || THISHOST=$(hostname -f)
THISDOMAIN=${THISHOST#*.}

#--------------------------------------------------------------------
# Install any necessary programs
for P in ploticus sendemail #webfs
do
    dpkg-query -W $P &>/dev/null
    [ $? -ne 0 ] && apt-get -y install $P
done

#--------------------------------------------------------------------
# Get possible program options
while getopts d:ht: OPTION
do
    case ${OPTION} in
    d)  STATS_DAY="$OPTARG"
        ;;
    t)  EMAIL_TO="$OPTARG"
        ;;
    *)  cat << EOT
Usage: $PROG [options]
       -t email  Send report to email address [default=don't send]
       -d day    Specify day to get stats for [default=$STATS_DAY]
EOT
        exit 0
        ;;
    esac
done
shift $((OPTIND - 1))

#--------------------------------------------------------------------
# Define the day to collect stats for
YESTERDAY=$(date '+%s' -d "$STATS_DAY")
DAY=$(date +%F -d @$YESTERDAY)
CSV_BASENAME=/var/tmp

#--------------------------------------------------------------------
# Extract the info for the desired day and hour
# 1. Overall server stats
TFILE=$(mktemp /tmp/$$XXXXXXXX)
for F in CPU IO Memory
do
    [ -s $CSV_BASENAME/${F}.csv ] && grep "^$DAY" $CSV_BASENAME/${F}.csv > ${TFILE}.${F}.csv
done
# 2. Interface stats
ETHS=''
for ETH in $(grep ':' /proc/net/dev | cut -d: -f1 | egrep -v '(lo|tap|vmbr|ovs|docker|veth|virbr)')
do
    if [ -s $CSV_BASENAME/${ETH}.csv ]
    then
        grep "^$DAY" $CSV_BASENAME/${ETH}.csv > ${TFILE}.ETH_${ETH}.csv
        ETHS="$ETH $ETHS"
    fi
done
# 3. Filesystem stats
FSS=''
for FS in $(awk '/[0-9]$/{print $NF}' /proc/partitions)
do
    if [ -s "$CSV_BASENAME/#dev#${FS}.csv" -a -s "$CSV_BASENAME/Inodes_#dev#${FS}.csv" ]
    then
        grep "^$DAY" "$CSV_BASENAME/#dev#${FS}.csv" > ${TFILE}.FS_USE_$FS.csv
        grep "^$DAY" "$CSV_BASENAME/Inodes_#dev#${FS}.csv" > ${TFILE}.FS_INODES_$FS.csv
        FSS="$FS $FSS"
    fi
done
# 4. Disk stats
DISKS=''
for DISK in $(awk '$4>0{print $3}' /proc/diskstats)
do
    if [ -s $CSV_BASENAME/${DISK}.csv ]
    then
        grep "^$DAY" $CSV_BASENAME/${DISK}.csv > ${TFILE}.DISK_${DISK}.csv
        DISKS="$DISK $DISKS"
    fi
done

#--------------------------------------------------------------------
# Gather stats
for HOUR in $(seq 0 23)
do
    HOUR=$(printf "%02d" $HOUR)

    # 1. CPU stats
    CPU_AVG=0
    [ -s ${TFILE}.CPU.csv ] && CPU_AVG=$(awk -F, "/$HOUR"'/{n++;sum+=$NF} END {print n?sum/n:0}' ${TFILE}.CPU.csv)
    # 2. IO stats
    IO_AVG=0  
    [ -s ${TFILE}.IO.csv ] && IO_AVG=$(awk -F, "/$HOUR"'/{n++;sum+=$NF} END {print n?sum/n:0}' ${TFILE}.IO.csv)
    # 3. Memory stats
    MEM_AVG=0
    [ -s ${TFILE}.Memory.csv ] && MEM_AVG=$(awk -F, "/$HOUR"'/{n++;sum+=$NF} END {print n?sum/n:0}' ${TFILE}.Memory.csv)

    # Write out the data into a file in this format:
    #
    #Time,CPU_AVG,IO_AVG,MEM_AVG
    #00:00:00,6,2,1
    #01:00:00,3,1,1
    #02:00:00,5,1,1

    # Remove any existing line for the hour
    [ -s ${CSV_BASENAME}/CPU.Standard ] && sed -i -e "/^${HOUR}:00:00/d" ${CSV_BASENAME}/CPU.Standard
    # Finally write the line
    cat << EOT >> ${CSV_BASENAME}/CPU.Standard
${HOUR}:00:00,$CPU_AVG,$IO_AVG,$MEM_AVG
EOT

    # 4. Interface stats
    for ETH in $ETHS
    do
        RX_SPEED=0
        TX_SPEED=0
        RX_ERR_AVG=0
        TX_ERR_AVG=0
        if [ -s ${TFILE}.ETH_${ETH}.csv ]
        then
            # These are in KBytes/sec
            grep "$HOUR" ${TFILE}.ETH_${ETH}.csv > ${TFILE}.${ETH}_$HOUR
            if [ -s ${TFILE}.${ETH}_$HOUR ]
            then
                RX_SPEED=$(awk -F, 'NR==1{v=$3};END{print int(($3 - v)/1024/3600)}' ${TFILE}.${ETH}_$HOUR)
                TX_SPEED=$(awk -F, 'NR==1{v=$11};END{print int(($11 - v)/1024/3600)}' ${TFILE}.${ETH}_$HOUR)

                RX_ERR_AVG=$(awk -F, "/$HOUR"'/{n++;sum+=$5} END {print n?sum/n:0}' ${TFILE}.ETH_${ETH}.csv)
                TX_ERR_AVG=$(awk -F, "/$HOUR"'/{n++;sum+=$13} END {print n?sum/n:0}' ${TFILE}.ETH_${ETH}.csv)
            fi
        fi

        # Remove any existing line for the hour
        [ -s ${CSV_BASENAME}/ETH_${ETH}.Standard ] && sed -i -e "/^${HOUR}:00:00/d" ${CSV_BASENAME}/ETH_${ETH}.Standard
        # Finally write the line
        cat << EOT >> ${CSV_BASENAME}/ETH_${ETH}.Standard
${HOUR}:00:00,$RX_SPEED,$RX_ERR_AVG,$TX_SPEED,$TX_ERR_AVG
EOT
    done

    # 5. Filesystem stats
    for FS in $FSS
    do
        FS_USE_AVG=0
        [ -s "${TFILE}.FS_USE_${FS}.csv" ] && FS_USE_AVG=$(awk -F, "/$HOUR"'/{n++;sum+=$NF} END {print n?sum/n:0}' "${TFILE}.FS_USE_${FS}.csv")
        FS_INODE_AVG=0
        [ -s "${TFILE}.FS_USE_${FS}.csv" ] && FS_INODE_AVG=$(awk -F, "/$HOUR"'/{n++;sum+=$NF} END {print n?sum/n:0}' "${TFILE}.FS_INODES_${FS}.csv")

        # Remove any existing line for the hour
        [ -s ${CSV_BASENAME}/FS_${FS}.Standard ] && sed -i -e "/^${HOUR}:00:00/d" ${CSV_BASENAME}/FS_${FS}.Standard
        # Finally write the line
        cat << EOT >> ${CSV_BASENAME}/FS_${FS}.Standard
${HOUR}:00:00,$FS_USE_AVG,$FS_INODE_AVG
EOT
    done

    # 6. Disk stats
    for DISK in $DISKS
    do
        DISK_WR_SPEED=0
        DISK_RD_SPEED=0
        if [ -s ${TFILE}.DISK_${DISK}.csv ]
        then
            # These are in KBytes/sec
            grep "$HOUR" ${TFILE}.DISK_${DISK}.csv > ${TFILE}.${DISK}_$HOUR
            if [ -s ${TFILE}.${DISK}_$HOUR ]
            then
                DISK_WR_SPEED=$(awk -F, 'NR==1{v=$3};END{print int(($3 - v)/1024/3600)}' ${TFILE}.${DISK}_$HOUR)
                DISK_RD_SPEED=$(awk -F, 'NR==1{v=$4};END{print int(($4 - v)/1024/3600)}' ${TFILE}.${DISK}_$HOUR)
            fi
        fi

        # Remove any existing line for the hour
        [ -s ${CSV_BASENAME}/DISK_${DISK}.Standard ] && sed -i -e "/^${HOUR}:00:00/d" ${CSV_BASENAME}/DISK_${DISK}.Standard
        # Finally write the line
        cat << EOT >> ${CSV_BASENAME}/DISK_${DISK}.Standard
${HOUR}:00:00,$DISK_RD_SPEED,$DISK_WR_SPEED
EOT
    done
done

# We are done if we don't have to email the graphs
[ -z "$EMAIL_TO" ] && exit 0

#--------------------------------------------------------------------
# Plot the data
# See also http://ploticus.sourceforge.net/gallery/sar-cpu.htm
DEBUG=''
[[ $- ==  *x* ]] && DEBUG='-debug'

# Create the graph for the CPU stats
ploticus -png -o ${TFILE}.CPU.png -stdin $DEBUG << EOT
#proc page
#if @DEVICE in png,gif
   scale: 0.9
#endif

#proc getdata
file: ${CSV_BASENAME}/CPU.Standard
fieldnames: time load ioload memory
delim: comma

#proc areadef
   areaname: standard
   title: CPU statistics
   titledetails: size=14 align=C
   xscaletype: time hh:mm:ss
   xrange: 00:00:00 23:59:59
   yautorange: datafield=load,ioload,memory lowfix=-0.01

#proc xaxis
   stubs: inc 2 hours
   stubformat: hhA
   minorticinc: 1 hour
   stubvert: yes
      
#proc yaxis
  stubs: inc
  gridskip: minmax
  ticincrement: 100 1000
  grid: color=dullyellow width=0.1 style=2
  label: average

#proc curvefit
  xfield: time
  yfield: load
  linedetails: color=red width=.5
  legendlabel: #usefname

#proc curvefit
  xfield: time
  yfield: ioload
  linedetails: color=orange width=.5
  legendlabel: #usefname

#proc curvefit
  xfield: time
  yfield: memory
  linedetails: color=blue width=.5
  legendlabel: #usefname

#proc legend
  location: max-1 max-0.1
  seglen: 0.2
EOT

for ETH in $ETHS
do
    # Create the graph for the Ethernet stats
    ploticus -png -o ${TFILE}.ETH_${ETH}.png -stdin $DEBUG << EOT
#proc page
#if @DEVICE in png,gif
   scale: 0.9
#endif

#proc getdata
file: ${CSV_BASENAME}/ETH_${ETH}.Standard
fieldnames: time rxKBps rxerrors txKBps txerrors
delim: comma

#proc areadef
   areaname: standard
   title: Ethernet $ETH statistics
   titledetails: size=14 align=C
   xscaletype: time hh:mm:ss
   xrange: 00:00:00 23:59:59
   yautorange: datafield=rxKBps,txKBps lowfix=-0.01

#proc xaxis
   stubs: inc 2 hours
   stubformat: hhA
   minorticinc: 1 hour
   stubvert: yes
      
#proc yaxis
  stubs: inc
  gridskip: minmax
  ticincrement: 100 1000
  grid: color=dullyellow width=0.1 style=2
  label: KB/sec

#proc curvefit
  xfield: time
  yfield: rxKBps
  linedetails: color=red width=.5
  legendlabel: #usefname

#proc curvefit
  xfield: time
  yfield: txKBps
  linedetails: color=blue width=.5
  legendlabel: #usefname

#proc curvefit
  xfield: time
  yfield: rxerrors
  linedetails: color=orange width=.5
  legendlabel: #usefname

#proc curvefit
  xfield: time
  yfield: txerrors
  linedetails: color=skyblue width=.5
  legendlabel: #usefname

#proc legend
  location: max-1 max-0.1
  seglen: 0.2
EOT
done

for FS in $FSS
do
    # Create the graph for the Ethernet stats
    ploticus -png -o ${TFILE}.FS_${FS}.png -stdin $DEBUG << EOT
#proc page
#if @DEVICE in png,gif
   scale: 0.9
#endif

#proc getdata
file: ${CSV_BASENAME}/FS_${FS}.Standard
fieldnames: time usage inodes
delim: comma

#proc areadef
   areaname: standard
   title: Filesystem /dev/$FS statistics
   titledetails: size=14 align=C
   xscaletype: time hh:mm:ss
   xrange: 00:00:00 23:59:59
   yautorange: datafield=usage,inodes lowfix=-0.01

#proc xaxis
   stubs: inc 2 hours
   stubformat: hhA
   minorticinc: 1 hour
   stubvert: yes
      
#proc yaxis
  stubs: inc
  gridskip: minmax
  ticincrement: 100 1000
  grid: color=dullyellow width=0.1 style=2
  label: percentage

#proc curvefit
  xfield: time
  yfield: usage
  linedetails: color=red width=.5
  legendlabel: #usefname

#proc curvefit
  xfield: time
  yfield: inodes
  linedetails: color=blue width=.5
  legendlabel: #usefname

#proc legend
  location: max-1 max-0.1
  seglen: 0.2
EOT
done

for DISK in $DISKS
do
    # Create the graph for the Ethernet stats
    ploticus -png -o ${TFILE}.DISK_${DISK}.png -stdin $DEBUG << EOT
#proc page
#if @DEVICE in png,gif
   scale: 0.9
#endif

#proc getdata
file: ${CSV_BASENAME}/DISK_${DISK}.Standard
fieldnames: time read-speed write-speed
delim: comma

#proc areadef
   areaname: standard
   title: Disk $DISK statistics
   titledetails: size=14 align=C
   xscaletype: time hh:mm:ss
   xrange: 00:00:00 23:59:59
   yautorange: datafield=read-speed,write-speed lowfix=-0.01

#proc xaxis
   stubs: inc 2 hours
   stubformat: hhA
   minorticinc: 1 hour
   stubvert: yes
      
#proc yaxis
  stubs: inc
  gridskip: minmax
  ticincrement: 100 1000
  grid: color=dullyellow width=0.1 style=2
  label: KBytes/sec

#proc curvefit
  xfield: time
  yfield: read-speed
  linedetails: color=red width=.5
  legendlabel: #usefname

#proc curvefit
  xfield: time
  yfield: write-speed
  linedetails: color=blue width=.5
  legendlabel: #usefname

#proc legend
  location: max-1 max-0.1
  seglen: 0.2
EOT
done

#--------------------------------------------------------------------
# Send the resulting image to "$EMAIL_TO"
DEBUG='-q'
[[ $- ==  *x* ]] && DEBUG='-v'
RN=$(od -vAn -N4 -tu4 < /dev/urandom | awk '{print int($1)}')
rm -f ${TFILE}.daily_stats
if [ $(date +%k) -gt 0 ]
then
    DAY_DAILY=$(date "+%b %d" -d @$YESTERDAY)
    zgrep "^$DAY_DAILY.*DAILY:" /var/log/syslog* | \
      sed "s/^.*DAILY:/ /" > ${TFILE}.daily_stats
fi
if [ -s ${TFILE}.daily_stats ]
then
    echo 'Daily summaries:' > ${TFILE}.mailbody
    for ETH in $ETHS
    do
        egrep "(Receive|Transmit).*for $ETH" ${TFILE}.daily_stats >> ${TFILE}.mailbody
        echo >> ${TFILE}.mailbody
    done

    for FS in $(awk '/[0-9]$/{print $NF}' /proc/partitions)
    do
        FS_MOUNT=$(awk "/$FS/"'{printf ("\\%s", $2)}' /proc/mounts)

        FS_READ=$(grep "Disk read.*for $FS" ${TFILE}.daily_stats)
        [ -z "$FS_READ" ] || (echo -n ' ';sed 's/ =/'" ($FS_MOUNT)"' =/' <<< $FS_READ) >> ${TFILE}.mailbody

        FS_WRITE=$(grep "Disk write.*for $FS" ${TFILE}.daily_stats)
        [ -z "$FS_WRITE" ] || (echo -n ' ';sed 's/ =/'" ($FS_MOUNT)"' =/' <<< $FS_WRITE) >> ${TFILE}.mailbody
         
        echo >> ${TFILE}.mailbody
    done

    MY_STATS=$(grep 'Mysql' ${TFILE}.daily_stats | sed 's/ -.*$//')
    if [ ! -z "$MY_STATS" ]
    then
        cat << EOT >> ${TFILE}.mailbody
$MY_STATS

EOT
    fi

    # Delete duplicate empty lines
    # See http://sed.sourceforge.net/sed1line.txt
    sed -i -e '$!N; /^\(.*\)\n\1$/!P; D' ${TFILE}.mailbody
fi

cat << EOT >> ${TFILE}.mailbody
The attached graphs show:
 Hourly CPU load averages
 Hourly ethernet interface receive and transmit
 Hourly filesystem usage
 Hourly disk read and write
EOT

# Collapse initial spaces into one before sending the email
tr -s " " < ${TFILE}.mailbody | \
  sendemail $DEBUG -f root@$THISDOMAIN -t $EMAIL_TO \
  -a ${TFILE}.CPU.png -a ${TFILE}.ETH*.png \
  -a ${TFILE}.FS*.png -a ${TFILE}.DISK*.png \
  -s mail -u "Daily server statistics for '$THISHOST' for $DAY" -o tls=no \
  -o message-header="Message-ID: <$RN-"$(date +%N)'@'$THISHOST'>'

# We are done
exit 0
