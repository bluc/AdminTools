#!/bin/bash
# Version 20240320-104523 checked into repository
###############################################################
# (c) Copyright 2014 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/SecurityCheck.sh

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            

# We need to be "root" to execute this script
if [ $(/usr/bin/id -u) -ne 0 ]
then
    echo "You need to be 'root' to execute this script"
    exit 1
fi

#--------------------------------------------------------------------
# LYNIS

# Check whether lynis is installed
LYNIS=$(command -v lynis)
if [ $? -eq 0 ]
then
    # Check the installed version of lynis
    CURRENT=$($LYNIS update info | grep 'Up-to-date')
else
    if [ ! -x /usr/local/sbin/lynis.install.sh ]
    then
        # Download the install script
        wget -q --no-check-certificate -O /usr/local/sbin/lynis.install.sh \
          'https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/lynis.install.sh'
        if [ $? -ne 0 ]
        then
            cat << EOT
ERROR: Couldn't download lynis installation script!

Please download 'https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/lynis.install.sh'
into /usr/local/sbin and execute it.
EOT
            exit 1
        fi
        chmod 744 /usr/local/sbin/lynis.install.sh
        CURRENT=''
    fi
fi

if [ -z "$CURRENT" ]
then
    # Install/update lynis
    /usr/local/sbin/lynis.install.sh
    LYNIS=$(command -v lynis)
    if [ -z "$LYNIS" ]
    then
        echo "'lynis' didn't install"
    fi
fi

if [ ! -z "$LYNIS" ]
then
    # Adapt the profile and run the test
    sed -e 's/# plugin/plugin/' /usr/local/etc/lynis/default.prf > /tmp/LocalVA.prf
    $LYNIS audit system --profile /tmp/LocalVA.prf --cronjob &> /dev/null
    case $? in
    0|78)
        if [ -s /var/log/lynis.log ]
        then
            echo '==> Overall hardening strength <=='
            awk '/Hardening strength/ {$1="";$2="";$3="";print}' /var/log/lynis.log
        fi
        if [ -s /var/log/lynis-report.dat ]
        then
            if [ $(grep -c '^warning' /var/log/lynis-report.dat) -ne 0 ]
            then
                echo '==> WARNINGS <=='
                awk -F\| '/^warning/ {$1="";print}' /var/log/lynis-report.dat
            fi
            SUGGESTIONS=$(grep -c '^suggestion' /var/log/lynis-report.dat)
            cat << EOT
==> Other data <==
 $SUGGESTIONS suggestion(s) found - for a list please issue this command:
 awk -F\| '/^suggestion/ {\$1="";print}' /var/log/lynis-report.dat

 For the full report, please issue this command:
 sed -e 's/|//' /var/log/lynis-report.dat
EOT
        fi
        ;;
    *)
        echo "'lynis' didn't run successfully"
        ;;
    esac
fi
    
#--------------------------------------------------------------------
# CHKROOTKIT

# Check whether "chkrootkit" is installed
command -v chkrootkit &> /dev/null
if [ $? -ne 0 ]
then
    apt-get -y install chkrootkit
    if [ $? -ne 0 ]
    then
        cat << EOT
ERROR: Couldn't install chkrootkit!

Please run (as root): apt-get install chkrootkit
EOT
        exit 1
    fi
fi

chattr -i /etc/chkrootkit.conf
cat << EOT > /etc/chkrootkit.conf
RUN_DAILY="false"
RUN_DAILY_OPTS="-q"
DIFF_MODE="false"
EOT
chattr -i /etc/cron.daily/chkrootkit
chmod 644 /etc/cron.daily/chkrootkit

TFILE=$(mktemp /tmp/$$XXXXXXXX)
chkrootkit -q -n &> $TFILE
grep -Ev '(init INFECTED|dhclient|p0f)' $TFILE > $TFILE.report
echo '==> ROOTKIT report <=='
if [ -s $TFILE.report ]
then
    sed -e 's/^/ /' $TFILE.report
else
    echo ' clean'
fi

#--------------------------------------------------------------------
# RKHUNTER

# Check whether "rkhunter" is installed
RKHUNTER=$(command -v rkhunter)
if [ $? -ne 0 ]
then
    apt-get -y install rkhunter
    if [ $? -ne 0 ]
    then
        cat << EOT
ERROR: Couldn't install rkhunter!

Please run (as root): apt-get install rkhunter
EOT
        exit 1
    fi
    RKHUNTER=$(command -v rkhunter)
fi

chattr -i /etc/default/rkhunter
cat << EOT > /etc/default/rkhunter
# Defaults for rkhunter automatic tasks
# sourced by /etc/cron.*/rkhunter and /etc/apt/apt.conf.d/90rkhunter
#
# This is a POSIX shell fragment
#

# Set this to yes to enable rkhunter daily runs
# (default: false)
CRON_DAILY_RUN="false"

# Set this to yes to enable rkhunter weekly database updates
# (default: false)
CRON_DB_UPDATE="yes"

# Set this to yes to enable reports of weekly database updates
# (default: false)
DB_UPDATE_EMAIL="false"

# Set this to the email address where reports and run output should be sent
# (default: root)
REPORT_EMAIL="consult@btoy1.net"

# Set this to yes to enable automatic database updates
# (default: false)
APT_AUTOGEN="yes"

# Nicenesses range from -20 (most favorable scheduling) to 19 (least favorable)
# (default: 0)
NICE="-10"

# Should daily check be run when running on battery
# powermgmt-base is required to detect if running on battery or on AC power
# (default: false)
RUN_CHECK_ON_BATTERY="false"
EOT
chattr -i /etc/cron.daily/rkhunter
chmod 644 /etc/cron.daily/rkhunter

nice -n -10 $RKHUNTER --cronjob --report-warnings-only --appendlog > $TFILE.report
echo '==> RKHUNTER report <=='
if [ -s $TFILE.report ]
then
    sed -e 's/^/ /' $TFILE.report
else
    echo ' clean'
fi

# We are done
exit 0
