#!/bin/bash
# Version 20240124-173039 checked into repository
################################################################
# (c) Copyright 2021 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/MFA.sh

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
#PROG=${0##*/}

TFILE=$(mktemp /tmp/$$XXXXXXXX)
# Install the Google authenticator software
dpkg -S libpam-google-authenticator &> /dev/null || apt-get -y install libpam-google-authenticator

# Adapt sudo and SSH
AA=''
read -rp 'Use MFA for root access via sudo [y/N] ? ' AA
if [ "T${AA^^}" = 'TY' ]
then
    if [ -z "$(grep pam_google_authenticator /etc/pam.d/sudo)" ]
    then
        # Require google authenticator for configured accounts only
        echo 'auth required pam_google_authenticator.so nullok' >> /etc/pam.d/sudo
    fi
fi

AA=''
read -rp 'Use MFA for remote access via ssh [Y/n] ? ' AA
if [ "T${AA^^}" = 'TY' ]
then
    RESTART_SSHD=0
    if [ -z "$(grep pam_google_authenticator /etc/pam.d/sshd)" ]
    then
        # Require google authenticator for configured accounts only
        echo 'auth required pam_google_authenticator.so nullok' >> /etc/pam.d/sshd
        RESTART_SSHD=1
    fi
    if [ -d /etc/ssh/sshd_config.d ]
    then
        if [ ! -s /etc/ssh/sshd_config.d/mfa.conf ]
        then
            cat << EOT > /etc/ssh/sshd_config.d/mfa.conf
# Change to yes to enable challenge-response passwords (beware of
#  possible issues with some PAM modules and threads)
ChallengeResponseAuthentication yes
EOT
            RESTART_SSHD=2
        fi
        if [ -z "$(grep '^Include /etc/ssh/sshd_config.d/..conf' /etc/ssh/sshd_config)" ]
        then
            { echo 'Include /etc/ssh/sshd_config.d/*.conf'; cat /etc/ssh/sshd_config; } > $TFILE
            cat $TFILE > /etc/ssh/sshd_config
            rm -f $TFILE
            RESTART_SSHD=3
        fi
    else
        [ -z "$(grep -i '^challengeresponseauthentication yes' /etc/ssh/sshd_config)" ] && cat << EOT >> /etc/ssh/sshd_config
# Change to yes to enable challenge-response passwords (beware issues with
# some PAM modules and threads)
ChallengeResponseAuthentication yes
EOT
        RESTART_SSHD=4
    fi
    [ $RESTART_SSHD -ne 0 ] && systemctl restart sshd
fi

# Activate MFA for some accounts
getent passwd | awk -F: '$3 > 999 && !/nobody/ {print $1" "$6}' | while read -r ACCT HOME_DIR
do
    [ -s $HOME_DIR/.google_authenticator ] && continue

    read -t 30 -rp "Activate MFA for $ACCT [y/N] ? " AA
    [ "T${AA^^}" = 'TY' ] || continue

    # Activate this account
    su - $ACCT -c 'google-authenticator'
done

# We are done
exit 0
