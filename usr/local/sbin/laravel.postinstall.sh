#!/bin/bash
################################################################
# (c) Copyright 2021 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/laravel.postinstall.sh
# Based on https://tecadmin.net/install-laravel-framework-on-ubuntu/

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            

#--------------------------------------------------------------------
# Global variables
UBUNTU_REL=$(lsb_release  -sc)

#--------------------------------------------------------------------
# Activate custom repositories
[ -z "$(apt-key list 2> /dev/null | grep Maria)" ] && apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
[ -s /etc/apt/sources.list.d/ondrej-ubuntu-apache2-${UBUNTU_REL}.list ] || LC_ALL=C.UTF-8 add-apt-repository -y ppa:ondrej/apache2
[ -s /etc/apt/sources.list.d/ondrej-ubuntu-php-${UBUNTU_REL}.list ] || LC_ALL=C.UTF-8 add-apt-repository -y ppa:ondrej/php
[ $(grep -v '#' /etc/apt/sources.list | grep -c accretive-networks) -eq 1 ] || add-apt-repository -y 'deb [arch=amd64,arm64,ppc64el] http://mirrors.accretive-networks.net/mariadb/repo/10.4/ubuntu bionic main'
apt-get update

#--------------------------------------------------------------------
# Install necessary packages
for P in php8.0 php8.0-gd php8.0-mbstring php8.0-xml php8.0-mcrypt apache2 libapache2-mod-php8.0 mariadb-server php8.0-mysql
do
    dpkg -l | grep "^ii[[:space:]]*${P}[[:space:]]" &> /dev/null || apt-get install -y $P
done

#--------------------------------------------------------------------
# Install PHP composer
curl -sS https://getcomposer.org/installer | php
cmp composer.phar /usr/local/bin/composer &> /dev/null
[ $? -ne 0 ] && mv composer.phar /usr/local/bin/composer
chmod +x /usr/local/bin/composer

#--------------------------------------------------------------------
# Install and configure Laravel
if [ ! -s /var/www/laravel/.env ]
then
    cd /var/www
    git clone https://github.com/laravel/laravel.git
    cd /var/www/laravel
    composer install
    chown -R www-data.www-data /var/www/laravel
    chmod -R 755 /var/www/laravel
    chmod -R 777 /var/www/laravel/storage

    cp .env.example .env
    php artisan key:generate
fi

#--------------------------------------------------------------------
# Create database for Laravel
systemctl enable mariadb
systemctl start mariadb
source /var/www/laravel/.env
[ -z "$DB_PASSWORD" ] && DB_PASSWORD=$(LC_ALL=C tr -dc 'A-Za-z0-9!' </dev/urandom | head -c 16)
mysql --defaults-file=/etc/mysql/debian.cnf -v << EOT
CREATE DATABASE IF NOT EXISTS laravel;
GRANT ALL ON laravel.* to 'laravel'@'localhost' IDENTIFIED BY '$DB_PASSWORD';
FLUSH PRIVILEGES;
EOT
sed -i "s/^DB_PASSWORD.*/DB_PASSWORD=$DB_PASSWORD/" /var/www/laravel/.env

#--------------------------------------------------------------------
# Configure Apache2
rm -f /etc/apache2/sites-enabled/000*
if [ ! -s /etc/apache2/sites-enabled/laravel.conf ]
then
    cat << EOT > /etc/apache2/sites-enabled/laravel.conf
<VirtualHost *:80>

        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/laravel/public

        <Directory />
                Options FollowSymLinks
                AllowOverride None
        </Directory>
        <Directory /var/www/laravel>
                AllowOverride All
        </Directory>

        ErrorLog \${APACHE_LOG_DIR}/error.log
        CustomLog \${APACHE_LOG_DIR}/access.log combined

</VirtualHost>
EOT
    chmod 644 /etc/apache2/sites-enabled/laravel.conf
    service apache2 restart
fi

#--------------------------------------------------------------------
# Adapt firewall
if [ -z "$(grep 'ssh http.*home_net' /etc/firehol/firehol.conf)" ]
then 
    sed -i 's/.*ssh.*home_net.*/        server4 "ssh http" accept src ${home_net}/' /etc/firehol/firehol.conf
    service firehol restart
fi

#--------------------------------------------------------------------
# We are done
exit 0
