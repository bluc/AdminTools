#!/bin/bash
# Version 20230306-163641 checked into repository
################################################################
# (c) Copyright 2017 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/UpdateBlocklists-Mikrotik.sh

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=${0##*/}

dpkg-query -s firehol &> /dev/null
if [ $? -ne 0 ]
then
    echo "ERROR: 'firehol' is not installed"
    exit 1
fi

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE

# Global variables
THISHOST=$(hostname)
[[ $THISHOST == *.* ]] || THISHOST=$(hostname -f)

# The level of blocking
# 1 - minimum
# 2 - medium
# 3 - all
BLK_LEVEL=1

# The list of countries
COUNTRIES=''

# Force update
FORCE=0

TFILE=$(mktemp /tmp/$$XXXXXXXX)

# Get possible program options
while getopts C:L:f OPTION
do
    case ${OPTION} in
        C)  COUNTRIES=$(printf "%s" $OPTARG 2> /dev/null)
            COUNTRIES=${COUNTRIES,,}
            ;;
        L)  BLK_LEVEL=$(printf "%d" $OPTARG 2> /dev/null)
            [ $BLK_LEVEL -lt 1 ] && BLK_LEVEL=1
            [ $BLK_LEVEL -gt 3 ] && BLK_LEVEL=3
            ;;
        f)  FORCE=1
            ;;
        *)  echo "Usage: $0 [-L [1|2|3]] (default=$BLK_LEVEL) [-C c1[c2[,c3]]]"
            ;;
    esac
done
shift $((OPTIND - 1))

#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Get IP addresses and network for bad actors
if [ ! -s /tmp/AllBlacklistedHosts.txt ] || [ ! -s /tmp/AllBlacklistedNets.txt ]
then
    # Force and updatef of the blocklists
    bash UpdateBlocklists.sh -f -L $BLK_LEVEL
else
    bash UpdateBlocklists.sh -L $BLK_LEVEL
fi

echo '/ip firewall address-list' > $TFILE
awk '{printf "add address=%s comment=\"My Blocklist\" list=mtblocklist\n", $1}' /tmp/AllBlacklistedHosts.txt >> $TFILE
if [ -x /usr/bin/aggregate ]
then
     # Shorten the list of blocked networks
     aggregate < /tmp/AllBlacklistedNets.txt > /tmp/$$.AllBlacklistedNets.txt
     awk '{printf "add address=%s comment=\"My Blocklist\" list=mtblocklist\n", $1}' /tmp/$$.AllBlacklistedNets.txt >> $TFILE
else
     awk '{printf "add address=%s comment=\"My Blocklist\" list=mtblocklist\n", $1}' /tmp/AllBlacklistedNets.txt >> $TFILE
fi

#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Get IP addresses and network for specified countries
if [ ! -z "$COUNTRIES" ]
then
    for cty in ${COUNTRIES//,/ }
    do
        [ -s /var/tmp/countrydb/$cty.txt ] || continue
        awk '{printf "add address= %s comment=\"My Blocklist\" list=mtblocklist\n", $1}' /var/tmp/countrydb/$cty.txt >> $TFILE
    done
fi
if [ -s $TFILE ]
then
    mv $TFILE /tmp/Mikrotik-blocklist.rsc
    cat <<EOT
Upload the file /tmp/Mikrotik-blocklist.rsc to the Mikrotik firewall.
Then execute these comands on the command line of the firewall:
/ip firewall address-list remove [find where comment="My Blocklist"]
/import file-name=Mikrotik-blocklist.rsc
/ip firewall filter add chain=forward action=drop protocol=tcp dst-address-list=mtblocklist log=yes log-prefix="reject-by-blocklist"
}
EOT
fi
    
# We are done
exit 0
