#!/bin/bash
# Version 20240626-133515 checked into repository
################################################################
# (c) Copyright 2013 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/LiSysCo.sh

#-------------------------------------------------------------------------
# Globals
#-------------------------------------------------------------------------
# This must run as "root"
[ "T$(/usr/bin/id -u)" = 'T0' ] || exit

PROG=${0##*/}

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
DEBUG=''
[[ $- ==  *x* ]] && DEBUG='-v'

# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE
logger -p info -t ${PROG}[$$] -- Starting

THISHOST=$(hostname)
[[ $THISHOST == *.* ]] || THISHOST=$(hostname -f)
THISDOMAIN=${THISHOST#*.}
TH_SHORT=${THISHOST%%.*}

# Define all important system configs
# -> Can be overwritten/expanded in "filelist" (see below)
FILELIST='/etc/firehol/firehol.conf /etc/host* /etc/rc.local /etc/cron.d/* /etc/net* /etc/ssh/* /boot'
for SUB in /usr/local/*
do
    [[ $SUB =~ LiSysCo ]] && continue
    [[ $SUB =~ lost+found ]] && continue
    FILELIST="$FILELIST $SUB"
done
[ -d /etc/stunnel ] && FILELIST="$FILELIST /etc/stunnel/*"
[ -d /etc/apt ] && FILELIST="$FILELIST /etc/apt"

# RSYNC share to upload backup to
# -> If empty, no upload happens
# -> Can be set in "filelist" (see below)
RSYNC_SHARE=''
RSYNC2_SHARE=''

# Days when rsync backups should run
# -> If empty, no upload happens
# -> Can be set in "filelist" (see below)
# -> Uses "date +%w" notation:
#  0 - Sunday, 1 - Monday ... 6 - Saturday
# -> Default is every day
RSYNC_DAYS='0 1 2 3 4 5 6'
RSYNC2_DAYS='0 1 2 3 4 5 6'

# To run the rsync upload through a SSH tunnel,
#  assign values to ALL FOUR variables below
# -> Should really be done in "filelist" (see below)
# -> After being set, run this script by hand once
SSH_SERVER=''   # The SSH server
REAL_SERVER=''  # The real server behind the SSH server
SSH_USERID=''   # The userid on the SSH server
SSH_PASSWD=''   # The password for the SSH server
SSH_PORT='22'	# The port for ssh, typically 22
SSH2_SERVER=''   # The SSH server
REAL2_SERVER=''  # The real server behind the SSH server
SSH2_USERID=''   # The userid on the SSH server
SSH2_PASSWD=''   # The password for the SSH server
SSH2_PORT='22'	# The port for ssh, typically 22

# The list of exclude patterns
TAR_EXCLUDE='--exclude=/usr/local/LiSysCo/*'

# The max. bandwidth
BWLIMIT=$((6 * 1024)) # 48 Mbit/sec

# The list of file extensions which shouldn't be compressed
SKIP_COMPRESS=3g2/3gp/3gpp/3mf/7z/aac/ace/amr/apk/appx/appxbundle/arc/arj/asf/avi/br/bz2/cab/crypt5/crypt7/crypt8/deb/dmg/drc/ear/gz/flac/flv/gpg/h264/h265/heif/iso/jar/jp2/jpg/jpeg/lz/lz4/lzma/lzo/m4a/m4p/m4v/mkv/msi/mov/mp3/mp4/mpeg/mpg/mpv/oga/ogg/ogv/opus/pack/png/qt/rar/rpm/rzip/s7z/sfx/svgz/tbz/tgz/tlz/txz/vob/vma/webm/webp/wim/wma/wmv/xz/z/zip/zst

# Get possible program options
VERBOSE=0
REVERSE=0
while getopts rv OPTION
do
    case ${OPTION} in
        r)  REVERSE=1
            ;;
        v)  VERBOSE=1
            ;;
        *)  echo "Usage: $0 [options]"
            echo "         -v              Show progress messages"
            echo "         -r              Restore files"
            echo "         Example: -R 'rhost::upload'"
            ;;
    esac
done
shift $((OPTIND - 1))

# Allow local hosts to overwrite or expand the FILELIST
if [ -s /usr/local/etc/LiSysCo.filelist ]
then
    logger -p info -t ${PROG}[$$] -- Reading configuration file
    source /usr/local/etc/LiSysCo.filelist
else
    logger -p info -t ${PROG}[$$] -- Creating configuration file
    cat << EOT > /usr/local/etc/LiSysCo.filelist
# Allow local hosts to overwrite or expand the FILELIST
# Examples:

#EX## Get nginx confs if present
#EX#[ -d /etc/nginx ] && FILELIST="\$FILELIST /etc/nginx/*"

#EX## Get dovecot confs if present
#EX#[ -d /etc/dovecot ] && FILELIST="\$FILELIST /etc/dovecot/*"

#EX## Get postfix confs if present
#EX#[ -d /etc/postfix ] && FILELIST="\$FILELIST /etc/postfix/*"

# The list of exclude patterns
#EX#TAR_EXCLUDE="\$TAR_EXCLUDE --exclude='*logs'"

# Define the rsync fileshare (overwrites the command line option)
# Format "host::share"
# Can use these variables:
# THISHOST, THISDOMAIN, TH_SHORT
#RSYNC_SHARE=host::share
#RSYNC2_SHARE=host::share

# Days when rsync backups should run
# -> If empty, no upload happens
# -> Can be set in "filelist" (see below)
# -> Uses "date +%w" notation:
#  0 - Sunday, 1 - Monday ... 6 - Saturday
# -> Default is every day
#RSYNC_DAYS='0 1 2 3 4 5 6'
#RSYNC2_DAYS='0 1 2 3 4 5 6'

# To run the rsync upload through a SSH tunnel,
#  assign values to ALL FOUR variables below
# -> After being set, run this script by hand once
#SSH_SERVER='1.2.3.4'           # The SSH server
#REAL_SERVER='127.0.0.1'        # The real server behind the SSH server
#SSH_USERID='joe'               # The userid on the SSH server
#SSH_PASSWD='J03_pw'            # The password for the SSH server
#SSH_PORT='22'			# The port for ssh, typically 22
#SSH2_SERVER='1.2.3.4'           # The SSH server
#REAL2_SERVER='127.0.0.1'        # The real server behind the SSH server
#SSH2_USERID='joe'               # The userid on the SSH server
#SSH2_PASSWD='J03_pw'            # The password for the SSH server
#SSH2_PORT='22'			# The port for ssh, typically 22
EOT
fi

# Create the directory for the system config backup
LSC=/usr/local/LiSysCo
mkdir -p $LSC

# Use "nocache" if possible
NOCACHE=''
dpkg-query -S nocache &> /dev/null && NOCACHE='nocache'

source /usr/local/include/loadcheck.sm

#-----------------------------------------------------------
function do_tar() {
    local TAR_FILE="$1"
    local TAR_RC=1
    local COMPRESS

    # Archive with plain tar (and keep the CPU load in check)
    nice $NOCACHE tar --create --preserve-permissions --exclude-vcs \
        --absolute-names $TARTOTALS --ignore-failed-read \
        $TAR_EXCLUDE --warning=no-file-ignored \
        --file $TAR_FILE $FILELIST /tmp/Installed.Packages.txt &
    ThrottleProcess $!

    # We are done if we couldn't create an archive
    [ -s $TAR_FILE ] || return 1

    # Use multi-core bzip2 if possible
    if [ $LC_MAXLOAD -gt 2 ] && [ -x /usr/bin/pbzip2 ]
    then
        COMPRESS="pbzip2 -f -5 -p$LC_MAXLOAD"
    else
        COMPRESS='bzip2 -f -5'
    fi
    nice $NOCACHE $COMPRESS $TAR_FILE &
    ThrottleProcess $!

    if [ -s $TAR_FILE.bz2 ]
    then
        return 0
    else
        return 1
    fi
}

#-----------------------------------------------------------
function do_rsync() {
    local LOCAL_KEY_FILE
    local RSYNC_PORT=873
    local SSHPID=''
    local TRY=0
    local RETCODE=0
    local ST=0
    local LOCAL_KEY_FILE=''

    if [[ $RSYNC_DAYS =~ $(date +%w) ]]
    then
        # Only upload files if we have at least one
        logger -p info -t ${PROG}[$$] -- Uploading system backup

        if [ -n "$ZIMBRA_BACKUPS" ] || [ -n "$ZFS_BACKUPS" ] || [ -n "$LISYSCO_BACKUPS" ]
        then
            # Upload the backup file(s)
            if [ -z "$DEBUG" ]
            then
                ST=$((RANDOM % 1800))
                [ $ST -lt 600 ] && ST=$((ST + 600))
                sleep $ST
            fi

            if [ -n "$SSH_SERVER" ] && [ -n "$REAL_SERVER" ] && [ -n "$SSH_USERID" ] && [ -n "$SSH_PASSWD" ]
            then
                # Create the local SSH key
                mkdir -p $HOME/.ssh
                LOCAL_KEY_FILE=$HOME/.ssh/rsync_id
                if [ ! -s $LOCAL_KEY_FILE ]
                then
                    ssh-keygen -q -t rsa -b 2048 -f $LOCAL_KEY_FILE -N ''
                    cat << EOT
Run this command as "root" now:
ssh-copy-id -p $SSH_PORT -i $LOCAL_KEY_FILE "$SSH_USERID@$SSH_SERVER"
EOT
                    exit 1
                fi

                # Set up SSH tunnel
                RSYNC_PORT=2873
                if [ -s $LOCAL_KEY_FILE ]
                then
                    logger -p info -t ${PROG}[$$] -- Setting up SSH tunnel
                    ssh $DEBUG -4 -N -p $SSH_PORT -i $LOCAL_KEY_FILE "$SSH_USERID@$SSH_SERVER" -L $RSYNC_PORT:127.0.0.1:873 &
                else
                    date "+ERROR: %F %T No way to set up SSH tunnel"
                    exit 1
                fi
                sleep 10
                SSHPID=$(ps -wefH  | awk "/ss[h].*$SSH_SERVER/"' {print $2}')
            else
                # no SSH tunnel
                SSHPID=''
                RSYNC_PORT=873
            fi

            TRY=0
            while true
            do
                # Add 30 seconds per try to the timeout
                logger -p info -t ${PROG}[$$] -- Try $TRY to upload system backup
                nice $NOCACHE rsync -v --timeout=$((180 + TRY * 30)) -rlptD --ipv4 \
                    --port $RSYNC_PORT --bwlimit=${BWLIMIT} \
                    --skip-compress=${SKIP_COMPRESS} \
                    $LISYSCO_BACKUPS $ZIMBRA_BACKUPS $ZFS_BACKUPS $RSYNC_SHARE > /tmp/$$.rsync.out
                RETCODE=$?
                [ $RETCODE -eq 0 ] && break

                # We had some kind of error -> show the output from rsync
                if [ -s /tmp/$$.rsync.out ]
                then
                    echo "Upload try $TRY failed with code $RETCODE - see log below:"
                    cat /tmp/$$.rsync.out
                fi

                # Try again - up to 3 times
                TRY=$((TRY + 1))
                [ $TRY -gt 2 ] && break
            done

            # Tear down the SSH tunnel (if present)
            if [ -n "$SSHPID" ]
            then
                logger -p info -t ${PROG}[$$] -- Tearing down SSH tunnel
                kill $SSHPID
            fi
        fi
    fi
}

#-----------------------------------------------------------
function do_rsync2() {
    local LOCAL_KEY_FILE
    local RSYNC_PORT=873
    local TRY=0
    local RETCODE=0
    local SSHPID=''
    local TRY=0
    local RETCODE=0
    local ST=0
    local LOCAL_KEY_FILE=''

    if [[ $RSYNC2_DAYS =~ $(date +%w) ]]
    then
        # Only upload files if we have at least one
        logger -p info -t ${PROG}[$$] -- Uploading system backup

        if [ -n "$ZIMBRA_BACKUPS" ] || [ -n "$ZFS_BACKUPS" ] || [ -n "$LISYSCO_BACKUPS" ]
        then
            # Upload the backup file(s)
            if [ -z "$DEBUG" ]
            then
                ST=$((RANDOM % 1800))
                [ $ST -lt 600 ] && ST=$((ST + 600))
                sleep $ST
            fi

            if [ -n "$SSH2_SERVER" ] && [ -n "$REAL2_SERVER" ] && [ -n "$SSH2_USERID" ] && [ -n "$SSH2_PASSWD" ]
            then
                # Create the local SSH key
                mkdir -p $HOME/.ssh
                LOCAL_KEY_FILE=$HOME/.ssh/rsync_id
                if [ ! -s $LOCAL_KEY_FILE ]
                then
                    ssh-keygen -q -t rsa -b 2048 -f $LOCAL_KEY_FILE -N ''
                    cat << EOT
Run this command as "root" now:
ssh-copy-id -p $SSH2_PORT -i $LOCAL_KEY_FILE "$SSH2_USERID@$SSH2_SERVER"
EOT
                    exit 1
                fi

                # Set up SSH tunnel
                RSYNC_PORT=2873
                if [ -s $LOCAL_KEY_FILE ]
                then
                    logger -p info -t ${PROG}[$$] -- Setting up SSH tunnel
                    ssh $DEBUG -4 -N -p $SSH2_PORT -i $LOCAL_KEY_FILE "$SSH2_USERID@$SSH2_SERVER" -L $RSYNC_PORT:127.0.0.1:873 &
                else
                    date "+ERROR: %F %T No way to set up SSH tunnel"
                    exit 1
                fi
                sleep 10
                SSHPID=$(ps -wefH  | awk "/ss[h].*$SSH2_SERVER/"' {print $2}')
            else
                # no SSH tunnel
                SSHPID=''
                RSYNC_PORT=873
            fi

            TRY=0
            while [ true ]
            do
                # Add 30 seconds per try to the timeout
                logger -p info -t ${PROG}[$$] -- Try $TRY to upload system backup
                nice $NOCACHE rsync $DEBUG --timeout=$((180 + TRY * 30)) -rlptD --ipv4 \
                    --port $RSYNC_PORT --bwlimit=${BWLIMIT} \
                    --skip-compress=${SKIP_COMPRESS} \
                    $LISYSCO_BACKUPS $ZIMBRA_BACKUPS $ZFS_BACKUPS $RSYNC2_SHARE
                RETCODE=$?
                logger -p info -t ${PROG}[$$] -- rsync finished with code $RETCODE
                [ $RETCODE -eq 0 ] && break
                # Try up to 3 times
                TRY=$((TRY + 1))
                [ $TRY -gt 2 ] && break
            done

            # Tear down the SSH tunnel (if present)
            if [ -n "$SSHPID" ]
            then
                logger -p info -t ${PROG}[$$] -- Tearing down SSH tunnel
                kill $SSHPID
            fi
        fi
    fi
}

if [ $REVERSE -eq 0 ]
then
    #-----------------------------------------------------------
    # Create the system config backup
    #-----------------------------------------------------------
    logger -p info -t ${PROG}[$$] -- Creating system backup

    # Get a list of installed packages
    dpkg --get-selections > /tmp/Installed.Packages.txt

    # Detect backups created by zos-backup.sh
    ZBL=''
    ZIMBRA_BACKUPS=''
    ZFS_BACKUPS=''
    if [ -s /backup/zos-backup.log ]
    then
       ZBL='/backup/zos-backup.log'
    elif [ -s /opt/zimbra/backup/zos-backup.log ]
    then
       ZBL='/opt/zimbra/backup/zos-backup.log'
    fi
    if [ -n "$ZBL" ]
    then
        # Also capture the zimbra and ZFS backup files,
        #  but wait for the Zimbra backups to finish
        while [[ ! $(tail -n 1 $ZBL) =~ done\  ]]
        do
            sleep 20
        done
        logger -p info -t ${PROG}[$$] -- Also capture possible Zimbra and ZFS backups
        [ -n "$(ls -1 \"${ZBL%/*}\"'/*t[ag][zr]')" ] && ZIMBRA_BACKUPS="${ZBL%/*}"'/*t[ag][zr]'
        [ -n "$(ls -1 \"${ZBL%/*}\"'/*zfs_*')" ] && ZFS_BACKUPS="${ZBL%/*}"'/*zfs_*'
    fi

    # Get a disk layout report to capture as well
    if [ -x /sbin/lvdisplay ]
    then
        echo >> /tmp/DiskLayoutReport.txt
        echo "=> LVM2 volumes:" >> /tmp/DiskLayoutReport.txt
        lvdisplay >> /tmp/DiskLayoutReport.txt
        vgdisplay >> /tmp/DiskLayoutReport.txt
        pvdisplay >> /tmp/DiskLayoutReport.txt
    fi
    if [ -s /proc/mdstat ]
    then
        echo >> /tmp/DiskLayoutReport.txt
        echo "=> Softraid volumes:" >> /tmp/DiskLayoutReport.txt
        grep -A 1 md /proc/mdstat >> /tmp/DiskLayoutReport.txt
    fi
    if [ -x /usr/sbin/zfs ]
    then
        if [[ ! $(zfs list 2>&1) =~ 'no datasets available' ]]
        then
            echo >> /tmp/DiskLayoutReport.txt
            echo "=> ZFS filesystems:" >> /tmp/DiskLayoutReport.txt
            /usr/sbin/zfs list -t filesystem >> /tmp/DiskLayoutReport.txt 2> /dev/null
            echo >> /tmp/DiskLayoutReport.txt
            echo "=> ZFS snapshots:" >> /tmp/DiskLayoutReport.txt
            /usr/sbin/zfs list -t snapshot | grep -v '0B' >> /tmp/DiskLayoutReport.txt 2> /dev/null
        fi
    fi
    echo >> /tmp/DiskLayoutReport.txt
    echo "=> Disks:" >> /tmp/DiskLayoutReport.txt
    fdisk -l 2>/dev/null | grep '^Disk /' >> /tmp/DiskLayoutReport.txt
    if [ -x /opt/MegaRAID/MegaCli/MegaCli64 ]
    then
        echo >> /tmp/DiskLayoutReport.txt
        echo "=> LSI RAID volumes:" >> /tmp/DiskLayoutReport.txt
        /opt/MegaRAID/MegaCli/MegaCli64 -LdPdInfo -aALL -NoLog | \
            grep -Ea '(Virtual Drive|Error Count|Speed:|^(Adapter|RAID Level|Size|Number Of Drives|PD|Coerced Size|Inquiry Data))' | \
            awk '/^PD:|Adapter|Virtual/{print "=> "$0;next}{print "  "$0}' >> /tmp/DiskLayoutReport.txt 2> /dev/null
    fi
    if [ -x /usr/local/sbin/areca_cli64 ]
    then
        echo >> /tmp/DiskLayoutReport.txt
        echo "=> ARECA RAID information:" >> /tmp/DiskLayoutReport.txt
        /usr/local/sbin/areca_cli64 rsf info >> /tmp/DiskLayoutReport.txt 2> /dev/null
        /usr/local/sbin/areca_cli64 vsf info >> /tmp/DiskLayoutReport.txt 2> /dev/null
        /usr/local/sbin/areca_cli64 disk info >> /tmp/DiskLayoutReport.txt 2> /dev/null
    fi
    [ -s /tmp/DiskLayoutReport.txt ] && FILELIST="$FILELIST /tmp/DiskLayoutReport.txt"

    # Save all relevant system configs
    rm -f $LSC/*tar.bz2
    cd /
    TARTOTALS=''
    if [ $VERBOSE -ne 0 ]
    then
        [ $VERBOSE -ne 0 ] && date '+ -> %F %T Saving important system configuration'
        logger -p info -t ${PROG}[$$] -- Saving important system configuration
        TARTOTALS='--totals'
    fi

    do_tar $LSC/${THISHOST}.LiSysCo.tar
    TAR_RC=$?
    if [ $TAR_RC -eq 0 ] || [ -s $LSC/${THISHOST}.LiSysCo.tar.bz2 ]
    then
        LISYSCO_BACKUPS="$LSC/*"
    else
        LISYSCO_BACKUPS=''
    fi

    if [ -n "$RSYNC_SHARE" ] && [ -n "$RSYNC_DAYS" ]
    then
        do_rsync &
    fi
    if [ -n "$RSYNC2_SHARE" ] && [ -n "$RSYNC2_DAYS" ]
    then
        do_rsync2 &
    fi
    wait
else
    #-----------------------------------------------------------
    # Restore the system config backup
    #-----------------------------------------------------------
    logger -p info -t ${PROG}[$$] -- Restoring system backup

    read -rp "Name of host to restore [default=$THISHOST] ? "
    [ -z "$REPLY" ] || THISHOST=$REPLY
    if [ ! -f $LSC/${THISHOST}.LiSysCo.tar.bz2 ]
    then
        date "+ERROR: %F %T System config backup '$LSC/${THISHOST}.LiSysCo.tar.bz2' does not exist"
        exit 1
    fi

    cd /
    TARTOTALS=''
    if [ $VERBOSE -ne 0 ]
    then
        date '+ -> %F %T  Restoring important configuration infos'
        TARTOTALS='--totals'
    fi
    logger -p info -t ${PROG}[$$] -- Restoring system backup
    nice $NOCACHE tar --extract --preserve-permissions --bzip2 --absolute-names $TARTOTALS \
        --file $LSC/${THISHOST}.LiSysCo.tar.bz2
    RETCODE=$?
    if [ $RETCODE -ne 0 ]
    then
        date "+ERROR: %F %T System config restoration failed: $RETCODE"
        exit 1
    fi
fi

[ $VERBOSE -ne 0 ] && date "+ -> %F %T Ending with exit code $RETCODE"
logger -p info -t ${PROG}[$$] -- Done
exit $RETCODE
