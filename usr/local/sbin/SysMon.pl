#!/usr/bin/perl -w
# Version 20241201-211322 checked into repository
#--------------------------------------------------------------------
# (c) CopyRight 2014-2022 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#--------------------------------------------------------------------
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/SysMon.pl
# Recommended installation procedure:
#INSTALL#wget -O /tmp/SysMon.install.sh https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/SysMon.install.sh
#INSTALL#bash /tmp/SysMon.install.sh
use 5.010;

# Constants
my $ProgName = '';
( $ProgName = $0 ) =~ s%.*/%%;
my $ProgVers = 'unknown';
if ( open( my $GPV, "$0" ) )
{
    while (<$GPV>)
    {
        if (/# Version ([\d\-]*) checked/o)
        {
            $ProgVers = "$1";
            chomp($ProgVers);
            last;
        } ## end if (/# Version ([\d\-]*) checked/o...)
    } ## end while (<$GPV>)
    close($GPV);
} ## end if ( open( my $GPV, "$0"...))
my $CopyRight = '(c) CopyRight 2014-2022 B-LUC Consulting Thomas Bullinger';

# Common sense options:
use strict qw(vars subs);
no warnings;
use warnings qw(FATAL closed threads internal debugging pack
    portable prototype inplace io pipe unpack malloc
    glob digit printf layer reserved taint closure
    semicolon);
no warnings qw(exec newline unopened);

#--------------------------------------------------------------------
# Needed packages
#--------------------------------------------------------------------
use Sys::Syslog qw(:macros :standard);
use Sys::Hostname;
use Net::SMTP;
use POSIX qw(setsid strftime);
use Digest::MD5;
use Fcntl qw < O_CREAT O_RDWR >;
use SDBM_File;
use Getopt::Std;

#--------------------------------------------------------------------
# Globals
#--------------------------------------------------------------------
local $ENV{PATH} = '/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin';

# Host to monitor
my $MHOST = hostname;
unless ( $MHOST =~ /\./ )
{
    if ( open( my $HOSTNAME, '-|', 'hostname -f' ) )
    {
        $MHOST = <$HOSTNAME>;
        close($HOSTNAME);
        chomp($MHOST);
    } ## end if ( open( my $HOSTNAME...))
} ## end unless ( $MHOST =~ /\./ )

# Program options
our $opt_a = '';
our $opt_d = 0;
our $opt_D = 0;
our $opt_h = 0;
our $opt_o = '';
our $opt_Q = '';
our $opt_m = 'localhost';
our $opt_s = '/var/log/SysMon.log';         # or 'syslog'
our $opt_P = '/usr/local/SysMon_Plugins';
our $opt_v = 0;

# These are in seconds:
our $opt_i = 5;
our $opt_I = 3 * 60;

# External programs
my $VMSTAT      = '/usr/bin/vmstat';
my $HaveCurl    = ( ( -x '/usr/bin/curl' ) ? 1 : 0 );
my $HaveTimeout = ( ( -x '/usr/bin/timeout' ) ? 1 : 0 );

# Other internal globals
my $CPUNo                   = 0;
my $TotalRAM                = 0;
my %md5_Sum                 = ();
my $DefaultGateway_MAC_Prev = '';
my $LogTime                 = '';
my $LogFile                 = '';

# The running tallies from "vmstat"
# 1. for the standard logging interval
my $VMLNo_Interval       = 0;
my $RunQueue_Interval    = 0;
my $BlockedP_Interval    = 0;
my $PageIn_Interval      = 0;
my $CPUWait_Interval     = 0;
my %TXPrevBytes_Interval = ();
my %RXPrevBytes_Interval = ();
my %ReadBytes_Interval   = ();
my %WriteBytes_Interval  = ();
# 2. for the hourly report
my $VMLNo_Hourly           = 0;
my $RunQueue_Hourly        = 0;
my $BlockedP_Hourly        = 0;
my $PageIn_Hourly          = 0;
my $CPUWait_Hourly         = 0;
my %TXPrevBytes_Hourly     = ();
my %RXPrevBytes_Hourly     = ();
my %ReadBytes_Hourly       = ();
my %WriteBytes_Hourly      = ();
my %DiskReallocated_Hourly = ();

# The footer for the report
my $ReportTail_CPU = '';
my $ReportTail_RAM = '';

# 'Quiet' hours
my @QuietHours = ();

# NFS Timeout values
my %NFSTimeouts = ();

my $LogFile = '';

#--------------------------------------------------------------------
# Become a daemon process
#--------------------------------------------------------------------
sub Daemonize()
{

    # pretty command line in ps
    local $0 = join( ' ', $0, @ARGV ) unless ($opt_d);

    chdir '/' or die "Can't chdir to '/': $!";

    # Redirect STDIN and STDOUT
    open( STDIN,  '<', '/dev/null' ) or die "Can't read '/dev/null': $!";
    open( STDOUT, '>', '/dev/null' ) or die "Can't write '/dev/null': $!";
    defined( my $pid = fork ) or die "Can't fork: $!";

    if ($pid)
    {

        # The parent can die now
        print 'DEBUG: Parent dies' if ($opt_d);
        exit;
    } ## end if ($pid)

    setsid or die "Can't start new session: $!";
    open STDERR, '>&STDOUT' or die "Can't duplicate stdout: $!";

    return (0);
} ## end sub Daemonize

#--------------------------------------------------------------------
# Commify a number
# See: http://www.perlmonks.org/index.pl?node_id=435729
#--------------------------------------------------------------------
sub commify
{
    my ( $sign, $int, $frac ) = ( $_[0] =~ /^([+-]?)(\d*)(.*)/o );
    my $commified
        = ( reverse scalar join ',', unpack '(A3)*', scalar reverse $int );
    return join( '', $sign, $commified, $frac );
} ## end sub commify

#--------------------------------------------------------------------
# Log a message
# See: http://www.perlmonks.org/index.pl?node_id=435729
#--------------------------------------------------------------------
sub mylog ($)
{
    my ($Report) = @_;

    if ( open my $LF, '>>', "$LogFile" )
    {
        print $LF "$Report\n";
        close($LF);
    } else
    {
        warn "Can't open $LogFile: $! $@";
    } ## end else [ if ( open my $LF, '>>'...)]
} ## end sub mylog ($)

#-------------------------------------------------------------------------
# Send an alert email (simple SMTP client)
#-------------------------------------------------------------------------
sub SendEmail ($$)
{
    my ( $MsgText, $Importance ) = @_;

    my $Recipient = "$opt_a";
    $Recipient =~ s/@/\\@/g;
    warn "DBG: Sending email '$MsgText' to '$Recipient'\n" if ($opt_d);
    $LogTime = strftime( "%F %T", localtime );

    if ( scalar(@QuietHours) )
    {
        my ( undef, undef, $CurHour ) = localtime;
        # See http://www.perlmonks.org/?node_id=2482
        if ( exists { map { $_ => 1 } @QuietHours }->{$CurHour} )
        {
            # No email during 'quiet' hours
            if ( $opt_s eq 'syslog' )
            {
                syslog 5,
                    "info %s Email sending supressed during quiet hour '%d'",
                    $MHOST, $CurHour;
            } else
            {
                mylog(
                    join( '',
                        "$LogTime $ProgName",
                        "[$$] INFO: Email sending supressed during quiet hour '$CurHour'"
                    )
                );
            } ## end else [ if ( $opt_s eq 'syslog'...)]

            return;
        } ## end if ( exists { map { $_...}})
    } ## end if ( scalar(@QuietHours...))

    # Define the importance/priority headers
    my %ImportanceHeader = (
        'Importance'        => 'normal',
        'X-MSMail-Priority' => 'normal',
        'X-Priority'        => '3'
    );
    if ( $Importance eq 'high' )
    {
        $ImportanceHeader{'Importance'}        = 'high';
        $ImportanceHeader{'X-MSMail-Priority'} = 'high';
        $ImportanceHeader{'X-Priority'}        = '1';
    } elsif ( $Importance eq 'low' )
    {
        $ImportanceHeader{'Importance'}        = 'low';
        $ImportanceHeader{'X-MSMail-Priority'} = 'low';
        $ImportanceHeader{'X-Priority'}        = '5';
    } ## end elsif ( $Importance eq 'low'...)

    my $Try = 0;
    while ( $Try < 3 )
    {
        my $smtp = Net::SMTP->new( "$opt_m", Debug => $opt_d );
        unless ( defined $smtp )
        {
            $Try++;
            next;
        } ## end unless ( defined $smtp )

        # The envelope
        my $res = $smtp->mail('root');
        unless ($res)
        {
            $Try++;
            next;
        } ## end unless ($res)
        $res = $smtp->to("$Recipient");
        unless ($res)
        {
            $Try++;
            next;
        } ## end unless ($res)

        # The real email
        $res = $smtp->data();
        unless ($res)
        {
            $Try++;
            next;
        } ## end unless ($res)

        # The email body
        my $Msg = join( '',
            "From: root <root\@$MHOST>\n",
            "To: $Recipient\n",
            "Subject: SysMon alert on $MHOST\n",
            "Date: ",
            strftime( "%a, %d %b %Y %H:%M:%S %z", localtime ),
            "\n",
            "Mime-Version: 1.0\n",
            "X-Mailer: $ProgName with PID $$ on '$MHOST'\n" );
        foreach my $EmailHeader ( keys %ImportanceHeader )
        {
            $Msg .= "$EmailHeader: $ImportanceHeader{$EmailHeader}\n";
        } ## end foreach my $EmailHeader ( keys...)
        $Msg
            .= "\nThe system monitoring script on the server '$MHOST' detected the following issue(s) of $ImportanceHeader{'Importance'} importance:\n\n$MsgText\n";

        $res = $smtp->datasend("$Msg");
        unless ($res)
        {
            $Try++;
            next;
        } ## end unless ($res)

        # End the SMTP session
        $res = $smtp->dataend();
        unless ($res)
        {
            $Try++;
            next;
        } ## end unless ($res)
        $res = $smtp->quit;
        unless ($res)
        {
            $Try++;
            next;
        } ## end unless ($res)
        last;
    } ## end while ( $Try < 3 )
    if ( $Try >= 3 )
    {
        # Could not open connection to mail server
        # (tried 3 times!)
        if ( $opt_s eq 'syslog' )
        {
            syslog 3, "critical %s Could not send email '%s'", $MHOST,
                $MsgText;
        } else
        {
            mylog(
                join( '',
                    "$LogTime $ProgName",
                    "[$$] CRITICAL: Could not send email '$MsgText'" )
            );
        } ## end else [ if ( $opt_s eq 'syslog'...)]
    } ## end if ( $Try >= 3 )

    return (0);
} ## end sub SendEmail ($$)

#--------------------------------------------------------------------
# Display the usage
#--------------------------------------------------------------------
sub ShowUsage()
{

    print "Usage: $ProgName [options]\n",
        "       -a address         Specify email address for alerts [default=none]\n",
        "       -o path            Specify path for '.csv' files [default=none]\n",
        "       -D                 Run as daemon [default=no]\n",
        "       -i seconds         Specify wait interval for vmstat in seconds [default=$opt_i]\n",
        "       -I seconds         Specify reporting interval in seconds [default=$opt_I]\n",
        "       -m IP|host         Specify mail server to use [default=$opt_m]\n",
        "       -s path            Log to logfile [default=$opt_s]\n",
        "       -Q hour[,hour...]  Specifiy 'quiet' hour(s) [default=none]\n",
        "       -P path            Specify path for plugins [default=$opt_P]\n",
        "       -h                 Show this help [default=no]\n",
        "       -v                 Be verbose [default=no]\n\n",
        "       -d                 Show some debug info on STDERR [default=no]\n\n",
        "       NOTES:\n",
        "        Alerts are disabled unless an email address is specified\n",
        "        Alerts are not sent during 'quiet' hours\n",
        "        Alerting requires an email listener running on the local host\n",
        "        Alerts are sent for any of these conditions:\n",
        "          CPU usage is high (error)\n",
        "          I/O usage is high (error)\n",
        "          Memory usage is high (error)\n",
        "          A partition usage exceeds an error threshold\n",
        "            90% for partitions that are at least 4TB in size\n",
        "            80% for partitions that are at least 1TB in size\n",
        "            75% for all other partitions\n",
        "          A partition usage exceeds a critical threshold\n",
        "            95% for partitions that are at least 4TB in size\n",
        "            85% for partitions that are at least 1TB in size\n",
        "            80% for all other partitions\n\n",
        "        '.csv' files are only written if the path is given\n",
        "        If the path does not exist, no '.csv' is not written\n",
        "        There will be individual '.csv' files for data:\n",
        "          CPU, I/O, Memory, Disk, Partition\n",
        "          MySQL (if running on the local host)\n";

    exit 0;
} ## end sub ShowUsage

#--------------------------------------------------------------------
# Get some MySQL stats
# See http://ronaldbradford.com/blog/are-you-monitoring-rss-vsz-2009-03-08/
#--------------------------------------------------------------------
sub GetMysqldStats()
{
    my $RSS      = 0;
    my $VSZ      = 0;
    my $TotalRam = 0;

    if (open(
            my $PSL,
            '-|',
            ( ($HaveTimeout) ? 'timeout 60s ' : '' )
                . 'ps -A --sort -rss -o rss,vsz,args'
        )
        )
    {
        my @lines = ();
        chomp( @lines = grep {/mysqld|mariadb/} <$PSL> );
        close($PSL);
        foreach (@lines)
        {

            if (/^(\d+)\s+(\d+).+(?:mysqld|mariadb)/o)
            {
                $RSS = "$1";
                $VSZ = "$2";

                last;
            } ## end if (/^(\d+)\s+(\d+).+(?:mysqld|mariadb)/o...)
        } ## end foreach (@lines)

        # Get the total memory on this server
        if ( open( my $MEM, '<', '/proc/meminfo' ) )
        {
            my @lines = ();
            chomp( @lines = grep {/MemTotal:/} <$MEM> );
            close($MEM);
            foreach (@lines)
            {
                if (/^MemTotal:\s+(\d+)/o)
                {
                    $TotalRam = $1;
                    last;
                } ## end if (/^MemTotal:\s+(\d+)/o...)
            } ## end foreach (@lines)
        } ## end if ( open( my $MEM, '<'...))

        if ($TotalRam)
        {
            # Calculate the percentage of total RAM
            my $RSS_percent = $RSS * 100 / $TotalRam;
            my $VSZ_percent = $VSZ * 100 / $TotalRam;
            return
                sprintf
                "Mysql resident memory is %s KB (%.f%%), virtual memory is %s KB (%.2f%%) - LS_MM=%0.2f=%0.2f=mysql",
                commify($RSS), $RSS_percent, commify($VSZ), $VSZ_percent,
                $RSS,
                $VSZ;
        } else
        {
            # Show the absolute numbers
            return
                sprintf
                "Mysql resident memory = %s KB, virtual memory = %s KB",
                commify($RSS), commify($VSZ);
        } ## end else [ if ($TotalRam) ]
    } ## end if ( open( my $PSL, '-|'...))
} ## end sub GetMysqldStats

#--------------------------------------------------------------------
# Log the report per specified interval
#--------------------------------------------------------------------
sub ReportInterval()
{
    my $Now = strftime( "%F,%T", localtime );
    $LogTime = strftime( "%F %T", localtime );

    my @Files2Check = (
        '/etc/passwd',          '/etc/shadow',
        '/bin/false',           '/bin/true',
        '/etc/ssh/sshd_config', '/etc/ssh/ssh_config',
        '/root/.bashrc',        '/root/.profile'
    );
    push( @Files2Check, '/etc/firehol/firehol.conf' )
        if ( -x '/usr/sbin/firehol' );
    my $ArraySize = @Files2Check;
    for ( my $i = 0; $i < $ArraySize; $i++ )
    {
        my $FName = $Files2Check[$i];
        unless ( -s "$FName" )
        {
            my $Report = "'$FName' doesn't exist on this server";
            if ( $opt_s eq 'syslog' )
            {
                syslog 3, "error %s", $Report;
            } else
            {
                mylog(
                    join( '', "$LogTime $ProgName", "[$$] ERROR: $Report" ) );
            } ## end else [ if ( $opt_s eq 'syslog'...)]
            next;
        } ## end unless ( -s "$FName" )

        if ( open( my $fh, '<', $FName ) )
        {
            binmode($fh);
            my $md5  = Digest::MD5->new;
            my $FSum = $md5->addfile($fh)->hexdigest;
            close($fh);

            my $Report = sprintf "MD5 checksum for %s is %s", $FName, $FSum;
            if ($opt_o)
            {
                # Also write data into corresponding '.csv' file
                my ($outfile) = "$opt_o/Checksum.csv" =~ /^([^\0]+)$/;
                if ( open( my $CSV, '>>', $outfile ) )
                {
                    printf $CSV "%s,%s,%s\n", $Now, $FName, $FSum;
                    close($CSV);
                } ## end if ( open( my $CSV, '>>'...))
            } ## end if ($opt_o)

            if ( exists $md5_Sum{"$FName"} )
            {
                unless ( $md5_Sum{"$FName"} =~ /^$FSum$/ )
                {
                    $Report = join( '',
                        "Changed MD5 checksum for $FName: ",
                        $md5_Sum{"$FName"}, " => $FSum" );
                    if ( $opt_s eq 'syslog' )
                    {
                        syslog 3, "error %s", $Report;
                    } else
                    {
                        mylog(
                            join( '',
                                "$LogTime $ProgName",
                                "[$$] ERROR: $Report" )
                        );
                    } ## end else [ if ( $opt_s eq 'syslog'...)]
                    if ($opt_a)
                    {
                        # Also send an email alert
                        SendEmail( "ERROR: $Report", 'normal' );
                    } ## end if ($opt_a)
                } ## end unless ( $md5_Sum{"$FName"...})
            } ## end if ( exists $md5_Sum{"$FName"...})
            $md5_Sum{"$FName"} = "$FSum";
        } else
        {
            my $Report = "Can't open '$FName': $!";
            warn "$Report";
            if ( $opt_s eq 'syslog' )
            {
                syslog 3, "error %s", $Report;
            } else
            {
                mylog(
                    join( '', "$LogTime $ProgName", "[$$] ERROR: $Report" ) );
            } ## end else [ if ( $opt_s eq 'syslog'...)]
        } ## end else [ if ( open( my $fh, '<'...))]
    } ## end for ( my $i = 0; $i < $ArraySize...)

    # Write a syslog entry with the correct severity
    my $Avg = $RunQueue_Interval / ( $VMLNo_Interval - 3 );
    my $Report
        = sprintf
        "Running process average (CPU usage) is %0.2f %s - LS_CPU=%0.2f=-=cpu",
        $Avg,
        $ReportTail_CPU, $Avg;
    if ($opt_o)
    {
        # Also write data into corresponding '.csv' file
        my ($outfile) = "$opt_o/CPU.csv" =~ /^([^\0]+)$/;
        if ( open( my $CSV, '>>', $outfile ) )
        {
            printf $CSV "%s,%0.2f\n", $Now, $Avg;
            close($CSV);
        } ## end if ( open( my $CSV, '>>'...))
    } ## end if ($opt_o)
    my $MaxCPUThreshold = ( $CPUNo + 1 ) * 2;
    if ( $Avg > $MaxCPUThreshold )
    {
        $Report =~ s/HHMMLL/high/;
        if ( $opt_s eq 'syslog' )
        {
            syslog 3, "error %s", $Report;
        } else
        {
            mylog( join( '', "$LogTime $ProgName", "[$$] ERROR: $Report" ) );
        } ## end else [ if ( $opt_s eq 'syslog'...)]
        if ($opt_a)
        {
            # Get the top 30 processes
            my $ProcList = '';
            if (open(
                    my $TopProc,
                    '-|',
                    ( ($HaveTimeout) ? 'timeout 60s ' : '' )
                        . 'ps -eo pid,ppid,euser,cmd,%mem,%cpu,lstart --sort=-%cpu'
                )
                )
            {
                my @lines = ();
                chomp( @lines = <$TopProc> );
                close($TopProc);
                foreach ( @lines[ 0 .. 30 ] )
                {
                    $ProcList .= "$_\n";
                } ## end foreach ( @lines[ 0 .. 30 ]...)
            } ## end if ( open( my $TopProc...))

            # Also send an email alert
            if ( length $ProcList )
            {
                SendEmail(
                    join( '',
                        "ERROR: $Report",
                        "\n\nTop 30 processes sorted by CPU usage:\n$ProcList"
                    ),
                    'normal'
                );
            } else
            {
                SendEmail( "ERROR: $Report", 'normal' );
            } ## end else [ if ( length $ProcList ...)]
        } ## end if ($opt_a)
    } elsif ( $Avg > $CPUNo )
    {
        $Report =~ s/HHMMLL/medium/;
        if ( $opt_s eq 'syslog' )
        {
            syslog 4, "warning %s", $Report;
        } else
        {
            mylog(
                join( '', "$LogTime $ProgName", "[$$] WARNING: $Report" ) );
        } ## end else [ if ( $opt_s eq 'syslog'...)]
    } else
    {
        $Report =~ s/HHMMLL/low/;
        if ( $opt_s eq 'syslog' )
        {
            syslog 6, "info %s", $Report;
        } else
        {
            mylog( join( '', "$LogTime $ProgName", "[$$] INFO: $Report" ) );
        } ## end else [ if ( $opt_s eq 'syslog'...)]
    } ## end else [ if ( $Avg > $MaxCPUThreshold...)]
    warn "DEBUG: $LogTime $Report" if ($opt_d);

    $Avg = $BlockedP_Interval / ( $VMLNo_Interval - 3 );
    $Report
        = sprintf
        "Blocked process average (I/O usage) is %0.2f %s - LS_IO=%0.2f=-=io",
        $Avg, $ReportTail_CPU, $Avg;
    if ($opt_o)
    {
        # Also write data into corresponding '.csv' file
        my ($outfile) = "$opt_o/IO.csv" =~ /^([^\0]+)$/;
        if ( open( my $CSV, '>>', $outfile ) )
        {
            printf $CSV "%s,%0.2f\n", $Now, $Avg;
            close($CSV);
        } ## end if ( open( my $CSV, '>>'...))
    } ## end if ($opt_o)
    if ( $Avg > $MaxCPUThreshold )
    {
        $Report =~ s/HHMMLL/high/;
        if ( $opt_s eq 'syslog' )
        {
            syslog 3, "error %s", $Report;
        } else
        {
            mylog( join( '', "$LogTime $ProgName", "[$$] ERROR: $Report" ) );
        } ## end else [ if ( $opt_s eq 'syslog'...)]
        if ($opt_a)
        {
            # Get the top 30 processes
            my $ProcList = '';
            if (open(
                    my $TopProc,
                    '-|',
                    ( ($HaveTimeout) ? 'timeout 60s ' : '' )
                        . 'ps -eo pid,ppid,euser,cmd,%mem,%cpu,lstart --sort=-%cpu'
                )
                )
            {
                my @lines = ();
                chomp( @lines = <$TopProc> );
                close($TopProc);
                foreach ( @lines[ 0 .. 30 ] )
                {
                    $ProcList .= "$_\n";
                } ## end foreach ( @lines[ 0 .. 30 ]...)
            } ## end if ( open( my $TopProc...))

            # Also send an email alert
            if ( length $ProcList )
            {
                SendEmail(
                    join( '',
                        "ERROR: $Report",
                        "\n\nTop 30 processes sorted by CPU usage:\n$ProcList"
                    ),
                    'normal'
                );
            } else
            {
                SendEmail( "ERROR: $Report", 'normal' );
            } ## end else [ if ( length $ProcList ...)]
        } ## end if ($opt_a)
    } elsif ( $Avg > $CPUNo )
    {
        $Report =~ s/HHMMLL/medium/;
        if ( $opt_s eq 'syslog' )
        {
            syslog 4, "warning %s", $Report;
        } else
        {
            mylog(
                join( '', "$LogTime $ProgName", "[$$] WARNING: $Report" ) );
        } ## end else [ if ( $opt_s eq 'syslog'...)]
    } else
    {
        $Report =~ s/HHMMLL/low/;
        if ( $opt_s eq 'syslog' )
        {
            syslog 6, "info %s", $Report;
        } else
        {
            mylog( join( '', "$LogTime $ProgName", "[$$] INFO: $Report" ) );
        } ## end else [ if ( $opt_s eq 'syslog'...)]
    } ## end else [ if ( $Avg > $MaxCPUThreshold...)]
    warn "DEBUG: $LogTime $Report" if ($opt_d);

    $Avg = $PageIn_Interval / ( $VMLNo_Interval - 3 );
    $Report
        = sprintf
        "Pagedin memory average (Mem usage) is %0.2f %s - LS_MEM=%0.2f=-=mem",
        $Avg, $ReportTail_RAM, $Avg;
    if ($opt_o)
    {
        # Also write data into corresponding '.csv' file
        my ($outfile) = "$opt_o/Memory.csv" =~ /^([^\0]+)$/;
        if ( open( my $CSV, '>>', $outfile ) )
        {
            printf $CSV "%s,%0.2f\n", $Now, $Avg;
            close($CSV);
        } ## end if ( open( my $CSV, '>>'...))
    } ## end if ($opt_o)
    if ( $Avg > ( $TotalRAM / 10 ) )
    {
        # More than 10% RAM swapped in
        $Report =~ s/HHMMLL/high/;
        if ( $opt_s eq 'syslog' )
        {
            syslog 3, "error %s", $Report;
        } else
        {
            mylog( join( '', "$LogTime $ProgName", "[$$] ERROR: $Report" ) );
        } ## end else [ if ( $opt_s eq 'syslog'...)]
        if ($opt_a)
        {
            # Also send an email alert
            SendEmail( "ERROR: $Report", 'normal' );
        } ## end if ($opt_a)
    } elsif ( $Avg > ( $TotalRAM / 20 ) )
    {
        # More than 5% RAM swapped in
        $Report =~ s/HHMMLL/medium/;
        if ( $opt_s eq 'syslog' )
        {
            syslog 4, "warning %s", $Report;
        } else
        {
            mylog(
                join( '', "$LogTime $ProgName", "[$$] WARNING: $Report" ) );
        } ## end else [ if ( $opt_s eq 'syslog'...)]
    } else
    {
        $Report =~ s/HHMMLL/low/;
        if ( $opt_s eq 'syslog' )
        {
            syslog 6, "info %s", $Report;
        } else
        {
            mylog( join( '', "$LogTime $ProgName", "[$$] INFO: $Report" ) );
        } ## end else [ if ( $opt_s eq 'syslog'...)]
    } ## end else [ if ( $Avg > ( $TotalRAM...))]
    warn "DEBUG: $LogTime $Report" if ($opt_d);

    #-------------------------------------------------
    # Also get partition infos (space in GB)
    if (open(
            my $PARTINFO, '-|',
            ( ($HaveTimeout) ? 'timeout 60s ' : '' ) . 'df -TPl -BG'
        )
        )
    {
        my @lines = ();
        chomp( @lines = grep {/ext[2-4]/} <$PARTINFO> );
        close($PARTINFO);
        foreach my $PartLine (@lines)
        {
            # We are only interested in ext2, ext3 and ext4 partitions
            next
                unless ( $PartLine
                =~ /^(\S+)\s+ext[2-4]\s+(\d+)G\s+\d+G\s+\d+G\s+(\d+)%\s+(\S+)/o
                );
            my $FileSystem = "$1";
            my $TotalSize  = $2;
            my $PercUsed   = $3;
            my $MountPoint = "$4";
            warn
                "DEBUG: FileSystem '$FileSystem' mounted on '$MountPoint' usage is $PercUsed% ($TotalSize GB)\n"
                if ($opt_d);
            if ($opt_o)
            {
                # Also write data into corresponding '.csv' file
                my $FileSystem_Sanitized = $FileSystem;
                $FileSystem_Sanitized =~ s@/@#@g;
                my ($outfile)
                    = "$opt_o/$FileSystem_Sanitized.csv" =~ /^([^\0]+)$/;
                if ( open( my $CSV, '>>', $outfile ) )
                {
                    printf $CSV "%s,%d\n", $Now, $PercUsed;
                    close($CSV);
                } ## end if ( open( my $CSV, '>>'...))
            } ## end if ($opt_o)

            # Use total size to define the percentage thresholds
            #  These are the thresholds for anything under 1TB
            my %PercThreshold = ( 'error' => 75, 'critical' => 85 );
            if ( $TotalSize >= 4096 )
            {
                # 4TB or more
                $PercThreshold{'error'}    = 90;
                $PercThreshold{'critical'} = 95;
            } elsif ( $TotalSize >= 1024 )
            {
                # 1TB or more
                $PercThreshold{'error'}    = 80;
                $PercThreshold{'critical'} = 85;
            } ## end elsif ( $TotalSize >= 1024...)

            # Issue error/critical messages based on the percentage used
            if ( $PercUsed > $PercThreshold{'error'} )
            {
                if ( $PercUsed > $PercThreshold{'critical'} )
                {
                    if ( $opt_s eq 'syslog' )
                    {
                        syslog 6,
                            "crit FileSystem '%s' mounted on '%s' usage is %d%%",
                            $FileSystem, $MountPoint, $PercUsed;
                    } else
                    {
                        mylog(
                            join( '',
                                "$LogTime $ProgName",
                                "[$$] CRITICAL: FileSystem '$FileSystem' mounted on '$MountPoint' usage is $PercUsed%"
                            )
                        );
                    } ## end else [ if ( $opt_s eq 'syslog'...)]
                    if ($opt_a)
                    {
                        # Also send an email alert
                        SendEmail(
                            "CRITICAL: FileSystem '$FileSystem' mounted on '$MountPoint' usage is $PercUsed%",
                            'high'
                        );
                    } ## end if ($opt_a)
                } else
                {
                    if ( $opt_s eq 'syslog' )
                    {
                        syslog 6,
                            "err FileSystem '%s' mounted on '%s' usage is %d%%",
                            $FileSystem, $MountPoint, $PercUsed;
                    } else
                    {
                        mylog(
                            join( '',
                                "$LogTime $ProgName",
                                "[$$] ERROR: FileSystem '$FileSystem' mounted on '$MountPoint' usage is $PercUsed%"
                            )
                        );
                    } ## end else [ if ( $opt_s eq 'syslog'...)]
                } ## end else [ if ( $PercUsed > $PercThreshold...)]
                if ($opt_a)
                {
                    # Also send an email alert
                    SendEmail(
                        "ERROR: FileSystem '$FileSystem' mounted on '$MountPoint' usage is $PercUsed%",
                        'normal'
                    );
                } ## end if ($opt_a)
            } else
            {
                if ( $opt_s eq 'syslog' )
                {
                    syslog 6,
                        "info FileSystem '%s' mounted on '%s' usage is %d%% - LS_DU=%0.2f=%s=%s",
                        $FileSystem, $MountPoint, $PercUsed, $PercUsed,
                        $FileSystem, $MountPoint;
                } else
                {
                    mylog(
                        join( '',
                            "$LogTime $ProgName",
                            "[$$] INFO: FileSystem '$FileSystem' mounted on '$MountPoint' usage is $PercUsed%"
                        )
                    );
                } ## end else [ if ( $opt_s eq 'syslog'...)]
            } ## end else [ if ( $PercUsed > $PercThreshold...)]
        } ## end foreach my $PartLine (@lines...)
    } ## end if ( open( my $PARTINFO...))

    #-------------------------------------------------
    # Also get partition infos (inodes)
    if (open(
            my $PARTINFO, '-|',
            ( ($HaveTimeout) ? 'timeout 60s ' : '' ) . 'df -TPil'
        )
        )
    {
        my @lines = ();
        chomp( @lines = grep {/ext[2-4]/} <$PARTINFO> );
        close($PARTINFO);
        foreach my $PartLine (@lines)
        {
            # We are only interested in ext2, ext3 and ext4 partitions
            next
                unless ( $PartLine
                =~ /^(\S+)\s+ext[2-4]\s+\d+\s+\d+\s+\d+\s+(\d+)%\s+(\S+)/o );
            my $FileSystem = "$1";
            my $PercUsed   = $2;
            my $MountPoint = "$3";
            warn
                "DEBUG: FileSystem '$FileSystem' mounted on '$MountPoint' inode usage is $PercUsed%\n"
                if ($opt_d);
            if ($opt_o)
            {
                # Also write data into corresponding '.csv' file
                my $FileSystem_Sanitized = $FileSystem;
                $FileSystem_Sanitized =~ s@/@#@g;
                my ($outfile)
                    = ( "$opt_o/Inodes_$FileSystem_Sanitized.csv"
                        =~ /^([^\0]+)$/ );
                if ( open( my $CSV, '>>', $outfile ) )
                {
                    printf $CSV "%s,%d\n", $Now, $PercUsed;
                    close($CSV);
                } ## end if ( open( my $CSV, '>>'...))
            } ## end if ($opt_o)

            # Issue error/critical messages based on the percentage used
            if ( $PercUsed > 75 )
            {
                if ( $PercUsed > 85 )
                {
                    if ( $opt_s eq 'syslog' )
                    {
                        syslog 6,
                            "crit FileSystem '%s' mounted on '%s' inode usage is %d%%",
                            $FileSystem, $MountPoint, $PercUsed;
                    } else
                    {
                        mylog(
                            join( '',
                                "$LogTime $ProgName",
                                "[$$] CRITICAL: FileSystem '$FileSystem' mounted on '$MountPoint' inode usage is $PercUsed%"
                            )
                        );
                    } ## end else [ if ( $opt_s eq 'syslog'...)]
                    if ($opt_a)
                    {
                        # Also send an email alert
                        SendEmail(
                            "CRITICAL: FileSystem '$FileSystem' mounted on '$MountPoint' inode usage is $PercUsed%",
                            'high'
                        );
                    } ## end if ($opt_a)
                } else
                {
                    if ( $opt_s eq 'syslog' )
                    {
                        syslog 6,
                            "err FileSystem '%s' mounted on '%s' inode usage is %d%%",
                            $FileSystem, $MountPoint, $PercUsed;
                    } else
                    {
                        mylog(
                            join( '',
                                "$LogTime $ProgName",
                                "[$$] ERROR: FileSystem '$FileSystem' mounted on '$MountPoint' inode usage is $PercUsed%"
                            )
                        );
                    } ## end else [ if ( $opt_s eq 'syslog'...)]
                } ## end else [ if ( $PercUsed > 85 ) ]
                if ($opt_a)
                {
                    # Also send an email alert
                    SendEmail(
                        "ERROR: FileSystem '$FileSystem' mounted on '$MountPoint' inode usage is $PercUsed%",
                        'normal'
                    );
                } ## end if ($opt_a)
            } else
            {
                if ( $opt_s eq 'syslog' )
                {
                    syslog 6,
                        "info FileSystem '%s' mounted on '%s' inode usage is %d%% - LS_DI=%0.2f=%s=%s",
                        $FileSystem, $MountPoint, $PercUsed, $PercUsed,
                        $FileSystem, $MountPoint;
                } else
                {
                    mylog(
                        join( '',
                            "$LogTime $ProgName",
                            "[$$] INFO: FileSystem '$FileSystem' mounted on '$MountPoint' inode usage is $PercUsed%"
                        )
                    );
                } ## end else [ if ( $opt_s eq 'syslog'...)]
            } ## end else [ if ( $PercUsed > 75 ) ]
        } ## end foreach my $PartLine (@lines...)
    } ## end if ( open( my $PARTINFO...))

    #-------------------------------------------------
    # Also compute network interface errors and speeds
    if ( open( my $DEVSTAT, '<', '/proc/net/dev' ) )
    {
        my @lines = ();
        chomp( @lines = <$DEVSTAT> );
        close($DEVSTAT);
        foreach my $DEVLine (@lines)
        {
            next unless ( $DEVLine =~ /^\s*(\S+):\s*(.*)/o );
            my $IFName = "$1";
            my $IFData = "$2";
            next if ( $IFName eq 'lo' );
            chomp($IFData);

            warn "DEBUG: $LogTime $IFData" if ($opt_d);
            my ($RXbytes, $RXpkts,  $RXerr,   $RXdrop, $RXfifo, $RXframe,
                $RXcomp,  $RXmulti, $TXbytes, $TXpkts, $TXerr,  $TXdrop,
                $TXfifo,  $TXframe, $TXcomp,  $TXmulti
            ) = split( /\s+/, $IFData );
            warn join( '',
                "DEBUG: $LogTime",
                " $RXbytes, $RXpkts,  $RXerr,  $RXdrop, $RXfifo,  $RXframe, $RXcomp, $RXmulti,",
                " $TXbytes, $TXpkts,  $TXerr,  $TXdrop, $TXfifo,  $TXframe, $TXcomp, $TXmulti"
            ) if ($opt_d);

            if ($opt_o)
            {
                # Also write data into corresponding '.csv' file
                my ($outfile) = "$opt_o/$IFName.csv" =~ /^([^\0]+)$/;
                if ( open( my $CSV, '>>', $outfile ) )
                {
                    printf $CSV
                        "%s,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",
                        $Now,    $RXbytes, $RXpkts, $RXerr,   $RXdrop,
                        $RXfifo, $RXframe, $RXcomp, $RXmulti, $TXbytes,
                        $TXpkts, $TXerr, $TXdrop, $TXfifo, $TXframe, $TXcomp,
                        $TXmulti;
                    close($CSV);
                } ## end if ( open( my $CSV, '>>'...))
            } ## end if ($opt_o)

            my $RX_percent
                = ( $RXpkts > 0 )
                ? ( $RXerr + $RXdrop + $RXfifo + $RXcomp ) * 100 / $RXpkts
                : 0;
            $Report
                = sprintf
                "Receive errors for %s is %0.2f%% - LS_RE=%0.2f=%s=-",
                $IFName, $RX_percent, $RX_percent, $IFName;
            if ( $RX_percent < 0.3 )
            {
                $Report =~ 's/ -/ (low) -/';
                if ( $opt_s eq 'syslog' )
                {
                    syslog 6, "info %s", $Report;
                } else
                {
                    mylog(
                        join(
                            '', "$LogTime $ProgName", "[$$] INFO: $Report"
                        )
                    );
                } ## end else [ if ( $opt_s eq 'syslog'...)]
            } elsif ( $RX_percent < 1 )
            {
                $Report =~ 's/ -/ (medium) -';
                if ( $opt_s eq 'syslog' )
                {
                    syslog 6, "info %s", $Report;
                } else
                {
                    mylog(
                        join(
                            '', "$LogTime $ProgName", "[$$] INFO: $Report"
                        )
                    );
                } ## end else [ if ( $opt_s eq 'syslog'...)]
            } else
            {
                $Report =~ 's/ -/ (high) -/';
                if ( $opt_s eq 'syslog' )
                {
                    syslog 6, "info %s", $Report;
                } else
                {
                    mylog(
                        join(
                            '', "$LogTime $ProgName", "[$$] INFO: $Report"
                        )
                    );
                } ## end else [ if ( $opt_s eq 'syslog'...)]
            } ## end else [ if ( $RX_percent < 0.3...)]
            warn "DEBUG: $LogTime $Report" if ($opt_d);

            # Calculate the receive speed
            if ( $RXPrevBytes_Interval{"$IFName"} )
            {
                my $Speed = 0;
                if ( $RXPrevBytes_Interval{"$IFName"} > $RXbytes )
                {

                    # Deal with the 4GB counter limitation of the Linux kernel
                    $Speed = (
                        (   ( 4294967296 - $RXPrevBytes_Interval{"$IFName"} )
                            + $RXbytes
                        ) / $opt_I
                    ) * 8;
                } else
                {
                    $Speed
                        = ( ( $RXbytes - $RXPrevBytes_Interval{"$IFName"} )
                        / $opt_I ) * 8;
                } ## end else [ if ( $RXPrevBytes_Interval...)]
                $Report
                    = sprintf
                    "Receive speed for %s is %s bits/s - LS_RS=%0.2f=%s=-",
                    $IFName, commify( sprintf "%.2f", $Speed ), $Speed,
                    $IFName;
                if ( $opt_s eq 'syslog' )
                {
                    syslog 6, "info %s", $Report;
                } else
                {
                    mylog(
                        join(
                            '', "$LogTime $ProgName", "[$$] INFO: $Report"
                        )
                    );
                } ## end else [ if ( $opt_s eq 'syslog'...)]
                warn "DEBUG: $LogTime $Report" if ($opt_d);
            } ## end if ( $RXPrevBytes_Interval...)
            $RXPrevBytes_Interval{"$IFName"} = $RXbytes;

            my $TX_percent
                = ( $TXpkts > 0 )
                ? ( $TXerr + $TXdrop + $TXfifo + $TXcomp ) * 100 / $TXpkts
                : 0;
            $Report
                = sprintf
                "Transmit errors for %s is %0.2f%% - LS_TE=%0.2f=%s=-",
                $IFName, $TX_percent, $TX_percent, $IFName;
            if ( $TX_percent < 0.3 )
            {
                $Report =~ 's/ -/ (low) -/';
                if ( $opt_s eq 'syslog' )
                {
                    syslog 6, "info %s", $Report;
                } else
                {
                    mylog(
                        join(
                            '', "$LogTime $ProgName", "[$$] INFO: $Report"
                        )
                    );
                } ## end else [ if ( $opt_s eq 'syslog'...)]
            } elsif ( $TX_percent < 1 )
            {
                $Report =~ 's/ -/ (medium) -/';
                if ( $opt_s eq 'syslog' )
                {
                    syslog 6, "info %s", $Report;
                } else
                {
                    mylog(
                        join(
                            '', "$LogTime $ProgName", "[$$] INFO: $Report"
                        )
                    );
                } ## end else [ if ( $opt_s eq 'syslog'...)]
            } else
            {
                $Report =~ 's/ -/ (high) -/';
                if ( $opt_s eq 'syslog' )
                {
                    syslog 6, "info %s", $Report;
                } else
                {
                    mylog(
                        join(
                            '', "$LogTime $ProgName", "[$$] INFO: $Report"
                        )
                    );
                } ## end else [ if ( $opt_s eq 'syslog'...)]
            } ## end else [ if ( $TX_percent < 0.3...)]
            warn "DEBUG: $LogTime $Report" if ($opt_d);

            # Calculate the transmit speed
            if ( $TXPrevBytes_Interval{"$IFName"} )
            {
                my $Speed = 0;
                if ( $TXPrevBytes_Interval{"$IFName"} > $TXbytes )
                {

                    # Deal with the 4GB counter limitation of the Linux kernel
                    $Speed = (
                        (   ( 4294967296 - $TXPrevBytes_Interval{"$IFName"} )
                            + $TXbytes
                        ) / $opt_I
                    ) * 8;
                } else
                {
                    $Speed
                        = ( ( $TXbytes - $TXPrevBytes_Interval{"$IFName"} )
                        / $opt_I ) * 8;
                } ## end else [ if ( $TXPrevBytes_Interval...)]
                $Report
                    = sprintf
                    "Transmit speed for %s is %s bits/s - LS_TS=%0.2f=%s=-",
                    $IFName, commify( sprintf "%.2f", $Speed ), $Speed,
                    $IFName;
                if ( $opt_s eq 'syslog' )
                {
                    syslog 6, "info %s", $Report;
                } else
                {
                    mylog(
                        join(
                            '', "$LogTime $ProgName", "[$$] INFO: $Report"
                        )
                    );
                } ## end else [ if ( $opt_s eq 'syslog'...)]
                warn "DEBUG: $LogTime $Report" if ($opt_d);
            } ## end if ( $TXPrevBytes_Interval...)
            $TXPrevBytes_Interval{"$IFName"} = $TXbytes;
        } ## end foreach my $DEVLine (@lines...)
    } ## end if ( open( my $DEVSTAT...))

    #-------------------------
    # Also compute disk speeds
    # See: http://ubuntuforums.org/showthread.php?t=31213
    if ( open( my $DEVSTAT, '<', '/proc/diskstats' ) )
    {
        my @lines = ();
        chomp( @lines = <$DEVSTAT> );
        close($DEVSTAT);
        foreach my $DEVLine (@lines)
        {
            # See: http://www.mjmwired.net/kernel/Documentation/iostats.txt
            my $DiskPart   = '';
            my $DiskReads  = 0;
            my $DiskWrites = 0;
            if ( $DEVLine
                =~ /^\s*\d+\s+\d+\s+([a-z]d[a-z]\d+)\s+\d+\s+\d+\s+(\d+)\s+\d+\s+\d+\s+\d+\s+(\d+)/o
                )
            {
                # [a-z]d[a-z][0-9], eg. sda1
                $DiskPart   = "$1";
                $DiskReads  = $2 * 512;
                $DiskWrites = $3 * 512;

            } elsif ( $DEVLine
                =~ /^\s*\d+\s+\d+\s+(dm\-\d+)\s+\d+\s+\d+\s+(\d+)\s+\d+\s+\d+\s+\d+\s+(\d+)/o
                )
            {
                # dm-[0-9], eg. dm-0
                $DiskPart   = "$1";
                $DiskReads  = $2 * 512;
                $DiskWrites = $3 * 512;
            } elsif ( $DEVLine
                =~ /^\s*\d+\s+\d+\s+(cciss\/c\dd\dp\d)\s+\d+\s+\d+\s+(\d+)\s+\d+\s+\d+\s+\d+\s+(\d+)/o
                )
            {
                # cciss/c?d?p?, eg. cciss/c0d0p1
                $DiskPart   = "$1";
                $DiskReads  = $2 * 512;
                $DiskWrites = $3 * 512;
            } ## end elsif ( $DEVLine =~ ...)

            # No need to continue if we don't have a disk partition
            next unless ( length($DiskPart) );

            warn "DEBUG: $LogTime $DiskPart $DiskReads $DiskWrites"
                if ($opt_d);
            if ($opt_o)
            {
                # Also write data into corresponding '.csv' file
                my ($outfile) = "$opt_o/$DiskPart.csv" =~ /^([^\0]+)$/;
                if ( open( my $CSV, '>>', $outfile ) )
                {
                    printf $CSV "%s,%d,%d\n", $Now, $DiskReads, $DiskWrites;
                    close($CSV);
                } ## end if ( open( my $CSV, '>>'...))
            } ## end if ($opt_o)

            # Calculate the read speed for the partition
            my $DiskReadSpeed  = -1;
            my $DiskWriteSpeed = -1;
            if ( $ReadBytes_Interval{"$DiskPart"} )
            {
                my $Speed = 0;
                if ( $ReadBytes_Interval{"$DiskPart"} > $DiskReads )
                {

                    # Deal with the 4GB counter limitation of the Linux kernel
                    $Speed
                        = ( ( 4294967296 - $ReadBytes_Interval{"$DiskPart"} )
                        + $DiskReads ) / $opt_I;
                } else
                {
                    $Speed = ( $DiskReads - $ReadBytes_Interval{"$DiskPart"} )
                        / $opt_I;
                } ## end else [ if ( $ReadBytes_Interval...)]

                # Remember the disk read speed for later
                $DiskReadSpeed = $Speed if ($opt_o);

                $Report
                    = sprintf
                    "Disk read speed for %s is %s bytes/s - LS_DR=%0.2f=%s=-",
                    $DiskPart, commify( sprintf "%.2f", $Speed ), $Speed,
                    $DiskPart;
                if ( $opt_s eq 'syslog' )
                {
                    syslog 6, "info %s", $Report;
                } else
                {
                    mylog(
                        join(
                            '', "$LogTime $ProgName", "[$$] INFO: $Report"
                        )
                    );
                } ## end else [ if ( $opt_s eq 'syslog'...)]
                warn "DEBUG: $LogTime $Report" if ($opt_d);
            } ## end if ( $ReadBytes_Interval...)
            $ReadBytes_Interval{"$DiskPart"} = $DiskReads;

            # Calculate the write speed for the partition
            if ( $WriteBytes_Interval{"$DiskPart"} )
            {
                my $Speed = 0;
                if ( $WriteBytes_Interval{"$DiskPart"} > $DiskWrites )
                {

                    # Deal with the 4GB counter limitation of the Linux kernel
                    $Speed
                        = ( ( 4294967296 - $WriteBytes_Interval{"$DiskPart"} )
                        + $DiskWrites ) / $opt_I;
                } else
                {
                    $Speed
                        = ( $DiskWrites - $WriteBytes_Interval{"$DiskPart"} )
                        / $opt_I;
                } ## end else [ if ( $WriteBytes_Interval...)]

                # Remember the disk write speed for later
                $DiskWriteSpeed = $Speed if ($opt_o);

                $Report
                    = sprintf
                    "Disk write speed for %s is %s bytes/s - LS_DW=%0.2f=%s=-",
                    $DiskPart, commify( sprintf "%.2f", $Speed ), $Speed,
                    $DiskPart;
                if ( $opt_s eq 'syslog' )
                {
                    syslog 6, "info %s", $Report;
                } else
                {
                    mylog(
                        join(
                            '', "$LogTime $ProgName", "[$$] INFO: $Report"
                        )
                    );
                } ## end else [ if ( $opt_s eq 'syslog'...)]
                warn "DEBUG: $LogTime $Report" if ($opt_d);
            } ## end if ( $WriteBytes_Interval...)
            $WriteBytes_Interval{"$DiskPart"} = $DiskWrites;

            if ($opt_o)
            {
                # Also write disk speed data into corresponding '.csv' file
                my ($outfile) = "$opt_o/Speeds_$DiskPart.csv" =~ /^([^\0]+)$/;
                if ( open( my $CSV, '>>', $outfile ) )
                {
                    printf $CSV "%s,%d,%d\n", $Now, $DiskReadSpeed,
                        $DiskWriteSpeed;
                    close($CSV);
                } ## end if ( open( my $CSV, '>>'...))
            } ## end if ($opt_o)
        } ## end foreach my $DEVLine (@lines...)
    } ## end if ( open( my $DEVSTAT...))

    #-------------------------
    # Get MySQL stats if it is running locally
    my $GetDBStats = 0;
    if (open(
            my $RunDB,
            '-|',
            ( ($HaveTimeout) ? 'timeout 60s ' : '' )
                . 'pgrep -c "(mysql|mariad)b"'
        )
        )
    {
        $GetDBStats = <$RunDB>;
        close($RunDB);
    } ## end if ( open( my $RunDB, ...))
    if ( $GetDBStats > 0 )
    {
        my $LS_regex = qr/^.+(LS_.*)/;
        $Report = GetMysqldStats();
        if ( $opt_s eq 'syslog' )
        {
            syslog 6, "info %s", $Report;
        } else
        {
            mylog( join( '', "$LogTime $ProgName", "[$$] INFO: $Report" ) );
        } ## end else [ if ( $opt_s eq 'syslog'...)]
        warn "DEBUG: $LogTime $Report" if ($opt_d);
        if ($opt_o)
        {
            # Also write data into corresponding '.csv' file
            my ($outfile) = "$opt_o/MySQL.csv" =~ /^([^\0]+)$/;
            if ( open( my $CSV, '>>', $outfile ) )
            {
                my ($LS_Part) = ( $Report =~ $LS_regex );
                printf $CSV "%s,memory,%s\n", $Now, $LS_Part;
                close($CSV);
            } ## end if ( open( my $CSV, '>>'...))
        } ## end if ($opt_o)

        if ( -s '/etc/mysql/debian.cnf' )
        {
            my %MysqlStatus;
            my $HaveDBI = eval {
                require DBI;
                DBI->import();
                1;
            };
            if ($HaveDBI)
            {

                # Get mysql stats via "DBI" module
                my $dbh = DBI->connect(
                    "DBI:mysql:database=mysql;host=localhost;mysql_read_default_file=/etc/mysql/debian.cnf;",
                ) or die $DBI::errstr;
                my $sth = $dbh->prepare('SHOW GLOBAL STATUS');
                $sth->execute() or die $DBI::errstr;
                while ( my ( $mysql_vname, $mysql_value )
                    = $sth->fetchrow_array() )
                {
                    $mysql_vname =~ tr/[A-Z]/[a-z]/;
                    $mysql_value = 'NULL' if !defined($mysql_value);
                    $MysqlStatus{"$mysql_vname"} = "$mysql_value";
                    warn join( '',
                        "Mysql Data: $mysql_vname = ",
                        $MysqlStatus{"$mysql_vname"}, "\n" )
                        if ($opt_d);
                } ## end while ( my ( $mysql_vname...))
                $sth = $dbh->prepare('SELECT @@max_connections');
                $sth->execute() or die $DBI::errstr;
                while ( my @data = $sth->fetchrow_array() )
                {
                    $MysqlStatus{'max_connections'} = $data[0];
                } ## end while ( my @data = $sth->...)
                $dbh->disconnect or die $DBI::errstr if $dbh;
            } else
            {
                # Get mysql stats via command line
                open(
                    my $MS,
                    '-|',
                    ( ($HaveTimeout) ? 'timeout 60s ' : '' )
                        . "mysql --defaults-file=/etc/mysql/debian.cnf -ANBe 'SHOW GLOBAL STATUS'"
                ) or die "$!";
                my @lines = ();
                chomp( @lines = <$MS> );
                close($MS);
                foreach my $Line (@lines)
                {
                    warn "$Line" if ($opt_d);
                    my ( $mysql_vname, $mysql_value ) = split( /\s+/, $Line );
                    $mysql_vname =~ tr/[A-Z]/[a-z]/;
                    $mysql_value = 'NULL' if !defined($mysql_value);
                    $MysqlStatus{"$mysql_vname"} = "$mysql_value";
                    warn join( '',
                        "Mysql Data: $mysql_vname = ",
                        $MysqlStatus{"$mysql_vname"}, "\n" )
                        if ($opt_d);
                } ## end foreach my $Line (@lines)
                open(
                    my $MS,
                    '-|',
                    ( ($HaveTimeout) ? 'timeout 60s ' : '' )
                        . "mysql --defaults-file=/etc/mysql/debian.cnf -ANBe 'SELECT \@\@max_connections'"
                ) or die "$!";
                @lines = ();
                chomp( @lines = <$MS> );
                close($MS);
                foreach my $Line (@lines)
                {
                    warn "$Line" if ($opt_d);
                    $MysqlStatus{'max_connections'} = "$Line";
                    warn join( '',
                        "Mysql Data: max_connections = ",
                        $MysqlStatus{'max_connections'}, "\n" )
                        if ($opt_d);
                } ## end foreach my $Line (@lines)
            } ## end else [ if ($HaveDBI) ]

            # Show slow query stats
            my $SlowPercent
                = ( $MysqlStatus{'slow_queries'} / $MysqlStatus{'queries'} )
                * 100;
            $Report
                = sprintf
                "Mysql slow queries %d (%0.2f%%) - LS_SQ=%d=%0.2f=mysql",
                $MysqlStatus{'slow_queries'}, $SlowPercent,
                $MysqlStatus{'slow_queries'}, $SlowPercent;
            if ( $opt_s eq 'syslog' )
            {
                syslog 6, "info %s", $Report;
            } else
            {
                mylog(
                    join( '', "$LogTime $ProgName", "[$$] INFO: $Report" ) );
            } ## end else [ if ( $opt_s eq 'syslog'...)]
            warn "DEBUG: $LogTime $Report" if ($opt_d);
            if ($opt_o)
            {
                # Also write data into corresponding '.csv' file
                my ($outfile) = "$opt_o/MySQL.csv" =~ /^([^\0]+)$/;
                if ( open( my $CSV, '>>', $outfile ) )
                {
                    my ($LS_Part) = ( $Report =~ $LS_regex );
                    printf $CSV "%s,queries,%s\n", $Now, $LS_Part;
                    close($CSV);
                } ## end if ( open( my $CSV, '>>'...))
            } ## end if ($opt_o)

            # Show connection percentage stats
            $Report
                = sprintf
                "Mysql connections max allowed %d, max used %d (%0.2f%%) - LS_CC=%d=%d=mysql",
                $MysqlStatus{'max_connections'},
                $MysqlStatus{'max_used_connections'},
                (     $MysqlStatus{'max_used_connections'}
                    / $MysqlStatus{'max_connections'} ) * 100,
                $MysqlStatus{'max_connections'},
                $MysqlStatus{'max_used_connections'};
            if ( $opt_s eq 'syslog' )
            {
                syslog 6, "info %s", $Report;
            } else
            {
                mylog(
                    join( '', "$LogTime $ProgName", "[$$] INFO: $Report" ) );
            } ## end else [ if ( $opt_s eq 'syslog'...)]
            warn "DEBUG: $LogTime $Report" if ($opt_d);
            if ($opt_o)
            {
                # Also write data into corresponding '.csv' file
                my ($outfile) = "$opt_o/MySQL.csv" =~ /^([^\0]+)$/;
                if ( open( my $CSV, '>>', $outfile ) )
                {
                    my ($LS_Part) = ( $Report =~ $LS_regex );
                    printf $CSV "%s,connections,%s\n", $Now, $LS_Part;
                    close($CSV);
                } ## end if ( open( my $CSV, '>>'...))
            } ## end if ($opt_o)

            # Show connection error stats
            $Report
                = sprintf
                "Mysql connection errors internal %d, total %d - LS_CE=%d=%d=mysql",
                $MysqlStatus{'connection_errors_internal'},
                $MysqlStatus{'connection_errors'},
                $MysqlStatus{'connection_errors_internal'},
                $MysqlStatus{'connection_errors'};
            if ( $opt_s eq 'syslog' )
            {
                syslog 6, "info %s", $Report;
            } else
            {
                mylog(
                    join( '', "$LogTime $ProgName", "[$$] INFO: $Report" ) );
            } ## end else [ if ( $opt_s eq 'syslog'...)]
            warn "DEBUG: $LogTime $Report" if ($opt_d);
            if ($opt_o)
            {
                # Also write data into corresponding '.csv' file
                my ($outfile) = "$opt_o/MySQL.csv" =~ /^([^\0]+)$/;
                if ( open( my $CSV, '>>', $outfile ) )
                {
                    my ($LS_Part) = ( $Report =~ $LS_regex );
                    printf $CSV "%s,connection_errors,%s\n", $Now, $LS_Part;
                    close($CSV);
                } ## end if ( open( my $CSV, '>>'...))
            } ## end if ($opt_o)

            # Show aborted connections stats
            $Report
                = sprintf
                "Mysql aborted clients internal %d, connections %d - LS_AC=%d=%d=mysql",
                $MysqlStatus{'aborted_clients'},
                $MysqlStatus{'aborted_connects'},
                $MysqlStatus{'aborted_clients'},
                $MysqlStatus{'aborted_connects'};
            if ( $opt_s eq 'syslog' )
            {
                syslog 6, "info %s", $Report;
            } else
            {
                mylog(
                    join( '', "$LogTime $ProgName", "[$$] INFO: $Report" ) );
            } ## end else [ if ( $opt_s eq 'syslog'...)]
            warn "DEBUG: $LogTime $Report" if ($opt_d);
            if ($opt_o)
            {
                # Also write data into corresponding '.csv' file
                my ($outfile) = "$opt_o/MySQL.csv" =~ /^([^\0]+)$/;
                if ( open( my $CSV, '>>', $outfile ) )
                {
                    my ($LS_Part) = ( $Report =~ $LS_regex );
                    printf $CSV "%s,aborted,%s\n", $Now, $LS_Part;
                    close($CSV);
                } ## end if ( open( my $CSV, '>>'...))
            } ## end if ($opt_o)

            # Show thread performance
            my $ThreadPercent = 100 - (
                (         $MysqlStatus{'threads_created'}
                        / $MysqlStatus{'connections'}
                ) * 100
            );
            $Report
                = sprintf
                "Mysql thread performance %0.2f%% - LS_TP=%0.2f=-=mysql",
                $ThreadPercent,
                $ThreadPercent;
            if ( $opt_s eq 'syslog' )
            {
                syslog 6, "info %s", $Report;
            } else
            {
                mylog(
                    join( '', "$LogTime $ProgName", "[$$] INFO: $Report" ) );
            } ## end else [ if ( $opt_s eq 'syslog'...)]
            warn "DEBUG: $LogTime $Report" if ($opt_d);
            if ($opt_o)
            {
                # Also write data into corresponding '.csv' file
                my ($outfile) = "$opt_o/MySQL.csv" =~ /^([^\0]+)$/;
                if ( open( my $CSV, '>>', $outfile ) )
                {
                    my ($LS_Part) = ( $Report =~ $LS_regex );
                    printf $CSV "%s,thread_performance,%s\n", $Now, $LS_Part;
                    close($CSV);
                } ## end if ( open( my $CSV, '>>'...))
            } ## end if ($opt_o)

            # Show InnoDB buffer pool performance
            my $InnoDB_PoolPercent
                = (   $MysqlStatus{'innodb_buffer_pool_pages_free'}
                    / $MysqlStatus{'innodb_buffer_pool_pages_total'} )
                * 100;
            $Report
                = sprintf
                "Mysql InnoDB pool performance %0.2f%% - LS_PP=%0.2f=-=mysql",
                $InnoDB_PoolPercent,
                $InnoDB_PoolPercent;
            if ( $opt_s eq 'syslog' )
            {
                syslog 6, "info %s", $Report;
            } else
            {
                mylog(
                    join( '', "$LogTime $ProgName", "[$$] INFO: $Report" ) );
            } ## end else [ if ( $opt_s eq 'syslog'...)]
            warn "DEBUG: $LogTime $Report" if ($opt_d);
            if ($opt_o)
            {
                # Also write data into corresponding '.csv' file
                my ($outfile) = "$opt_o/MySQL.csv" =~ /^([^\0]+)$/;
                if ( open( my $CSV, '>>', $outfile ) )
                {
                    my ($LS_Part) = ( $Report =~ $LS_regex );
                    printf $CSV "%s,innodb_pool_performance,%s\n", $Now,
                        $LS_Part;
                    close($CSV);
                } ## end if ( open( my $CSV, '>>'...))
            } ## end if ($opt_o)

            # Show InnoDB buffer read performance
            my $InnoDB_ReadPercent
                = (   $MysqlStatus{'innodb_buffer_pool_reads'}
                    / $MysqlStatus{'innodb_buffer_pool_read_requests'} )
                * 100;
            $Report
                = sprintf
                "Mysql InnoDB read performance %0.2f%% - LS_RP=%0.2f=-=mysql",
                $InnoDB_ReadPercent,
                $InnoDB_ReadPercent;
            if ( $opt_s eq 'syslog' )
            {
                syslog 6, "info %s", $Report;
            } else
            {
                mylog(
                    join( '', "$LogTime $ProgName", "[$$] INFO: $Report" ) );
            } ## end else [ if ( $opt_s eq 'syslog'...)]
            warn "DEBUG: $LogTime $Report" if ($opt_d);
            if ($opt_o)
            {
                # Also write data into corresponding '.csv' file
                my ($outfile) = "$opt_o/MySQL.csv" =~ /^([^\0]+)$/;
                if ( open( my $CSV, '>>', $outfile ) )
                {
                    my ($LS_Part) = ( $Report =~ $LS_regex );
                    printf $CSV "%s,innodb_read_performance,%s\n", $Now,
                        $LS_Part;
                    close($CSV);
                } ## end if ( open( my $CSV, '>>'...))
            } ## end if ($opt_o)
        } ## end if ( -s '/etc/mysql/debian.cnf'...)
    } ## end if ( $GetDBStats > 0 )

    #-------------------------
    # Find any reverse proxies
    if ($HaveCurl)
    {
        # Curl with compression is faster that wget
        system(
            join( '',
                'curl --compressed -sL -o /dev/null -D /tmp/frp --max-time 60 --user-agent ',
                "$ProgName/$ProgVers",
                ' http://google.com' )
        );
    } else
    {
        # Oh well, wget works too
        system(
            join( '',
                'wget -q -t 2 -T 20 -S -O /dev/null --timeout=60 --user-agent=',
                "$ProgName/$ProgVers",
                ' http://google.com 2> /tmp/frp' )
        );
    } ## end else [ if ($HaveCurl) ]
    if ( open( my $FRP, '<', '/tmp/frp' ) )
    {
        my @lines = ();
        chomp( @lines = <$FRP> );
        close($FRP);
        foreach my $Line (@lines)
        {
            next
                unless ( $Line
                =~ /\|(?:127|10|172\.1[6-9]|172\.2[0-9]|172\.3[0-1]|^192\.168)\./o
                );
            $Report
                = "Found a possible reverse proxy running on the local host - for a full report see /tmp/frp";
            if ( $opt_s eq 'syslog' )
            {
                syslog 6, "info %s", $Report;
            } else
            {
                mylog(
                    join( '', "$LogTime $ProgName", "[$$] INFO: $Report" ) );
            } ## end else [ if ( $opt_s eq 'syslog'...)]
            warn "DEBUG: $LogTime $Report" if ($opt_d);
            last;
        } ## end foreach my $Line (@lines)
    } ## end if ( open( my $FRP, '<'...))

    #-------------------------
    # Get free memory percentage
    my %MemInfo = ();
    if (open(
            my $FREE, '-|', ( ($HaveTimeout) ? 'timeout 60s ' : '' ) . 'free'
        )
        )
    {
        while (<$FREE>)
        {
            warn("DEBUG: $LogTime $_") if ($opt_d);
            if (/^Mem:\s+(\d+)\s+\d+\s+(\d+)/o)
            {
                $MemInfo{'Total'} = $1;
                $MemInfo{'Free'}  = $2;
                warn join( '',
                    "DEBUG: $LogTime Total: ",
                    $MemInfo{'Total'}, ", Free: ", $MemInfo{'Free'}, "\n" )
                    if ($opt_d);
                last;
            } ## end if (/^Mem:\s+(\d+)\s+\d+\s+(\d+)/o...)
        } ## end while (<$FREE>)
        close($FREE);
    } ## end if ( open( my $FREE, '-|'...))
    my $MemFreePerc = ( $MemInfo{'Free'} / $MemInfo{'Total'} ) * 100;
    warn "DEBUG: $LogTime MemFreePerc: $MemFreePerc\n" if ($opt_d);

    # Log the percentage of free memory
    $Report = sprintf "%.2f%% free memory", $MemFreePerc;
    if ( $opt_s eq 'syslog' )
    {
        syslog 6, "info %s", $Report;
    } else
    {
        mylog( join( '', "$LogTime $ProgName", "[$$] INFO: $Report" ) );
    } ## end else [ if ( $opt_s eq 'syslog'...)]

    if ( $MemFreePerc < 1 )
    {
        # Clear filesystem buffers and page caches
        #  See https://linux-mm.org/Drop_Caches
        if ( open( my $DC, '>', '/proc/sys/vm/drop_caches' ) )
        {
            $Report = 'Clearing filesystem buffers and page caches';
            if ( $opt_s eq 'syslog' )
            {
                syslog 6, "info %s", $Report;
            } else
            {
                mylog(
                    join( '', "$LogTime $ProgName", "[$$] INFO: $Report" ) );
            } ## end else [ if ( $opt_s eq 'syslog'...)]
             #            system( ( ($HaveTimeout) ? 'timeout 60s ' : '' ) . 'sync' );
            print $DC "3\n";
            close($DC);
        } ## end if ( open( my $DC, '>'...))
    } elsif ( $MemFreePerc < 5 )
    {
        # Clear OS page caches
        if ( open( my $DC, '>', '/proc/sys/vm/drop_caches' ) )
        {
            $Report = 'Clearing page caches';
            if ( $opt_s eq 'syslog' )
            {
                syslog 6, "info %s", $Report;
            } else
            {
                mylog(
                    join( '', "$LogTime $ProgName", "[$$] INFO: $Report" ) );
            } ## end else [ if ( $opt_s eq 'syslog'...)]
             #            system( ( ($HaveTimeout) ? 'timeout 60s ' : '' ) . 'sync' );
            print $DC "1\n";
            close($DC);
        } ## end if ( open( my $DC, '>'...))
    } ## end elsif ( $MemFreePerc < 5 ...)

    # Renice the vzdump processes if necessary
    if (open(
            my $PSL, '-|',
            ( ($HaveTimeout) ? 'timeout 60s ' : '' ) . 'ps -eo pid,pri,cmd'
        )
        )
    {
        my @lines = ();
        chomp( @lines = grep {/vzdump/} <$PSL> );
        close($PSL);
        foreach (@lines)
        {
            if (/^\s*(\d+)\s+(\d+).*vzdump[:\s\-]q*/o)
            {
                my $VZD_PID  = $1;
                my $VZD_PRIO = $2;
                if ( $VZD_PRIO == 9 )
                {
                    $Report = "Renicing vzdump process $VZD_PID";
                    if ( $opt_s eq 'syslog' )
                    {
                        syslog 6, "info %s", $Report;
                    } else
                    {
                        mylog(
                            join( '',
                                "$LogTime $ProgName",
                                "[$$] INFO: $Report" )
                        );
                    } ## end else [ if ( $opt_s eq 'syslog'...)]
                    system( ( ($HaveTimeout) ? 'timeout 60s ' : '' )
                        . "renice -n 19 -p $VZD_PID" );
                } ## end if ( $VZD_PRIO == 9 )
            } ## end if (/^\s*(\d+)\s+(\d+).*vzdump[:\s\-]q*/o...)
        } ## end foreach (@lines)
    } ## end if ( open( my $PSL, '-|'...))

    # Start the next reporting cycle
    $VMLNo_Interval    = 3;
    $RunQueue_Interval = 0;
    $BlockedP_Interval = 0;
    $PageIn_Interval   = 0;
    $CPUWait_Interval  = 0;

    return (0);
} ## end sub ReportInterval

#--------------------------------------------------------------------
# Create an hourly report
#--------------------------------------------------------------------
sub ReportHourly()
{
    $LogTime = strftime( "%F %T", localtime );

    # Write a syslog entry with the correct severity
    my $Avg = $RunQueue_Hourly / ( $VMLNo_Hourly - 3 );
    my $Report
        = sprintf "HOURLY: Running process average (CPU usage) = %0.2f %s",
        $Avg,
        $ReportTail_CPU;
    my $MaxCPUThreshold = ( $CPUNo + 1 ) * 2;
    if ( $Avg > $MaxCPUThreshold )
    {
        $Report =~ s/HHMMLL/high/;
        if ( $opt_s eq 'syslog' )
        {
            syslog 3, "error %s", $Report;
        } else
        {
            mylog( join( '', "$LogTime $ProgName", "[$$] INFO: $Report" ) );
        } ## end else [ if ( $opt_s eq 'syslog'...)]
    } elsif ( $Avg > $CPUNo )
    {
        $Report =~ s/HHMMLL/medium/;
        if ( $opt_s eq 'syslog' )
        {
            syslog 4, "warning %s", $Report;
        } else
        {
            mylog( join( '', "$LogTime $ProgName", "[$$] INFO: $Report" ) );
        } ## end else [ if ( $opt_s eq 'syslog'...)]
    } else
    {
        $Report =~ s/HHMMLL/low/;
        if ( $opt_s eq 'syslog' )
        {
            syslog 6, "info %s", $Report;
        } else
        {
            mylog( join( '', "$LogTime $ProgName", "[$$] INFO: $Report" ) );
        } ## end else [ if ( $opt_s eq 'syslog'...)]
    } ## end else [ if ( $Avg > $MaxCPUThreshold...)]
    warn "DEBUG: $LogTime $Report" if ($opt_d);

    $Avg = $BlockedP_Hourly / ( $VMLNo_Hourly - 3 );
    $Report
        = sprintf "HOURLY: Blocked process average (I/O usage) = %0.2f %s",
        $Avg, $ReportTail_CPU;
    if ( $Avg > $MaxCPUThreshold )
    {
        $Report =~ s/HHMMLL/high/;
        if ( $opt_s eq 'syslog' )
        {
            syslog 3, "error %s", $Report;
        } else
        {
            mylog( join( '', "$LogTime $ProgName", "[$$] INFO: $Report" ) );
        } ## end else [ if ( $opt_s eq 'syslog'...)]
    } elsif ( $Avg > $CPUNo )
    {
        $Report =~ s/HHMMLL/medium/;
        if ( $opt_s eq 'syslog' )
        {
            syslog 4, "warning %s", $Report;
        } else
        {
            mylog( join( '', "$LogTime $ProgName", "[$$] INFO: $Report" ) );
        } ## end else [ if ( $opt_s eq 'syslog'...)]
    } else
    {
        $Report =~ s/HHMMLL/low/;
        if ( $opt_s eq 'syslog' )
        {
            syslog 6, "info %s", $Report;
        } else
        {
            mylog( join( '', "$LogTime $ProgName", "[$$] INFO: $Report" ) );
        } ## end else [ if ( $opt_s eq 'syslog'...)]
    } ## end else [ if ( $Avg > $MaxCPUThreshold...)]
    warn "DEBUG: $LogTime $Report" if ($opt_d);

    $Avg = $PageIn_Hourly / ( $VMLNo_Hourly - 3 );
    $Report
        = sprintf "HOURLY: Paged-in memory average (Mem usage) = %0.2f %s",
        $Avg, $ReportTail_RAM;
    if ( $Avg > ( $TotalRAM / 10 ) )
    {
        # More than 10% RAM swapped in
        $Report =~ s/HHMMLL/high/;
        if ( $opt_s eq 'syslog' )
        {
            syslog 3, "error %s", $Report;
        } else
        {
            mylog( join( '', "$LogTime $ProgName", "[$$] ERROR: $Report" ) );
        } ## end else [ if ( $opt_s eq 'syslog'...)]
    } elsif ( $Avg > ( $TotalRAM / 20 ) )
    {
        # More than 5% RAM swapped in
        $Report =~ s/HHMMLL/medium/;
        if ( $opt_s eq 'syslog' )
        {
            syslog 4, "warning %s", $Report;
        } else
        {
            mylog(
                join( '', "$LogTime $ProgName", "[$$] WARNING: $Report" ) );
        } ## end else [ if ( $opt_s eq 'syslog'...)]
    } else
    {
        $Report =~ s/HHMMLL/low/;
        if ( $opt_s eq 'syslog' )
        {
            syslog 6, "info %s", $Report;
        } else
        {
            mylog( join( '', "$LogTime $ProgName", "[$$] INFO: $Report" ) );
        } ## end else [ if ( $opt_s eq 'syslog'...)]
    } ## end else [ if ( $Avg > ( $TotalRAM...))]
    warn "DEBUG: $LogTime $Report" if ($opt_d);

    #-------------------------------------------------
    # Also compute network interface errors and speeds
    if ( open( my $DEVSTAT, '<', '/proc/net/dev' ) )
    {
        my @lines = ();
        chomp( @lines = grep { !/lo|Inter|face/ } <$DEVSTAT> );
        close($DEVSTAT);
        foreach my $DEVLine (@lines)
        {
            next unless ( $DEVLine =~ /^\s*(\S+):\s*(.*)/o );
            my $IFName = "$1";
            my $IFData = "$2";
            next if ( $IFName eq 'lo' );
            chomp($IFData);

            warn "DEBUG: $LogTime $IFData" if ($opt_d);
            my ($RXbytes, $RXpkts,  $RXerr,   $RXdrop, $RXfifo, $RXframe,
                $RXcomp,  $RXmulti, $TXbytes, $TXpkts, $TXerr,  $TXdrop,
                $TXfifo,  $TXframe, $TXcomp,  $TXmulti
            ) = split( /\s+/, $IFData );
            warn join( '',
                "DEBUG: $LogTime",
                " $RXbytes, $RXpkts,  $RXerr,  $RXdrop, $RXfifo,  $RXframe, $RXcomp, $RXmulti,",
                " $TXbytes, $TXpkts,  $TXerr,  $TXdrop, $TXfifo,  $TXframe, $TXcomp, $TXmulti"
            ) if ($opt_d);

            my $RX_percent
                = ( $RXpkts > 0 )
                ? ( $RXerr + $RXdrop + $RXfifo + $RXcomp ) * 100 / $RXpkts
                : 0;
            $Report = sprintf "HOURLY: Receive errors for %s = %0.2f%% ",
                $IFName, $RX_percent;
            if ( $RX_percent < 0.3 )
            {
                $Report .= '(low)';
                if ( $opt_s eq 'syslog' )
                {
                    syslog 6, "info %s", $Report;
                } else
                {
                    mylog(
                        join(
                            '', "$LogTime $ProgName", "[$$] INFO: $Report"
                        )
                    );
                } ## end else [ if ( $opt_s eq 'syslog'...)]
            } elsif ( $RX_percent < 1 )
            {
                $Report .= '(low)';
                if ( $opt_s eq 'syslog' )
                {
                    syslog 6, "info %s", $Report;
                } else
                {
                    mylog(
                        join(
                            '', "$LogTime $ProgName", "[$$] INFO: $Report"
                        )
                    );
                } ## end else [ if ( $opt_s eq 'syslog'...)]
            } else
            {
                $Report .= '(high)';
                if ( $opt_s eq 'syslog' )
                {
                    syslog 6, "info %s", $Report;
                } else
                {
                    mylog(
                        join(
                            '', "$LogTime $ProgName", "[$$] INFO: $Report"
                        )
                    );
                } ## end else [ if ( $opt_s eq 'syslog'...)]
            } ## end else [ if ( $RX_percent < 0.3...)]
            warn "DEBUG: $LogTime $Report" if ($opt_d);

            # Calculate the receive speed
            if ( $RXPrevBytes_Hourly{"$IFName"} )
            {
                my $Speed = 0;
                if ( $RXPrevBytes_Hourly{"$IFName"} > $RXbytes )
                {

                    # Deal with the 4GB counter limitation of the Linux kernel
                    $Speed = (
                        (   ( 4294967296 - $RXPrevBytes_Hourly{"$IFName"} )
                            + $RXbytes
                        ) / $opt_I
                    ) * 8;
                } else
                {
                    $Speed
                        = (
                        ( $RXbytes - $RXPrevBytes_Hourly{"$IFName"} ) / 3600 )
                        * 8;
                } ## end else [ if ( $RXPrevBytes_Hourly...)]
                $Report = sprintf "HOURLY: Receive speed for %s = %s bits/s ",
                    $IFName, commify( sprintf "%.2f", $Speed );
                if ( $opt_s eq 'syslog' )
                {
                    syslog 6, "info %s", $Report;
                } else
                {
                    mylog(
                        join(
                            '', "$LogTime $ProgName", "[$$] INFO: $Report"
                        )
                    );
                } ## end else [ if ( $opt_s eq 'syslog'...)]
                warn "DEBUG: $LogTime $Report" if ($opt_d);
            } ## end if ( $RXPrevBytes_Hourly...)
            $RXPrevBytes_Hourly{"$IFName"} = $RXbytes;

            my $TX_percent
                = ( $TXpkts > 0 )
                ? ( $TXerr + $TXdrop + $TXfifo + $TXcomp ) * 100 / $TXpkts
                : 0;
            $Report = sprintf "HOURLY: Transmit errors for %s = %0.2f%% ",
                $IFName, $TX_percent;
            if ( $TX_percent < 0.3 )
            {
                $Report .= '(low)';
                if ( $opt_s eq 'syslog' )
                {
                    syslog 6, "info %s", $Report;
                } else
                {
                    mylog(
                        join(
                            '', "$LogTime $ProgName", "[$$] INFO: $Report"
                        )
                    );
                } ## end else [ if ( $opt_s eq 'syslog'...)]
            } elsif ( $TX_percent < 1 )
            {
                $Report .= '(medium)';
                if ( $opt_s eq 'syslog' )
                {
                    syslog 6, "info %s", $Report;
                } else
                {
                    mylog(
                        join(
                            '', "$LogTime $ProgName", "[$$] INFO: $Report"
                        )
                    );
                } ## end else [ if ( $opt_s eq 'syslog'...)]
            } else
            {
                $Report .= '(high)';
                if ( $opt_s eq 'syslog' )
                {
                    syslog 6, "info %s", $Report;
                } else
                {
                    mylog(
                        join(
                            '', "$LogTime $ProgName", "[$$] INFO: $Report"
                        )
                    );
                } ## end else [ if ( $opt_s eq 'syslog'...)]
            } ## end else [ if ( $TX_percent < 0.3...)]
            warn "DEBUG: $LogTime $Report" if ($opt_d);

            # Calculate the transmit speed
            if ( $TXPrevBytes_Hourly{"$IFName"} )
            {
                my $Speed = 0;
                if ( $TXPrevBytes_Hourly{"$IFName"} > $TXbytes )
                {

                    # Deal with the 4GB counter limitation of the Linux kernel
                    $Speed = (
                        (   ( 4294967296 - $TXPrevBytes_Hourly{"$IFName"} )
                            + $TXbytes
                        ) / $opt_I
                    ) * 8;
                } else
                {
                    $Speed
                        = (
                        ( $TXbytes - $TXPrevBytes_Hourly{"$IFName"} ) / 3600 )
                        * 8;
                } ## end else [ if ( $TXPrevBytes_Hourly...)]
                $Report
                    = sprintf "HOURLY: Transmit speed for %s = %s bits/s ",
                    $IFName, commify( sprintf "%.2f", $Speed );
                if ( $opt_s eq 'syslog' )
                {
                    syslog 6, "info %s", $Report;
                } else
                {
                    mylog(
                        join(
                            '', "$LogTime $ProgName", "[$$] INFO: $Report"
                        )
                    );
                } ## end else [ if ( $opt_s eq 'syslog'...)]
                warn "DEBUG: $LogTime $Report" if ($opt_d);
            } ## end if ( $TXPrevBytes_Hourly...)
            $TXPrevBytes_Hourly{"$IFName"} = $TXbytes;
        } ## end foreach my $DEVLine (@lines...)
    } ## end if ( open( my $DEVSTAT...))

    #-------------------------
    # Also compute disk speeds
    # See: http://ubuntuforums.org/showthread.php?t=31213
    if ( open( my $DEVSTAT, '<', '/proc/diskstats' ) )
    {
        my @lines = ();
        chomp( @lines = grep {/\s(?:sd|dm|nv)/} <$DEVSTAT> );
        close($DEVSTAT);
        foreach my $DEVLine (@lines)
        {
            # See: http://www.mjmwired.net/kernel/Documentation/iostats.txt
            my $DiskPart   = '';
            my $DiskReads  = 0;
            my $DiskWrites = 0;
            if ( $DEVLine
                =~ /^\s*\d+\s+\d+\s+(([a-z]d[a-z]|nvme\d+n\d+p)\d+)\s+\d+\s+\d+\s+(\d+)\s+\d+\s+\d+\s+\d+\s+(\d+)/o
                )
            {
                # [a-z]d[a-z][0-9], eg. sda1
                # or nmve[0-9]+n[0-9]+p[0-9], e.g. nvme0n1p1
                $DiskPart   = "$1";
                $DiskReads  = $2 * 512;
                $DiskWrites = $3 * 512;

            } elsif ( $DEVLine
                =~ /^\s*\d+\s+\d+\s+(dm\-\d+)\s+\d+\s+\d+\s+(\d+)\s+\d+\s+\d+\s+\d+\s+(\d+)/o
                )
            {
                # dm-[0-9], eg. dm-0
                $DiskPart   = "$1";
                $DiskReads  = $2 * 512;
                $DiskWrites = $3 * 512;
            } ## end elsif ( $DEVLine =~ ...)

            # No need to continue if we don't have a disk partition
            next unless ( length($DiskPart) );

            warn join( '',
                "DEBUG: $LogTime",
                " $DiskPart $DiskReads $DiskWrites" )
                if ($opt_d);

            # Calculate the read speed for the partition
            if ( $ReadBytes_Hourly{"$DiskPart"} )
            {
                my $Speed = 0;
                if ( $ReadBytes_Hourly{"$DiskPart"} > $DiskReads )
                {

                    # Deal with the 4GB counter limitation of the Linux kernel
                    $Speed
                        = ( ( 4294967296 - $ReadBytes_Hourly{"$DiskPart"} )
                        + $DiskReads ) / $opt_I;
                } else
                {
                    $Speed = ( $DiskReads - $ReadBytes_Hourly{"$DiskPart"} )
                        / 3600;
                } ## end else [ if ( $ReadBytes_Hourly...)]
                $Report
                    = sprintf "HOURLY: Disk read speed for %s = %s bytes/s ",
                    $DiskPart, commify( sprintf "%.2f", $Speed );
                if ( $opt_s eq 'syslog' )
                {
                    syslog 6, "info %s", $Report;
                } else
                {
                    mylog(
                        join(
                            '', "$LogTime $ProgName", "[$$] INFO: $Report"
                        )
                    );
                } ## end else [ if ( $opt_s eq 'syslog'...)]
                warn "DEBUG: $LogTime $Report" if ($opt_d);
            } ## end if ( $ReadBytes_Hourly...)
            $ReadBytes_Hourly{"$DiskPart"} = $DiskReads;

            # Calculate the write speed for the partition
            if ( $WriteBytes_Hourly{"$DiskPart"} )
            {
                my $Speed = 0;
                if ( $WriteBytes_Hourly{"$DiskPart"} > $DiskWrites )
                {

                    # Deal with the 4GB counter limitation of the Linux kernel
                    $Speed
                        = ( ( 4294967296 - $WriteBytes_Hourly{"$DiskPart"} )
                        + $DiskWrites ) / $opt_I;
                } else
                {
                    $Speed = ( $DiskWrites - $WriteBytes_Hourly{"$DiskPart"} )
                        / 3600;
                } ## end else [ if ( $WriteBytes_Hourly...)]
                $Report
                    = sprintf "HOURLY: Disk write speed for %s = %s bytes/s ",
                    $DiskPart, commify( sprintf "%.2f", $Speed );
                if ( $opt_s eq 'syslog' )
                {
                    syslog 6, "info %s", $Report;
                } else
                {
                    mylog(
                        join(
                            '', "$LogTime $ProgName", "[$$] INFO: $Report"
                        )
                    );
                } ## end else [ if ( $opt_s eq 'syslog'...)]
                warn "DEBUG: $LogTime $Report" if ($opt_d);
            } ## end if ( $WriteBytes_Hourly...)
            $WriteBytes_Hourly{"$DiskPart"} = $DiskWrites;
        } ## end foreach my $DEVLine (@lines...)
    } ## end if ( open( my $DEVSTAT...))

    unless (
        system(
            ( ($HaveTimeout) ? 'timeout 60s ' : '' )
            . 'pgrep mysqld > /dev/null 2>&1'
        )
        )
    {
        $Report = GetMysqldStats();
        if ( $opt_s eq 'syslog' )
        {
            syslog 6, "info HOURLY: %s", $Report;
        } else
        {
            mylog( join( '', "$LogTime $ProgName", "[$$] INFO: $Report" ) );
        } ## end else [ if ( $opt_s eq 'syslog'...)]
        warn "DEBUG: $LogTime $Report" if ($opt_d);
    } ## end unless ( system( ( ($HaveTimeout...))))

    # Check for any critical disk errors
    if ( -x '/usr/sbin/smartctl' )
    {
        # Disk health info is kept in an external file
        #  so that the info can survive a restart
        tie( %DiskReallocated_Hourly, 'SDBM_File', '/var/tmp/DiskHealth',
            O_RDWR | O_CREAT, 0640 )
            or die
            "ERROR: Can't create or connect to disk health stats: $!\n";

        my @disks = ();
        # Get the list of disks to check directly from smartctl
        if (open(
                my $GDL,
                '-|',
                ( ($HaveTimeout) ? 'timeout 60s ' : '' )
                    . '/usr/sbin/smartctl --scan'
            )
            )
        {
            my @lines = ();
            chomp( @lines = <$GDL> );
            close($GDL);
            foreach (@lines)
            {
                if (/^([^#]+)/o)
                {
                    my $Disk = "$1";
                    # Remove '-d scsi' from disk name
                    $Disk =~ s/ -d scsi//;
                    # Trim leading and trailing spaces from the disk spec
                    $Disk =~ s/\s+$//;
                    $Disk =~ s/^\s+//;
                    push( @disks, "$Disk" );
                } ## end if (/^([^#]+)/o)
            } ## end foreach (@lines)
        } ## end if ( open( my $GDL, '-|'...))

        my $ArraySize       = @disks;
        my $DiskTemp_Report = '';
        for ( my $i = 0; $i < $ArraySize; $i++ )
        {
            my $Disk = $disks[$i];

            # The default temperature thresholds apply to standard hard disks
            my $ColdThreshold = 20;
            my $HotThreshold  = 45;
            if ( $Disk =~ /nvme/o )
            {
                # SSDs can be colder and hotter than hard disks
                $ColdThreshold = 10;
                $HotThreshold  = 60;
            } else
            {
                if ( $Disk =~ /[a-z]d[a-z]/o )
                {
                    my $Device = $Disk;
                    $Device =~ s@/dev/@@;
                    if (open(
                            my $DM, '<', "/sys/block/$Device/device/model"
                        )
                        )
                    {
                        my @lines = ();
                        chomp( @lines = <$DM> );
                        close($DM);

                        # Skip emulated disks
                        next if ( $lines[0] =~ /QEMU/o );

                        if ( $lines[0] =~ /RS3DC080|SSD/o )
                        {
                            # SSDs can be colder and hotter than hard disks
                            $ColdThreshold = 10;
                            $HotThreshold  = 60;
                        } ## end if ( $lines[0] =~ /RS3DC080|SSD/o...)
                    } ## end if ( open( my $DM, '<'...))
                } else
                {

                    if (open(
                            my $DM,
                            '-|',
                            ( ($HaveTimeout) ? 'timeout 60s ' : '' )
                                . "/usr/sbin/smartctl -i $Disk"
                        )
                        )
                    {
                        my @lines = ();
                        chomp(
                            @lines = grep {
                                /^Rotation\s+Rate:\s+Solid\s+State\s+Device/
                            } <$DM>
                        );
                        close($DM);

                        if ( scalar @lines )
                        {
                            # SSDs can be colder and hotter than hard disks
                            $ColdThreshold = 10;
                            $HotThreshold  = 60;
                        } ## end if ( scalar @lines )
                    } ## end if ( open( my $DM, '-|'...))
                } ## end else [ if ( $Disk =~ /[a-z]d[a-z]/o...)]
            } ## end else [ if ( $Disk =~ /nvme/o ...)]

            # Get some attributes for the disk
            if (open(
                    my $GDA,
                    '-|',
                    ( ($HaveTimeout) ? 'timeout 60s ' : '' )
                        . "/usr/sbin/smartctl -A $Disk"
                )
                )
            {
                my @lines = ();
                chomp( @lines
                        = grep {/^(?:19[48]|\s+5|Temperature:)\s/} <$GDA> );
                close($GDA);
                foreach (@lines)
                {
                    my @Cols = split();
                    if ( $Cols[0] == 194 )
                    {
                        # We watch the "Temperature_Celsius" parameter
                        # See https://hdsentinel.com/blog/safe-hard-drive-temp
                        $Report = '';
                        my $CurTemp = $Cols[9];
                        if ( $CurTemp < $ColdThreshold )
                        {
                            $Report
                                = "Disk '$Disk' seems cold ($CurTemp C is below $ColdThreshold C)";
                            $DiskTemp_Report .= "$Report\n";
                        } elsif ( $CurTemp > $HotThreshold )
                        {
                            $Report
                                = "Disk '$Disk' is too hot ($CurTemp C is above $HotThreshold C) - investigate";
                            $DiskTemp_Report .= "$Report\n";
                        } else
                        {
                            $Report
                                = "Disk '$Disk' has an acceptable temperature ($CurTemp C is between $ColdThreshold C and $HotThreshold C)";
                            $DiskTemp_Report .= "$Report\n";
                        } ## end else [ if ( $CurTemp < $ColdThreshold...)]
                        if ( $opt_s eq 'syslog' )
                        {
                            syslog 6, "info HOURLY: %s", $Report;
                        } else
                        {
                            mylog(
                                join( '',
                                    "$LogTime $ProgName",
                                    "[$$] INFO: $Report" )
                            );
                        } ## end else [ if ( $opt_s eq 'syslog'...)]
                        warn "DEBUG: $LogTime $Report" if ($opt_d);
                    } elsif ( $Cols[0] == 'Temperature:' )
                    {
                        # We watch the "Temperature:" parameter
                        # See https://hdsentinel.com/blog/safe-hard-drive-temp
                        $Report = '';
                        my $CurTemp = $Cols[1];
                        if ( $CurTemp < $ColdThreshold )
                        {
                            $Report
                                = "Disk '$Disk' seems cold ($CurTemp C is below $ColdThreshold C)";
                            $DiskTemp_Report .= "$Report\n";
                        } elsif ( $CurTemp > $HotThreshold )
                        {
                            $Report
                                = "Disk '$Disk' is too hot ($CurTemp C is above $HotThreshold C) - investigate";
                            $DiskTemp_Report .= "$Report\n";
                        } else
                        {
                            $Report
                                = "Disk '$Disk' has an acceptable temperature ($CurTemp C is between $ColdThreshold C and $HotThreshold C)";
                            $DiskTemp_Report .= "$Report\n";
                        } ## end else [ if ( $CurTemp < $ColdThreshold...)]
                        if ( $opt_s eq 'syslog' )
                        {
                            syslog 6, "info HOURLY: %s", $Report;
                        } else
                        {
                            mylog(
                                join( '',
                                    "$LogTime $ProgName",
                                    "[$$] INFO: $Report" )
                            );
                        } ## end else [ if ( $opt_s eq 'syslog'...)]
                        warn "DEBUG: $LogTime $Report" if ($opt_d);
                    } elsif ( $Cols[0] == 198 )
                    {
                        # We watch the "Offline_Uncorrectable" parameter
                        $Report = '';
                        my $DiskError = $Cols[9];
                        if ( $DiskError > 0 )
                        {
                            my $Disk_sys = $Disk;
                            $Disk_sys =~ s@/dev/@/sys/block/@;
                            my $TotalSectors = 1;    # Some default value
                            if ( open( my $DS, '<', "$Disk_sys/size" ) )
                            {
                                $TotalSectors = <$DS>;
                                chomp($TotalSectors);
                            } ## end if ( open( my $DS, '<'...))
                            if ( $DiskError > ( $TotalSectors / 100 ) )
                            {
                                # More than 1% of all sectors are bad
                                $Report
                                    = "Disk '$Disk' has $DiskError uncorrectable sectors - replace it";
                                # This is important - send an email!
                                SendEmail( $Report, 'high' );
                            } else
                            {
                                $Report
                                    = "Disk '$Disk' has $DiskError uncorrectable sectors";
                            } ## end else [ if ( $DiskError > ( $TotalSectors...))]
                        } else
                        {
                            $Report = "Disk '$Disk' has no critical errors";
                        } ## end else [ if ( $DiskError > 0 ) ]
                        if ( $opt_s eq 'syslog' )
                        {
                            syslog 6, "info HOURLY: %s", $Report;
                        } else
                        {
                            mylog(
                                join( '',
                                    "$LogTime $ProgName",
                                    "[$$] INFO: $Report" )
                            );
                        } ## end else [ if ( $opt_s eq 'syslog'...)]
                        warn "DEBUG: $LogTime $Report" if ($opt_d);
                    } elsif ( $Cols[0] == 5 )
                    {
                        # We watch the "Reallocated_Sector_Ct" parameter
                        $Report = '';
                        my $DiskError = $Cols[9];
                        if ( $DiskError > $DiskReallocated_Hourly{"$Disk"} )
                        {
                            $Report = join(
                                '',
                                "Disk '$Disk' has $DiskError (",
                                (         $DiskError
                                        - $DiskReallocated_Hourly{"$Disk"}
                                ),
                                " more than last hour) reallocated errors - investigate"
                            );
                            # This is important - send an email!
                            SendEmail( $Report, 'high' );
                        } else
                        {
                            $Report
                                = "Disk '$Disk' has no increase in reallocated errors";
                        } ## end else [ if ( $DiskError > $DiskReallocated_Hourly...)]
                        $DiskReallocated_Hourly{"$Disk"} = $DiskError;
                        if ( $opt_s eq 'syslog' )
                        {
                            syslog 6, "info HOURLY: %s", $Report;
                        } else
                        {
                            mylog(
                                join( '',
                                    "$LogTime $ProgName",
                                    "[$$] INFO: $Report" )
                            );
                        } ## end else [ if ( $opt_s eq 'syslog'...)]
                        warn "DEBUG: $LogTime $Report" if ($opt_d);
                    } ## end elsif ( $Cols[0] == 5 )
                } ## end foreach (@lines)

            } ## end if ( open( my $GDA, '-|'...))
        } ## end for ( my $i = 0; $i < $ArraySize...)

        if ( $DiskTemp_Report =~ /cold|hot/o )
        {
            # This is important - send an email!
            SendEmail( $DiskTemp_Report, 'medium' );
        } ## end if ( $DiskTemp_Report ...)
        untie(%DiskReallocated_Hourly);
    } else
    {
        # Delete any potential disk health files
        unlink glob('/var/tmp/DiskHealth.*');
    } ## end else [ if ( -x '/usr/sbin/smartctl'...)]

    # Check for any changes in installed packages
    my $rc = 0;
    if ( -x '/usr/bin/dpkg' )
    {
        $rc = system('dpkg --verify > /tmp/package.changes.txt 2>&1');
    } else
    {
        $rc = system('rpm -Va > /tmp/package.changes.txt 2>&1');
    } ## end else [ if ( -x '/usr/bin/dpkg'...)]
    if ( ($rc) && ( -s '/tmp/package.changes.txt' ) )
    {
        # Send an email with the changed packages
        my $Report
            = "WARNING: One or more packages might have been changed since install or last update:\n\n";
        if ( open( my $PC, '<', '/tmp/package.changes.txt' ) )
        {
            $Report .= do { local $/; <$PC> };
            close($PC);
        } ## end if ( open( my $PC, '<'...))
        $Report .= "\n\nTo fix 'md5sum' files, run this command as 'root':\n"
            . "sed -i '/^\\s*\$/d' /var/lib/dpkg/info/*.md5sums\n";
        SendEmail( $Report, 'normal' );
    } ## end if ( ($rc) && ( -s '/tmp/package.changes.txt'...))

    # Start the next reporting cycle
    $VMLNo_Hourly    = 3;
    $RunQueue_Hourly = 0;
    $BlockedP_Hourly = 0;
    $PageIn_Hourly   = 0;
    $CPUWait_Hourly  = 0;

    untie(%DiskReallocated_Hourly);
    return (0);
} ## end sub ReportHourly

#--------------------------------------------------------------------
# Create a daily report
#--------------------------------------------------------------------
sub ReportDaily()
{
    if ( $opt_s ne 'syslog' )
    {
        my $LogTime = strftime( "%F %T", localtime );
        if ( $opt_s =~ /^(\/(?:tmp|var\/log)\/\S+)/ )
        {
            $LogFile = "$1";
        } ## end if ( $opt_s =~ /^(\/(?:tmp|var\/log)\/\S+)/...)
        die "ERROR: Logfile must be in /tmp or in /var/log\n"
            unless ( length($LogFile) );

        # Close and rotate existing log file
        warn "DEBUG: $LogTime Rotating $LogFile" if ($opt_d);
        mylog(
            join( '', "$LogTime $ProgName", "[$$] INFO: Rotating $LogFile" )
        );

        my ( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst )
            = localtime();
        my $Yesterday = strftime "%F", $sec, $min, $hour, $mday - 1, $mon,
            $year, $wday, $yday, $isdst;
        rename( "$LogFile", "$LogFile" . '_' . "$Yesterday" ) or die "$@";

        # Start a new log file
        warn "DEBUG: $LogTime Logging to $LogFile" if ($opt_d);
        mylog(
            join( '',
                "$LogTime $ProgName",
                "[$$] INFO: Starting new $LogFile" )
        );
    } ## end if ( $opt_s ne 'syslog'...)

    return (0);
} ## end sub ReportDaily

#--------------------------------------------------------------------
# Main function
#--------------------------------------------------------------------
$|++;

# Get possible options
getopts('a:dhi:Dm:s:o:I:P:Q:v') or ShowUsage();
ShowUsage() if ( ($opt_h) or ( $opt_i <= 0 ) );
print "$ProgName\n$CopyRight\n\n" if ($opt_v);

# Ensure that only one instance is running at all times
open( my $PSList, '-|',
    ( ($HaveTimeout) ? 'timeout 60s ' : '' ) . "ps -wefH | grep 'Sys[M]on'" )
    or die "ERROR: Can not get process list: $!";
while (<$PSList>)
{
# Example: root       82581       1  0 08:41 ?        00:00:00   /usr/local/sbin/SysMon.pl
    chomp();
    my (@part) = split(/\s+/);
    warn "PID = $part[1], Command = $part[7]" if ($opt_d);
    if ( $part[7] =~ m[/usr/local/sbin/SysMon.pl]o )
    {
        print
            "$ProgName already running with PID $part[1] (since $part[4])\n";
        exit(0);
    } ## end if ( $part[7] =~ m[/usr/local/sbin/SysMon.pl]o...)
} ## end while (<$PSList>)
close($PSList);

if ($opt_Q)
{
    # Check validity of 'quiet' hours
    if ( $opt_Q =~ /^[\d,]+/o )
    {
        @QuietHours = split( /,/, $opt_Q );
    } else
    {
        warn
            "'$opt_Q' does not contain valid hours - 'quiet' hours disabled!\n";
    } ## end else [ if ( $opt_Q =~ /^[\d,]+/o...)]
} ## end if ($opt_Q)

if ($opt_a)
{
# Check validity of alert email address
# see: http://www.rcamilleri.com/blog/perl-validate-email-addresses-using-regex/
    if (   ( $opt_a !~ /^(\w|\-|\_|\.)+\@((\w|\-|\_)+\.)+[a-zA-Z]{2,}$/ )
        || ( $opt_a =~ /\.@|\.\./ ) )
    {
        warn "'$opt_a' is not a valid email address - disabling alerts!\n";
        $opt_a = '';
    } ## end if ( ( $opt_a !~ ...))
} ## end if ($opt_a)

if ($opt_o)
{
    # Check whether output path for '.csv' files exists
    if ( !-d $opt_o )
    {
        warn
            "Output path '$opt_o' does not exist - disabling creation of '.csv' files\n";
        $opt_o = '';
    } ## end if ( !-d $opt_o )
} ## end if ($opt_o)

# Determine the number of processors in the system
open( my $CPUS, '<', '/proc/cpuinfo' )
    or die "ERROR: Can not determine number of processors\n";
while ( my $CPULine = <$CPUS> )
{
    $CPUNo++ if ( $CPULine =~ /^processor/o );
} ## end while ( my $CPULine = <$CPUS>...)
close($CPUS);
warn join( '',
    'DEBUG: ',
    strftime( "%F %T", localtime ),
    " Number of processors = $CPUNo" )
    if ($opt_d);
die "ERROR: No processors found\n" unless ($CPUNo);
$ReportTail_CPU = sprintf "(HHMMLL for %d processor%s)", $CPUNo,
    ( ( $CPUNo > 1 ) ? 's' : '' );

# Determine the amount of RAM in the system
open( my $MEMINFO, '<', '/proc/meminfo' )
    or die "ERROR: Can not determine amount of RAM\n";
while ( my $MemLine = <$MEMINFO> )
{
    if ( $MemLine =~ /^MemTotal:\s+(\d+)/o )
    {
        $TotalRAM = $1;
        last;
    } ## end if ( $MemLine =~ /^MemTotal:\s+(\d+)/o...)
} ## end while ( my $MemLine = <$MEMINFO>...)
close($MEMINFO);
warn join( '',
    'DEBUG: ',
    strftime( "%F %T", localtime ),
    " Amount of RAM = $TotalRAM KB" )
    if ($opt_d);
die "ERROR: No RAM found\n" unless ($TotalRAM);
$ReportTail_RAM = sprintf "(HHMMLL for %d KB)", $TotalRAM;

# Become a daemon process (if specified)
Daemonize if ($opt_D);

# Write the PID if necessary
my $PID_Path = '';
if ( -d '/var/run' )
{
    $PID_Path = '/var/run';
} elsif ( -d '/run' )
{
    $PID_Path = '/run';
} ## end elsif ( -d '/run' )
if ( length($PID_Path) )
{
    my $CurPID = -1;
    if ( open( my $PF, '<', "$PID_Path/SysMon.pid" ) )
    {
        $CurPID = <$PF>;
        chomp($CurPID);
        close($PF);
    } ## end if ( open( my $PF, '<'...))
    if ( $CurPID != $$ )
    {
        if ( open( my $PF, '>', "$PID_Path/SysMon.pid" ) )
        {
            print $PF "$$\n";
            close($PF);
        } ## end if ( open( my $PF, '>'...))
    } ## end if ( $CurPID != $$ )
} ## end if ( length($PID_Path)...)

# Create the plugin directory (if possible and specified)
if ($opt_P)
{
    system( ( ($HaveTimeout) ? 'timeout 60s ' : '' ) . 'mkdir',
        '-m', '0700', '-p', "$opt_P" );
    my $uid = getpwnam 'nobody';
    chown $uid, -1, $opt_P;
    my $mode       = ( stat($opt_P) )[2];
    my $mode_octal = sprintf "%04o", $mode & 07777;
    unless ( $mode_octal =~ /^0700$/ )
    {
        die
            "ERROR: Plugin directory '$opt_P' has incorrect permissions '$mode_octal'\n";
    } ## end unless ( $mode_octal =~ /^0700$/...)
    my $owner = ( stat($opt_P) )[4];
    unless ( $owner == $uid )
    {
        die
            "ERROR: Plugin directory '$opt_P' is owned by incorrect owner '$owner'\n";
    } ## end unless ( $owner == $uid )
} ## end if ($opt_P)

open( my $VMS, '-|', "$VMSTAT -n $opt_i" )
    or die "ERROR: Can not find or start 'vmstat': $!";

# Create the correct output for logging
if ( $opt_s eq 'syslog' )
{
    warn join( '',
        'DEBUG: ',
        strftime( "%F %T", localtime ),
        " Logging to syslog via 'daemon' facility" )
        if ($opt_d);
    openlog "$ProgName", 'ndelay,pid', 'daemon';
} else
{
    if ( $opt_s =~ /^(\/(?:tmp|var\/log)\/\S+)/ )
    {
        $LogFile = "$1";
    } ## end if ( $opt_s =~ /^(\/(?:tmp|var\/log)\/\S+)/...)
    die "ERROR: Logfile must be in /tmp or in /var/log\n"
        unless ( length($LogFile) );
    warn join( '',
        'DEBUG: ',
        strftime( "%F %T", localtime ),
        " Logging to $LogFile" )
        if ($opt_d);
} ## end else [ if ( $opt_s eq 'syslog'...)]

### Kludge to restore remote access to a specific host
if ( $MHOST eq 'pxarc.digitaltowpath.org' )
{
    system(
        ( ($HaveTimeout) ? 'timeout 60s ' : '' ) . '/usr/sbin/firehol stop' );
} ## end if ( $MHOST eq 'pxarc.digitaltowpath.org'...)

# Constantly get the input from "vmstat"
while ( my $VMLine = <$VMS> )
{
    $VMLNo_Interval++;
    $VMLNo_Hourly++;
    warn join( '',
        'DEBUG: ',
        strftime( "%F %T", localtime ),
        " vmstat line number = $VMLNo_Interval" )
        if ($opt_d);

    # Skip 1st three lines of output
    next unless ( $VMLNo_Interval > 3 );

    $VMLine =~ s/^\s+//;
    warn join( '',
        'DEBUG: ',
        strftime( "%F %T", localtime ),
        " VMLine = $VMLine" )
        if ($opt_d);
    my ($RQ,   $BP,   undef, undef, undef, undef, $PI,   undef,
        undef, undef, undef, undef, undef, undef, undef, $WA
    ) = ( split( /\s+/, $VMLine ) );
    # Update the counters for interval logging
    $RunQueue_Interval += $RQ;
    $BlockedP_Interval += $BP;
    $PageIn_Interval   += $PI;
    $CPUWait_Interval  += $WA;
    # Update the counters for hourly report
    $RunQueue_Hourly += $RQ;
    $BlockedP_Hourly += $BP;
    $PageIn_Hourly   += $PI;
    $CPUWait_Hourly  += $WA;
    warn join( '',
        'DEBUG: ',
        strftime( "%F %T", localtime ),
        " RunQueue = $RQ, Blocked processes = $BP, Paged in memory = $PI, CPUWait = $CPUWait_Interval"
    ) if ($opt_d);

    # Check for possible malware
    if ( -d '/usr/bin/dpkgd' )
    {
        # Send an email alert
        SendEmail( "ALERT: Possible malware found, '/usr/bin/dpkgd' exists",
            'high' );
        warn join( '',
            strftime( "%F %T", localtime ),
            " Possible malware found, '/usr/bin/dpkgd' exists" )
            if ($opt_d);
    } ## end if ( -d '/usr/bin/dpkgd'...)

    # Check for adminstrative and malware accounts
    my $AdminAcct = 0;
    my $AAName    = '';
    while (
        my ( $name, undef, $uid, $gid, undef, undef, $gcos, $udir, $ushell )
        = getpwent() )
    {
        if ( ( $uid == 0 ) or ( $gid == 0 ) )
        {
            $AAName .= "$name ($gcos, $ushell) ";
            $AdminAcct++;
        } ## end if ( ( $uid == 0 ) or ...)
        if ( $name =~ /^(?:setup|tor|admin)$/io )
        {
            # Send an email alert
            SendEmail(
                "ALERT: Possible malware found, account '$name' exists",
                'high' );
            warn join( '',
                'DEBUG: ',
                strftime( "%F %T", localtime ),
                " Possible malware found, account '$name' exists" )
                if ($opt_d);
        } ## end if ( $name =~ /^(?:setup|tor|admin)$/io...)

        # Also check for any changes to 'authorized_keys' ssh files
        my $FName = "$udir/.ssh/authorized_keys";
        if ( -s $FName )
        {
            if ( open( my $fh, '<', $FName ) )
            {
                binmode($fh);
                my $md5 = Digest::MD5->new;
                while (<$fh>)
                {
                    $md5->add($_);
                } ## end while (<$fh>)
                close($fh);
                my $FSum = $md5->hexdigest;

                my $Report = sprintf "MD5 checksum for %s is %s", $FName,
                    $FSum;
                if ($opt_o)
                {
                    # Also write data into corresponding '.csv' file
                    my ($outfile) = "$opt_o/Checksum.csv" =~ /^([^\0]+)$/;
                    if ( open( my $CSV, '>>', $outfile ) )
                    {
                        my $Now = strftime( "%F,%T", localtime );
                        printf $CSV "%s,%s,%s\n", $Now, $FName, $FSum;
                        close($CSV);
                    } ## end if ( open( my $CSV, '>>'...))
                } ## end if ($opt_o)

                if ( exists $md5_Sum{"$FName"} )
                {
                    unless ( $md5_Sum{"$FName"} =~ /^$FSum$/ )
                    {
                        $Report = join( '',
                            "Changed MD5 checksum for $FName: ",
                            $md5_Sum{"$FName"}, " => $FSum" );
                        if ( $opt_s eq 'syslog' )
                        {
                            syslog 3, "error %s", $Report;
                        } else
                        {
                            mylog(
                                join( '',
                                    "$LogTime $ProgName",
                                    "[$$] ERROR: $Report" )
                            );
                        } ## end else [ if ( $opt_s eq 'syslog'...)]
                        if ($opt_a)
                        {
                            # Also send an email alert
                            SendEmail( "ERROR: $Report", 'normal' );
                        } ## end if ($opt_a)
                    } ## end unless ( $md5_Sum{"$FName"...})
                } ## end if ( exists $md5_Sum{"$FName"...})
                $md5_Sum{"$FName"} = "$FSum";
            } ## end if ( open( my $fh, '<'...))
        } ## end if ( -s $FName )
    } ## end while ( my ( $name, undef...))

    if ( $AdminAcct > 1 )
    {
        # Send an email alert
        SendEmail(
            "ERROR: More than one administrative accounts found: $AAName",
            'normal' );
        warn join( '',
            'DEBUG: ',
            strftime( "%F %T", localtime ),
            " More than one administrative accounts found: $AAName" )
            if ($opt_d);
    } ## end if ( $AdminAcct > 1 )

    # Check for a change of the MAC address for the default gateway
    my $DefaultGateway_IP      = '';
    my $DefaultGateway_MAC_Cur = '';
    if (open(
            my $IP,
            '-|',
            ( ($HaveTimeout) ? 'timeout 60s ' : '' ) . 'ip route get 8.8.8.8'
        )
        )
    {
        my @lines = ();
        chomp( @lines = grep {/via\s/} <$IP> );
        close($IP);
        foreach (@lines)
        {
            if (/via\s+(\S+)/o)
            {
                # We have the IP address
                $DefaultGateway_IP = "$1";
                last;
            } ## end if (/via\s+(\S+)/o)
        } ## end foreach (@lines)
        close($IP);
    } ## end if ( open( my $IP, '-|'...))
    if ( $DefaultGateway_IP ne '' )
    {
        if (open(
                my $ARP, '-|',
                ( ($HaveTimeout) ? 'timeout 60s ' : '' ) . 'arp -n'
            )
            )
        {

            my @lines = ();
            chomp( @lines = grep { !/^Address/ } <$ARP> );
            close($ARP);
            foreach (@lines)
            {
                if (/^$DefaultGateway_IP\s+\S+\s+(\S+)/o)
                {
                    # We have the MAC address
                    $DefaultGateway_MAC_Cur = "$1";
                    last;
                } ## end if (/^$DefaultGateway_IP\s+\S+\s+(\S+)/o...)
            } ## end foreach (@lines)
        } ## end if ( open( my $ARP, '-|'...))
        if ( $DefaultGateway_MAC_Cur ne '' )
        {
            # Get the previous MAC address
            if ($opt_o)
            {
                # Also write data into corresponding '.csv' file
                my ($outfile)
                    = "$opt_o/DefaultGateway_MAC.csv" =~ /^([^\0]+)$/;
                if ( open( my $CSV, '>>', $outfile ) )
                {
                    my $Now = strftime( "%F,%T", localtime );
                    printf $CSV "%s,%s,%s\n", $Now, $DefaultGateway_IP,
                        $DefaultGateway_MAC_Cur;
                    close($CSV);
                } ## end if ( open( my $CSV, '>>'...))
            } ## end if ($opt_o)
            if ( $DefaultGateway_MAC_Prev ne '' )
            {
                # Compare the current MAC address to the previous one
                if ( $DefaultGateway_MAC_Cur ne $DefaultGateway_MAC_Prev )
                {
                    my $Report
                        = "Changed MAC address for default gateway from '$DefaultGateway_MAC_Prev' to '$DefaultGateway_MAC_Cur'";
                    if ( $opt_s eq 'syslog' )
                    {
                        syslog 3, "error %s", $Report;
                    } else
                    {
                        mylog(
                            join( '',
                                "$LogTime $ProgName",
                                "[$$] ERROR: $Report" )
                        );
                    } ## end else [ if ( $opt_s eq 'syslog'...)]
                    if ($opt_a)
                    {
                        # Also send an email alert
                        SendEmail( "ERROR: $Report", 'high' );
                    } ## end if ($opt_a)
                } ## end if ( $DefaultGateway_MAC_Cur...)
            } ## end if ( $DefaultGateway_MAC_Prev...)

            # Save the current MAC address of the default gateway
            $DefaultGateway_MAC_Prev = $DefaultGateway_MAC_Cur;
        } ## end if ( $DefaultGateway_MAC_Cur...)
    } ## end if ( $DefaultGateway_IP...)

    # Check NFS client stats (if any NFS shares are mounted)
    my $NFSMounts = 0;
    if ( open( my $NC, '<', '/proc/mounts' ) )
    {
        my @lines = ();
        chomp( @lines = grep {/ nfs/} <$NC> );
        close($NC);
        $NFSMounts = scalar(@lines);
    } ## end if ( open( my $NC, '<'...))
    if ( $NFSMounts > 0 )
    {
        # This is a NFS client
        my %NFSStats  = ();
        my $EmailBody = '';
        if (open(
                my $NFSSTAT,
                '-|',
                ( ($HaveTimeout) ? 'timeout 60s ' : '' ) . 'nfsstat -co rpc'
            )
            )
        {
            # Check the "retrans" value
            my @lines = ();
            chomp( @lines = grep {/^\s*\d+/} <$NFSSTAT> );
            close($NFSSTAT);
            foreach (@lines)
            {
                if (/^\s*(\d+)\s+(\d+)/o)
                {
                    my $NFS_Calls   = $1;
                    my $NFS_Retrans = $2;
                    $NFSStats{"retrans"} = $NFS_Retrans;
                    my $NFS_OldRetrans
                        = exists( $NFSStats{"old_retrans"} )
                        ? $NFSStats{"old_retrans"}
                        : -1;
                    # Remember the current value for next check
                    $NFSStats{"old_retrans"} = $NFS_Retrans;
                    if ( $opt_s eq 'syslog' )
                    {
                        syslog 5,
                            "info %s NFS retransmissions: now=%d, previous=%d",
                            $MHOST,
                            $NFS_Retrans,
                            $NFSStats{"old_retrans"};
                    } else
                    {
                        mylog(
                            join( '',
                                strftime( "%F %T", localtime ),
                                " $ProgName",
                                "[$$] INFO: NFS retransmissions: now=$NFS_Retrans, previous=$NFSStats{'old_retrans'}"
                            )
                        );
                    } ## end else [ if ( $opt_s eq 'syslog'...)]

                    # Check for a change in retransmissions
                    if (    ( $NFS_OldRetrans >= 0 )
                        and ( $NFS_Retrans != $NFS_OldRetrans ) )
                    {
                        my $Change
                            = ( $NFS_Retrans > $NFS_OldRetrans )
                            ? 'increased'
                            : 'decreased';
                        # Issue an error message
                        $EmailBody
                            .= "ERROR: NFS retransmissions $Change from $NFS_OldRetrans to $NFS_Retrans\n";
                    } ## end if ( ( $NFS_OldRetrans...))
                    last;
                } ## end if (/^\s*(\d+)\s+(\d+)/o...)
            } ## end foreach (@lines)
        } ## end if ( open( my $NFSSTAT...))
        if ( open( my $MS, '<', '/proc/self/mountstats' ) )
        {
            my @lines = ();
            chomp( @lines = <$MS> );
            close($MS);
            my $MountPoint = '';
            my $EmailBody  = '';
            my %MountPointDone;
            foreach my $MSLINE (@lines)
            {
                if ( $MSLINE
                    =~ /^device\s+(\S+)\s+mounted\s+on\s+(\S+)\s+with\s+fstype\s+nfs/o
                    )
                {
                    # Save the server info for the mountpoint
                    $MountPoint = "$2";
                    $NFSStats{"$MountPoint"} = "export=$1";
                    next;
                } ## end if ( $MSLINE =~ ...)
# parameters: see https://utcc.utoronto.ca/~cks/space/blog/linux/NFSMountstatsBytesEvents
                if ( $MSLINE
                    =~ /bytes:\s+\d+\s+\d+\s+\d+\s+\d+\s+(\d+)\s+(\d+)/o )
                {
                    # Save the NFS read and write for the mountpoint
                    $NFSStats{"$MountPoint"} .= " nfsread=$1 nfswrite=$2";
                    next;
                } ## end if ( $MSLINE =~ ...)
# parameters: see https://utcc.utoronto.ca/~cks/space/blog/linux/NFSMountstatsBytesEvents
                if ( $MSLINE =~ /events:\s+/o )
                {
                    next;
                } ## end if ( $MSLINE =~ /events:\s+/o...)
# parameters: see https://utcc.utoronto.ca/~cks/space/blog/linux/NFSMountstatsXprt
                if ( $MSLINE =~ /xprts:\s+/o )
                {
                    next;
                } ## end if ( $MSLINE =~ /xprts:\s+/o...)
# parameters: see https://utcc.utoronto.ca/~cks/space/blog/linux/NFSMountstatsNFSOps
                if ( $MSLINE =~ /(WRITE|READ):\s+\d+\s+\d+\s+(\d+)/o )
                {
                    my $NFSOp         = "$1";
                    my $NFSCurTimeout = $2;
                    my $NFSOldTimeout
                        = exists( $NFSTimeouts{"$NFSOp-$MountPoint"} )
                        ? $NFSTimeouts{"$NFSOp-$MountPoint"}
                        : -1;
                    $NFSTimeouts{"$NFSOp-$MountPoint"} = $NFSCurTimeout;
                    if ( $opt_s eq 'syslog' )
                    {
                        syslog 5,
                            "info %s NFS %s timeouts for '%s': now=%d, previous=%d",
                            $MHOST, $NFSOp, $MountPoint,
                            $NFSCurTimeout,
                            $NFSOldTimeout;
                    } else
                    {
                        mylog(
                            join( '',
                                strftime( "%F %T", localtime ),
                                " $ProgName",
                                "[$$] INFO: NFS $NFSOp timeouts for '$MountPoint': now=$NFSCurTimeout, previous=$NFSOldTimeout"
                            )
                        );
                    } ## end else [ if ( $opt_s eq 'syslog'...)]

                    if ( exists( $MountPointDone{"$MountPoint-$NFSOp"} ) )
                    {
                        # Now need to check again for the same issue
                        next;
                    } ## end if ( exists( $MountPointDone...))

                    # Check for a change in timeouts
                    if (    ( $NFSOldTimeout >= 0 )
                        and ( $NFSCurTimeout != $NFSOldTimeout ) )
                    {
                        my $Change
                            = ( $NFSCurTimeout > $NFSOldTimeout )
                            ? 'increased'
                            : 'decreased';

                        # Issue an error message
                        $EmailBody
                            .= "ERROR: NFS mount point '$MountPoint' '$NFSOp' timeouts $Change from $NFSOldTimeout to $NFSCurTimeout\n";
                        if ( $opt_s eq 'syslog' )
                        {
                            syslog 5,
                                "warning %s NFS mount point %s %s timeout %s %s from %d to %d",
                                $MHOST, $MountPoint, $NFSOp, $Change,
                                $NFSOldTimeout, $NFSCurTimeout;
                        } else
                        {
                            mylog(
                                join( '',
                                    strftime( "%F %T", localtime ),
                                    " $ProgName"
                                        . "[$$] WARNING: NFS mount point '$MountPoint' '$NFSOp' timeouts $Change from $NFSOldTimeout to $NFSCurTimeout"
                                )
                            );
                        } ## end else [ if ( $opt_s eq 'syslog'...)]
                        $MountPointDone{"$MountPoint-$NFSOp"} = 1;
                    } ## end if ( ( $NFSOldTimeout ...))
                    if ( ($opt_d) and ( $NFSOp eq 'WRITE' ) )
                    {
                        warn join( '',
                            'DEBUG: ',
                            strftime( "%F %T", localtime ),
                            " NFS stats = ",
                            $NFSStats{"$MountPoint"} );
                    } ## end if ( ($opt_d) and ( $NFSOp...))
                    next;
                } ## end if ( $MSLINE =~ /(WRITE|READ):\s+\d+\s+\d+\s+(\d+)/o...)
            } ## end foreach my $MSLINE (@lines)

            # Email any error messages that we might have found along the way
            SendEmail( "$EmailBody", 'normal' ) if ( length($EmailBody) );
        } ## end if ( open( my $MS, '<'...))
    } ## end if ( $NFSMounts > 0 )

    if ( $VMLNo_Interval == ( ( $opt_I / $opt_i ) + 3 ) )
    {
        # Create the standard report per interval
        ReportInterval();

        my ( undef, $CurMin, $CurHour ) = localtime();
        if ( $CurMin < ( $opt_I / 60 ) )
        {
            # Create an hourly report
            ReportHourly();

            # Create a daily report at midnight
            ReportDaily() if ( $CurHour == 0 );
        } ## end if ( $CurMin < ( $opt_I...))
    } ## end if ( $VMLNo_Interval ==...)

    # Run plugins (if present)
    if ($opt_P)
    {
        warn join( '',
            'DEBUG: ',
            strftime( "%F %T", localtime ),
            " Plugin directory '$opt_P' specified" )
            if ($opt_d);
        # Just to be safe!
        chmod 0700, "$opt_P";
        if ( opendir( my $DIR, $opt_P ) )
        {
            warn join( '',
                'DEBUG: ',
                strftime( "%F %T", localtime ),
                " Executing plugins in '$opt_P'" )
                if ($opt_d);
            while ( my $plugin = readdir($DIR) )
            {
                # Use the absolute path
                $plugin = "$opt_P/$plugin";

                # Ignore non executable files
                next unless ( -x $plugin );
                # Ignore directories
                next if ( -d $plugin );
                # Ignore any file starting with "."
                next if ( $plugin =~ /^\./o );

                # Untaint the variable
                my ($file) = $plugin =~ m/^([\/A-Z0-9_.-]+)$/ig;
                warn join( '',
                    'DEBUG: ',
                    strftime( "%F %T", localtime ),
                    " Executing '$file'" )
                    if ($opt_d);
                # Plugin scripts must have '0700' permissions
                my $mode       = ( stat($file) )[2];
                my $mode_octal = sprintf "%04o", $mode & 07777;
                if ( $mode_octal =~ /^0700$/ )
                {
                    # Run script as "nobody"
                    system( ( ($HaveTimeout) ? 'timeout 60s ' : '' ) . 'sudo',
                        '-u', 'nobody', "$file" );
                } ## end if ( $mode_octal =~ /^0700$/...)
            } ## end while ( my $plugin = readdir...)
            closedir($DIR);
        } ## end if ( opendir( my $DIR,...))
    } ## end if ($opt_P)

    # Check listening TCP and UDP ports
    if ( -x '/bin/ss' )
    {
        my %PortList_Now  = ();
        my %PortList_Prev = ();
        my %ProcList_Now  = ();
        my %ProcList_Prev = ();
        my $HavePrev      = 0;
        my $PLP;
        # Read in the previous port list (if present)
        if ( open( $PLP, '<', '/var/tmp/plp.txt' ) )
        {
            my @lines = ();
            chomp( @lines = grep {/PORT|PROC/} <$PLP> );
            close($PLP);
            foreach (@lines)
            {
                warn "DEBUG: PLP Line = $_" if ($opt_d);
                if (/^(PORT|PROC)\s+(\S+)/o)
                {
                    my $What = "$1";
                    my $key  = "$2";
                    if ( $What eq 'PORT' )
                    {
                        $PortList_Prev{"$key"} = 1;
                        $HavePrev++;
                    } elsif ( $What eq 'PROC' )
                    {
                        $ProcList_Prev{"$key"} = 1;
                        $HavePrev++;
                    } ## end elsif ( $What eq 'PROC' )
                } ## end if (/^(PORT|PROC)\s+(\S+)/o...)
            } ## end foreach (@lines)
        } ## end if ( open( $PLP, '<', ...))
        my $WritePLP = ( open( $PLP, '>', '/var/tmp/plp.txt' ) ) ? 1 : 0;
        if ($WritePLP)
        {
            my $Now = localtime();
            print $PLP "# Last Updated: $Now\n";
        } ## end if ($WritePLP)

        # Get the list of current ports
        if (open(
                my $SS,
                "-|",
                ( ($HaveTimeout) ? 'timeout 60s ' : '' )
                    . '/bin/ss -ntluwp4OH'
            )
            )
        {
            my @lines = ();
            chomp( @lines = grep {/^(udp|tcp)/} <$SS> );
            close($SS);
            foreach (@lines)
            {
                # Extract the meaningful data
                if (/^(udp|tcp)\s+(?:LISTEN|UNCONN)\s+\d+\s+\d+\s+([\d\.]+|\[[:\d]+\]):(\S+)[^"]+"([^"]+)",pid=(\d+)/o
                    )
                {
                    my $proto = "$1";
                    my $addr  = "$2";
                    my $port  = "$3";
                    my $key   = "$proto:$addr:$port";
                    my ($prog) = split( /\s+/, "$4" );
                    my $pid = $5;
                    $PortList_Now{"$key"} = 1;
                    warn join( '',
                        "DEBUG: PortList_Now{$key} = ",
                        $PortList_Now{"$key"}, "\n" )
                        if ($opt_d);
                    $ProcList_Now{"$prog"} = 1;
                    warn join( '',
                        'DEBUG: ', ProcList_Now {"$prog"},
                        '= ', $ProcList_Now{"$prog"}, "\n" )
                        if ($opt_d);
                    my $PortReport = '';

                    # Remember the current findings
                    print $PLP "PORT $key\nPROC $prog\n" if ($WritePLP);

                    if ( $port == 1080 )
                    {
                        $PortReport
                            = "Possible SOCKS5 proxy detected: $proto $addr:$port ('$prog' with PID #$pid)";
                        if ( $opt_s eq 'syslog' )
                        {
                            syslog 4, "warning %s", $PortReport;
                        } else
                        {
                            mylog(
                                join( '',
                                    strftime( "%F %T", localtime ),
                                    " $ProgName",
                                    "[$$] WARNING: $PortReport" )
                            );
                        } ## end else [ if ( $opt_s eq 'syslog'...)]
                    } ## end if ( $port == 1080 )

                    # We are done if we have no previous info
                    #  or seen this previously
                    next
                        if ( ( !$HavePrev )
                        or ( $PortList_Prev{"$key"} == 1 )
                        or ( $ProcList_Prev{"$prog"} == 1 ) );

                    # We have something not seen before
                    $PortReport
                        = "New listening port detected: $proto $addr:$port ('$prog' with PID #$pid)";
                    if ( $opt_s eq 'syslog' )
                    {
                        syslog 4, "warning %s", $PortReport;
                    } else
                    {
                        mylog(
                            join( '',
                                strftime( "%F %T", localtime ),
                                " $ProgName",
                                "[$$] WARNING: $PortReport" )
                        );
                    } ## end else [ if ( $opt_s eq 'syslog'...)]
                } ## end if (...)
            } ## end foreach (@lines)
            close($PLP) if ($WritePLP);

            if ($HavePrev)
            {
                # Find any ports that have disappeared
                my @NoLongerPresent = ();
                foreach ( keys %PortList_Prev )
                {
                    push( @NoLongerPresent, $_ )
                        unless exists $PortList_Now{$_};
                } ## end foreach ( keys %PortList_Prev...)

                my $ArraySize = @NoLongerPresent;
                for ( my $i = 0; $i < $ArraySize; $i++ )
                {
                    my ( $proto, $addr, $port )
                        = split( /:/, $NoLongerPresent[$i] );

                    # Skip UDP ports that are not well-known
                    next if ( $proto eq 'udp' ) and ( $port > 1023 );
                    my $PortReport
                        = "Listening port disappeared: $proto $addr:$port";
                    if ( $opt_s eq 'syslog' )
                    {
                        syslog 4, "warning %s", $PortReport;
                    } else
                    {
                        mylog(
                            join( '',
                                strftime( "%F %T", localtime ),
                                " $ProgName",
                                "[$$] WARNING: $PortReport" )
                        );
                    } ## end else [ if ( $opt_s eq 'syslog'...)]
                } ## end for ( my $i = 0; $i < $ArraySize...)

                # Find any processes that have disappeared
                @NoLongerPresent = ();
                foreach ( keys %ProcList_Prev )
                {
                    push( @NoLongerPresent, $_ )
                        unless exists $ProcList_Now{$_};
                } ## end foreach ( keys %ProcList_Prev...)
                $ArraySize = @NoLongerPresent;
                for ( my $i = 0; $i < $ArraySize; $i++ )
                {
                    my $PortReport
                        = "Process $NoLongerPresent[$i] no longer listening on any port";
                    if ( $opt_s eq 'syslog' )
                    {
                        syslog 4, "warning %s", $PortReport;
                    } else
                    {
                        mylog(
                            join( '',
                                strftime( "%F %T", localtime ),
                                " $ProgName",
                                "[$$] WARNING: $PortReport" )
                        );
                    } ## end else [ if ( $opt_s eq 'syslog'...)]
                } ## end for ( my $i = 0; $i < $ArraySize...)
            } ## end if ($HavePrev)
        } ## end if ( open( my $SS, "-|"...))
    } ## end if ( -x '/bin/ss' )

    # Find all users logged in to dovecot from IP
    #  addresses other than the local network
    if ( -x '/usr/bin/doveadm' )
    {
        if (open(
                my $DA,
                '-|',
                ( ($HaveTimeout) ? 'timeout 60s ' : '' )
                    . '/usr/bin/doveadm who'
            )
            )
        {
            my @lines = ();
            chomp( @lines = grep { !/^username/ } <$DA> );
            close($DA);
            my %UserList = ();
            my %IPList   = ();
            foreach (@lines)
            {
                next
                    unless (
                    /^([^\@]+\@\S+)\s+\d+\s+(\S+)\s+\([\d\s]+\)\s+\(([\d\.\s]+)\)/o
                    );
                my $User  = "$1";
                my $Proto = "$2";
                my $IPs   = "$3";
                # Ignore localhost (stems from "DeleteDuplicateEmails.sh")
                $IPs =~ s/127\.0\.0\.1//g;
                $IPList{$User} = "$IPs";

                # Show what we have
                my $HowOften = split( /\s+/, $IPs );
                $IPs =~ s/\s+/ and /g;
                my $DAReport
                    = "Dovecot user $User is logged in via $Proto from $IPs ($HowOften time(s))";
                $UserList{$User} = $HowOften;
                if ( $opt_s eq 'syslog' )
                {
                    syslog 4, "info %s", $DAReport;
                } else
                {
                    mylog(
                        join( '',
                            strftime( "%F %T", localtime ),
                            " $ProgName",
                            "[$$] INFO: $DAReport" )
                    );
                } ## end else [ if ( $opt_s eq 'syslog'...)]
            } ## end foreach (@lines)

            # Show all users that are logged in too often
            foreach my $User ( sort keys %UserList )
            {
                if ( $UserList{$User} > 3 )
                {
                    # More than three times - a very likely issue
                    my $DAReport
                        = "Dovecot user $User is logged in $UserList{$User} times from $IPList{$User} (use 'doveadm kick -f $User' to remove the user)";
                    if ( $opt_s eq 'syslog' )
                    {
                        syslog 4, "error %s", $DAReport;
                    } else
                    {
                        mylog(
                            join( '',
                                strftime( "%F %T", localtime ),
                                " $ProgName",
                                "[$$] ERROR: $DAReport" )
                        );
                    } ## end else [ if ( $opt_s eq 'syslog'...)]
                    if ($opt_a)
                    {
                        # Also send an email alert
                        SendEmail( "ERROR: $DAReport", 'normal' );
                    } ## end if ($opt_a)
                } ## end if ( $UserList{$User} ...)
            } ## end foreach my $User ( sort keys...)
        } ## end if ( open( my $DA, '-|'...))
    } ## end if ( -x '/usr/bin/doveadm'...)

    # Only continue loop if we are running in daemon mode
    last unless ($opt_D);
} ## end while ( my $VMLine = <$VMS>...)
if ( $opt_s eq 'syslog' )
{
    closelog;
} else
{
    close(LF);
} ## end else [ if ( $opt_s eq 'syslog'...)]

close(VMS);

# We are done
exit 0;
__END__
