#!/bin/bash
# Version 20240303-173101 checked into repository
################################################################
# (c) Copyright 2013-2018 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/findhidden.sh
# Based on https://github.com/wazuh/wazuh/issues/3392

if [ $(/usr/bin/id -u) -ne 0 ]
then
    echo "ERROR: $0 must run as 'root'"
    exit 1
fi

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=${0##*/}

# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            

#--------------------------------------------------------------------
# Globals
#DEBUG=0
#[[ $- ==  *x* ]] && DEBUG=1

PID_MAX_FILE=/proc/sys/kernel/pid_max
if [ ! -f $PID_MAX_FILE ]
then
    echo "ERROR: No such file $PID_MAX_FILE"
    exit 1
fi

pid_max=$(< $PID_MAX_FILE)
LOGFILE=/var/log/findhidden.log
date "+%F %T $PROG Scanning up to PID $pid_max" >> $LOGFILE 2>&1

#--------------------------------------------------------------------
# The main loop
RC=0
pid=1
found=0
PS_LIST=$(ps aux)
while [ $pid -lt $pid_max ]
do
    MSG=''
    PROC_EXISTS=0
    if builtin kill -0 $pid 2> /dev/null
    then
        PROC_EXISTS=1
        MSG=$(date "+%F %T $PROG Process $pid exists")
        [ -e /proc/$pid/cmdline ] && MSG="${MSG}: $(tr '\0' ' ' </proc/$pid/cmdline)"
        found=$((found + 1))

        if [ -d "/proc/$pid" ]
        then
            rm -f /tmp/$$.pdpid
            if [ -f /proc/$pid/status ]
            then
                TRY=0
                while [ $TRY -lt 3 ]
                do
                    tgid=$(awk '/^Tgid:/{printf "%d", int($NF)}' /proc/$pid/status 2> /dev/null)
                    [ -z "$tgid" ] && tgid=-1
                    if [ $tgid -ge 0 ] 
                    then
                        if [ $pid -ne $tgid ]
                        then
                            [ $tgid -ne 0 ] && MSG="${MSG}\n Process $pid is thread of $tgid"
                        else
                            MSG="${MSG}\n Process $pid is a standalone process"
                        fi
                        [ -e /proc/$tgid/cmdline ] && MSG="${MSG}: $(tr '\0' ' ' </proc/$tgid/cmdline)"
                        break
                    fi
                    TRY=$((TRY + 1))
                done
                if [ $TRY -ge 3 ]
                then
                    MSG=$(date "+%F %T $PROG Cannot get status of process $pid")
                    echo -e "${MSG}\n"
                fi
            else
                MSG=$(date "+%F %T $PROG Cannot get status of process $pid")
                echo -e "${MSG}\n"
            fi
        else
            MSG=$(date "+%F %T $PROG Process $pid might be hidden, it is not in /proc ($$ is this script's own process ID)")
            # Try to get some infos (if at all possible)
            MSG="${MSG}\n $(ps -e -q $pid -o 'PID=%p PPID=%P CMD=%a USER=%u' | grep $pid)"
            echo -e "${MSG}\n"
            RC=$((RC + 1))
        fi
    fi

    [ $PROC_EXISTS -ne 0 ] && echo -e "${MSG}\n" >> $LOGFILE 2>&1
    pid=$((pid+1))
done

SUMMARY=$(date "+%F %T $PROG Found $found process(es), of which $RC was/were hidden")
echo "$SUMMARY" >> $LOGFILE
echo >> $LOGFILE
if [ $RC -ne 0 ] || [[ ! $SUMMARY =~ 'which 0 was/were hidden' ]]
then
    # We had some problem or found at least 1 hidden process
    echo "$SUMMARY"
    echo
    echo "$PS_LIST"
fi

#--------------------------------------------------------------------
# Keep the log file at a reasonable size
CURLINES=$(sed -n '$=' $LOGFILE)
if [ $CURLINES -gt 50000 ]
then
    LINES2DEL=$((CURLINES - 50000))
    sed -i -e "1,${LINES2DEL}d" $LOGFILE
fi

#--------------------------------------------------------------------
# We are done
exit $RC
