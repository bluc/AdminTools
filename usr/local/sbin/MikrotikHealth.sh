#!/bin/bash
# Version 20240320-104520 checked into repository
################################################################
# (c) Copyright 2023 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/MikrotikHealth.sh

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

PROG=${0##*/}
DEBUG='-q'
[[ $- ==  *x* ]] && DEBUG='-v'

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -rf $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE

# This host and domain name
THISHOST=$(hostname)
[[ $THISHOST == *.* ]] || THISHOST=$(hostname -f)
THISDOMAIN=${THISHOST#*.}

# Files created by this script
CSV='/var/tmp/MikrotikHealth.csv'
GRAPH_CPU_TEMP='/tmp/MikrotikHealth_CPU_TEMP.png'
GRAPH_PERC='/tmp/MikrotikHealth_PERC.png'

# Current date and time
CUR_DATETIME="$(date '+%H %M')"

# Email configuration
if [ -s /usr/local/etc/rootmail.conf ]
then
    source /usr/local/etc/rootmail.conf
    [ -z "$EMAIL_FROM" ] && EMAIL_FROM="root@$THISHOST"
    [ -z "$EMAIL_TO" ] && EMAIL_FROM="root@$THISDOMAIN"
    [ -z "$EMAIL_SERVER" ] && EMAIL_SERVER="mail"
else
    echo "ERROR: Can't find email configuration"
    exit 1
fi


#--------------------------------------------------------------------
# What we watch
CPULOAD='1.3.6.1.2.1.25.3.3.1'
MEM_TOTAL='1.3.6.1.2.1.25.2.3.1.5.65536'
MEM_USED='1.3.6.1.2.1.25.2.3.1.6.65536'
DISK_TOTAL='1.3.6.1.2.1.25.2.3.1.5.131072'
DISK_USED='1.3.6.1.2.1.25.2.3.1.6.131072'
TEMP='1.3.6.1.4.1.14988.1.1.3.100.1.3.14'

# Get the CPU load as sum of load on each core
cpuload=$(snmpwalk -v 1 -c public gw $CPULOAD | awk '/hrProcessorLoad/{load+=$NF}END{print load}')
set $(snmpget -Oq -Ov -v 1 -c public gw $MEM_TOTAL $MEM_USED $DISK_TOTAL $DISK_USED $TEMP)
mem_total=$1
mem_used=$2
mem_perc=$((mem_used / $((mem_total / 100))))
disk_total=$3
disk_used=$4
disk_perc=$((disk_used / $((disk_total / 100))))
temp=$5
date "+%s,$cpuload,$mem_perc,$disk_perc,$temp" >> $CSV

#--------------------------------------------------------------------
# Sanity checks
if [ $cpuload -gt 50 ]
then
    echo "Last reported CPU load is $cpuload" | \
     sendemail $DEBUG -f $EMAIL_FROM -t $EMAIL_TO \
       -u "WARNING: Mikrotik CPU load high" -s $EMAIL_SERVER
fi
if [ $temp -gt 50 ]
then
    echo "Last reported temperature is $temp" | \
     sendemail $DEBUG -f $EMAIL_FROM -t $EMAIL_TO \
       -u "WARNING: Mikrotik temperature high" -s $EMAIL_SERVER
fi
if [ $mem_perc -gt 50 ]
then
    echo "Last reported mem usage is ${mem_perc}%" | \
     sendemail $DEBUG -f $EMAIL_FROM -t $EMAIL_TO \
       -u "WARNING: Mikrotik mem usage" -s $EMAIL_SERVER
fi
if [ $disk_perc -gt 85 ]
then
    echo "Last reported disk usage is ${disk_perc}%" | \
     sendemail $DEBUG -f $EMAIL_FROM -t $EMAIL_TO \
       -u "WARNING: Mikrotik disk usage" -s $EMAIL_SERVER
fi

#--------------------------------------------------------------------
# Create a graph
if [ -s $CSV ]
then
    #--------------------------------------------------------------------
    # Install any necessary programs
    export DEBIAN_FRONTEND=noninteractive
    for P in ploticus sendemail gawk zip #webfs
    do
        command -v "$P" &> /dev/null || dpkg-query -S $P &>/dev/null || apt-get -yq install $P
        command -v "$P" &> /dev/null
        if [ $? -ne 0 ]
        then
            if [ "T$P" = 'Tploticus' ]
            then
                wget -qO /tmp/pl.tar.gz \
                  https://sourceforge.net/projects/ploticus/files/ploticus/2.42/ploticus242_linuxbin64.tar.gz/download
                if [ -s /tmp/pl.tar.gz ]
                then
                    CWD=$(pwd)
                    cd /tmp
                    rm -rf plo*
                    tar xzf pl.tar.gz
                    cp plo*/bin/pl /usr/bin/ploticus
                    cd $CWD
                fi
            fi
        fi
        command -v "$P" &> /dev/null
        if [ $? -ne 0 ]
        then
            echo "ERROR: '$P' is not installed locally"
            exit 1
        fi
    done

    # Truncate csv file 50000 lines
    CURLINES=$(sed -n '$=' $CSV)
    if [ $CURLINES -gt 50000 ]
    then
        # Truncate the file to 50000 lines
        LINES2DEL=$((CURLINES - 50000))
        sed -i -e "1,${LINES2DEL}d" $CSV
    fi

    # Get date for the last 2 days and combine data and time fields
    echo 'DateTime,CPULoad,MemPer,DiskPerc,Temp' > /tmp/$$.csv
    START_DATE=$(date +%s -d '2 days ago')
    tail -n 700 $CSV | while IFS=, read -r dt cl mp dp t
    do
        [ $dt -lt $START_DATE ] && continue
        printf '%s,%d,%d,%d,%d\n' "$(date +'%F.%T' --date="@$dt")" $cl $mp $dp $t >> /tmp/$$.csv
    done

    # Create the graphs
    if [ $(sed -n '$=' /tmp/$$.csv) -gt 1 ]
    then
        ploticus -prefab chron png -o $GRAPH_CPU_TEMP -showbad \
          data=/tmp/$$.csv delim=comma header=yes \
          x=1 y=2 y2=5 datefmt=yyyy-mm-dd mode=line unittype=datetime \
          xinc="1 hour" stubvert=yes xstubfmt="dd-Mmm.hh:mm" xstubdet="size=5" \
          xgrid="color=gray(0.9) width=0.1 style=2" ygrid="color=gray(0.9) width=0.1 style=2" \
          xlbl="" ylbl="" \
          linedet="color=red width=0.75" linedet2="color=brightblue width=0.75" \
          name="CPU Load" name2='Temp. (Celsius)' \
          legend="min+1 max+0.01" legendfmt=across legbox="gray(0.9)" legtextdet="size=5" \
          title='CPU Load and Temperature' 2> /tmp/$$.error

        ploticus -prefab chron png -o $GRAPH_PERC -showbad \
          data=/tmp/$$.csv delim=comma header=yes \
          x=1 y=3 y2=4 datefmt=yyyy-mm-dd mode=line unittype=datetime \
          xinc="1 hour" stubvert=yes xstubfmt="dd-Mmm.hh:mm" xstubdet="size=5" \
          xgrid="color=gray(0.9) width=0.1 style=2" ygrid="color=gray(0.9) width=0.1 style=2" \
          xlbl="" ylbl="Percentage" \
          linedet="color=red width=0.75" linedet2="color=brightblue width=0.75" \
          name="Memory %" name2="Disk %" \
          legend="min+1 max+0.01" legendfmt=across legbox="gray(0.9)" legtextdet="size=5" \
          title="Memory and Disk % usage" 2> /tmp/$$.error

        if [ "T$CUR_DATETIME" = 'T23 07' ] && [ -s $GRAPH_CPU_TEMP ] && [ -s $GRAPH_PERC ]
        then
            # Email the graph at the end of the day
            (echo 'See attached graphs for an overview'; echo; cat /tmp/$$.error) | \
              sendemail $DEBUG -f $EMAIL_FROM -t $EMAIL_TO \
                -u "Mikrotik CPU load, temperature, and memory and disk percentages" \
                -s $EMAIL_SERVER -a $GRAPH_CPU_TEMP $GRAPH_PERC
        fi
    fi
fi

# We are done
exit 0
