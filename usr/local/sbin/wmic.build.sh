#!/bin/bash
################################################################
# (c) Copyright 2017 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# Only "root" can run this script
[ "T$(/usr/bin/id -u)" = 'T0' ] || exit

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=${0##*/}

# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi
trap "rm -rf $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            
TFILE=$(mktemp /tmp/$$XXXXXXXX)

# Get possible program options
FORCE_BUILD=0
while getopts f OPTION
do
    case ${OPTION} in
    f)  FORCE_BUILD=1
        ;;
    *)  echo "Usage: $0 [-f]"
        echo "       -f Force a build"
        ;;
    esac
done
shift $((OPTIND - 1))

#--------------------------------------------------------------------
if [ $FORCE_BUILD -eq 0 ]
then
    [ -x /bin/wmic ] || FORCE_BUILD=2
fi

if [ $FORCE_BUILD -ne 0 ]
then
    # Based on https://askubuntu.com/questions/885407/installing-wmic-on-ubuntu-16-04-lts/921123
    ulimit -n 100000
    mkdir -p $TFILE.wmic
    cd $TFILE.wmic
    rm -rf wmi-1*

    # Install the prerequsites
    apt-get -y install autoconf gcc libdatetime-perl make build-essential g++ python-dev

    # Get the source and unpack it
    wget http://www.opsview.com/sites/default/files/wmi-1.3.16.tar_.bz2 -O wmi-1.3.16.tar_.bz2
    [ -s wmi-1.3.16.tar_.bz2 ] || wget https://gitlab.com/bluc/AdminTools/raw/master/usr/local/src/wmi-1.3.16.tar_.bz2 -O wmi-1.3.16.tar_.bz2
    tar xjf wmi-1.3.16.tar_.bz2
    cd wmi-1.3.16/

    # Fix a typo
    sed -i 's/defined @$pidl/@$pidl/' Samba/source/pidl/pidl 

    # Compile, build and install the software
    # (disregard the numerous error messages)
    export ZENHOME=/usr
    make "CPP=gcc -E -ffreestanding"
    cp Samba/source/bin/{wmic,winexe} /bin
fi

# Build a .deb package (amd64 only)
if [ -f /etc/debian_version ]
then
    # Install GIT and FPM if necessary
    dpkg-query -W git &> /dev/null
    [ $? -ne 0 ] && apt-get -y install git
    dpkg-query -W ruby-dev &> /dev/null
    [ $? -ne 0 ] && apt-get -y install ruby-dev
    dpkg-query -W rubygems &> /dev/null
    [ $? -ne 0 ] && apt-get -y install rubygems
    [ -x /usr/local/bin/fpm ] || gem install fpm

    # Create the debian package
    VERSION='1.0'
    rm -rf $TFILE.wmic
    mkdir -p $TFILE.wmic/bin
    cp /bin/{wmic,winexe} $TFILE.wmic/bin
    cd $TFILE.wmic
    fpm -n wmic -v $VERSION -a amd64 -C $TFILE.wmic -m "consult@btoy1.net" \
      --description "WMI Standalone Clients (wmic and winexe)" \
      -f --package /tmp/wmic_${VERSION}_amd64.deb -t deb \
      --url 'https://gitlab.com/bluc/AdminTools' -s dir .
    RETCODE=$?
    if [ -s /tmp/wmic_${VERSION}_amd64.deb ]
    then
        dpkg-deb -c /tmp/wmic_${VERSION}_amd64.deb
        echo "The Debian package is '/tmp/wmic_${VERSION}_amd64.deb'"
    fi
else
    echo "Unsupported Linux distribution"
fi

#--------------------------------------------------------------------
# We are done
exit 0
