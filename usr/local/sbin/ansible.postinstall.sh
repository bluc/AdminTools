#!/bin/bash
################################################################
# (c) Copyright 2021  B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/ansible.postinstall.sh

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=${0##*/}

# This needs to run as "root"
if [ ! "T$(/usr/bin/id -u)" = 'T0' ]
then
    echo "$PROG needs to run as 'root'"
    exit 1
fi

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

#--------------------------------------------------------------------
# This host and domain
THISHOST=$(hostname)
[[ $THISHOST == *.* ]] || THISHOST=$(hostname -f)
THISDOMAIN=${THISHOST#*.}

# Get possible program options
UPDATE=0
while getopts u OPTION
do
    case ${OPTION} in
    u)  UPDATE=1
        ;;
    *)  echo "Usage: $0 [-u]"
        ;;
    esac
done
shift $((OPTIND - 1))

#--------------------------------------------------------------------
# See https://launchpad.net/~ansible/+archive/ubuntu/ansible
dpkg-query -s ansible &> /dev/null
if [ $? -ne 0 ]
then
    add-apt-repository ppa:ansible/ansible
    apt-get update
    apt-get install ansible
fi
ansible --version
sleep 3

# Create/update the list of known hosts
[ -s /etc/ansible/hosts ] || cat << EOT > /etc/ansible/hosts
localhost ansible_connection=local
#[servers]
#server1 ansible_host=203.0.113.111 ansible_user=ansible ansible_become_pass=<sudo_pass>
#server2 ansible_host=203.0.113.112 ansible_user=ansible ansible_become_pass=<sudo_pass>
#server3 ansible_host=203.0.113.113 ansible_user=ansible ansible_become_pass=<sudo_pass>
EOT
if [ -z "$(grep python3 /etc/ansible/hosts)" ]
then
    cat << EOT >> /etc/ansible/hosts
[all:vars]
ansible_python_interpreter=/usr/bin/python3
ansible_ssh_private_key_file=/etc/ssh/ssh_host_ecdsa_key
EOT
fi
if [ -z "$(grep ^ANSIBLE_VAULT /etc/ansible/hosts)" ]
then
    vim.tiny /etc/ansible/hosts
    ansible-inventory --list -y
else
    ansible-vault edit --vault-password-file=/etc/ansible/vault.cfg /etc/ansible/hosts
fi

# Encrypt the hosts file
if [ ! -s /etc/ansible/vault.cfg ]
then
    EP=''
    while [ -z "$EP" ]
    do
        read -p 'Encryption password for the vault ? ' EP
    done
    echo "$EP" > /etc/ansible/vault.cfg
    ansible-vault --vault-password-file=/etc/ansible/vault.cfg encrypt /etc/ansible/hosts
fi

#--------------------------------------------------------------------
# Create the ansible user
if [ -z "$(getent passwd ansible)" ]
then
    useradd -c 'Ansible User' -d /etc/ansible -s /bin/bash -m -G sudo ansible
    # Use the password in the ansible vault for the user ID
    echo 'ansible:'$(< /etc/ansible/vault.cfg) | chpasswd
fi

#--------------------------------------------------------------------
# Create playbook directory
mkdir -p /etc/ansible/playbook

#--------------------------------------------------------------------
# Install ansible frontend script
[ -s /usr/local/bin/AnsibleUtils.sh ] || cat << EOAU > /usr/local/bin/AnsibleUtils.sh
#!/bin/bash
################################################################
# (c) Copyright 2013-2017 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################

# This script must be run as "ansible"
[ "T\$(/usr/bin/id -u)" = "T\$(getent passwd ansible | cut -d: -f3)" ] || exit

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/opt/zimbra/bin

RED='\e[00;31m'
BLUE='\e[01;34m'
CYAN='\e[01;36m'
NC='\e[0m' # No Color
DOUBLE="\033#6"

# The base directory for all things ansible
ABD=/etc/ansible

# The vault password
VPF=\${ABD}/vault.cfg
[ -s \$VPF ] || echo '1BafyeS' > \$VPF

# The inventory file, playbooks etc.
AIF=\${ABD}/hosts
APB_DIR=\${ABD}/playbooks

################################################################
# Main loop
while [ 1 ]
do
    clear
    echo
    echo -e "\${DOUBLE} ANSIBLE UTILITIES"
    echo \$(hostname -f)', '\$(date)
    echo
    echo -e " \${CYAN} 1\${NC} - Show current version      \${CYAN} 2\${NC} - Show configuration"
    echo -e " \${CYAN} 3\${NC} - Show inventory            \${CYAN} 4\${NC} - Edit inventory"
    echo -e " \${CYAN} 5\${NC} - Show host info            \${CYAN} 6\${NC} - Run ad-hoc ansible command"
    echo -e " \${CYAN} 7\${NC} - Show playbooks            \${CYAN} 8\${NC} - Run a playbook"
    echo -e " \${CYAN} 9\${NC} - Edit/create a playbook"
    echo
    echo -e " \${RED}0\${NC} - Return to prompt"
    echo
    read -p 'Please enter a choice: ' CHOICE

    case \$CHOICE in
    #=======================================================
    0)  exit
    ;;
    #=======================================================
    1)  ansible --version
        read -p 'Press ENTER to continue'
    ;;
    #=======================================================
    2)  ansible-config dump | less
    ;;
    #=======================================================
    3)  ansible-inventory --list -y --vault-password-file=\$VPF -i \$AIF | less
    ;;
    #=======================================================
    4)  ansible-vault edit --vault-password-file=\$VPF \$AIF
    ;;
    #=======================================================
    5)  H=''; read -p 'Specify host or group [default=all] ? ' H
        [ -z "\$H" ] && H='all'
        ansible \$H --vault-password-file=\$VPF -m setup | less
    ;;
    #=======================================================
    6)  C=''; read -p 'Specify command: ' C
        [ -z "\$C" ] && continue
        H=''; read -p 'Specify host or group [default=all] ? ' H
        [ -z "\$H" ] && H='all'
        ansible \$H --become --vault-password-file=\$VPF -a "\$C" &> /tmp/\$\$
        less /tmp/\$\$
    ;;
    #=======================================================
    7)  echo -e 'List of available playbooks:\n' > /tmp/\$\$
        for F in \${APB_DIR}/*.yaml
        do
          DESC=\$(awk -F: '/name:/{print \$NF;exit}' \$F)
          LM=\$(date '+%F %T' -r \$F)
          printf "%s %s %20s %s\n" \$LM \${F##*/} "\$DESC" >> /tmp/\$\$
        done
        less /tmp/\$\$
        rm -f /tmp/\$\$
    ;;
    #=======================================================
    8)  PB=''; read -p 'Specify name of playbook: ' PB
        if [ -s \${APB_DIR}/\${PB}.yaml ]
        then
            PB=\${APB_DIR}/\${PB}.yaml
        elif [ -s \$PB.yaml ]
        then
            PB=\${PB}.yaml
        else
            echo "Playbook '\$PB' doesn't exist"
            read -p 'Press ENTER to continue'
            continue
        fi

        PH=''; read -p 'Specify host or group [default=all]: ' PH
        [ -z "\$PH" ] || PH="-l \$PH"

        DEBUG=''
        DM=''; read -p 'Run in debug mode [y/N] ? ' DM
        [ "T\${DM,,}" = 'Ty' ] && DEBUG='-vvvv'
        # This assumes that the inventory contains the sudo password,
        # preferably in an ansible vault. Example:
        # <host> ansible_host=<ip.add.re.ss> ansible_user=<userid> ansible_become_pass=<sudo_passwd> ansible_ssh_private_key_file=/etc/ssh/ssh_host_ecdsa_key
        echo -e "Running '\$PB'\n\n"
        ansible-playbook \$DEBUG -f \$(grep -c 'core id.*0' /proc/cpuinfo) \$PH --vault-password-file=\$VPF \$PB
        read -p 'Press ENTER to continue'
    ;;
    #=======================================================
    9)  PB=''; read -p 'Specify name of playbook: ' PB
        [ -z "\$PB" ] && continue
        if [ -s \${APB_DIR}/\${PB}.yaml ]
        then
            PB=\${APB_DIR}/\${PB}.yaml
        elif [ -s \$PB.yaml ]
        then
            PB=\${PB}.yaml
        else
            echo "Playbook '\$PB' doesn't exist"
            read -p 'Create it [Y/n] ? ' CPB
            [ -z "\$CPB" ] && CPB='y'
            [ "\$T{CPB,,}" = 'Ty' ] || continue
            cat << EOT > \$PB
---
# This is an example - please edit!
- name: Playbook name
  hosts: all
  become: false (or true of running as "root")

  tasks:
  - name: Task name
    command: shell command
    register: command_output

  - debug:
     var: command_output.stdout_lines
EOT
        fi
        vim \$PB
    ;;
    esac
done

################################################################
# We are done
exit 0
EOAU
chmod 700 /usr/local/bin/AnsibleUtils.sh

# Adjust ownership for necessary files
chown -R ansible: /etc/ansible
chown ansible /etc/ssh/ssh_host_ecdsa_key /usr/local/bin/AnsibleUtils.sh

#--------------------------------------------------------------------
# Test connectivity to remote servers
ansible all --vault-password-file=/etc/ansible/vault.cfg -m ping

#--------------------------------------------------------------------
# We are done
exit 0
