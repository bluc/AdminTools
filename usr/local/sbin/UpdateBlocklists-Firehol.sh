#!/bin/bash
# Version 20221109-081420 checked into repository
################################################################
# (c) Copyright 2017 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/UpdateBlocklists-Firehol.sh

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=${0##*/}

dpkg-query -s firehol &> /dev/null
if [ $? -ne 0 ]
then
    echo "ERROR: 'firehol' is not installed"
    exit 1
fi

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE

# Global variables
THISHOST=$(hostname)
[[ $THISHOST == *.* ]] || THISHOST=$(hostname -f)

# The level of blocking
# 1 - minimum
# 2 - medium
# 3 - all
BLK_LEVEL=1

# The list of countries
COUNTRIES=''

# Force update
FORCE=0

TFILE=$(mktemp /tmp/$$XXXXXXXX)

# Get possible program options
while getopts C:L:f OPTION
do
    case ${OPTION} in
        C)  COUNTRIES=$(printf "%s" $OPTARG 2> /dev/null)
            COUNTRIES=${COUNTRIES,,}
            ;;
        L)  BLK_LEVEL=$(printf "%d" $OPTARG 2> /dev/null)
            [ $BLK_LEVEL -lt 1 ] && BLK_LEVEL=1
            [ $BLK_LEVEL -gt 3 ] && BLK_LEVEL=3
            ;;
        f)  FORCE=1
            ;;
        *)  echo "Usage: $0 [-L [1|2|3]] (default=$BLK_LEVEL) [-C c1[c2[,c3]]]"
            ;;
    esac
done
shift $((OPTIND - 1))

# Force an update if the blocklists are not activated
[ -z "$(iptables -nvL INPUT 2> /dev/null | grep -Ea 'BL_(NET|HOST)S')" ] && FORCE=1
[ -z "$(iptables -nvL INPUT 2> /dev/null | grep -Ea 'C_(NET|HOST)S_')" ] && FORCE=1

# Find out whether we can use the "-w" option for iptables
WAIT='-w'
[ -z "$(iptables --help | grep 'wait.*seconds')" ] && WAIT=''

# Preset blocklists if present
HOSTS_HASH_SIZE=$((2048 * 1024))
if [ $FORCE -eq 0 ]
then
    TRY=0
    iptables -nvL INPUT 2> /dev/null > /tmp/$$
    [ -x /usr/sbin/iptables-legacy ] && iptables-legacy  -nvL INPUT 2> /dev/null >> /tmp/$$
    while [ -z "$(grep -m 1 ctstate /tmp/$$)" ]
    do
        # Wait until firehol has fully initialized
        TRY=$((TRY + 1))
        [ $TRY -gt 30 ] && exit 1
        sleep 10
        iptables -nvL INPUT 2> /dev/null > /tmp/$$
        [ -x /usr/sbin/iptables-legacy ] && iptables-legacy  -nvL INPUT 2> /dev/null >> /tmp/$$
    done

    if [ -x /sbin/ipset ]
    then
        # Create the active hosts blocklist and populate it
        if ! ipset list BL_HOSTS > /dev/null 2>&1
        then
            # Create the active BLOCKLIST
            ipset create BL_HOSTS hash:ip hashsize 16384 maxelem $HOSTS_HASH_SIZE -exist > /dev/null
            if [ -s "/var/tmp/BL_HOSTS.sav" ]
            then
                # Restore the previous BLOCKLIST
                grep -v 'create' /var/tmp/BL_HOSTS.sav | ipset restore
            fi
        fi
        # Create the active networks blocklist and populate it
        if ! ipset list BL_NETS > /dev/null 2>&1
        then
            # Create the active BLOCKLIST
            ipset create BL_NETS hash:net hashsize 16384 maxelem $HOSTS_HASH_SIZE -exist > /dev/null
            if [ -s "/var/tmp/BL_NETS.sav" ]
            then
                # Restore the previous BLOCKLIST
                grep -v 'create' /var/tmp/BL_NETS.sav | ipset restore
            fi
        fi
    fi

    # Don't do any update unless it was not yet done
    #  or not done for the past 24 hours
    if [ -s /tmp/Blocklist.lastupdate ]
    then
        [ $(date +%s -f /tmp/Blocklist.lastupdate) -gt $(date +%s -d '24 hours ago') ] && exit 0
    fi
fi

#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Get IP addresses and network for bad actors
bash UpdateBlocklists.sh -L $BLK_LEVEL

if [ -x /sbin/ipset ]
then
    # Convert the IP addresses into "ipset" statements
    cat << EOT > $TFILE
# Last updated: $(date)
ipset create BL_HOSTS_NEW hash:ip family inet hashsize 16384 maxelem $HOSTS_HASH_SIZE -exist
ipset create BL_HOSTS hash:ip family inet hashsize 16384 maxelem $HOSTS_HASH_SIZE -exist
ipset create BL_NETS_NEW hash:net family inet hashsize 16384 maxelem $HOSTS_HASH_SIZE -exist
ipset create BL_NETS hash:net family inet hashsize 16384 maxelem $HOSTS_HASH_SIZE -exist
EOT

    awk '{printf "ipset add BL_HOSTS_NEW %s -exist\n", $1}' /tmp/AllBlacklistedHosts.txt >> $TFILE
    awk '{printf "ipset add BL_NETS_NEW %s -exist\n", $1}' /tmp/AllBlacklistedNets.txt >> $TFILE
    cat << EOT >> $TFILE
ipset swap BL_HOSTS_NEW BL_HOSTS
ipset destroy BL_HOSTS_NEW
ipset save BL_HOSTS > /var/tmp/BL_HOSTS.sav

ipset swap BL_NETS_NEW BL_NETS
ipset destroy BL_NETS_NEW
ipset save BL_NETS > /var/tmp/BL_NETS.sav

# Activate the "BLOCKLIST" checks if necessary
for CHAIN in INPUT FORWARD OUTPUT
do
    if [ -z "\$(iptables -nvL \$CHAIN 2> /dev/null | grep BL_HOSTS)" ]
    then
        iptables $WAIT -I \$CHAIN -m set --match-set BL_HOSTS src,dst -j DROP
        iptables $WAIT -I \$CHAIN -m set --match-set BL_HOSTS src,dst -j LOG --log-prefix "BL_HOSTS BLOCKED:" -m limit --limit 3/min --limit-burst 10
    fi
    if [ -z "\$(iptables -nvL \$CHAIN 2> /dev/null | grep BL_NETS)" ]
    then
        iptables $WAIT -I \$CHAIN -m set --match-set BL_NETS src,dst -j DROP
        iptables $WAIT -I \$CHAIN -m set --match-set BL_NETS src,dst -j LOG --log-prefix "BL_NETS BLOCKED:" -m limit --limit 3/min --limit-burst 10
    fi
done
EOT

    if [ ! -z "$COUNTRIES" ]
    then
        for cty in ${COUNTRIES//,/ }
        do
            [ -s /var/tmp/countrydb/$cty.txt ] || continue
            cat << EOT >> $TFILE

ipset list C_NETS_NEW_$cty 2> /dev/null && ipset destroy C_NETS_NEW_
ipset create C_NETS_NEW_$cty hash:net family inet hashsize 16384 maxelem $HOSTS_HASH_SIZE -exist
ipset create C_NETS_$cty hash:net family inet hashsize 16384 maxelem $HOSTS_HASH_SIZE -exist

EOT
            awk '{printf "ipset add C_NETS_NEW_'$cty' %s -exist\n", $1}' /var/tmp/countrydb/$cty.txt >> $TFILE
            cat << EOT >> $TFILE
ipset swap C_NETS_NEW_$cty C_NETS_$cty
if [ \$? -eq 0 ]
then
    ipset destroy C_NETS_NEW_$cty
else
    ipset rename C_NETS_NEW_$cty C_NETS_$cty
fi
ipset save C_NETS_$cty > /var/tmp/C_NETS_$cty.sav

# Activate the "Country List" checks if necessary
for CHAIN in INPUT FORWARD OUTPUT
do
    if [ -z "\$(iptables -nvL \$CHAIN 2> /dev/null | grep C_NETS_$cty)" ]
    then
        iptables $WAIT -I \$CHAIN -m set --match-set C_NETS_$cty src,dst -j DROP
        iptables $WAIT -I \$CHAIN -m set --match-set C_NETS_$cty src,dst -j LOG --log-prefix "C_NETS_$cty BLOCKED:" -m limit --limit 3/min --limit-burst 10
    fi
done
EOT
        done
    fi
else
    # Convert the IP addresses into "iptables" statements
    iptables-save > $TFILE.iptables-save 2> /dev/null
    head -n -2 $TFILE.iptables-save | grep -v 'A BL_' > $TFILE.iptables-restore
    awk '{printf "-A BL_HOSTS -s %s -j LOG --log-prefix \"BL_HOSTS BLOCKED: \"\n", $1;
     printf "-A BL_HOSTS -s %s -j DROP\n", $1;
     printf "-A BL_HOSTS -d %s -j LOG --log-prefix \"BL_HOSTS BLOCKED: \"\n", $1;
    printf "-A BL_HOSTS -d %s -j DROP\n", $1}' /tmp/AllBlacklistedHosts.txt >> $TFILE.iptables-restore
    awk '{printf "-A BL_NETS -s %s -j LOG --log-prefix \"BL_NETS BLOCKED: \"\n", $1;
     printf "-A BL_NETS -s %s -j DROP\n", $1;
     printf "-A BL_NETS -d %s -j LOG --log-prefix \"BL_NETS BLOCKED: \"\n", $1;
    printf "-A BL_NETS -d %s -j DROP\n", $1}' /tmp/AllBlacklistedNets.txt >> $TFILE.iptables-restore
    tail -n 2 $TFILE.iptables-save >> $TFILE.iptables-restore
    cp -f $TFILE.iptables-restore /var/tmp/iptables-restore.txt

    cat << EOT > $TFILE
# Last updated: $(date)
iptables --new BL_HOSTS &> /dev/null
iptables --new BL_NETS &> /dev/null
iptables --flush BL_HOSTS
iptables --flush BL_NETS

# First test whether the new ruleset is okay
iptables-restore -t $TFILE.iptables-restore &> /var/tmp/iptables-restore.debug
[ \$? -ne 0 ] && exit
# Actually apply the new ruleset
iptables-restore $TFILE.iptables-restore 2> /dev/null

# Activate the "BLOCKLIST" checks if necessary
for CHAIN in INPUT FORWARD OUTPUT
do
    if [ -z "\$(iptables -nvL \$CHAIN 2> /dev/null | grep BL_HOSTS)" ]
    then
        iptables $WAIT -I \$CHAIN -j BL_HOSTS
    fi
    if [ -z "\$(iptables -nvL \$CHAIN 2> /dev/null | grep BL_NETS)" ]
    then
        iptables $WAIT -I \$CHAIN -j BL_NETS
    fi
done
EOT
fi
[ -s $TFILE ] && source $TFILE

# We are done
exit 0
