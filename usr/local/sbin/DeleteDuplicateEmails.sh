#!/bin/bash
# Version 20240121-215241 checked into repository
################################################################
# (c) Copyright 2023 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/DeleteDuplicateEmails.sh
# Run this just a week - at most!

trap "rm -rf /tmp/$$*" EXIT

source /usr/local/include/loadcheck.sm

# See https://stackoverflow.com/questions/16908084/bash-script-to-calculate-time-elapsed
secs_to_human()
{
     echo "$(( ${1} / 3600 ))h $(( (${1} / 60) % 60 ))m $(( ${1} % 60 ))s"
}

# Do we need debugging (can be overwritten by program options)
DEBUG=''
LC_DEBUG=0
if [[ $- ==  *x* ]]
then
    DEBUG='-d'
    LC_DEBUG=1
fi

ADMIN_ID='admin'
ADMIN_PW=''
dryrun=''
O_DOMAIN=''
O_USER=''
while getopts "hnD:U:P:d" OPTION
do
    case "${OPTION}" in
    P)  ADMIN_PW="${OPTARG}"
        ;;
    n)  dryrun='-n'
        ;;
    D)  O_DOMAIN="${OPTARG}"
        ;;
    U)  O_USER="${OPTARG}"
        ;;
    d)  DEBUG='d'
        ;;
    h|*)
        cat << EOHELP
Usage: $0 [ options]

  -D domain   Specify domain to process [default=all]
  -U email    Specify email to process [default=all]
  -P password Specify password for "$ADMIN_ID"
  -d          Debug "imapdedup.py" [default=no]
  -n          Don't actually remove duplicate emails [default=no]

  For "dovecot" servers use a snippet like this to define a '$ADMIN_ID' user:

#!/bin/bash
# Define a dovecot "root" user and it's password
#  use 'https://consult.btoy1.net/rp.php' to get a good passwd
[ -z "\$(grep ^admin /etc/dovecot/passwd-rootusers)" ] && htpasswd b -c -s /etc/dovecot/passwd-rootusers admin password
chown vmail:dovecot /etc/dovecot/passwd-rootusers

cat << EOT > /tmp/dovecot-rootdb
# Define a list of root users in /etc/dovecot/passwd-rootusers
# See https://doc.dovecot.org/configuration_manual/authentication/root_users/
auth_master_user_separator = *
passdb {
  driver = passwd-file
  args = /etc/dovecot/passwd-rootusers
  master = yes
  result_success = continue
}
EOT
if [ -z "\$(grep passwd-rootusers /etc/dovecot/local.conf)" ]
then
    cat /tmp/dovecot-rootdb >> /etc/dovecot/local.conf
    dovecot -n &> /dev/null && systemctl restart dovecot.service
fi

EOHELP
        exit 1
        ;;
    esac
done
shift $((OPTIND-1))
if [ -z "$ADMIN_PW" ]
then
    echo "ERROR: No password specified for '$ADMIN_ID'"
    exit 1
fi

# The real start of the script is here!
LogFile='/root/DeleteDuplicateEmails.log'
START=$EPOCHSECONDS
date "+%F %T: Starting $0" >> $LogFile

# Determine the type of mail server
if [ -d /opt/zimbra ]
then
    SERVER_TYPE='Zimbra'
elif [ -d /opt/zextras ]
then
    SERVER_TYPE='Carbonio'
elif [ -x /usr/bin/doveadm ]
then
    SERVER_TYPE='Dovecot'
else
    echo "ERROR: Unsupported mail server type"
    exit 1
fi

# Ensure that we have "imapdedup.py"
[ -x /usr/local/bin/imapdedup.py ] || wget -qO /usr/local/bin/imapdedup.py https://gitlab.com/bluc/AdminTools/raw/master/usr/local/bin/imapdedup.py
chmod 0755 /usr/local/bin/imapdedup.py

if [ -n "${O_DOMAIN}" ]
then
    # Check just that one domain
    echo "${O_DOMAIN}" > /tmp/$$.AllDomains
else
    # Get all domains
    case ${SERVER_TYPE} in
    Z*)
        su - zimbra -c 'zmprov -l gad' | sort > /tmp/$$.AllDomains
        ;;
    C*)
        su - zextras -c 'carbonio -l gad' | sort > /tmp/$$.AllDomains
        ;;
    D*)
        doveadm user '*' | grep '@' | sed 's/^.*@//' | sort -u > /tmp/$$.AllDomains
        ;;
    esac
fi

while read -r DOMAIN
do
    date "+%F %T: Processing domain '$DOMAIN'" >> $LogFile
    # Get all accounts in this domain
    rm -f /tmp/$$.AllAccounts
    LoadCheck $LC_DEBUG
    if [ -n "${O_DOMAIN}" ]
    then
        # Check just that one user
        echo "${O_USER}" > /tmp/$$.AllAccounts
    else
        case ${SERVER_TYPE} in
        Z*)
            su - zimbra -c 'nice zmprov -l gaa' | grep "@$DOMAIN" | sort > /tmp/$$.AllAccounts
            ;;
        C*)
            su - zextras -c 'nice carbonio -l gaa' | grep "@$DOMAIN" | sort > /tmp/$$.AllAccounts
            ;;
        D*)
            doveadm user '*@'$DOMAIN | sort -u > /tmp/$$.AllAccounts
            ;;
        esac
    fi
    while read -r U
    do
        date "+%F %T: Processing user '$U'" >> $LogFile

        # Get all folders for the user
        rm -f /tmp/$$.folders
        LoadCheck $LC_DEBUG
        imapdedup.py -s localhost -p 143 $DEBUG -a "$ADMIN_ID" -w "$ADMIN_PW" -l -u ${U} | \
          grep -Eiv '^(Junk|Drafts|Trash)$' > /tmp/$$.folders
        while read -r FOLDER
        do
            LoadCheck $LC_DEBUG
            date "+%F %T: Removing duplicates in folder '$FOLDER' for '$U'" >> $LogFile
            nice imapdedup.py -s localhost -p 143 $DEBUG -a "$ADMIN_ID" -w "$ADMIN_PW" \
              ${dryrun} -m -c -u ${U} "$FOLDER" &>> $LogFile
            date "+%F %T: Folder '$FOLDER' for '$U' done" >> $LogFile
        done < /tmp/$$.folders
        date "+%F %T: User '$U' done" >> $LogFile
    done <  /tmp/$$.AllAccounts
    date "+%F %T: Domain '$DOMAIN' done" >> $LogFile
done < /tmp/$$.AllDomains

# Show any log entries for deletion
grep -aE '(now [0-9]* message|NOT.*would now be)' "$LogFile" > /tmp/$$.duplicates
if [ -s /tmp/$$.duplicates ]
then
    date "+%F %T: List of duplicates found:"
    cat /tmp/$$.duplicates
fi

# Truncate log file
CURLINES=$(sed -n '$=' "$LogFile")
if [ $CURLINES -gt 250000 ]
then
   # Truncate the file to 500000 lines
   LINES2DEL=$((CURLINES - 250000))
   sed -i -e "1,${LINES2DEL}d" "$LogFile"
fi

# We are done
ELAPSED=$(secs_to_human $((EPOCHSECONDS - START)))
date "+%F %T: $0 finished in $ELAPSED"
date "+%F %T: $0 finished in $ELAPSED" >> "$LogFile"
exit 0
