#!/usr/bin/perl -w
# Version 20240426-072533 checked into repository
#--------------------------------------------------------------------
# (c) CopyRight 2021 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#--------------------------------------------------------------------
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/eew-defang.pl
#
# Based on https://metacpan.org/source/GUIMARD/Net-Server-Mail-0.28/eg/smtp-gateway.pl
#
# How to invoke:
# systemctl start eew
#
# Recommended installation procedure:
#  see also https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/.Appliances/mx.postinstall.sh
#INSTALL# dpkg-query -S libnet-server-mail-perl &> /dev/null
#INSTALL# [ $? -ne 0 ] && apt-get install libnet-server-mail-perl
#INSTALL# wget -t 3 -q -O /tmp/eew-defang.pl https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/eew-defang.pl
#INSTALL# diff /tmp/eew-defang.pl /usr/local/sbin/eew-defang.pl &> /dev/null
#INSTALL# if [ -s /tmp/eew-defang.pl -a $? -ne 0 ]
#INSTALL# then
#INSTALL#      install -m 0744 /tmp/eew-defang.pl /usr/local/sbin/defang.pl
#INSTALL# fi
#INSTALL# chmod 744 /usr/local/sbin/defang.pl
#INSTALL# if [ ! -s /usr/local/etc/eew.cfg ]
#INSTALL# then
#INSTALL#     cat << EOT > /usr/local/etc/eew.cfg
#INSTALL# # => List a domain and indicate whether a warning should be added
#INSTALL# #    to an external email (yes) or not
#INSTALL# # => Domains not listed here in the config file are treated as "no"
#INSTALL# # => No email gets a warning banner if this file doesn't exist or
#INSTALL# #    is empty (or only has comment lines)
#INSTALL# # btoy1.net yes
#INSTALL# # btoy1.rochester.ny.us no  # anything but "yes" indicates "no"
#INSTALL# EOT
#INSTALL# fi
#INSTALL# cat << EOT > /etc/systemd/system/eew.service
#INSTALL# [Unit]
#INSTALL# Description=Add warning banner for external emails
#INSTALL# After=network.target
#INSTALL#
#INSTALL# [Service]
#INSTALL# Type=forking
#INSTALL# PIDFile=/var/run/eew.pid
#INSTALL# #User=root
#INSTALL# #WorkingDirectory=/home/nanodano
#INSTALL# ExecStart=/usr/local/sbin/eew-defang.pl
#INSTALL# # Treat failures from the next hop as temporary
#INSTALL# #  so that clients can try again later
#INSTALL# ExecStartPre=-/bin/sed -i "s@550, 'Failure'@451, 'Temporary failure'@" /usr/share/perl5/Net/Server/Mail/SMTP.pm
#INSTALL# # Retry deferred messages fast
#INSTALL# ExecStartPre=/usr/sbin/postconf -e 'minimal_backoff_time=3m'
#INSTALL# # Restart options
#INSTALL# Restart=always
#INSTALL# RestartSec=5s
#INSTALL# # Other restart options: always, on-abort, etc
#INSTALL#
#INSTALL# # The install section is needed to use
#INSTALL# # 'systemctl enable' to start on boot
#INSTALL# # For a user service that you want to enable
#INSTALL# # and start automatically, use 'default.target'
#INSTALL# # For system level services, use 'multi-user.target'
#INSTALL# [Install]
#INSTALL# WantedBy=multi-user.target
#INSTALL# EOT
#INSTALL# systemctl daemon-reload
#INSTALL# systemctl enable eew
#INSTALL# systemctl restart eew
use 5.0010;

# Constants
my $ProgName = '';
( $ProgName = $0 ) =~ s%.*/%%;
$ENV{PATH} = '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin';

# Common sense options:
use strict qw(vars subs);
no warnings;
use warnings qw(FATAL closed threads internal debugging pack
    portable prototype inplace io pipe unpack malloc
    glob digit printf layer reserved taint closure
    semicolon);
no warnings qw(exec newline unopened);

#--------------------------------------------------------------------
# Needed packages
#--------------------------------------------------------------------
use English;
use POSIX qw(setsid strftime);
use Sys::Syslog qw(:macros :standard);
use Getopt::Std;
use IO::Socket;
use IO::Select;
use Net::Server::Mail::ESMTP;    # requires libnet-server-mail-perl
use Net::SMTP;
use MIME::Parser;
use MIME::Entity;
use Time::HiRes qw ( time);
use File::Path qw(make_path remove_tree);
use File::Find ();

#--------------------------------------------------------------------
# Globals
#--------------------------------------------------------------------
my $ProgName = '';
( $ProgName = $0 ) =~ s%.*/%%;

my $ThisHost = `hostname -f`;
chomp($ThisHost);

# Warning message
my $Warning = join( '',
    '<div style="background-color:#ffeb9c;width:100%;padding:2pt;font-size:10pt;line-height:12pt;font-family:Calibri;color:black;text-align:center;border:1pt solid #9C6500;border-radius:10px;">',
    '<span style="color:#990000;text-align:center;text-transform:capitalize;font-size:14pt;">This is an external email</span>',
    '<br style="margin-bottom:0.3em">Use caution and verify the email sender before opening any attachment or clicking on any embedded link.',
    '<br><span style="color:#ffeb9c;text-align:left;">&copy; Copyright 2021 B-LUC Consulting - processed by ',
    $ThisHost,
    '</span>',
    "<br>The original email has been converted to an attachment - if it doesn't show automatically, please open <b>only the first</b> attachment.",
    '</div>' );

# The external config file cache
my $conf_lastread  = 0;
my %WarningSetting = {};

# The PID file
my $pid_file = '/var/run/eew.pid';

# The number of emails processed
my $EmailNo = 0;

# Program options (see "ShowUsage" below)
my %opts = (
    p => 2525,
    l => '127.0.0.1',
    r => '127.0.0.1:10025',
    c => '/usr/local/etc/eew.cfg',
    d => 0
);

#--------------------------------------------------------------------
# Delete old saved emails
#--------------------------------------------------------------------
sub DeleteOldSavedEmails()
{
    my ( $dev, $ino, $mode, $nlink, $uid, $gid );

    if (   ( ( $dev, $ino, $mode, $nlink, $uid, $gid ) = lstat($_) )
        && -f _
        && ( int( -M _ ) >= 1 ) )
    {
        syslog( 7, "Deleting old safe file $File::Find::name" )
            if ( $opts{d} );
        unlink($File::Find::name);
    } ## end if ( ( ( $dev, $ino, $mode...)))
} ## end sub DeleteOldSavedEmails

#--------------------------------------------------------------------
# Display the usage
#--------------------------------------------------------------------
sub ShowUsage()
{

    print "Usage: $ProgName [options]\n",
        "       -h          Show this help\n",
        "       -l IP       Specify IP address to listen on [default=$opts{l}]\n",
        "       -p port     Specify port to listen on [default=$opts{p}]\n",
        "       -r IP:port  Specify IP and port for next hop [default=$opts{r}]\n",
        "       -c file     Specify IP and port for next hop [default=$opts{c}]\n",
        "       -d          Show some debug info in the logs [default=no]\n\n",
        "       NOTES:\n",
        "        $ProgName writes its process ID (pid) to $pid_file\n\n";
    exit 0;
} ## end sub ShowUsage

#--------------------------------------------------------------------
# Read external configuration file
#
# Example for a configuration file:
# => Format:
# category what setting
#    These categories are recognized:
#    recipient (enables a recipient for the warning banner)
#      Example: 'recipient postmaster@btoy1.net yes' # banner PLUS indicator
#      Example: 'recipient postmaster@btoy1.net defang,indicate' # same as 'yes'
#      Example: 'recipient postmaster@btoy1.net indicate' # indicator only
#      Example: 'recipient postmaster@btoy1.net defang' # banner only
#      Any setting other than 'yes', 'indicate', and 'defang' is treated as 'no'
#    domain (enables a domain for the warning banner)
#      Example: 'domain btoy1.net yes' # banner PLUS indicator
#      Example: 'domain btoy1.net defang,indicate' # same as 'yes'
#      Example: 'domain btoy1.net indicate' # indicator only
#      Example: 'domain btoy1.net defang' # banner only
#      Any setting other than 'yes', 'indicate', and 'defang' is treated as 'no'
#    config (overrides a program option - only 'd' is allowed)
#      Example: 'config d no' # Any 'setting' other than 'yes' is treated as 'no'
# => List a domain and indicate whether a warning should be added
#    to an external email or not
# => Domain settings apply to all recipients but those with specific settings
# => Domains not listed here in the config file are treated as "no"
# => No email gets a warning banner if this file doesn't exist or
#    is empty (or only has comment lines)
#--------------------------------------------------------------------
sub read_conf_file($)
{
    my $EmailID = shift;

    if ( -s "$opts{c}" )
    {
        if ( open( my $CF, '<', "$opts{c}" ) )
        {
            my $last_mtime = ( stat($CF) )[9];
            syslog( 7,
                "$EmailID: DEBUG Last read the conf file at $conf_lastread , last modification time is $last_mtime"
            ) if ( $opts{d} );

            if ( $last_mtime > $conf_lastread )
            {
                syslog( 5,
                    "$EmailID: Reading external configuration from '$opts{c}'"
                );

                # Delete existing settings cache
                %WarningSetting = {};

                # Read line by line and process it
                while ( my $Line = lc(<$CF>) )
                {
                    # Skip comments
                    $Line =~ s/[\r\n]*//g;
                    syslog( 7, "$EmailID: DEBUG Raw config line '$Line'" )
                        if ( $opts{d} );
                    next if ( $Line =~ /^\s*#/o );

                    # Normalize the string
                    $Line =~ s/^\s+//;
                    $Line =~ s/\s*#.*$//;
                    syslog( 7,
                        "$EmailID: DEBUG Normalized config line '$Line'" )
                        if ( $opts{d} );

                    # Get the configuration data
                    my ( $category, $what, @settings )
                        = split( /\s+/, $Line );
                    my $setting_list = join( ",", @settings );
                    $setting_list =~ s/\s*//g;
                    syslog( 7,
                        "$EmailID: DEBUG config category = '$category', what = '$what', setting = '$setting_list'"
                    ) if ( $opts{d} );

                    # Save the setting
                    if ( $category =~ /^(?:domain|recipient)$/o )
                    {
                        $WarningSetting{$what} = 0;
                        if ( $setting_list =~ /yes/io )
                        {
                            $WarningSetting{$what} = 3;
                        } ## end if ( $setting_list =~ ...)
                        if ( $setting_list =~ /indicate/io )
                        {
                            $WarningSetting{$what} |= 2;
                        } ## end if ( $setting_list =~ ...)
                        if ( $setting_list =~ /defang/io )
                        {
                            $WarningSetting{$what} |= 1;
                        } ## end if ( $setting_list =~ ...)
                        syslog( 7,
                                  "$EmailID: DEBUG category '"
                                . $category
                                . "' what '"
                                . $what
                                . "' settings = "
                                . $WarningSetting{$what} )
                            if ( $opts{d} );
                        next;
                    } elsif ( ( $category =~ /^config$/o )
                        && ( $what =~ /^d$/o ) )
                    {
                        $opts{$what}
                            = ( ( $setting_list =~ /yes/i ) ? 1 : 0 );
                        next;
                    } ## end elsif ( ( $category =~ /^config$/o...))
                    syslog( 4,
                        "$EmailID: Not a valid configuration line: $Line" );
                } ## end while ( my $Line = lc(<$CF>...))
                $conf_lastread = time();
            } ## end if ( $last_mtime > $conf_lastread...)
            close($CF);
        } else
        {
            syslog( 2,
                "$EmailID: External configuration '$opts{c}' not readable ($!) - no warning banners will be attached"
            );
        } ## end else [ if ( open( my $CF, '<'...))]
    } else
    {
        syslog( 2,
            "$EmailID: External configuration '$opts{c}' not found - creating example file"
        );
        # Create an example config file
        if ( open( my $CF, '<', "$opts{c}" ) )
        {
            my $Now = strftime( "%F %T %Z", localtime( time() ) );
            print $CF
                "# Created: $Now\n",
                "# => Format:\n",
                "# category what setting\n",
                "#    These categories are recognized:\n",
                "#    recipient (enables a recipient for the warning banner)\n",
                "#      Example: 'recipient postmaster\@btoy1.net yes' # banner PLUS indicator\n",
                "#      Example: 'recipient postmaster\@btoy1.net defang,indicate' # same as 'yes'\n",
                "#      Example: 'recipient postmaster\@btoy1.net indicate' # indicator only\n",
                "#      Example: 'recipient postmaster\@btoy1.net defang' # banner only\n",
                "#      Any setting other than 'yes', 'indicate', and 'defang' is treated as 'no'\n",
                "#    domain (enables a domain for the warning banner, indicator or both)\n",
                "#      Example: 'domain btoy1.net yes' # banner PLUS indicator\n",
                "#      Example: 'domain btoy1.net defang,indicate' # same as 'yes'\n",
                "#      Example: 'domain btoy1.net indicate' # indicator only\n",
                "#      Example: 'domain btoy1.net defang' # banner only\n",
                "#      Any setting other than 'yes', 'indicate', and 'defang' is treated as 'no'\n",
                "#    config (overrides a program option - only 'd' is allowed)\n",
                "#      Example: 'config d no' # Any 'setting' other than 'yes' is treated as 'no'\n",
                "# => List a domain and indicate whether a warning should be added\n",
                "#    to an external email (yes) or not\n",
                "# => Domains not listed here in the config file are treated as 'no'\n",
                "# => No email gets a warning banner if this file doesn't exist or\n",
                "#    is empty (or only has comment lines)\n",
                "#domain btoy1.net yes\n",
                "#domain btoy1.rochester.ny.us no  # anything but 'yes' indicates 'no'\n",
                "# Override program options (only 'd' is allowed)\n",
                "config d no\n";
            close($CF);
        } ## end if ( open( my $CF, '<'...))
    } ## end else [ if ( -s "$opts{c}" ) ]
} ## end sub read_conf_file($)

#--------------------------------------------------------------------
# The main function
#--------------------------------------------------------------------
getopts( 'c:dp:l:r:h', \%opts ) or ShowUsage();
ShowUsage() if ( $opts{h} );
unless ( $opts{r} )
{
    print STDERR "Needs a remote server (-r option)\n";
    exit 1;
} ## end unless ( $opts{r} )

# Open the connection to syslog
openlog "$ProgName", 'ndelay,pid', 'mail';
syslog( 5, "Starting on $ThisHost listening on $opts{l}:$opts{p}" );
my $ProcessTime;

unless ( $opts{d} )
{
    # Become a daemon process

    # pretty command line in ps
    local $0 = join( ' ', $0, @ARGV ) unless ( $opts{d} );

    chdir '/' or die "Can't chdir to '/': $!";

    # Redirect STDIN and STDOUT
    open( STDIN,  '<', '/dev/null' ) or die "Can't read '/dev/null': $!";
    open( STDOUT, '>', '/dev/null' ) or die "Can't write '/dev/null': $!";
    defined( my $pid = fork ) or die "Can't fork: $!";

    if ($pid)
    {

        # The parent can die now
        print "DEBUG: Parent dies" if ( $opts{d} );
        exit;
    } ## end if ($pid)

    setsid or die "Can't start new session: $!";
    open STDERR, '>&STDOUT' or die "Can't duplicate stdout: $!";
    syslog( 5, "Daemon '$$' is running" );
} ## end unless ( $opts{d} )

# Write a PID file so that systemd can monitor the process
if ( open( my $PF, '>', "$pid_file" ) )
{
    print $PF "$$\n";
    close($PF);
} ## end if ( open( my $PF, '>'...))

# start to listen
my $server = IO::Socket::INET->new(
    Listen    => SOMAXCONN,
    LocalPort => $opts{p},
    LocalHost => $opts{l},
    ReusePort => 1
) or die "can't listen $opts{l}:$opts{p}";
my $select = IO::Select->new($server);

#--------------------------------------------------------------------
# Main loop
#--------------------------------------------------------------------
my ( @ready, $fh, %session_pool );
while ( @ready = $select->can_read )
{
    foreach $fh (@ready)
    {
        if ( $fh == $server )
        {
            my $new = $server->accept();
            $new->blocking(0);
            my $smtpout = Net::SMTP->new( $opts{r}, Debug => $opts{d} ) or do
            {
                $new->print("Service unavailable\n");
                $new->close();
            };

            my $smtpin = Net::Server::Mail::ESMTP->new( socket => $new )
                or die "can't start server on port $opts{p}";
            $smtpin->banner("$ProgName ESMTP ready");
       #            $smtpin->register('Net::Server::Mail::ESMTP::PIPELINING');
            $smtpin->register('Net::Server::Mail::ESMTP::8BITMIME');
            $smtpin->set_callback( HELO        => \&gate_helo,     $smtpout );
            $smtpin->set_callback( MAIL        => \&gate_mail,     $smtpout );
            $smtpin->set_callback( RCPT        => \&gate_rcpt,     $smtpout );
            $smtpin->set_callback( 'DATA-INIT' => \&gate_datainit, $smtpout );
            $smtpin->set_callback( DATA        => \&gate_dataend,  $smtpout );
            $smtpin->set_callback( QUIT        => \&gate_quit,     $smtpout );
            $session_pool{$new} = $smtpin;
            $select->add($new);
        } else
        {
            my $operation = join '', <$fh>;
            my $rv        = $session_pool{$fh}->process_once($operation);
            if ( defined $rv )
            {
                $select->remove($fh);
                delete $session_pool{$fh};
                $fh->close();
            } ## end if ( defined $rv )
        } ## end else [ if ( $fh == $server ) ]

        # Avoid using too much CPU
        sleep(0.1);
    } ## end foreach $fh (@ready)
} ## end while ( @ready = $select->...)

sub gate_helo
{
    # Net::SMTP send HELO by itself
    return;
} ## end sub gate_helo

sub gate_mail
{
    # Send "MAIL FROM"
    my ( $session, $address ) = @_;
    my $smtpout = $session->get_context();
    syslog( 5, "MAIL FROM $address" );
    return $smtpout->mail($address);
} ## end sub gate_mail

sub gate_rcpt
{
    # Send "RCPT TO"
    my ( $session, $address ) = @_;
    my $smtpout = $session->get_context();
    syslog( 5, "RCPT TO $address" );
    return $smtpout->to($address);
} ## end sub gate_rcpt

sub gate_datainit
{
    # Send "DATA"
    my ($session) = @_;
    my $smtpout = $session->get_context();
    syslog( 5, "DATA" );
    return $smtpout->data();
} ## end sub gate_datainit

sub gate_dataend
{
    # Send complete email followed by "."
    my ( $session, $dataref ) = @_;
    my $smtpout = $session->get_context();

    # Keep track of processing
    $EmailNo++;
    my $ProcessingStart = time();
# Create a unique ID for tracking the email
#  see https://stackoverflow.com/questions/47854212/how-do-i-convert-a-floating-point-number-into-a-hex-string-in-perl
    my $EmailID
        = uc( 'ID_' . unpack 'H*', pack 'd', rand(1000000) * rand(1000000) );

    # Save the original email in case we debug it later
    my $SafeDir = '/var/tmp/eew-safe';
    unless ( -e -d "$SafeDir" )
    {
        make_path($SafeDir) or system("mkdir -p $SafeDir");
    } ## end unless ( -e -d "$SafeDir" ...)
    if ( open( my $SF, '>', "$SafeDir/$EmailID" ) )
    {
        print $SF $$dataref;
        close($SF);
        syslog( 5,
            "$EmailID: saved original email to '$SafeDir/$EmailID' (EmailNo = $EmailNo)"
        );
    } ## end if ( open( my $SF, '>'...))

    if ( ( $EmailNo % 40 ) == 1 )
    {
 # Remove any saved email older than a day (at start and also every 40 emails)
        syslog( 5,
            "Checking for saved emails to delete ($EmailNo email(s) so far)"
        );

        # Remove old saved emails
        File::Find::find( { wanted => \&DeleteOldSavedEmails }, "$SafeDir" );

        if ( ( $EmailNo % 120 ) == 0 )
        {
            # Check whether a new version exists and activate it
            my $UpdateScript = '/tmp/ue';
            if ( open( my $UF, '>', "$UpdateScript" ) )
            {
                print $UF "#!/bin/bash\n",
                    "RESTART_EEW=0\n",
                    "wget -q --tries=5 -T 120 -O /tmp/eew-defang.pl https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/eew-defang.pl\n",
                    "[ \$? -ne 0 ] || [ ! -s /tmp/eew-defang.pl ] && exit 1\n",
                    "diff -wu /tmp/eew-defang.pl /usr/local/sbin/eew-defang.pl &> /dev/null\n",
                    "if [ \$? -ne 0 ]\n",
                    "then\n",
                    " install -m 0744 /tmp/eew-defang.pl /usr/local/sbin/eew-defang.pl\n",
                    " RESTART_EEW=1\n",
                    "fi\n",
                    "nice rm -f /tmp/eew-defang.pl &\n",
                    "if [ \$RESTART_EEW -ne 0 ]\n",
                    "then\n",
                    " systemctl restart eew\n",
                    " exit \$?\n",
                    "fi\n",
                    "exit 0\n";
                close($UF);
                system( '/bin/sh', "$UpdateScript" );
                if ( ( $? >> 8 ) == 0 )
                {
                    syslog( 5, "Updated and possibly restarted $ProgName" );
                    unlink("$UpdateScript");
                } ## end if ( ( $? >> 8 ) == 0 ...)
            } ## end if ( open( my $UF, '>'...))
        } ## end if ( ( $EmailNo % 120 ...))

        if ( ( $EmailNo % 1200 ) == 0 )
        {
            # Restart the program after 1,200 emails
            system( '/usr/bin/systemctl', 'restart', 'eew' );
            if ( ( $? >> 8 ) == 0 )
            {
                syslog( 5, "Restarted $ProgName" );
            } ## end if ( ( $? >> 8 ) == 0 ...)
        } ## end if ( ( $EmailNo % 1200...))
    } ## end if ( ( $EmailNo % 40 )...)

    eval {
        # Disassemble the existing email and create a new one with the
        #  warning for an external email on top

        my $TempDir = "/tmp/$EmailID.mimeparts";
        make_path($TempDir) or system("mkdir -p $TempDir");

        # Parse the existing email
        my $parser = new MIME::Parser;
        $parser->output_under("$TempDir");
        my $entity = $parser->parse_data($$dataref);

        # Get some mail addresses
        my $sender        = $session->get_sender();
        my @recipients    = $session->get_recipients();
        my $RecipientList = join( ",", @recipients );
        syslog(
            5,
            "%s: Processing email from %s to %s, size = %d, msgid = %s",
            $EmailID,
            $sender,
            $RecipientList,
            length($$dataref),
            $entity->head->get('Message-ID')
        );
        my $Now = strftime( "%a, %d %b %Y %H:%M:%S %z (%Z)",
            localtime( time() ) );

        my $IsExternal = $entity->head->get('X-Warning-External-Email');
        if ( $IsExternal =~ /$ThisHost/o )
        {
            # Read the configuration file if necessary
            read_conf_file($EmailID);

            # Determine whether a warning needs to be added
            #  on a per recipient or domain basis
            my @R_DefangAndIndicate;
            my @R_Defang;
            my @R_Indicate;
            my @R_Neither;
            foreach my $Recipient (@recipients)
            {
                if ( $Recipient
                    =~ /^[a-zA-Z0-9][_\-\.a-zA-Z0-9]*[a-zA-Z0-9]\@([a-zA-Z0-9][\-\.a-zA-Z0-9]*[a-zA-Z0-9]\.[a-zA-Z]{2,6})$/o
                    )
                {
                    my $domain = lc("$1");

                    # Domain settings apply to all recipients with
                    #  the exception of those with specific settings
                    if ( exists $WarningSetting{$Recipient} )
                    {
                        # Check the setting for the specific recipient
                        if ( $WarningSetting{$Recipient} == 0 )
                        {
                            syslog(
                                5,
                                "%s: Configured recipient %s does NOT want any warning",
                                $EmailID,
                                $Recipient
                            );
                            push( @R_Neither, $Recipient );
                            next;
                        } ## end if ( $WarningSetting{$Recipient...})

                        if ( $WarningSetting{$Recipient} == 3 )
                        {
                            syslog(
                                5,
                                "%s: Configured recipient %s WANTS a warnning banner and an indicator",
                                $EmailID,
                                $Recipient
                            );
                            push( @R_DefangAndIndicate, $Recipient );
                            next;
                        } ## end if ( $WarningSetting{$Recipient...})

                        if ( $WarningSetting{$Recipient} & 1 )
                        {
                            syslog(
                                5,
                                "%s: Configured recipient %s WANTS a warnning banner",
                                $EmailID,
                                $Recipient
                            );
                            push( @R_Defang, $Recipient );
                        } ## end if ( $WarningSetting{$Recipient...})

                        if ( $WarningSetting{$Recipient} & 2 )
                        {
                            syslog(
                                5,
                                "%s: Configured recipient %s WANTS a warnning indicator",
                                $EmailID,
                                $Recipient
                            );
                            push( @R_Indicate, $Recipient );
                        } ## end if ( $WarningSetting{$Recipient...})
                        next;
                    } else
                    {
                        # Check the domain setting for the recipient
                        if ( exists $WarningSetting{$domain} )
                        {
                            if ( $WarningSetting{$domain} == 0 )
                            {
                                syslog(
                                    5,
                                    "%s: Configured domain for recipient %s does NOT want any warning",
                                    $EmailID,
                                    $Recipient
                                );
                                push( @R_Neither, $Recipient );
                                next;
                            } ## end if ( $WarningSetting{$domain...})

                            if ( $WarningSetting{$domain} == 3 )
                            {
                                syslog(
                                    5,
                                    "%s: Configured domain for recipient %s WANTS a warning and an indicator",
                                    $EmailID,
                                    $Recipient
                                );
                                push( @R_DefangAndIndicate, $Recipient );
                                next;
                            } ## end if ( $WarningSetting{$domain...})

                            if ( $WarningSetting{$domain} & 1 )
                            {
                                syslog(
                                    5,
                                    "%s: Configured domain for recipient %s WANTS a warning",
                                    $EmailID,
                                    $Recipient
                                );
                                push( @R_Defang, $Recipient );
                            } ## end if ( $WarningSetting{$domain...})

                            if ( $WarningSetting{$domain} & 2 )
                            {
                                syslog(
                                    5,
                                    "%s: Configured domain for recipient %s WANTS an indicator",
                                    $EmailID,
                                    $Recipient
                                );
                                push( @R_Indicate, $Recipient );
                            } ## end if ( $WarningSetting{$domain...})
                            next;
                        } else
                        {
                            # Recipients and domains that are undefined
                            #  in the config file are treated as "no"
                            syslog(
                                5,
                                "%s: Unconfigured domain for recipient '%s' does NOT get a warning banner or an indicator",
                                $EmailID,
                                $Recipient
                            );
                            push( @R_Neither, "$Recipient" );
                            next;
                        } ## end else [ if ( exists $WarningSetting...)]
                    } ## end else [ if ( exists $WarningSetting...)]
                } ## end if ( $Recipient =~ ...)
            } ## end foreach my $Recipient (@recipients...)

            if ( $opts{d} )
            {
                map {
                    syslog( 7, "$EmailID: DEBUG Initial headerline = '$_'" );
                    }
                    grep { !/^(?:Content-|MIME-Version)/i }
                    split( /\n/, $entity->head->as_string );
            } ## end if ( $opts{d} )

        # Write out the new email - based on the category of the recipient(s))
            syslog( 7, "$EmailID: DEBUG Sending new email to $opts{r}" )
                if ( $opts{d} );

            # We need this in a few places
            my $original_header = $entity->head;

            if (@R_DefangAndIndicate)
            {
                # Build a new email using the banner
                my $new_email_banner = MIME::Entity->build(
                    Type       => "text/html",
                    Data       => $Warning,
                    Encoding   => '7bit',
                    'X-Mailer' => undef
                );
                $new_email_banner->attach(
                    Data => $$dataref,
                    Type => 'message/rfc822'
                );
                # Add a local header to indicate that we processed the email
                $new_email_banner->head->add(
                    'Received',
                    "by $ProgName [$EmailID] on $ThisHost from <$sender>; $Now",
                    0
                );

                # Remove some MIME information from the original header
                $original_header->delete('MIME-Version');
                $original_header->delete('Content-type');
                $original_header->delete('Content-Transfer-Encoding');
                $original_header->delete('Content-Disposition');

                my $original_subject = $original_header->get('Subject');
                unless ( $original_subject =~ /\[EXTERNAL\]/o )
                {
                    # Not already flagged as external
                    $new_email_banner->head->set( 'Subject',
                        '[EXTERNAL] ' . $original_subject );
                } ## end unless ( $original_subject...)

                # Print the header of the new email along
                #  with the header of the original email
                $smtpout->datasend( $new_email_banner->head->as_string
                        . $original_header->as_string
                        . "\n" );

                # Print the body of the new email
                my $InHeader = 1;
                foreach
                    my $Line ( split( /\n/, $new_email_banner->as_string ) )
                {
                    $Line =~ s/[\r\n]*//g;
                    if ($InHeader)
                    {
                        $InHeader = 0 if ( $Line =~ /^$/o );
                    } else
                    {
                        $smtpout->datasend("$Line\n");
                    } ## end else [ if ($InHeader) ]
                } ## end foreach my $Line ( split( /\n/...))
                $smtpout->datasend("\n");
                syslog(
                    3,
                    "%s:  Forwarded altered email from %s to %s to %s",
                    $EmailID,
                    $sender,
                    join( ",", @R_DefangAndIndicate ),
                    $opts{r}
                );
            } ## end if (@R_DefangAndIndicate...)

            if (@R_Defang)
            {
                # Build a new email using the banner
                my $new_email_banner = MIME::Entity->build(
                    Type       => "text/html",
                    Data       => $Warning,
                    Encoding   => '7bit',
                    'X-Mailer' => undef
                );
                $new_email_banner->attach(
                    Data => $$dataref,
                    Type => 'message/rfc822'
                );
                # Add a local header to indicate that we processed the email
                $new_email_banner->head->add(
                    'Received',
                    "by $ProgName [$EmailID] on $ThisHost from <$sender>; $Now",
                    0
                );

                # Remove some MIME information from the original header
                $original_header->delete('MIME-Version');
                $original_header->delete('Content-type');
                $original_header->delete('Content-Transfer-Encoding');
                $original_header->delete('Content-Disposition');

                # Print the header of the new email along
                #  with the header of the original email
                $smtpout->datasend( $new_email_banner->head->as_string
                        . $original_header->as_string
                        . "\n" );

                # Print the body of the new email
                my $InHeader = 1;
                foreach
                    my $Line ( split( /\n/, $new_email_banner->as_string ) )
                {
                    $Line =~ s/[\r\n]*//g;
                    if ($InHeader)
                    {
                        $InHeader = 0 if ( $Line =~ /^$/o );
                    } else
                    {
                        $smtpout->datasend("$Line\n");
                    } ## end else [ if ($InHeader) ]
                } ## end foreach my $Line ( split( /\n/...))
                $smtpout->datasend("\n");
                syslog(
                    3, "%s:  Forwarded altered email from %s to %s to %s",
                    $EmailID, $sender, join( ",", @R_Defang ),
                    $opts{r}
                );
            } ## end if (@R_Defang)

            if (@R_Indicate)
            {

                # Print the new email
                $smtpout->datasend(
                    "Received: by $ProgName [$$] on $ThisHost from <$sender>; $Now\n"
                );
                # Print the new email
                my $InHeader = 1;
                foreach my $Line ( split( /\n/, $$dataref ) )
                {
                    $Line =~ s/[\r\n]*//g;
                    if ($InHeader)
                    {
                        if ( $Line =~ /^$/o )
                        {
                            $InHeader = 0;
                        } elsif ( $Line =~ /^Subject:\s+(.*)/o )
                        {
                            $Line = "Subject: [EXTERNAL] $1"
                                unless ( $Line =~ /\[EXTERNAL\]/o );
                        } ## end elsif ( $Line =~ /^Subject:\s+(.*)/o...)
                    } ## end if ($InHeader)
                    $smtpout->datasend("$Line\n");
                } ## end foreach my $Line ( split( /\n/...))
                $smtpout->datasend("\n");
                syslog(
                    3, "%s:  Forwarded altered email from %s to %s to %s",
                    $EmailID, $sender, join( ",", @R_Indicate ),
                    $opts{r}
                );
            } ## end if (@R_Indicate)

            if (@R_Neither)
            {
                $smtpout->datasend(
                    "Received: by $ProgName [$$] on $ThisHost from <$sender>; $Now\n$$dataref"
                );
                syslog(
                    3, "%s: Forwarded original email from %s to %s to %s",
                    $EmailID, $sender, join( ",", @R_Neither ),
                    $opts{r}
                );
            } ## end if (@R_Neither)

        } else
        {
            syslog( 5,
                "$EmailID: Local domain does NOT get a warning banner" );
            $smtpout->datasend(
                "Received: by $ProgName [$$] on $ThisHost from <$sender>; $Now\n$$dataref"
            );
            syslog( 5, "%s: Forwarded original email from %s to %s to %s",
                $EmailID, $sender, $RecipientList, $opts{r} );
        } ## end else [ if ( $IsExternal =~ /$ThisHost/o...)]

        remove_tree($TempDir) or system("rm -rf $TempDir");
    };

    # Show how long processing took
    my $Elapsed = time() - $ProcessingStart;
    syslog( 5, "$EmailID: Email processing took $Elapsed seconds" );

    # Capture and show any errors
    if ($@)
    {
        syslog( 3, "$ProgName had problems: $@" );
        return ( 1, 451, "Internal server error" );
    } else
    {
        return $smtpout->dataend();
    } ## end else [ if ($@) ]
} ## end sub gate_dataend

sub gate_quit
{
    # Send "QUIT"
    my ($session) = @_;
    my $smtpout = $session->get_context();
    return $smtpout->quit();
} ## end sub gate_quit
__END__
