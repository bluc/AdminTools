#!/bin/bash
################################################################
# (c) Copyright 2020 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/GetKMon.sh

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            

# Determine the download URL and get the tarball
KMON_URL='https://github.com'$(wget -q -O - 'https://github.com/orhun/kmon/releases' | \
  grep -m 1 'tar.gz' | sed 's/^.*href="//;s/.tar.gz.*/.tar.gz/')
wget -q -O /tmp/kmon.tar.gz "$KMON_URL"

# Extract the tarball
rm -rf /tmp/kmon
mkdir -p /tmp/kmon
cd /tmp/kmon
tar xvzf /tmp/kmon.tar.gz

# Some parting words
cat << EOT
The kernel monitor tools is in /tmp/kmon/kmon.

For help please see https://github.com/orhun/kmon
EOT

# We are done
exit 0
