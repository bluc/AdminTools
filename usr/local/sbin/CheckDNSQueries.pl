#!/usr/bin/perl -Tw
#--------------------------------------------------------------------
# (c) CopyRight 2017 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#--------------------------------------------------------------------
use 5.0010;

# Constants
my $ProgName = '';
( $ProgName = $0 ) =~ s%.*/%%;
my $CopyRight = "(c) CopyRight 2017 B-LUC Consulting Thomas Bullinger";

# Common sense options:
use strict qw(vars subs);
no warnings;
use warnings qw(FATAL closed threads internal debugging pack
    portable prototype inplace io pipe unpack malloc
    glob digit printf layer reserved taint closure
    semicolon);
no warnings qw(exec newline unopened);

#--------------------------------------------------------------------
# Needed packages
#--------------------------------------------------------------------
use Getopt::Std;

#--------------------------------------------------------------------
# Globals
#--------------------------------------------------------------------
$ENV{PATH} = '/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin';

# Program options
our $opt_i = '/var/log/named/queries.log';
our $opt_d = 0;
our $opt_h = 0;

my %SuspDomain       = {};
my %SuspClient       = {};
my %SuspClientDomain = {};

#--------------------------------------------------------------------
# Display the usage
#--------------------------------------------------------------------
sub ShowUsage()
{

    print "Usage: $ProgName [options]\n",
        "       -i file    Specify input file [default='$opt_i']\n",
        "       -h                 Show this help [default=no]\n",
        "       -d                 Show some debug info on STDERR [default=no]\n";

    exit 0;
} ## end sub ShowUsage

#--------------------------------------------------------------------
# Main function
#--------------------------------------------------------------------
$|++;
print "$ProgName\n$CopyRight\n\n";

# Get possible options
getopts('dhi:') or ShowUsage();
ShowUsage() if ($opt_h);

# Get data from DNS queries log
open( QL, '<', "$opt_i" )
    or die "ERROR: Can not open '$opt_i': $!";

while ( my $Line = <QL> )
{
    # Ignore entries from local host
    next if ( $Line =~ /client\s+127\./o );

    # Get the client IP and the domain name
    my ( undef, undef, undef, $client, $domain )
        = split( /\s+/, $Line );
    $client =~ s/#\d+//g;
    $domain =~ s/[\(\):]//g;
    #warn "client = $client, domain = $domain";

    # We only care about domain named 52 chars or longer
    next unless ( length($domain) );
    next if ( length($domain) < 52 );

    # Get the components of the domain name
    my @dparts = split( /\./, $domain );
    # We only care about the 1st compoment with 25 or more chars
    next if ( length( $dparts[0] ) < 25 );

    # Save the client and domain
    $SuspClient{"$client"}++;
    $SuspDomain{"$domain"}++;
    $SuspClientDomain{ "$client" . ' -> ' . "$domain" }++;
} ## end while ( my $Line = <QL> )

close(QL);

# Show what we found
foreach my $domain (
    sort { $SuspDomain{$b} <=> $SuspDomain{$a} }
    keys %SuspDomain
    )
{
    # Only look at anything larger than 10 occurences
    next if ( $SuspDomain{"$domain"} < 10 );
    print "Possibly suspcious domain: $domain (",
        $SuspDomain{"$domain"}, ")\n";
} ## end foreach my $domain ( sort {...})
print "\n";
foreach my $client (
    sort { $SuspClient{$b} <=> $SuspClient{$a} }
    keys %SuspClient
    )
{
    # Only look at anything larger than 10 occurences
    next if ( $SuspClient{"$client"} < 10 );
    print "Possibly suspcious client: $client (",
        $SuspClient{"$client"}, ")\n";
} ## end foreach my $client ( sort {...})
print "\n";
foreach my $clientdomain (
    sort { $SuspClientDomain{$b} <=> $SuspClientDomain{$a} }
    keys %SuspClientDomain
    )
{
    # Only look at anything larger than 10 occurences
    next if ( $SuspClientDomain{"$clientdomain"} < 10 );
    print "Possibly suspcious client to domain lookups: $clientdomain (",
        $SuspClientDomain{"$clientdomain"}, ")\n";
} ## end foreach my $clientdomain ( ...)

# We are done
exit 0;
__END__
