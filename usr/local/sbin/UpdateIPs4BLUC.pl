#!/usr/bin/perl -Tw
# Version 20221031-073354 checked into repository
#--------------------------------------------------------------------
# (c) CopyRight 2014-2020 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#--------------------------------------------------------------------
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/UpdateIPs4BLUC.pl
# Recommended invocation via a cron job:
# Update the IPs for B-LUC and iperf
#*/3 * * * *     root    [ -x /usr/local/sbin/UpdateIPs4BLUC.pl ] && /usr/local/sbin/UpdateIPs4BLUC.pl -w 60
use 5.0010;

# Constants
my $ProgName = '';
( $ProgName = $0 ) =~ s%.*/%%;

# Common sense options:
use strict qw(vars subs);
no warnings;
use warnings qw(FATAL closed threads internal debugging pack
    portable prototype inplace io pipe unpack malloc
    glob digit printf layer reserved taint closure
    semicolon);
no warnings qw(exec newline unopened);

#--------------------------------------------------------------------
# Needed packages
#--------------------------------------------------------------------
use Socket;
use Sys::Syslog qw(:macros :standard);
use File::Compare;
use Getopt::Std;

#--------------------------------------------------------------------
# Globals
#--------------------------------------------------------------------
local $ENV{PATH} = '/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin';

# Program options
our $opt_d = 0;
our $opt_h = 0;
our $opt_w = 0;

# Keep a tally of the number of lines in config files
my %LineNo = (
    '/etc/firehol/firehol.conf' => 0,
    '/tmp/firehol.conf'         => 0,
    '/etc/ssh/sshd_config'      => 0,
    '/tmp/sshd_config'          => 0
);

# The current minute
my ( undef, $CurMin ) = localtime(time);

#--------------------------------------------------------------------
# Display the usage
#--------------------------------------------------------------------
sub ShowUsage()
{

    print "Usage: $ProgName [options]\n",
        "       -w seconds   Wait <seconds> before updating anything [default=$opt_w]\n",
        "       -h           Show this help [default=no]\n",
        "       -d           Show some debug info on STDERR [default=no]\n";

    exit 0;
} ## end sub ShowUsage

#--------------------------------------------------------------------
# Main function
#--------------------------------------------------------------------
$|++;

#--------------------------------------------------------------------
# Get options
getopts('dhw:') or ShowUsage();
ShowUsage() if ( ($opt_h) or ( $opt_w !~ /\d+/ ) );

if ($opt_w)
{
    my $Wait = abs($opt_w);
    warn "DEBUG: Waiting $Wait second(s)\n" if ($opt_d);
    sleep($Wait);
} ## end if ($opt_w)

openlog "$ProgName", 'ndelay,pid', 'daemon';

#--------------------------------------------------------------------
# Get all IP addresses for B-LUC
my %BLUC_IP = {};
foreach my $HName ( 'cl1.btoy1.net', 'cl2.btoy1.net', 'btoy1.mooo.com' )
{
    # Try hard to get the IP address(es)
    for ( my $i = 1; $i <= 3; $i++ )
    {
        warn "DEBUG: Try #$i to get IP for $HName\n" if ($opt_d);
        if ( $HName =~ /mooo\.com$/ )
        {
            # Special case - check with the authoritave DNS server
            if (open(
                    my $DNS_Request,
                    '-|', "dig \@ns4.afraid.org +short $HName 2> /dev/null"
                )
                )
            {
                $BLUC_IP{"$HName"} = <$DNS_Request>;
                chomp $BLUC_IP{"$HName"};
                close $DNS_Request;
            } ## end if ( open( my $DNS_Request...))
        } else
        {
            # Special case - check with a public DNS server
            if (open(
                    my $DNS_Request,
                    '-|', "dig \@1.1.1.1 +short $HName 2> /dev/null"
                )
                )
            {
                $BLUC_IP{"$HName"} = <$DNS_Request>;
                chomp $BLUC_IP{"$HName"};
                close $DNS_Request;
            } ## end if ( open( my $DNS_Request...))
        } ## end else [ if ( $HName =~ /mooo\.com$/...)]
        if ( $BLUC_IP{"$HName"} =~ /connection timed out/o )
        {
            $BLUC_IP{"$HName"} = '';
            next;
        } ## end if ( $BLUC_IP{"$HName"...})
        last
            if (( defined $BLUC_IP{"$HName"} )
            and ( length $BLUC_IP{"$HName"} ) );
    } ## end for ( my $i = 1; $i <= ...)
    $BLUC_IP{"$HName"} = '127.0.0.1'
        unless ( ( defined $BLUC_IP{"$HName"} )
        and ( length $BLUC_IP{"$HName"} ) );
    warn "DEBUG: $HName is '" . $BLUC_IP{"$HName"} . "'\n" if ($opt_d);
} ## end foreach my $HName ( 'cl1.btoy1.net'...)

#--------------------------------------------------------------------
# Update firehol for B-LUC external IP address
if ( -f '/etc/firehol/firehol.conf' )
{
    if ( open( my $FH_New, '>', '/tmp/firehol.conf' ) )
    {
        if ( open( my $FH_Existing, '<', '/etc/firehol/firehol.conf' ) )
        {
            while ( my $Line = <$FH_Existing> )
            {
                chomp($Line);
                $LineNo{'/etc/firehol/firehol.conf'}++;
                if ( $Line =~ /^b(toy|luc)/o )
                {

                    # Use the correct IP addresses
                    $Line = "bluc='";
                    for ( sort ( values %BLUC_IP ) ) { $Line .= "$_ " }
                    $Line .= "'";
                    $Line =~ s/' /'/g;
                    $Line =~ s/ '/'/g;
                    $Line =~ s/\s+/ /g;
                } elsif ( $Line =~ /btoy1/o )
                {

                    # Use "bluc" as variable in firehol.conf
                    $Line =~ s/btoy1/bluc/g;
                } ## end elsif ( $Line =~ /btoy1/o...)
                print $FH_New "$Line\n";
                $LineNo{'/tmp/firehol.conf'}++;
            } ## end while ( my $Line = <$FH_Existing>...)
            close($FH_Existing);
        } ## end if ( open( my $FH_Existing...))
        close($FH_New);
    } ## end if ( open( my $FH_New,...))

    if ($opt_d)
    {
        for my $F ( '/etc/firehol/firehol.conf', '/tmp/firehol.conf' )
        {
            warn "DEBUG: Lines in $F: " . $LineNo{"$F"} . "\n";
        } ## end for my $F ( '/etc/firehol/firehol.conf'...)
    } ## end if ($opt_d)

    # Compare the two firehol configuration files
    if ( -s '/tmp/firehol.conf' )
    {
        if ( -s '/etc/firehol/firehol.conf' )
        {
            # Compare the two firehol configuration files
            if ( $LineNo{'/tmp/firehol.conf'}
                == $LineNo{'/etc/firehol/firehol.conf'} )
            {
                if (compare( '/etc/firehol/firehol.conf',
                        '/tmp/firehol.conf' ) == 1
                    )
                {

                    # Update the existing file
                    print
                        "Updating /etc/firehol/firehol.conf and restarting firewall\n";
                    system(
                        'diff',              '-wu',
                        '/tmp/firehol.conf', '/etc/firehol/firehol.conf'
                    );
                    syslog( 5,
                        'Updating /etc/firehol/firehol.conf and restarting firewall'
                    );
                    my $Cmd
                        = 'install --mode=600 --owner=root --group=root --backup=numbered /tmp/firehol.conf /etc/firehol/firehol.conf';
                    warn "DEBUG: Cmd = '$Cmd'\n" if ($opt_d);
                    system("$Cmd; service firehol condrestart");
                } else
                {
                    syslog( 5, '/etc/firehol/firehol.conf is unchanged' );
                } ## end else [ if ( compare( '/etc/firehol/firehol.conf'...))]
            } ## end if ( $LineNo{'/tmp/firehol.conf'...})
        } else
        {
            syslog( 5, '/etc/firehol/firehol.conf is empty' );
        } ## end else [ if ( -s '/etc/firehol/firehol.conf'...)]
    } else
    {
        syslog( 5, '/tmp/firehol.conf is empty' );
    } ## end else [ if ( -s '/tmp/firehol.conf'...)]

    if ( $CurMin == 12 )
    {
        # Delete any backup file older than a week
        syslog( 5, 'Deleting old backups of /etc/firehol/firehol.conf' );
        system(
            'find /etc/firehol -type f -name "firehol.conf.~*" -mtime +7 -delete'
        );
    } ## end if ( $CurMin == 12 )
} else
{
    syslog( 5, '/etc/firehol/firehol.conf is not present on this host' );
    if ( -s '/etc/firehol/firehol.conf.~.1~' )
    {
        syslog( 5, 'Restoring /etc/firehol/firehol.conf from backup' );
        system(
            'cp /etc/firehol/firehol.conf.~.1~ /etc/firehol/firehol.conf; service firehol restart'
        );
    } ## end if ( -s '/etc/firehol/firehol.conf.~.1~'...)
} ## end else [ if ( -f '/etc/firehol/firehol.conf'...)]
unlink('/tmp/firehol.conf');

#--------------------------------------------------------------------
# We are done
closelog;
exit(0);
__END__
