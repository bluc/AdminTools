#!/bin/bash
# Version 20221013-233641 checked into repository
################################################################
# (c) Copyright 2020 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/pihole.postinstall.sh

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            

TFILE=$(mktemp /tmp/$$XXXXXXXX)

#--------------------------------------------------------------------
# We need "git" and "chrony"
for P in git chrony
do
    dpkg-query -S git > /dev/null 2>&1
    [ $? -ne 0 ] && apt-get install git
done

#--------------------------------------------------------------------
# Function to convert netmasks into CIDR notation and back
# See: https://forums.gentoo.org/viewtopic-t-888736-start-0.html
function mask2cdr ()
{
   # Assumes there's no "255." after a non-255 byte in the mask
   local x=${1##*255.}
   set -- 0^^^128^192^224^240^248^252^254^ $(( (${#1} - ${#x})*2 )) ${x%%.*}
   x=${1%%$3*}
   echo $(( $2 + (${#x}/4) ))
}

#--------------------------------------------------------------------
if [ -x /usr/local/bin/pihole ]
then
    # Upgrade pi-hole
    pihole -up
else
    if [ -s /etc/firehol/firehol.conf ]
    then
        # Disable local firewall for now
        firehol stop

        # Adapt firehol config
        if [ -z "$(grep bellow /etc/firehol/firehol.conf)" ]
        then
            LOCALIF=$(ip route get 8.8.8.8 | awk '/via/{print $5; exit}')
            DEFAULT_GW=$(ip route get 8.8.8.8 | awk '/via/{print $3}')
            LOCALIP=$(ifconfig $LOCALIF | sed -n "s/.*inet addr:\([0-9.]*\).*/\1/p")
            [ -z "$LOCALIP" ] && LOCALIP=$(ifconfig $LOCALIF | sed -n "s/.*inet \([0-9.]*\).*/\1/p")
            [ -z "$LOCALIP" ] && LOCALIP=$(ip addr list $LOCALIF | sed -n "s/.*inet \([0-9.]*\).*/\1/p")
            LOCALMASK=$(ifconfig $LOCALIF | sed -n -e 's/.*Mask:\(.*\)$/\1/p')
            [ -z "$LOCALMASK" ] && LOCALMASK=$(ifconfig $LOCALIF | awk '/netmask/ {print $4}')
            # From: http://www.routertech.org/viewtopic.php?t=1609
            l="${LOCALIP%.*}";r="${LOCALIP#*.}";n="${LOCALMASK%.*}";m="${LOCALMASK#*.}"
            LOCALNET=$((${LOCALIP%%.*}&${LOCALMASK%%.*})).$((${r%%.*}&${m%%.*})).$((${l##*.}&${n##*.})).$((${LOCALIP##*.}&${LOCALMASK##*.}))
            CIDRMASK=$(mask2cdr $LOCALMASK)

            # Get the IP address for B-LUC
            BLUC_CL1=$(dig +short cl1.btoy1.net)
            BLUC_CL2=$(dig +short cl2.btoy1.net)
            BLUC_MX=$(dig +short btoy1.mooo.com)
            cat << EOT > /tmp/firehol.conf
#!/sbin/firehol
# : firehol.sh,v 1.273 2008/07/31 00:46:41 ktsaou Exp $
#
# This config will have the same effect as NO PROTECTION!
# Everything that found to be running, is allowed.
# YOU SHOULD NEVER USE THIS CONFIG AS-IS.
#
# Date: Fri Oct 15 07:38:13 EDT 2010 on host linux2
#
# IMPORTANT:
# The TODOs bellow, are *YOUR* to-dos!
#

bluc='$BLUC_CL1 $BLUC_CL2 $BLUC_MX'
home_net='${LOCALNET}/${CIDRMASK}'

# Fix some TOS values
# See: http://www.docum.org/docum.org/faq/cache/49.html
# and: https://github.com/ktsaou/firehol/blob/master/sbin/firehol.in
iptables -t mangle -N ackfix
iptables -t mangle -A ackfix -m tos ! --tos Normal-Service -j RETURN
iptables -t mangle -A ackfix -p tcp -m length --length 0:128 -j TOS --set-tos Minimize-Delay
iptables -t mangle -A ackfix -p tcp -m length --length 128: -j TOS --set-tos Maximize-Throughput
iptables -t mangle -A ackfix -j RETURN
iptables -t mangle -I POSTROUTING -p tcp -m tcp --tcp-flags SYN,RST,ACK ACK -j ackfix

iptables -t mangle -N tosfix
iptables -t mangle -A tosfix -p tcp -m length --length 0:512 -j RETURN
iptables -t mangle -A tosfix -m limit --limit 2/s --limit-burst 10 -j RETURN
iptables -t mangle -A tosfix -j TOS --set-tos Maximize-Throughput
iptables -t mangle -A tosfix -j RETURN
iptables -t mangle -I POSTROUTING -p tcp -m tos --tos Minimize-Delay -j tosfix

# Interface No 1a - frontend (public).
# The purpose of this interface4 is to control the traffic
# on the ${LOCALIF} interface4 with IP 192.168.1.53 (net: "192.168.1.0/24").
interface4 ${LOCALIF} internal_1 src "\${home_net} 0.0.0.0"

        # The default policy is DROP. You can be more polite with REJECT.
        # Prefer to be polite on your own clients to prevent timeouts.
        policy drop

        # Don't trust the clients behind ${LOCALIF} (net "${LOCALNET}/${CIDRMASK}")
        protection strong 75/sec 50

        # Here are the services listening on ${LOCALIF}.
        server4 "ssh http dns ping ntp" accept src "\${home_net}"
        server4 "dhcp" accept src "0.0.0.0"

        # The following means that this machine can REQUEST anything via ${LOCALIF}.
        client4 all accept

# Interface No 1b - frontend (public).
# The purpose of this interface4 is to control the traffic
# from/to unknown networks behind the default gateway
interface4 ${LOCALIF} external_1 src not "\${home_net} 0.0.0.0"

        # The default policy is DROP. You can be more polite with REJECT.
        policy drop

        # Don't trust the clients behind $LOCALIF (net not "\${UNROUTABLE_IPS} ${LOCALNET}/${CIDRMASK}")
        protection strong 75/sec 50

        # Here are the services listening on ${LOCALIF}.
        # TODO: Normally, you will have to remove those not needed.
        server4 "ssh" accept src "\${bluc}"
        server4 ping accept

        # Portscan defense
        iptables -A in_external_1 -m psd -j LOG --log-prefix 'IN-ISP-Portscan'
        iptables -A in_external_1 -m psd -j DROP

        # The following means that this machine can REQUEST anything via ${LOCALIF}.
        client4 all accept
EOT

             # Create a script to set the local IP address
             cat << EOT > /usr/local/sbin/SetNetwork.sh
#!/bin/bash
################################################################
# (c) Copyright 2020 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=\${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/\$PROG.lock
if [ -f \$LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=\$(< \$LOCKFILE)
    [ -z "\$(ps h -p \$MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f \$LOCKFILE /tmp/\$\$*" EXIT
echo "\$\$" > \$LOCKFILE            

#--------------------------------------------------------------------
# Disable IPv6
for IF in \$(awk -F: '/:/{print \$1}' /proc/net/dev)
do
    [ \$(sysctl -n net.ipv6.conf.\${IF}.disable_ipv6) -eq 0 ] && sysctl -e net.ipv6.conf.\${IF}.disable_ipv6=1
done

#--------------------------------------------------------------------
# Set the local IP address
if [[ ! \$(ip address show dev ${LOCALIF}) =~ ${LOCALIP} ]]
then
    ip address add ${LOCALIP}/${CIDRMASK} dev ${LOCALIF}
    ip link set ${LOCALIF} up
    ip route add ${LOCALNET}/${CIDRMASK} dev ${LOCALIF}
    ip route add default via ${DEFAULT_GW}
fi

#--------------------------------------------------------------------
# Restart pihole if necessary
[ -z "\$(pidof pihole-FTL)" ] || [ -z "\$(netstat -nuap | grep '^udp .*[0-9]:53.*pihole-FTL')" ] && service pihole-FTL restart

#--------------------------------------------------------------------
# We are done
exit 0
EOT
             chmod 744 /usr/local/sbin/SetNetwork.sh
        fi
    fi

    # Create a script to get the external IP address of the device
    cat << EOT > /usr/local/sbin/GetExternalIP.sh
#!/bin/bash
################################################################
# (c) Copyright 2020 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=\${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/\$PROG.lock
if [ -f \$LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=\$(< \$LOCKFILE)
    [ -z "\$(ps h -p \$MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f \$LOCKFILE /tmp/\$\$*" EXIT
echo "\$\$" > \$LOCKFILE            

THISHOST=\$(hostname)
[[ \$THISHOST == *.* ]] || THISHOST=\$(hostname -f)
THISDOMAIN=\${THISHOST#*.}

#--------------------------------------------------------------------
# Get the external IP address of the server
wget -q -O - -U DTP-appliance http://consult.btoy1.net/whatsmyip.php | awk '/IP/{print \$NF}' > /tmp/\$\$
diff /tmp/\$\$ /tmp/ExternalIP &> /dev/null
if [ \$? -ne 0 ]
then
    if [ -s /tmp/ExternalIP ]
    then
        OLD_IP=\$(< /tmp/ExternalIP)
    else
        OLD_IP='unknown'
    fi
    cat /tmp/\$\$ > /tmp/ExternalIP
    NEW_IP=\$(< /tmp/ExternalIP)
    logger -i -p err -t daemon -- \$PROG External IP changed from \$OLD_IP to \$NEW_IP

    # Determine the email recipient
    M_RECIPIENT=\$(awk '/SysMon/{sub(/^.*-a /,"");sub(/ -o.*/,"");print}' /etc/rc.local)
    [ -z "\$M_RECIPIENT" ] && M_RECIPIENT='consult@btoy1.net'
    # Determine the MX host(s) for the email recipient
    host -t mx \${M_RECIPIENT##*@} &> /tmp/\$\$
    if [ \$? -eq 0 ]
    then
        # We have some results
        MX=\$(awk '{sub(/\.\$/,"");print \$NF;exit}' /tmp/\$\$)

        # Send an email notification
        SENT_EMAIL=1
        TRY=0
        while [ \$SENT_EMAIL -ne 0 -a \$TRY -lt 3 ]
        do
            echo "External IP for \$THISHOST changed from \$OLD_IP to \$NEW_IP" | \
              sendemail -f root@\$THISDOMAIN -t \$M_RECIPIENT -q \
                -o message-header='X-Priority: 1 (Highest)' \
                -o message-header='X-MSMail-Priority: High' \
                -o message-header='Importance: High' \
                -u "\$THISHOST: New external IP address" -s \${MX}
            if [ \$? -eq 0 ]
            then
                # Email was sent successfully
                SENT_EMAIL=0
                break
            fi
            # Try again unless the email was successfully sent
            [ \$SENT_EMAIL -ne 0 ] && TRY=\$((TRY + 1))
        done
    else
        logger -i -p err -t daemon -- \$PROG Unknown MX host for \$M_RECIPIENT
    fi
fi

#--------------------------------------------------------------------
# Create a simple CSV file with some stats (every 5 minutes)
if [ \$((\$(date +%-M) % 5)) -eq 2 ]
then
    wget -q 'http://localhost/admin/api.php?summary' -O - | \\
      python3 -m json.tool | \\
      awk '/(ads_percentage_today|domains_being_blocked|clients_ever_seen)/{
             gsub(/[",:]/,"");sub(/^ */,"");gsub(/ /,",");print strftime("%T")","\$0
          }' >> /var/tmp/Pihole-Stats.\$(date +%F)
    # Delete any file older than a month
    find /var/tmp/ -name "Pihole-Stats.2*" -mtime +31 -delete
fi

#--------------------------------------------------------------------
# Set the local DNS resolver to use the local server
cat << EOCF > /etc/resolv.conf
# Dynamic resolv.conf(5) file for glibc resolver(3) generated by resolvconf(8)
#     DO NOT EDIT THIS FILE BY HAND -- YOUR CHANGES WILL BE OVERWRITTEN
# 127.0.0.53 is the systemd-resolved stub resolver.
# run "systemd-resolve --status" to see details about the actual nameservers.

nameserver 127.0.0.1
#nameserver 127.0.0.53
search \$THISDOMAIN
EOCF

#--------------------------------------------------------------------
# We are done
exit 0
EOT
    chmod 744 /usr/local/sbin/GetExternalIP.sh
    [ -z "$(grep GetExternal /etc/cron.d/bluc)" ] && cat << EOT >> /etc/cron.d/bluc
# Get external IP address of the server
* * * * *       root    [ -x /usr/local/sbin/GetExternalIP.sh ] && /usr/local/sbin/GetExternalIP.sh
EOT

    # Install pi-hole from scratch
    # See https://www.ubuntuboss.com/how-to-install-pihole-on-ubuntu-18-04/

    # Get the newest package and install it
    cd /usr/local/src
    rm -rf Pi_hole
    git clone --depth 1 https://github.com/pi-hole/pi-hole.git Pi-hole
    cd Pi-hole/automated\ install/
    bash basic-install.sh

    # Disable dnsmasq listener
    [ -z "$(grep '^DNSStubListener=no' /etc/systemd/resolved.conf)" ] && sed -i 's/#DNSStubListener.*/DNSStubListener=no/' /etc/systemd/resolved.conf

    # Set a new web password
    pihole -a -p

    #- - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Customizations

    # Get the installed (major) version
    PV=`pihole -v | awk -F. '/Pi-hole/{gsub(/^.*v/,"");print $1;exit}'`

    cat << EOS > /usr/local/sbin/pihole.blocklists.sh
#!/bin/sh

trap "rm -f /tmp/pb" EXIT

# Standard lists
cat << EOT > /etc/pihole/adlists.list
#https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts
https://mirror1.malwaredomains.com/files/justdomains
http://sysctl.org/cameleon/hosts
https://s3.amazonaws.com/lists.disconnect.me/simple_tracking.txt
https://s3.amazonaws.com/lists.disconnect.me/simple_ad.txt
https://hosts-file.net/ad_servers.txt
EOT

# URLHaus list
cat << EOT >> /etc/pihole/adlists.list
https://urlhaus.abuse.ch/downloads/rpz/
EOT

# Lists from hosts-file.net
cat << EOT >> /etc/pihole/adlists.list
https://hosts-file.net/emd.txt
https://hosts-file.net/exp.txt
https://hosts-file.net/fsa.txt
https://hosts-file.net/hjk.txt
https://hosts-file.net/pha.txt
https://hosts-file.net/psh.txt
EOT

# Replacement list from Steven Black
cat << EOT >> /etc/pihole/adlists.list
https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews/hosts
EOT

# Block YouTube ads
cat << EOT >> /etc/pihole/adlists.list
https://raw.githubusercontent.com/kboghdady/youTube_ads_4_pi-hole/master/youtubelist.txt
EOT

# Lists from blocklist.site
cat << EOT >> /etc/pihole/adlists.list
https://blocklist.site/app/dl/crypto
https://blocklist.site/app/dl/fraud
https://blocklist.site/app/dl/malware
https://blocklist.site/app/dl/phishing
https://blocklist.site/app/dl/ransomware
https://blocklist.site/app/dl/scam
https://blocklist.site/app/dl/tracking
EOT

# All ticked lists from "firebog.net"
wget -q 'https://v.firebog.net/hosts/lists.php?type=tick' -O /tmp/ticked

# Update all the list of block lists
sort -u /etc/pihole/adlists.list /tmp/ticked > /tmp/pb
cat /tmp/pb > /etc/pihole/adlists.list

# Update the list of blocked domains (unless the
#  parameter "no-rebuild' was specified)
[ "T$1" = 'Tno-rebuild' ] || pihole -g

# We are done
exit 0
EOS
    chmod 744 /usr/local/sbin/pihole.blocklists.sh

    # Deal with Roku based blocking
    if [ `grep -wc 'roku' /etc/pihole/regex.list` -eq 0 ]
    then
        YN=''
        read -p 'Stop Roku ads and logging [Y/n] ? ' YN
        [ -z "$YN" ] && YN='Y'
        if [ "T$YN" = 'TY' -o "T$YN" = 'Ty' ]
        then
            echo '(ads|captive|logs)\.roku\.com' > /etc/pihole/regex.list
        fi
    fi

    # Install custom lists
    if [ `grep -c 'fakenews' /etc/pihole/adlists.list` -eq 0 ]
    then
        YN=''
        read -p 'Install custom black lists [Y/n] ? ' YN
        [ -z "$YN" ] && YN='Y'
        if [ "T$YN" = 'TY' -o "T$YN" = 'Ty' ]
        then
            /usr/local/sbin/pihole.blocklists.sh no-rebuild
        fi
    fi

    # White list "gitlab.com" if necessary
    [ -z "`grep gitlab /etc/pihole/whitelist.txt`" ] && echo 'gitlab.com' >> /etc/pihole/whitelist.txt
    # White list "raw.githubusercontent.com" if necessary
    [ -z "`grep raw.githubusercontent.com /etc/pihole/whitelist.txt`" ] && echo 'raw.githubusercontent.com' >> /etc/pihole/whitelist.txt
    # White list "bitbucket.org" if necessary
    [ -z "`grep bitbucket /etc/pihole/whitelist.txt`" ] && echo 'bitbucket.org' >> /etc/pihole/whitelist.txt

    # Update the lists
    if [ $PV -gt 4 ]
    then
        pihole -w -q `cat /etc/pihole/whitelist.txt | xargs`
        pihole -b -q `cat /etc/pihole/blacklist.txt | xargs`
    fi
    pihole -g

    # Block trackers used by the "ring" app on android
    if [ `egrep -c '^(branch.io|mixpanel.com|appsflyer.com)' /etc/pihole/gravity.list` -lt 3 ]
    then
        cat << EOT >> /etc/pihole/blacklist.txt
branch.io
mixpanel.com
appsflyer.com
EOT
    fi

    # Install script to download white list
    wget -O /usr/local/sbin/pihole.whitelist.sh \
      https://raw.githubusercontent.com/anudeepND/whitelist/master/scripts/whitelist.sh
    chmod 744 /usr/local/sbin/pihole.whitelist.sh

    # Expand the crontab for pihole
    if [ -z "`grep 'reboot.*updateGravity' /etc/cron.d/pihole`" ]
    then
        cat << EOT >> /etc/cron.d/pihole

# Update the list of blocked domains at reboot
@reboot root    PATH="$PATH:/usr/local/bin/" pihole updateGravity >/var/log/pihole_updateGravity.log
EOT
    fi
    if [ -z "`grep 'whitelist.sh' /etc/cron.d/pihole`" ]
    then
        WHEN=`awk '/updateG/{print $1" "$2" "$3" "$4; exit}' /etc/cron.d/pihole`
        cat << EOT >> /etc/cron.d/pihole

# Update whitelist
$WHEN 6   root    [ -x /usr/local/sbin/pihole.whitelist.sh ] && /usr/local/sbin/pihole.whitelist.sh
@reboot root    [ -x /usr/local/sbin/pihole.whitelist.sh ] && /usr/local/sbin/pihole.whitelist.sh
EOT
    fi
    if [ -z "`grep 'blocklists.sh' /etc/cron.d/pihole`" ]
    then
        WHEN=`awk '/updateG/{print $1" "$2" "$3" "$4; exit}' /etc/cron.d/pihole`
        cat << EOT >> /etc/cron.d/pihole

# Update blocklists
$WHEN 5   root    [ -x /usr/local/sbin/pihole.blocklists.sh ] && /usr/local/sbin/pihole.blocklists.sh
@reboot root    [ -x /usr/local/sbin/pihole.blocklists.sh ] && /usr/local/sbin/pihole.blocklists.sh
EOT
    fi

    # Update gravity lists more often
    YN=''
    read -p 'Update list of blocked domains ("gravity") daily [Y/n] ? ' YN
    [ -z "$YN" ] && YN='Y'
    if [ "T$YN" = 'TY' -o "T$YN" = 'Ty' ]
    then
        sed -i 's/7\(.*updateGravity\)/* \1/' /etc/cron.d/pihole
    fi

    # Update "pihole" weekly
    cat << EOT > /etc/cron.weekly/pihole
#!/bin/bash
#--------------------------------------------------------------------
# (c) CopyRight 2020 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#--------------------------------------------------------------------

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

if [ \$(/usr/bin/id -u) -ne 0 ]
then
    echo 'You must be root to continue'
    exit 0
fi

#-------------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/\$PROG.lock
if [ -f \$LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=\$(< \$LOCKFILE)
    [ -z "\$(ps h -p \$MYPID)" ] || exit 0
fi
trap "rm -f \$LOCKFILE /tmp/\$\$*" EXIT
echo "\$\$" > \$LOCKFILE

#-------------------------------------------------------------------------
# Update "pihole" if necessary/possible
pihole -up
exit \$?
EOT
    chmod 755 /etc/cron.weekly/pihole

    # Manage local DHCP server
    YN=''
    read -p 'Is this a DHCP server [y/N] ? ' YN
    [ -z "$YN" ] && YN='N'
    if [ "T$YN" = 'TY' -o "T$YN" = 'Ty' ]
    then
        apt-get purge dhcpcd5
        /usr/local/sbin/SetNetwork.sh
        [ -z "$(grep SetNetwork /etc/cron.d/bluc)" ] && cat << EOT >> /etc/cron.d/bluc
# Set the local IP address
* * * * *       root    [ -x /usr/local/sbin/SetNetwork.sh ] && /usr/local/sbin/SetNetwork.sh
EOT
        [ -s /etc/pihole/setupVars.conf ] && source /etc/pihole/setupVars.conf
        cat << EOT > /etc/netplan/01-netcfg.yaml
# This file describes the network interfaces available on your system
# For more information, see netplan(5).
network:
  version: 2
  renderer: networkd
## =====
## NOTE: Verify and adapt the IP addresses and names below
  ethernets:
    ${LOCALIF}:
      addresses: [ ${LOCALIP}/${CIDRMASK} ]
      gateway4: ${DEFAULT_GW}
      nameservers:
          search: [ ${PIHOLE_DOMAIN} ]
          addresses:
              - "${LOCALIP}"
## =====
EOT
        vi /etc/netplan/01-netcfg.yaml
        netplan apply

        echo 'Disable any existing DHCP server, e.g. on the firewall'
    else
        sed -iE 's/(.*dhcp.*)/#\1/' /etc/firehol/firehol.conf
        echo "Ensure that the existing DHCP server can set the DNS server to ${LOCALIP}"
    fi
    read -t 120 -p 'Press ENTER to continue or wait 2 minutes' YN

    if [ -d /etc/chrony ]
    then
        egrep -v '^(pool|allow) ' /etc/chrony/chrony.conf > $TFILE
        cat << EOT >> $TFILE
pool us.pool.ntp.org        iburst maxsources 4
pool 0.us.pool.ntp.org iburst maxsources 1
pool 1.us.pool.ntp.org iburst maxsources 1
pool 2.us.pool.ntp.org iburst maxsources 2
allow ${LOCALNET}/${CIDRMASK}
EOT
        diff $TFILE /etc/chrony/chrony.conf &> /dev/null
        if [ $? -ne 0 ]
        then
             cat $TFILE > /etc/chrony/chrony.conf
             service chrony restart
        fi
    fi

    # Reboot
    shutdown -r 1 Activate pi-hole
fi

#--------------------------------------------------------------------
# We are done
exit 0
