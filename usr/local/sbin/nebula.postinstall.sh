#!/bin/bash
################################################################
# (c) Copyright 2021 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/nebula.postinstall.sh

#--------------------------------------------------------------------
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            

#--------------------------------------------------------------------
# Specifying "-x" to the bash invocation = DEBUG
DEBUG=''
[[ $- ==  *x* ]] && DEBUG='-x'

#--------------------------------------------------------------------
# This host and domain
THISHOST=$(hostname)
[[ $THISHOST == *.* ]] || THISHOST=$(hostname -f)
THISDOMAIN=${THISHOST#*.}

#--------------------------------------------------------------------
# Global variables
SNAP_COMMON=/var/snap/nebula/common
MESH_NET='192.168.69'

#--------------------------------------------------------------------
# Install the actual software
[ -x /usr/bin/snap ] || apt-get install snapd
snap install nebula; snap refresh nebula

#--------------------------------------------------------------------
# Set up the CA (if necessary)
YN=''
read -p "Is this the CA (Certificate Authority) server [y/n] ? " YN
if [ "T${YN^^}" = 'TY' ]
then
    if [ ! -s $SNAP_COMMON/certs/ca.crt -o ! -s $SNAP_COMMON/certs/ca.key ]
    then
        CA_NAME='test'
        while [ "T$CA_NAME" = 'Ttest' ]
        do
            read -p "Name for the Nebula VPN ? " CA_NAME
            [ -z "$CA_NAME" ] || break
        done
        nebula.cert-ca -name "$CA_NAME" -out-crt $SNAP_COMMON/certs/ca.crt \
          -out-key $SNAP_COMMON/certs/ca.key
    fi

    # Setup node certificates
    # Node '1' is always the lighthouse server
    if [ ! -s $SNAP_COMMON/certs/bm-lighthouse.crt -o ! -s $SNAP_COMMON/certs/bm-lighthouse.key ]
    then
        # The 'lighthouse' server cert
        nebula.cert-sign -name "bm-lighthouse" -ip "${MESH_NET}.1/24" \
          -out-crt $SNAP_COMMON/certs/bm-lighthouse.crt \
          -out-key $SNAP_COMMON/certs/bm-lighthouse.key
    fi
    for E in 'crt' 'key'
    do
        ln -fs $SNAP_COMMON/certs/bm-lighthouse.$E $SNAP_COMMON/certs/${THISHOST}.$E
    done

    NODE_NO=2
    while [ 1 ]
    do
        NODE=$(printf "%04d" $NODE_NO)
        if [ ! -s $SNAP_COMMON/certs/bm-node${NODE}.crt -o ! -s $SNAP_COMMON/certs/bm-node${NODE}.key ]
        then
            # Ask whether we need more nodes
            YN=''
            read -p "Create certificate for node $NODE_NO [y/n] ? " YN
            [ "T${YN^^}" = 'TY' ] || break

            nebula.cert-sign -name "bm-node" -ip "${MESH_NET}.${NODE_NO}/24" \
              -out-crt $SNAP_COMMON/certs/bm-node${NODE}.crt \
              -out-key $SNAP_COMMON/certs/bm-node${NODE}.key

            NODE_NAME=''
            read -p "DNS name for $NODE_NO [y/n] ? " NODE_NAME
            for E in 'crt' 'key'
            do
                ln -fs $SNAP_COMMON/certs/bm-node${NODE}.$E $SNAP_COMMON/certs/${NODE_NAME}.$E
            done
        else
            echo "Certificate and key files already exist for node ${NODE_NO}"
        fi
        cat << EOT

                              !!! IMPORTANT !!!

Copy these files to the node ${NODE_NO}:
 $SNAP_COMMON/certs/ca.crt
 $SNAP_COMMON/certs/bm-node${NODE}.crt as $SNAP_COMMON/certs/bm-node.crt
 $SNAP_COMMON/certs/bm-node${NODE}.key as $SNAP_COMMON/certs/bm-node.key
EOT
        read -t 10 -p "Press 'ENTER' to continue" PE
        NODE_NO=$(($NODE_NO + 1))
    done
fi

#--------------------------------------------------------------------
# Write out the configuration file
YN=''
read -p "Is this the 'lighthouse' server [y/n] ? " YN
if [ "T${YN^^}" = 'TY' ]
then
    LOCALIF=$(ip route get 8.8.8.8 | awk '/via/{print $5; exit}')
    LOCALIP=$(ifconfig $LOCALIF | sed -n "s/.*inet addr:\([0-9.]*\).*/\1/p")
    [ -z "$LOCALIP" ] && LOCALIP=$(ifconfig $LOCALIF | sed -n "s/.*inet \([0-9.]*\).*/\1/p")
    [ -z "$LOCALIP" ] && LOCALIP=$(ip addr list $LOCALIF | sed -n "s/.*inet \([0-9.]*\).*/\1/p")

    cat << EOT > $SNAP_COMMON/config/config.yaml
# This is based on https://raw.githubusercontent.com/jimsalterjrs/nebula-sample-configs/master/config.lighthouse.yaml
# It is suitable for a 'lighthouse' server
#

pki:
  # every node needs a copy of the CA certificate,
  # and its own certificate and key, ONLY.
  #
  ca: $SNAP_COMMON/certs/ca.crt
  cert: $SNAP_COMMON/certs/bm-lighthouse.crt
  key: $SNAP_COMMON/certs/bm-lighthouse.key

static_host_map:
 # how to find one or more lighthouse nodes
 # you do NOT need every node to be listed here!
 #
 # format "Nebula IP": ["public IP or hostname:port"]
 # 
 "${MESH_NET}.1": ["$LOCALIP:4242"]

lighthouse:
  interval: 60

  # if you're a lighthouse, say you're a lighthouse
  #
  am_lighthouse: true

  hosts:
    # If you're a lighthouse, this section should be EMPTY
    # or commented out. If you're NOT a lighthouse, list 
    # lighthouse nodes here, one per line, in the following
    # format:
    #
    # - "192.168.98.1"

listen:
  # 0.0.0.0 means "all interfaces," which is probably what you want
  #
  host: 0.0.0.0
  port: 4242

# "punchy" basically means "send frequent keepalive packets"
# so that your router won't expire and close your NAT tunnels.
#
punchy: true

# "punch_back" allows the other node to try punching out to you,
# if you're having trouble punching out to it. Useful for stubborn
# networks with symmetric NAT, etc.
#
punch_back: true

tun:
  # sensible defaults. don't monkey with these unless
  # you're CERTAIN you know what you're doing.
  #
  dev: nebula1
  drop_local_broadcast: false
  drop_multicast: false
  tx_queue: 500
  mtu: 1300
  routes:

logging:
  level: info
  format: text

# you NEED this firewall section.
#
# Nebula has its own firewall in addition to anything
# your system has in place, and it's all default deny.
#
# So if you don't specify some rules here, you'll drop
# all traffic, and curse and wonder why you can't ping
# one node from another.
# 
firewall:
  conntrack:
    tcp_timeout: 120h
    udp_timeout: 3m
    default_timeout: 10m
    max_connections: 100000

# since everything is default deny, all rules you
# actually SPECIFY here are allow rules.
#
  outbound:
    - port: any
      proto: any
      host: any

  inbound:
    - port: any
      proto: any
      host: any
EOT
    echo "The configuration for the 'lighthouse' server is at $SNAP_COMMON/config/config.yaml"
    systemctl restart snap.nebula.daemon.service
    cat << EOT

                              !!! IMPORTANT !!!

Adjust the local (and global) firewall to allow inbound UDP traffic on port 4242!
EOT
else
    LH_SERVER=''
    read -p "IP address of the 'lighthouse' server ? " LH_SERVER
    NODE_NO=2
    read -p "Node number ? " NODE_NO
    cat << EOT > $SNAP_COMMON/config/config.yaml
# This is based on https://raw.githubusercontent.com/jimsalterjrs/nebula-sample-configs/master/config.node.yaml
# It is suitable for a 'node' server
#

pki:
  # every node needs a copy of the CA certificate,
  # and its own certificate and key, ONLY.
  # COPY THE CORRECT certificate from the 'lighthouse' server!
  ca: $SNAP_COMMON/certs/ca.crt
  cert: $SNAP_COMMON/certs/bm-node.crt
  key: $SNAP_COMMON/certs/bm-node.key

static_host_map:
 # how to find one or more lighthouse nodes
 # you do NOT need every node to be listed here!
 #
 # format "Nebula IP": ["public IP or hostname:port"]
 # 
 "${MESH_NET}.1": ["${LH_SERVER}:4242"]

lighthouse:
  interval: 60

  # if you're a lighthouse, say you're a lighthouse
  #
  am_lighthouse: false

  hosts:
    # If you're a lighthouse, this section should be EMPTY
    # or commented out. If you're NOT a lighthouse, list 
    # lighthouse nodes here, one per line, in the following
    # format:
    #
    - "${MESH_NET}.1"

listen:
  # 0.0.0.0 means "all interfaces," which is probably what you want
  #
  host: 0.0.0.0
  port: 4242

# "punchy" basically means "send frequent keepalive packets"
# so that your router won't expire and close your NAT tunnels.
#
punchy: true

# "punch_back" allows the other node to try punching out to you,
# if you're having trouble punching out to it. Useful for stubborn
# networks with symmetric NAT, etc.
#
punch_back: true

tun:
  # sensible defaults. don't monkey with these unless
  # you're CERTAIN you know what you're doing.
  #
  dev: nebula1
  drop_local_broadcast: false
  drop_multicast: false
  tx_queue: 500
  mtu: 1300
  routes:

logging:
  level: info
  format: text

# you NEED this firewall section.
#
# Nebula has its own firewall in addition to anything
# your system has in place, and it's all default deny.
#
# So if you don't specify some rules here, you'll drop
# all traffic, and curse and wonder why you can't ping
# one node from another.
# 
firewall:
  conntrack:
    tcp_timeout: 120h
    udp_timeout: 3m
    default_timeout: 10m
    max_connections: 100000

# since everything is default deny, all rules you
# actually SPECIFY here are allow rules.
#
  outbound:
    - port: any
      proto: any
      host: any

  inbound:
    - port: any
      proto: any
      host: any
EOT
    echo "The configuration for the node is at $SNAP_COMMON/config/config.yaml"

    if [ -s $SNAP_COMMON/certs/ca.crt -a -s $SNAP_COMMON/certs/bm-node.crt -a $SNAP_COMMON/certs/bm-node.key ]
    then
        systemctl restart snap.nebula.daemon.service
    else
        NODE=$(printf "%04d" $NODE_NO)
        cat << EOT

                              !!! IMPORTANT !!!

Copy the correct certificates from the CA server:

 $SNAP_COMMON/certs/ca.crt
 $SNAP_COMMON/certs/bm-node${NODE}.crt as $SNAP_COMMON/certs/bm-node.crt
 $SNAP_COMMON/certs/bm-node${NODE}.key as $SNAP_COMMON/certs/bm-node.key

Adjust the local host firewall to allow all traffic on the interface
 "nebula1", e.g. for firehol:

interface4 nebula1 mesh
        masquerade
        #-----------------------------------------------------------------
        # Protect against attacks as best as we can
        policy reject
        protection strong 10/sec 10
        protection reverse strong
        #-----------------------------------------------------------------
        # Allow everything
        client all accept
        server all accept

Finally issue these commands:
 systemctl restart snap.nebula.daemon.service
 systemctl status snap.nebula.daemon.service

EOT
    fi
fi

#--------------------------------------------------------------------
# We are done
exit 0
