#!/usr/bin/perl -Tw
#--------------------------------------------------------------------
# (c) CopyRight 2022 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#--------------------------------------------------------------------
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/ModSecLog2Syslog.pl
#
# Recommended invocation via cron:
# * * * * * root [ -z "`pidof ModSecLog2Syslog.pl`" ] && /usr/local/sbin/ModSecLog2Syslog.pl

use 5.0010;

# This script MUST run as "root"
BEGIN { unshift @INC, "./blib/lib/"; die "Must run as root\n" if $> != 0; }

# Constants
my $ProgName = '';
( $ProgName = $0 ) =~ s%.*/%%;

# Common sense options:
use strict qw(vars subs);
no warnings;
use warnings qw(FATAL closed threads internal debugging pack
    portable prototype inplace io pipe unpack malloc
    glob digit printf layer reserved taint closure
    semicolon);
no warnings qw(exec newline unopened);

#--------------------------------------------------------------------
# Needed packages
#--------------------------------------------------------------------
use Sys::Syslog qw(:macros :standard);
use POSIX qw(setsid);
use Getopt::Std;
use File::Tail;

# The files to "tail"
my @LogFiles = qw( /var/log/apache2/error.log );
my @files;

# Program options
our $opt_d = 0;
our $opt_h = 0;

#--------------------------------------------------------------------
# Get possible options
getopts('dh') or ShowUsage();
if ($opt_h)
{
    print "Usage: $ProgName [-d]\n",
        "       -d Debug mode\n\n",
        "The script runs as a 'daemon' and forwards (most) log entries from
        these files:\n";
    foreach (@LogFiles)
    {
        print " $_\n";
    } ## end foreach (@LogFiles)

    exit(0);
} ## end if ($opt_h)

#--------------------------------------------------------------------
# Become a daemon process
#--------------------------------------------------------------------
sub Daemonize()
{

    # pretty command line in ps
    $0 = join( ' ', $0, @ARGV ) unless ($opt_d);

    chdir '/' or die "Can't chdir to '/': $!";

    # Redirect STDIN and STDOUT
    open( STDIN,  '<', '/dev/null' ) or die "Can't read '/dev/null': $!";
    open( STDOUT, '>', '/dev/null' ) or die "Can't write '/dev/null': $!";
    defined( my $pid = fork ) or die "Can't fork: $!";

    if ($pid)
    {

        # The parent can die now
        print "DEBUG: Parent dies" if ($opt_d);
        exit;
    } ## end if ($pid)

    setsid or die "Can't start new session: $!";
    open STDERR, '>&STDOUT' or die "Can't duplicate stdout: $!";

    # Show just the program name in the "ps" listing
    $0 = $ProgName;
} ## end sub Daemonize

#--------------------------------------------------------------------
# Open the "tail" files
foreach (@LogFiles)
{
    push( @files, File::Tail->new( name => "$_", debug => $opt_d ) );
} ## end foreach (@LogFiles)

#--------------------------------------------------------------------
# Open the connection to syslog
openlog( "$ProgName", 'ndelay,pid', LOG_MAIL );
syslog( LOG_INFO, "%s", "Starting $ProgName" );

#--------------------------------------------------------------------
# Become a daemon process
Daemonize;

#--------------------------------------------------------------------
# Eternal loop
while (1)
{
    my $nfound = File::Tail::select( undef, undef, undef, 60, @files );
    unless ($nfound)
    {
        my @ints;
        foreach (@files)
        {
            push( @ints, $_->interval );
        } ## end foreach (@files)
        print "Nothing new! - "
            . localtime(time) . "("
            . join( ",", @ints ) . ")\n";
    } ## end unless ($nfound)

    foreach (@files)
    {
        my $LogLine = $_->read unless $_->predict;
        next unless ( length($LogLine) );
        print "DEBUG: $LogLine" if ($opt_d);

        # Log only meaningful messages
        if ( $LogLine
            =~ /client ([\d\.]+)\] ModSecurity: Access denied with code \d+ .*msg ([^\]]+).*hostname ([^\]]+).*uri ([^\]]+)/o
            )
        {
            my $ClientIP = "$1";
            my $Reason   = "$2";
            $Reason =~ s/"//g;
            my $HostName = "$3";
            $HostName =~ s/"//g;
            my $URI = "$4";
            $URI =~ s/"//g;

            if ( $_->{"input"} =~ /error/o )
            {

                my $LogMsg
                    = "Client IP $ClientIP was denied access to $HostName/$URI due to '$Reason'";
                print "DEBUG: $LogMsg" if ($opt_d);
                syslog( LOG_ERR, "%s", "$LogMsg" );
                next;
            } ## end if ( $_->{"input"} =~ ...)
        } ## end if ( $LogLine =~ ...)
    } ## end foreach (@files)
} ## end while (1)

# We are done
syslog( LOG_INFO, "%s", "Stopping $ProgName" );
closelog();
exit(0);
__END__
