#!/usr/bin/perl
# Version 20240216-140242 checked into repository
#--------------------------------------------------------------------
# (c) CopyRight 2022 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#--------------------------------------------------------------------
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/NetworkStats.pl
use 5.0010;

# Constants
my $ProgName = '';
( $ProgName = $0 ) =~ s%.*/%%;
my $ProgVers = 'unknown';
if ( open( my $GPV, "$0" ) )
{
    while (<$GPV>)
    {
        if (/# Version ([\d\-]*) checked/o)
        {
            $ProgVers = "$1";
            chomp($ProgVers);
            last;
        } ## end if (/# Version ([\d\-]*) checked/o...)
    } ## end while (<$GPV>)
    close($GPV);
} ## end if ( open( my $GPV, "$0"...))
my $CopyRight = '(c) CopyRight 2014-2022 B-LUC Consulting Thomas Bullinger';

# Common sense options:
use strict qw(vars subs);
no warnings;
use warnings qw(FATAL closed threads internal debugging pack
    portable prototype inplace io pipe unpack malloc
    glob digit printf layer reserved taint closure
    semicolon);
no warnings qw(exec newline unopened);

# Time units
my $Hourly = 60 * 60;

#--------------------------------------------------------------------
# Commify a number
# See: http://www.perlmonks.org/index.pl?node_id=435729
#--------------------------------------------------------------------
sub commify
{
    my ( $sign, $int, $frac ) = ( $_[0] =~ /^([+-]?)(\d*)(.*)/ );
    my $commified
        = ( reverse scalar join ',', unpack '(A3)*', scalar reverse $int );
    return $sign . $commified;
} ## end sub commify

#--------------------------------------------------------------------
# Get the current uptime (in seconds)
#--------------------------------------------------------------------
my $CurUptime = 0;
open( my $UT, '<', '/proc/uptime' )
    or die "ERROR: Can't get current uptime: $!";
while (<$UT>)
{
    if (/^([\d\.]*)/o)
    {
        $CurUptime = $1;
        last;
    } ## end if (/^([\d\.]*)/o)
} ## end while (<$UT>)
close($UT);
die "Device is up less than an hour\n" if ( $CurUptime < $Hourly );

#--------------------------------------------------------------------
# Get the current inbound and outbound bytes
#--------------------------------------------------------------------
my $InBytes  = 0;
my $OutBytes = 0;
my @lines = ();
open( my $NT, '<', '/proc/net/netstat' )
    or die "ERROR: Can't get current network stats: $!";
chomp ( @lines = grep {/^IpExt:/} <$NT>);
close ($NT);
foreach (@lines)
{
    if (/^IpExt:\s+\d+\s+\d+\s+\d+\s+\d+\s+\d+\s+\d+\s+(\d+)\s+(\d+)/o)
    {
        $InBytes  = $1;
        $OutBytes = $2;
        last;
    } ## end if (...)
} ## end while (<$NT>)

#--------------------------------------------------------------------
# Calculate some throughput
#--------------------------------------------------------------------
my $HourlyIngress = int( ( $InBytes / $CurUptime ) * $Hourly );
my $HourlyEgress  = int( ( $OutBytes / $CurUptime ) * $Hourly );
printf "Hourly ingress: %20s Bytes, %20s KBytes, %20s MBytes\n",
    commify($HourlyIngress),
    commify( $HourlyIngress / 1024 ),
    commify( $HourlyIngress / 1024 / 1024 );
printf "Hourly egress : %20s Bytes, %20s KBytes, %20s MBytes\n",
    commify($HourlyEgress),
    commify( $HourlyEgress / 1024 ),
    commify( $HourlyEgress / 1024 / 1024 );

# More stats :)
if ( $CurUptime > ( $Hourly * 24 ) )
{
    my $HourlyIngress_Daily = $HourlyIngress * 24;
    my $HourlyEgress_Daily  = $HourlyEgress * 24;
    print "\n";
    printf "Daily ingress : %20s Bytes, %20s KBytes, %20s MBytes\n",
        commify($HourlyIngress_Daily),
        commify( int( $HourlyIngress_Daily / 1024 ) ),
        commify( int( $HourlyIngress_Daily / 1024 / 1024 ) );
    printf "Daily egress  : %20s Bytes, %20s KBytes, %20s MBytes\n",
        commify($HourlyEgress_Daily),
        commify( int( $HourlyEgress_Daily / 1024 ) ),
        commify( int( $HourlyEgress_Daily / 1024 / 1024 ) );

    # Weekly stats
    if ( $CurUptime > ( $Hourly * 24 * 7 ) )
    {
        my $HourlyIngress_Weekly = $HourlyIngress_Daily * 7;
        my $HourlyEgress_Weekly  = $HourlyEgress_Daily * 7;
        print "\n";
        printf "Weekly ingress: %20s Bytes, %20s KBytes, %20s MBytes\n",
            commify( int($HourlyIngress_Weekly) ),
            commify( int( $HourlyIngress_Weekly / 1024 ) ),
            commify( int( $HourlyIngress_Weekly / 1024 / 1024 ) );
        printf "Weekly egress : %20s Bytes, %20s KBytes, %20s MBytes\n",
            commify( int($HourlyEgress_Weekly) ),
            commify( int( $HourlyEgress_Weekly / 1024 ) ),
            commify( int( $HourlyEgress_Weekly / 1024 / 1024 ) );

        if ( $CurUptime > ( $Hourly * 24 * 7 * 4 ) )
        {
            print "\n";
            printf "4-week ingress: %20s Bytes, %20s KBytes, %20s MBytes\n",
                commify( int( $HourlyIngress_Weekly * 4 ) ),
                commify( int( ( $HourlyIngress_Weekly * 4 ) / 1024 ) ),
                commify( int( ( $HourlyIngress_Weekly * 4 ) / 1024 / 1024 ) );
            printf "4-week egress : %20s Bytes, %20s KBytes, %20s MBytes\n",
                commify( int( $HourlyEgress_Weekly * 4 ) ),
                commify( int( $HourlyEgress_Weekly * 4 / 1024 ) ),
                commify( int( $HourlyEgress_Weekly * 4 / 1024 / 1024 ) );
        } ## end if ( $CurUptime > ( $Hourly...))
    } ## end if ( $CurUptime > ( $Hourly...))
} ## end if ( $CurUptime > ( $Hourly...))

#--------------------------------------------------------------------
# We are done
exit(0);
__END__
