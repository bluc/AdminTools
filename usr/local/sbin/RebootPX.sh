#!/bin/bash
# Version 20240114-090839 checked into repository
################################################################
# (c) Copyright 2024 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/RebootPX.sh

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=${0##*/}

# This needs to run as "root"
if [ ! "T$(/usr/bin/id -u)" = 'T0' ]
then
    echo "$PROG needs to run as 'root'"
    exit 1
fi

# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE

#--------------------------------------------------------------------
function ShowHelp()
{
    cat << EOT
Usage: $0 [options]
         -b dir  Specify the backup directory for list of local VM guests
                  This needs to be accessible from all Proxmox nodes
         -f      Force the reboot
         -r      Reverse migrations
EOT
    exit 0
}

#--------------------------------------------------------------------
# Get possible program options
FORCE=0
REVERSE=0
KVMBACKUP=''
while getopts hb:rf OPTION
do
    case ${OPTION} in
        r)  REVERSE=1
            ;;
        f)  FORCE=1
            ;;
        b)  KVMBACKUP="$OPTARG"
            ;;
        *)  ShowHelp
            ;;
    esac
done
shift $((OPTIND - 1))

if [ -z "$KVMBACKUP" ] || [ ! -d "$KVMBACKUP" ]
then
    ShowHelp
fi

# This host's short name
THISHOST=$(hostname -s)

#--------------------------------------------------------------------
# No reboot required :)
if [ $FORCE -eq 0 ]
then
    [ -f /var/run/reboot-required ] || exit 0
fi

#--------------------------------------------------------------------
# Wait until everything is lining up
while [ 1 ]
do
    # Check for any running backups
    if [ -z "$(ps -wefH | grep vz[d])" ]
    then
        logger -p info -t ${PROG}[$$] -- Disable backups
        chmod 644 /usr/local/sbin/VMDump.sh

        logger -p info -t ${PROG}[$$] -- Determine the target Proxmox node
        TARGET=$(pvecm nodes | awk '/^[[:space:]]*[[:digit:]]/ {print $3}' | grep -v $THISHOST | shuf -n1)
        ping -c 3 $TARGET &> /dev/null
        if [ $? -eq 0 ]
        then
           logger -p info -t ${PROG}[$$] -- Save the list of locally define VM guests
           #  (needed for the reverse migration later)
           find /etc/pve/local/qemu-server -type f -name "*.conf" | \
             grep -o -E '[0-9]+' | sort > $KVMBACKUP/$THISHOST.localguests

           for VMID in $(qm list | awk '/running/{print $1}')
           do
               logger -p info -t ${PROG}[$$] -- Migrate VM $VMID to $TARGET
               qm migrate $VMID $TARGET -online &
               sleep 2
           done
           wait

           if [ $(qm list | grep -c running) -eq 0 ]
           then
               logger -p info -t ${PROG}[$$] -- Reboot this server in 5 minutes
               shutdown -r +5 Reboot via $PROG
               exit 0
           else
               logger -p info -t ${PROG}[$$] -- No reboot possible since there are still running VM guests
               date "+%F %T: No reboot possible since there are still running VM guests"
           fi
        fi
    fi
    sleep 5
done

# We are done
exit 0
