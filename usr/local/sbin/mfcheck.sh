#!/bin/bash
# Version 20240411-085122 checked into repository
################################################################
# (c) Copyright 2024 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/mfcheck.sh

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=${0##*/}

# This needs to run as "root"
if [ ! "T$(/usr/bin/id -u)" = 'T0' ]
then
    echo "$PROG needs to run as 'root'"
    exit 1
fi

# Remove temp files at exit
trap "rm -f /tmp/$$*" EXIT

# Keep track of any problems
RESTARTS=0

#====================================================================
# Get the list of TCP listeners and processes
ss -ntl > /tmp/$$.tcp-listeners
ps -wefH > /tmp/$$.processes

#====================================================================
# Check postfix instances
if [ -z "$(grep 'postfix.*maste[r]' /tmp/$$.processes)" ]
then
    # No postfix running
    RESTARTS=$((RESTARTS + 1))
    systemctl restart postfix
else
    for PORT in 25 10025
    do
        rm -f /tmp/$$.postfix.results
        echo 'quit' | nc 127.0.0.1 $PORT &> /tmp/$$.postfix.results
        if [ ! -s /tmp/$$.postfix.results ]
        then
            # Postfix not answering
            RESTARTS=$((RESTARTS + 1))
            systemctl restart postfix
            break
        fi
        if [ -z "$(grep '^221 ' /tmp/$$.postfix.results)" ]
        then
            # Postfix not answering correctly
            RESTARTS=$((RESTARTS + 1))
            systemctl restart postfix
            break
        fi
    done
fi

#====================================================================
# Check ppolicyd instances
if [ $(grep -c 'ppolicy[d]' /tmp/$$.processes) -lt 2 ]
then
    # No ppolicyd running
    RESTARTS=$((RESTARTS + 1))
    systemctl restart ppolicyd
else
    echo '' | nc -w 5 127.0.0.1 2522 &> /tmp/$$.ppolicyd.results
    if [ ! -s /tmp/$$.ppolicyd.results ]
    then
        # Ppolicyd not answering
        RESTARTS=$((RESTARTS + 1))
        systemctl restart ppolicyd
    elif [ -z "$(grep 'action=action=dunno' /tmp/$$.ppolicyd.results)" ]
    then
        # Ppolicyd not answering correctly
        RESTARTS=$((RESTARTS + 1))
        systemctl restart ppolicyd
    fi
fi

#====================================================================
# Check postgrey instances
if [ $(grep -c 'postgre[y]' /tmp/$$.processes) -lt 1 ]
then
    # No postgrey running
    RESTARTS=$((RESTARTS + 1))
    systemctl restart postgrey
else
    nc -w 5 127.0.0.1 10023 &> /dev/null
    if [ $? -ne 0  ]
    then
        # Postgrey not answering
        RESTARTS=$((RESTARTS + 1))
        systemctl restart postgrey
    fi
fi

#====================================================================
# Check amavisd instances
if [ $(grep -c 'amavis[d]' /tmp/$$.processes) -lt 2 ]
then
    # No amavisd running
    RESTARTS=$((RESTARTS + 1))
    systemctl restart amavis
else
    nc -w 5 127.0.0.1 10024 &> /tmp/$$.amavis.results
    if [ ! -s /tmp/$$.amavis.results ]
    then
        # Amavisd not answering
        RESTARTS=$((RESTARTS + 1))
        systemctl restart amavis
    elif [ -z "$(grep '^220 ' /tmp/$$.amavis.results)" ]
    then
        # Amavisd not answering correctly
        RESTARTS=$((RESTARTS + 1))
        systemctl restart amavis
    fi
fi

#====================================================================
# Check clamd instances
if [ $(grep -c 'clam[d]' /tmp/$$.processes) -lt 1 ]
then
    # No clamd running
    RESTARTS=$((RESTARTS + 1))
    systemctl restart clamav-daemon
else
    clamdscan /etc/passwd &> /dev/null
    if [ $? -ne 0 ]
    then
        # Clamd not answering correctly
        RESTARTS=$((RESTARTS + 1))
        systemctl restart clamav-daemon
    fi
fi

#====================================================================
# Check eew instances
if [ $(grep -c 'ee[w]-defang' /tmp/$$.processes) -lt 1 ]
then
    # No eew-defang running
    RESTARTS=$((RESTARTS + 1))
    systemctl restart eew
else
    echo quit | nc -w 5 127.0.0.1 2525  &> /tmp/$$.eew.results
    if [ ! -s /tmp/$$.eew.results ]
    then
        # Eew not answering
        RESTARTS=$((RESTARTS + 1))
        systemctl restart eew
    elif [ -z "$(grep '^221 ' /tmp/$$.eew.results)" ]
    then
        # Eew not answering correctly
        RESTARTS=$((RESTARTS + 1))
        systemctl restart eew
    fi
fi

# We are done
[ $RESTARTS -eq 0 ] && date "+%F %T: $PROG No problems detected"
exit 0
