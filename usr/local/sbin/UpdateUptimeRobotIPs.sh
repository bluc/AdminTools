#!/bin/bash
# Version 20240422-083440 checked into repository
################################################################
# (c) Copyright 2024 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/UpdateUptimeRobotIPs.sh
# Should be run as a cronjob under "root"

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=${0##*/}

# This needs to run as "root"
if [ ! "T$(/usr/bin/id -u)" = 'T0' ]
then
    echo "$PROG needs to run as 'root'"
    exit 1
fi

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f "$LOCKFILE" ]
then
    # The file exists so read the PID
    MYPID=$(< "$LOCKFILE")
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > "$LOCKFILE"

#--------------------------------------------------------------------
# Get the list of IP addresses for UptimeRobot
GET_LIST=0
if [ -s /var/tmp/UR-IPs.sav ]
then
    # Get the list of IP addresses once an hour
    if [ -n "$EPOCHSECONDS" ]
    then
        ONE_HOUR_AGO=$((EPOCHSECONDS - 3600))
    else
        ONE_HOUR_AGO=$(date +%s -d '1 hour ago')
    fi
    if [ $(date +%s -r /var/tmp/UR-IPs.sav) -lt $ONE_HOUR_AGO ]
    then
        # Force a fetch (along with a timestamp update)
        GET_LIST=1
        rm -f /var/tmp/UR-IPs.sav
    fi
else
    # Get the list if it doesn't exist yet
    GET_LIST=1
fi
if [ $GET_LIST -ne 0 ]
then
    # Sensible options for "wget"
    WGET_OPTS='-q --no-check-certificate -t 3 -T 60'
    [[ "$(wget --help)" =~ compression= ]] && WGET_OPTS="$WGET_OPTS --compression=auto"

    wget $WGET_OPTS 'https://uptimerobot.com/inc/files/ips/IPv4.txt' -O /tmp/$$
    # Bail if we don't have it
    if [ ! -s /tmp/$$ ]
    then
        date "+%F %T: Could not retrieve IPs for UptimeRobot"
        exit 1
    fi

    # Update list if necessary
    perl -pi -e's/\015\012/\012/g' /tmp/$$
    diff -wu /var/tmp/UR-IPs.sav /tmp/$$ &> /dev/null
    [ $? -ne 0 ] && cat /tmp/$$ > /var/tmp/UR-IPs.sav
fi

#--------------------------------------------------------------------
# Preset blocklists if present
TRY=0
HAVE_IPTL=0
[ -x /usr/sbin/iptables-legacy-save ] && HAVE_IPTL=1
while true
do
    # Wait until firehol has fully initialized
    if [ $TRY -gt 30 ]
    then
        date "+%F %T: Host firewall not fully initialized"
        exit 1
    fi

    iptables-save 2> /dev/null > /tmp/$$
    [ $HAVE_IPTL -ne 0 ] && iptables-legacy-save 2> /dev/null >> /tmp/$$
    [ -n "$(grep -m 1 'FLOOD:' /tmp/$$)" ] && break

    if [ -x /usr/sbin/firehol ]
    then
        # Restart the firehol rules
        yes commit | firehol try
    fi
    sleep 10
    TRY=$((TRY + 1))
done

if [ -z "$(command -v ipset)" ]
then
    date "+%F %T: Could not find 'ipset'"
    exit 1
fi

HOSTS_HASH_SIZE=$((2048 * 1024))
# Create the active list and populate it
cat << EOT >> /tmp/$$.sh
ipset create UR_HOSTS_NEW hash:ip hashsize 16384 maxelem $HOSTS_HASH_SIZE -exist
ipset create UR_HOSTS hash:ip hashsize 16384 maxelem $HOSTS_HASH_SIZE -exist
EOT
[ -s "/var/tmp/UR-IPs.sav" ] && awk '{printf "ipset add UR_HOSTS_NEW %s -exist\n", $1}' /var/tmp/UR-IPs.sav >> /tmp/$$.sh
cat << EOT >> /tmp/$$.sh
ipset swap UR_HOSTS_NEW UR_HOSTS
ipset destroy UR_HOSTS_NEW
EOT
bash "-$-" /tmp/$$.sh

# Activate the checks if necessary
WAIT='-w'
[[ $(iptables --help) =~ \-\-wait[[:space:]] ]] || WAIT=''
for CHAIN in INPUT FORWARD OUTPUT
do
    ADD_RULE=0
    # Skip any chain that already has the checks active (they need to be first)
    RULES2DELETE=$(iptables -L $CHAIN --line-numbers 2> /dev/null | grep -c 'UR_HOSTS ALLOWED')
    if [ $RULES2DELETE -gt 1 ]
    then
       for (( i=1; i<$RULES2DELETE; i++ ))
       do
           iptables $WAIT -D $CHAIN -m set --match-set UR_HOSTS src,dst -j ACCEPT
           iptables $WAIT -D $CHAIN -m set --match-set UR_HOSTS src,dst -j LOG --log-prefix "UR_HOSTS ALLOWED:" -m limit --limit 3/min --limit-burst 10
       done
       ADD_RULE=1
    elif [ $RULES2DELETE -eq 0 ]
    then
       ADD_RULE=1
    fi
    if [ $ADD_RULE -ne 0 ]
    then
       iptables $WAIT -I $CHAIN -m set --match-set UR_HOSTS src,dst -j ACCEPT
       iptables $WAIT -I $CHAIN -m set --match-set UR_HOSTS src,dst -j LOG --log-prefix "UR_HOSTS ALLOWED:" -m limit --limit 3/min --limit-burst 10
    fi
done

#--------------------------------------------------------------------
# We are done
exit 0
