#!/usr/bin/perl -Tw
#--------------------------------------------------------------------
# (c) CopyRight 2018 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#--------------------------------------------------------------------
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/WatchLog.pl
#
# Recommended invocation in /etc/rc.local:
# [ -x /usr/local/sbin/WatchLogs.pl ] && /usr/local/sbin/WatchLogs.pl
use 5.0010;

# This script MUST run as "root"
BEGIN { unshift @INC, "./blib/lib/"; die "Must run as root\n" if $> != 0; }

# Constants
my $ProgName = '';
( $ProgName = $0 ) =~ s%.*/%%;

# Common sense options:
use strict qw(vars subs);
no warnings;
use warnings qw(FATAL closed threads internal debugging pack
    portable prototype inplace io pipe unpack malloc
    glob digit printf layer reserved taint closure
    semicolon);
no warnings qw(exec newline unopened);

#--------------------------------------------------------------------
# Needed packages
#--------------------------------------------------------------------
use POSIX qw(setsid strftime);
use Getopt::Std;
use File::Tail 0.91;

# The files to "tail"
my @LogFiles = qw( /var/log/audit/audit.log /var/log/auth.log /var/log/syslog );
my @files;

# The local log copy
my $LL_Copy;

# Program options
our $opt_d = 0;
our $opt_h = 0;

#--------------------------------------------------------------------
# Get possible options
getopts('dh') or ShowUsage();
if ($opt_h)
{
    print "Usage: $ProgName [-d]\n",
        "       -d Debug mode\n\n",
        "The script runs as a 'daemon' and forwards (most) log entries from these files:\n";
    foreach (@LogFiles)
    {
        print " $_\n";
    } ## end foreach (@LogFiles)

    exit(0);
} ## end if ($opt_h)

#--------------------------------------------------------------------
# Become a daemon process
#--------------------------------------------------------------------
sub Daemonize()
{

    # pretty command line in ps
    $0 = join( ' ', $0, @ARGV ) unless ($opt_d);

    chdir '/' or die "Can't chdir to '/': $!";

    # Redirect STDIN and STDOUT
    open( STDIN,  '<', '/dev/null' ) or die "Can't read '/dev/null': $!";
    open( STDOUT, '>', '/dev/null' ) or die "Can't write '/dev/null': $!";
    defined( my $pid = fork ) or die "Can't fork: $!";

    if ($pid)
    {

        # The parent can die now
        print "DEBUG: Parent dies" if ($opt_d);
        exit;
    } ## end if ($pid)

    setsid or die "Can't start new session: $!";
    open STDERR, '>&STDOUT' or die "Can't duplicate stdout: $!";

    # Show just the program name in the "ps" listing
    $0 = $ProgName;
} ## end sub Daemonize

#--------------------------------------------------------------------
# Open the "tail" files
foreach (@LogFiles)
{
    push( @files, File::Tail->new( name => "$_", debug => $opt_d ) );
} ## end foreach (@LogFiles)

#--------------------------------------------------------------------
# Open the local log copy
open ($LL_Copy, '>>', '/var/tmp/LLCopy.txt') || die "ERROR: Can't create/append '$LL_Copy'\n";
print $LL_Copy strftime( "%F %T", localtime ) . " Starting $ProgName\n";

#--------------------------------------------------------------------
# Become a daemon process
Daemonize;

#--------------------------------------------------------------------
# Eternal loop
while (1)
{

    my $nfound = File::Tail::select( undef, undef, undef, 60, @files );
    unless ($nfound)
    {
        my @ints;
        foreach (@files)
        {
            push( @ints, $_->interval );
        } ## end foreach (@files)
        print "Nothing new! - "
            . localtime(time) . "("
            . join( ",", @ints ) . ")\n";
    } ## end unless ($nfound)

    foreach (@files)
    {
        my $LogLine = $_->read unless $_->predict;
        next unless ( length($LogLine) );
        print "DEBUG: $LogLine" if ($opt_d);

        # Copy line into local log copy
        print $LL_Copy strftime( "%F %T", localtime ) . " $LogLine";

    } ## end foreach (@files)

    # Rotate local log copy
    my $LL_Size = (stat '/var/tmp/LLCopy.txt')[7];
    if ($LL_Size > 104857600)
    {
        # Local log copy is larger than 100MB
        print $LL_Copy strftime( "%F %T", localtime ) . " Rotating\n";
        close ($LL_Copy);
        open ($LL_Copy, '>>', '/var/tmp/LLCopy.txt') || die "ERROR: Can't create/append '$LL_Copy'\n";
        print $LL_Copy strftime( "%F %T", localtime ) . " Reopened\n";
    }
} ## end while (1)

# We are done
print $LL_Copy strftime( "%F %T", localtime ) . " Ending $ProgName\n";
exit(0);
__END__
