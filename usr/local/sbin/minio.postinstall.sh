#!/bin/bash
# Version 20241101-152722 checked into repository
################################################################
# (c) Copyright 2024 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/minio.postinstall.sh

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE

DEBUG='-q'
[[ $- ==  *x* ]] && DEBUG='-v'

#--------------------------------------------------------------------
# Create the user and group for minio
groupadd -r minio-user
useradd -m -r -g minio-user minio-user

# Get the package
HAVE_NEW=0
WGET_OPTS='--tries=2 --timeout=60'
DL_URL='https://dl.min.io/server/minio/release/linux-amd64/'
DL_IMG=$(wget $DEBUG $WGET_OPTS -O - "$DL_URL" | \
  grep -o '>minio.*amd64\.deb<' | sed 's/^.//;s/<$//')
wget $DEBUG $WGET_OPTS -O /tmp/minio.deb "${DL_URL}$DL_IMG"
if [ $? -eq 0 ] && [ -s /tmp/minio.deb ]
then
    cmp /tmp/minio.deb /usr/local/src/minio.deb
    if [ $? -ne 0 ]
    then
        HAVE_NEW=1
        cat /tmp/minio.deb > /usr/local/src/minio.deb
        dpkg -i /usr/local/src/minio.deb
    fi
    chown minio-user: /usr/local/bin/minio
    chmod 744 /usr/local/bin/minio
fi

# Set default values for minio
cat << EOT > /etc/default/minio
# Default setting for minio - adapt if necessary
MINIO_VOLUMES=/data

MINIO_OPTS=' --console-address=":9001" --address=":9000"'

MINIO_ROOT_USER=minioadmin
MINIO_ROOT_PASSWORD=minioadmin
EOT

#--------------------------------------------------------------------
# Create the SSL certificates
wget -q -O /usr/local/sbin/certgen \
  https://github.com/minio/certgen/releases/latest/download/certgen-linux-amd64
chmod 700 /usr/local/sbin/certgen
# Make the certificate valid for 100 years :)a
certgen -host "localhost,$(hostname -f)" -duration $((100*365*24))'h0m0s'
install -o minio-user -m 0600 public.crt private.key /home/minio-user/.minio/certs/

#--------------------------------------------------------------------
# Adapt the local firewall if necessary
if [ $(grep -c minio /etc/firehol/firehol.conf) -eq 0 ]
then
    patch -u /etc/firehol/firehol.conf << EOT
--- /etc/firehol/firehol.conf~	2024-10-21 05:11:34.000000000 -0400
+++ /etc/firehol/firehol.conf	2024-10-21 14:25:44.610100782 -0400
@@ -17,6 +17,10 @@
 # Private server4 ports
 #server_xwaadmin_ports="tcp/7071"
 #client_xwaadmin_ports="default"
+server_minio_ports="tcp/9000"
+client_minio_ports="default"
+server_mc_ports="tcp/9001"
+client_mc_ports="default"
 
 # Fix some TOS values
 tosfix
@@ -34,8 +38,7 @@
         protection strong 75/sec 50
 
         # Here are the services listening on enp6s18.
-        server4 "ssh" accept src "${home_net}"
-        #server4 "smtp imaps smtps https" accept
+        server4 "ssh minio mc" accept
         server4 ping accept
 
         # The following means that this machine can REQUEST anything via enp6s18.
EOT
    firehol restart
fi

# Enable and start the service
systemctl enable minio.service
if [ $HAVE_NEW -ne 0 ]
then
    systemctl restart minio.service || systemctl status minio.service
fi    

#--------------------------------------------------------------------
# Get the command line tool
wget $DEBUG $WGET_OPTS 'https://dl.min.io/client/mc/release/linux-amd64/mc' -O /tmp/mc
if [ $? -eq 0 ] && [ -s /tmp/mc ]
then
    cmp /tmp/mc /usr/local/sbin/mc &> /dev/null
    [ $? -ne 0 ] && cat /tmp/mc > /usr/local/sbin/mc
    chmod 744 /usr/local/sbin/mc
fi

#--------------------------------------------------------------------
# We are done
exit 0
