#!/bin/bash
################################################################
# (c) Copyright 2014 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################

#--------------------------------------------------------------------
# Only root can execute this script!
if [ $EUID -ne 0 ]
then
    echo 'You must be root to continue'
    exit 0
fi

#--------------------------------------------------------------------
# Sanity checks
if [ -s /etc/lsb-release ]
then
    source /etc/lsb-release
    if [ "T${DISTRIB_ID^^}" != 'TUBUNTU' ]
    then
        echo 'This is not an Ubuntu Server'
        exit 0
    fi
else
    echo 'This is not an Ubuntu Server'
    exit 0
fi

if [ "T$(uname -i)" != 'Tx86_64' ]
then
    echo 'This is not a 64-bit server'
    exit 0
fi

#--------------------------------------------------------------------
# Sensible defaults
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
DEBUG=''
[[ $- = *x* ]] && DEBUG='-v'

# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            

#--------------------------------------------------------------------
# This host and domain
THISHOST=$(hostname)
[[ $THISHOST = *.* ]] || THISHOST=$(hostname -f)
THISDOMAIN=${THISHOST#*.}


source /etc/lsb-release
if [ ! -x /usr/bin/add-apt-repository ]
then
    if [ ${DISTRIB_RELEASE%.*} -lt 14 ]
    then
        apt-get install python-software-properties
    else
        apt-get install software-properties-common
    fi
fi

if [ ! -s /etc/apt/sources.list.d/mrazavi-ubuntu-openvas-${DISTRIB_CODENAME}.list ]
then
    # Install the packages from the PPA
    add-apt-repository ppa:mrazavi/openvas
fi
apt-get update
apt-get install openvas nmap nikto wapiti smbclient

# Install arachni
A_DOWN=$(wget -q http://www.arachni-scanner.com/download/ -O - | \
   sed -e 's/<a href/\n<a href/' | grep -m 1 'linux-x86_64' | \
   sed -e 's/^.*http/http/;s/".*//;s/\.sha1//')
wget -q $A_DOWN -O /usr/local/src/arachni.tar.gz

# Configure certificates
if [ ! -s /var/lib/openvas/CA/cacert.pem ]
then
    openvas-mkcert
fi
if [ ! -s /var/lib/openvas/CA/clientcert.pem ]
then
    COUNTRY=$(sed -n 's/^..*_\(..\).*$/\1/p' <<< $LANG)
    read -p 'City for the client certificate ? ' CITY
    sed -e "s/^BASEDIR=.*/BASEDIR=\/tmp\/clientcert.$$/;s/DE/$COUNTRY/;s/Berlin/$CITY/" /usr/bin/openvas-mkcert-client > /tmp/openvas-mkcert-client
    chmod 700 /tmp/openvas-mkcert-client
    /tmp/openvas-mkcert-client -n om -i

    cp /tmp/clientcert.$$/01.pem /var/lib/openvas/CA/clientcert.pem
    rm -rf /tmp/clientcert.$$ /tmp/openvas-mkcert-client
fi

# Update databases
apt-get install sqlite3
openvas-nvt-sync
openvas-scapdata-sync
openvas-certdata-sync

# Restart with updated databases and rebuild caches
service openvas-scanner restart
service openvas-manager restart
openvasmd --rebuild --progress

# Setup the initial admin account
#openvasad -c add_user -n admin -r Admin

# Adjust the IP address that the manager listens on
#/etc/default/greenbone-security-assistant

#--------------------------------------------------------------------
# We are done
exit 0
