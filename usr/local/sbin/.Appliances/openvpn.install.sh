#!/bin/bash

source /etc/lsb-release

# As per https://openvpn.fox-it.com/repos/deb/README.html
wget 'https://openvpn.fox-it.com/repos/fox-crypto-gpg.asc' \
  -O /tmp/fox-crypto-gpg.asc
apt-key add /tmp/fox-crypto-gpg.asc
echo "deb http://openvpn.fox-it.com/repos/deb $DISTRIB_CODENAME main" > /etc/apt/sources.list.d/openvpn-nl.net.list
apt-get update
apt-get install openvpn-nl

cd /etc/openvpn-nl
if [ -d /usr/share/doc/openvpn-nl/examples/easy-rsa/2.0 ]
then
    cp -r /usr/share/doc/openvpn-nl/examples/easy-rsa/2.0 ./easy-rsa
elif [ -d /usr/share/easy-rsa ]
then
    cp -r /usr/share/easy-rsa ./easy-rsa
else
    echo "Please install a suiatble 'easy-rsa' package into /etc/openvpn-nl/easy-rsa"
    exit 1
fi
sed -i 's@EASY_RSA=.*@EASY_RSA="/etc/openvpn-nl/easy-rsa"@;s/KEY_SIZE=.*/KEY_SIZE=2048/;s/KEY_COUNTRY=.*/KEY_COUNTRY="US"/' easy-rsa/vars
sed -i 's/KEY_PROVINCE=.*/KEY_PROVINCE="NY"/;s/KEY_CITY=.*/KEY_CITY="Rochester"/;s/KEY_CN=.*/KEY_CN="www.btoy1.net"/' easy-rsa/vars
sed -i 's/KEY_ORG=.*/KEY_ORG="B-LUC Consulting"/;s/KEY_EMAIL=.*/KEY_EMAIL="consult@btoy1.net"/' easy-rsa/vars
sed -i 's/KEY_OU=.*/KEY_OU=/;s@etc/openvpn/@etc/openvpn-nl/@' easy-rsa/vars
[[ $- = *x* ]] && cat easy-rsa/vars

. ./easy-rsa/vars
./easy-rsa/clean-all
cd easy-rsa
ln -s openssl-1.0.0.cnf openssl.cnf
cd /etc/openvpn-nl
./easy-rsa/build-ca OpenVPN
./easy-rsa/build-key-server server
./easy-rsa/build-key client1
./easy-rsa/build-dh

openvpn-nl --genkey --secret ta.key

cat << EOT > /etc/openvpn-nl/openvpn.conf
# Default settings:
dev tun
proto udp
port 1194

# The encryption parameters:
ca /etc/openvpn-nl/easy-rsa/keys/ca.crt
cert /etc/openvpn-nl/easy-rsa/keys/server.crt
key /etc/openvpn-nl/easy-rsa/keys/server.key
dh /etc/openvpn-nl/easy-rsa/keys/dh2048.pem
tls-auth ta.key 0
#auth sha256

# Keep connections open:
keepalive 10 120
persist-key
persist-tun

# Drop privileges:
user nobody
group nogroup

# The VPN subnet:
server 192.168.194.0 255.255.255.0
ifconfig-pool-persist ipp.txt

# Push some settings:
push "route 192.168.1.0 255.255.255.0"
push "dhcp-option DNS 192.168.1.24"
push "dhcp-option DNS 4.2.2.1"

# Logs go here:
log-append /var/log/openvpn
status /var/log/openvpn-status.log
verb 3

# The localhost can manage this instance
management 127.0.0.1 7505

# Set the TOS field of the tunnel packet to what the payload's TOS is
passtos

# Let's use compression:
comp-lzo
cipher AES-256-CBC
min-platform-entropy 16

# External scripts:
script-security 2
client-connect /etc/openvpn-nl/Notify.sh
client-disconnect /etc/openvpn-nl/Notify.sh

# Tweaks as per https://community.openvpn.net/openvpn/wiki/Gigabit_Networks_Linux
#  (might or might not work):
tun-mtu 12000
fragment 0
mssfix 0

# Experimental (only on Linux):
fast-io
EOT

cat << EOT > /etc/openvpn-nl/Notify.sh
#!/bin/bash

case \$script_type in
client-connect)
        # Executed once when a OpenVPN client connects
        sendemail -q -f root@btoy1.net -t consult@btoy1.net -s mx -u 'OpenVPN - connection starting' << EON
'\$common_name' connected at \$time_ascii:
 Local IP : \$ifconfig_pool_local_ip
 Remote IP: \$ifconfig_pool_remote_ip
 Actual IP: \$trusted_ip

 Protocol : \$proto
EON
        ;;
client-disconnect)
        sendemail -q -f root@btoy1.net -t consult@btoy1.net -s mx -u  'OpenVPN - connection stopping' << EON
'\$common_name' will disconnect:
 Local IP : \$ifconfig_pool_local_ip
 Remote IP: \$ifconfig_pool_remote_ip
 Actual IP: \$trusted_ip

 Connected: \$time_duration second(s)
 RX Bytes : \$bytes_received
 TX Bytes : \$bytes_sent
EON
        ;;
esac
exit 0
EOT
chmod 755 /etc/openvpn-nl/Notify.sh

cat << EOT > /etc/openvpn-nl/btoy1.conf.client
dev tun
client
proto udp
remote 24.97.81.129 1194
resolv-retry infinite
user nobody
group nogroup
nobind
persist-key
persist-tun
ca /etc/openvpn-nl/ca.crt
cert /etc/openvpn-nl/cl1.btoy1.net.crt
key /etc/openvpn-nl/cl1.btoy1.net.key
tls-auth ta.key 1
auth sha256
remote-cert-tls server
comp-lzo
verb 3
log-append /etc/openvpn-nl/btoy1.log
status /var/log/openvpn-status.log
cipher AES-256-CBC
min-platform-entropy 16
passtos

# Tweaks as per https://community.openvpn.net/openvpn/wiki/Gigabit_Networks_Linux
#  (might or might not work):
tun-mtu 12000 
fragment 0 
mssfix 0 

# Experimental:
fast-io
EOT

# Enable port forwading
[ "T$(< /proc/sys/net/ipv4/ip_forward)" = 'T1' ] || echo 1 > /proc/sys/net/ipv4/ip_forward
