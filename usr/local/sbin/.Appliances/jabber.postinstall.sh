#!/bin/bash
################################################################
# (c) Copyright 2016 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            

#--------------------------------------------------------------------
# Specifying "-x" to the bash invocation = DEBUG
DEBUG='-q'
[[ $- = *x* ]] && DEBUG='-v'

#--------------------------------------------------------------------
# This host and domain
THISHOST=$(hostname)
[[ $THISHOST = *.* ]] || THISHOST=$(hostname -f)
THISDOMAIN=${THISHOST#*.}

#--------------------------------------------------------------------
# Install some pre-requisites
for PR in luarocks mercurial lua-dbi-sqlite3 sqlite3
do
    dpkg-query -S "$PR" &> /dev/null
    [ $? -ne 0 ] && apt install "$PR"
done

# Install the repository and get the package
# See https://prosody.im/download/package_repository
[ -s/etc/apt/sources.list.d/prosody.list ] || echo deb http://packages.prosody.im/debian $(lsb_release -sc) main > /etc/apt/sources.list.d/prosody.list
wget https://prosody.im/files/prosody-debian-packages.key -O- | sudo apt-key add -
apt update

#--------------------------------------------------------------------
# Install the package
for PR in prosody prosody-modules
do
    dpkg-query -S "$PR" &> /dev/null
    [ $? -ne 0 ] && apt install "$PR"
done

# Get some additional modules
# See https://prosody.im/doc/installing_modules#prosody-modules
mkdir -p /usr/local/Mercurial
cd /usr/local/Mercurial
if [ -d prosody-modules ]
then
    cd prosody-modules
    hg pull --update
else
    hg clone https://hg.prosody.im/prosody-modules/ prosody-modules
fi

# We need this for both the POP3 and Zimbra authentication modules
[ -z "$(luarocks list | grep ^pop3)" ] && luarocks install pop3

#--------------------------------------------------------------------
# Create a authentication method using "pop3"
# See https://prosody.im/doc/authentication and http://modules.prosody.im/mod_auth_external.html
wget -q -O /usr/lib/prosody/modules/mod_auth_pop3.lua \
  https://gitlab.com/bluc/AdminTools/raw/master/usr/local/bin/mod_auth_pop3.lua

#--------------------------------------------------------------------
# Create a authentication method for zimbra
# See https://prosody.im/doc/authentication
wget -q -O /usr/lib/prosody/modules/mod_auth_zimbra.lua \
  https://gitlab.com/bluc/AdminTools/raw/master/usr/local/bin/mod_auth_zimbra.lua

#--------------------------------------------------------------------
# Determine the correct editor for any edit functions
if [ -z "$EDITOR" ]
then
    # Use a flavor of "vi"
    if [ -x /usr/bin/vim.tiny ]
    then
        M_EDITOR=vim.tiny
    else
        M_EDITOR=vi
    fi
else
    # Use the editor specified in the environment variable
    M_EDITOR=$EDITOR
fi

#--------------------------------------------------------------------
# Use "sqlite3" as the storage backend
if [ -z "$(grep '^storage' /etc/prosody/prosody.cfg.lua)" ]
then
    sed -i 's/\-\-storage/storage/;s/\-\-sql.*SQLite3.*/sql = { driver = "SQLite3", database = "prosody.sqlite" }/' /etc/prosody/prosody.cfg.lua
fi

#--------------------------------------------------------------------
# Create a suitable "virtualhost" (if necessary)
# See https://prosody.im/doc/configure
if [ ! -s /etc/prosody/conf.d/${THISDOMAIN}.cfg.lua ]
then
    #- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Create a configuration file for the local domain
    cat << EOT > /etc/prosody/conf.d/${THISDOMAIN}.cfg.lua
VirtualHost "${THISDOMAIN}"
	-- Assign this host a certificate for TLS, otherwise it would use the one
	-- set in the global section (if any).
	-- Note that old-style SSL on port 5223 only supports one certificate, and will always
	-- use the global one.
	ssl = {
		key = "/etc/prosody/certs/${THISDOMAIN}.key";
		certificate = "/etc/prosody/certs/${THISDOMAIN}.crt";
	        options = { "no_sslv2", "no_sslv3", "cipher_server_preference"};
	}
	c2s_require_encryption = true

        -- Define admins for this domain
	-- ADAPT and enable the list of specific admins (if desired):
        -- admins = { "root@${THISDOMAIN}" }

	-- Use Zimbra authentication module
	authentication = "zimbra"
        zimbra_domain = "${THISDOMAIN}"
	-- REVIEW and ADAPT the IP address:
	zimbra_auth_host = "192.168.1.110"

------ Components ------
-- You can specify components to add hosts that provide special services,
-- like multi-user conferences, and transports.
-- For more information on components, see http://prosody.im/doc/components

---Set up a MUC (multi-user chat) room server on conference.${THISDOMAIN}:
Component "conference.${THISDOMAIN}" "muc"
	name = "Chatrooms for ${THISDOMAIN}"

-- Set up a SOCKS5 bytestream proxy for server-proxied file transfers:
Component "proxy.${THISDOMAIN}" "proxy65"
        proxy65_address = "${THISHOST}"
        proxy65_acl = { "${THISDOMAIN}" }
        
---Set up an external component (default component port is 5347)
--
-- External components allow adding various services, such as gateways/
-- transports to other networks like ICQ, MSN and Yahoo. For more info
-- see: http://prosody.im/doc/components#adding_an_external_component
--
--Component "gateway.${THISDOMAIN}"
--	component_secret = "password"
EOT
    $M_EDITOR +/admins /etc/prosody/conf.d/${THISDOMAIN}.cfg.lua

    #- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Create the custom SSL certificate

    # Define some parameters for openssl cert
    DURATION=$((5 * 366))

    # Create a temporary directory for this process
    TEMPDIR=$(mktemp -d)
    cd $TEMPDIR

    # Create a private key for the CA
    echo '01' > serial
    yes "" | openssl genrsa -out ca.key 2048 &> /dev/null

    # Create CA signing request
    sed -e "s/@TBD@/Technical Operations/" -e "s/@ORG@/$THISDOMAIN/" -e "s/@CN@/$THISHOST/" << EOT > .config
HOME                    = .
RANDFILE                = /dev/urandom
oid_section             = new_oids
[ new_oids ]
[ ca ]
[ CA_default ]
policy          = policy_match
[ policy_match ]
countryName             = match
stateOrProvinceName     = match
organizationName        = match
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional
[ policy_anything ]
countryName             = optional
stateOrProvinceName     = optional
localityName            = optional
organizationName        = optional
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional
[ req ]
default_bits            = 2048
default_keyfile         = privkey.pem
distinguished_name      = req_distinguished_name
attributes              = req_attributes
string_mask             = nombstr
req_extensions		= v3_req
[ req_distinguished_name ]
countryName                     = Country Name (2 letter code)
countryName_default             = US
countryName_min                 = 2
countryName_max                 = 2
stateOrProvinceName             = State or Province Name (full name)
stateOrProvinceName_default     = New York
localityName                    = Locality Name (eg, city)
localityName_default            = Rochester
0.organizationName              = Organization Name (eg, company)
0.organizationName_default      = @ORG@
organizationalUnitName          = Organizational Unit Name (eg, section)
# For an authority certificate use: Technical Operations
# For a server certificate use: External Support
organizationalUnitName_default  = @TBD@
commonName                      = Common Name (eg, YOUR name)
commonName_max                  = 64
commonName_default              = @CN@
emailAddress                    = Email Address
emailAddress_max                = 40
[ req_attributes ]
challengePassword               = A challenge password
challengePassword_min           = 4
challengePassword_max           = 20
unstructuredName                = An optional company name
[ usr_cert ]
basicConstraints=CA:FALSE
nsComment                       = "OpenSSL Generated Certificate"
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid,issuer:always
[ v3_req ]
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
subjectAltName = @alt_names
[alt_names]
DNS.1 = $THISHOST
DNS.2 = $THISDOMAIN
[ v3_ca ]
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always,issuer:always
basicConstraints = CA:true
[ crl_ext ]
authorityKeyIdentifier=keyid:always,issuer:always
EOT
$M_EDITOR .config
    yes "" | openssl req -new -x509 -config .config -key ca.key -out server.pem -days $DURATION

    # Create CA signing request
    sed -e "s/@TBD@/Technical Operations/" -e "s/@ORG@/$THISDOMAIN/" -e "s/@CN@/$THISDOMAIN/" << EOT > .config
HOME                    = .
RANDFILE                = /dev/urandom
oid_section             = new_oids
[ new_oids ]
[ ca ]
[ CA_default ]
policy          = policy_match
[ policy_match ]
countryName             = match
stateOrProvinceName     = match
organizationName        = match
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional
[ policy_anything ]
countryName             = optional
stateOrProvinceName     = optional
localityName            = optional
organizationName        = optional
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional
[ req ]
default_bits            = 2048
default_keyfile         = privkey.pem
distinguished_name      = req_distinguished_name
attributes              = req_attributes
string_mask             = nombstr
req_extensions		= v3_req
[ req_distinguished_name ]
countryName                     = Country Name (2 letter code)
countryName_default             = US
countryName_min                 = 2
countryName_max                 = 2
stateOrProvinceName             = State or Province Name (full name)
stateOrProvinceName_default     = New York
localityName                    = Locality Name (eg, city)
localityName_default            = Rochester
0.organizationName              = Organization Name (eg, company)
0.organizationName_default      = @ORG@
organizationalUnitName          = Organizational Unit Name (eg, section)
# For an authority certificate use: Technical Operations
# For a server certificate use: External Support
organizationalUnitName_default  = @TBD@
commonName                      = Common Name (eg, YOUR name)
commonName_max                  = 64
commonName_default              = @CN@
emailAddress                    = Email Address
emailAddress_max                = 40
[ req_attributes ]
challengePassword               = A challenge password
challengePassword_min           = 4
challengePassword_max           = 20
unstructuredName                = An optional company name
[ usr_cert ]
basicConstraints=CA:FALSE
nsComment                       = "OpenSSL Generated Certificate"
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid,issuer:always
[ v3_req ]
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
subjectAltName = @alt_names
[alt_names]
DNS.1 = $THISHOST
DNS.2 = $THISDOMAIN
[ v3_ca ]
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always,issuer:always
basicConstraints = CA:true
[ crl_ext ]
authorityKeyIdentifier=keyid:always,issuer:always
EOT
$M_EDITOR .config
    yes "" | openssl req -config .config -new -sha256 -key ca.key -out ca.csr &> /dev/null

    # Create X.509 certificate for CA signed by itself
    cat <<EOT > .config
#extensions = x509v3
#[ x509v3 ]
#subjectAltName   = email:copy
#basicConstraints = CA:true,pathlen:0
#nsComment        = "Custom CA certificate"
#nsCertType       = sslCA
EOT
    yes "" | openssl x509 -extfile .config -req -days $DURATION -signkey ca.key -in ca.csr -out ca.crt

    # Create private key for a server
    yes "" | openssl genrsa -out server.key 2048 &> /dev/null

    # Create server signing request
    sed -e "s/@TBD@/External support/" -e "s/@ORG@/$THISDOMAIN/" -e "s/@CN@/$THISDOMAIN/" << EOT > .config
HOME                    = .
RANDFILE                = /dev/urandom
oid_section             = new_oids
[ new_oids ]
[ ca ]
[ CA_default ]
policy          = policy_match
[ policy_match ]
countryName             = match
stateOrProvinceName     = match
organizationName        = match
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional
[ policy_anything ]
countryName             = optional
stateOrProvinceName     = optional
localityName            = optional
organizationName        = optional
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional
[ req ]
default_bits            = 2048
default_keyfile         = privkey.pem
distinguished_name      = req_distinguished_name
attributes              = req_attributes
string_mask             = nombstr
req_extensions		= v3_req
[ req_distinguished_name ]
countryName                     = Country Name (2 letter code)
countryName_default             = US
countryName_min                 = 2
countryName_max                 = 2
stateOrProvinceName             = State or Province Name (full name)
stateOrProvinceName_default     = New York
localityName                    = Locality Name (eg, city)
localityName_default            = Rochester
0.organizationName              = Organization Name (eg, company)
0.organizationName_default      = @ORG@
organizationalUnitName          = Organizational Unit Name (eg, section)
# For an authority certificate use: Technical Operations
# For a server certificate use: External Support
organizationalUnitName_default  = @TBD@
commonName                      = Common Name (eg, YOUR name)
commonName_max                  = 64
commonName_default              = @CN@
emailAddress                    = Email Address
emailAddress_max                = 40
[ req_attributes ]
challengePassword               = A challenge password
challengePassword_min           = 4
challengePassword_max           = 20
unstructuredName                = An optional company name
[ usr_cert ]
basicConstraints=CA:FALSE
nsComment                       = "OpenSSL Generated Certificate"
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid,issuer:always
[ v3_req ]
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
subjectAltName = @alt_names
[alt_names]
DNS.1 = $THISHOST
DNS.2 = $THISDOMAIN
[ v3_ca ]
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always,issuer:always
basicConstraints = CA:true
[ crl_ext ]
authorityKeyIdentifier=keyid:always,issuer:always
EOT
$M_EDITOR .config
    yes "" | openssl req -config .config -new -key server.key -out server.csr &> /dev/null

# Shortcut for a ECDSA key and request
#openssl req -new -sha256 -newkey ec:<(openssl ecparam -name prime256v1) -nodes -days $DURATION \
#  -keyout newkey.pem -out newreq.pem

    LOCALIF=$(ip route get 8.8.8.8 | awk '/via/{print $5; exit}')
    LOCALIP=$(ifconfig $LOCALIF | sed -n "s/.*inet addr:\([0-9.]*\).*/\1/p")
    cat <<EOT > .config
[ x509v3 ]
subjectAltName   = DNS:${THISHOST},DNS:${THISDOMAIN},IP:${LOCALIP}
#basicConstraints = CA:false,pathlen:0
#nsComment        = "Client certificate"
#nsCertType       = client
EOT
    yes "" | openssl x509 -extfile .config -days $DURATION -CAserial serial -CA ca.crt -CAkey ca.key -in server.csr -req -out server.crt -extensions x509v3

    # Show the new cert
    openssl x509 -in server.crt -noout -text

    # Save the certificates and the key
    install -m 0640 -o root -g ssl-cert server.key /etc/prosody/certs/${THISDOMAIN}.key
    install -m 0644 -o root -g root server.crt /etc/prosody/certs/${THISDOMAIN}.crt
    cd /usr/local/sbin
    rm -rf $TEMPDIR
fi

#--------------------------------------------------------------------
# Adjust the log rotation
if [ ! -z "$(grep 'prosody.log' /etc/logrotate.d/prosody)" ]
then
    echo '/var/log/prosody/prosody.* {' > /tmp/logrotate.prosody
    sed '1d' /etc/logrotate.d/prosody >> /tmp/logrotate.prosody
    cat /tmp/logrotate.prosody > /etc/logrotate.d/prosody
    rm -f /tmp/logrotate.prosody
fi
if [ -z "$(grep prosody.debug /etc/prosody/prosody.cfg.lua)" ]
then
    sed -i 's@info = @debug = "/var/log/prosody/prosody.debug";\n	info = @' /etc/prosody/prosody.cfg.lua 
fi

#--------------------------------------------------------------------
# We are done
service prosody reload
exit 0
