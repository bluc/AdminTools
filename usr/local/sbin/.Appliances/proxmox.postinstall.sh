#!/bin/bash
################################################################
# (c) Copyright 2017 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# Based on http://www.thekunit.com/install-plex-server-ubuntu-server

#--------------------------------------------------------------------
# Only root can execute this script!
if [ $EUID -ne 0 ]
then
    echo 'You must be root to continue'
    exit 0
fi

#--------------------------------------------------------------------
# Sanity checks
if [ -s /etc/os-release ]
then
    source /etc/os-release
    if [ "T$ID" != 'Tdebian' ]
    then
        echo 'This is not a Debian Server'
        exit 1
    fi
else
    echo 'This is not an Debian Server'
    exit 1
fi

if [ "T$(uname -m)" != 'Tx86_64' ]
then
    echo 'This is not a 64-bit server'
    exit 1
fi

if [ ! -s /etc/pve/datacenter.cfg ]
then
    echo 'This is not a Proxmox server'
    exit 1
fi

#--------------------------------------------------------------------
# Sensible defaults
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
DEBUG=''
[[ $- = *x* ]] && DEBUG='-v'

# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            

#--------------------------------------------------------------------
# This host, domain and networking parameters
THISHOST=$(hostname)
[[ $THISHOST = *.* ]] || THISHOST=$(hostname -f)
THISDOMAIN=${THISHOST#*.}

LOCALIP=$(ifconfig vmbr0 | sed -rn 's/.*r:([^ ]+) .*/\1/p')
LOCALMASK=$(ifconfig vmbr0 | sed -n -e 's/.*Mask:\(.*\)$/\1/p')
# From: http://www.routertech.org/viewtopic.php?t=1609
l="${LOCALIP%.*}";r="${LOCALIP#*.}";n="${LOCALMASK%.*}";m="${LOCALMASK#*.}"
LOCALNET=$((${LOCALIP%%.*}&${LOCALMASK%%.*})).$((${r%%.*}&${m%%.*})).$((${l##*.}&${n##*.})).$((${LOCALIP##*.}&${LOCALMASK##*.}))
GATEWAY=$(ip route show |awk '/default/{print $3}')

#--------------------------------------------------------------------
# Adjust the mount options for any ext{3|4} partition
# See http://www.howtoforge.com/reducing-disk-io-by-mounting-partitions-with-noatime
awk 'substr($3,1,3) != "ext" {print}' /etc/fstab > /tmp/$$.fstab
awk 'substr($3,1,3) == "ext" {print $1" "$2" "$3" noatime,errors=remount-ro "$5" "$6}' /etc/fstab >> /tmp/$$.fstab
diff -u /tmp/$$.fstab /etc/fstab &> /dev/null
[ $? -ne 0 ] && cat /tmp/$$.fstab > /etc/fstab
rm -f /tmp/$$.fstab

#--------------------------------------------------------------------
# Adjust the Proxmox repositories
rm -f /etc/apt/sources.list.d/pve-enterprise.list
DEBIAN_RELEASE=$(awk '/^VERSION=/{gsub(/[\(\)]/,"");gsub(/"/,"");print $2}' /etc/os-release)
cat << EOT > /etc/apt/sources.list.d/pve-non-enterprise.list
# As per http://pve.proxmox.com/wiki/Package_repositories#Proxmox_VE_Enterprise_Repository
deb http://download.proxmox.com/debian $DEBIAN_RELEASE pve-no-subscription
EOT
apt-get update

#--------------------------------------------------------------------
# Check whether "Virtualization Extensions" are enabled in BIOS
apt-get install msr-tools
modprobe msr
if [ "T$(rdmsr 0x3A)" = 'T5' ]
then
    echo "'Virtualization Extensions' are enabled in BIOS"
elif [ ! -z "$(lspci -v | grep -m  1 'Dell Device')" ]
then
    if [ -x /opt/dell/toolkit/bin/syscfg ]
    then
        /opt/dell/toolkit/bin/syscfg --virtualization=enable
        shutdown -r +2min Activating virtualization extensions in BIOS
    else
        echo "Go into the BIOS and enable 'Virtualization Extensions'"
        exit 1
    fi
else
    echo "Go into the BIOS and enable 'Virtualization Extensions'"
    exit 1
fi

#--------------------------------------------------------------------
# Get some useful packages
apt-get update
apt-get upgrade
apt-get install python-software-properties sendemail joe firehol lshw postfix bsd-mailx libunix-syslog-perl sudo irqbalance ethtool unzip openvswitch-switch

#--------------------------------------------------------------------
# Make sure our operator can use "sudo"
read -p 'User-ID of operator ? ' UO
if [ ! -z "$UO" ]
then
    [ -z "$(getent passwd $UO)" ] || usermod -G sudo $UO
fi

# Adapt SSH configs
#--------------------------------------------------------------------
[ -z "$(grep '^[[:space:]]*PermitRootLogin.*without-password' /etc/ssh/sshd_config)" ] && sed -ie 's/^[[:space:]]*PermitRootLogin.*/PermitRootLogin without-password/' /etc/ssh/sshd_config
sed -ie 's/^[[:space:]]*Protocol.*/Protocol 2/' /etc/ssh/ssh_config

#--------------------------------------------------------------------
# Setup email
read -p 'Name or IP address of email relay host ? ' RHOST
if [ ! -z "$RHOST" ]
then
    [ -z "$(postconf -h relayhost)" ] && postconf -e relayhost=$RHOST
fi
[ "T$(postconf -h smtp_host_lookup)" = 'Tdns,native' ] || postconf -e 'smtp_host_lookup=dns,native'

#--------------------------------------------------------------------
# Setup the firewall
if [ -d /etc/firehol ]
then
        cat << EOT > /etc/default/firehol
#To enable firehol at startup set START_FIREHOL=YES
START_FIREHOL=YES
#If you want to have firehol wait for an iface to be up add it here
WAIT_FOR_IFACE=""
EOT

        cat << EOT > /etc/firehol/firehol.conf
################################################################
version 5

# IP address ranges
home_ips='${LOCALNET}/$LOCALMASK'
# Proxmox server itself:
pm_ip='$LOCALIP'

# Web access to Proxmox GUI
server_pm_gui_ports="tcp/8006"
client_pm_gui_ports="default"

# Access to the Proxmox server itself
interface vmbr0 home
        # Protect against malicious traffic
        protection strong 10/sec 10
        # By default REJECT traffic
        policy reject
        # Allow some traffic from internal
        server all accept src "\${home_ips}" dst "\${pm_ip}"
        server "ssh" accept dst "\${pm_ip}"
        server all accept dst not "\${pm_ip}"
        # Allow everything outbound
        client all accept

# Access to any guest machines
router LAN2LAN inface vmbr0 outface vmbr0
        policy reject
        protection strong 10/sec 10
        server all accept
        client all accept

## Access to secondary network (e.g. storage)
#interface eth1 storage
#        policy reject
#        protection strong 10/sec 10
#        server all accept
#        client all accept
EOT

        # Activate the firewall rules
        yes commit | firehol try
fi

#--------------------------------------------------------------------
# Create a meaningful cronjob
read -p 'Email address for cron messages ? ' MTO
cat << EOT > /etc/cron.d/bluc
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/sbin:/sbin:/usr/local/bin:/usr/bin:/bin
MAILTO=$MTO
#===============================================================
# Healthchecks
* * * * *       root    [ -x /usr/local/sbin/HealthCheck.sh ] && /usr/local/sbin/HealthCheck.sh
# The nightly cleanups, updates and backup
12 1 * * *      root    [ -x /usr/local/sbin/LiSysCo.sh ] && /usr/local/sbin/LiSysCo.sh
11 0 * * *      root    [ -x /usr/local/sbin/VMDump.sh ] && /usr/local/sbin/VMDump.sh
EOT

#--------------------------------------------------------------------
# Show what network configuration should look like:
echo "Adapt '/etc/network/interfaces' to support Open-vSwitch:"
cat << EOT
allow-vmbr0 eth0
iface eth0 inet manual
        ovs_type OVSPort
        ovs_bridge vmbr0

auto vmbr0
iface vmbr0 inet static
        address  $LOCALNET
        netmask  $LOCALMASK
        gateway  $GATEWAY
        ovs_type OVSBridge
        ovs_ports eth0
        post-up /sbin/ip route change default via $GATEWAY dev vmbr0 initcwnd 128 initrwnd 128
EOT
read -p 'Press ENTER to continue' YN

#--------------------------------------------------------------------
# We are done
exit 0
