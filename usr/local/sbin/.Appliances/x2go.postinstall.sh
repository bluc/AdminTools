#!/bin/bash
################################################################
# (c) Copyright 2017 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# Based on http://www.thekunit.com/install-plex-server-ubuntu-server

#--------------------------------------------------------------------
# Only root can execute this script!
if [ $EUID -ne 0 ]
then
    echo 'You must be root to continue'
    exit 0
fi

#--------------------------------------------------------------------
# Sanity checks
if [ -s /etc/lsb-release ]
then
    source /etc/lsb-release
    if [ "T${DISTRIB_ID^^}" != 'TUBUNTU' ]
    then
        echo 'This is not an Ubuntu Server'
        exit 0
    fi
else
    echo 'This is not an Ubuntu Server'
    exit 0
fi

if [ "T$(uname -m)" != 'Tx86_64' ]
then
    echo 'This is not a 64-bit server'
    exit 1
fi

#--------------------------------------------------------------------
# Sensible defaults
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
DEBUG=''
[[ $- = *x* ]] && DEBUG='-v'

# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            

#--------------------------------------------------------------------
# This host and domain
THISHOST=$(hostname)
[[ $THISHOST = *.* ]] || THISHOST=$(hostname -f)
THISDOMAIN=${THISHOST#*.}

#--------------------------------------------------------------------
# Adjust the mount options for any ext{3|4} partition
# See http://www.howtoforge.com/reducing-disk-io-by-mounting-partitions-with-noatime
awk 'substr($3,1,3) != "ext" {print}' /etc/fstab > /tmp/$$.fstab
awk 'substr($3,1,3) == "ext" {print $1" "$2" "$3" noatime,errors=remount-ro "$5" "$6}' /etc/fstab >> /tmp/$$.fstab
diff -u /tmp/$$.fstab /etc/fstab &> /dev/null
[ $? -ne 0 ] && cat /tmp/$$.fstab > /etc/fstab
rm -f /tmp/$$.fstab

# See http://wiki.x2go.org/doku.php/doc:installation:x2goserver
# Setup the x2go repositories
apt-get install software-properties-common
add-apt-repository ppa:x2go/stable

# Install the x2go packages
apt-get update
apt-get install x2goserver x2goserver-xsession

# Get some useful packages
apt-get install sendemail joe firehol lshw postfix bsd-mailx libunix-syslog-perl sudo ethtool unzip xtables-addons-dkms

#--------------------------------------------------------------------
# Adapt SSH configs
[ -z "$(grep '^[[:space:]]*PermitRootLogin.*without-password' /etc/ssh/sshd_config)" ] && sed -ie 's/^[[:space:]]*PermitRootLogin.*/PermitRootLogin without-password/' /etc/ssh/sshd_config
sed -ie 's/^[[:space:]]*Protocol.*/Protocol 2/' /etc/ssh/ssh_config

apt-get update
apt-get upgrade

# We are done
exit 0
