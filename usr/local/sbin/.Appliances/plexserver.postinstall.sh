#!/bin/bash
################################################################
# (c) Copyright 2014 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# Based on http://www.thekunit.com/install-plex-server-ubuntu-server

#--------------------------------------------------------------------
# Only root can execute this script!
if [ $EUID -ne 0 ]
then
    echo 'You must be root to continue'
    exit 0
fi

#--------------------------------------------------------------------
# Sanity checks
if [ -s /etc/lsb-release ]
then
    source /etc/lsb-release
    if [ "T${DISTRIB_ID^^}" != 'TUBUNTU' ]
    then
        echo 'This is not an Ubuntu Server'
        exit 0
    fi
else
    echo 'This is not an Ubuntu Server'
    exit 0
fi

if [ "T$(uname -i)" != 'Tx86_64' ]
then
    echo 'This is not a 64-bit server'
    exit 0
fi

#--------------------------------------------------------------------
# Sensible defaults
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
DEBUG=''
[[ $- = *x* ]] && DEBUG='-v'

# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            

#--------------------------------------------------------------------
# This host and domain
THISHOST=$(hostname)
[[ $THISHOST = *.* ]] || THISHOST=$(hostname -f)
THISDOMAIN=${THISHOST#*.}

#--------------------------------------------------------------------
# Adjust the mount options for any ext{3|4} partition
# See http://www.howtoforge.com/reducing-disk-io-by-mounting-partitions-with-noatime
awk 'substr($3,1,3) != "ext" {print}' /etc/fstab > /tmp/$$.fstab
awk 'substr($3,1,3) == "ext" {print $1" "$2" "$3" noatime,errors=remount-ro "$5" "$6}' /etc/fstab >> /tmp/$$.fstab
diff -u /tmp/$$.fstab /etc/fstab &> /dev/null
[ $? -ne 0 ] && cat /tmp/$$.fstab > /etc/fstab
rm -f /tmp/$$.fstab

#--------------------------------------------------------------------
# Install some pre-requisites
apt-get install libavahi-core7 avahi-daemon avahi-utils

# Extract the download link for the Ubuntu package
# Eg.: plexmediaserver_0.9.15.2.1663-7efd046_amd64.deb
wget -q -O /tmp/$$ https://plex.tv/downloads
PLEX_LINK=$(awk -F\" '/Ubuntu..64-bit/ {print $2}' /tmp/$$)

# Download the package
rm -f /tmp/$$
wget -q -O /tmp/$$ $PLEX_LINK
if [ -s /tmp/$$ ]
then
    cmp /tmp/$$ /usr/local/src/plexmediaserver_amd64.deb &> /dev/null
    [ $? -ne 0 ] && mv /tmp/$$ /usr/local/src/plexmediaserver_amd64.deb
fi

[ -s /usr/local/src/plexmediaserver_amd64.deb ] && dpkg -Ei /usr/local/src/plexmediaserver_amd64.deb

# Get the Discover Channel plugin
wget -q -O /tmp/$$ https://github.com/meriko/Discovery.bundle/archive/master.zip
cd /tmp
unzip -o $$
PLEX_HOME=$(getent passwd plex | cut -d: -f6)
mv Discovery.bundle-master $PLEX_HOME/Library/Application\ Support/Plex\ Media\ Server/Plug-ins/Discover.bundle
chown -R plex: $PLEX_HOME/Library/Application\ Support/Plex\ Media\ Server/Plug-ins/Discover.bundle
service plexmediaserver restart

#--------------------------------------------------------------------
# Setup a crontab
if [ ! -s /etc/cron.d/btoy1 ]
then
    cat << EOT > /etc/cron.d/btoy1
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/sbin:/sbin:/usr/local/bin:/usr/bin:/bin
MAILTO=root
#---------------------------------------------------------------
# Perform health checks
* * * * *       root    [ -x /usr/local/sbin/HealthCheck.sh ] && HealthCheck.sh
EOT
fi

#--------------------------------------------------------------------
# Setup some housekeeping scripts
if [ ! -s /usr/local/sbin/HealthCheck.sh ]
then
    cat << EOT > /usr/local/sbin/HealthCheck.sh
#!/bin/bash

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

PROG=\${0##*/}
ionice -c2 -n7 -p \$\$

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/\$PROG.lock
if [ -f \$LOCKFILE ]
then
    # The file exists so read the PID   
    MYPID=\$(< \$LOCKFILE)
    [ -z "\$(ps h -p \$MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f \$LOCKFILE /tmp/\$\$*" EXIT
echo "\$\$" > \$LOCKFILE            

DEBUG=''
[[ \$- = *x* ]] && DEBUG='-v'
CURMIN=\$(date +%-M)
CURHR=\$(date +%-H)

#--------------------------------------------------------------------
# Every 5 minutes:
if [ "T\$DEBUG" = 'T-v' -o \$((\$CURMIN % 5)) -eq 2 ]
then
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Discard unused blocks on supported file systems
#SSD_DRIVES_ONLY#    if [ -x /sbin/fstrim-all ]
#SSD_DRIVES_ONLY#    then
#SSD_DRIVES_ONLY#        # Use the script providing by Linux distribution
#SSD_DRIVES_ONLY#        fstrim-all
#SSD_DRIVES_ONLY#    else
#SSD_DRIVES_ONLY#        for FS in \$(awk '/ext3|ext4|xfs|btrfs/ {print \$2}' /proc/mounts)
#SSD_DRIVES_ONLY#        do
#SSD_DRIVES_ONLY#            fstrim \$DEBUG \$FS
#SSD_DRIVES_ONLY#        done
#SSD_DRIVES_ONLY#    fi

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Check whether we use more than 256MB swap
    SW_TOTAL=\$(awk '/^SwapTotal/ {print \$2}' /proc/meminfo)
    SW_FREE=\$(awk '/^SwapFree/ {print \$2}' /proc/meminfo)  
    SW_USED=\$((\$SW_TOTAL - \$SW_FREE))
    if [ \$SW_USED -gt 262144 ]
    then
        # Using more than 256MB swap
        logger -it HealthCheck -p daemon.info -- "Swap usage over 256MB"
        if [ \$((\$CURMIN % 30)) -eq 11 ]
        then
            # Send an email every 30 minutes
            echo "Used swap: \${SW_USED}KB" > /tmp/\$\$

            # Get the top 10 swap eating processes
            for F in /proc/*/status
            do
                awk '/^(Pid|VmSwap)/{printf \$2 " " \$3}END{ print ""}' \$F
            done | sort -k 2 -n -r | head >> /tmp/\$\$

            sendemail -q -f root@\$THISHOST -u "\$THISHOST: Swap usage over 256MB" \
              -t root -s mail -o tls=no < /tmp/\$\$
        fi
    fi

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Check filesystem usage
    df -PTh | awk '/ext3|ext4|xfs|btrfs/ {print \$7" "int(\$6)}' | while read FS PERC
    do
        logger -it HealthCheck -p daemon.info -- "Filesystem usage on '\$FS' is \${PERC}%"
        if [ \$PERC -gt 90 ]
        then
            # We have a real problem!
            echo "" | sendemail -q -f root@\$THISHOST \
              -u "\$THISHOST: EXTREME HIGH DISK USAGE on '\$FS': \${PERC}%" \
              -t root -s mail -o tls=no
        elif [ \$PERC -gt 85 ]
        then
            # We should pay attention
            echo "" | sendemail -q -f root@\$THISHOST \
              -u "\$THISHOST: High disk usage on '\$FS': \${PERC}%" \
              -t root -s mail -o tls=no
        fi
    done
fi

#--------------------------------------------------------------------
# Hourly:
if [ "T\$DEBUG" = 'T-v' -o \$CURMIN -eq 2 ]
then
    # Update time
    ntpdate -u \$DEBUG 0.debian.pool.ntp.org 1.debian.pool.ntp.org 2.debian.pool.ntp.org 3.debian.pool.ntp.org &
fi

#--------------------------------------------------------------------
# We are done 
exit 0
EOT
    chmod 744 /usr/local/sbin/HealthCheck.sh
fi

#--------------------------------------------------------------------
# We are done
exit 0

