#!/bin/bash
################################################################
# (c) Copyright 2016 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            

#--------------------------------------------------------------------
# Specifying "-x" to the bash invocation = DEBUG
DEBUG='-q'
[[ $- = *x* ]] && DEBUG='-v'

#--------------------------------------------------------------------
# This host and domain
THISHOST=$(hostname)
[[ $THISHOST = *.* ]] || THISHOST=$(hostname -f)
THISDOMAIN=${THISHOST#*.}

#--------------------------------------------------------------------
# Install some pre-requisites
apt-get install apache2 libapache2-mod-php7.0 php7.0-cli php7.0-mbstring php7.0-sqlite3
apt-get install php7.0-opcache php7.0-json php7.0-mysql php7.0-pgsql php7.0-ldap php7.0-gd

#--------------------------------------------------------------------
# Download the package and install it
# See https://kanboard.net/documentation/ubuntu-installation
wget https://kanboard.net/kanboard-latest.zip -O /usr/local/src/kanboard-latest.zip
cd /var/www/html
unzip -o /usr/local/src/kanboard-latest.zip
chown -R www-data:www-data kanboard/data

read -p 'Is this server behind a reverse proxy ? ' YN
if [ "T${YN^^}" = 'TY' ]
then
     # Enable logging of proxy IP address in Apache logs
     a2enmod remoteip
     echo 'RemoteIPHeader X-Forwarded-For' > /etc/apache2/conf-enabled/remoteip.conf
fi
sed -i -e 's/%h/%a/g' /etc/apache2/apache2.conf

# Obfuscate the server signature as best as we can
if [ -z "$(grep 'ServerSignature off' /etc/apache2/conf-enabled/security.conf)" ]
then
    sed -e '/^ServerSignature/#SEE BOTTOM OF FILE# ServerSignature/' /etc/apache2/conf-enabled/security.conf > /tmp/$$.security.conf
    echo 'ServerSignature off' >> /tmp/$$.security.conf
    cat /tmp/$$.security.conf > /etc/apache2/conf-enabled/security.conf
fi

#--------------------------------------------------------------------
# Finally restart Apache
service apache2 restart

#--------------------------------------------------------------------
# Create the cronjob for Kanban
cat << EOT > /etc/cron.d/kanban
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/sbin:/sbin:/usr/local/bin:/usr/bin:/bin
MAILTO=rootmail@btoy1.rochester.ny.us
##############################################################################
# Daily Kanban jobs
3 3 * * * cd /var/www/html/kanboard && ./cli cronjob >/dev/null 2>&1
EOT

#--------------------------------------------------------------------
# Edit the main HTML file
cat << EOT > /var/www/html/index.html
<?php
header("Location: http://".\$_SERVER['SERVER_NAME']."/kanboard/", true, 301);
exit();
?>
EOT

#--------------------------------------------------------------------
# Install simple SMTP mailer
apt-get install ssmtp
if [ -z "$(grep KANBOARD /etc/ssmtp/ssmtp.conf)" ]
then
    read -p 'Name or IP of email smart host ? ' MRELAY
    cat > /etc/ssmtp/ssmtp.conf <<EOT
# DO NOT DELETE THIS LINE: KANBOARD email config
# The person who gets all mail for userids < 1000
# Make this empty to disable rewriting.
root=postmaster
mailhub=${MRELAY}:25
#UseSTARTTLS=NO
UseTLS=NO
#AuthUser=username
#AuthPass=password
#AuthMethod=LOGIN

# Where will the mail seem to come from?
rewriteDomain=$THISDOMAIN

# The full hostname
hostname=$THISHOST

# Are users allowed to set their own From: address?
# YES - Allow the user to specify their own From: address
# NO - Use the system generated From: address
FromLineOverride=YES
EOT
fi

#--------------------------------------------------------------------
# We are done
exit 0
