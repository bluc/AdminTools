#!/bin/bash
################################################################
# (c) Copyright 2017 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/os.postinstall.sh

# Get the official debian installer
# Adapt per https://sourceforge.net/projects/dolibarr/files/Dolibarr%20installer%20for%20Debian-Ubuntu%20%28DoliDeb%29/
wget -O /tmp/dolibarr.deb \
  https://sourceforge.net/projects/dolibarr/files/Dolibarr%20installer%20for%20Debian-Ubuntu%20%28DoliDeb%29/7.0.0/dolibarr_7.0.0-4_all.deb/download

# Only continue if we have a newer version
cmp /tmp/dolibarr.deb /usr/local/src/dolibarr.deb &> /dev/null
[ $? -eq 0 ] && exit 0
cat /tmp/dolibarr.deb > /usr/local/src/dolibarr.deb

# Install some prerequisites
dpkg-query -s software-properties-common &> /dev/null
[ $? -ne 0 ] && apt-get install software-properties-common
wget -O /tmp/mariadb_repo_setup https://downloads.mariadb.com/MariaDB/mariadb_repo_setup
bash /tmp/mariadb_repo_setup
#apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
#add-apt-repository 'deb [arch=amd64,i386,ppc64el] https://mirrors.evowise.com/mariadb/repo/10.2/ubuntu xenial main'
apt-get update
for P in mariadb-server mariadb-client apache2
do
    dpkg-query -s $P &> /dev/null
    [ $? -ne 0 ] && apt-get install $P
done

# Install the dolibarr software
dpkg -i /usr/local/src/dolibarr.deb
apt-get -f install

# Create a nice redirect for the main Apache page
cat << EOT > /var/www/html/index.html
<meta http-equiv="refresh" content="1; url=http://accounting.btoy1.net/" />
EOT

# Set some useful Apache variables
read -p 'Web server name : ' S_NAME
sed -i 's/#ServerName.*/ServerName '"$S_NAME"';/' /etc/apache2/sites-enabled/000-default.conf

# Enable SSL
a2enmod ssl
a2ensite default-ssl

cat << EOT > /etc/apache2/sites-enabled/default-ssl.conf
<IfModule mod_ssl.c>
	<VirtualHost _default_:443>
		ServerAdmin webmaster@localhost
		ServerName $S_NAME;

		DocumentRoot /var/www/html

		ErrorLog ${APACHE_LOG_DIR}/error.log
		CustomLog ${APACHE_LOG_DIR}/access.log combined

		SSLEngine on

		SSLCertificateFile	/etc/ssl/certs/ssl-cert-snakeoil.pem
		SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key

		<FilesMatch "\.(cgi|shtml|phtml|php)$">
				SSLOptions +StdEnvVars
		</FilesMatch>
		<Directory /usr/lib/cgi-bin>
				SSLOptions +StdEnvVars
		</Directory>

		SSLCipherSuite ALL:!aNULL:!ADH:!eNULL:!LOW:!EXP:RC4+RSA:+HIGH:+MEDIUM
		SSLProtocol -ALL +TLSv1.2
		ServerSignature Off
		TraceEnable off

	</VirtualHost>
</IfModule>
ServerTokens Prod
EOT
cat << EOT > /etc/apache2/sites-enabled/000-default.conf
<VirtualHost *:80>
        ServerName $S_NAME;

        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html
        Redirect / https://$S_NAME/

        ErrorLog \${APACHE_LOG_DIR}/error.log
        CustomLog \${APACHE_LOG_DIR}/access.log combined

</VirtualHost>
EOT
# Enable Apache caching
sed -i 's/#Expires/Expires/g' /etc/apache2/conf-enabled/dolibarr.conf
a2enmod expires

# Finally restart apache
service apache2 restart

# Some parting words
cat << EOT

Adjust the firewall rules in /etc/firehol/firehol.conf to allow
 incoming HTTP and HTTPS traffic.

EOT

# We are done
exit 0
