#!/bin/bash
################################################################
# (c) Copyright 2014 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            

#--------------------------------------------------------------------
# Specifying "-x" to the bash invocation = DEBUG
DEBUG=''
[[ $- = *x* ]] && DEBUG='-x'

#--------------------------------------------------------------------
# This host and domain
THISHOST=$(hostname)
[[ $THISHOST = *.* ]] || THISHOST=$(hostname -f)
THISDOMAIN=${THISHOST#*.}

# Function to convert netmasks into CIDR notation and back
# See: https://forums.gentoo.org/viewtopic-t-888736-start-0.html
function mask2cdr ()
{
   # Assumes there's no "255." after a non-255 byte in the mask
   local x=${1##*255.}
   set -- 0^^^128^192^224^240^248^252^254^ $(( (${#1} - ${#x})*2 )) ${x%%.*}
   x=${1%%$3*}
   echo $(( $2 + (${#x}/4) ))
}
function cdr2mask ()
{
   # Number of args to shift, 255..255, first non-255 byte, zeroes
   set -- $(( 5 - ($1 / 8) )) 255 255 255 255 $(( (255 << (8 - ($1 % 8))) & 255 )) 0 0 0
   [ $1 -gt 1 ] && shift $1 || shift
   echo ${1-0}.${2-0}.${3-0}.${4-0}
}

#--------------------------------------------------------------------
# Determine the Linux distribution
LINUX_DIST=''
INSTALL_PROG=''
if [ -s /etc/debian_version ]
then
    LINUX_DIST='DEBIAN'
    INSTALL_PROG='apt-get'
elif [ -s /etc/redhat-release ]
then
    LINUX_DIST='REDHAT'
    INSTALL_PROG='yum'

    # Install the necessary redhat packages
    $INSTALL_PROG list > /tmp/redhat.packages.list
    SRV_ARCH=$(uname -i)
    if [ -z "$(grep '^rpmforge-release.'$SRV_ARCH /tmp/redhat.packages.list)" ]
    then
        # Get "rpmforge" repository and install it
        curl -L 'http://pkgs.repoforge.org/rpmforge-release/rpmforge-release-0.5.3-1.el6.rf.x86_64.rpm' \
            > /tmp/rpmforge-release-0.5.3-1.el6.rf.x86_64.rpm	
        rpm --import http://apt.sw.be/RPM-GPG-KEY.dag.txt
        $INSTALL_PROG install /tmp/rpmforge-release-0.5.3-1.el6.rf.x86_64.rpm
        # Reget the list of install packages
        $INSTALL_PROG list > /tmp/redhat.packages.list
    fi
else
    echo "Unsupported Linux distribution"
    exit 1
fi

# Is this a virtual guest?
#IS_VIRTUAL=0
#if [ ! -z "$(grep -m1 VMware /proc/scsi/scsi)" ]
#then
#    IS_VIRTUAL=1
#elif [ ! -z "$(grep QEMU /proc/cpuinfo)" -a ! -z "$(grep Bochs /sys/class/dmi/id/bios_vendor)" ]
#then
#    IS_VIRTUAL=2
#elif [ ! -z "$(grep '^flags[[:space:]]*.*hypervisor' /proc/cpuinfo)" ]
#then
#    IS_VIRTUAL=3
#fi

#--------------------------------------------------------------------
if [ "T$LINUX_DIST" = 'TDEBIAN' ]
then
    # See https://www.howtoforge.com/how-to-install-owncloud-7-on-ubuntu-14.04

    # See https://mariadb.com/kb/en/library/mariadb-package-repository-setup-and-usage/
    wget -O /tmp/mariadb_repo_setup https://downloads.mariadb.com/MariaDB/mariadb_repo_setup
    bash /tmp/mariadb_repo_setup

    # Install the database server
    $INSTALL_PROG install mariadb-server

    # Configure the ownCloud database access (if necessary)
    HAVE_DB=0
    mysql -AB owncloud -e 'exit' &> /dev/null
    if [ $? -ne 0 ]
    then
        OC_PASSWD=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-16})
        mysql mysql << EOT
CREATE DATABASE IF NOT EXISTS owncloud;
GRANT ALL ON owncloud.* to 'owncloud'@'localhost' IDENTIFIED BY '$OC_PASSWD';
EOT
        HAVE_DB=1
    fi

    # Install the ownCloud packages
    wget -q http://download.opensuse.org/repositories/isv:ownCloud:community/xUbuntu_14.04/Release.key -O - | \
      apt-key add -
    source /etc/lsb-release
    echo "deb http://download.opensuse.org/repositories/isv:/ownCloud:/community/x${DISTRIB_ID}_${DISTRIB_RELEASE}/ /" > /etc/apt/sources.list.d/owncloud.list
    $INSTALL_PROG install owncloud
fi

# Update and upgrade
$INSTALL_PROG update
if [ "T$LINUX_DIST" = 'TDEBIAN' ]
then
    $INSTALL_PROG autoremove
    $INSTALL_PROG dist-upgrade
fi

# Change the default "index.html" file
if [ -z "$(grep B-LUC /var/www/html/index.html)" ]
then
    cat << EOT > /var/www/html/index.html
<!DOCTYPE html>
<html>
<head>
<title>Welcome</title>
 <meta name="copyright" content="B-LUC Consulting">
 <meta http-equiv="refresh" content="2;url=owncloud/">
</head>
<body>
 You will be redirected to the ownCloud server in two seconds. If
 you aren't forwarded to the it, please click <a href=owncloud/> here </a>.
</body>
</html>
EOT
fi

# Enhance the web server security
read -p 'Public name of server : ' SERVER_NAME

cat << EOT > /etc/apache2/conf-enabled/security2.conf
ServerTokens Prod
ServerSignature Off

# See http://hardenubuntu.com/hardening/apache/
<IfModule mod_headers.c>
 Header set X-XSS-Protection "1; mode=block"
 Header always append X-Frame-Options SAMEORIGIN
  Header unset Server  
  Header unset X-Powered-By  
</IfModule>
Header edit Set-Cookie ^(.*)\$ \$1;HttpOnly;Secure

TraceEnable off
FileETag None

RewriteEngine On
RewriteCond %{THE_REQUEST} !HTTP/1\.1$
RewriteRule .* - [F]

ServerTokens Prod
ServerSignature Off

ServerName $SERVER_NAME
EOT

# Enable/enforce https
# See http://ubuntuserverguide.com/2013/04/how-to-setup-owncloud-server-5-with-ssl-connection.html
a2enmod rewrite && a2enmod headers && a2enmod ssl
apt-get install ssl-cert
make-ssl-cert generate-default-snakeoil --force-overwrite

rm -f /etc/apache2/sites-enabled/000-default.conf
cat << EOT > /etc/apache2/sites-enabled/000-default.conf
<VirtualHost *:80>
	# The ServerName directive sets the request scheme, hostname and port that
	# the server uses to identify itself. This is used when creating
	# redirection URLs. In the context of virtual hosts, the ServerName
	# specifies what hostname must appear in the request's Host: header to
	# match this virtual host. For the default virtual host (this file) this
	# value is not decisive as it is used as a last resort host regardless.
	# However, you must set it for any further virtual host explicitly.
	ServerName $SERVER_NAME

	ServerAdmin webmaster@${SERVER_NAME#*.}
	DocumentRoot /var/www/html

	# Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
	# error, crit, alert, emerg.
	# It is also possible to configure the loglevel for particular
	# modules, e.g.
	#LogLevel info ssl:warn

	ErrorLog \${APACHE_LOG_DIR}/error.log
	CustomLog \${APACHE_LOG_DIR}/access.log combined

	# For most configuration files from conf-available/, which are
	# enabled or disabled at a global level, it is possible to
	# include a line for only one particular virtual host. For example the
	# following line enables the CGI configuration for this host only
	# after it has been globally disabled with "a2disconf".
	#Include conf-available/serve-cgi-bin.conf

	# Redirect HTTP to HTTPS
	# See http://blog.edwards-research.com/2010/02/force-apache2-to-redirect-from-http-to-https/
        RewriteEngine on
        ReWriteCond %{SERVER_PORT} !^443$
        RewriteRule ^/(.*) https://%{HTTP_HOST}/$1 [NC,R,L]
</VirtualHost>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
EOT
rm -f /etc/apache2/sites-enabled/default-ssl.conf
cat << EOT > /etc/apache2/sites-enabled/default-ssl.conf
<IfModule mod_ssl.c>
	<VirtualHost _default_:443>
		ServerAdmin webmaster@${SERVER_NAME#*.}
		ServerName $SERVER_NAME

		DocumentRoot /var/www/html

		# Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
		# error, crit, alert, emerg.
		# It is also possible to configure the loglevel for particular
		# modules, e.g.
		#LogLevel info ssl:warn

		ErrorLog \${APACHE_LOG_DIR}/error.log
		CustomLog \${APACHE_LOG_DIR}/access.log combined

		# For most configuration files from conf-available/, which are
		# enabled or disabled at a global level, it is possible to
		# include a line for only one particular virtual host. For example the
		# following line enables the CGI configuration for this host only
		# after it has been globally disabled with "a2disconf".
		#Include conf-available/serve-cgi-bin.conf

		#   SSL Engine Switch:
		#   Enable/Disable SSL for this virtual host.
		SSLEngine on

		#   A self-signed (snakeoil) certificate can be created by installing
		#   the ssl-cert package. See
		#   /usr/share/doc/apache2/README.Debian.gz for more info.
		#   If both key and certificate are stored in the same file, only the
		#   SSLCertificateFile directive is needed.
		SSLCertificateFile	/etc/ssl/certs/ssl-cert-snakeoil.pem
		SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key

		#   Server Certificate Chain:
		#   Point SSLCertificateChainFile at a file containing the
		#   concatenation of PEM encoded CA certificates which form the
		#   certificate chain for the server certificate. Alternatively
		#   the referenced file can be the same as SSLCertificateFile
		#   when the CA certificates are directly appended to the server
		#   certificate for convinience.
		#SSLCertificateChainFile /etc/apache2/ssl.crt/server-ca.crt

		#   Certificate Authority (CA):
		#   Set the CA certificate verification path where to find CA
		#   certificates for client authentication or alternatively one
		#   huge file containing all of them (file must be PEM encoded)
		#   Note: Inside SSLCACertificatePath you need hash symlinks
		#		 to point to the certificate files. Use the provided
		#		 Makefile to update the hash symlinks after changes.
		#SSLCACertificatePath /etc/ssl/certs/
		#SSLCACertificateFile /etc/apache2/ssl.crt/ca-bundle.crt

		#   Certificate Revocation Lists (CRL):
		#   Set the CA revocation path where to find CA CRLs for client
		#   authentication or alternatively one huge file containing all
		#   of them (file must be PEM encoded)
		#   Note: Inside SSLCARevocationPath you need hash symlinks
		#		 to point to the certificate files. Use the provided
		#		 Makefile to update the hash symlinks after changes.
		#SSLCARevocationPath /etc/apache2/ssl.crl/
		#SSLCARevocationFile /etc/apache2/ssl.crl/ca-bundle.crl

		#   Client Authentication (Type):
		#   Client certificate verification type and depth.  Types are
		#   none, optional, require and optional_no_ca.  Depth is a
		#   number which specifies how deeply to verify the certificate
		#   issuer chain before deciding the certificate is not valid.
		#SSLVerifyClient require
		#SSLVerifyDepth  10

		#   SSL Engine Options:
		#   Set various options for the SSL engine.
		#   o FakeBasicAuth:
		#	 Translate the client X.509 into a Basic Authorisation.  This means that
		#	 the standard Auth/DBMAuth methods can be used for access control.  The
		#	 user name is the 'one line' version of the client's X.509 certificate.
		#	 Note that no password is obtained from the user. Every entry in the user
		#	 file needs this password: 'xxj31ZMTZzkVA'.
		#   o ExportCertData:
		#	 This exports two additional environment variables: SSL_CLIENT_CERT and
		#	 SSL_SERVER_CERT. These contain the PEM-encoded certificates of the
		#	 server (always existing) and the client (only existing when client
		#	 authentication is used). This can be used to import the certificates
		#	 into CGI scripts.
		#   o StdEnvVars:
		#	 This exports the standard SSL/TLS related 'SSL_*' environment variables.
		#	 Per default this exportation is switched off for performance reasons,
		#	 because the extraction step is an expensive operation and is usually
		#	 useless for serving static content. So one usually enables the
		#	 exportation for CGI and SSI requests only.
		#   o OptRenegotiate:
		#	 This enables optimized SSL connection renegotiation handling when SSL
		#	 directives are used in per-directory context.
		#SSLOptions +FakeBasicAuth +ExportCertData +StrictRequire
		<FilesMatch "\.(cgi|shtml|phtml|php)\$">
				SSLOptions +StdEnvVars
		</FilesMatch>
		<Directory /usr/lib/cgi-bin>
				SSLOptions +StdEnvVars
		</Directory>

		#   SSL Protocol Adjustments:
		#   The safe and default but still SSL/TLS standard compliant shutdown
		#   approach is that mod_ssl sends the close notify alert but doesn't wait for
		#   the close notify alert from client. When you need a different shutdown
		#   approach you can use one of the following variables:
		#   o ssl-unclean-shutdown:
		#	 This forces an unclean shutdown when the connection is closed, i.e. no
		#	 SSL close notify alert is send or allowed to received.  This violates
		#	 the SSL/TLS standard but is needed for some brain-dead browsers. Use
		#	 this when you receive I/O errors because of the standard approach where
		#	 mod_ssl sends the close notify alert.
		#   o ssl-accurate-shutdown:
		#	 This forces an accurate shutdown when the connection is closed, i.e. a
		#	 SSL close notify alert is send and mod_ssl waits for the close notify
		#	 alert of the client. This is 100% SSL/TLS standard compliant, but in
		#	 practice often causes hanging connections with brain-dead browsers. Use
		#	 this only for browsers where you know that their SSL implementation
		#	 works correctly.
		#   Notice: Most problems of broken clients are also related to the HTTP
		#   keep-alive facility, so you usually additionally want to disable
		#   keep-alive for those clients, too. Use variable "nokeepalive" for this.
		#   Similarly, one has to force some clients to use HTTP/1.0 to workaround
		#   their broken HTTP/1.1 implementation. Use variables "downgrade-1.0" and
		#   "force-response-1.0" for this.
		BrowserMatch "MSIE [2-6]" \\
				nokeepalive ssl-unclean-shutdown \\
				downgrade-1.0 force-response-1.0
		# MSIE 7 and newer should be able to use keepalive
		BrowserMatch "MSIE [17-9]" ssl-unclean-shutdown

		# PCI compliant setup
		# See http://www.acunetix.com/blog/articles/tls-ssl-cipher-hardening/
		SSLProtocol ALL -SSLv2 -SSLv3
		SSLHonorCipherOrder On
		SSLCipherSuite ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:ECDH+3DES:DH+3DES:RSA+AESGCM:RSA+AES:RSA+3DES:!aNULL:!MD5
		SSLCompression Off
	</VirtualHost>
</IfModule>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
EOT

service apache2 restart

# Give instructions for the next step
if [ $HAVE_DB -eq 0 ]
then
    LOCALIF=$(ip route get 8.8.8.8 | awk '/via/{print $5; exit}')
    LOCALIP=$(ifconfig $LOCALIF | sed -rn 's/.*r:([^ ]+) .*/\1/p')
    cat << EOT
Once this installation step is done, point your browser to
http://${LOCALIP}/owncloud

Select "MySQL/MariaDB" under "Storage & database" and use these credentials:
username: owncloud
password: $OC_PASSWD

Create a new administrative account

In the lower tab below "MySQL/MariaDB" input:
username=owncloud
password=$OC_PASSWD
databasename=owncloud
EOT
fi
