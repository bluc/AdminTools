#!/bin/bash

# Based on https://wiki.zoneminder.com/Ubuntu_Server_18.04_64-bit_with_Zoneminder_1.32.x_the_easy_way
# Use the official "nginx" repository
if [ ! -s /etc/apt/sources.list.d/nginx.list ]
then
    add-apt-repository ppa:nginx/stable 
    apt-get update
fi

# Install the necessary packages
for PKG in nginx libnginx-mod-http-headers-more-filter mariadb-server php-fpm php-mysql fcgiwrap
do
   dpkg-query -S $PKG 2> /dev/null
   [ $? -ne 0 ] && apt install $PKG
done

# Adapt mariadb
mysql_secure_installation
systemctl restart mysql

# Adapt PHP config
sed -i -e 's@.*cgi.fix_pathinfo.*@cgi.fix_pathinfo=0@' /etc/php/7.2/fpm/php.ini
[ -z "$(grep ^date.timezone /etc/php/7.2/apache2/php.ini)" ] && awk '$0="date.timezone = "$0' /etc/timezone >> /etc/php/7.2/fpm/php.ini
systemctl restart php7.2-fpm

# Finally install zoneminder
dpkg-query -S zoneminder &> /dev/null
if [ $? -ne 0 ]
then
    add-apt-repository ppa:iconnor/zoneminder-1.32
    apt update
    apt install zoneminder
fi

# Adapt for ZondMinder
adduser www-data video
chown -R www-data:www-data /usr/share/zoneminder/
sed -i -e 's@ZM_PATH_ZMS.*@ZM_PATH_ZMS=/cgi-bin/nph-zms@' /etc/zm/conf.d/01-system-paths.conf

systemctl enable zoneminder
service zoneminder start

# Adapt nginx configuration
sed -i -e 's@index .*@index index.php index.html index.htm index.nginx-debian.html;@' /etc/nginx/sites-available/default
if [ -z "$(grep 'zoneminder.conf' /etc/nginx/sites-available/default)" ]
then
   sed -i -e 's@^}@ include /etc/nginx/zoneminder.conf;\n}@' /etc/nginx/sites-enabled/default
fi
cat << EOT > /etc/nginx/zoneminder.conf
location /cgi-bin {
auth_basic off;
        alias /usr/lib/zoneminder/cgi-bin;     
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME \$request_filename;
        fastcgi_param HTTP_PROXY "";
        fastcgi_pass unix:/var/run/fcgiwrap.socket;
}
location /zm/cache {
auth_basic off;
        alias /var/cache/zoneminder/cache;
}
location ~ /zm/api/(css|img|ico) {
auth_basic off;
        rewrite ^/zm/api(.+)\$ /api/app/webroot/\$1 break;
        try_files \$uri \$uri/ =404;
}
location /zm {
auth_basic off;
        alias /usr/share/zoneminder/www;
        try_files \$uri \$uri/ /index.php?\$args =404;    
        location /zm/api {
auth_basic off;
                rewrite ^/zm/api(.+)\$ /zm/api/app/webroot/index.php?p=\$1 last;
         }
                location ~ \.php\$ {
                auth_basic off;
                        include fastcgi_params;
                        fastcgi_param SCRIPT_FILENAME \$request_filename;
                        fastcgi_param HTTP_PROXY "";
                fastcgi_index index.php;
                        fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
        }
    }
EOT
service nginx restart

# Adapt fcgiwrap's config
echo 'DAEMON_OPTS=-c 10' > /etc/default/fcgiwrap
systemctl restart fcgiwrap

# We are done
exit 0
