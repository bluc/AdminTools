#!/bin/bash
################################################################
# (c) Copyright 2017 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            

#--------------------------------------------------------------------
# Specifying "-x" to the bash invocation = DEBUG
DEBUG=''
[[ $- = *x* ]] && DEBUG='-x'

#--------------------------------------------------------------------
# This host and domain
THISHOST=$(hostname)
[[ $THISHOST = *.* ]] || THISHOST=$(hostname -f)
THISDOMAIN=${THISHOST#*.}

# Function to convert netmasks into CIDR notation and back
# See: https://forums.gentoo.org/viewtopic-t-888736-start-0.html
mask2cdr ()
{
   # Assumes there's no "255." after a non-255 byte in the mask
   local x=${1##*255.}
   set -- 0^^^128^192^224^240^248^252^254^ $(( (${#1} - ${#x})*2 )) ${x%%.*}
   x=${1%%$3*}
   echo $(( $2 + (${#x}/4) ))
}
function cdr2mask ()
{
   # Number of args to shift, 255..255, first non-255 byte, zeroes
   set -- $(( 5 - ($1 / 8) )) 255 255 255 255 $(( (255 << (8 - ($1 % 8))) & 255 )) 0 0 0
   [ $1 -gt 1 ] && shift $1 || shift
   echo ${1-0}.${2-0}.${3-0}.${4-0}
}

LOCALIF=$(ip route get 8.8.8.8 | awk '/via/{print $5; exit}')
LOCALIP=$(ifconfig $LOCALIF | sed -rn 's/.*r:([^ ]+) .*/\1/p')
LOCALMASK=$(ifconfig $LOCALIF | sed -n -e 's/.*Mask:\(.*\)$/\1/p')
# From: http://www.routertech.org/viewtopic.php?t=1609
l="${LOCALIP%.*}";r="${LOCALIP#*.}";n="${LOCALMASK%.*}";m="${LOCALMASK#*.}"
LOCALNET=$((${LOCALIP%%.*}&${LOCALMASK%%.*})).$((${r%%.*}&${m%%.*})).$((${l##*.}&${n##*.})).$((${LOCALIP##*.}&${LOCALMASK##*.}))
CIDRMASK=$(mask2cdr $LOCALMASK)

# As per: http://www.howtoforge.com/installing-nginx-with-php5-and-php-fpm-and-mysql-support-lemp-on-ubuntu-12.04-lts
RESTART_NGINX=0
RESTART_FPM=0

# Use the official "nginx" repository
[ -s /etc/apt/sources.list.d/nginx.list ] || add-apt-repository ppa:nginx/stable 

# Install nginx itself
apt-get update
apt-get install nginx libnginx-mod-http-headers-more-filter

# Adapt nginx globals
CPUS=$(grep -c '^processor' /proc/cpuinfo)
if [ -z "$(grep '^[[:space:]]*worker_processes.*'$CPUS';' /etc/nginx/nginx.conf)" ]
then
    sed -i 's/worker_processes.*;/worker_processes '"$CPUS;/" /etc/nginx/nginx.conf 
    RESTART_NGINX=1
fi
if [ -z "$(grep '^[[:space:]]*server_names_hash_bucket_size' /etc/nginx/nginx.conf)" ]
then
    sed -i 's/# server_names_hash_bucket_size.*/server_names_hash_bucket_size 128; # Make this at least 3 times the number of vhosts/' /etc/nginx/nginx.conf 
    RESTART_NGINX=1
fi
if [ -z "$(grep '^[[:space:]]*keepalive_timeout.*10;' /etc/nginx/nginx.conf)" ]
then
    sed -i 's/keepalive_timeout.*;/keepalive_timeout 10;/' /etc/nginx/nginx.conf 
    RESTART_NGINX=1
fi
if [ -z "$(grep 'modules-enabled' /etc/nginx/nginx.conf)" ]
then
    mkdir -p /etc/nginx/modules-enabled
    sed -i 's@events {@include /etc/nginx/modules-enabled/*.conf;\n\nevents {@' /etc/nginx/nginx.conf 
    RESTART_NGINX=1
fi

# Install php-fpm etc.
apt-get install php-fpm #php5-suhosin

# Create some possibly missing conf files
if [ ! -s /etc/nginx/nginx.conf ]
then
    cat << EOT > /etc/nginx/nginx.conf
user       www-data www-data;  ## Default: nobody
worker_processes 2;  ## Default: 1
error_log  /var/log/nginx/error.log;
pid        /var/run/nginx.pid;
worker_rlimit_nofile 8192;
 
events {
  worker_connections  4096;  ## Default: 1024
  use epoll;
}
 
http {
  include    /etc/nginx/mime.types;
  include    /etc/nginx/proxy.conf;
  include    /etc/nginx/fastcgi_params;
  index    index.html index.htm index.php;
 
  sendfile     on;
  tcp_nopush   on;
  tcp_nodelay off;
  server_names_hash_bucket_size 64; # this seems to be required for some vhosts
  types_hash_max_size 8192;

  default_type text/html;
  log_format main '\$remote_addr - \$remote_user [\$time_local]  '
                    '"\$request" \$status \$body_bytes_sent '
                    '"\$http_referer" "\$http_user_agent"';
  access_log /var/log/nginx/access.log main;
  include /etc/nginx/conf.d/*.conf;
  include /etc/nginx/sites-enabled/*.site;
}
EOT
fi

if [ ! -s /etc/nginx/fastcgi_params -o -z "$(grep SCRIPT_FILENAME /etc/nginx/fastcgi_params)" ]
then
    cat << EOT > /etc/nginx/fastcgi_params
fastcgi_param   QUERY_STRING            \$query_string;
fastcgi_param   REQUEST_METHOD          \$request_method;
fastcgi_param   CONTENT_TYPE            \$content_type;
fastcgi_param   CONTENT_LENGTH          \$content_length;
 
fastcgi_param   SCRIPT_FILENAME         \$document_root\$fastcgi_script_name;
fastcgi_param   SCRIPT_NAME             \$fastcgi_script_name;
fastcgi_param   PATH_INFO               \$fastcgi_path_info;
fastcgi_param 	PATH_TRANSLATED		\$document_root\$fastcgi_path_info;
fastcgi_param   REQUEST_URI             \$request_uri;
fastcgi_param   DOCUMENT_URI            \$document_uri;
fastcgi_param   DOCUMENT_ROOT           \$document_root;
fastcgi_param   SERVER_PROTOCOL         \$server_protocol;
 
fastcgi_param   GATEWAY_INTERFACE       CGI/1.1;
fastcgi_param   SERVER_SOFTWARE         nginx/\$nginx_version;
 
fastcgi_param   REMOTE_ADDR             \$remote_addr;
fastcgi_param   REMOTE_PORT             \$remote_port;
fastcgi_param   SERVER_ADDR             \$server_addr;
fastcgi_param   SERVER_PORT             \$server_port;
fastcgi_param   SERVER_NAME             \$server_name;
 
fastcgi_param   HTTPS                   \$https;
 
# PHP only, required if PHP was built with --enable-force-cgi-redirect
fastcgi_param   REDIRECT_STATUS         200;
EOT
fi

if [ ! -s /etc/nginx/mime.types ]
then
    cat << EOT > /etc/nginx/mime.conf
types {
text/html html htm shtml;
text/css css;
text/xml xml;
image/gif gif;
image/jpeg jpeg jpg;
application/x-javascript js;
application/atom+xml atom;
application/rss+xml rss;

text/mathml mml;
text/plain txt;
text/vnd.sun.j2me.app-descriptor jad;
text/vnd.wap.wml wml;
text/x-component htc;

image/png png;
image/tiff tif tiff;
image/vnd.wap.wbmp wbmp;
image/x-icon ico;
image/x-jng jng;
image/x-ms-bmp bmp;
image/svg+xml svg;

application/java-archive jar war ear;
application/mac-binhex40 hqx;
application/msword doc;
application/pdf pdf;
application/postscript ps eps ai;
application/rtf rtf;
application/vnd.ms-excel xls;
application/vnd.ms-powerpoint ppt;
application/vnd.wap.wmlc wmlc;
application/vnd.wap.xhtml+xml xhtml;
application/vnd.google-earth.kml+xml kml;
application/vnd.google-earth.kmz kmz;
application/x-7z-compressed 7z;
application/x-cocoa cco;
application/x-java-archive-diff jardiff;
application/x-java-jnlp-file jnlp;
application/x-makeself run;
application/x-perl pl pm;
application/x-pilot prc pdb;
application/x-rar-compressed rar;
application/x-redhat-package-manager rpm;
application/x-sea sea;
application/x-shockwave-flash swf;
application/x-stuffit sit;
application/x-tcl tcl tk;
application/x-x509-ca-cert der pem crt;
application/x-xpinstall xpi;
application/zip zip;

application/octet-stream bin exe dll;
application/octet-stream deb;
application/octet-stream dmg;
application/octet-stream eot;
application/octet-stream iso img;
application/octet-stream msi msp msm;

audio/midi mid midi kar;
audio/mpeg mp3 m4a;
audio/x-realaudio ra;
audio/ogg oga;

video/3gpp 3gpp 3gp;
video/mpeg mpeg mpg;
video/quicktime mov;
video/x-flv flv;
video/x-mng mng;
video/x-ms-asf asx asf;
video/x-ms-wmv wmv;
video/x-msvideo avi;

application/ogg ogg ogx;

video/ogg ogv;
video/mp4 mp4 m4v;
video/webm webm;
}
EOT
fi

if [ ! -s /etc/nginx/proxy.conf ]
then
    read -p 'IP address of upstream server ? ' UPSTREAM_SERVER
    cat << EOT > /etc/nginx/proxy.conf
proxy_redirect          off;
proxy_set_header        Host            \$host;
proxy_set_header        X-Real-IP       \$remote_addr;
proxy_set_header        X-Forwarded-For \$proxy_add_x_forwarded_for;
client_body_buffer_size 128k;
proxy_connect_timeout   90;
proxy_send_timeout      90;
proxy_read_timeout      90;
proxy_buffers           32 4k;
proxy_buffer_size	2k;
proxy_http_version	1.1;

# See https://www.nginx.com/blog/nginx-caching-guide/
proxy_cache my_cache;
proxy_cache_revalidate on;
proxy_cache_min_uses 3;
proxy_cache_use_stale error timeout updating http_500 http_502 http_503 http_504;
proxy_cache_lock on;
add_header X-Cache-Status \$upstream_cache_status;

# Correctly handle browser "clear cache" requests
# See https://www.digitalocean.com/community/tutorials/understanding-nginx-http-proxying-load-balancing-buffering-and-caching
proxy_cache_bypass $http_cache_control;

# Proxy everything to an upstream server
# proxy_pass http://$UPSTREAM_SERVER:80;

#Example for excluded pages# No caching for admin pages
#Example for excluded pageslocation ~ /admin
#Example for excluded pages{
#Example for excluded pages    more_clear_headers 'Expires';
#Example for excluded pages
#Example for excluded pages    # I don't why - but this needs to be repeated in here as well
#Example for excluded pages    proxy_redirect          off;
#Example for excluded pages    proxy_set_header        Host            \$host;
#Example for excluded pages    proxy_set_header        X-Real-IP       \$remote_addr;
#Example for excluded pages    proxy_set_header        X-Forwarded-For \$proxy_add_x_forwarded_for;
#Example for excluded pages    client_body_buffer_size 128k;
#Example for excluded pages    proxy_connect_timeout   90;
#Example for excluded pages    proxy_send_timeout      90;
#Example for excluded pages    proxy_read_timeout      90;
#Example for excluded pages    proxy_buffers           32 4k;
#Example for excluded pages    proxy_buffer_size   2k;
#Example for excluded pages    proxy_http_version  1.1;
#Example for excluded pages    proxy_buffering off;
#Example for excluded pages
#Example for excluded pages    # See https://www.nginx.com/blog/nginx-caching-guide/
#Example for excluded pages    proxy_cache off;
#Example for excluded pages    add_header X-Cache-Status DISABLED_ON_PURPOSE;
#Example for excluded pages
#Example for excluded pages    # Correctly handle browser "clear cache" requests
#Example for excluded pages    # See https://www.digitalocean.com/community/tutorials/understanding-nginx-http-proxying-load-balancing-buffering-and-caching
#Example for excluded pages    proxy_cache_bypass \$http_cache_control;
#Example for excluded pages
#Example for excluded pages    # Proxy everything to the CMS server
#Example for excluded pages    proxy_pass http://$UPSTREAM_SERVER:80;
#Example for excluded pages
#Example for excluded pages    location ~ /Archive
#Example for excluded pages    {
#Example for excluded pages        client_max_body_size 1024m;
#Example for excluded pages        more_clear_headers 'Expires';
#Example for excluded pages
#Example for excluded pages        # I don't why - but this needs to be repeated in here as well
#Example for excluded pages        proxy_redirect          off;
#Example for excluded pages        proxy_set_header        Host            $host;
#Example for excluded pages        proxy_set_header        X-Real-IP       $remote_addr;
#Example for excluded pages        proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
#Example for excluded pages        client_body_buffer_size 128k;
#Example for excluded pages        proxy_connect_timeout   90;
#Example for excluded pages        proxy_send_timeout      90;
#Example for excluded pages        proxy_read_timeout      90;
#Example for excluded pages        proxy_buffers           32 4k;
#Example for excluded pages        proxy_buffer_size       2k;
#Example for excluded pages        proxy_http_version      1.1;
#Example for excluded pages        proxy_buffering off;
#Example for excluded pages
#Example for excluded pages        # See https://www.nginx.com/blog/nginx-caching-guide/
#Example for excluded pages        proxy_cache off;
#Example for excluded pages        add_header X-Cache-Status DISABLED_ON_PURPOSE;
#Example for excluded pages
#Example for excluded pages        # Correctly handle browser "clear cache" requests
#Example for excluded pages        # See https://www.digitalocean.com/community/tutorials/understanding-nginx-http-proxying-load-balancing-buffering-and-cachin
#Example for excluded pages        proxy_cache_bypass $http_cache_control;
#Example for excluded pages
#Example for excluded pages        # Proxy everything to the CMS server
#Example for excluded pages        proxy_pass http://$UPSTREAM_SERVER:80;
#Example for excluded pages    }
#Example for excluded pages}
EOT
fi

if [ ! -s /etc/nginx/conf.d/proxy-system.conf ]
then
    cat << EOT > /etc/nginx/conf.d/proxy-system.conf
# See: https://www.nginx.com/blog/nginx-caching-guide/
proxy_cache_path /var/cache/nginx levels=1:2 keys_zone=my_cache:10m max_size=10g inactive=60m use_temp_path=off;

# Enable caching for certain HTTP codes
# See: https://www.digitalocean.com/community/tutorials/understanding-nginx-http-proxying-load-balancing-buffering-and-caching
proxy_cache_valid 200 302 10m;
proxy_cache_valid 404 1m;
# See: http://www.web-cache.com/Writings/http-status-codes.html
proxy_cache_valid 303 307 10m;

# See: https://www.nginx.com/resources/admin-guide/logging-and-monitoring/
log_format upstream_time '\$remote_addr - \$remote_user [\$time_local] '
                         '"\$request" \$status \$body_bytes_sent '
                         '"\$host" "\$http_referer" "\$http_user_agent" "\$http_x_forwarded_for" '
                         'country=\$geoip_country_code '
                         'rt=\$request_time uct=\$upstream_connect_time uht=\$upstream_header_time urt=\$upstream_response_time '
                         'reqlen=\$request_length ucs=\$upstream_cache_status';
EOT
fi

# Adjust the default site
read -p "Web server name [default=$THISHOST] ?" WS_NAME
[ -z "$WS_NAME" ] && WS_NAME=$THISHOST
mkdir -p /var/www/${WS_NAME}/web

if [ -z "$(grep '^[[:space:]]*listen.*443' /etc/nginx/sites-enabled/default)" ]
then
    FPM_SOCK=$(awk '/^listen /{print $NF}' /etc/php/*/fpm/pool.d/www.conf)
    cat << EOT > /etc/nginx/sites-enabled/default
# You may add here your
# server {
#	...
# }
# statements for each of your virtual hosts to this file

##
# You should look at the following URL's in order to grasp a solid understanding
# of Nginx configuration files in order to fully unleash the power of Nginx.
# http://wiki.nginx.org/Pitfalls
# http://wiki.nginx.org/QuickStart
# http://wiki.nginx.org/Configuration
#
# Generally, you will want to move this file somewhere, and start with a clean
# file but keep this around for reference. Or just disable in sites-enabled.
#
# Please see /usr/share/doc/nginx-doc/examples/ for more detailed examples.
##

server {
	listen   80; ## listen for ipv4; this line is default and implied
	#listen   [::]:80 default ipv6only=on; ## listen for ipv6

	root /var/www/${WS_NAME}/web;
	if (\$http_host = "webmail.btoy1.net") {
		rewrite ^ https://webmail.btoy1.net\$request_uri permanent;
	}
	index index.php index.html index.htm;

	# Make site accessible from http://localhost/
	server_name _ ${WS_NAME};

	location / {
		# First attempt to serve request as file, then
		# as directory, then fall back to index.php and
		# index.html
		try_files \$uri \$uri/ /index.php /index.html;
		# Uncomment to enable naxsi on this location
		# include /etc/nginx/naxsi.rules
	}

	location /doc/ {
		alias /usr/share/doc/;
		autoindex on;
		allow 127.0.0.1;
		deny all;
	}

	# Only for nginx-naxsi : process denied requests
	#location /RequestDenied {
		# For example, return an error code
		#return 418;
	#}

	#error_page 404 /404.html;

	# redirect server error pages to the static page /50x.html
	#
	#error_page 500 502 503 504 /50x.html;
	#location = /50x.html {
	#	root /var/www/${WS_NAME}/web;
	#}

	# deny access to .htaccess files, if Apache's document root
	# concurs with nginx's one
	#
	#location ~ /\.ht {
	#	deny all;
	#}

	# pass the PHP scripts to FastCGI server listening on local socket
	location ~ \.php\$ {
		try_files \$uri =404;
		fastcgi_split_path_info ^(.+\.php)(/.+)\$;
		# fastcgi_pass 127.0.0.1:9000;
		fastcgi_pass unix:${FPM_SOCK};
		fastcgi_index index.php;
		include fastcgi_params;
	}

	location ~ /favicon.(ico|gif)\$ {
#		root /var/www/${WS_NAME}/web/images;
		log_not_found off;
		access_log off;
		expires max;
	}

	location = /robots.txt {
		allow all;
		log_not_found off;
		access_log off;
	}

	# Deny all attempts to access certain files
	location ~ ^/(README|INSTALL|LICENSE|CHANGELOG|UPGRADING)\$ {
		deny all;
	}
	location ~ ^/(bin|SQL)/ {
		deny all;
	}
	location ~ \.inc\$ {
		deny all;
	}

	# Deny all attempts to access hidden files such as .htaccess, .htpasswd, .DS_Store (Mac).
	location ~ /\. {
		deny all;
		access_log off;
		log_not_found off;
	}

	# See: http://fralef.org/nginx-hardening-some-good-security-practices.html
	## Deny access based on HTTP method
	if (\$bad_method = 1) {
		return 444;
	}

	## Deny access based on the User-Agent header.
	if (\$bad_bot = 1) {
		return 403;
	}

	## Deny access based on the Referer header.
	if (\$bad_referer = 1) {
		return 403;
	}

	## Enable internal status page
	location /nginx_status {
	        # Turn on nginx stats
        	stub_status;
	        # I do not need logs for stats
        	access_log   off;
	        # Security: Only allow access from local host
        	allow $LOCALIP;
	        # Send rest of the world to /dev/null
        	deny all;
	}

	## Allow access to the status graphs
	location /status {
		alias /var/www/html/rrd/;
		autoindex on;
		# Security: Only allow from local network
		allow ${LOCALNET}/${CIDRMASK};
		deny all;
	}

	# See: http://projectdaenney.org/blog/2012/11/11/tougher-ssl-security-in-nginx/
	# Prevent loading framed content except if it’s the same origin (aka same machine)
	add_header X-Frame-Options SAMEORIGIN;

	# See https://www.owasp.org/index.php/List_of_useful_HTTP_headers
	add_header X-XSS-Protection '1; mode=block';
	add_header X-Content-Type-Options nosniff;
}


# another virtual host using mix of IP-, name-, and port-based configuration
#
#server {
#	listen 8000;
#	listen somename:8080;
#	server_name somename alias another.alias;
#	root /var/www/${WS_NAME}/web;
#	index index.php index.html index.htm;
#
#	location / {
#		try_files \$uri \$uri/ /index.html;
#	}
#}


# HTTPS server
server {
	listen 443;
	server_name _;

	root /var/www/${WS_NAME}/web;
	index index.php index.html index.htm;

	ssl on;
	ssl_certificate /etc/ssl/certs/webmail.btoy1.net.pem;
	ssl_certificate_key /etc/ssl/private/webmail.btoy1.net.key;

	ssl_session_cache    shared:SSL:10m;
        ssl_session_timeout  10m;

	# As per http://www.acunetix.com/blog/articles/tls-ssl-cipher-hardening/
	ssl_prefer_server_ciphers On;
	ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
	ssl_ciphers ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:ECDH+3DES:DH+3DES:RSA+AESGCM:RSA+AES:RSA+3DES:!aNULL:!MD5;

	location / {
		try_files \$uri \$uri/ /index.html;
	}

	# pass the PHP scripts to FastCGI server listening on local socket
	location ~ \.php\$ {
		try_files \$uri =404;
		fastcgi_split_path_info ^(.+\.php)(/.+)\$;
		# fastcgi_pass 127.0.0.1:9000;
		fastcgi_pass unix:${FPM_SOCK};
		fastcgi_index index.php;
		include fastcgi_params;
	}

	location ~ ^/favicon.ico\$ {
#		root /var/www/${WS_NAME}/web/images;
		log_not_found off;
		access_log off;
		expires max;
	}

	location = /robots.txt {
		allow all;
		log_not_found off;
		access_log off;
	}

	# Deny all attempts to access certain files
	location ~ ^/(README|INSTALL|LICENSE|CHANGELOG|UPGRADING)\$ {
		deny all;
	}
	location ~ ^/(bin|SQL)/ {
		deny all;
	}
	location ~ \.inc\$ {
		deny all;
	}

	# Deny all attempts to access hidden files such as .htaccess, .htpasswd, .DS_Store (Mac).
	location ~ /\. {
		deny all;
		access_log off;
		log_not_found off;
	}

	# See: http://fralef.org/nginx-hardening-some-good-security-practices.html
	## Deny access based on HTTP method
	if (\$bad_method = 1) {
		return 444;
	}

	## Deny access based on the User-Agent header.
	if (\$bad_bot = 1) {
		return 403;
	}

	## Deny access based on the Referer header.
	if (\$bad_referer = 1) {
		return 403;
	}

	# See: http://projectdaenney.org/blog/2012/11/11/tougher-ssl-security-in-nginx/
	# Prevent loading framed content except if it’s the same origin (aka same machine)
	add_header X-Frame-Options SAMEORIGIN;

	# See https://www.owasp.org/index.php/List_of_useful_HTTP_headers
	add_header X-XSS-Protection '1; mode=block';
	add_header X-Content-Type-Options nosniff;
	# Only for HTTPS sites:
	#add_header Strict-Transport-Security 'max-age=16070400; includeSubDomains';
}
EOT
    RESTART_NGINX=1
fi
if [ ! -s /etc/ssl/private/webmail.btoy1.net.key ]
then
	cat << EOT > /etc/ssl/private/webmail.btoy1.net.key
-----BEGIN RSA PRIVATE KEY-----
MIIEpQIBAAKCAQEAxAvXoxkfOSM6Ezsg4HA7j/jWzk5o6aX6X1H397PG4pCgjYY6
1R/kTLcl28sibyp+m+O+QqqTPoHP826WAAHOHXZOs+Af6byb36cQhPtVRIHLJ1d+
uxUZOx0WlVEX99KGcc9FtCuqtabbKB14kVHtejjW6L4GBW+Mmk7rIKUj1EpBOHC2
j++FleiPTXTPWjP9+8VwW68pAJwfk/hRfpxmp69IHOrCLNOIT2STLorL0hKZSwaq
f7sEHEDtJJ0HZk+JVjWMOE4Nu/M891D29It8AoDagCzByQzsVg08WuChbjX6SpHf
UvGiykgIMm5p19MnpjdgFXA7VG1G6S99KMkhHwIDAQABAoIBAEdgnGlIe0K9Xaak
z4qHslfv3J2OlMoE4UyVNngcH1xcFeiRJ5hzHRcFU6ZbpjTdPu2ZFKuHGUcX0lOq
PrjP73Bwo6UAA/6A6iua0vvn2hqwOvwe8ghBXUdVhuRkexqCrup+8bzwbR3oG0DW
wg/+yTzHtw9UKDMQcxu3ggoHtizTHHbCn2tQtiQq5iZe2/pSID2gODRvU7CY1tC0
XBfQgOI9Xp7nKbcEwaZUmEsB7DPnA7BcB1OEU4Qlc7mRFjhD873tS92j8PvLgVYH
EPF6auUcA+j7+4XLV693z2W/0LbXdT6hw1jDXYQLhRF4F3EGAkUddJLb7MvLt4PZ
bgsOFkECgYEA9OAxYBb2FraDYF4512oaAs1JmtWZ/gRw0bOK6zlm1aumtAIp+yfG
btAGFYm112oH6ux8gUQcdycdPfEAYY4Lib21Iw7N8bd7l1fyS62KP274LdL4DndN
KN7BarlcPy2VH0SKxKZJPEjaEIRPMb2osgvA/QkBRwW6ZJdWrcuOFWECgYEAzPPI
LoemYyL5vd8YGJ2rxNUFovvsE0O0AZbLLqD/nUsPJWQbsolXzs17n6BSzSQPytxp
vfMg571Zlle/oEMUaoUweQJdIzgkjQxAivpSe1sJdn8CP4lWIiDWkLG3IEYkHx5J
FimmxwG8Jac4u7NTT8A1lpjbfZigHbVTMdP0Rn8CgYEApUoTagX1tF0cDtdreaFP
z2ZccZwO5ux+br4fXimP5ViJHOZ9Cd/OTIw0HyOmT7Jth8B7RhIAmNkZHu9nT3I/
DYm1E+XxU2CDlyOxFC4erDSuZtgJwlH7DkRWEEDwKGEPUFzGwnhv5LZUI2P7xNp9
XTkb2nDSoOsX5sBUrNlCneECgYEAp+GtAUhGaAB3WCuNfmRxFlt0MYxwVVto8MBH
kNt6ZqrcLkGxWm6cSZ8R+7CFAtreQMqJp3mnY4w7/SXFpw4rhBGzofvotPfBIPR/
Thg8RiVNPA5PliH2NyB1AbNBESPDZOYW8huOLIdkSeePVllPDg2hF8oASQIjbhJg
onk/KBsCgYEAyZgWByNxjJVF0ijmnWfiDe3OP9Gj0YCJVB3w3vUrtpYP0oRSm0sW
OibHsroXQv5ev6UeR6hvVaW6RUJnFCWDL7lVzWpeQfCLSOYwT/G2+Pfy+uMch7U8
M15oWSs9sv/2beh1ng414ePwqJsm31jVl2dnXKX2UgLxLAFVzhSbiR0=
-----END RSA PRIVATE KEY-----
EOT
    chmod 400 /etc/ssl/private/webmail.btoy1.net.key
    read -p 'Get the cert from CACert.org and install it as "/etc/ssl/certs/webmail.btoy1.net.pem"' YN
    RESTART_NGINX=1
fi

apt-get install php-geoip
nginx -V 2> /tmp/$$
if [ ! -z "$(grep with-http_geoip_module /tmp/$$)" ]
then
    # GEOip module is compiled in
    if [ ! -s /etc/nginx/conf.d/geoip.conf ]
    then
        cat << EOT > /etc/nginx/conf.d/geoip.conf
# See https://nginx.org/en/docs/http/ngx_http_geoip_module.html

# Where the databases are
geoip_country         /usr/share/GeoIP/GeoIP.dat;  # the country IP database
geoip_city            /usr/share/GeoIP/GeoLiteCity.dat; # the city IP database

# Define trusted proxies (if any)
# geoip_proxy           192.168.100.0/24;
# geoip_proxy           2001:0db8::/32;
geoip_proxy_recursive on;
EOT
        # Update the geo database for nginx
        wget -q http://geolite.maxmind.com/download/geoip/database/GeoLiteCountry/GeoIP.dat.gz \
           -O /tmp/GeoIP.dat.gz
        [ $? -eq 0 ] && gunzip -c /tmp/GeoIP.dat.gz > /usr/share/GeoIP/GeoIP.dat
        wget -q http://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz \
          -O /tmp/GeoLiteCity.dat.gz
        [ $? -eq 0 ] && gunzip -c /tmp/GeoLiteCity.dat.gz > /usr/share/GeoIP/GeoLiteCity.dat
        RESTART_NGINX=1
    fi
    if [ -z "$(grep 'GEOIP_' /etc/nginx/fastcgi_params)" ]
    then
        cat << EOT >> /etc/nginx/fastcgi_params

# GEOIP parameters
fastcgi_param GEOIP_CITY_COUNTRY_CODE \$geoip_city_country_code;
fastcgi_param GEOIP_CITY_COUNTRY_CODE3 \$geoip_city_country_code3;
fastcgi_param GEOIP_CITY_COUNTRY_NAME \$geoip_city_country_name;
fastcgi_param GEOIP_REGION \$geoip_region;
fastcgi_param GEOIP_CITY \$geoip_city;
fastcgi_param GEOIP_POSTAL_CODE \$geoip_postal_code;
fastcgi_param GEOIP_CITY_CONTINENT_CODE \$geoip_city_continent_code;
fastcgi_param GEOIP_LATITUDE \$geoip_latitude;
fastcgi_param GEOIP_LONGITUDE \$geoip_longitude;
EOT
    RESTART_NGINX=1
    fi
fi
rm -f /tmp/$$

# Speedup nginx
if [ ! -s /etc/nginx/conf.d/performance.conf ]
then
    cat << EOT > /etc/nginx/conf.d/performance.conf
# See: http://www.blackhatlibrary.net/NGINX

# Compression
gzip on;
gzip_vary on;
gzip_comp_level 4;
gzip_min_length 1100;
gzip_http_version 1.1;
gzip_proxied any;
gzip_types      text/plain text/css application/x-javascript text/xml application/xml application/xml+rss text/javascript;
gzip_buffers    16 8k;

# Output buffering
output_buffers 8 32k;

# See: http://wiki.nginx.org/HttpCoreModule#open_file_cache
open_file_cache max=1000 inactive=20s; 
open_file_cache_valid    30s; 
open_file_cache_min_uses 2;
open_file_cache_errors   on;

# Use this script to adjust the number after about a week of operation:
#SCRIPT##!/bin/bash
#SCRIPT## Based on https://easyengine.io/tutorials/nginx/tweaking-fastcgi-buffers/
#SCRIPT## Where the nginx logs are
#SCRIPT#NLD=/var/log/nginx
#SCRIPT#trap "rm -rf /tmp/$$*" EXIT
#SCRIPT## Get the sizes of positive responses
#SCRIPT#grep ' 200 ' ${NLD}/nginx-proxy.log ${NLD}/access.log > /tmp/$$.log
#SCRIPT#LOGLINES=$(sed -n '$=' /tmp/$$.log)
#SCRIPT#MAX_SIZE=$(awk '{print $10}' /tmp/$$.log  | sort -nr | head -n 1)
#SCRIPT#AVG_SIZE=$(awk '{print $10}' /tmp/$$.log | awk '{s+=$1} END {print int(s / '$LOGLINES')}')
#SCRIPT## Calculate the recommended buffer size (round up to a multiple of 4)
#SCRIPT#BUFFER_SIZE=$(($(($AVG_SIZE / 1024)) + 1))
#SCRIPT#BUFFER_SIZE=$(($(($(($BUFFER_SIZE / 4)) + 1)) * 4))
#SCRIPT## Calculate the number buffers to hold the max. size
#SCRIPT#BUFFER_NUMBER=$(($MAX_SIZE / $(($BUFFER_SIZE * 1024))))
#SCRIPT## Show what we found
#SCRIPT#cat << EOT
#SCRIPT#Set "fastcgi_buffer_size" to "${BUFFER_SIZE}k"
#SCRIPT#Set "fastcgi_buffers" to "$BUFFER_NUMBER ${BUFFER_SIZE}k"
#SCRIPT#EOT
fastcgi_buffers 12 16k;
fastcgi_buffer_size 16k;

# See https://www.nginx.com/blog/thread-pools-boost-performance-9x/
#sendfile on;
sendfile_max_chunk 512k;
aio threads;

EOT
    sed -i 's@[[:space:]]*gzip on;@# Now set in /etc/nginx/conf.d/performance.conf\n# gzip on;@' /etc/nginx/nginx.conf
    RESTART_NGINX=1
fi

if [ ! -s /etc/systemd/system/nginx.service.d/nginx.conf ]
then
    mkdir -p /etc/systemd/system/nginx.service.d
    cat << EOT > /etc/systemd/system/nginx.service.d/nginx.conf
# See https://gist.github.com/denji/8359866
[Service]
LimitNOFILE=30000
EOT
    systemctl daemon-reload
    RESTART_NGINX=1
fi

# Harden nginx
if [ ! -s /etc/nginx/conf.d/security.conf ]
then
    cat << EOT > /etc/nginx/conf.d/security.conf
# See: http://fralef.org/nginx-hardening-some-good-security-practices.html

## Silently block all undefined vhost access : that's a good start!
server {
        server_name _;
        return 444;
}

## Disable Nginx version number in error pages and Server header
server_tokens off;

## Socket settings : Set buffer size limitations (protect against buffer overflows)
client_header_buffer_size   4k;
large_client_header_buffers 8 8k;
client_max_body_size 20m;
connection_pool_size 8192;
request_pool_size 8k;

## Add here all HTTP method allowed
map \$request_method \$bad_method {
        default 1;
        ~(?i)(GET|HEAD|POST) 0;
}

## Add here all user agents that are to be blocked.
map \$http_user_agent \$bad_bot {
        default 0;
        ~(?i)(httrack|WinHTTrack|htmlparser|urllib|Zeus|scan|email|PycURL|Pyth|PyQ|WebCollector|WebCopier|WebCopy|webcraw|Havij|SemrushBot|\\<) 1;
}

## Add here all referrers that are to blocked.
map \$http_referer \$bad_referer {
        default 0;
        ~(?i)(babes|click|forsale|jewelry|nudit|organic|poker|porn|amnesty|poweroversoftware|webcam|zippo|casino|replica) 1;
}

## Block by country (some also done in /etc/firehol/firehol.conf)
# See also /etc/nginx/conf.d/geoip.conf
#map \$geoip_country_code \$allow_visit { 
#  default no; 
#  US yes;
#  CA yes;
#}

# See http://www.cyberciti.biz/tips/linux-unix-bsd-nginx-webserver-security.html

 ## Start: Timeouts ##
  client_body_timeout   10;
  client_header_timeout 10;
 # keepalive_timeout     5 5;
  send_timeout          10;
 ## End: Timeouts ##

 ### Directive describes the zone, in which the session states are stored i.e. store in perip_limit. ###
 ### 1m can handle 16000 sessions with 64 bytes/session, set to 10m x 16000 session ###
       limit_conn_zone \$binary_remote_addr zone=perip_limit:10m; 
 
 ### Control maximum number of simultaneous connections for one session i.e. ###
 ### restricts the amount of connections from a single ip address ###
  limit_conn perip_limit 10;
  limit_conn_log_level info;
EOT
    RESTART_NGINX=1
fi

[ $RESTART_NGINX -ne 0 ] && service nginx restart

# Create a self-signed cert if necessary
CREATE_CERT=0
if [ -s /etc/nginx/nginx.pem ]
then
    # Will the existing cert expire in less than 1 week?
    openssl x509 -checkend $((7 * 24 * 60 * 60)) -in /etc/ssl/certs/nginx.pem &> /dev/null
    [ $? -ne 0 ] && CREATE_CERT=1
else
    CREATE_CERT=1
fi
[ $CREATE_CERT -ne 0 ] && CreateSelfSignedCert.sh -C ${WS_NAME} -O 'B-LUC Consulting'

[ -s /usr/share/nginx/www/info.php ] || cat << EOT > /usr/share/nginx/www/info.php
<?php
if (preg_match ( '/^$LOCALNET/', \$_SERVER["REMOTE_ADDR"]) == 1) phpinfo();
?>
EOT

# Increase some process limits
cat << EOT > /etc/security/limits.conf
# /etc/security/limits.conf
#
#Each line describes a limit for a user in the form:
#
#<domain>        <type>  <item>  <value>
#
#Where:
#<domain> can be:
#        - an user name
#        - a group name, with @group syntax
#        - the wildcard *, for default entry
#        - the wildcard %, can be also used with %group syntax,
#                 for maxlogin limit
#        - NOTE: group and wildcard limits are not applied to root.
#          To apply a limit to the root user, <domain> must be
#          the literal username root.
#
#<type> can have the two values:
#        - "soft" for enforcing the soft limits
#        - "hard" for enforcing hard limits
#
#<item> can be one of the following:
#        - core - limits the core file size (KB)
#        - data - max data size (KB)
#        - fsize - maximum filesize (KB)
#        - memlock - max locked-in-memory address space (KB)
#        - nofile - max number of open files
#        - rss - max resident set size (KB)
#        - stack - max stack size (KB)
#        - cpu - max CPU time (MIN)
#        - nproc - max number of processes
#        - as - address space limit (KB)
#        - maxlogins - max number of logins for this user
#        - maxsyslogins - max number of logins on the system
#        - priority - the priority to run user process with
#        - locks - max number of file locks the user can hold
#        - sigpending - max number of pending signals
#        - msgqueue - max memory used by POSIX message queues (bytes)
#        - nice - max nice priority allowed to raise to values: [-20, 19]
#        - rtprio - max realtime priority
#        - chroot - change root to directory (Debian-specific)
#
#<domain>      <type>  <item>         <value>
#

#*               soft    core            0
#root            hard    core            100000
#*               hard    rss             10000
#@student        hard    nproc           20
#@faculty        soft    nproc           20
#@faculty        hard    nproc           50
#ftp             hard    nproc           0
#ftp             -       chroot          /ftp
#@student        -       maxlogins       4

*	soft	nofile		8192
*	hard	nofile		100000

# End of file
EOT

# Adapt syslog to capture nginx errors
if [ -d /etc/rsyslog.d ]
then
    # Suppress the "rsync not running" log messages
    cat << EOT > /etc/rsyslog.d/49-local.conf
# Suppress "Process 'rsync' not running" messages
:msg,contains,"Process 'rsync' not running" ~
EOT

    # Forward nginx logs to syslog
    cat << EOT > /etc/rsyslog.d/40-nginx.conf
# See: http://loggly.com/support/sending-data/logging-from/web-services/nginx/

# Enable reading log files
# => group owned by "adm"
\$PrivDropToGroup adm
# => in general
module(load="imfile")

# The NGINX access log
input(type="imfile"
      File="/var/log/nginx/access.log"
      Tag="nginx-access"
      Severity="info"
      Facility="local7")

# The NGINX proxy access log
input(type="imfile"
      File="/var/log/nginx/nginx-proxy.log"
      Tag="nginx-proxy-access"
      Severity="info"
      Facility="local7")

# The NGINX error log
input(type="imfile"
      File="/var/log/nginx/error.log"
      Tag="nginx-error"
      Severity="error"
      Facility="local7")

# -> Suppress anything but "forbidden" messages
if (\$programname contains 'nginx-error') and (not (re_match(\$rawmsg, ' forbidden|ModSecurity'))) then stop
EOT
    service rsyslog restart

    read -p 'Forward syslogs to central server  [y/N] ? ' ANSWER
    if [ "T${ANSWER^^}" = 'TY' ]
    then
        read -p 'IP address of central log server ? ' CLIP
        if [ ! -z "$CLIP" ]
        then
             apt-get install rsyslog-gnutls rsyslog-relp

             cat << EOT > /etc/rsyslog.d/LogMate.conf
# Enable log forwarding via RELP
\$ModLoad omrelp
# Forward messages to the central server
*.* :omrelp:${CLIP}:20514;RSYSLOG_ForwardFormat
EOT
             service rsyslog restart
        fi
    fi
fi

# Add server stat pages
apt-get install libwww-perl librrds-perl
cat << EOT > /usr/local/sbin/rrd_nginx.pl
#!/usr/bin/perl
# requires: libwww-perl librrds-perl
use RRDs;
use LWP::UserAgent;

# define location of rrdtool databases
my \$rrd = '/usr/local/rrd';
# define location of images
my \$img = '/var/www/html/rrd';
# define your nginx stats URL
my \$URL = "http://${WS_NAME}/nginx_status";

# Create the necessary directories
system("mkdir -p \$rrd \$img");

my \$ua = LWP::UserAgent->new( timeout => 30 );
my \$response = \$ua->get(\$URL);
die "Can not get server stats: " . \$response->message . "\\n"
  unless ( \$response->is_success );

my \$requests = 0;
my \$total    = 0;
my \$reading  = 0;
my \$writing  = 0;
my \$waiting  = 0;

foreach ( split( /\\n/, \$response->content ) )
{
    \$total = \$1 if (/^Active connections:\\s+(\\d+)/);
    if (/^Reading:\\s+(\\d+).*Writing:\\s+(\\d+).*Waiting:\\s+(\\d+)/)
    {
        \$reading = \$1;
        \$writing = \$2;
        \$waiting = \$3;
    } ## end if (/^Reading:\\s+(\\d+).*Writing:\\s+(\\d+).*Waiting:\\s+(\\d+)/...)
    \$requests = \$3 if (/^\\s+(\\d+)\\s+(\\d+)\\s+(\\d+)/);
} ## end foreach ( split( /\\n/, \$response...))

#print "RQ:\$requests; TT:\$total; RD:\$reading; WR:\$writing; WA:\$waiting\\n";

# if rrdtool database doesn't exist, create it
if ( !-e "\$rrd/nginx.rrd" )
{
    RRDs::create "\$rrd/nginx.rrd",
      "-s 60",
      "DS:requests:COUNTER:120:0:100000000",
      "DS:total:ABSOLUTE:120:0:60000",
      "DS:reading:ABSOLUTE:120:0:60000",
      "DS:writing:ABSOLUTE:120:0:60000",
      "DS:waiting:ABSOLUTE:120:0:60000",
      "RRA:AVERAGE:0.5:1:2880",
      "RRA:AVERAGE:0.5:30:672",
      "RRA:AVERAGE:0.5:120:732",
      "RRA:AVERAGE:0.5:720:1460";
} ## end if ( !-e "\$rrd/nginx.rrd"...)

# insert values into rrd database
RRDs::update "\$rrd/nginx.rrd",
  "-t", "requests:total:reading:writing:waiting",
  "N:\$requests:\$total:\$reading:\$writing:\$waiting";

# Generate graphs
CreateGraphs('day');
CreateGraphs('week');
CreateGraphs('month');
CreateGraphs('year');

# Generate the index.html file
if ( open (IHF, '>', "\$img/index.html" ) )
{
    my \$Now = localtime;
    print IHF <<"EOIF";
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
 <title> NGINX Server Status </title>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <meta http-equiv="refresh" content="60">
 <style media="screen" type="text/css">
 body {
  background-color: #FFFFF0;
  /* Fonts */
  font-family: Arial, Verdana, sans-serif;
  font-size: 18px;
  color: #00008B;
  /* Scrollbar */
  scrollbar-face-color: antiquewhite;
  scrollbar-highlight-color: darkorange;
  scrollbar-3dlight-color: yellow;
  scrollbar-darkshadow-color: black;
  scrollbar-shadow-color: antiquewhite;
  scrollbar-arrow-color: blue;
  scrollbar-track-color: lightyellow;
}
a { 
  font-family: Arial, Verdana, sans-serif; font-size: 18px; color: #483D8B; text-decoration: underline;
}
a:hover { 
  font-family: Arial, Verdana, sans-serif; font-size: 18px; color: #A52A2A; background-color: #FAEBD7;
}
table { 
  font-family: Arial, Verdana, sans-serif; font-size: 14px; color: #00008B; 
  margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px;
  padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0p;
}
 </style>
</head>
   
<body>
<div align="center">
 <table border="0">
  <tr>
   <th width="12%">&nbsp;</th>
   <th colspan="2">
    <span style="font-size:36px;color:#A52A2A">NGINX Server Status</span>
    <br>
    <span style="font-size:10px">Last updated: \$Now</span>
   </th>
  </tr>
  <tr>
   <th width="12%">&nbsp;</th>
   <th width="44%">Connections</th>
   <th width="44%">Requests</th>
  </tr>
  <tr>
   <th width="12%">Daily</th>
   <td width="44%"><a href="connections-day.png" target="_blank"><img width="300px" height="100px" src="connections-day.png"></a></td>
   <td width="44%"><a href="requests-day.png" target="_blank"><img width="300px" height="100px" src="requests-day.png"></a></td>
  </tr>
  <tr>
   <th width="12%">Weekly</th>
   <td width="44%"><a href="connections-week.png" target="_blank"><img width="300px" height="100px" src="connections-week.png"></a></td>
   <td width="44%"><a href="requests-week.png" target="_blank"><img width="300px" height="100px" src="requests-week.png"></a></td>
  </tr>
  <tr>
   <th width="12%">Monthly</th>
   <td width="44%"><a href="connections-month.png" target="_blank"><img width="300px" height="100px" src="connections-month.png"></a></td>
   <td width="44%"><a href="requests-month.png" target="_blank"><img width="300px" height="100px" src="requests-month.png"></a></td>
  </tr>
  <tr>
   <th width="12%">Yearly</th>
   <td width="44%"><a href="connections-year.png" target="_blank"><img width="300px" height="100px" src="connections-year.png"></a></td>
   <td width="44%"><a href="requests-year.png" target="_blank"><img width="300px" height="100px" src="requests-year.png"></a></td>
  </tr>
 </table>
</div>
</body>
</html>
EOIF
    close(IHF);
} ## end if ( open( IHF, '>', "$img/index.html"...))

# We are done
exit 0;

#------------------------------------------------------------------------------
sub CreateGraphs(\$)
{
    my \$period = shift;

    RRDs::graph "\$img/requests-\$period.png",
      "-s -1\$period",
      "-t Requests on nginx per \$period",
      "--lazy",
      "-h", "150", "-w", "700",
      "-l 0",
      "-a", "PNG",
      "-v requests/sec",
      "DEF:requests=\$rrd/nginx.rrd:requests:AVERAGE",
      "LINE2:requests#336600:Requests",
      "GPRINT:requests:MAX:  Max\\\\: %5.2lf",
      "GPRINT:requests:AVERAGE: Avg\\\\: %5.2lf",
      "GPRINT:requests:LAST: Current\\\\: %5.2lf req/sec",
      "HRULE:0#000000";
    if ( \$ERROR = RRDs::error )
    {
        print "\$0: unable to generate \$period graph: \$ERROR\\n";
    } ## end if ( \$ERROR = RRDs::error...)

    RRDs::graph "\$img/connections-\$period.png",
      "-s -1\$period",
      "-t Connections on nginx per \$period",
      "--lazy",
      "-h", "150", "-w", "700",
      "-l 0",
      "-a", "PNG",
      "-v conns/sec",
      "DEF:total=\$rrd/nginx.rrd:total:AVERAGE",
      "DEF:reading=\$rrd/nginx.rrd:reading:AVERAGE",
      "DEF:writing=\$rrd/nginx.rrd:writing:AVERAGE",
      "DEF:waiting=\$rrd/nginx.rrd:waiting:AVERAGE",

      "LINE2:total#22FF22:Total",
      "GPRINT:total:LAST:   Current\\\\: %5.2lf",
      "GPRINT:total:MIN:  Min\\\\: %5.2lf",
      "GPRINT:total:AVERAGE: Avg\\\\: %5.2lf",
      "GPRINT:total:MAX:  Max\\\\: %5.2lf conns/sec\\\\n",

      "LINE2:reading#0022FF:Reading",
      "GPRINT:reading:LAST: Current\\\\: %5.2lf",
      "GPRINT:reading:MIN:  Min\\\\: %5.2lf",
      "GPRINT:reading:AVERAGE: Avg\\\\: %5.2lf",
      "GPRINT:reading:MAX:  Max\\\\: %5.2lf conns/sec\\\\n",

      "LINE2:writing#FF0000:Writing",
      "GPRINT:writing:LAST: Current\\\\: %5.2lf",
      "GPRINT:writing:MIN:  Min\\\\: %5.2lf",
      "GPRINT:writing:AVERAGE: Avg\\\\: %5.2lf",
      "GPRINT:writing:MAX:  Max\\\\: %5.2lf conns/sec\\\\n",

      "LINE2:waiting#00AAAA:Waiting",
      "GPRINT:waiting:LAST: Current\\\\: %5.2lf",
      "GPRINT:waiting:MIN:  Min\\\\: %5.2lf",
      "GPRINT:waiting:AVERAGE: Avg\\\\: %5.2lf",
      "GPRINT:waiting:MAX:  Max\\\\: %5.2lf conns/sec\\\\n",

      "HRULE:0#000000";
    if ( \$ERROR = RRDs::error )
    {
        print "\$0: unable to generate \$period graph: \$ERROR\\n";
    } ## end if ( \$ERROR = RRDs::error...)
} ## end sub CreateGraphs(\$)
__END__
EOT

#--------------------------------------------------------------------
# Create the cronjob for the stats
cat << EOT > /etc/cron.d/nginx
#
# Regular cron jobs for the nginx package
#
* * * * *	root	[ -x /usr/local/sbin/rrd_nginx.pl ] && /usr/local/sbin/rrd_nginx.pl
EOT

#--------------------------------------------------------------------
cat << EOT > /etc/logrotate.d/nginx
/var/log/nginx/*.log {
        daily
        missingok
        rotate 14
        compress
        delaycompress
        notifempty
        create 644 nginx adm
        sharedscripts
        postrotate
                [ -f /var/run/nginx.pid ] && kill -USR1 \`cat /var/run/nginx.pid\`
                [ -f /var/run/rsyslogd.pid ] && kill -HUP \`cat /var/run/rsyslogd.pid\`
        endscript
}
EOT

#--------------------------------------------------------------------
# Get/update the main log_format for ngxinx
sed '/START/,/END/d' /etc/goaccess.conf > /tmp/goaccess.conf
RAW_LF=$(sed -n '/log_format/,/;/p' /etc/nginx/nginx.conf)
LOG_FORMAT=$(sed -e "s/'//g;s/;//g" <<< $RAW_LF | cut -d' ' -f3-)

echo '# START: Log format from /etc/nginx/nginx.conf' >> /tmp/goaccess.conf
nginx2goaccess.sh "$LOG_FORMAT" | grep format >> /tmp/goaccess.conf
echo '# END: Log format from /etc/nginx/nginx.conf' >> /tmp/goaccess.conf

diff -wu /tmp/goaccess.conf /etc/goaccess.conf &> /dev/null
[ $? -ne 0 ] && cat /tmp/goaccess.conf > /etc/goaccess.conf

# Get/update the upstream_time log_format for ngxinx
sed '/START/,/END/d' /etc/goaccess.conf > /tmp/goaccess.conf
RAW_LF=$(sed -n '/log_format upstream_time/,/;/p' /etc/nginx/conf.d/proxy-system.conf)
LOG_FORMAT=$(sed -e "s/'//g;s/;//g" <<< $RAW_LF | cut -d' ' -f3-)

echo '# START: Log format from /etc/nginx/conf.d/proxy-system.conf' >> /tmp/goaccess.conf
nginx2goaccess.sh "$LOG_FORMAT" | grep format >> /tmp/goaccess.conf
echo '# END: Log format from /etc/nginx/conf.d/proxy-system.conf' >> /tmp/goaccess.conf

diff -wu /tmp/goaccess.conf /etc/proxy-goaccess.conf &> /dev/null
[ $? -ne 0 ] && cat /tmp/goaccess.conf > /etc/proxy-goaccess.conf

#--------------------------------------------------------------------
# Get the custom recompile script for nginx
wget -O /usr/local/sbin/nginx-recompile.sh \
  https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/nginx-recompile.sh
chmod 744 /usr/local/sbin/nginx-recompile.sh

#--------------------------------------------------------------------
# We are done
exit 0
