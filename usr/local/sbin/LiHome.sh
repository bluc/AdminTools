
# Version 20240119-110246 checked into repository
################################################################
# (c) Copyright 2017 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/LiHome.sh

#-------------------------------------------------------------------------
# Globals
#-------------------------------------------------------------------------
PROG=${0##*/}

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
DEBUG=''
[[ $- ==  *x* ]] && DEBUG='-v'

# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE
logger -p info -t ${PROG}[$$] -- Starting

THISHOST=$(hostname)
[[ $THISHOST == *.* ]] || THISHOST=$(hostname -f)
THISDOMAIN=${THISHOST#*.}
TH_SHORT=${THISHOST%%.*}
VERBOSE=''
START_TIME=$EPOCHSECONDS

# See https://stackoverflow.com/questions/16908084/bash-script-to-calculate-time-elapsed
secs_to_human()
{
     echo "$(( ${1} / 3600 ))h $(( (${1} / 60) % 60 ))m $(( ${1} % 60 ))s"
}

# Define all important system configs
FILELIST=''
RSYNC_PORT=873
# This is a perl regex:
EXCLUDE_PATTERNS='([Cc]ache|.cache)'
# The list of file extensions which shouldn't be compressed
SKIP_COMPRESS=3g2/3gp/3gpp/3mf/7z/aac/ace/amr/apk/appx/appxbundle/arc/arj/asf/avi/br/bz2/cab/crypt5/crypt7/crypt8/deb/dmg/drc/ear/gz/flac/flv/gpg/h264/h265/heif/iso/jar/jp2/jpg/jpeg/lz/lz4/lzma/lzo/m4a/m4p/m4v/mkv/msi/mov/mp3/mp4/mpeg/mpg/mpv/oga/ogg/ogv/opus/pack/png/qt/rar/rpm/rzip/s7z/sfx/svgz/tbz/tgz/tlz/txz/vob/vma/webm/webp/wim/wma/wmv/xz/z/zip/zst

TFILE=$(mktemp /tmp/$$XXXXXXXX) 

# Allow local hosts to overwrite or expand the FILELIST
if [ -s /usr/local/etc/LiHome.filelist ]
then
    logger -p info -t ${PROG}[$$] -- Reading configuration file
    source /usr/local/etc/LiHome.filelist
else
    logger -p info -t ${PROG}[$$] -- Creating default configuration file
    cat << EOT > /usr/local/etc/LiHome.filelist
# Allow local hosts to overwrite or expand the FILELIST
# Examples:

#EX## Include home directory for "opa"
#EX#[ -d /home/opa ] && FILELIST="\$FILELIST /home/opa/*"

# This is a perl regex:
EXCLUDE_PATTERNS="$EXCLUDE_PATTERNS"

# Define the rsync fileshare
# Format "host::share"
# Can use these variables:
# THISHOST, THISDOMAIN, TH_SHORT
#RSYNC_SHARE=host::${TH_SHORT}-home

# You can overide the rsync port:
RSYNC_PORT=873
EOT
fi

if [ -z "$FILELIST" ]
then
    # Nothing to sync - we are done
    [ -z "$VERBOSE" ] || echo "Empty 'FILELIST'"
    RETCODE=0
elif [ -z "$RSYNC_SHARE" ]
then
    # Nowhere to sync to - we are done
    [ -z "$VERBOSE" ] || echo "Empty 'RSYNC_SHARE'"
    RETCODE=0
else

    # Get possible program options
    while getopts v OPTION
    do
        case ${OPTION} in
            v)  VERBOSE='-v'
                ;;
            *)  echo "Usage: $0 [-v]"
                ;;
        esac
    done
    shift $((OPTIND - 1))

    # Update the "gigasync" script
    WGET_OPTS='-q --no-check-certificate -t 3 -T 60'
    [[ "\$(wget --help)" =~ compression= ]] && WGET_OPTS="\$WGET_OPTS --compression=auto"    
    wget $WGET_OPTS -O /usr/local/bin/gigasync-bluc.pl \
      https://gitlab.com/bluc/AdminTools/raw/master/usr/local/bin/gigasync-bluc.pl
    chmod 755 /usr/local/bin/gigasync-bluc.pl

    # Upload the backup file - one home directory at a time
    if [ -z "$DEBUG" ]
    then
        ST=$((RANDOM % 1800))
        [ $ST -lt 600 ] && ST=$((ST + 600))
        logger -p info -t ${PROG}[$$] -- Waiting $ST seconds
        sleep $ST
    fi

    for FL in $FILELIST
    do
        # Ignore anything that is NOT a directory
        [ "T${FL:0:1}" = 'T/' ] || continue
        [ -d $FL ] || continue

        RETCODE=0
        logger -p info -t ${PROG}[$$] -- Syncing $FL to $RSYNC_SHARE/${FL##*/}
        gigasync-bluc.pl \
          --exclude_pattern="$EXCLUDE_PATTERNS" \
          $FL $RSYNC_SHARE/${FL##*/}
        # Save the log file for later debugging
        [ -s /tmp/gigasync.log ] && mv /tmp/gigasync.log /tmp/gigasync.log.${FL##*/}

#            nice rsync -v --timeout=$((RSYNC_TIMEOUT + TRY * 300)) -rlptD --ipv4 \
#                --port $RSYNC_PORT --bwlimit=${BWLIMIT} \
#                --skip-compress=${SKIP_COMPRESS} \
#                --exclude '[Cc]ache/' $FILELIST $RSYNC_SHARE >> $TFILE 2>&1
        logger -p info -t ${PROG}[$$] -- Syncing to remote server ended with $?
    done
fi

[ -z "$VERBOSE" ] || echo " -> Ending with exit code $RETCODE"
ELAPSED=$(secs_to_human $((EPOCHSECONDS - START_TIME)))
logger -p info -t ${PROG}[$$] -- Done after $ELAPSED
exit $RETCODE
