#!/bin/bash
################################################################
# (c) Copyright 2013 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE

#--------------------------------------------------------------------
# Install fail2ban if necessary
[ -d /etc/fail2ban/filter.d ] || apt-get -y install fail2ban

# Create specific filters for wordpress
RESTART_FAIL2BAN=0
TFILE=$(mktemp /tmp/$$XXXXXXXX)
cat << EOT > $TFILE
# Fail2Ban configuration file
#
# Author: Charles Lecklider
#
 
[INCLUDES]

# Read common prefixes. If any customizations available -- read them from
# common.local
before = common.conf


[Definition]

_daemon = wordpress

# Option:  failregex
# Notes.:  regex to match the password failures messages in the logfile. The
#          host must be matched by a group named "host". The tag "<HOST>" can
#          be used for standard IP/hostname matching and is only an alias for
#          (?:::f{4,6}:)?(?P<host>[\w\-.^_]+)
# Values:  TEXT
#
failregex = <HOST>.*GET.*securimage_show\\.php\\?si_form_id=.*(?!oswegocountytoday\\.com)

# Option:  ignoreregex
# Notes.:  regex to ignore. If this regex matches, the line is ignored.
# Values:  TEXT
#
ignoreregex =
EOT
diff -wu $TFILE /etc/fail2ban/filter.d/wordpress-captcha.conf &> /dev/null
if [ $? -ne 0 ]
then
    cat $TFILE > /etc/fail2ban/filter.d/wordpress-captcha.conf
    RESTART_FAIL2BAN=1
fi

cat << EOT > $TFILE
# Fail2Ban configuration file
#
# Author: Charles Lecklider
#

[INCLUDES]

# Read common prefixes. If any customizations available -- read them from
# common.local
before = common.conf


[Definition]

_daemon = wordpress

# Option:  failregex
# Notes.:  regex to match the password failures messages in the logfile. The
#          host must be matched by a group named "host". The tag "<HOST>" can
#          be used for standard IP/hostname matching and is only an alias for
#          (?:::f{4,6}:)?(?P<host>[\w\-.^_]+)
# Values:  TEXT
#
failregex = <HOST>.*POST.*xmlrpc\\.php.*Googlebot
            <HOST>.*POST.*xmlrpc\\.php HTTP/1\\.." 200 579 

# Option:  ignoreregex
# Notes.:  regex to ignore. If this regex matches, the line is ignored.
# Values:  TEXT
#
ignoreregex =
EOT
diff -wu $TFILE /etc/fail2ban/filter.d/wordpress-xmlrpc.conf &> /dev/null
if [ $? -ne 0 ]
then
    cat $TFILE > /etc/fail2ban/filter.d/wordpress-xmlrpc.conf
    RESTART_FAIL2BAN=1
fi

cat << EOT > $TFILE
# Fail2Ban configuration file
#
# Author: Charles Lecklider
#
 
[INCLUDES]

# Read common prefixes. If any customizations available -- read them from
# common.local
before = common.conf


[Definition]

_daemon = wordpress

# Option:  failregex
# Notes.:  regex to match the password failures messages in the logfile. The
#          host must be matched by a group named "host". The tag "<HOST>" can
#          be used for standard IP/hostname matching and is only an alias for
#          (?:::f{4,6}:)?(?P<host>[\w\-.^_]+)
# Values:  TEXT
#
failregex = GET.*verifying pingback from <HOST>
            <HOST>.*GET.*verifying pingback from

# Option:  ignoreregex
# Notes.:  regex to ignore. If this regex matches, the line is ignored.
# Values:  TEXT
#
ignoreregex =
EOT
diff -wu $TFILE /etc/fail2ban/filter.d/wordpress-pingback.conf &> /dev/null
if [ $? -ne 0 ]
then
    cat $TFILE > /etc/fail2ban/filter.d/wordpress-pingback.conf
    RESTART_FAIL2BAN=1
fi

if [ ! -s /etc/fail2ban/jail.local ]
then
    read -p 'Email address for fail2ban notificatons: ' F_EMAIL
    cat << EOT > /etc/fail2ban/jail.local
[DEFAULT]
destemail = $F_EMAIL
banaction = iptables-multiport-log

[apache]

enabled  = true
port     = http,https
filter   = apache-auth
logpath  = /var/log/apache*/*error.log
maxretry = 6

[apache-badbots]

enabled  = true
port     = http,https
filter   = apache-badbots
logpath  = /var/log/apache*/access.log
maxretry = 2

[pure-ftpd]

enabled  = true
port     = ftp,ftp-data,ftps,ftps-data
filter   = pure-ftpd
logpath  = /var/log/auth.log
maxretry = 6

[wordpress]

enabled = true
port     = http,https
filter = wordpress
logpath = /var/log/auth.log
EOT
fi

# Enable the specific filters for wordpress
if [ -z "$(grep wp-captcha /etc/fail2ban/jail.local)" ]
then
    cat << EOT >> /etc/fail2ban/jail.local

[wp-captcha]

enabled  = true
port     = http,https
filter   = wordpress-captcha
logpath  = /var/log/apache*/access.log
maxretry = 2
findtime = 60
bantime  = 300
EOT
    RESTART_FAIL2BAN=99
fi
if [ -z "$(grep wp-xmlrpc /etc/fail2ban/jail.local)" ]
then
    cat << EOT >> /etc/fail2ban/jail.local

[wp-xmlrpc]

enabled  = true
port     = http,https
filter   = wordpress-xmlrpc
logpath  = /var/log/apache*/access.log
maxretry = 1
bantime  = 86400
EOT
    RESTART_FAIL2BAN=99
fi
if [ -z "$(grep wp-pingback /etc/fail2ban/jail.local)" ]
then
    cat << EOT >> /etc/fail2ban/jail.local

[wp-pingback]

enabled  = true
port     = http,https
filter   = wordpress-pingback
logpath  = /var/log/apache*/access.log
maxretry = 1
bantime  = 600
EOT
    RESTART_FAIL2BAN=99
fi
[ $RESTART_FAIL2BAN -ne 0 ] && service fail2ban restart

#--------------------------------------------------------------------
# We are done
exit 0
