#!/bin/bash
# Version 20240507-063325 checked into repository
################################################################
# (c) Copyright 2014 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/RemoveOldKernel.sh

# Set the correct path for commands
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=${0##*/}

# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE

if [ $(/usr/bin/id -u) -ne 0 ]
then
    echo 'You must be root to continue'
    exit 0
fi

# Stop if the host needs a reboot first
if [ -f /var/run/reboot-required ]
then
    cat /var/run/reboot-required
    exit 0
fi

# The temp file used below
TFILE=$(mktemp /tmp/$$XXXXXXXX)

# Get possible program options
PURGE=0
RUN2=1
VERBOSE=0
while getopts pv OPTION
do
    case ${OPTION} in
        p)  PURGE=1
            RUN2=2
            ;;
        v)  VERBOSE=1
            ;;
        *)  echo "Usage: $0 [-p|-v]"
            ;;
    esac
done
shift $((OPTIND - 1))

if [ -x /usr/bin/apt-get ]
then
    APT='apt-get'
elif [ -x /usr/bin/apt ]
then
    APT='apt'
else
    echo "ERROR: Can't find any suitable 'apt' tool"
    exit 1
fi

# Get the currenty active kernel
CURKV=$(uname -r)
echo "Currently active kernel version: $CURKV"
CURKV_DIG=$(sed 's/[A-Za-z]//g;s/[\.\-]/\+/g;s/\+$//' <<< $CURKV)

# Based on http://ubuntugenius.wordpress.com/2011/01/08/ubuntu-cleanup-how-to-remove-all-unused-linux-kernel-headers-images-and-modules/
# Remove any old kernel images
if [ $PURGE -eq 0 ]
then
    ROK=''
    read -rp 'Purge old kernels [p/n] ? ' ROK
else
    ROK='P'
fi
if [ "T${ROK^^}" = 'TP' ]
then
    RUN=0
    while [ $RUN -lt $RUN2 ]
    do
        [ $VERBOSE -eq 0 ] || date '+%F %T: Removing old kernels and their headers unconditionally'

        dpkg -l 'linux-*' 2> /dev/null | \
            sed '/^ii/!d;/'"$(uname -r | sed "s/\(.*\)-\([^0-9]\+\)/\1/")"'/d;s/^[^ ]* [^ ]* \([^ ]*\).*/\1/;/[0-9]/!d' > $TFILE
        dpkg -l 'pve-kernel-*' 2> /dev/null | \
            sed '/^ii/!d;/'"$(uname -r | sed "s/\(.*\)-\([^0-9]\+\)/\1/")"'/d;s/^[^ ]* [^ ]* \([^ ]*\).*/\1/;/[0-9]/!d' >> $TFILE
        grep -Ea '(\-pve|image)' $TFILE | while read -r P
        do
            P_DIG=$(sed 's/[A-Za-z]//g;s/[\.\-]/\+/g;s/\+$//g;s/^\+//g' <<< $P)
            [ $((P_DIG)) -lt $((CURKV_DIG)) ] || continue
            $APT -y purge $P ${P//image/headers}
            PV=$(grep -o '[0-9\.\-]*' <<< $P | sed 's/^\-//g;s/\-$//g;/^$/d')
            [ -z "$PV" ] && continue
            echo Deleting $PV
            rm -rf /boot/*${PV:?}* /lib/modules/${PV:?}*
        done

        # Handle newer proxmox kernels
        apt-get purge $(dpkg --list | grep -P -o "(proxmox|pve)-kernel-\d\S+" | grep -v $(uname -r | grep -P -o ".+\d") | grep pve)

        RUN=$((RUN + 1))
    done
fi

# We are done
exit 0
