#!/bin/bash
# Version 20240112-193228 checked into repository
################################################################
# (c) Copyright 2013-2018 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/KVMInfo.sh

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
trap "rm -f /tmp/USED_VM_MEM /tmp/USED_VM_CPU /tmp/VMStats.pl" EXIT

# Globals
TOTAL_MEM=$(awk '/^MemTotal/ {print int($2 / 1024)}' /proc/meminfo)
FREE_MEM=$(awk '/^MemFree/ {print int($2 / 1024)}' /proc/meminfo)
TOTAL_CPU=$(grep -c processor /proc/cpuinfo)
USED_VM_MEM=0
USED_VM_CPU=0

# Get info per defined KVM guest
QM_NO=0
qm list | while read -r VM_ID VM_NAME VM_STATUS VM_ALLOC_MEM VM_BOOTDISK VM_PID
do
    [ "T$VM_ID" = 'TVMID' ] && continue
    if [ "T$VM_STATUS" = 'Trunning' ]
    then
        set $(qm status $VM_ID -verbose | awk '/^maxmem/{MM=int($2/1048576)};/^mem/{CM=int($2/1048576)};/^uptime/{UT=$2};/^cpus/{CPU=$2}END{print MM" "CM" "UT" "CPU}')
        # Example:
        #cpus: 1
        #disk: 0
        #diskread: 0
        #diskwrite: 0
        #maxdisk: 0
        #maxmem: 805306368
        #mem: 609363751
        #name: backoffice
        #netin: 3140235182
        #netout: 122618113
        #pid: 2093
        #status: running
        #uptime: 313584
        if [ $# -eq 4 ]
        then
            VM_USED_MEM=$2
            VM_UPTIME=$3
            VM_CPUS=$4
        else
            # Could not get all infos => assume some defaults
            VM_USED_MEM=$VM_ALLOC_MEM
            VM_CPUS=1
        fi
    else
        # Not running - using no memory and no CPU
        VM_USED_MEM=0
        VM_CPUS=0
    fi
    USED_VM_MEM=$((VM_USED_MEM + USED_VM_MEM))
    echo "$USED_VM_MEM" > /tmp/USED_VM_MEM
    USED_VM_CPU=$((VM_CPUS + USED_VM_CPU))
    echo "$USED_VM_CPU" > /tmp/USED_VM_CPU

    # Show what we have for this guest
    if [ $QM_NO -eq 0 ]
    then
        cat << EOT
==========================================================================
EOT
    else
        cat << EOT
--------------------------------------------------------------------------
EOT
    fi
    QM_NO=$((QM_NO + 1))
    cat << EOT
Virtual guest    : $VM_NAME (ID: $VM_ID)
EOT
    if [ $VM_USED_MEM -eq 0 ]
    then
        echo "*** NOT RUNNING ***"
    else
        cat << EOT
Memory allocated : $VM_ALLOC_MEM MB
Memory usage     : $VM_USED_MEM MB ($(( VM_USED_MEM * 100 / VM_ALLOC_MEM ))%)
Number of CPUs   : $VM_CPUS
Bootdisk size    : $VM_BOOTDISK GB ($(awk '/disk-1/ {print $2}' /etc/pve/qemu-server/$VM_ID.conf))
Up since         : $(date "+%F %T" -d "$VM_UPTIME seconds ago")
EOT
    fi
done

# Show summary at the end
PXM_VERS=$(pvesh get /version 2> /dev/null | awk '/version/{gsub(/[",]/,"");V=$NF};/repoid/{gsub(/[",]/,"");R=$NF}END{print V"/"R}')
[ "T$PXM_VERS" = 'T│/│' ] && PXM_VERS=$(pvesh get /version --output-format yaml | awk '/version/{gsub(/[",]/,"");V=$NF};/repoid/{gsub(/[",]/,"");R=$NF}END{print V"/"R}')
ANONHP=$(awk '/AnonHugePages/ {print $(NF-1)}' /proc/meminfo)
KSM_PAGES=$(< /sys/kernel/mm/ksm/pages_sharing)
cat << EOT
==========================================================================
Proxmox version                   : $PXM_VERS
==========================================================================
EOT
[ -f /etc/pve/corosync.conf ] && cat << EOT
PVE Cluster Nodes:
$(pvecm nodes)
==========================================================================
PVE Cluster Status:
$(pvecm status)
==========================================================================
EOT
cat << EOT
Kernel Samepage Merging (KSM)     : $((KSM_PAGES * 4 / 1024)) MB ($KSM_PAGES page(s) @ 4K each)
==========================================================================
Total memory in system            : $TOTAL_MEM MB
Transparent huge pages            : $ANONHP KB
EOT
if [ -s /tmp/USED_VM_MEM ] && [ -s /tmp/USED_VM_CPU ]
then
    USED_VM_MEM=$(< /tmp/USED_VM_MEM)
    USED_TOTAL_MEM=$((TOTAL_MEM - FREE_MEM))

    USED_VM_CPU=$(< /tmp/USED_VM_CPU)
    cat << EOT
Memory used by all virtual guests : $USED_VM_MEM MB ($(( USED_VM_MEM * 100 / TOTAL_MEM ))%)
Memory used across ALL processes
(includes OS buffers etc.)        : $USED_TOTAL_MEM MB ($(( USED_TOTAL_MEM * 100 / TOTAL_MEM ))%)
EOT
fi
cat << EOT
Total number of CPUs in system    : $TOTAL_CPU
CPUs used by all virtual guests   : $USED_VM_CPU ($(( USED_VM_CPU * 100 / TOTAL_CPU ))%)
==========================================================================
EOT

# We are done
exit 0
