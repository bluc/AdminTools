#!/usr/bin/python3
#--------------------------------------------------------------------
# (c) CopyRight 2014-2022 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#--------------------------------------------------------------------
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/geolookup-server.py

# Import necessary modules
import socketserver
import threading
from ipaddress import ip_address,IPv4Address,IPv4Network
import re
import sys
import os
from datetime import datetime
# Requires: python3-geoip2
import geoip2.database

"""
Reference implementation of a client for this server:

import sys
import socket
from ipaddress import ip_address,IPv4Address,IPv4Network
import re

# Regex for valid IP addresses
valid_ip = re.compile('((?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))')

# The UDP server to connect to
serverAddressPort   = ("127.0.0.1", 43210)

msg = 'any string with at least one valid IP address in it, can be read from STDIN'
rc = valid_ip.findall(msg)
if rc:
    countries = []
    for i, ip in enumerate(rc):
        country = 'na'
        if not ((ip_address(ip).is_private) or (ip_address(ip).is_reserved)
            or (ip_address(ip).is_multicast) or (ip_address(ip).is_loopback)
            or (ip_address(ip).is_unspecified) or (ip_address(ip).is_link_local)
            or (IPv4Address(ip) in IPv4Network('100.64.0.0/10'))):

            # Get country for IP
            UDPClientSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
            try:
                UDPClientSocket.sendto(ip.encode(), serverAddressPort)
                country = UDPClientSocket.recvfrom(1024)
                country = country[0].decode()
            except Exception as Ex:
                print("Exception Occurred: %s" % Ex);
                country = 'na'

            countries.append(country[:2])

    newmsg = str.join(countries)
"""

# Create a tuple with IP Address and Port Number
ServerAddress = ("127.0.0.1", 43210)

# The regex for a valid IP address
valid_ip = re.compile ('((?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))')

# Subclass the DatagramRequestHandler
class MyUDPRequestHandler(socketserver.DatagramRequestHandler):
    allow_reuse_address = True

    # Override the handle() method
    def handle(self):

        # Receive and print the datagram received from client
        datagram = self.rfile.readline().strip()
        data = datagram.decode()
        msg = "Received {}" . format(data) + " from {}" . format(self.client_address) + " in thread {}" . format(threading.current_thread().name)
        now = datetime.now()
        print (now.strftime("%F %T DEBUG: ") + msg)

        # Only process valid IP(v4) addresses
        rc = valid_ip.match(data)
        if rc:
            # Weed out private IP addresses
            if not ((ip_address(rc.group()).is_private) or (ip_address(rc.group()).is_reserved)
               or (ip_address(rc.group()).is_multicast) or (ip_address(rc.group()).is_loopback)
               or (ip_address(rc.group()).is_unspecified) or (ip_address(rc.group()).is_link_local)
               or (IPv4Address(rc.group()) in IPv4Network('100.64.0.0/10'))):

               # Get country for IP
               try:
                   response = reader.city(rc.group())
                   country = response.country.iso_code
               except:
                   now = datetime.now()
                   print (now.strftime("%F %T ERROR: Can't retrieve country for ") + data)
                   country = 'na'
        else:
            now = datetime.now()
            print (now.strftime("%F %T DEBUG: ") + data + " isn't a public IP address")
            country = 'na'

        # Send a message to the client
        now = datetime.now()
        print (now.strftime("%F %T DEBUG: Country for ") + format(data) + " is " + country)
        self.wfile.write(country.encode())


# Initialize the connection to the geo database
reader = geoip2.database.Reader('/var/lib/GeoIP/GeoLite2-City.mmdb')

# Create a Server Instance
UDPServerObject = socketserver.ThreadingUDPServer(ServerAddress, MyUDPRequestHandler)
mypid = os.getpid()
now = datetime.now()
print (now.strftime("%F %T INFO: Listening on port 43210 with pid ") + str(mypid))

# Notate the PID of this process
with open('/var/run/geolookup-server.pid', 'w') as f:
    mypid = mypid
    f.write(format(mypid))
    f.close()
    now = datetime.now()
    print (now.strftime("%F %T INFO: Updated /var/run/geolookup-server.pid"))

# Make the server wait forever serving connections
UDPServerObject.serve_forever()
