#!/bin/bash
################################################################
# (c) Copyright 2019 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/Customize_Fail2Ban.sh

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            

#--------------------------------------------------------------------
# Patch fail2ban's config (if necessary)
if [ -s /etc/fail2ban/jail.conf ]
then
    RESTART_F2B=0

    TFILE=$(mktemp /tmp/$$XXXXXXXX)
    cat << EOT > $TFILE
# Fail2Ban configuration file
#
# Author: Daniel Black
#
# This is a included configuration file and includes the definitions for the iptables
# used in all iptables based actions by default.
#
# The user can override the defaults in iptables-common.local
#
# Modified: Alexander Koeppe <format_c@online.de>, Serg G. Brester <serg.brester@sebres.de>
#       made config file IPv6 capable (see new section Init?family=inet6)

[INCLUDES]

[Definition]

[Init]

# Always start all chains at start
actionstart_on_demand = false
EOT

    diff $TFILE /etc/fail2ban/action.d/iptables-common.local &> /dev/null
    if [ $? -ne 0 ]
    then
        cat $TFILE > /etc/fail2ban/action.d/iptables-common.local
        RESTART_F2B=1
    fi

    # Based on https://www.righter.ch/index.php/2014/12/10/block-a-whole-ip-range-with-fail2ban/
    if [ ! -s /etc/fail2ban/get_network.py ]
    then
        # Get the python script to get the full range for an IP address
        #  (using the information provided by "whois")
        wget -q -O /etc/fail2ban/get_network.py \
          https://www.righter.ch/wp-content/uploads/2014/12/get_network.txt
        [ -s /etc/fail2ban/get_network.py ] || exit 1
    fi
    chmod +x /etc/fail2ban/get_network.py

    CREATE_CF=0
    if [ -s /etc/fail2ban/action.d/iptables-multiport-range-log.conf ]
    then
        [ -z "$(grep get_network.py /etc/fail2ban/action.d/iptables-multiport-range-log.conf)" ] && CREATE_CF=1
    else
        CREATE_CF=1
    fi
    if [ $CREATE_CF -ne 0 ]
    then
        # Create the correct config file
        sed 's@^actionban.*@actionban = /etc/fail2ban/get_network.py <ip> | while read PREFIX; do iptables -C f2b-<name> -s $PREFIX -j DROP >/dev/null || iptables -I f2b-<name> 1 -s $PREFIX -j DROP; done@;
             s@^actionunban.*@actionunban = /etc/fail2ban/get_network.py <ip> | while read PREFIX; do iptables -D f2b-<name> -s $PREFIX -j DROP; done@' \
          /etc/fail2ban/action.d/iptables-multiport-log.conf > /etc/fail2ban/action.d/iptables-multiport-range-log.conf
    fi

    TFILE=$(mktemp /tmp/$$XXXXXXXX)
    cat << EOT > $TFILE
# Fail2Ban configuration file
#
# Author: Cyril Jaquier
#
# Added "unban" action: Thomas Bullinger

[INCLUDES]

before = sendmail-common.conf

[Definition]

# Option:  actionban
# Notes.:  command executed when banning an IP. Take care that the
#          command is executed with Fail2Ban user rights.
# Tags:    See jail.conf(5) man page
# Values:  CMD
#
actionban = printf %%b "Subject: [Fail2Ban] <name>: banned <ip> from \`uname -n\`
            Date: \`LC_ALL=C date +"%%a, %%d %%h %%Y %%T %%z"\`
            From: <sendername> <<sender>>
            To: <dest>\n
            Hi,\n
            The IP <ip> has just been banned for <bantime> seconds
            by Fail2Ban after <failures> attempts against <name>.\n\n
            Here is more information about <ip> :\n
            \`/usr/bin/whois <ip> || echo missing whois program\`\n\n
            Lines containing IP:<ip> in <logpath>\n
            \`grep -E <grepopts> '(^|[^0-9])<ip>([^0-9]|$)' <logpath>\`\n\n
            Regards,\n
            Fail2Ban" | /usr/sbin/sendmail -f <sender> <dest>

actionunban = printf %%b "Subject: [Fail2Ban] <name>: unbanned <ip> from \`uname -n\`
              Date: \`LC_ALL=C date +"%%a, %%d %%h %%Y %%T %%z"\`
              From: <sendername> <<sender>>
              To: <dest>\n
              Hi,\n
              The IP <ip> has been unbanned by Fail2Ban after <bantime> seconds.\n\n
              Regards,\n
              Fail2Ban" | /usr/sbin/sendmail -f <sender> <dest>

[Init]

# Default name of the chain
#
name = default

# Path to the log files which contain relevant lines for the abuser IP
#
logpath = /dev/null

# Number of log lines to include in the email
#
grepopts = -m 1000 -a
EOT
    diff $TFILE /etc/fail2ban/action.d/sendmail-whois-lines.conf &> /dev/null
    if [ $? -ne 0 ]
    then
        cat $TFILE > /etc/fail2ban/action.d/sendmail-whois-lines.conf
        RESTART_F2B=1
    fi
    rm -f $TFILE

    TFILE=$(mktemp /tmp/$$XXXXXXXX)
    cat << EOT > $TFILE
# For details see /etc/fail2ban/jail.conf

[DEFAULT]

destemail = root@btoy1.net  # <<< ADAPT!
sender = fail2ban@btoy1.net # <<< ADAPT!

banaction = iptables-multiport-log

# ban & send an e-mail with whois report and relevant log lines
# to the destemail.
action_mwl = %(banaction)s[name=%(__name__)s, bantime="%(bantime)s", port="%(port)s", protocol="%(protocol)s", chain="%(chain)s"]
            %(mta)s-whois-lines[name=%(__name__)s, sender="%(sender)s", dest="%(destemail)s", logpath=%(logpath)s, chain="%(chain)s", bantime="%(bantime)s"]

[sshd]

enabled = true
port    = ssh
logpath = %(sshd_log)s
backend = %(sshd_backend)s
action = %(action_mwl)s
banaction = iptables-multiport-range-log
bantime = 7200 ; 2 hours
findtime = 180  ; 3 minutes
maxretry = 3 ; 3 occurences

# Ban repeated offenders more harshly
[recidive]

enabled  = true
logpath  = /var/log/fail2ban.log
banaction = %(banaction_allports)s
#bantime  = 604800  ; 1 week
#findtime = 86400   ; 1 day
bantime = 86400  ; 1 day
findtime = 10800   ; 3 hours
maxretry = 5 ; 5 occurences
EOT
    diff $TFILE /etc/fail2ban/jail.local &> /dev/null
    if [ $? -ne 0 ]
    then
        cat $TFILE > /etc/fail2ban/jail.local
        RESTART_F2B=1
    fi
    rm -f $TFILE

    [ -z "$(iptables-save | grep -m 1 :f2b)" ] && RESTART_F2B=1

    # Finally actually restart fail2ban if required
    [ $RESTART_F2B -ne 0 ] && service fail2ban restart
fi

# Force an update if the blocklists are not activated (or hourly)
if [ -z "$(iptables -nvL INPUT | egrep '(C|BL)_(NET|HOST)S')" -o $(date +%-M) -eq 17 ]
then
    for F in ipset iprange
    do
        dpkg-query -l $F &> /dev/null
        [ $? -ne 0 ] && apt-get -f install $F
    done
    UpdateBlocklists-Firehol.sh -L 1 -C cn,ru,vn,kp,ua
fi
