#!/bin/bash
# Version 20240228-155159 checked into repository
################################################################
# (c) Copyright 2017 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/UpdateBlocklists-Fortigate.sh

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE

# Global variables
THISHOST=$(hostname)
[[ $THISHOST == *.* ]] || THISHOST=$(hostname -f)
DEBUG='-q'
[[ $- ==  *x* ]] && DEBUG='-v'

# The level of blocking
# 1 - minimum
# 2 - medium
# 3 - all
BLK_LEVEL=1

TFILE=$(mktemp /tmp/$$XXXXXXXX)

# Get possible program options
while getopts L OPTION
do
    case ${OPTION} in
        L)  BLK_LEVEL=$(printf "%d" $OPTARG 2> /dev/null)
            [ $BLK_LEVEL -lt 1 ] && BLK_LEVEL=1
            [ $BLK_LEVEL -gt 3 ] && BLK_LEVEL=3
            ;;
        *)  echo "Usage: $0 [-L [1|2|3]] (default=$BLK_LEVEL)"
            ;;
    esac
done
shift $((OPTIND - 1))

#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Get IP addresses and network for bad actors
bash UpdateBlocklists.sh -L $BLK_LEVEL -f

# Split the input files into files with 100 entries each
split -l 100 /tmp/AllBlacklistedHosts.txt $TFILE.BH_
split -l 100 /tmp/AllBlacklistedNets.txt $TFILE.BN_

# Create the script for a Fortigate firewall
rm -f /tmp/Fortigate.block-script
GroupNo=0
echo '0' > $TFILE.HostNo
for BHF in $TFILE.BH_*
do
    rm -f $TFILE.BadHosts
    GroupNo=$((GroupNo + 1))
    awk '{print $1" "$2}' $BHF | while read IP COMMENT
    do
        HostNo=$(< $TFILE.HostNo)
        HostNo=$((HostNo + 1))
        echo "$HostNo" > $TFILE.HostNo
        BL="BAD_HOST_"$(printf "%06d" $HostNo)
        echo -n "\"$BL\" " >> $TFILE.BadHosts
        cat << EOT >> /tmp/Fortigate.block-script
config firewall address
delete "$BL"
edit "$BL"
   set subnet $IP 255.255.255.255
   set comment "$COMMENT"
next
end
EOT
    done
    cat << EOT >> /tmp/Fortigate.block-script
config firewall addrgrp
  edit "BAD_HOSTS_$GroupNo"
    set member $(< $TFILE.BadHosts)
    set comment "Bad hosts"
  next
end
EOT
    echo -n "\"BAD_HOSTS_$GroupNo\" " >> $TFILE.AllBadIPs
done

GroupNo=0
echo '0' > $TFILE.NetNo
for BNF in $TFILE.BN_*
do
    rm -f $TFILE.BadNets
    GroupNo=$((GroupNo + 1))

    awk '{print $1" "$2}' $BNF | while read IP COMMENT
    do
        NetNo=$(< $TFILE.NetNo)
        NetNo=$((NetNo + 1))
        echo "$NetNo" > $TFILE.NetNo
        BL="BAD_NET_"$(printf "%06d" $NetNo)
        echo -n "\"$BL\" " >> $TFILE.BadNets
        cat << EOT >> /tmp/Fortigate.block-script
config firewall address
delete "$BL"
edit "$BL"
   set subnet $IP
   set comment "$COMMENT"
next
end
EOT
    done
    cat << EOT >> /tmp/Fortigate.block-script
config firewall addrgrp
  edit "BAD_NETS_$GroupNo"
    set member $(< $TFILE.BadNets)
    set comment "Bad networks"
  next
end
EOT
    echo -n "\"BAD_NETS_$GroupNo\" " >> $TFILE.AllBadIPs
done

# Create the "supergroup" used in the firewall rule
cat << EOT >> /tmp/Fortigate.block-script
config firewall addrgrp
  edit "BAD_IPS"
    set member $(< $TFILE.AllBadIPs)
    set comment "All bad IP addresses"
  next
end

config firewall policy
    edit 998
        set name "Bad IP Addresses inbound"
        set srcintf "any"
        set dstintf "any"
        set srcaddr "BAD_IPS"
        set dstaddr "all"
        set schedule "always"
        set service "ALL"
        set logtraffic all
    next
    edit 999
        set name "Bad IP Addresses outbound"
        set srcintf "any"
        set dstintf "any"
        set srcaddr "all"
        set dstaddr "BAD_IPS"
        set schedule "always"
        set service "ALL"
        set logtraffic all
    next
end
EOT

[ "T$DEBUG" = 'T-v' ] && less /tmp/Fortigate.block-script

# We are done
exit 0
