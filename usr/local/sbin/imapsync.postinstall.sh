#!/bin/bash
################################################################
# (c) Copyright 2018 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/imapsync.postinstall.sh

# This MUST run as "root"
[ $(/usr/bin/id -u) -ne 0 ] && exit 1

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            

#--------------------------------------------------------------------
# Based on https://www.jverdeyen.be/ubuntu/imapsync-on-ubuntu-16.04/

# Get the prerequisites
apt-get install makepasswd rcs perl-doc libio-tee-perl git \
 libmail-imapclient-perl libdigest-md5-file-perl libterm-readkey-perl \
 libfile-copy-recursive-perl build-essential make automake \
 libunicode-string-perl libauthen-ntlm-perl libcrypt-ssleay-perl \
 libdigest-hmac-perl libfile-copy-recursive-perl libio-compress-perl \
 libio-socket-inet6-perl libio-socket-ssl-perl libio-tee-perl \
 libmodule-scandeps-perl libnet-ssleay-perl libpar-packer-perl \
 libreadonly-perl libterm-readkey-perl libtest-pod-perl libtest-simple-perl \
 libunicode-string-perl liburi-perl cpanminus libssl-dev libregexp-common-perl \
 libencode-imaputf7-perl libfile-tail-perl
cpanm Crypt::OpenSSL::RSA JSON::WebToken::Crypt::RSA Sys::MemInfo JSON::WebToken Test::MockObject Unicode::String Data::Uniqid

# Just get the prebuilt script from GitHUB
wget -q --no-check-certificate -O /usr/bin/imapsync \
  https://gitlab.com/bluc/AdminTools/raw/master/usr/bin/imapsync
# Original is at https://raw.githubusercontent.com/imapsync/imapsync/master/imapsync
if [ ! -s /usr/bin/imapsync ]
then
    # Get the GIT source code
    mkdir -p /tmp/imapsync
    cd /tmp/imapsync
    rm -rf *
    git clone http://github.com/imapsync/imapsync.git

    # Compile and install the executable
    cd imapsync
    mkdir -p dist
    make install  # this might fail
fi

# Show what we just installed:
imapsync -v

#--------------------------------------------------------------------
# We are done
exit 0
