#!/bin/bash
# Version 20241014-214828 checked into repository
################################################################
# (c) Copyright 2013 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/MailqWarning.sh

# Set the correct path for commands
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
DEBUG='-q'
[[ $- ==  *x* ]] && DEBUG='-v'

# The script's basename
PROG=${0##*/}

# This host and domain
THISHOST=$(hostname)
[[ $THISHOST == *.* ]] || THISHOST=$(hostname -f)
THISDOMAIN=${THISHOST#*.}

# Default setting for options
M_TOTAL_DEF=1000
M_TOTAL=$M_TOTAL_DEF
M_SINGLE_DEF=500
M_SINGLE=$M_SINGLE_DEF
W_INTERVAL_DEF=300
W_INTERVAL=$W_INTERVAL_DEF
# NOTE: Do not use the following defaults - since the mail queue
#       is usually already full, using the local server might not
#       deliver the warning in the first place!
M_SERVER=''
M_RECIPIENT="root@$THISDOMAIN"

#==============================================================================
# Don't run several instances
PROG=${0##*/}
LOCKFILE=/tmp/${PROG}.lock
if [ -s $LOCKFILE ]
then
        # The file exists so read the PID
        MYPID=$(< $LOCKFILE)
        [ -z "$(ps -p $MYPID | grep $MYPID)" ] || exit 0
fi

# The process is not running (or no lockfile exists)
echo $$ > $LOCKFILE
trap "rm -rf /tmp/$$* $LOCKFILE" EXIT
TFILE=$(mktemp /tmp/$$XXXXXXXX)

# Get the location of postfix's queue directory
QDIR=''
if [ -x /opt/zimbra/postfix/sbin/postconf ]
then
    QDIR=$(/opt/zimbra/postfix/sbin/postconf -h queue_directory 2> /dev/null)
    POSTQUEUE='/opt/zimbra/postfix/sbin/postqueue'
    POSTSUPER='/opt/zimbra/postfix/sbin/postsuper'
elif [ -x /opt/zimbra/common/sbin/postconf ]
then
    QDIR=$(/opt/zimbra/common/sbin/postconf -h queue_directory 2> /dev/null)
    POSTQUEUE='/opt/zimbra/common/sbin/postqueue'
    POSTSUPER='/opt/zimbra/common/sbin/postsuper'
elif [ -x /opt/zimbra/bin/postconf ]
then
    QDIR=$(/opt/zimbra/bin/postconf -h queue_directory 2> /dev/null)
    POSTQUEUE='/opt/zimbra/bin/postqueue'
    POSTSUPER='/opt/zimbra/bin/postsuper'
elif [ -x /opt/zextras/common/sbin/postconf ]
then
    QDIR=$(/opt/zextras/common/sbin/postconf -h queue_directory 2> /dev/null)
    POSTQUEUE='/opt/zextras/common/sbin/postqueue'
    POSTSUPER='/opt/zextras/common/sbin/postsuper'
elif [ -x /usr/sbin/postconf ]
then
    QDIR=$(/usr/sbin/postconf -h queue_directory 2> /dev/null)
    POSTQUEUE='/usr/sbin/postqueue'
    POSTSUPER='/usr/sbin/postsuper'
else
    echo 'ERROR: Not a postfix server'
    exit 1
fi

if [ ! -x /usr/bin/sendemail ]
then
    cat << EOT
ERROR: Can not find 'sendemail' executable
       You can get it using one of the other method below:
       1. via 'apt-get install sendemail'
       2. from http://caspian.dotconf.net/menu/Software/SendEmail/sendEmail-v1.56.tar.gz
EOT
    exit 1
fi

# Get possible program options
FORCE_ALERT=0
while getopts hs:t:C:c:i:f OPTION
do
    case ${OPTION} in
    s)  M_SERVER="$OPTARG"
        ;;
    t)  M_RECIPIENT="$OPTARG"
        ;;
    c)  if [[ $OPTARG != *[!0-9]* ]]
        then
            M_SINGLE=$OPTARG
        else
            echo "-c '$OPTARG' is not an integer - using default '$M_SINGLE_DEF'"
            M_SINGLE=$M_SINGLE_DEF
        fi
        if [ $M_SINGLE -le 0 ]
        then
            echo "-c '$OPTARG' is '0' - using default '$M_SINGLE_DEF'"
            M_SINGLE=$M_SINGLE_DEF
        fi
        ;;
    C)  if [[ $OPTARG != *[!0-9]* ]]
        then
            M_TOTAL=$OPTARG
        else
            echo "-C '$OPTARG' is not an integer - using default '$M_TOTAL_DEF'"
            M_TOTAL=$M_TOTAL_DEF
        fi
        if [ $M_TOTAL -le 0 ]
        then
            echo "-C '$OPTARG' is '0' - using default '$M_TOTAL_DEF'"
            M_TOTAL=$M_TOTAL_DEF
        fi
        ;;
    i)  if [[ $OPTARG != *[!0-9]* ]]
        then
            W_INTERVAL=$OPTARG
        else
            echo "-i '$OPTARG' is not an integer - using default '$W_INTERVAL_DEF'"
            W_INTERVAL=$W_INTERVAL_DEF
        fi
        if [ $W_INTERVAL -le 0 ]
        then
            echo "-i '$OPTARG' is '0' - using default '$W_INTERVAL_DEF'"
            W_INTERVAL=$W_INTERVAL_DEF
        fi
        ;;
    f)  FORCE_ALERT=1
        ;;
    *)  cat << EOT
Usage: $PROG options
       -s server  Specify the mail server to use [default=MX hosts for recipient's domain]
       -t email   Specify the email recipient [default=$M_RECIPIENT]
       -C number  Specify the total threshold for ALL queues [default=$M_TOTAL_DEF]
       -c number  Specify the threshold for any single queue [default=$M_SINGLE_DEF]
       -i number  Specify the warning interval in seconds [default=$W_INTERVAL_DEF]
       -f         Force alert regardless of thresholds
EOT
        exit 0
        ;;
    esac
done
shift $((OPTIND - 1))

# Don't run several instances
LOCKFILE=/tmp/${PROG}.lock
if [ -s $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    if [ ! -z "$(ps -p $MYPID | grep -m 1 $MYPID)" ]
    then
        # The old process still runs - find out how long
        OLDPID_START=$(date +%s -r $LOCKFILE)
        if [ $((EPOCHSECONDS - 7200)) -gt $OLDPID_START ]
        then
            # The old process runs for at least 2 hours - kill it
            kill $MYPID
            # And try again later
            exit 1
        fi
    fi
fi

if [ ! -z "$(ps -elf | awk '/amavisd/ && !/master/{if ($5 == 1) {print $0}}')" ]
then
    # Kill any orphaned amavis processes and flush mail queue
    kill $(ps -elf | awk '/amavisd/ && !/master/{if ($5 == 1) {print $5}}')
    $POSTQUEUE -f 2> /dev/null
fi

# The log file for "postsuper -h" actions
PS_LOG=/tmp/PS.txt
rm -f /tmp/PS.txt

function get_mail_server ()
{
    if [ -z "$M_SERVER" ]
    then
        # No mail server specified - determine the MX host(s) for the email recipient
        USE_DIG=0
        [ -x /usr/bin/dig ] && USE_DIG=1
        TRY=0
        while [ $TRY -lt 3 ]
        do
            # Try the nameservers in /etc/resolv.conf first
            awk '/^nameserver/{print $NF}' /etc/resolv.conf | while read -r NS
            do
                if [ $USE_DIG -eq 0 ]
                then
                    M_RECIP_MX=$(host -t mx ${M_RECIPIENT##*@} @$NS | awk '/handled/ {sub(/\.$/,"",$NF);printf("%s ",$NF)}END{printf("\n")}')
                else
                    M_RECIP_MX=$(dig @$NS +short mx ${M_RECIPIENT##*@} | sort -n | awk '/^[0-9]/{print $NF; exit}')
                fi
                if [ -n "$M_RECIP_MX" ]
                then
                    echo "$M_RECIP_MX" > /tmp/$$.m_recip_mx
                    break
                fi
            done
            [ -s /tmp/$$.m_recip_mx ] && break
            TRY=$((TRY + 1))
        done
        M_RECIP_MX=$(< /tmp/$$.m_recip_mx)
        if [ -z "$M_RECIP_MX" ]
        then
            # Try one more time using a public DNS resolver
            NS='8.8.8.8'
            if [ $USE_DIG -eq 0 ]
            then
                M_RECIP_MX=$(host -t mx ${M_RECIPIENT##*@} @$NS | awk '/handled/ {sub(/\.$/,"",$NF);printf("%s ",$NF)}END{printf("\n")}')
            else
                M_RECIP_MX=$(dig @$NS +short mx ${M_RECIPIENT##*@} | sort -n | awk '/^[0-9]/{print $NF; exit}')
            fi
            [ -n "$M_RECIP_MX" ] && return
            if [ -z "$M_RECIP_MX" ]
            then
                echo "$PROG ERROR: No MX host found for '$M_RECIPIENT', please use the '-s' option"
                exit 1
            fi
        fi
        # Use the first (or only) host in the list
        M_SERVER="${M_RECIP_MX%% *}"
    fi

    # Determine the port for mail server
    M_SERVER_PORT=':25'
    if [ "T$M_SERVER" = 'Tlocalhost' ]
    then
        [[ $QDIR =~ '/opt' ]] && M_SERVER_PORT=':10025'
    fi
}

# Get the sizes of the relevant queues
Q_ACTIVE=$(find ${QDIR}/active -type f | wc -l)
Q_INCOMING=$(find ${QDIR}/incoming -type f | wc -l)
Q_DEFERRED=$(find ${QDIR}/deferred -type f | wc -l)
Q_MAILDROP=$(find ${QDIR}/maildrop -type f | wc -l)
Q_TOTAL=$((Q_ACTIVE + Q_INCOMING + Q_DEFERRED + Q_MAILDROP))

# Log the queue sizes
logger --id=$$ -p mail.info -t $PROG -- TOTAL=$Q_TOTAL MAILDROP=$Q_MAILDROP DEFERRED=$Q_DEFERRED INCOMING=$Q_INCOMING ACTIVE=$Q_ACTIVE

# In "FORCE_ALERT" mode reset ALL "send last" indicators
[ $FORCE_ALERT -ne 0 ] && rm -f /var/tmp/MailqWarning.*.last

ORPHANED_AMAVIS=$(ps -elf | awk '/amavisd/{if ($5 == 1) {print $0}}'| wc -l)

# Send warning for total queue size
if [ $Q_TOTAL -ge $M_TOTAL ] || [ $FORCE_ALERT -ne 0 ]
then
    logger --id=$$ -p mail.err -t $PROG -- Large TOTAL mail queue $Q_TOTAL
    $POSTQUEUE -p 2> /dev/null | awk '/^[A-Z0-9]/ && $2 > 10000{gsub(/[*!]/,"");print $1}' | $POSTSUPER -h - &>> $PS_LOG
    sed -i '/ 0 messages/d' $PS_LOG

    LAST_WARNING=0
    NOW=$EPOCHSECONDS
    if [ -s /tmp/MailqWarning.TOTAL.last ]
    then
        # When was the last warning sent?
        LAST_WARNING=$(date +%s -r /tmp/MailqWarning.TOTAL.last)
    fi
    if [ $((NOW - LAST_WARNING)) -gt $W_INTERVAL ]
    then
        cat << EOT > $TFILE
TOTAL mail queue size = $Q_TOTAL

Top 10 email senders:
EOT
        cleanq | awk '{count[$7]++} END {for (word in count) print count[word], word}' | sort -rn | head >> $TFILE

        if [ $ORPHANED_AMAVIS -gt 1 ]
        then
            cat << EOT >> $TFILE

The root cause is likely a "stuck" amavis process.  To fix it,
run this command sequence:
for P in \$(ps -wefH| awk '/amavis[d]/{print \$2}')
do
     kill -9 \$P
done
service amavis restart
EOT
        fi

        if [ $ORPHANED_AMAVIS -gt 1 ] || [ $Q_TOTAL -ge 500 ]
        then
            X_PRIO='1'
            IMPORTANCE='High'
            X_MS_PRIO='High'
        else
            X_PRIO='3'
            IMPORTANCE='Normal'
            X_MS_PRIO='Normal'
        fi

        # Last warning was more than 5 minutes ago
        #  - try up to 3 times to send email using
        #    the MX host(s) or specified mail server(s)
        TRY=0
        SENT_EMAIL=1
        get_mail_server
        while [ $SENT_EMAIL -ne 0 ] && [ $TRY -lt 3 ]
        do
            for MS in $M_SERVER
            do
                sendemail -f root@$THISDOMAIN -t $M_RECIPIENT $DEBUG \
                  -o message-header="X-Priority: ${X_PRIO}" \
                  -o message-header="Importance: ${IMPORTANCE}" \
                  -o message-header="X-MS-Priority: ${X_MS_PRIO}" \
                  -u "$THISHOST: Large ($Q_TOTAL) TOTAL mail queue" -s ${MS}${M_SERVER_PORT} < $TFILE
                if [ $? -eq 0 ]
                then
                    # Email was sent successfully
                    SENT_EMAIL=0
                    break
                fi
            done
            # Try again unless the email was successfully sent
            [ $SENT_EMAIL -ne 0 ] && TRY=$((TRY + 1))
        done
        [ $SENT_EMAIL -eq 0 ] && date > /tmp/MailqWarning.TOTAL.last
    fi
elif [ $Q_TOTAL -ge $((M_TOTAL / 2)) ]
then
    logger --id=$$ -p mail.err -t $PROG -- Large TOTAL mail queue $Q_TOTAL
    if [ ! -z "$($POSTQUEUE -p 2> /dev/null | grep -m 1 -oP '^[A-Z0-9]+!\s+\d+')" ]
    then
        $POSTQUEUE -p 2> /dev/null| awk '/^[A-Z0-9]/ && $2 < 50001{gsub(/[*!]/,"");print $1}' | $POSTSUPER -H - &>> $PS_LOG
        sed -i '/ 0 messages/d' $PS_LOG
        $POSTQUEUE -f 2> /dev/null
    fi
    $POSTQUEUE -p 2> /dev/null | awk '/^[A-Z0-9]/ && $2 > 50000{gsub(/[*!]/,"");print $1}' | $POSTSUPER -h - &>> $PS_LOG
    sed -i '/ 0 messages/d' $PS_LOG
else
    # Remove the "last warning" file
    rm -f /tmp/MailqWarning.TOTAL.last
    if [ ! -z "$($POSTQUEUE -p 2> /dev/null | grep -m 1 -oP '^[A-Z0-9]+!\s+\d+')" ]
    then
        $POSTSUPER -H ALL &>> $PS_LOG
        sed -i '/ 0 messages/d' $PS_LOG
        $POSTQUEUE -f 2> /dev/null
    fi
    [ -z "$($POSTQUEUE -p 2> /dev/null | grep -Eam 1 '(Temporary MTA failure on relaying|lost connection with 127\.0\.0\.1)')" ] || $POSTQUEUE -f 2> /dev/null
fi
# Show the logs from "postsuper -h" actions
[ -s $PS_LOG ] && sed -i '/overriding earlier entry/d' $PS_LOG
[ -s $PS_LOG ] && cat $PS_LOG

# Send warnings for single queues
if [ $Q_ACTIVE -ge $M_SINGLE ] || [ $FORCE_ALERT -ne 0 ]
then
    logger --id=$$ -p mail.err -t $PROG -- Large ACTIVE mail queue $Q_ACTIVE

    LAST_WARNING=0
    NOW=$EPOCHSECONDS
    if [ -s /tmp/MailqWarning.ACTIVE.last ]
    then
        # When was the last warning sent?
        LAST_WARNING=$(date +%s -r /tmp/MailqWarning.ACTIVE.last)
    fi
    if [ $((NOW - LAST_WARNING)) -gt $W_INTERVAL ]
    then
        cat << EOT > $TFILE
ACTIVE mail queue size = $Q_ACTIVE

Top 10 email senders:
EOT
        cleanq | awk '{count[$7]++} END {for (word in count) print count[word], word}' | sort -rn | head >> $TFILE

        if [ $ORPHANED_AMAVIS -gt 1 ]
        then
            cat << EOT >> $TFILE

The root cause is likely a "stuck" amavis process.  To fix it,
run this command sequence:
for P in \$(ps -wefH| awk '/amavis[d]/{print \$2}')
do
     kill -9 \$F
done
service amavis restart
EOT
        fi
        # Last warning was more than 5 minutes ago
        #  - try up to 3 times to send email using
        #    the MX host(s) or specified mail server(s)
        TRY=0
        SENT_EMAIL=1
        get_mail_server
        while [ $SENT_EMAIL -ne 0 ] && [ $TRY -lt 3 ]
        do
            for MS in $M_SERVER
            do
                sendemail -f root@$THISDOMAIN -t $M_RECIPIENT $DEBUG \
                  -u "$THISHOST: Large ($Q_ACTIVE) ACTIVE mail queue" -s ${MS}${M_SERVER_PORT} < $TFILE
                if [ $? -eq 0 ]
                then
                    # Email was sent successfully
                    SENT_EMAIL=0
                    break
                fi
            done
            # Try again unless the email was successfully sent
            [ $SENT_EMAIL -ne 0 ] && TRY=$((TRY + 1))
        done
        [ $SENT_EMAIL -eq 0 ] && date > /tmp/MailqWarning.ACTIVE.last
    fi
else
    # Remove the "last warning" file
    rm -f /tmp/MailqWarning.ACTIVE.last
fi
if [ $Q_DEFERRED -ge $M_SINGLE ] || [ $FORCE_ALERT -ne 0  ]
then
    LAST_WARNING=0
    NOW=$EPOCHSECONDS
    if [ -s /tmp/MailqWarning.DEFERRED.last ]
    then
        # When was the last warning sent?
        LAST_WARNING=$(date +%s -r /tmp/MailqWarning.DEFERRED.last)
    fi
    if [ $((NOW - LAST_WARNING)) -gt $W_INTERVAL ]
    then
        cat << EOT > $TFILE
DEFERRED mail queue size = $Q_DEFERRED

Top 10 email senders:
EOT
        cleanq | awk '{count[$7]++} END {for (word in count) print count[word], word}' | sort -rn | head >> $TFILE

        if [ $ORPHANED_AMAVIS -gt 1 ]
        then
            cat << EOT >> $TFILE

The root cause is likely a "stuck" amavis process.  To fix it,
run this command sequence:
for P in \$(ps -wefH| awk '/amavis[d]/{print \$2}')
do
     kill -9 \$F
done
service amavis restart
EOT
        fi
        # Last warning was more than 5 minutes ago
        #  - try up to 3 times to send email using
        #    the MX host(s) or specified mail server(s)
        TRY=0
        SENT_EMAIL=1
        get_mail_server
        while [ $SENT_EMAIL -ne 0 ] && [ $TRY -lt 3 ]
        do
            for MS in $M_SERVER
            do
                sendemail -f root@$THISDOMAIN -t $M_RECIPIENT $DEBUG \
                  -u "$THISHOST: Large ($Q_DEFERRED) DEFERRED mail queue" -s ${MS}${M_SERVER_PORT} < $TFILE
                if [ $? -eq 0 ]
                then
                    # Email was sent successfully
                    SENT_EMAIL=0
                    break
                fi
            done
            # Try again unless the email was successfully sent
            [ $SENT_EMAIL -ne 0 ] && TRY=$((TRY + 1))
        done
        [ $SENT_EMAIL -eq 0 ] && date > /tmp/MailqWarning.DEFERRED.last
    fi
else
    # Remove the "last warning" file
    rm -f /tmp/MailqWarning.DEFERRED.last
fi
if [ $Q_INCOMING -ge $M_SINGLE ] || [ $FORCE_ALERT -ne 0  ]
then
    LAST_WARNING=0
    NOW=$EPOCHSECONDS
    if [ -s /tmp/MailqWarning.INCOMING.last ]
    then
        # When was the last warning sent?
        LAST_WARNING=$(date +%s -r /tmp/MailqWarning.INCOMING.last)
    fi
    if [ $((NOW - LAST_WARNING)) -gt $W_INTERVAL ]
    then
        cat << EOT > $TFILE
INCOMING mail queue size = $Q_INCOMING

Top 10 email senders:
EOT
        cleanq | awk '{count[$7]++} END {for (word in count) print count[word], word}' | sort -rn | head >> $TFILE

        if [ $ORPHANED_AMAVIS -gt 1 ]
        then
            cat << EOT >> $TFILE

The root cause is likely a "stuck" amavis process.  To fix it,
run this command sequence:
for P in \$(ps -wefH| awk '/amavis[d]/{print \$2}')
do
     kill -9 \$F
done
service amavis restart
EOT
        fi
        # Last warning was more than 5 minutes ago
        #  - try up to 3 times to send email using
        #    the MX host(s) or specified mail server(s)
        TRY=0
        SENT_EMAIL=1
        get_mail_server
        while [ $SENT_EMAIL -ne 0 ] && [ $TRY -lt 3 ]
        do
            for MS in $M_SERVER
            do
                sendemail -f root@$THISDOMAIN -t $M_RECIPIENT $DEBUG \
                  -u "$THISHOST: Large ($Q_INCOMING) INCOMING mail queue" -s ${MS}${M_SERVER_PORT} < $TFILE
                if [ $? -eq 0 ]
                then
                    # Email was sent successfully
                    SENT_EMAIL=0
                    break
                fi
            done
            # Try again unless the email was successfully sent
            [ $SENT_EMAIL -ne 0 ] && TRY=$((TRY + 1))
        done
        [ $SENT_EMAIL -eq 0 ] && date > /tmp/MailqWarning.INCOMING.last
    fi
else
    # Remove the "last warning" file
    rm -f /tmp/MailqWarning.INCOMING.last
fi
if [ $Q_MAILDROP -ge $M_SINGLE ] || [ $FORCE_ALERT -ne 0  ]
then
    LAST_WARNING=0
    NOW=$EPOCHSECONDS
    if [ -s /tmp/MailqWarning.MAILDROP.last ]
    then
        # When was the last warning sent?
        LAST_WARNING=$(date +%s -r /tmp/MailqWarning.MAILDROP.last)
    fi
    if [ $((NOW - LAST_WARNING)) -gt $W_INTERVAL ]
    then
        cat << EOT > $TFILE
MAILDROP mail queue size = $Q_MAILDROP

Top 10 email senders:
EOT
        cleanq | awk '{count[$7]++} END {for (word in count) print count[word], word}' | sort -rn | head >> $TFILE

        if [ $ORPHANED_AMAVIS -gt 1 ]
        then
            cat << EOT >> $TFILE

The root cause is likely a "stuck" amavis process.  To fix it,
run this command sequence:
for P in \$(ps -wefH| awk '/amavis[d]/{print \$2}')
do
     kill -9 \$F
done
service amavis restart
EOT
        fi
        # Last warning was more than 5 minutes ago
        #  - try up to 3 times to send email using
        #    the MX host(s) or specified mail server(s)
        TRY=0
        SENT_EMAIL=1
        get_mail_server
        while [ $SENT_EMAIL -ne 0 ] && [ $TRY -lt 3 ]
        do
            for MS in $M_SERVER
            do
                sendemail -f root@$THISDOMAIN -t $M_RECIPIENT $DEBUG \
                  -u "$THISHOST: Large ($Q_MAILDROP) MAILDROP mail queue" -s ${MS}${M_SERVER_PORT} < $TFILE
                if [ $? -eq 0 ]
                then
                    # Email was sent successfully
                    SENT_EMAIL=0
                    break
                fi
            done
            # Try again unless the email was successfully sent
            [ $SENT_EMAIL -ne 0 ] && TRY=$((TRY + 1))
        done
        [ $SENT_EMAIL -eq 0 ] && date > /tmp/MailqWarning.MAILDROP.last
    fi
else
    # Remove the "last warning" file
    rm -f /tmp/MailqWarning.MAILDROP.last
fi

# We are done
exit 0
