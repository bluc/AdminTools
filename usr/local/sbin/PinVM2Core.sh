#!/bin/bash
# Version 20230405-090314 checked into repository
################################################################
# (c) Copyright 2013-2023 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/PinVM2Core.sh

# This works for Proxmox only
# See https://pve.proxmox.com/pve-docs/pve-admin-guide.html#_hookscripts

# This only works for Proxmox VE
[ -d /etc/pve/local ] || exit 1

# Create the directory for the hook script
mkdir -p /var/lib/vz/snippets

# Create the hook script
cat << EOHS > /var/lib/vz/snippets/cpuset
#!/usr/bin/perl

# Example hook script for PVE guests (hookscript config option)
# derived from /usr/share/pve-docs/examples/guest-example-hookscript.pl
# You can set this via pct/qm with
# pct set <vmid> -hookscript <volume-id>
# qm set <vmid> -hookscript <volume-id>
# where <volume-id> has to be an executable file in the snippets folder
# of any storage with directories e.g.:
# qm set 100 -hookscript local:snippets/hookscript.pl

use strict;
use warnings;

print "GUEST HOOK: " . join( ' ', @ARGV ) . "\n";

# First argument is the vmid

my \$vmid = shift;

# Second argument is the phase

my \$phase = shift;

if ( \$phase eq 'pre-start' )
{

    # First phase 'pre-start' will be executed before the guest
    # is started. Exiting with a code != 0 will abort the start

    print "VM guest \$vmid is starting, doing preparations.\n";

    # print "preparations failed, aborting."
    # exit(1);

} elsif ( \$phase eq 'post-start' )
{

    # Second phase 'post-start' will be executed after the guest
    # successfully started.

    print "VM guest \$vmid started successfully.\n";

    if ( -x '/usr/bin/numactl' )
    {
        # Pin VM guest to the CPU cores of a specific NUMA node
        my @NumaNode;
        if ( open( my \$NC, '-|', 'numactl --hardware' ) )
        {
            while (<\$NC>)
            {
                chomp;
                if (/node (\d+) cpus: (.+)/o)
                {

                    # Remember the CPUs for this node
                    my \$NodeNumber = \$1;
                    my \$NodeCPUs   = "\$2";
                    \$NodeCPUs =~ s/\s/,/g;
                    \$NumaNode[\$NodeNumber] = "\$NodeCPUs";
                    print "=> node \$NodeNumber has CPUs '\$NodeCPUs'\n";
                } ## end if (/node (\d+) cpus: (.+)/o...)
            } ## end while (<\$NC>)
            close(\$NC);
            if ( scalar @NumaNode )
            {
                my \$PIDFile = '/run/qemu-server/' . \$vmid . '.pid';
                print "=> getting PID for VM guest \$vmid from \$PIDFile\n";
                if ( open( my \$VID, '<', "\$PIDFile" ) )
                {
                    my \$vmidPID = <\$VID>;
                    chomp(\$vmidPID);
                    close(\$VID);
                    print '=> Found '
                        . ( scalar @NumaNode )
                        . " NUMA nodes \n";
                    my \$CPU = 0;

                    if ( ( scalar @NumaNode ) > 1 )
                    {

                        # Multiple CPUs - determine the NUMA node from the
                        #  list of VMs defined on this server
                        if ( open( my \$QL, '-|', 'qm list' ) )
                        {
                            my \$QMNo = 0;
                            while (<\$QL>)
                            {
                                if (/^\s+(\$vmid)\s/o)
                                {

                                    # We found the VM in the list
                                    \$CPU = ( \$QMNo % ( scalar @NumaNode ) );
                                    last;
                                } ## end if (/^\s+(\$vmid)\s/o)
                                \$QMNo++;
                            } ## end while (<\$QL>)
                            close(\$QL);
                        } ## end if ( open( my \$QL, '-|'...))

                        # Finally pin the VM guest
                        print
                            "=> pinning VM guest '\$vmid' to NUMA node \$CPU\n";
                        system "taskset -apc \$NumaNode[\$CPU] \$vmidPID";
                    } ## end if ( ( scalar @NumaNode...))
                } ## end if ( open( my \$VID, '<'...))
            } ## end if ( scalar @NumaNode ...)
        } ## end if ( open( my \$NC, '-|'...))
    } ## end if ( -x '/usr/bin/numactl'...)
} elsif ( \$phase eq 'pre-stop' )
{

    # Third phase 'pre-stop' will be executed before stopping the guest
    # via the API. Will not be executed if the guest is stopped from
    # within e.g., with a 'poweroff'

    print "VM guest \$vmid will be stopped.\n";

} elsif ( \$phase eq 'post-stop' )
{

    # Last phase 'post-stop' will be executed after the guest stopped.
    # This should even be executed in case the guest crashes or stopped
    # unexpectedly.

    print "VM guest \$vmid stopped. Doing cleanup.\n";

} else
{
    die "got unknown phase '\$phase'\n";
} ## end else [ if ( \$phase eq 'pre-start'...)]

exit(0);
__END__
EOHS

# Make the script executable
chmod +x /var/lib/vz/snippets/cpuset

# Assign it to all defined virtual guests
for F in /etc/pve/local/qemu-server/*conf
do
    [ -n "$(grep ^hookscript $F)" ] && continue
    VMID=${F%.conf}
    VMID=${VMID##*/}
    qm set $VMID --hookscript local:snippets/cpuset
done

# We are done
exit 0
