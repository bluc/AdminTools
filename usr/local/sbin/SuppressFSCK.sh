#!/bin/bash
# Version 20240112-191429 checked into repository
#--------------------------------------------------------------------
# (c) CopyRight 2017 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE

#--------------------------------------------------------------------
# Suppress any file system check on ext[234] file systems
awk '/ext[234]/{print $1}' /proc/mounts | while read -r FS
do
    # Show the current state
    echo "Current state of '$FS':"
    tune2fs -l "$FS" | grep -Ea '^(Last checked|Mount count)'
    echo
    tune2fs -C 1 -T now "$FS"
done

#--------------------------------------------------------------------
# We are done
exit 0
