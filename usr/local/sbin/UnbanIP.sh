#!/bin/bash
# Version 20240402-141820 checked into repository
################################################################
# (c) Copyright 2018 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/UnbanIP.sh

# Bail if fail2ban is not installed
[ -s /etc/fail2ban/fail2ban.conf ] || exit 0

# This must run as "root"
if [ $(/usr/bin/id -u) -ne 0 ]
then
    echo 'You must be root to continue'
    exit 0
fi

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin

# Remove temp files at exit
trap "rm -f /tmp/$$*" EXIT

#--------------------------------------------------------------------
# Options
JAIL_LIST=''
UNBAN_IP=''
VERBOSE=0

# Get possible options
while getopts hvj:i: OPTION
do
    case ${OPTION} in
        j)  # Use specified jail
            JAIL_LIST="$OPTARG"
            ;;
        i)  # Use the specified IP address
            UNBAN_IP=$OPTARG
            ;;
        v)  VERBOSE=1
            ;;
        h|\?)
            echo "$0 [-j|-i|v] [backup_directory]"
            echo '  -j jail  Unban IP from "jail" [default=all]'
            echo '  -i IP    Unbank this IP address [default=none]'
            echo '  -v       Show what is being unbanned where'
            exit 1
            ;;
    esac
done
shift $((OPTIND - 1))

#--------------------------------------------------------------------
if [ -z "$JAIL_LIST" ]
then
    # Get the list of all started jails
    JAIL_LIST=$(fail2ban-client status | awk -F: '/Jail list/{gsub(/,/,"");gsub(/^[ \t]+/,"",$2);print $2}')
fi

#--------------------------------------------------------------------
if [ -z "$UNBAN_IP" ]
then
    read -rp 'IP to unban ? ' UNBAN_IP
fi
[ -z "$UNBAN_IP" ] && exit 2

#--------------------------------------------------------------------
# Actually unban the IP address
for JAIL in $JAIL_LIST
do
    [ $VERBOSE -ne 0 ] && date "+%F %T: Unbanning $UNBAN_IP from jail '$JAIL'"
    fail2ban-client set $JAIL unbanip $UNBAN_IP &> /tmp/$$.unban.txt
    if [[ $(< /tmp/$$.unban.txt) =~ 'is not banned' ]]
    then
        date "+%F %T: IP address $UNBAN_IP is not banned by $JAIL"
    else
        date "+%F %T: IP address $UNBAN_IP is now unbanned from $JAIL"
    fi
done

#--------------------------------------------------------------------
# Also remove the IP address from any "recent" netfilter
grep "$UNBAN_IP" /proc/net/xt_recent/* | while read -r F
do
    XTF=${F%%:*}
    [ -f $XTF ] || continue
    echo "Exempting $UNBAN_IP for denial-of-service filter ${XTF##*/}"
    echo -$UNBAN_IP > $XTF
done

#--------------------------------------------------------------------
# We are done
exit 0
