#!/bin/bash
#--------------------------------------------------------------------
# (c) CopyRight 2015 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#--------------------------------------------------------------------

#--------------------------------------------------------------------
# Set a sensible path for executables which includes Zimbra
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/opt/zimbra/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -rf $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE

# Specifying "-x" to the bash invocation = DEBUG
DEBUG=''
[[ $- ==  *x* ]] && DEBUG='-v'

# This host and domain
THISHOST=$(hostname)
[[ $THISHOST == *.* ]] || THISHOST=$(hostname -f)
THISDOMAIN=${THISHOST#*.}
ZIMBRA_HOSTNAME=$(zmhostname)
[ -z "$ZIMBRA_HOSTNAME" ] && ZIMBRA_HOSTNAME=$(zmprov gas | head -n 1)

# Current hour and minute
set $(date '+%-H %-M')
HR_NOW=$1
MIN_NOW=$2

# Get the zimbra version
set $(su - zimbra -c 'zmcontrol -v' | awk -F. '{gsub(/Release /,"");print $1" "$2" "$3}')
ZIMBRA_VERS_MAJ=$1
ZIMBRA_VERS_MIN=$2
ZIMBRA_VERS_MIN2=$3

#-------------------------------------------------------------------------
# Determine the external IP for this host (at most once an hour)
#  This is needed to determine whether we are in a mirrored environment or
#  not
if [ ! -s /tmp/RemoteIP -o $MIN_NOW -eq 3 -o "T$DEBUG" = 'T-v' ]
then
    if [ "T$DEBUG" = 'T-v' -o $(($HR_NOW % 6)) -eq 2 ]
    then
        # Find any and all emails that are held up by GMail
        PFQ=$(postconf -hf queue_directory)
        TDIR=$(mktemp -d)
        cleanq | egrep '(he email account that you tried to reach is over quota|This message does not have authentication information or fails to pass)' > /tmp/$$
        if [ -s $TDIR/MailList ]
        then
            for MSG in $(cut -d ' ' -f1 $TDIR/MailList)
            do
                [ -f $PFQ/deferred/${MSG:0:1}/$MSG ] || continue
                postcat $PFQ/deferred/${MSG:0:1}/$MSG | sed -n '/ENVELOPE/,/^ *$/p' > $TDIR/header.$MSG.txt
            done
            cd $TDIR
            zip headers.zip header.*txt
            EH= $(sed -n '=' MailList)
            sendemail -q -f tbulli@${THISDOMAIN} -o tls=no \
              -a headers.zip \
              -t ateam@${THISDOMAIN} -s localhost -u "$EH email(s) held up by GMail" < MailList
        fi
        rm -rf $TDIR
    fi

    # Get the external IP for this host
    # wget -q -O - -U DTP-appliance http://consult.btoy1.net/whatsmyip.php |
    wget -q '--header=Host: consult.btoy1.net' --no-check-certificate -O - -U DTP-appliance http://172.104.211.41/whatsmyip.php | \
      awk '/IP/{print $NF}' > /tmp/RemoteIP

    # Adapt system parameters for better network throughput
    [ "T$(sysctl -n net.ipv4.tcp_rmem)" = 'T8192	262144	16777216' ] || sysctl -q -w 'net.ipv4.tcp_rmem=8192 262144 16777216'
    [ "T$(sysctl -n net.core.rmem_max)" = 'T16777216' ] || sysctl -q -w 'net.core.rmem_max=16777216'
    [ "T$(sysctl -n net.core.rmem_default)" = 'T262144' ] || sysctl -q -w 'net.core.rmem_default=262144'

    # Restart host firewall if necessary
    if [[ $(iptables -nL INPUT | head -n 1) =~ ACCEPT ]]
    then
        logger -p info -t kernel --id=$$ -- Restarting host firewall
        firehol start
    fi

    # Update the local firewall's block lists
    #  banning traffic to/from China, Russia, Vietnam, North Korea, Ukraine and Netherlands
    UpdateBlocklists-Firehol.sh -L 1 -C cn,ru,vn,kp,ua,nl

    # Create IP list for US
    HOSTS_HASH_SIZE=$((2048 * 1024))
    > /tmp/$$
    for cty in 'us'
    do
        [ -s /var/tmp/countrydb/$cty.txt ] || continue
        cat << EOT >> /tmp/$$

ipset create C_NETS_NEW_$cty hash:net family inet hashsize 16384 maxelem $HOSTS_HASH_SIZE -exist
ipset create C_NETS_$cty hash:net family inet hashsize 16384 maxelem $HOSTS_HASH_SIZE -exist

EOT
        awk '{printf "ipset add C_NETS_NEW_'$cty' %s -exist\n", $1}' /var/tmp/countrydb/$cty.txt >> /tmp/$$
        cat << EOT >> /tmp/$$
# Consider local network IP range as in the USA
ipset add C_NETS_NEW_$cty 192.168.20.0/24 -exist
ipset swap C_NETS_NEW_$cty C_NETS_$cty
if [ \$? -eq 0 ]
then
    ipset destroy C_NETS_NEW_$cty
else
    ipset rename C_NETS_NEW_$cty C_NETS_$cty
fi
ipset save C_NETS_$cty > /var/tmp/C_NETS_$cty.sav
EOT

        # Activate the "login" checks if necessary
        if [ -z "$(iptables -nvL INPUT | grep C_NETS_$cty)" ]
        then
            iptables -I INPUT -p tcp -m multiport --dports 443,465,587,993,995 -m set ! --match-set C_NETS_$cty src -j DROP
            iptables -I INPUT -p tcp -m multiport --dports 443,465,587,993,995 -m set ! --match-set C_NETS_$cty src -j LOG --log-level 5 --log-prefix "Non-US BLOCKED: "
        fi
        if [ -z "$(iptables -nvL OUTPUT | grep C_NETS_$cty)" ]
        then
            iptables -I OUTPUT -p tcp -m multiport --sports 443,465,587,993,995 -m set ! --match-set C_NETS_$cty dst -j DROP
            iptables -I OUTPUT -p tcp -m multiport --dports 443,465,587,993,995 -m set ! --match-set C_NETS_$cty dst -j LOG --log-level 5 --log-prefix "Non-US BLOCKED: "
        fi
    done

    [ -s /tmp/$$ ] && source /tmp/$$
    rm -f /tmp/$$

    iptables-save > /tmp/$$
    for C in $(awk '/^:ACC_REC/{sub(/:/,"",$1);print $1}' /tmp/$$)
    do
        [ -z "$(grep ${C}.*LOG /tmp/$$)" ] || continue
    
        logger -p info -t kernel --id=$$ -- "Adding log rules to chain '$C'"
        CUR_RULE="$(grep ${C}.*update /tmp/$$)"
        RULE_COMMENT=${C//ACC_REC_/}
        NEW_RULE="$(sed 's/-A.*-m recent/-m recent/;s/-j RETURN//' <<< $CUR_RULE)""-j LOG --log-prefix recent_${RULE_COMMENT}: "
        iptables -I $C 3 $NEW_RULE
    done
fi

#-------------------------------------------------------------------------
# Make sure we have some sensible values for wget
sed '/##BTOY1 START/,/##BTOY1 END/d' /etc/wgetrc >> /tmp/$$.wget
cat << EOT >> /tmp/$$.wget
##BTOY1 START
timeout = 120
prefer-family = IPv4
#http_proxy = http://192.168.1.1:3128/
#bind-address = $(ifconfig eth0 | sed -n -e 's/\(.*\)inet addr:\(.*\)B.*$/\2/p')
tries = 5
user_agent = DTP $THISHOST $PROG
##BTOY1 END
EOT
diff -w /tmp/$$.wget /etc/wgetrc &>/dev/null
[ $? -ne 0 ] && cat /tmp/$$.wget > /etc/wgetrc

#--------------------------------------------------------------------
# Adjust local "rkhunter" configuration
cat << EOT > /tmp/$$
# Allow version and update checks
WEB_CMD=wget
MIRRORS_MODE=0
# Set allowed root login
ALLOW_SSH_ROOT_USER=prohibit-password
# Don't allow SSH version 1
ALLOW_SSH_PROT_V1=0
# Exempt some "hidden" directories
ALLOWHIDDENDIR=/etc/.java
ALLOWHIDDENDIR=/dev/.udev
# Exempt some shared memory files
ALLOWDEVFILE=/dev/shm/qb*
ALLOWDEVFILE=/dev/shm/PostgreSQL*
ALLOWDEVFILE=/dev/.udev/rules.d/root.rules
ALLOWDEVFILE=/dev/.initramfs
# Exempt some local scripts (if present)
EOT
[ -s /usr/bin/lwp-request ] && echo 'SCRIPTWHITELIST=/usr/bin/lwp-request' >> /tmp/$$
[ -s /usr/bin/unhide.rb ] && echo 'SCRIPTWHITELIST=/usr/bin/unhide.rb' >> /tmp/$$
echo '# Exempt some Ethernet interfaces (if present)' >> /tmp/$$
for IF in $(cut -d: /proc/net/dev -f1 | grep tap)
do
    echo "ALLOWPROMISCIF=$IF" >> /tmp/$$
done
diff /tmp/$$ /etc/rkhunter.conf.local &> /dev/null
if [ $? -ne 0 ]
then
    chattr -i /etc/rkhunter.conf.local
    cat /tmp/$$ > /etc/rkhunter.conf.local
fi
rm -f /tmp/$$

#--------------------------------------------------------------------
if [ "T$DEBUG" = 'T-v' -o $(($MIN_NOW % 5)) -eq 2 ]
then
    # Actions to be done every 5 minutes

    if [ $(/opt/zimbra/bin/zmprov ga quarantinereport@digitaltowpath.org | egrep -c '(Bypass|Lover|ApplyUser).*TRUE') -lt 5 ]
    then
        # Allow that user to bypass all spam checks
        su - zimbra -c 'zmprov ma quarantinereport@digitaltowpath.org +amavisSpamLover TRUE +amavisBypassSpamChecks TRUE +amavisBannedFilesLover TRUE +amavisBypassVirusChecks TRUE +zimbraSpamApplyUserFilters TRUE'
    fi

    # Increase the entropy for "haveged"
    if [ -z "$(grep '^DAEMON_ARGS.*2048' /etc/default/haveged)" ]
    then
        sed -i 's/-w 1024/-w 2048/' /etc/default/haveged
        service haveged restart
    fi

    # Check whether we have new or changed
    #  or deleted files for bind9 (name server)
    sha256sum /etc/bind/* /etc/bind/named/* /etc/bind/named/custom/* > /tmp/$$.bind9.checksums &>/dev/null
    diff -wu /tmp/$$.bind9.checksums /var/tmp/bind9.checksums &> /dev/null
    if [ $? -ne 0 ]
    then
        # Reload bind9 since we have some changes
        cat /tmp/$$.bind9.checksums > /var/tmp/bind9.checksums
        named-checkconf &> /tmp/$$.bind9.errors
        if [ $? -eq 0 ]
        then
            service bind9 restart
        else
            echo "DNS setup on $THISHOST has some errors:"
            cat /tmp/$$.bind9.errors
        fi
    fi

    # Remove the default mail logging
    if [ ! -z "$(grep ^mail /etc/rsyslog.d/50-default.conf)" ]
    then
        sed -i 's/^mail/#mail/' /etc/rsyslog.d/50-default.conf
        service rsyslog restart
    fi

    # Create the "LOGNDROP" chain if necessary
    if [ $(iptables -S LOGNDROP 2> /dev/null | wc -l) -le 1 ]
    then
        iptables -N LOGNDROP
        iptables -A LOGNDROP -j LOG --log-level 5 --log-prefix "BLOCKED: "
        iptables -A LOGNDROP -j DROP
    fi

    # Update the "DNSReturn" iptables chain
    # (to avoid the DNS return packets to be rejected)
    cat << EOT > /tmp/$$
# Last updated: $(date)
ipset destroy DNSReturn
ipset -exist create DNSReturn hash:ip family inet

EOT
    # Get the list of DNS forwarders from /etc/bind/named.conf.options
    for SRV in $(awk -F{ '/^[[:space:]]*forwarders/ {gsub(/;/,"");gsub(/}/,"");print $2}' /etc/bind/named.conf.options)
    do
        echo "ipset add DNSReturn $SRV -exist" >> /tmp/$$
    done
    # This will take a while, but it runs in the background
    [ -z "$DEBUG" ] || less /tmp/$$
    source /tmp/$$ &> /dev/null

    # Activate the "DNSReturn" checks if necessary
    if [ -z "$(iptables -nvL INPUT | grep DNSReturn)" ]
    then
        iptables -I INPUT -p udp --sport 53 -i eth0 -m set --match-set DNSReturn src -j ACCEPT
    fi

    # Disable MOST of the motd updater
    cd /etc/update-motd.d
    chmod -x *
    # Create our own footer
    cat << EOM > 99-footer
#!/bin/bash
if [ -x /usr/bin/linux_logo ]
then
    OS_LOGO=11
    if [ -s /etc/lsb-release ]
    then
        source /etc/lsb-release
        OS_LOGO=\$(linux_logo -L list | awk '/Ubuntu/{print \$1}' | tail -n 1)
    elif [ -s /etc/debian_version ]
    then
        OS_LOGO=\$(linux_logo -L list | awk '/Debian/{print \$1}' | tail -n 1)
    fi
    linux_logo -ys -L \$OS_LOGO -F "#O Version #V, Compiled #C\n#N #M #T #P, #R RAM\n#H\n"
else
    echo -n 'Linux version '; uname -r
    echo -n 'Compiled '; uname -v
    echo \$(grep -c 'core id' /proc/cpuinfo)\$(awk -F: '/^model name/{print \$2;exit}' /proc/cpuinfo)
    awk '/^MemTotal/{printf "%.2fGB RAM\\\n",\$2/1024/1024}' /proc/meminfo
fi
EOM
    chmod +x 99*

    # LDAP database backup
    OUT_FILE='/tmp/ldap.bak'
    [ -s $OUT_FILE ] && mv -f $OUT_FILE ${OUT_FILE%.bak}.old
    su - zimbra -c '/opt/zimbra/libexec/zmslapcat /tmp' 2> /dev/null
    if [ -s $OUT_FILE ]
    then
        mv $OUT_FILE /var/tmp/ldap.bak.$(date "+%FT%T")
        rm -f /tmp/ldap.bak*
        find /var/tmp -type f -name "ldap.bak.*" \! -size 0 -mmin +20 -delete
    else
        date "+%F %T Error creating LDAP database backup"
    fi

    # Cleanup swap space
#    if [ ! -z "$(grep -m 1 'flags.*hypervisor' /proc/cpuinfo)" ]
#    then
#        SWAP_FREE=$(awk '/^SwapFree/ {print $2}' /proc/meminfo)
#        SWAP_TOTAL=$(awk '/^SwapTotal/ {print $2}' /proc/meminfo)
#       SWAP_USED=$(($SWAP_TOTAL - $SWAP_FREE))
#        if [ $SWAP_USED -gt 0 ]
#        then
#            MEM_FREE=$(awk '/^MemFree/ {print $2}' /proc/meminfo)
#            if [ $SWAP_USED -lt $MEM_FREE ]
#            then
#                logger --id -p info -t kernel -- $PROG Cleaning up swap space
#                swapoff -a && swapon -a
#            fi
#        fi
#    fi
fi

#--------------------------------------------------------------------
if [ "T$DEBUG" = 'T-v' -o $HR_NOW -eq 6 ]
then
    # Actions to be done between 6:00am and 7:00am

    # Create Zimbra account warnings (if not done within the past 24 hours)
    if [ -x /usr/local/sbin/ZimbraAccountWarnings.sh ]
    then
        DO_ZAW=0
        if [ -s /tmp/ZimbraAccountWarnings.last ]
        then
             LASTRUN=$(date +%s -r /tmp/ZimbraAccountWarnings.last)
             [ $(($LASTRUN + 86400)) -lt $(date +%s) ] && DO_ZAW=2
        else
            # Not run at all since the last reboot
            DO_ZAW=1
        fi
    fi
    if [ $DO_ZAW -ne 0 ]
    then
        /usr/local/sbin/ZimbraAccountWarnings.sh
        [ $? -eq 0 ] && date > /tmp/ZimbraAccountWarnings.last
    fi
fi

#--------------------------------------------------------------------
# Force an update if the blocklists are not activated
[ -z "$(iptables -nvL INPUT | egrep '(C|BL)_(NET|HOST)S')" ] && UpdateBlocklists-Firehol.sh -L 1 -C cn,ru,vn,kp,ua,nl

#--------------------------------------------------------------------
RESTART_ZIMBRA=0
if [ "T$DEBUG" = 'T-v' -o $(($MIN_NOW % 3)) -eq 1 ]
then
    # Actions to be done every 3 minutes
    bash $DEBUG MailqWarning.sh -t joe.aiello@me.com,consult@btoy1.net

   #-------------------------------------------------------------------------
    # Check clamd and restart it necessary
    if [ ! -x /usr/local/sbin/CheckZimbraClamd.pl ]
    then
        wget -q -O /usr/local/sbin/CheckZimbraClamd.pl \
          https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/CheckZimbraClamd.pl
        chmod 744 /usr/local/sbin/CheckZimbraClamd.pl          
    fi
    /usr/local/sbin/CheckZimbraClamd.pl

    # Adapt zimbraMtaMyNetworks (if necessary)
    MN='127.0.0.0/8 192.168.20.0/24 [::1]/128 [fe80::]/64'
    if [ "T$(zmprov gs $ZIMBRA_HOSTNAME zimbraMtaMyNetworks | awk '/^zimbraMtaMyNetworks/ {$1="";print}' | sed -e 's/^ //')" != "T$MN" ]
    then
        su - zimbra -c "zmprov ms $ZIMBRA_HOSTNAME zimbraMtaMyNetworks \"$MN\""
    fi

    # Show original connecting IP address in logs
    #  (needed for authentucation failure detection)
    if [ -z "$(zmprov gcf zimbraMailTrustedIP)" ]
    then
        LOCALIF=$(ip route get 8.8.8.8 | awk '/via/{print $5; exit}')
        LOCALIP=$(ifconfig $LOCALIF | sed -n "s/.*inet addr:\([0-9.]*\).*/\1/p")
        [ -z "$LOCALIP" ] && LOCALIP=$(ifconfig $LOCALIF | sed -n "s/.*inet \([0-9.]*\).*/\1/p")
        [ -z "$LOCALIP" ] && LOCALIP=$(ip addr list $LOCALIF | sed -n "s/.*inet \([0-9.]*\).*/\1/p")
        su - zimbra -c "zmprov mcf +zimbraMailTrustedIP 127.0.0.1 +zimbraMailTrustedIP $LOCALIP"
    fi

    # Optimizations based on recommendations at:
    #  https://wiki.zimbra.com/wiki/Performance_Tuning_Guidelines_for_Large_Deployments

    # Allow for more threads for POP3, IMAP, and HTTP(s)
    zmprov gs $ZIMBRA_HOSTNAME > /tmp/$$.zimbra_sysconfig
    if [ -s /tmp/$$.zimbra_sysconfig ]
    then
        POP3_THREADS=$(awk '/zimbraPop3NumThreads/ {print $NF}' /tmp/$$.zimbra_sysconfig)
        [ -z "$POP3_THREADS" ] && POP3_THREADS=0
        if [ $POP3_THREADS -lt 300 ]
        then
            su - zimbra -c "zmprov ms $ZIMBRA_HOSTNAME zimbraPop3NumThreads 300"
            logger --id -p mail.warning -t $PROG "Setting Pop3 Threads to 300"
            # Restart Zimbra for increased POP3 threads to take effect
            RESTART_ZIMBRA=1
        fi
        IMAP_THREADS=$(awk '/zimbraImapNumThreads/ {print $NF}' /tmp/$$.zimbra_sysconfig)
        [ -z "$IMAP_THREADS" ] && IMAP_THREADS=0
        if [ $IMAP_THREADS -lt 500 ]
        then
            su - zimbra -c "zmprov ms $ZIMBRA_HOSTNAME zimbraImapNumThreads 500"
            logger --id -p mail.warning -t $PROG "Setting Imap Threads to 500"
            # Restart Zimbra for increased IMAP3 threads to take effect
            RESTART_ZIMBRA=1
        fi
        IMAP_THREADS=$(awk '/zimbraImapMaxConnections/ {print $NF}' /tmp/$$.zimbra_sysconfig)
        [ -z "$IMAP_THREADS" ] && IMAP_THREADS=0
        if [ $IMAP_THREADS -lt 10000 ]
        then
            su - zimbra -c "zmprov ms $ZIMBRA_HOSTNAME zimbraImapMaxConnections 10000"
            logger --id -p mail.warning -t $PROG "Setting Imap Max Connections to 10000"
            # Restart Zimbra for increased IMAP3 threads to take effect
            RESTART_ZIMBRA=1
        fi

        WEB_THREADS=$(awk '/zimbraHttpNumThreads/ {print $NF}' /tmp/$$.zimbra_sysconfig)
        [ -z "$WEB_THREADS" ] && WEB_THREADS=0
        if [ $WEB_THREADS -lt 500 ]
        then
            su - zimbra -c "zmprov ms $ZIMBRA_HOSTNAME zimbraHttpNumThreads 500"
            logger --id -p mail.warning -t $PROG "Setting web threads to 500"
            # Restart Zimbra for increased web threads to take effect
            RESTART_ZIMBRA=1
        fi

        # Allow for more LMTP threads as well
        LMTP_THREADS=$(awk '/zimbraLmtpNumThreads/ {print $NF}' /tmp/$$.zimbra_sysconfig)
        [ -z "$LMTP_THREADS" ] && LMTP_THREADS=0
        if [ $LMTP_THREADS -lt 50 ]
        then
            su - zimbra -c "zmprov mcf zimbraLmtpNumThreads 50"
            logger --id -p mail.warning -t $PROG "Setting Lmtp Threads to 50"
            # Restart Zimbra for increased LMTP threads to take effect
            RESTART_ZIMBRA=1
        fi
    fi

    # On a 8GB system, set Java heap size percent to 30 and
    #  mysql innodb buffer pool to 25% of system memory:
    RAM=$(awk '/^MemTotal/ {print $2}' /proc/meminfo)
    RAM25P=$(($RAM * 25 / 100))

    zmlocalconfig > /tmp/$$.zimbra_localconfig
    if [ -s /tmp/$$.zimbra_localconfig ]
    then
        JHP=$(awk '/mailboxd_java_heap_size/ {print $NF}' /tmp/$$.zimbra_localconfig)
        if [ $JHP -lt $(($RAM25P / 1024)) ]
        then
            su - zimbra -c 'zmlocalconfig -e mailboxd_java_heap_size='$(($RAM25P / 1024))
            logger --id -p mail.warning -t $PROG "Increasing Java Heap Size from $JHP to "$(($RAM25P / 1024))
            # Restart Zimbra for increased JAVA heap size to take effect
            RESTART_ZIMBRA=1
        fi
        JHP=$(awk '/mailboxd_java_heap_new_size_percent/ {print $NF}' /tmp/$$.zimbra_localconfig)
        if [ $JHP -ne 30 ]
        then
            su - zimbra -c 'zmlocalconfig -e mailboxd_java_heap_new_size_percent=30'
            logger --id -p mail.warning -t $PROG 'Setting Java Heap Percent to 30%'
            # Restart Zimbra for increased JAVA heap percent to take effect
            RESTART_ZIMBRA=1
        fi
        MIBP=$(awk '/mysql_memory_percent/ {print $NF}' /tmp/$$.zimbra_localconfig)
        if [ $MIBP -ne 25 ]
        then
            su - zimbra -c 'zmlocalconfig -e mysql_memory_percent=25'
            logger --id -p mail.warning -t $PROG 'Setting MySQL Memory Percent to 25%'
            # Restart Zimbra for increased MySQL memory percent to take effect
            RESTART_ZIMBRA=1
        fi

        # Disable SSL inter-process communication since we are
        #  within ONE server (or within a trusted network)
        IPC_SSL=$(awk '/zimbra_require_interprocess_security/ {print $NF}' /tmp/$$.zimbra_localconfig)
        if [ $IPC_SSL -ne 0 ]
        then
            su - zimbra -c 'zmlocalconfig -e zimbra_require_interprocess_security=0'
            logger --id -p mail.warning -t $PROG Clearing Interprocess Security
            # Restart Zimbra for clear-text IPC to take effect
            RESTART_ZIMBRA=1
        fi

        # Disable LDAPs since we are within ONE server
        #  (or within a trusted network)
        for P in ldap_starttls_supported ldap_starttls_required
        do
            LDAP_SSL=$(awk '/'$P'/{print $NF}' /tmp/$$.zimbra_localconfig)
            if [ $LDAP_SSL -ne 0 ]
            then
                su - zimbra -c "zmlocalconfig -e '$P=0'"
                logger --id -p mail.warning -t $PROG Disabling LDAP STARTTLS $P
                # Restart Zimbra for clear-text LDAP to take effect
                RESTART_ZIMBRA=1
            fi
        done

        # The default heap size reserved for classes and code is
        #  too small for the Zimbra application
        if [ $ZIMBRA_VERS_MAJ -eq 8 ]
        then
            MJO_OPTIM=''
            if [ $ZIMBRA_VERS_MIN2 -lt 12 ]
            then
                # Only for version < 8.12
                MJO_OPTIM='-server -Djava.awt.headless=true -XX:+UseConcMarkSweepGC -XX:+UseParNewGC -XX:NewRatio=2 -XX:PermSize=196m -XX:MaxPermSize=350m -XX:SoftRefLRUPolicyMSPerMB=1 -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -XX:+PrintGCApplicationStoppedTime -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/opt/zimbra/log -XX:ErrorFile=/opt/zimbra/log/hs_err_pid%p.log -Djavax.net.debug=ssl,handshake'
            elif [ $ZIMBRA_VERS_MIN2 -ge 12 ]
            then
                if [[ ! "$(zmprov gcf zimbraReverseProxySSLProtocols)" =~ TLSv1.3 ]]
                then
                    # Enable TLS 1.3
                    su - zimbra -c 'zmprov mcf +zimbraReverseProxySSLProtocols TLSv1.3'
                    su - zimbra -c 'zmprov mcf zimbraReverseProxySSLCiphers "ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128:AES256:TLS_AES_256_GCM_SHA384:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!MD5:!PSK:!RC4"'
                    logger --id -p mail.warning -t $PROG "Enable TLS 1.3"
                    RESTART_ZIMBRA=1
                fi
                MJO_OPTIM='-server -Djava.awt.headless=true -XX:+UseG1GC -XX:SoftRefLRUPolicyMSPerMB=1 -XX:+UnlockExperimentalVMOptions -XX:G1NewSizePercent=15 -XX:G1MaxNewSizePercent=45 -XX:-OmitStackTraceInFastThrow -verbose:gc -Xlog:gc*=info,safepoint=info:file=/opt/zimbra/log/gc.log:time:filecount=20,filesize=10m -Djavax.net.debug=ssl,handshake -Dhttps.protocols=TLSv1,TLSv1.1,TLSv1.2,TLSv1.3 -Djdk.tls.client.protocols=TLSv1,TLSv1.1,TLSv1.2,TLSv1.3 -Djava.net.preferIPv4Stack=true'
            fi
            if [ ! -z "MJO_OPTIM" ]
            then
                MJO_NOW=$(grep '^mailboxd_java_options =' /tmp/$$.zimbra_localconfig | sed 's/mailboxd_java_options = //')
                if [ "T$MJO_NOW" != "T$MJO_OPTIM" ]
                then
                    su - zimbra -c 'zmlocalconfig -e mailboxd_java_options='"'$MJO_OPTIM'"
                    logger --id -p mail.warning -t $PROG "Setting Java options '$MJO_OPTIM'"
                    # Restart Zimbra for increased JAVA options to take effect
                    RESTART_ZIMBRA=1
                fi
            fi

            # If more than 100 domains are configured, you should adjust ldad_cache_domain_maxsize
            # to the lower of the number of domains you have configured and 30,000.
            # As per https://wiki.zimbra.com/wiki/OpenLDAP_Performance_Tuning_8.0#Mailbox_store_tuning_with_LDAP
            NOD=$(zmprov -l gad | wc -l)
            if [ $NOD -gt 100 ]
            then
                if [ $NOD -gt 30000 ]
                then
                    # Restrict to max. 30,000
                    NOD=30000
                else
                    # Build in a small buffer
                    [ $NOD -lt 30000 ] && NOD=$(($NOD / 10 * 10 + 10))
                fi
                LDAP_NOD=$(awk '/ldap_cache_domain_maxsize/ {print $NF}' /tmp/$$.zimbra_localconfig)
                if [ $LDAP_NOD -lt $NOD ]
                then
                    su - zimbra -c 'zmlocalconfig -e ldap_cache_domain_maxsize='$NOD
                    logger --id -p mail.warning -t $PROG Setting LDAP Cache Size to $NOD
                    # Restart Zimbra for increased LDAP domain cache to take effect
                    RESTART_ZIMBRA=1
                fi
            fi
        fi
    fi

    CUR_INNOPOOL=$(awk '/^innodb_buffer_pool_size/ {print $NF}' /opt/zimbra/conf/my.cnf)
    if [ $CUR_INNOPOOL -ne 6442450944 ]
    then
        sed -i 's/innodb_buffer_pool_size.*/innodb_buffer_pool_size = 6442450944/' /opt/zimbra/conf/my.cnf
        sed -i 's/innodb_log_file_size.*/innodb_log_file_size = 1610612736/'  /opt/zimbra/conf/my.cnf
        rm -f /opt/zimbra/db/data/ib_logfile*
        logger --id -p mail.warning -t $PROG Increasing MySQL InnoDB Buffer Pool Size
        # Restart Zimbra for increased MySQL InnoDB buffer pool to take effect
        RESTART_ZIMBRA=1
    fi


    # Up the MySQL threads and max. client connections
    #  see http://www.sfu.ca/~hillman/zimbra-hied-admins/msg00235.html
    CUR_THREADS=$(awk '/^thread_cache_size/ {print $NF}' /opt/zimbra/conf/my.cnf)
    if [ $CUR_THREADS -lt 150 ]
    then
        sed -i "s/thread_cache_size.*/thread_cache_size = 150/" /opt/zimbra/conf/my.cnf
        logger --id -p mail.warning -t $PROG Setting MySQL Thread Size
        # Restart Zimbra for increased MySQL threads to take effect
        RESTART_ZIMBRA=1
    fi
    CUR_MAXCONN=$(awk '/^max_connections/ {print $NF}' /opt/zimbra/conf/my.cnf)
    if [ $CUR_MAXCONN -lt 220 ]
    then
        sed -i "s/max_connections.*/max_connections = 220/" /opt/zimbra/conf/my.cnf
        logger --id -p mail.warning -t $PROG Setting MySQL Maximum Connections
        # Restart Zimbra for increased MySQL max. connections to take effect
        RESTART_ZIMBRA=1
    fi

    # Create the dhparam.pem file if needed
    if [ ! -s /opt/zimbra/conf/dhparam.pem ]
    then
        su - zimbra -c 'openssl dhparam -out /opt/zimbra/conf/dhparam.pem 2048'
        logger --id -p mail.warning -t $PROG Creating missing /opt/zimbra/conf/dhparam.pem
        RESTART_ZIMBRA=1
    fi

    # See https://forums.zimbra.org/viewtopic.php?f=15&t=21242
    zmprov gacf > /tmp/$$.zimbra.gacf
    if [ -s /tmp/$$.zimbra.gacf ]
    then
        for MS in zimbraMtaMaxMessageSize zimbraFileUploadMaxSize
        do
            if [ $(awk '/'$MS'/{print $NF}') -lt 31457280 ]
            then
                su - zimbra -c "zmprov mcf $MS 31457280"
                logger --id -p mail.warning -t $PROG Increasing $MS to 30MB
                # Restart Zimbra for larger upload sizes to take effect
                RESTART_ZIMBRA=1
            fi
        done
    fi

    # Enhance spam checking
    RESTART_AMAVIS=0

    # Create custom rules for "spamassassin" (the antispam part of amavis)
    cat << EOT > /tmp/sauser.cf

# Enable ONLY if pyzor and razor are installed
# See http://wiki.zimbra.com/wiki/SpamAssassin_Customizations
# apt-get install razor pyzor
# su - zimbra -c 'pyzor --homedir /opt/zimbra/data/amavisd/.pyzor discover'
# su - zimbra -c 'razor-admin -home=/opt/zimbra/data/amavisd/.razor -create'
# su - zimbra -c 'razor-admin -home=/opt/zimbra/data/amavisd/.razor -discover'
# su - zimbra -c 'razor-admin -home=/opt/zimbra/data/amavisd/.razor -register'

# pyzor
use_pyzor 1
pyzor_path /usr/bin/pyzor
# DNS lookups for pyzor can time out easily.  Set the following line IF you want to give pyzor up to 20 seconds to respond
# may slow down email delivery
pyzor_timeout 20

# razor
use_razor2 1

###########################################################################
# Copied from a newer spamassassin configuration, originally
#  located at /usr/share/spamassassin/25_uribl.cf
###########################################################################

# Requires the Mail::SpamAssassin::Plugin::URIDNSBL plugin be loaded.
# Note that this plugin defines a new config setting, 'uridnsbl',
# which lists the zones to look up in advance.  The rules will
# not hit unless each rule has a corresponding 'uridnsbl' line.

ifplugin Mail::SpamAssassin::Plugin::URIDNSBL

###########################################################################
## Spamhaus

uridnssub       URIBL_SBL        zen.spamhaus.org.       A   127.0.0.2
body            URIBL_SBL        eval:check_uridnsbl('URIBL_SBL')
describe        URIBL_SBL        Contains an URL's NS IP listed in the SBL blocklist
tflags          URIBL_SBL        net
reuse           URIBL_SBL

if (version >= 3.004000)
  ifplugin Mail::SpamAssassin::Plugin::URIDNSBL

    uridnsbl        URIBL_SBL_A    sbl.spamhaus.org.   A
    body            URIBL_SBL_A    eval:check_uridnsbl('URIBL_SBL_A')
    describe        URIBL_SBL_A    Contains URL's A record listed in the SBL blocklist
    tflags          URIBL_SBL_A    net a
  endif
endif

# DBL, http://www.spamhaus.org/dbl/
if can(Mail::SpamAssassin::Plugin::URIDNSBL::has_tflags_domains_only)
urirhssub       URIBL_DBL_SPAM   dbl.spamhaus.org.       A   127.0.1.2
body            URIBL_DBL_SPAM   eval:check_uridnsbl('URIBL_DBL_SPAM')
describe        URIBL_DBL_SPAM   Contains an URL listed in the DBL blocklist
tflags          URIBL_DBL_SPAM   net domains_only

urirhssub       URIBL_DBL_REDIR  dbl.spamhaus.org.       A   127.0.1.3
body            URIBL_DBL_REDIR  eval:check_uridnsbl('URIBL_DBL_REDIRECTOR')
describe        URIBL_DBL_REDIR  Contains a URL listed in the DBL as a spammed redirector domain
tflags          URIBL_DBL_REDIR  net domains_only

# this indicates that IP-address queries were sent to DBL, and should
# never appear; if it does, something is wrong with SpamAssassin
urirhssub       URIBL_DBL_ERROR  dbl.spamhaus.org.       A   127.0.1.255
body            URIBL_DBL_ERROR  eval:check_uridnsbl('URIBL_DBL_ERROR')
describe        URIBL_DBL_ERROR  Error: queried the DBL blocklist for an IP
tflags          URIBL_DBL_ERROR  net domains_only
endif

###########################################################################
## SURBL

urirhssub       URIBL_SC_SURBL  multi.surbl.org.        A   2
body            URIBL_SC_SURBL  eval:check_uridnsbl('URIBL_SC_SURBL')
describe        URIBL_SC_SURBL  Contains an URL listed in the SC SURBL blocklist
tflags          URIBL_SC_SURBL  net
reuse           URIBL_SC_SURBL

urirhssub       URIBL_WS_SURBL  multi.surbl.org.        A   4
body            URIBL_WS_SURBL  eval:check_uridnsbl('URIBL_WS_SURBL')
describe        URIBL_WS_SURBL  Contains an URL listed in the WS SURBL blocklist
tflags          URIBL_WS_SURBL  net
reuse           URIBL_WS_SURBL

urirhssub       URIBL_PH_SURBL  multi.surbl.org.        A   8
body            URIBL_PH_SURBL  eval:check_uridnsbl('URIBL_PH_SURBL')
describe        URIBL_PH_SURBL  Contains an URL listed in the PH SURBL blocklist
tflags          URIBL_PH_SURBL  net
reuse           URIBL_PH_SURBL

urirhssub       URIBL_MW_SURBL  multi.surbl.org.        A   16
body            URIBL_MW_SURBL  eval:check_uridnsbl('URIBL_MW_SURBL')
describe        URIBL_MW_SURBL  Contains a Malware Domain or IP listed in the MW SURBL blocklist
tflags          URIBL_MW_SURBL  net
reuse           URIBL_MW_SURBL

urirhssub       URIBL_AB_SURBL  multi.surbl.org.        A   32
body            URIBL_AB_SURBL  eval:check_uridnsbl('URIBL_AB_SURBL')
describe        URIBL_AB_SURBL  Contains an URL listed in the AB SURBL blocklist
tflags          URIBL_AB_SURBL  net
reuse           URIBL_AB_SURBL

urirhssub       URIBL_JP_SURBL  multi.surbl.org.        A   64
body            URIBL_JP_SURBL  eval:check_uridnsbl('URIBL_JP_SURBL')
describe        URIBL_JP_SURBL  Contains an URL listed in the JP SURBL blocklist
tflags          URIBL_JP_SURBL  net
reuse           URIBL_JP_SURBL

###########################################################################
## URIBL

urirhssub       URIBL_BLACK     multi.uribl.com.        A   2
body            URIBL_BLACK     eval:check_uridnsbl('URIBL_BLACK')
describe        URIBL_BLACK     Contains an URL listed in the URIBL blacklist
tflags          URIBL_BLACK     net
reuse           URIBL_BLACK

urirhssub       URIBL_GREY      multi.uribl.com.        A   4
body            URIBL_GREY      eval:check_uridnsbl('URIBL_GREY')
describe        URIBL_GREY      Contains an URL listed in the URIBL greylist
tflags          URIBL_GREY      net
reuse           URIBL_GREY

urirhssub       URIBL_RED       multi.uribl.com.        A   8
body            URIBL_RED       eval:check_uridnsbl('URIBL_RED')
describe        URIBL_RED       Contains an URL listed in the URIBL redlist
tflags          URIBL_RED       net
reuse           URIBL_RED

#URIBL BLOCK RULES - Bit 1 means your DNS has been blocked and this rule should be triggered to notify you.
urirhssub       URIBL_BLOCKED   multi.uribl.com.        A   1
body            URIBL_BLOCKED   eval:check_uridnsbl('URIBL_BLOCKED')
describe        URIBL_BLOCKED   ADMINISTRATOR NOTICE: The query to URIBL was blocked.  See http://wiki.apache.org/spamassassin/DnsBlocklists\#dnsbl-block for more information.
tflags          URIBL_BLOCKED   net noautolearn
reuse           URIBL_BLOCKED

###########################################################################
## DOMAINS TO SKIP (KNOWN GOOD)

# Don't bother looking for example domains as per RFC 2606.
uridnsbl_skip_domain example.com example.net example.org

uridnsbl_skip_domain local.cf

# MUA CSS class definitions
uridnsbl_skip_domain div.tk p.tk li.tk no.tk

# (roughly) top 200 domains not blacklisted by SURBL
uridnsbl_skip_domain 126.com 163.com 2o7.net 4at1.com
uridnsbl_skip_domain 5iantlavalamp.com about.com adelphia.net adobe.com addthis.com
uridnsbl_skip_domain agora-inc.com agoramedia.com akamai.net
uridnsbl_skip_domain akamaitech.net amazon.com ancestry.com aol.com
uridnsbl_skip_domain apache.org apple.com arcamax.com astrology.com
uridnsbl_skip_domain atdmt.com att.net bbc.co.uk
uridnsbl_skip_domain bcentral.com bellsouth.net bfi0.com
uridnsbl_skip_domain bridgetrack.com cafe24.com charter.net
uridnsbl_skip_domain citibank.com citizensbank.com cjb.net
uridnsbl_skip_domain classmates.com clickbank.net cnet.com
uridnsbl_skip_domain cnn.com com.com com.ne.kr comcast.net
uridnsbl_skip_domain corporate-ir.net cox.net cs.com
uridnsbl_skip_domain custhelp.com daum.net dd.se debian.org
uridnsbl_skip_domain dell.com directtrack.com directnic.com domain.com
uridnsbl_skip_domain dsbl.org earthlink.net ebay.co.uk ebay.com
uridnsbl_skip_domain ebayimg.com ebaystatic.com edgesuite.net ediets.com
uridnsbl_skip_domain egroups.com emode.com excite.com f-secure.com
uridnsbl_skip_domain free.fr freebsd.org
uridnsbl_skip_domain gentoo.org geocities.com gmail.com gmx.net
uridnsbl_skip_domain go.com google.com googleadservices.com grisoft.com
uridnsbl_skip_domain hallmark.com hinet.net hotbar.com hotmail.com
uridnsbl_skip_domain hotpop.com hp.com ibm.com incredimail.com
uridnsbl_skip_domain investorplace.com ivillage.com joingevalia.com
uridnsbl_skip_domain juno.com kernel.org livejournal.com lycos.com
uridnsbl_skip_domain m7z.net mac.com macromedia.com
uridnsbl_skip_domain mail.com mail.ru mailscanner.info marketwatch.com
uridnsbl_skip_domain mcafee.com mchsi.com messagelabs.com
uridnsbl_skip_domain microsoft.com military.com mindspring.com mit.edu
uridnsbl_skip_domain monster.com msn.com nate.com
uridnsbl_skip_domain netflix.com netscape.com netscape.net netzero.net
uridnsbl_skip_domain norman.com nytimes.com optonline.net osdn.com
uridnsbl_skip_domain overstock.com pacbell.net pandasoftware.com
uridnsbl_skip_domain paypal.com peoplepc.com plaxo.com
uridnsbl_skip_domain prodigy.net radaruol.com.br
uridnsbl_skip_domain real.com redhat.com regions.com regionsnet.com
uridnsbl_skip_domain rogers.com rr.com sbcglobal.net sec.gov sf.net
uridnsbl_skip_domain shaw.ca shockwave.com smithbarney.com
uridnsbl_skip_domain sourceforge.net spamcop.net speedera.net sportsline.com
uridnsbl_skip_domain sun.com suntrust.com sympatico.ca t-online.de
uridnsbl_skip_domain tails.nl telus.net terra.com.br ticketmaster.com
uridnsbl_skip_domain tinyurl.com tiscali.co.uk tom.com
uridnsbl_skip_domain tone.co.nz tux.org uol.com.br
uridnsbl_skip_domain ups.com verizon.net w3.org usps.com
uridnsbl_skip_domain wamu.com wanadoo.fr washingtonpost.com weatherbug.com
uridnsbl_skip_domain web.de webshots.com webtv.net wsj.com
uridnsbl_skip_domain yahoo.ca yahoo.co.kr yahoo.co.uk
uridnsbl_skip_domain yahoo.com yahoo.com.br yahoogroups.com yimg.com
uridnsbl_skip_domain yopi.de yoursite.com zdnet.com
uridnsbl_skip_domain openxmlformats.org passport.com xmlsoap.org

# wtogami's most frequent known good URIDNSBL lookups (1/1/2011)
uridnsbl_skip_domain alexa.com ask.com baidu.com bing.com craigslist.org
uridnsbl_skip_domain doubleclick.com ebay.de facebook.com flickr.com godaddy.com
uridnsbl_skip_domain google.co.in google.it mozilla.com myspace.com rediff.com
uridnsbl_skip_domain twitter.com wordpress.com yahoo.co.jp youtube.com

# axb's frequent known good URIDNSBL lookups

uridnsbl_skip_domain fedex.com
uridnsbl_skip_domain openoffice.org

endif   # Mail::SpamAssassin::Plugin::URIDNSBL

# Copied from a newer spamassassin configuration, originally
#  located at /usr/share/spamassassin/50_scores.cf
###########################################################################
score URIBL_AB_SURBL 0 4.499 0 4.499 # n=0 n=2
score URIBL_JP_SURBL 0 1.948 0 1.250 # n=0 n=2
score URIBL_PH_SURBL 0 0.001 0 0.610 # n=0 n=2
score URIBL_RHS_DOB 0 0.276 0 1.514 # n=0 n=2
score URIBL_SBL 0 0.644 0 1.623 # n=0 n=2
score URIBL_SBL_A 0 0.1 0 0.1
score URIBL_SC_SURBL 0 0.001 0 0.568 # n=0 n=2
score URIBL_WS_SURBL 0 1.659 0 1.608 # n=0 n=2
score URIBL_MW_SURBL 0 1.263 0 1.263
score URIBL_BLACK 0 1.775 0 1.725 # n=0 n=2
score URIBL_GREY 0 1.084 0 0.424 # n=0 n=2
score URIBL_DBL_SPAM 0 1.7 0 1.7
score URIBL_DBL_REDIR 0 0.001 0 0.001
# score URIBL_GREY 0.25
score URIBL_RED 0 0.001 0 0.001
score URIBL_BLOCKED 0 0.001 0 0.001
score URIBL_DBL_ERROR 0 0.001 0 0.001

###########################################################################
# Local META rules
meta LOCAL_BL_BLACK_DSPAM (URIBL_BLACK && DSPAM.Spam)
score  LOCAL_BL_BLACK_DSPAM 5.5

# Label everything but some domains as possible spam
header __CSA_SPAMDOMAINS_b From =~ /\\@\\S+\\.[^\\.]{3,}>/
header __CSA_SPAMDOMAINS_g From =~ /\\@\\S+\\.(com|net|org|edu|mil|gov|info)>/i
meta CSA_SPAMDOMAINS ( __CSA_SPAMDOMAINS_b && !__CSA_SPAMDOMAINS_g )
describe CSA_SPAMDOMAINS Blacklisted domains
score CSA_SPAMDOMAINS 0.5

# 25-Aug-2016:
header CSA_2016082500 Subject =~ /^\\\\(\\\\S+\\\\)/
describe CSA_2016082500 Email contains fake news
score CSA_2016082500 1.5

# 26-Oct-2020:
header __CSA_2020102600_L X-Spam-Level =~ /S{8,}/
header __CSA_2020102600_R Return-Path =~ /mailfilter.quarantine\@digitaltowpath\.org/i
meta   CSA_2020102600 ( __CSA2020102600_L && __CSA_2020102600_R )
describe CSA_2020102600 No email should end up in "Junk"
score CSA_2020102600 -8
EOT
    if [ $ZIMBRA_VERS_MAJ -gt 8 ]
    then
        SAUSER_LOC=/opt/zimbra/data/spamassassin/localrules
    elif [ $ZIMBRA_VERS_MAJ -eq 8 -a $ZIMBRA_VERS_MIN -ge 5 ]
    then
        SAUSER_LOC=/opt/zimbra/data/spamassassin/localrules
    else
        SAUSER_LOC=/opt/zimbra/conf/sa
    fi

    diff /tmp/sauser.cf $SAUSER_LOC/sauser.cf &> /dev/null
    if [ $? -ne 0 ]
    then
        # Activate the new rules
        cat /tmp/sauser.cf > $SAUSER_LOC/sauser.cf
        logger --id -p mail.warning -t $PROG -- Activate new local antispam rules
        RESTART_AMAVIS=1
    fi

    # Disable dspam (it clogs up amavisd)
    if [ $ZIMBRA_VERS_MAJ -gt 8 ]
    then
        if [ "T$(zmprov gs $ZIMBRA_HOSTNAME zimbraAmavisDSPAMEnabled | awk '/:/ {print $NF}')" = 'TTRUE' ]
        then
            logger --id -p mail.warning -t $PROG Disabling dspam
            su - zimbra -c "zmprov ms $ZIMBRA_HOSTNAME zimbraAmavisDSPAMEnabled FALSE"

            # Knock DSpam's score down from 10 ... way too aggressive
            for F in /opt/zimbra/conf/amavisd.conf /opt/zimbra/conf/amavisd.conf.in
            do
                sed -i 's/score_factor => 1/score_factor => 0.1/' $F
            done
            logger --id -p mail.warning -t $PROG -- Tone down DSPAM score
            RESTART_AMAVIS=1
        fi
    elif [ $ZIMBRA_VERS_MAJ -eq 8 -a $ZIMBRA_VERS_MIN -ge 5 ]
    then
        if [ "T$(zmprov gs $ZIMBRA_HOSTNAME zimbraAmavisDSPAMEnabled | awk '/:/ {print $NF}')" = 'TTRUE' ]
        then
            logger --id -p mail.warning -t $PROG -- Disabling dspam
            su - zimbra -c "zmprov ms $ZIMBRA_HOSTNAME zimbraAmavisDSPAMEnabled FALSE"

            # Knock DSpam's score down from 10 ... way too aggressive
            for F in /opt/zimbra/conf/amavisd.conf /opt/zimbra/conf/amavisd.conf.in
            do
                sed -i 's/score_factor => 1/score_factor => 0.1/' $F
            done
            logger --id -p mail.warning -t $PROG -- Tone down DSPAM score
            RESTART_AMAVIS=1
        fi
    elif [ "T$(zmlocalconfig amavis_dspam_enabled 2>/dev/null | awk '{print $NF}')" = 'Ttrue' ]
    then
        su - zimbra -c "zmlocalconfig -e 'amavis_dspam_enabled=false'"
        logger --id -p mail.warning -t $PROG -- Disabling dspam

        # Knock DSpam's score down from 10 ... way too aggressive
        for F in /opt/zimbra/conf/amavisd.conf /opt/zimbra/conf/amavisd.conf.in
        do
            sed -i 's/score_factor => 1/score_factor => 0.1/' $F
        done
        logger --id -p mail.warning -t $PROG -- Tone down DSPAM score
        RESTART_AMAVIS=1
    fi

    # Log more verbose amavis messages
    if [ $(zmprov gcf zimbraAmavisLogLevel | awk '{print int($NF)}') -lt 2 ]
    then
        su - zimbra -c 'zmprov mcf zimbraAmavisLogLevel 2'
        logger --i -p mail.warning -t $PROG -- Making amavis logs more verbose
        [ $RESTART_AMAVIS -eq 0 ] && RESTART_AMAVIS=2
    fi

    # Automatically update spamassassin rule
    # See https://wiki.zimbra.com/wiki/SpamAssassin_Customizations
    for ZA in antispam_enable_rule_updates antispam_enable_restarts
    do
        if [ "T$(zmlocalconfig $ZA 2>/dev/null | awk '{print $NF}')" != 'Ttrue' ]
        then
            su - zimbra -c "zmlocalconfig -e ${ZA}=true"
            logger --id -p mail.warning -t $PROG -- Automatically enable spamassassin rules
            [ $RESTART_AMAVIS -eq 0 ] && RESTART_AMAVIS=3
        fi
    done

    # Adapt/optimize handling of clamav signatures
    if [ -z "$(grep 'virus_name_to_spam_score_maps' /opt/zimbra/conf/amavisd.conf.in)" ]
    then
        grep -v '^1;' /opt/zimbra/conf/amavisd.conf.in > /tmp/amavisd.conf.in
        cat << EOT >> /tmp/amavisd.conf.in
### B-LUC: Optimized handling of clamav signatures
@virus_name_to_spam_score_maps =
 (new_RE(  # the order matters, first match wins
  [ qr'^Structured\.(SSN|CreditCardNumber)\b'            => 0.1 ],
  [ qr'^(Heuristics\.)?Phishing\.'                       => 0.1 ],
  [ qr'^(Email|HTML)\.Phishing\.(?!.*Sanesecurity)'      => 0.1 ],
  [ qr'^Sanesecurity\.(Malware|Badmacro|Foxhole|Rogue|Trojan)\.' => undef ],# keep as infected
  [ qr'^Sanesecurity\.'                                  => 0.1 ],
  [ qr'^Sanesecurity.TestSig_'                           => 0   ],
  [ qr'^Email\.Spam\.Bounce(\.[^., ]*)*\.Sanesecurity\.' => 0   ],
  [ qr'^Email\.Spammail\b'                               => 0.1 ],
  [ qr'^MSRBL-(Images|SPAM)\b'                           => 0.1 ],
  [ qr'^VX\.Honeypot-SecuriteInfo\.com\.Joke'            => 0.1 ],
  [ qr'^VX\.not-virus_(Hoax|Joke)\..*-SecuriteInfo\.com(\.|\z)' => 0.1 ],
  [ qr'^Email\.Spam.*-SecuriteInfo\.com(\.|\z)'          => 0.1 ],
  [ qr'^SecuriteInfo\.com\.Spam\.'                       => 0.1 ],
  [ qr'^Safebrowsing\.'                                  => 0.1 ],
  [ qr'^winnow\.(exploit|trojan|malware)\.'              => undef ],# keep as infected
  [ qr'^winnow\.(botnet|compromised|trojan)'             => undef ],# keep as infected
  [ qr'^winnow\.(exe|ms|js)\.'                           => undef ],# keep as infected
  [ qr'^winnow\.(phish|spam)\.'                          => 0.1 ],
  [ qr'^winnow\.'                                        => 0.1 ],
  [ qr'^INetMsg\.SpamDomain'                             => 0.1 ],
  [ qr'^Doppelstern\.(Spam|Scam|Phishing|Junk|Lott|Loan)'=> 0.1 ],
  [ qr'^BofhlandMW\.'                                    => undef ],# keep as infected
  [ qr'^Bofhland\.Malware\.'                             => undef ],# keep as infected
  [ qr'^Bofhland\.'                                      => 0.1 ],
  [ qr'^ScamNailer\.'                                    => 0.1 ],
  [ qr'^HTML/Bankish'                                    => 0.1 ],  # F-Prot
  [ qr'^Porcupine\.(Malware|JS|Java|Win32|MSIL|VBS)\.'   => undef ],# keep as infected
  [ qr'^Porcupine\.'                                     => 0.1 ],
  [ qr'^PhishTank\.Phishing\.'                           => 0.1 ],
  [ qr'(-)?SecuriteInfo\.com(\.|\z)'                     => undef ],  # keep as infected
  [ qr'^MBL_NA\.UNOFFICIAL'                              => 0.1 ],    # false positives
  [ qr'^MBL_'                                            => 8 ], # treat as spam
));

1;  # insure a defined return value
EOT
        diff -wu /opt/zimbra/conf/amavisd.conf.in /tmp/amavisd.conf.in &> /dev/null
        if [ $? -ne 0 ]
        then
            logger --id -p mail.warning -t $PROG -- Expand Clamav signatures
            cat /tmp/amavisd.conf.in > /opt/zimbra/conf/amavisd.conf.in
            RESTART_AMAVIS=4
        fi
    fi

    # Restart zimbra amavis (if needed)
    if [ $RESTART_AMAVIS -ne 0 ]
    then
        logger --id -p mail.warning -t $PROG -- Restarting Amavis
        su - zimbra -c 'zmamavisdctl stop; sleep 1; zmamavisdctl start'
    fi
fi

# Make "tbulli" an admin user
[ -z "$(zmprov -l gaaa | grep tbulli)" ] && zmprov ma tbulli@${THISDOMAIN} zimbraIsAdminAccount TRUE

# Check this ...
#zmlocalconfig -e mailboxd_java_options="-server -Djava.awt.headless=true \
#    -XX:+UseConcMarkSweepGC -XX:+UseParNewGC -XX:NewRatio=2 \
#    -XX:PermSize=196m -XX:MaxPermSize=350m -XX:SoftRefLRUPolicyMSPerMB=1 \
#    -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCTimeStamps \
#    -XX:+PrintGCApplicationStoppedTime \
#    -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/opt/zimbra/log \
#    -XX:ErrorFile=/opt/zimbra/log/hs_err_pid%p.log"

# Strenghten the SSL/HTTPS settings
RESTART_ZP=0
if [ ! -z "$(/opt/zimbra/bin/zmprov gcf zimbraReverseProxySSLProtocols | grep 'TLSv1$')" ]
then
    su - zimbra -c 'zmprov mcf -zimbraReverseProxySSLProtocols TLSv1'
    RESTART_ZP=1
fi
PSC=$(/opt/zimbra/bin/zmprov gcf zimbraReverseProxySSLCiphers | cut -d: -f2-)
if [ ! "T$PSC" = 'T ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128:AES256:TLS_AES_256_GCM_SHA384:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!MD5:!PSK:!RC4' ]
then
    su - zimbra -c 'zmprov mcf zimbraReverseProxySSLCiphers "ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128:AES256:TLS_AES_256_GCM_SHA384:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!MD5:!PSK:!RC4"'
    RESTART_ZP=1
fi
[ $RESTART_ZP -ne 0 ] && su - zimbra -c 'zmproxyctl restart'

# Restart Zimbra if asked for above (hopefully only once)
if [ $RESTART_ZIMBRA -ne 0 ]
then
    logger --id -p mail.warning -t $PROG -- Restarting Zimbra
    su - zimbra -c 'zmcontrol restart'
fi

ZP=$(ps -wefH | grep -c zmsta[t])
if [ $ZP -ge 20 ]
then
    logger --id -p mail.warning -t $PROG -- Too many zmstat processes [$ZP]
    sendemail -q -f tbulli@${THISDOMAIN} -o tls=no \
      -t consult@btoy1.net \
      -s localhost -u "Too many zmstat processes [$ZP]"
fi

#--------------------------------------------------------------------
if [ "T$DEBUG" = 'T-v' -o "T$HR_NOW:$MIN_NOW" = 'T1:58' ]
then
    # Actions to be done at 1:58am

    # Cleanup the fail2ban database
    if [ -x /usr/bin/sqlite3 ]
    then
        sqlite3 /var/lib/fail2ban/fail2ban.sqlite3 "VACUUM;"
    else
        cat << EOT > /tmp/$$.f2b.vacuum
#!/usr/bin/python
import sqlite3
try:
    conn = sqlite3.connect('/var/lib/fail2ban/fail2ban.sqlite3')
except:
    sys.exit ("Couldn't connect to /var/lib/fail2ban/fail2ban.sqlite3")

print "Connected to /var/lib/fail2ban/fail2ban.sqlite3";
conn.execute('''VACUUM;''')
conn.commit
print "Cleaned up /var/lib/fail2ban/fail2ban.sqlite3";
conn.close()
EOT
        python /tmp/$$.f2b.vacuum
        rm -f /tmp/$$.f2b.vacuum
    fi

    # Create a report for Zimbra clients
    rm -f /tmp/zc.txt
    ZimbraGetClients.pl > /tmp/zc.txt
    [ -s /tmp/zc.txt ] && [ $(date +%u) -eq 5 ] && sendemail -q -f root@$THISDOMAIN -o tls=no \
      -t ateam@$THISDOMAIN -s localhost \
      -u "Zimbra Client Statistics for "$(date +%F -d yesterday) < /tmp/zc.txt

    # Update the Zimbra SSL certificate (if necessary)
    RPROXY=$(host rproxy 192.168.20.251 | awk '/has address/{print $NF}')
    if [ ! -z "$RPROXY" ]
    then
        if [ $ZIMBRA_VERS_MAJ -gt 8 ]
        then
            su - zimbra -c '/opt/zimbra/bin/zmcertmgr checkcrtexpiration -days 7 all' &> /dev/null
            SSL_EXPIRES=$?
        elif [ $ZIMBRA_VERS_MAJ -eq 8 -a $ZIMBRA_VERS_MIN -ge 7 ]
        then
            su - zimbra -c '/opt/zimbra/bin/zmcertmgr checkcrtexpiration -days 7 all' &> /dev/null
            SSL_EXPIRES=$?
        else
            /opt/zimbra/bin/zmcertmgr checkcrtexpiration -days 7 all &> /dev/null
            SSL_EXPIRES=$?
        fi
        if [ $SSL_EXPIRES -ne 0 ]
        then
            RESTART_ZIMBRA=0

            # Get the SSL wildcard certificate files from the proxy server
            for F in wildcard.key wildcard.crt gd_bundle-g2-g1.crt wildcard.csr
            do
                rsync ${RPROXY}::wildcard/$F /tmp
            done

            # Save the current commercial SSL files
            NOW=$(date +%s)
            for F in commercial.crt commercial.key commercial.csr commercial_ca.crt
            do
                cp -f /opt/zimbra/ssl/zimbra/commercial/$F /opt/zimbra/ssl/zimbra/commercial/$F.$NOW
            done
            # Delete any SSL certificates older than a year
            find /opt/zimbra/ssl/zimbra/commercial -type f -name commercial*.[0-9]* -mtime +365 -delete

            # Deploy the new certificate
            cp /tmp/wildcard.key /opt/zimbra/ssl/zimbra/commercial/commercial.key
            cp /tmp/wildcard.csr /opt/zimbra/ssl/zimbra/commercial/commercial.csr
            su - zimbra -c '/opt/zimbra/bin/zmcertmgr deploycrt comm /tmp/wildcard.crt /tmp/gd_bundle-g2-g1.crt'
            su - zimbra -c 'zmcontrol restart'
        fi
    fi

    # Check whether any "zmstat" logs are corrupt
    grep -c ^timestamp /opt/zimbra/zmstat/*csv | awk -F: '$NF > 2 {print}' > /tmp/$$.zmstat.logs
    [ -s /tmp/$$.zmstat.logs ] && sendemail -q -f tbulli@$THISDOMAIN -o tls=no \
       -t ateam@$THISDOMAIN -s localhost -u "Found possibly corrupt zmstat log files" < /tmp/$$.zmstat.logs

    # Update the 3rd party signatures for clamav and activate them
    logger -p mail.info -- ${PROG}[$$] Updating 3rd party clamav signatures
    WDEBUG='-q'
    [[ $- ==  *x* ]] && WDEBUG='-v'
    mkdir -p /tmp/$$.av
    cd /tmp/$$.av
    wget -t 3 -T 180 $WDEBUG http://cl1.btoy1.net/index.html -O - | \
      egrep -v '(main.c[lv]d|daily.cld|bytecode.c[lv]d|index.html|MiscreantPunch099-Low.ldb|GeoLite2.*mmdb|mmdb.md5sum)' | \
      sed 's@^@http://cl1.btoy1.net/@g' > /tmp/avindex
    rm -f /tmp/avupdate
    for F in $(< /tmp/avindex)
    do
        wget -t 3 -T 180 $WDEBUG $F.md5sum
        diff ${F##*/}.md5sum /opt/zimbra/data/clamav/db/${F##*/}.md5sum &> /dev/null
        if [ $? -ne 0 ]
        then
            # Download the signature file and notate it
            wget -t 10 -T 180 $WDEBUG $F
            if [ $? -eq 0 ]
            then
                sha512sum -b "${F##*/}" | awk '{print $1}' > ${F##*/}.md5sum.downloaded
                diff ${F##*/}.md5sum ${F##*/}.md5sum.downloaded &> /dev/null
                [ $? -eq 0 ] && echo "${F##*/}" >> /tmp/avupdate
            fi
        fi
    done

    logger -p mail.info -- ${PROG}[$$] Updating Clamav signatures
    if [ -s /tmp/avupdate ]
    then
        # Only update what we had to download
        for F in $(< /tmp/avupdate)
        do
            RC=0
            [[ $F =~ md5sum ]] || /opt/zimbra/common/bin/clamscan -d $F /etc/passwd &> /dev/null
            RC=$?
            if [ $RC -eq 0 ]
            then
                install -o zimbra $F /opt/zimbra/data/clamav/db
                [ -s $F.mdsum ] && install -o zimbra $F.mdsum /opt/zimbra/data/clamav/db
            fi
        done
    fi

    # Activate the extra signatures
    sed -i 's/^#Scan/Scan/;s/^#Phish/Phish/;s/^ScanOnAccess.*/# ScanOnAccess no/' /opt/zimbra/conf/clamd.conf.in
    sed 's/^#Scan/Scan/;s/^#Phish/Phish/;s/^ScanOnAccess.*/# ScanOnAccess no/' /opt/zimbra/conf/clamd.conf > /tmp/$$
    diff /opt/zimbra/conf/clamd.conf /tmp/$$ &> /dev/null
    if [ $? -ne 0 ]
    then
        logger --id -p mail.warning -t $PROG -- Activating additional Clamav signatures
        cat /tmp/$$ > /opt/zimbra/conf/clamd.conf
        su - zimbra -c '/opt/zimbra/bin/zmclamdctl stop; /opt/zimbra/bin/zmclamdctl start' > /dev/null
    fi

    # Check whether clamd is running okay
    sleep 2
    if [ -z "$(pgrep clamd)" ]
    then
        MAIL_RECIP='consult@btoy1.net'
        echo 'No running clamav daemon found' | \
          sendemail -q -f tbulli@$THISDOMAIN -o tls=no \
            -t $MAIL_RECIP -cc ateam@$THISDOMAIN \
            -s localhost -u "Problem with clamav on $THISHOST"
    else
        RESTART_CLAMD=0
        for F in $(awk '/failed.*to.*parse/{sub(/,/,"",$14);print $14}' /opt/zimbra/log/clamd.log | sort -u)
        do
            if [ -s $F ]
            then
                logger --id -p mail.warning -t $PROG -- Remove bad clamav signature file $F
                rm -f $F
                RESTART_CLAMD=1
            fi
        done
        [ $RESTART_CLAMD -ne 0 ] && su - zimbra -c 'zmclamdctl restart'
    fi

    # Create antispam stats
    AntiSpamStats.sh /var/log/zimbra.log | \
      sendemail -q -f root@$THISDOMAIN -t ateam@$THISDOMAIN \
       -u "$THISHOST: Anti spam statistics" -o tls=no -s 127.0.0.1

    # Create zimbra charts
    [ -x /usr/local/sbin/ZimbraCharts.sh ] && /usr/local/sbin/ZimbraCharts.sh

    # Process any disabled member domains
    [ -x /usr/local/sbin/ProcessDisabledMembers.sh ] && /usr/local/sbin/ProcessDisabledMembers.sh &> /tmp/PDM.txt
    [ -s /tmp/PDM.txt ] && sendemail -q -f root@$THISDOMAIN -t ateam@$THISDOMAIN $DEBUG \
       -u "$THISHOST: Disabled Member Domains" -o tls=no -s 127.0.0.1 < /tmp/PDM.txt

    # Get list of "disabled" domains
    zmprov -l gad | grep disabled > /tmp/$$
    if [ -s /tmp/$$ ]
    then
        sendemail -q -f tbulli@${THISDOMAIN} -o tls=no \
          -t ateam@digitaltowpath.org \
          -s localhost -u "List of 'disabled' zimbra domains" < /tmp/$$
    fi        

    # Workaround for Zimbra's anti-DOS measures which lead to "network errors"
    # See http://community.zimbra.com/collaboration/f/1886/t/1133484
    if [ $(zmprov gs $ZIMBRA_HOSTNAME zimbraInvalidLoginFilterMaxFailedLogin | awk '/^zimbra/{print $NF}') -ne 0 ]
    then
        su - zimbra -c "zmprov ms $ZIMBRA_HOSTNAME zimbraInvalidLoginFilterMaxFailedLogin 0;zmprov mcf zimbraInvalidLoginFilterMaxFailedLogin 0;zmmailboxdctl restart"
    fi

    # Check disk space and create warning/error messages
    df -PTh | awk '/ext[234]/ {print $7" "int($6)}' | while read FS PERC
    do
        if [ $PERC -gt 75 ]
        then
            if [ $PERC -gt 85 ]
            then
                logger --id -p crit -t disk -- Usage on $FS is extremely high - ${PERC}%
                echo "CRITICAL: Extreme high usage (${PERC}%) on file system $FS"
                echo 'See subject ' | sendemail -q -f root@$THISDOMAIN -t ateam@$THISDOMAIN \
                  $DEBUG -u "$THISHOST: CRITICAL: Extreme high usage (${PERC}%) on file system $FS" \
                  -o tls=no -s 127.0.0.1
            else
                logger --id -p err -t disk -- Usage on $FS is high - ${PERC}%
                echo "WARNING: High usage (${PERC}%) on file system $FS"
                echo 'See subject ' | sendemail -q -f root@$THISDOMAIN -t ateam@$THISDOMAIN \
                  $DEBUG -u "$THISHOST: WARNING: High usage (${PERC}%) on file system $FS" \
                  -o tls=no -s 127.0.0.1
            fi
        else
            logger --id -p info -t disk -- Usage on $FS is normal - ${PERC}%
        fi
    done

    # Get the daily system monitoring data
    zgrep -h "$(date '+%b %_d')"'.*SysMon.*DAILY' /var/log/syslog* | awk -F: '{print $NF}' > /tmp/$$
    if [ -s /tmp/$$ ]
    then
        echo > /tmp/SyslogSummary.txt
        date +"Daily system monitoring summary on $THISHOST for %F:" -d yesterday >> /tmp/SyslogSummary.txt
        echo >> /tmp/SyslogSummary.txt
        grep average /tmp/$$ >> /tmp/SyslogSummary.txt
        echo >> /tmp/SyslogSummary.txt
        egrep '(Receive|Transmit)' /tmp/$$ >> /tmp/SyslogSummary.txt
        echo >> /tmp/SyslogSummary.txt
        egrep 'Disk (read|write)' /tmp/$$ >> /tmp/SyslogSummary.txt
        echo >> /tmp/SyslogSummary.txt
        grep 'Mysql' /tmp/$$ >> /tmp/SyslogSummary.txt
        # Delete duplicate empty lines
        # See http://sed.sourceforge.net/sed1line.txt
        sed -i -e '$!N; /^\(.*\)\n\1$/!P; D' /tmp/SyslogSummary.txt
        sendEmail -q -f root@$THISHOST -t ateam@$THISDOMAIN \
          -u "SysMon statistics for `date +%D -d yesterday` on $THISHOST" \
          -o tls=no -s mail < /tmp/SyslogSummary.txt
    fi

    # Allow the "recent" iptables module to remember more packets per IP address
    # See http://www.thatsgeeky.com/2011/02/escalating-consequences-with-iptables/
    if [ -z "$(grep ip_pkt_list_tot /etc/modprobe.d/dkms.conf)" ]
    then
        echo 'options xt_recent ip_pkt_list_tot=40' >> /etc/modprobe.d/dkms.conf
        echo 'Increased remembered IP addresses for iptables "recent" module' | \
          sendEmail -q -f root@$THISHOST -t ateam@$THISDOMAIN \
            -u "$THISHOST"': Reboot requested' -o tls=no -s mail
    fi

    # Create a report of accesses from outside the US or Canada
    DAY2SEARCH=$(date "+%b %_d" -d "${1-yesterday}")
    egrep -h "^$DAY2SEARCH.*not from.*DPT=(80|443|993|995) " /var/log/kern.log /var/log/kern.log.1 > /tmp/$$
    if [ -s /tmp/$$ ]
    then
        > /tmp/RemoteAccesses
        awk '
         {
          sub(/SRC=/,"",$14);
          A[$7" from "$14]++
         }
      END{
          for (var in A)
            print A[var]" accesses to "var
         }' /tmp/$$ | \
           sort -rn -k 1 | \
             while read LINE
             do
               awk '{printf "%8d ",$1;$1="";printf "%s in ",$0}' <<< $LINE >> /tmp/RemoteAccesses
               geoiplookup $(awk '{print $NF}' <<< $LINE) | sed -e 's/^Geo.*: //' >> /tmp/RemoteAccesses
             done
        sendEmail -q -f root@$THISHOST -t ateam@$THISDOMAIN \
          -u "$THISHOST: Accesses from outside the USA and Canada for '$DAY2SEARCH'" \
          -o tls=no -s mail < /tmp/RemoteAccesses
    fi

    STUNNEL_ENDDATE=$(date +%s -d "$(openssl x509 -enddate -noout -in /etc/stunnel/stunnel.pem | cut -d= -f2)")
    if [ $STUNNEL_ENDDATE -lt $(date +%s -d '+ 1 week') ]
    then
        # Renew the SSL cert
        cat << EOF > /tmp/CONFIG_FILE
[req]
default_bits              = 2048
req_extensions            = extension_requirements
distinguished_name        = dn_requirements

[extension_requirements]
basicConstraints          = CA:FALSE
keyUsage                  = nonRepudiation, digitalSignature, keyEncipherment

[dn_requirements]
countryName               = Country Name (2 letter code)
countryName_value         = US
stateOrProvinceName       = State or Province Name (full name)
stateOrProvinceName_value = New York
localityName              = Locality Name (eg, city)
localityName_value        = Barneveld
0.organizationName        = Organization Name (eg, company)
0.organizationName_value  = Digital Towpath Cooperative
organizationalUnitName    = Organizational Unit Name (eg, section)
organizationalUnitName_value = Technical Operations
commonName                = Common Name (e.g. server FQDN or YOUR name)
commonName_value          = $(hostname -f)
emailAddress              = Email Address
emailAddress_value        = support@digitaltowpath.org
EOF
        echo '-----BEGIN RSA PRIVATE KEY-----' > /tmp/$$.stunnel.key
        sed -n '/BEGIN RSA PRIVATE KEY/,/END RSA PRIVATE KEY/{//!p;}' /etc/stunnel/stunnel.pem >> /tmp/$$.stunnel.key
        echo '-----END RSA PRIVATE KEY-----' >> /tmp/$$.stunnel.key
        openssl req -config /tmp/CONFIG_FILE -new -x509 -key /tmp/$$.stunnel.key -out /tmp/$$.stunnel.pem -days 1095
        if [ $? -eq 0 ]
        then
            cat /tmp/$$.stunnel.key /tmp/$$.stunnel.pem > /etc/stunnel/stunnel.pem
            service stunnel4 restart

            sendEmail -q -f root@$THISHOST -t ateam@digitaltowpath.org \
              -u "$THISHOST: New SSL certificate for stunnel" \
              -o tls=no -s mail -a /etc/stunnel/stunnel.pem << EOT
Please copy the attached certificate file to /etc/stunnel/stunnel.pem on both
Mail Filter servers.  Also add this stanza to /etc/stunnel/stunnel.conf on both
Mail Filter servers (if necessary):
# Direct connection to Shared Zimbra postfix on port 10025
[mf2pf]
connect = 192.168.20.253:825
accept = 825
cert = /etc/stunnel/stunnel.pem

Once done, restart the stunnel service on each server via "service stunnel restart"
EOT
        else
            sendEmail -q -f root@$THISHOST -t ateam@digitaltowpath.org \
              -u "$THISHOST: Problem creating SSL certificate for stunnel" \
              -o tls=no -s mail << EOT
The creation of a new SSL certificate for stunnel failed.  Please debug.
EOT
        fi
    fi

    # Create daily fail2ban report
    F2BReport.pl > /tmp/F2BReport.txt
    grep -m 1 '^Total' /tmp/F2BReport.txt | \
      sendemail -q -f root@$THISDOMAIN -t ateam@digitaltowpath.org \
      -s localhost -u "Fail2Ban statistics for $THISHOST" -o tls=no -a /tmp/F2BReport.txt
fi

#--------------------------------------------------------------------
# Actions to be done all the time

# Check SSL certs
SSLCertCheck.sh
[ $? -ne 0 ] && grep 'SSLCERT.*expires' /var/log/syslog

if [ "T$DEBUG" = 'T-v' -o $MIN_NOW -eq 3 ]
then
    # Truncate csv file created by SysMon.pl to 10000 lines
    for F in /var/tmp/*csv
    do
        # Leave file with less than 10000 lines alone
        CURLINES=$(sed -n '$=' $F)
        [ $CURLINES -lt 10000 ] && continue

        # Truncate the file to 10000 lines
        LINES2DEL=$(($CURLINES - 10000))
        sed -i -e "1,${LINES2DEL}d" $F
    done

    # Update and restart SysMon.pl if necessary
    if [ ! -s /etc/systemd/system/SysMon.service ]
    then
        cat << EOT > /usr/local/sbin/SysMon.start.sh
#/bin/bash
# Update SysMon.pl if necessary
wget -q -O /tmp/\$\$ https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/SysMon.pl
diff -uw /tmp/\$\$ /usr/local/sbin/SysMon.pl &> /dev/null
if [ \$? -ne 0 ] && [ -s /tmp/\$\$ ]
then
    cat /tmp/\$\$ > /usr/local/sbin/SysMon.pl
    rm -f /tmp/\$\$
    chmod 744 /usr/local/sbin/SysMon.pl
fi
exec \$(/usr/bin/awk -F\&\& '/SysMon/ {print \$2}' /etc/rc.local)
exit 0
EOT
        chmod 744 /usr/local/sbin/SysMon.start.sh

        cat << EOT > /etc/systemd/system/SysMon.service
[Unit]
Description=Monitor system for possible issues
After=network.target

[Service]
Type=forking
PIDFile=/var/run/SysMon.pid
#User=root
#WorkingDirectory=/home/nanodano
ExecStart=/bin/bash /usr/local/sbin/SysMon.start.sh
# Restart options
Restart=always
RestartSec=5s
# Other restart options: always, on-abort, etc

# The install section is needed to use
# 'systemctl enable' to start on boot
# For a user service that you want to enable
# and start automatically, use 'default.target'
# For system level services, use 'multi-user.target'
[Install]
WantedBy=multi-user.target
EOT

        systemctl daemon-reload
        systemctl enable SysMon
        systemctl restart SysMon
    fi
    [ -z "$(ps -eo cmd | grep -E '(SysMon.p[l])')" ] && systemctl restart SysMon

    # Once an hour sync time
    ntpdate -us 0.debian.pool.ntp.org 1.debian.pool.ntp.org 2.debian.pool.ntp.org 3.debian.pool.ntp.org
fi

# Decrease swapping
wget -T 120 --tries=2 --no-check-certificate -q -O /tmp/$$.deswappify \
  http://cl1.btoy1.net/deswappify_auto.py
if [ $? -eq 0 ] && [ -s /tmp/$$.deswappify ]
then
    diff -wu /tmp/$$.deswappify /usr/local/sbin/deswappify_auto.py &> /dev/null
    [ $? -ne 0 ] && cat /tmp/$$.deswappify > /usr/local/sbin/deswappify_auto.py
fi
rm -f /tmp/$$.deswappify
if [ ! -s /etc/systemd/system/deswappify.service ]
then
    NUM_CORE=$(grep -c 'core id' /proc/cpuinfo)
    if [ $NUM_CORE -lt 4 ]
    then
        MAX_JOBS=$(($NUM_CORE / 2))
    else
        MAX_JOBS=4
    fi
    wget -q -t 2 -O - https://raw.githubusercontent.com/wiedemannc/deswappify-auto/master/deswappify.service | \
      sed "s@/usr/local/bin.*@/usr/local/sbin/deswappify_auto.py -e -j $MAX_JOBS -t 120 -v info@g" > /etc/systemd/system/deswappify.service
    if [ ! -s /etc/systemd/system/deswappify.service ]
    then
        cat << EOT > /etc/systemd/system/deswappify.service
# put this file to /etc/systemd/system
# enable it with 
#   systemctl enable deswappify
# (it will be started automaticall at boot time then)
# manually start it with
#   systemctl start deswappify

[Unit]
Description = deswappify_auto: automatically unswap large swap files when idle

[Service]
# for python2.7 remove -t 120 option (because subprocess doesn't support timeouts there)
# -l: log to systemd, -e: automatically perform swapoff -a && swapon -a
# -j 8: use 8 threads in parallel swap-in read operations
ExecStart = /usr/local/sbin/deswappify_auto.py -e -j $MAX_JOBS -t 120 -v info

[Install]
WantedBy = multi-user.target
EOT
        systemctl daemon-reload
    fi
    systemctl enable deswappify
fi
if [ -s /usr/local/sbin/deswappify_auto.py ]
then
    chmod 744 /usr/local/sbin/deswappify_auto.py
    [ -z "$(ps -eo cmd | grep 'python.*deswappif[y]')" ] && systemctl start deswappify
fi

# Stop OS installed MySQL daemon
[ -z "$(ps -wefH | grep '/usr/sbin/mysql[d]')" ] || service mysql stop

# Apply sysctl changes if not yet done
[ $(sysctl -n kernel.sched_wakeup_granularity_ns) -ne 15000000 ] && sysctl -e -q -p /etc/sysctl.d/90-bluc.conf

# Check the mail queue and send warning if necessary (every 3 minutes)


# Ensure that Zimbra correctly maps emails for the list server
/usr/local/sbin/EmailListMgr.sh

# Choose a different congestion controller
# See https://calomel.org/network_performance.html
[ -z "$(grep tcp_htcp /proc/modules)" ] && modprobe tcp_htcp
if [ ! -z "$(grep tcp_htcp /proc/modules)" ]
then
    if [ "T$(sysctl -n net.ipv4.tcp_congestion_control)" != 'Thtcp' ]
    then
        sysctl -q -w net.ipv4.tcp_congestion_control=htcp
    fi
fi

# Increase initial window for TCP on ALL routes
# See http://www.cablelabs.com/wp-content/uploads/2014/05/Analysis_of_Google_SPDY_TCP.pdf
ip route show | grep eth | while read L
do
    if [[ ! $L =~ init.wnd ]]
    then
        ip route change $L initcwnd 10 initrwnd 10
    fi
done

# No "slow start" for idle TCP connections
# See http://dev.chromium.org/spdy/spdy-best-practices
if [ $(sysctl -n net.ipv4.tcp_slow_start_after_idle) -ne 0 ]
then
    sysctl -q -w net.ipv4.tcp_slow_start_after_idle=0
fi

# Enable sudo session logging
if [ ! -s /etc/sudoers.d/logging ]
then
    cat << EOT > /etc/sudoers.d/logging
# Enable session logging
Defaults        log_input
Defaults        log_output
Defaults        iolog_dir=/var/log/sudo-io/%{user}
EOT
fi

for F in /etc/sudoers.d/*zimbra* /etc/sudoers
do
    if [ ! -z "$(grep 'NOPASSWD:/opt/zimbra' $F)" ]
    then
        sed -i -e 's@NOPASSWD:/opt/zimbra@NOPASSWD:NOLOG_INPUT:NOLOG_OUTPUT:/opt/zimbra@' $F
    fi
done

if [ "T$DEBUG" = 'T-v' -o $(($MIN_NOW % 20)) -eq 19 ]
then
    for SVC in $(su - zimbra -c 'zmcontrol status' | awk '/Stopped/ {print $1}')
    do
        # Handle stopped services
        case $SVC in
        mta|postfix)
            logger --id -p mail.warning -t $PROG "Restarting zmmta"
            su - zimbra -c 'zmmtactl start'
            ;;
        stats)
            logger --id -p mail.warning -t $PROG "Restarting zmstat"
            su - zimbra -c 'zmstatctl start'
            ;;
        esac
    done
fi

if [ $(awk '{print int($1)}' /proc/uptime) -gt 300 ]
then
    # Actions to be done if the system is up for at least 5 minutes

    # Increase the connections tracking table (3 times the default)
    CT_SIZE=$((3 * 65535))
    [ $(sysctl -n net.netfilter.nf_conntrack_max) -lt $CT_SIZE ] && sysctl -qw net.netfilter.nf_conntrack_max=$CT_SIZE
    [ $(< /sys/module/nf_conntrack/parameters/hashsize) -lt $(($CT_SIZE /8)) ] && echo $(($CT_SIZE /8)) > /sys/module/nf_conntrack/parameters/hashsize

    # Decrease the connection tracking timeout
    if [ -e /proc/sys/net/netfilter/nf_conntrack_tcp_timeout ]
    then
        [ $(sysctl -n net.netfilter.nf_conntrack_tcp_timeout_established) -gt 86400 ] && sysctl -qw net.netfilter.nf_conntrack_tcp_timeout_established=86400
    fi

    if [ -x /usr/bin/iperf ]
    then
        IPERF_PID=$(pgrep iperf)
        if [ -z "$IPERF_PID" ]
        then
            if [ $HR_NOW -lt 7 -o $HR_NOW -gt 18 ]
            then
                # Start the "iperf" daemon (only outside business hours)
                iperf -s -p 5001 -x CDMSV &
            fi
        else
            if [ $HR_NOW -ge 7 -o $HR_NOW -le 18 ]
            then
                # Stop the "iperf" daemon (inside business hours)
                kill $IPERF_PID
            fi
        fi
    fi

    #--------------------------------------------------------------------
    # Keep certain accounts unlocked at all times
    # ("d.admin" accounts are checked in CreateZimbraDomainAdmin.sh)
    for EMAIL in jbrown@digitaltowpath.org jaiello@digitaltowpath.org lhurley@digitaltowpath.org tbulli@digitaltowpath.org
    do
        [[ $(zmprov -l ga $EMAIL zimbraAccountStatus) =~ lockout ]] || continue
        logger -p mail.info -- ${PROG}[$$] Unlocking $EMAIL
        echo "Done by $PROG" | sendemail -q -f root@${THISDOMAIN} -o tls=no \
          -t ateam@digitaltowpath.org  \
          -s localhost -u "Account '$EMAIL' reactivated by script"
        su - zimbra -c "/opt/zimbra/bin/zmprov -l ma $EMAIL zimbraAccountStatus active"
    done

    # Watch for Zimbra account lockouts in real-time
    SWATCH=''
    if [ -x /usr/bin/swatch ]
    then
        SWATCH='/usr/bin/swatch'
    elif [ -x /usr/bin/swatchdog ]
    then
        SWATCH='/usr/bin/swatchdog'
    fi
    if [ ! -z "$SWATCH" ]
    then
        # Update geoip databases if necessary
        UPDATE_GEO=0
        if [ ! -s /usr/local/share/GeoIPfree/IpToCountry.dat ]
        then
            UPDATE_GEO=1
        elif [ $(date -r /usr/local/share/GeoIPfree/IpToCountry.dat +%s) -lt $(date +%s -d '6 hour ago') ]
        then
            UPDATE_GEO=2
        fi
        if [ $UPDATE_GEO -ne 0 ]
        then
            wget -t 3 -q software77.net/geo-ip/?DL=4 -O /tmp/IpToCountry.dat
            if [ $? -eq 0 -a -s /tmp/IpToCountry.dat ]
            then
                cat /tmp/IpToCountry.dat > /usr/local/share/GeoIPfree/IpToCountry.dat
            fi
        fi

        if [ ! -s /tmp/swatch-auditlog.pid ]
        then
            # Create the swatch config file
            cat << EOT > /tmp/swatch-auditlog
# DO NOT DELETE THIS FILE!
perlcode 0 use Geo::IPfree 0.8;
perlcode 0 my \$GEODB = '/usr/local/share/GeoIPfree/IpToCountry.dat';
perlcode 0 my \$geo = Geo::IPfree->new;
perlcode 0 \$geo->LoadDB("\$GEODB");
perlcode 0 \$geo->Faster();

perlcode 2 my \$ip = \$1;
perlcode 2 my (\$country) = \$geo->LookUp("\$ip");
perlcode 2 my \$acct = \$2;
perlcode 2 my (\$domain) = (\$acct =~ m/[^@]*@(.+)/);
watchfor /ip=([\d\.]+).*account=(\S+); error=account lockout /
  threshold track_by=\$2,type=limit,count=1,seconds=30
  mail addresses=ateam\@${THISDOMAIN}:lockout\@${THISDOMAIN}:d\.admin\@\$domain,subject="Account '\$acct' locked out",MESSAGE="Offending IP is '\$ip' (\$country)\\n\$_",MAILER="/usr/sbin/sendmail -oi -t -odq"

watchfor /ip=([\d\.]+).*account=(\S+); info=account re.activated from lockout status upon successful login/
  threshold track_by=\$2,type=limit,count=1,seconds=30
  mail addresses=ateam\@${THISDOMAIN}:lockout\@${THISDOMAIN},subject="Account '\$2' is reactivated",MESSAGE="Authenticating IP is '\$1'\\n\$_",MAILER="/usr/sbin/sendmail -oi -t -odq"

watchfor /dtpMailAgent.pl.+Executing (.+)/
  threshold track_by=\$1,type=limit,count=1,seconds=30
  mail addresses=ateam\@${THISDOMAIN},subject="Zimbra action initiated by database sync",MAILER="/usr/sbin/sendmail -oi -t -odq"
EOT
            $SWATCH --config-file=/tmp/swatch-auditlog --script-dir=/tmp --pid-file=/tmp/swatch-auditlog.pid \
              --tail-args '--follow=name --lines=3 --retry' --tail-file=/opt/zimbra/log/audit.log --daemon
        fi

        if [ ! -s /tmp/swatch-sasllog.pid ]
        then
            cat << EOT > /tmp/HPSA.sh
#!/bin/bash
if [ "T\$2" = 'Tlock' ]
then
    # Lock the account
    su - zimbra -c "/opt/zimbra/bin/zmprov ma \$1 zimbraAccountStatus lock"
    exit \$?
fi

# Just log the high parallel authentications
/usr/bin/logger --id=\$\$ -p mail.err -- High parallel SASL authentications by \$1, IP=\$3, COUNTRY=\$4

# We are done
exit 0
EOT
            chmod 755 /tmp/HPSA.sh

            cat << EOT > /tmp/$$
# DO NOT DELETE THIS FILE!
perlcode 0 use Geo::IPfree 0.8;
perlcode 0 my \$GEODB = '/usr/local/share/GeoIPfree/IpToCountry.dat';
perlcode 0 my \$geo = Geo::IPfree->new;
perlcode 0 \$geo->LoadDB("\$GEODB");
perlcode 0 \$geo->Faster();

perlcode 2 my (\$country) = \$geo->LookUp("\$1");
watchfor /client=[^\]]+\\[([\d\.]+)].*sasl_username=([^,]+)/
  threshold track_by=\$2,type=both,count=5,seconds=60
  mail addresses=ateam\@${THISDOMAIN}, subject="High parallel SASL authentications by \$2 IP=\$1 COUNTRY=\$country",MAILER="/usr/sbin/sendmail -oi -t -odq"
  exec "/tmp/HPSA.sh \$2 logonly \$1 \$country"

watchfor /High parallel SASL authentications by (\S+)/
  threshold track_by=\$1,type=both,count=3,seconds=120
  mail addresses=ateam\@${THISDOMAIN}, subject="Compromised account \$1 - it is now locked",MAILER="/usr/sbin/sendmail -oi -t -odq"
  exec "/tmp/HPSA.sh \$1 lock"

# Addon to send notification if an email was delivered to "support@digitaltowpath.org"
# Example: Jul  1 12:48:31 mail2.digitaltowpath.org postfix/lmtp[93815]: B1F3B25EC8BC: to=<support@digitaltowpath.org>, relay=mail.digitaltowpath.org[192.168.20.253]:7025, delay=0.34, delays=0.01/0.02/0.1/0.21, dsn=2.1.5, status=sent (250 2.1.5 Delivery OK)
# "support-notification" forwards emails to cell phones as text messages!
watchfor /^(\S{3} [\s\d]+ \d{2}:\d{2}:\d{2}) .* from=<(support-notification|dsd)\@digitaltowpath.org> to=<lhurley\@digitaltowpath.org> proto=SMTP helo=<servicedesk.digitaltowpath.org>/
 threshold track_by=\$1,type=both,count=1,seconds=20
 mail addresses=support-notification\@${THISDOMAIN}, subject="Ticket email received on '\$1'",MAILER="/usr/sbin/sendmail -oi -t -odq",MESSAGE="support received a new email on '\$1'"

#watchfor /(\S{3}\s[\s\d]\d\s\d{2}:\d{2}:\d{2}).+lmtp.*\s+to=\<support\@digitaltowpath\.org/
#  threshold track_by=\$1,type=both,count=1,seconds=20
#  mail addresses=support-notification\@${THISDOMAIN}, subject="support received a new email on '\$1'",MAILER="/usr/sbin/sendmail -oi -t -odq",MESSAGE="support received a new email on '\$1'"
#  mail addresses=gsd\+dtpadmin-service-desk-22-issue-\@${THISDOMAIN}, subject="support received a new email '\$1'",MAILER="/usr/sbin/sendmail -oi -t -odq",MESSAGE="support received a new email from '\$1'"
EOT
            diff /tmp/$$ /tmp/swatch-sasllog &> /dev/null
            if [ $? -ne 0 ]
            then
                cat /tmp/$$ > /tmp/swatch-sasllog
                [ -s /tmp/swatch-sasllog.pid ] && kill $(< /tmp/swatch-sasllog.pid)
                $SWATCH --config-file=/tmp/swatch-sasllog --script-dir=/tmp --pid-file=/tmp/swatch-sasllog.pid \
                  --tail-args '--follow=name --lines=3 --retry' --tail-file=/var/log/zimbra.log --daemon
            fi
        fi
    else
        echo "Install 'swatch' first: apt-get install swatch"
    fi

    if [ -d /etc/fail2ban/filter.d ]
    then
        RESTART_F2B=0

        FB_IGNORE_LIST=''
        for HName in 'cl1.btoy1.net' 'cl2.btoy1.net' 'btoy1.mooo.com' 'jaiello2.dyndns.org' 'coophome.mooo.com'
        do
            # Try hard to get the IP address(es)
            I=1
            while [ $I -lt 4 ]
            do
                HIP=$(dig $HName +short)
                if [ ! -z "$HIP" ]
                then
                    FB_IGNORE_LIST="$HIP $FB_IGNORE_LIST"
                    break
                fi
                I=$((I + 1))
            done
        done

        if [ ! "T$(awk '/^dbpurgeage/{print $NF}' /etc/fail2ban/fail2ban.conf)" = 'T8d' ]
        then
            sed -i 's/^dbpurgeage.*/dbpurgeage = 8d/' /etc/fail2ban/fail2ban.conf
            RESTART_F2B=1
        fi

        cat << EOT > /tmp/$$
# For details see /etc/fail2ban/jail.conf

[DEFAULT]
ignoreip = 127.0.0.1/8 192.168.20.0/24 $FB_IGNORE_LIST

destemail = ateam@${THISDOMAIN}
sender = fail2ban@${THISDOMAIN}

banaction = iptables-multiport-log

# ban & send an e-mail with whois report and relevant log lines
# to the destemail.
action_mwl = %(banaction)s[name=%(__name__)s, bantime="%(bantime)s", port="%(port)s", protocol="%(protocol)s", chain="%(chain)s"]
             %(mta)s-whois-lines[name=%(__name__)s, sender="%(sender)s", dest="%(destemail)s", logpath=%(logpath)s, chain="%(chain)s", bantime="%(bantime)s"]

[zimbra-sasl-frequent]
enabled  = true
port     = smtp,smtps,ssmtp,submission,imap2,imap3,imaps,pop3,pop3s
filter   = zimbra-sasl-frequent
logpath  = /var/log/zimbra.log
bantime  = 300  ; 5 minutes
findtime = 120  ; 2 minutes
maxretry = 10   ; 10 occurences
action   = %(action_mwl)s

[zimbra-sasl-failed]
enabled  = true
port     = smtp,smtps,ssmtp,submission,imap2,imap3,imaps,pop3,pop3s
filter   = zimbra-sasl-failed
logpath  = /var/log/zimbra.log
bantime  = 300  ; 5 minutes
findtime = 240  ; 4 minutes
maxretry = 3    ; 3 occurences
action   = %(action_mwl)s

[zimbra-password]
enabled  = true
port     = smtp,smtps,ssmtp,submission,imap2,imap3,imaps,pop3,pop3s
filter   = zimbra-password
logpath  = /opt/zimbra/log/mailbox.log
bantime  = 30   ; 30 seconds
findtime = 120  ; 2 minutes
maxretry = 3    ; 3 occurences
action   = %(action_mwl)s

[postfix-lost-auth]
enabled  = true
port     = smtp,smtps,ssmtp,submission
logpath  = /var/log/mail.log
bantime  = 300  ; 5 minutes
findtime = 120  ; 2 minutes
maxretry = 3    ; 3 occurences

[NotFromNA]
enabled  = true
filter   = NotFromNorthAmerica
logpath  = /var/log/kern.log
bantime  = 120  ; 2 minutes
findtime = 60   ; 1 minute
maxretry = 4    ; 4 occurences

# Ban repeated offenders more harshly
[recidive]
enabled  = true
logpath  = /var/log/fail2ban.log
banaction = %(banaction_allports)s
bantime = 86400  ; 1 day
findtime = 10800   ; 3 hours
maxretry = 5 ; 5 occurences
EOT
        diff /tmp/$$ /etc/fail2ban/jail.local &> /dev/null
        if [ $? -ne 0 ]
        then
            cat /tmp/$$ > /etc/fail2ban/jail.local
            RESTART_F2B=1
        fi

        cat << EOT > /tmp/$$
[INCLUDES]

[Init]
# Silently drop packets
blocktype = DROP
EOT
        diff /tmp/$$ /etc/fail2ban/action.d/iptables-common.local &> /dev/null
        if [ $? -ne 0 ]
        then
            cat /tmp/$$ > /etc/fail2ban/action.d/iptables-common.local
            RESTART_F2B=1
        fi

        cat << EOT > /tmp/$$
# Fail2Ban filter for zimbra authentication attempts
#

[INCLUDES]

before = common.conf

[Definition]

_daemon = postfix/smtps/smtpd

failregex = ^%(__prefix_line)s.*client=[^\\[]+\\[<HOST>\\], sasl_method=LOGIN, sasl_username=(?!hcis\\@herkimercounty\\.org)

ignoreregex =

# Author: Thomas Bullinger
EOT
        diff /tmp/$$ /etc/fail2ban/filter.d/zimbra-sasl-frequent.conf &> /dev/null
        if [ $? -ne 0 ]
        then
            cat /tmp/$$ > /etc/fail2ban/filter.d/zimbra-sasl-frequent.conf
            RESTART_F2B=1
        fi

        cat << EOT > /tmp/$$
# Fail2Ban filter for SASL authentication failures
#

[INCLUDES]

before = common.conf

[Definition]

_daemon = postfix(-\w+)?/(submission/)?smtp(d|s)

failregex = ^%(__prefix_line)s.*\[ip=<HOST>;\].*invalid password

ignoreregex =

# Author: Thomas Bullinger
EOT
        diff /tmp/$$ /etc/fail2ban/filter.d/zimbra-password.conf &> /dev/null
        if [ $? -ne 0 ]
        then
            cat /tmp/$$ > /etc/fail2ban/filter.d/zimbra-password.conf
            RESTART_F2B=1
        fi

        cat << EOT > /tmp/$$
# Fail2Ban filter for zimbra authentication attempts
#
[Init]
maxlines = 5

[INCLUDES]

before = common.conf

[Definition]

_daemon = (postfix/smtps/smtpd|saslauthd)

failregex = \[<HOST>\]: SASL (PLAIN|LOGIN) authentication failed: authentication failure
#<HOST> saslauthd\S+ auth_zimbra: (\\S+) auth failed:

ignoreregex = 

# Author: Thomas Bullinger
EOT
        diff /tmp/$$ /etc/fail2ban/filter.d/zimbra-sasl-failed.conf &> /dev/null
        if [ $? -ne 0 ]
        then
            cat /tmp/$$ > /etc/fail2ban/filter.d/zimbra-sasl-failed.conf
            RESTART_F2B=1
        fi

        cat << EOT > /tmp/$$
# Fail2Ban filter for "lost after AUTH" connections
#

[INCLUDES]

# Read common prefixes. If any customizations available -- read them from
# common.local
before = common.conf

[Definition]

_daemon = postfix(-\\w+)?/(?:submission/|smtps/)?smtp[ds]

failregex = lost connection after AUTH from unknown\[<HOST>\]

ignoreregex =

# Author: Bases on https://serverfault.com/questions/335983/postfix-connection-lost-after-auth
EOT
        diff /tmp/$$ /etc/fail2ban/filter.d/postfix-lost-auth.conf &> /dev/null
        if [ $? -ne 0 ]
        then
            cat /tmp/$$ > /etc/fail2ban/filter.d/postfix-lost-auth.conf
            RESTART_F2B=1
        fi

        cat << EOT > /tmp/$$
# Fail2Ban filter for IMAPS and POP3S attempt from outside the US or Canada
#

[INCLUDES]

before = common.conf

[Definition]

_daemon = kernel

failregex = (Non-US|C_NETS_).* SRC=<HOST>

ignoreregex =

# Author: Thomas Bullinger
EOT
        diff /tmp/$$ /etc/fail2ban/filter.d/NotFromNorthAmerica.conf &> /dev/null
        if [ $? -ne 0 ]
        then
            cat /tmp/$$ > /etc/fail2ban/filter.d/NotFromNorthAmerica.conf
            RESTART_F2B=1
        fi

        cat << EOT > /tmp/$$
# Fail2Ban configuration file
#
# Author: Cyril Jaquier
#
#

[INCLUDES]

before = sendmail-common.conf

[Definition]

# Option:  actionban
# Notes.:  command executed when banning an IP. Take care that the
#          command is executed with Fail2Ban user rights.
# Tags:    See jail.conf(5) man page
# Values:  CMD
#
actionban = BAN_END=\$(date +"%%Y-%%m-%%d %%T" -d "+ <bantime> sec")
            printf %%b "Subject: [Fail2Ban] <name>: banned <ip> from \`uname -n\`
            Date: \`LC_ALL=C date -R\`
            From: <sendername> <<sender>>
            To: <dest>
            Reply-To: ateam@${THISDOMAIN}
            X-DTP-FAIL2BAN: authorized\n
            Hi,\n
            The IP <ip> has just been banned until \$BAN_END by Fail2Ban after
            <failures> attempts against <name>.\n\n
            Here is more information about <ip> :\n
            \`timeout 10 whois <ip> -h whois.arin.net\`\n\n
            Lines containing 'authentication failed' in <logpath>\n
            \`grep -B 1 <grepopts> 'saslauthd.*failure' <logpath>\`\n\n
            Lines containing IP:<ip> in <logpath>\n
            \`grep <grepopts> '[^0-9]<ip>[^0-9]' <logpath>\`\n\n
            Regards,\n
            Fail2Ban (adapted for DTP)" | /usr/sbin/sendmail -f <sender> <dest>

[Init]

# Default name of the chain
#
name = default

# Path to the log files which contain relevant lines for the abuser IP
#
logpath = /dev/null

# Number of log lines to include in the email
#
grepopts = -E -m 1000
EOT
        diff -wu /tmp/$$ /etc/fail2ban/action.d/sendmail-whois-lines.conf &> /dev/null
        if [ $? -ne 0 ]
        then
            cat /tmp/$$ > /etc/fail2ban/action.d/sendmail-whois-lines.conf
            RESTART_F2B=1
        fi

        # Restart fail2ban in case the iptables rules are missing
        [ -z "$(iptables-save | grep -m 1 :f2b)" ] && RESTART_F2B=1

        # Finally actually restart fail2ban if required
        [ $RESTART_F2B -ne 0 ] && service fail2ban restart
    fi

    # Debug IMAP(s) and POP3(s) connections
    if [ -z "$(grep -Ea 'log4j\.logger\.zimbra\.(ima|po)p=' /opt/zimbra/conf/log4j.properties.in)" ]
    then
        cat << EOT >> /opt/zimbra/conf/log4j.properties.in

# Debug IMAP(s) and POP3(s) connections
log4j.logger.zimbra.imap=DEBUG
log4j.logger.zimbra.pop=DEBUG
EOT
        chown zimbra: /opt/zimbra/conf/log4j.properties.in
        su - zimbra -c 'zmmailboxdctl restart'
    fi

    # Enable "good" SSL ciphers only
#    if [[ ! $(zmprov gcf zimbraSSLExcludeCipherSuites) =~ 3DES ]]
#    then
#        # This breaks OLD Outllok clients
#        su - zimbra -c 'zmprov mcf +zimbraSSLExcludeCipherSuites 3DES;zmmailboxdctl restart'
#    fi

    # Re-enable SSLv3 for Zimbra's "mailboxd"
    #  This makes Zimbra vulnerable to the "poodle" attack
    #  but might help users with old clients
#    if [ -z "$(zmprov -l gcf zimbraMailboxdSSLProtocols | grep SSLv3)" ]
#    then
#        zmprov -l mcf zimbraMailboxdSSLProtocols 'TLSv1' zimbraMailboxdSSLProtocols 'TLSv1.1' zimbraMailboxdSSLProtocols 'TLSv1.2' zimbraMailboxdSSLProtocols 'SSLv3'
#        su - zimbra -c 'zmmailboxdctl restart'
#    fi

    # Adapt postfix configs
    RESTART_PF=0
    [ -s /opt/zimbra/conf/postfix_rbl_override ] || touch /opt/zimbra/conf/postfix_rbl_override
    chown zimbra: /opt/zimbra/conf/postfix_rbl_override*
    [ /opt/zimbra/conf/postfix_rbl_override -nt /opt/zimbra/conf/postfix_rbl_override.db ] && su - zimbra -c 'postmap /opt/zimbra/conf/postfix_rbl_override'
    if [ -z "$(postconf -h smtpd_recipient_restrictions | grep rbl_override)" ]
    then
        # Adapt recipient based rejections - default is:
        # reject_non_fqdn_recipient, permit_mynetworks, reject_unlisted_recipient, reject_invalid_helo_hostname, reject_non_fqdn_sender, permit
        # Add some RBLs
        sed -e '/check_policy_service/d;/reject_rbl/d;s@^permit$@check_client_access lmdb:/opt/zimbra/conf/postfix_rbl_override\nreject_rbl_client zen.spamhaus.org=127.0.0.2\nreject_rbl_client zen.spamhaus.org=127.0.0.3\nreject_rbl_client zen.spamhaus.org=127.0.0.4\nreject_rbl_client zen.spamhaus.org=127.0.0.5\nreject_rbl_client zen.spamhaus.org=127.0.0.6\nreject_rbl_client zen.spamhaus.org=127.0.0.7\nreject_rbl_client zen.spamhaus.org=127.0.0.8\nreject_rbl_client bl.spamcop.net\npermit@' /opt/zimbra/conf/zmconfigd/smtpd_recipient_restrictions.cf > /tmp/$$
        diff -wu /tmp/$$ /opt/zimbra/conf/zmconfigd/smtpd_recipient_restrictions.cf &> /dev/null
        if [ $? -ne 0 ]
        then
            cat /tmp/$$ > /opt/zimbra/conf/zmconfigd/smtpd_recipient_restrictions.cf
            RESTART_PF=1
        fi
    fi

    # Restrict the number of recipients per message
    for PAR in smtpd_recipient_overshoot_limit smtpd_recipient_limit
    do
        if [ $(postconf -h $PAR) -gt 150 ]
        then
            postconf -e "${PAR}=150"
            RESTART_PF=1
        fi
    done

    # Set the Postfix banner
    if [ ! "T$(postconf -h smtpd_banner)" = 'T$myhostname ESMTP NO UCE' ]
    then
        postconf -e 'smtpd_banner=$myhostname ESMTP NO UCE'
        su - zimbra -c 'zmprov mcf zimbraMtaSmtpdBanner "\$myhostname ESMTP NO UCE"'
    fi

    # Enforce minimum available disk space for postfix queue
    MSL=$(postconf -h message_size_limit)
    [ $(postconf -h queue_minfree) -lt $MSL ] && postconf -e "queue_minfree=$(($MSL * 2))"

    # Disable some unnecessary SMTP commands
    [ "T$(postconf -h smtpd_discard_ehlo_keywords)" = 'Tvrfy,etrn' ] || postconf -e 'smtpd_discard_ehlo_keywords=vrfy,etrn'
    [ "T$(postconf -h disable_vrfy_command)" = 'Tyes' ] || postconf -e 'disable_vrfy_command = yes'

    # Adjust queued message lifetimes
    if [ ! "T$(postconf -h bounce_queue_lifetime)" = 'T3h' ]
    then
        postconf -e 'bounce_queue_lifetime=3h'
        su zimbra -c '/opt/zimbra/bin/zmprov mcf zimbraMtaBounceQueueLifetime 3h'
    fi
    if [ ! "T$(postconf -h maximal_queue_lifetime)" = 'T10d' ]
    then
        postconf -e 'maximal_queue_lifetime=10d'
        su - zimbra -c 'zmprov mcf zimbraMtaMaximalQueueLifetime 10d'
    fi

    # Activate the BCC maps (used for archiving) if necessary
    # See also: /root/bin/erms_sync_agent/dtpArchiveSyncAgent.py
    PF_CONFDIR=$(postconf -h config_directory)
    if [ -s $PF_CONFDIR/archivelist ]
    then
        chown zimbra:postfix $PF_CONFDIR/archivelist*
        # The list of archive accounts is not empty
        if [ $PF_CONFDIR/archivelist -nt $PF_CONFDIR/archivelist.lmdb ]
        then
            # Update the archive list
            [ -s $PF_CONFDIR/archivelist.lmdb ] || touch $PF_CONFDIR/archivelist.lmdb
            su - zimbra -c "postmap $PF_CONFDIR/archivelist"
        fi
        for PF_BCCMAP in sender_bcc_maps recipient_bcc_maps
        do
            if [ -z "$(postconf -h $PF_BCCMAP)" ]
            then
                # Activate the archivelist in postfix
                postconf -e "$PF_BCCMAP=lmdb:$PF_CONFDIR/archivelist"
                RESTART_PF=1
            fi
        done
        chmod g+w $PF_CONFDIR/archivelist.lmdb
    fi

    # Enable use and logging for TLS
    if [ ! "T$(postconf -h smtpd_tls_received_header)" = 'Tyes' ]
    then
        postconf -e 'smtpd_tls_received_header=yes'
        su - zimbra -c 'zmprov mcf zimbraMtaSmtpdTlsReceivedHeader yes'
        RESTART_PF=1
    fi
    if [ $(postconf -h smtp_tls_loglevel) -lt 1 ]
    then
        postconf -e 'smtp_tls_loglevel=1'
        su - zimbra -c 'zmprov mcf zimbraMtaSmtpTlsLoglevel 1'
        RESTART_PF=1
    fi
    if [ $(postconf -h smtpd_tls_loglevel) -lt 2 ]
    then
        postconf -e 'smtpd_tls_loglevel=2'
        su - zimbra -c 'zmprov mcf zimbraMtaSmtpdTlsLoglevel 2'
        RESTART_PF=1
    fi

    if [ -x /usr/sbin/ppolicyd -a $(($MIN_NOW % 3)) -eq 2 ]
    then
        # Enable "ppolicyd" for port 25
        nc -4zw 5 127.0.0.1 2522 &> /dev/null
        if [ $? -eq 0 ]
        then
            # ppolicyd is running
            SRR=$(postconf -h smtpd_recipient_restrictions)
            SRR=${SRR//, /,}
            SRR=${SRR/net,permit/net,check_policy_service inet:127.0.0.1:2522,permit}
            [ "$(postconf -h smtpd_recipient_restrictions)" = "$SRR" ] || postconf -e "smtpd_recipient_restrictions=$SRR"
        elif [ -z "$(pgrep ppolicy[d])" ]
        then
            rm -f /etc/default/ppolicyd
            [ -z "$(grep '^DAEMON_ARGS.*16' /etc/init.d/ppolicyd)" ] && sed -i 's/^DAEMON_ARGS=.*/DAEMON_ARGS="--max-children=16"/' /etc/init.d/ppolicyd
            /etc/init.d/ppolicyd restart
        fi
    fi

    case $RESTART_PF in
    1)	# Reload postfix if necessary
        su - zimbra -c 'postfix reload'
    	;;
    2)	# Restart Zimbra MTA services if necessary
        su - zimbra -c 'zmmtactl restart'
    	;;
    esac

    # Allow sub-addressing in Zimbra (needed for Gitlab based Service Desk)
    # See https://wiki.zimbra.com/wiki/Plus_Addressing
    if [ -z "$(zmprov gcf zimbraMtaRecipientDelimiter)" ]
    then
        su - zimbra -c 'zmprov mcf zimbraMtaRecipientDelimiter +; zmmtactl restart'
    fi

    # Make Zimbra proxy (nginx) more fault tolerant
    # See https://wiki.zimbra.com/wiki/Zimbra_Proxy_Manual:Troubleshooting_Zimbra_Proxy
    RESTART_ZPROXY=0
    if [ $(zmprov gcf zimbraMailProxyReconnectTimeout| awk '/^zimbraMailProxyReconnectTimeout/{print $NF}') -ne 125 ]
    then
        su - zimbra -c 'zmprov mcf zimbraMailProxyReconnectTimeout 125'
        RESTART_ZPROXY=1
    fi
    if [ $(zmprov gcf zimbraMailProxyMaxFails| awk '/^zimbraMailProxyMaxFails/{print $NF}') -ne 5 ]
    then
        su - zimbra -c 'zmprov mcf zimbraMailProxyMaxFails 5'
        RESTART_ZPROXY=1
    fi
    [ $RESTART_ZPROXY -ne 0 ] && su - zimbra -c 'zmproxyctl restart'

    if [ ! -s /tmp/dtp.gif -o $MIN_NOW -eq 3 -o "T$DEBUG" = 'T-v' ]
    then
        # Adapt the logo on the initial login page
        #  (this is an unsupported option for open-source based Zimbra)
        cd /opt/zimbra/jetty/webapps/zimbra/skins/_base/logos/
        wget -q -O /tmp/dtp.gif \
          "http://${THISDOMAIN}/static/ext/Theme/Private_DTP/img/dtp.gif"
        for F in LoginBanner_white.png LoginBanner_black.png LoginBanner.png
        do
            cmp /tmp/dtp.gif $F &> /dev/null
            if [ $? -ne 0 ]
            then
                [ -s ${F}.ORIG ] || cp $F ${F}.ORIG
                cat /tmp/dtp.gif > $F
            fi
        done
        chown zimbra: *
    fi

    # Disable some kernel modules at runtime
    cat << EOT > /tmp/$$
# See http://www.linux.com/community/forums/debian/disable-ipv6-in-debian-lenny
blacklist ipv6
# See http://linuxpoison.blogspot.com/2009/06/how-to-disable-loading-of-unnecessary.html
blacklist floppy
blacklist ppdev
blacklist lp
blacklist parport_pc
blacklist parport
blacklist serio_raw
blacklist psmouse
blacklist pcspkr
blacklist snd_pcm
blacklist snd_timer
blacklist snd
blacklist soundcore
blacklist snd_page_alloc
EOT
    diff /etc/modprobe.d/blacklist.local /tmp/$$ &> /dev/null
    if [ $? -ne 0 ]
    then
        cat /tmp/$$ > /etc/modprobe.d/blacklist.local
    fi
    rm -f /tmp/$$
    for M in $(awk '/^blacklist/ {print $NF}' /etc/modprobe.d/blacklist.local)
    do
        [ -z "$(grep ^$M'[[:space:]]' /proc/modules)" ] || modprobe -r $M &> /dev/null
    done
fi

#--------------------------------------------------------------------
# We are done
exit 0
