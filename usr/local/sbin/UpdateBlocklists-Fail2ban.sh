#!/bin/bash
# Version 20240121-215305 checked into repository
################################################################
# (c) Copyright 2017 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/UpdateBlocklists-Fail2ban.sh

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=${0##*/}

dpkg-query -s fail2ban &> /dev/null
if [ $? -ne 0 ]
then
    echo "ERROR: 'fail2ban' is not installed"
    exit 1
fi

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE

# Global variables
THISHOST=$(hostname)
[[ $THISHOST == *.* ]] || THISHOST=$(hostname -f)
DEBUG='-q'
[[ $- ==  *x* ]] && DEBUG='-v'

# The level of blocking
# 1 - minimum
# 2 - medium
# 3 - all
BLK_LEVEL=1

# Force update
FORCE=0
TFILE=$(mktemp /tmp/$$XXXXXXXX)

# Get possible program options
while getopts L:f OPTION
do
    case ${OPTION} in
        f)  FORCE=1
            ;;
        L)  BLK_LEVEL=$(printf "%d" $OPTARG 2> /dev/null)
            [ $BLK_LEVEL -lt 1 ] && BLK_LEVEL=1
            [ $BLK_LEVEL -gt 3 ] && BLK_LEVEL=3
            ;;
        *)  echo "Usage: $0 [-L [1|2|3]] (default=$BLK_LEVEL)"
            ;;
    esac
done
shift $((OPTIND - 1))

# Preset blocklists if present
if [ $FORCE -eq 0 ]
then
    TRY=0
    while [[ ! $(iptables -nvL INPUT) =~ RH-Lokkit-0-50-INPUT ]]
    do
        # Wait until firehol has fully initialized
        TRY=$((TRY + 1))
        [ $TRY -gt 30 ] && exit 1
        sleep 10
    done

    # Don't do any update unless it was not yet done
    #  or not done for the past 24 hours
    if [ -s /tmp/Blocklist.lastupdate ]
    then
        LU=$(date +%s -f /tmp/Blocklist.lastupdate)
        [ $((EPOCHSECONDS - 86400)) -lt $LU ] && exit 0
    fi
fi

#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Get IP addresses and network for bad actors
bash UpdateBlocklists.sh -L $BLK_LEVEL

# Convert ALL IP addresses into CIDR notation
sort -u /tmp/AllBlacklistedHosts.txt /tmp/AllBlacklistedNets.txt | \
    aggregate $DEBUG -t -p 32 -m 32 -o 32 > $TFILE.AllBlacklistedIPAddresses.cidr
[ "T$DEBUG" = 'T-v' ] && less $TFILE.AllBlacklistedIPAddresses.cidr

# Extract single IP addresses
awk '/\/32/{sub(/\/32/,"",$1);print $1}' $TFILE.AllBlacklistedIPAddresses.cidr > $TFILE.AllBlacklistedIPAddresses.txt

# Expand all subnets
grep -v '/32' $TFILE.AllBlacklistedIPAddresses.cidr | awk '{print $NF}' | xargs -n 1 prips >> $TFILE.AllBlacklistedIPAddresses.txt
[ "T$DEBUG" = 'T-v' ] && wc -l $TFILE.AllBlacklistedIPAddresses.txt
[ "T$DEBUG" = 'T-v' ] && less $TFILE.AllBlacklistedIPAddresses.txt

# Bail if we don't have any data
[ -s $TFILE.AllBlacklistedIPAddresses.txt ] || exit 0

# Convert the IP addresses into (pseudo) log lines
#  so that "fail2ban" can pick them up
[ -s /var/tmp/BLOCKLIST.fail2ban ] && mv -f /var/tmp/BLOCKLIST.fail2ban /var/tmp/BLOCKLIST.fail2ban.old
awk '{print strftime("%F %T block this ip: ")$1}' $TFILE.AllBlacklistedIPAddresses.txt > /var/tmp/BLOCKLIST.fail2ban

# Create a suitable filter for fail2ban
cat << EOT > /etc/fail2ban/filter.d/BLOCKLIST.conf
# See https://www.mankier.com/5/jail.conf
[INCLUDES]
before = common.conf

[Definition]
_daemon=iptables
failregex = block this ip: <HOST>
ignoreregex =
EOT

# Create a suitable action for fail2ban
cat << EOT > /etc/fail2ban/action.d/mail-start-stop.conf
# See https://www.mankier.com/5/jail.conf
[Definition]
actionstart = printf %%b "Hi,\n
              The jail <name> has been started successfully.\n
              Regards,\n
              Fail2Ban"|mail -s "[Fail2Ban] <name>: started" <dest>

actionstop = printf %%b "Hi,\n
             The jail <name> has been stopped.\n
             Regards,\n
             Fail2Ban"|mail -s "[Fail2Ban] <name>: stopped" <dest>

actioncheck =
actionban =
actionunban =

[Init]
name = default
dest = root

EOT

RESTART_FAIL2BAN=0
if [ ! -s /etc/fail2ban/jail.local ]
then
    # Create a local jail conf file
    grep -v '^[[:space:]]*#' /etc/fail2ban/jail.conf > /etc/fail2ban/jail.local
    RESTART_FAIL2BAN=1
fi
if [ -z "$(grep blocklist /etc/fail2ban/jail.local)" ]
then
    cat << EOT >> /etc/fail2ban/jail.local

# See https://www.mankier.com/5/jail.conf
[blocklist]

enabled = true
protocol = all
port = 0:65535
filter  = BLOCKLIST
logpath = /var/tmp/BLOCKLIST.fail2ban
maxretry = 1
bantime = 86400
findtime = 86400
action = iptables-allports[name=%(__name__)s, port=%(port)s, protocol=all]
         mail-start-stop[name=%(__name__)s, dest=%(destemail)s]

EOT
    RESTART_FAIL2BAN=1
fi
[ $RESTART_FAIL2BAN -ne 0 ] && /etc/init.d/fail2ban restart

# We are done
exit 0
