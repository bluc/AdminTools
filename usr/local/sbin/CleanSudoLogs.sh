#!/bin/bash
# Version 20240124-235610 checked into repository
#--------------------------------------------------------------------
# (c) CopyRight 2022 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#--------------------------------------------------------------------
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/CleanSudoLogs.sh
# Recommended installation:
# Run as a cron job every minute, e.g.
# * * * * *       root    [ -x /usr/local/sbin/CleanSudoLogs.sh ] && /usr/local/sbin/CleanSudoLogs.sh 
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=${0##*/}

#-------------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi
trap "rm -rf $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE

# Use "nocache" for find if possible
NOCACHE=''

#-------------------------------------------------------------------------
# Keep the log files to a manageable amount (max. 7 days)
SUDO_LOG_DIR=$(sudo -V | awk -F': ' '/Directory in which to store .* logs/{gsub(/%{user}/,"");print $NF}')
[ -d ${SUDO_LOG_DIR%/*} ] || exit 0
if [ $(df -i "${SUDO_LOG_DIR%/*}" | awk '$2 ~ /^[0-9]+$/{print int($5)}') -gt 20 ]
then
    # We use at least 20% of all available inodes
    dpkg-query -S nocache &> /dev/null && NOCACHE='nocache'

    # Delete any file that is too old
    nice -n 19 $NOCACHE find ${SUDO_LOG_DIR%/*} -type f -mtime +7 -delete

    # Remove empty directories
    nice -n 19 $NOCACHE find ${SUDO_LOG_DIR%/*} -ignore_readdir_race -type d -empty -delete
fi

#-------------------------------------------------------------------------
# We are done
exit 0
