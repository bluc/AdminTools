#!/usr/bin/perl -w
# Version 20230122-044037 checked into repository
#--------------------------------------------------------------------
# (c) CopyRight 2020 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#--------------------------------------------------------------------
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/F2BReport.pl
use 5.0010;

# Constants
my $ProgName = '';
( $ProgName = $0 ) =~ s%.*/%%;
my $ProgVers = 'unknown';
if ( open( my $GPV, "$0" ) )
{
    while (<$GPV>)
    {
        if (/# Version ([\d\-]*) checked/o)
        {
            $ProgVers = "$1";
            chomp($ProgVers);
            last;
        } ## end if (/[\d\-]*/o)
    } ## end while (<$GPV>)
    close($GPV);
} ## end if ( open( my $GPV, '-|'...))
my $CopyRight = "(c) CopyRight 2020 B-LUC Consulting Thomas Bullinger";

# Common sense options:
use strict qw(vars subs);
no warnings;
use warnings qw(FATAL closed threads internal debugging pack
    portable prototype inplace io pipe unpack malloc
    glob digit printf layer reserved taint closure
    semicolon);
no warnings qw(exec newline unopened);

#--------------------------------------------------------------------
# Needed packages
#--------------------------------------------------------------------
use Getopt::Std;
use File::Temp;

#--------------------------------------------------------------------
# Globals
#--------------------------------------------------------------------
local $ENV{PATH} = '/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin';

# Program options
our $opt_d = 0;
our $opt_D = 'yesterday';
our $opt_h = 0;

# External programs
my $DATE        = '/bin/date';
my $GET_COUNTRY = '';
my $LWPAgent;
if ( -x '/usr/bin/mmdblookup' )
{
    $GET_COUNTRY = '/usr/bin/mmdblookup';
} elsif ( -x '/usr/bin/GET' )
{
    require LWP;
    $LWPAgent = LWP::UserAgent->new;
    $LWPAgent->agent("$ProgName/$ProgVers");
    $GET_COUNTRY = 'LWP';
} elsif ( -x '/usr/bin/wget' )
{
    $GET_COUNTRY = '/usr/bin/wget';

} ## end elsif ( -x '/usr/bin/wget'...)

# Other internal globals
my %IPs_Hour   = ();
my %Country_IP = ();
my %Country    = ();
my $TotalBans  = 0;
my $GeoDB      = '';

#--------------------------------------------------------------------
# Display the usage
#--------------------------------------------------------------------
sub ShowUsage()
{

    print "Usage: $ProgName [options]\n",
        "       -D day  Specify day for report [default=$opt_D]\n",
        "       -h      Show this help [default=no]\n",
        "       -d      Show some debug info on STDERR [default=no]\n";

    exit 0;
} ## end sub ShowUsage

#--------------------------------------------------------------------
# Main function
#--------------------------------------------------------------------
$|++;
print "$ProgName\n$CopyRight\n\n";

# Get possible options
getopts('dD:h') or ShowUsage();
ShowUsage() if ($opt_h);

# Determine the day for the report
open( my $D, "-|", "$DATE +%F -d \"$opt_D\"" )
    or die "ERROR: Can not find or start '$DATE': $!";
my $Day = <$D>;
close($D);
chomp($Day);

# Determine the location of the geo database to use
if ( -s '/var/lib/GeoIP/GeoLite2-Country.mmdb' )
{
    $GeoDB = '/var/lib/GeoIP/GeoLite2-Country.mmdb';
} elsif ( -s '/usr/share/GeoIP/GeoLite2-Country.mmdb' )
{
    $GeoDB = '/usr/share/GeoIP/GeoLite2-Country.mmdb';
} ## end elsif ( -s '/usr/share/GeoIP/GeoLite2-Country.mmdb'...)

# Get the log data
# remove the file when the reference goes away with the UNLINK option
my $tmp = File::Temp->new( UNLINK => 1 );
system( "zgrep -h '^$Day' /var/log/fail2ban.log* > " . $tmp->filename );
open( my $D, '<', $tmp->filename ) or die "ERROR: Can not get log data: $!";
while ( my $Line = <$D> )
{
    # Process only lines that we care about
    next if ( $Line =~ /\waction\w/o );
    next
        unless ( $Line
        =~ / Ban ((?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))/o
        );
    my $IP   = $1;
    my $Hour = $1 if ( $Line =~ /(\d{2}):\d{2}:\d{2}/o );
    $TotalBans++;

    # Note the IP address and hour
    $IPs_Hour{"$Hour"}{"$IP"}++;

    # Get the country for the IP address
    my $Cty = 'undetermined';
    if ( exists( $Country_IP{"$IP"} ) )
    {
        warn "DEBUG: known country: " . $Country_IP{"$IP"} if ($opt_d);
        $Cty = $Country_IP{"$IP"};
    } else
    {
        if ( ( $GeoDB ne "" ) and ( $GET_COUNTRY =~ /mmdblookup/o ) )
        {
            warn
                "DEBUG: mmdblookup -f $GeoDB -i $IP registered_country names en"
                if ($opt_d);
            open( my $GC, '-|',
                "mmdblookup -f $GeoDB -i $IP registered_country names en" )
                or die "ERROR: Can't get country for $IP: $!";
            while ( my $CLine = <$GC> )
            {
                if ( $CLine =~ /"([^"]+)"/o )
                {
                    $Cty = "$1";
                    last;
                } ## end if ( $CLine =~ /"([^"]+)"/o...)
            } ## end while ( my $CLine = <$GC>...)
            close($GC);
        } elsif ( $GET_COUNTRY =~ /LWP/o )
        {
            warn "DEBUG: use LWP for $IP" if ($opt_d);
            my $response
                = $LWPAgent->get(
                "http://cl1.btoy1.net/whatsmyip.php?IPGeo=$IP",
                'Host' => 'consult.btoy1.net' );
            if ( $response->is_success )
            {
                $Cty = "$1"
                    if ( $response->content =~ /Country : ([\w ]+)/o );
            } ## end if ( $response->is_success...)
        } elsif ( $GET_COUNTRY =~ /wget/o )
        {
            my $Cmd
                = 'wget -q -O - --header="host: consult.btoy1.net" '
                . ' --no-check-certificate -t 3 -T 60 --user-agent='
                . "$ProgName/$ProgVers http://cl1.btoy1.net/whatsmyip.php?IPGeo=$IP";
            warn "DEBUG: $Cmd" if ($opt_d);
            open( my $GC, '-|', "$Cmd" )
                or die "ERROR: Can't get country for $IP: $!";
            while ( my $CLine = <$GC> )
            {
                if ( $CLine =~ /Country : (.+)$/o )
                {
                    $Cty = "$1";
                    last;
                } ## end if ( $CLine =~ /Country : (.+)$/o...)
            } ## end while ( my $CLine = <$GC>...)
            close($GC);
        } ## end elsif ( $GET_COUNTRY =~ /wget/o...)
    } ## end else [ if ( exists( $Country_IP...))]
    if ( "$Cty" ne "" )
    {
        $Country_IP{"$IP"} = "$Cty";
        $Country{"$Cty"}++;
    } ## end if ( "$Cty" ne "" )

    warn "DEBUG: Line = '$Line', Hour = $Hour, IP = $IP, Cty = $Cty\n"
        if ($opt_d);
} ## end while ( my $Line = <$D> )
close($D);

print "Total bans for $Day: $TotalBans\n\n";
# We are done if there are no bans at all
exit 0 if ( $TotalBans == 0 );

# Write the report (to STDOUT)
print "IP addresses banned per hour:\n";
for ( my $Hour = 0; $Hour < 24; $Hour++ )
{
    $Hour = sprintf( "%02d", $Hour );
    warn "DEBUG: Hour = $Hour" if ($opt_d);
    my %HourVal = ();
    foreach my $IP ( keys %{ $IPs_Hour{"$Hour"} } )
    {
        warn "DEBUG: IP = $IP (" . $IPs_Hour{"$Hour"}{"$IP"} . ")"
            if ($opt_d);
        $HourVal{"$IP"} = $IPs_Hour{"$Hour"}{"$IP"};
    } ## end foreach my $IP ( keys %{ $IPs_Hour...})
    if (%HourVal)
    {
        foreach
            my $IP ( sort { $HourVal{$b} <=> $HourVal{$a} } keys %HourVal )
        {
            warn "DEBUG: val = $HourVal{$IP}, $IP" if ($opt_d);
            printf "%3s %10d %s (%s, https://www.whois.com/whois/%s)\n",
                $Hour,
                $HourVal{$IP}, $IP, $Country_IP{"$IP"}, $IP;
        } ## end foreach my $IP ( sort { $HourVal...})
    } else
    {
        printf "%3s            no bans\n", $Hour;
    } ## end else [ if (%HourVal) ]
    print "\n";
} ## end for ( my $Hour = 0; $Hour...)

print "\nTop 10 countries with bans:\n";
my $Count = 0;
foreach my $Cty ( sort { $Country{$b} <=> $Country{$a} } keys %Country )
{
    printf "%10d (%d%% of %d) %s\n", $Country{"$Cty"},
        ( $Country{"$Cty"} / $TotalBans ) * 100, $TotalBans, $Cty;
    $Count++;
    last if ( $Count == 10 );
} ## end foreach my $Cty ( sort { $Country...})

# We are done
exit 0;
__END__
