#!/bin/bash
################################################################
# (c) Copyright 2013-2018 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/DeleteOldElasticSearchIndeces.sh

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            

# Days for indeces to delete or close - can 
#  be overwritten via command line options
Delete=92
Close=7

# Get possible program options
while getopts hd:c: OPTION
do
    case ${OPTION} in
    c)  Close=$OPTARG
        ;;
    d)  Delete=$OPTARG
        ;;
    *)  cat << EOT
Usage: $PROG [options] [logfile]
       -c days  Specify the "close" threshold [default=$Close days]
       -d days  Specify the "delete" threshold [default=$Delete days]
EOT
        exit 0
        ;;
    esac
done
shift $((OPTIND - 1))

#--------------------------------------------------------------------
# Install the required packages
for PP in libwww-perl libjson-perl
do
    dpkg-query -S $PP &> /dev/null
    [ $? -ne 0 ] && apt-get -y install $PP
done

#--------------------------------------------------------------------
# Get the list of indeces (minus the header line and minus "kibana")
TFILE=$(mktemp /tmp/$$XXXXXXXX)
curl  --show-error --silent  'localhost:9200/_cat/indices?v' | \
  sed '1d;/kibana/d;s/close/1 close/' > ${TFILE}.indeces.raw

#--------------------------------------------------------------------
# Process each index
for Index in $(awk '{print $3}' ${TFILE}.indeces.raw | sort -u)
do
    # Delete anything "Delete" days or older
    for DAYS in $(seq $Delete $((Delete * 2)))
    do
        NDAYS_AGO=$(date "+%Y.%m.%d" -d "$DAYS days ago")
        [[ $Index =~ '-'$NDAYS_AGO ]] || continue

        # Delete old index
        echo "Deleting ElasticSearch index '$Index'"
        curl -XDELETE --show-error --silent  "localhost:9200/$Index"
        echo
    done

    # Close anything "Close" days or older
    for DAYS in $(seq $Close $((Close * 2)))
    do
        NDAYS_AGO=$(date "+%Y.%m.%d" -d "$DAYS days ago")
        [[ $Index =~ '-'$NDAYS_AGO ]] || continue

        # Skip already closed index
        [ $(grep -c "close.*$Index" ${TFILE}.indeces.raw) -ne 0 ] && continue

        # Finally close it
        echo "Closing ElasticSearch index '$Index'"
        curl -XPOST --show-error --silent "localhost:9200/$Index/_close"
        echo
    done
done


#--------------------------------------------------------------------
# Allow more fields in any index (default is 1000)
curl -H "Content-Type:application/json" -XPUT 'http://localhost:9200/_all/_settings?preserve_existing=true' -d '
{
  "index.mapping.total_fields.limit" : "10000"
}'

#--------------------------------------------------------------------
# We are done
exit 0
