#!/bin/bash
# Version 20230201-164250 checked into repository
################################################################
# (c) Copyright 2023 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/pkgupdate
# Based on https://thesmarthomejourney.com/2021/09/12/home-server-zfs-backup/
# CAUTION: This script uses just ONE disk per ZFS pool

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=${0##*/}

# This needs to run as "root"
if [ ! "T$(/usr/bin/id -u)" = 'T0' ]
then
    echo "$PROG needs to run as 'root'"
    exit 1
fi

#--------------------------------------------------------------------
# First install the necessary package
if [ ! -x '/usr/sbin/zfs-auto-snapshot' ]
then
    apt-get update
    /usr/bin/env DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::='--force-confdef' \
      install zfs-auto-snapshot
fi

#--------------------------------------------------------------------
# Get the names for the pool and the disk to use for it
POOL=''
DISK=''
while [ -z "$POOL" ]
do
    read -p 'Specify the name for the ZFS pool [default=backups]: ' POOL
    [ -z "$POOL" ] && POOL='backups'
    if [ -n "$(zpool list | grep ^${POOL})" ]
    then
        echo "Pool '${POOL}' already exists"
    else
        while [ -z "$DISK" ]
        do
            echo 'List of disks in the system (first column is the disk id):'
            ls -l /dev/disk/by-id/ | awk '$9~/./{for (i=1;i<9;i++){$i=""}; gsub(/^ +/,""); print}'
            read -p 'Specify the disk id to use for the ZFS pool: ' DISK
            [ -z "$DISK" ] && exit 0
        done
    fi
done

#--------------------------------------------------------------------
# Create a pool on a single disk without any RAID-like capability(!)
if [ -z "$(zpool list | grep ^${POOL})" ]
then
    zpool create "${POOL}" "/dev/disk/by-id/$DISK"
fi

#--------------------------------------------------------------------
# Create a file system in the pool
read -p "Specify the name for the filesystem in ${POOL} [default=backups]: " FSNAME
[ -z "${FSNAME}" ] && FSNAME='backups'

if [ -n "$(zfs list | grep ${POOL}/${FSNAME})" ]
then
    echo "Filesystem '${FSNAME}' already exists in pool '${POOL}'"
else
    if [ -d "/${FSNAME}" ]
    then
        read -p "File system '/${FSNAME}' already exist - remove it [y/N] ? " YN
        [ -z "${YN}" ] && YN='N'
        [ "T${YN}" = 'TY' ] && rm -rf "/${FSNAME}"
    fi
    MP="/$FSNAME"
    [ "T$FSNAME" = 'Tbackups' ] && MP='/backup'
    zfs create -o mountpoint="$MP" "${POOL}/${FSNAME}"
fi
#--------------------------------------------------------------------
# Enable snapshots for non-backup file systems
if [ "T${POOL}" = 'Tbackups' ]
then
    zfs set com.sun:auto-snapshot=false backups/backups
    # Also use compression on the backup pool
    zfs set compression=lz4 /backups/backups
else
    zfs set com.sun:auto-snapshot:monthly=true ${POOL}/${FSNAME}
    zfs set com.sun:auto-snapshot:weekly=true ${POOL}/${FSNAME}
    zfs set com.sun:auto-snapshot:daily=true ${POOL}/${FSNAME}
    zfs set com.sun:auto-snapshot:hourly=true ${POOL}/${FSNAME}
    zfs set com.sun:auto-snapshot:frequent=true ${POOL}/${FSNAME}
fi

#--------------------------------------------------------------------
# Show what we have so far
zpool status
zfs list
read -p 'Press ENTER to continue' E

#--------------------------------------------------------------------
# Setup automated backups
if [ "$(zfs list | grep -c ^backups)" -ne 2 ]
then
    cat << EOT
Please setup a backup pool first.

Use 'backups' as both the pool name and the name for the file system.
EOT
    exit 1
fi

# Install zfs-prune-snapshots (if needed)
wget -q -O /tmp/zfs-prune-snapshots \
  https://raw.githubusercontent.com/bahamas10/zfs-prune-snapshots/master/zfs-prune-snapshots
diff -wu /tmp/zfs-prune-snapshots /usr/local/sbin/zfs-prune-snapshots &> /dev/null
[ $? -ne 0 ] && install -m 744 -o root -g root /tmp/zfs-prune-snapshots /usr/local/sbin/zfs-prune-snapshots

# Install sanoid
if [ ! -x '/usr/sbin/sanoid' ]
then
    apt-get update
    /usr/bin/env DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::='--force-confdef' \
      install sanoid
fi

if [ ! "T${POOL}" = 'Tbackups' ]
then
    # Create the ZFS backups script
    # Based on https://raw.githubusercontent.com/OliverHi/zfs-homeserver/main/ansible/templates/backup.sh
    cat << EOT > /tmp/$$.zfs-backup.sh
#!/bin/bash
#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=\${0##*/}

# This needs to run as "root"
if [ ! "T\$(/usr/bin/id -u)" = 'T0' ]
then
    echo "\$PROG needs to run as 'root'"
    exit 1
fi

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/\$PROG.lock
if [ -f "\$LOCKFILE" ]
then
    # The file exists so read the PID
    MYPID=\$(< "\$LOCKFILE")
    [ -z "\$(ps h -p \$MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f \$LOCKFILE /tmp/\$\$*" EXIT
echo "\$\$" > "\$LOCKFILE"

#--------------------------------------------------------------------
# Pool with the data that needs a backup
SOURCE_POOL="${POOL}"

# Backup-Pools
BACKUP_POOL='backups'

# ZFS file systems to backup
BACKUP_FS=("$FSNAME}")

# The log file
LOGFILE='/var/log/zfs-backup.log'

#--------------------------------------------------------------------
# The main program
echo >> \$LOGFILE
if [ "T\$(zpool list -H -o health \${BACKUP_POOL})" = 'TONLINE' ]
then
    date "+%F %T: backup pool '\${BACKUP_POOL}' is online. Starting backup" >> \$LOGFILE
    awk '{print " Current load="\$1", last 5 minutes load="\$2", last 15 minutes load="\$3}' /proc/loadavg >> \$LOGFILE

    # sync snapshots to backup pool
    for BACKUP_SYS in \${BACKUP_FS[@]}
    do
        date "+%F %T: Starting backup of \${SOURCE_POOL}/\${BACKUP_SYS} to backup pool '\${BACKUP_POOL}'" >> \$LOGFILE
        awk '{print " Current load="\$1", last 5 minutes load="\$2", last 15 minutes load="\$3}' /proc/loadavg >> \$LOGFILE
        syncoid \${SOURCE_POOL}/\${BACKUP_SYS} \${BACKUP_POOL}/backups/\${BACKUP_SYS} --no-sync-snap >> \$LOGFILE 2>&1
        RC=\$?
        date "+%F %T: Backup of \${SOURCE_POOL}/\${BACKUP_SYS} to '\${BACKUP_POOL}' is done with exit code \$RC" >> \$LOGFILE
        awk '{print " Current load="\$1", last 5 minutes load="\$2", last 15 minutes load="\$3}' /proc/loadavg >> \$LOGFILE
    done

    # cleanup
    date "+%F %T: Cleanup 'frequent' snapshots" >> \$LOGFILE
    zfs-prune-snapshots -p 'zfs-auto-snap_frequent' 2h \S{BACKUP_POOL} >> \$LOGFILE 2>&1
    date "+%F %T: Cleanup 'hourly' snapshots" >> \$LOGFILE
    zfs-prune-snapshots -p 'zfs-auto-snap_hourly' 25h \S{BACKUP_POOL} >> \$LOGFILE 2>&1
    date "+%F %T: Cleanup 'daily' snapshots" >> \$LOGFILE
    zfs-prune-snapshots -p 'zfs-auto-snap_daily' 2d \S{BACKUP_POOL} >> \$LOGFILE 2>&1
    date "+%F %T: Cleanup 'weekly' snapshots" >> \$LOGFILE
    zfs-prune-snapshots -p 'zfs-auto-snap_weekly' 5w \S{BACKUP_POOL} >> \$LOGFILE 2>&1
    date "+%F %T: Cleanup 'monthly' snapshots" >> \$LOGFILE
    zfs-prune-snapshots -p 'zfs-auto-snap_monthly' 16w \S{BACKUP_POOL} >> \$LOGFILE 2>&1
    date "+%F %T: Cleanup 'yearly' snapshots" >> \$LOGFILE
    zfs-prune-snapshots -p 'zfs-auto-snap_yearly' 2y \S{BACKUP_POOL} >> \$LOGFILE 2>&1

    # snapshots in the "master pool" are cleaned up by zfs-auto-snapshot

    # Truncate log file to 50000 lines
    CURLINES=\$(sed -n '\$=' \$LOGFILE)
    if [ \$CURLINES -gt 50000 ]
    then
        # Truncate the file to 50000 lines
        date "+%F %T: Truncating \$LOGFILE" >> \$LOGFILE
        awk '{print " Current load="\$1", last 5 minutes load="\$2", last 15 minutes load="\$3}' /proc/loadavg >> \$LOGFILE
        LINES2DEL=\$((CURLINES - 50000))
        sed -i -e "1,\${LINES2DEL}d" \$LOGFILE
     fi
else
    date "%F %T: \${BACKUP_POOL} is not online. Trying to import it" >> \$LOGFILE
    zpool import \${BACKUP_POOL}
fi

#--------------------------------------------------------------------
# We are done
awk '{print " Current load="\$1", last 5 minutes load="\$2", last 15 minutes load="\$3}' /proc/loadavg >> \$LOGFILE
date "+%F %T: \$PROG run done" >> \$LOGFILE
exit 0
EOT
    diff -wu /tmp/$$.zfs-backup.sh /usr/local/sbin/zfs-backup.sh &> /dev/null
    [ $? -ne 0 ] && install -m 744 -o root -g root /tmp/$$.zfs-backup.sh /usr/local/sbin/zfs-backup.sh

    cat <<EOT > /etc/cron.d/zfs-backup
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/sbin:/sbin:/usr/local/bin:/usr/bin:/bin:/snap/bin
#MAILTO=<FILL IN THE CORRECT EMAIL ADDRESS>
#===============================================================
# Copy ZFS backups and clean up
2,32 * * * * root [ -x /usr/local/sbin/zfs-backup.sh ] && /usr/local/sbin/zfs-backup.sh
EOT
fi

#--------------------------------------------------------------------
# We are done
exit 0
