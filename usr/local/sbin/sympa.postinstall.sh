#!/bin/bash
# Version 20240122-144825 checked into repository
################################################################
# (c) Copyright 2014 B-LUC Consulting and Thomas Bullinger
################################################################

#-------------------------------------------------------------------------
# Globals
#-------------------------------------------------------------------------
# This must run as "root"
[ "T$EUID" = 'T0' ] || exit

PROG=${0##*/}

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

trap "rm -f /tmp/$$*" EXIT
LOCALIP=$(ifconfig eth0 | sed -n "s/.*inet addr:\([0-9.]*\).*/\1/p")
THISHOST=$(hostname)
[[ $THISHOST == *.* ]] || THISHOST=$(hostname -f)
THISDOMAIN=${THISHOST#*.}

echo '=> Installing/updating sympa'
apt-get install sympa
#dpkg-reconfigure sympa

# Adapt the sympa authentication file
if [ -z "$(egrep ' DTP (START|END) ' /etc/sympa/auth.conf)" ]
then
    echo '=> Setting up sympa authentication'
    HURL='http://mail.digitaltowpath.org/help/en_US/advanced/zwc_help_files/1_setting_up/changing_your_password.htm'
    read -p "URL for help link [default=$HURL]? " UI
    [ -z "$UI" ] || HURL="$UI"

    cat << EOT > /etc/sympa/auth.conf
# Authentication services for Sympa

# Internal authentication by email and password

user_table
        negative_regexp ${THISDOMAIN//./\\\.}

#### DTP START ###
# LDAP authentication against Zimbra server
# See: http://www.sympa.org/manualtest/authentication
ldap
        regexp  ${THISDOMAIN//./\\\.}
        host    mail.digitaltowpath.org:389                
        timeout 30
        suffix  dc=${THISDOMAIN//./,dc=}
        scope   sub
        bind_dn uid=listmaster,ou=people,dc=${THISDOMAIN//./,dc=}
        bind_password Myh2asga
        get_dn_by_uid_filter    (uid=[sender])
        get_dn_by_email_filter  (|(mail=[sender]))
        email_attribute mail
        authentication_info_url  $HURL

#### DTP END ###
EOT
fi

# Install and enable fastcgi for Apache2
apt-get  install libapache2-mod-fastcgi
a2enmod actions fastcgi 

if [ $(awk '/^use_fast_cgi/ {print $NF}' /etc/sympa/wwsympa.conf) -ne 1 ]
then
    # Esanble fastcgi
    perl -p -i -e 's/^use_fast_cgi.*/use_fast_cgi 1/' /etc/sympa/wwsympa.conf
fi

# Make sure that the DNS entry is known
echo '=> Checking global DNS'
host listserv.$THISDOMAIN 209.244.0.4
if [ $? -ne 0 ]
then
    echo "Create DNS 'A' entry for 'listserv.$THISDOMAIN' to use '$LOCALIP'!"
    exit 1
fi
host -t mx listserv.$THISDOMAIN 8.8.8.8
if [ $? -ne 0 ]
then
    echo "Create DNS 'MX; entry for 'listserv.$THISDOMAIN' to point to 'mx2'!"
    exit 1
fi

# Create the sympa directories for listserv.$THISDOMAIN
# See https://www.sympa.org/manual/virtual-hosts#how_to_create_a_virtual_host
mkdir -p /etc/sympa/listserv.$THISDOMAIN
[ -s /etc/sympa/listserv.$THISDOMAIN/robot.conf ] || cat << EOT > /etc/sympa/listserv.$THISDOMAIN/robot.conf
# See sympa.conf for legend
http_host   http://listserv.$THISDOMAIN
wwsympa_url http://listserv.$THISDOMAIN/wws
soap_url    http://listserv.$THISDOMAIN/sympasoap
email       sympa
logo_html_definition <a href="http://www.$THISDOMAIN"><img style="float: left; margin-top: 7px; margin-left: 1px; width: 330px; height: 80px;" src="http://$THISDOMAIN/static/ext/Theme/Private_DTP/img/header/home.gif" alt="Digital Towpath" /></a>
EOT
mkdir -p /var/lib/sympa/list_data/listserv.$THISDOMAIN
chmod 750 /var/lib/sympa/list_data/listserv.$THISDOMAIN

# Make sure that Apache is configured with SSL
RESTART_APACHE=0
[ -h /etc/apache2/mods-enabled/ssl.conf ] || a2enmod ssl

if [ ! -s /etc/apache2/conf-enabled/sympa-ssl.conf ]
then
    echo '=> Setting up sympa virtual host'
    cat << EOT > /etc/apache2/conf-enabled/sympa-ssl.conf
<VirtualHost *:443>
    ServerName listserv.$THISDOMAIN
        
    SSLEngine on
    SSLCertificateFile /home/www/ssl/secure.$THISDOMAIN.crt
    SSLCertificateKeyFile /home/www/ssl/secure.$THISDOMAIN.key
    SSLCertificateChainFile /home/www/ssl/godaddy.crt 

    # See https://httpd.apache.org/docs/trunk/ssl/ssl_howto.html
    SSLProtocol all -SSLv3 -TLSv1 -TLSv1.1
    SSLCipherSuite      ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256
    SSLHonorCipherOrder on

    # Defend against clickjacking (requires 'a2enmod headers')
    Header set X-Frame-Options: "SAMEORIGIN"
    Header always set Strict-Transport-Security "max-age=31536000; includeSubDomains"
</VirtualHost>
EOT
    a2enmod headers
    RESTART_APACHE=1
fi

if [ -z "$(grep 'FcgidMaxRequestLen 524288' /etc/apache2/conf-enabled/sympa.conf)" ]
then
    cat << EOT >> /etc/apache2/conf-enabled/sympa.conf

<IfModule mod_fcgid.c>
    FcgidMaxRequestLen 524288
</IfModule>
EOT
    RESTART_APACHE=1
fi

# Replace the default index.html for Apache
if [ -z "$(grep B-LUC /var/www/html/index.html)" ]
then
    cat << EOT > /var/www/html/index.html
<!DOCTYPE html>
<html>
<head>
<title>Welcome</title>
 <meta name="copyright" content="B-LUC Consulting">
 <meta http-equiv="refresh" content="2;url=http://listserv.digitaltowpath.org/wws">
</head>
 <body>
   You will be redirected to the Mailing List Server in two seconds. If
   you aren't forwarded to the it, please click
   <a href='http://listserv.digitaltowpath.org/wws'> here </a>.
 </body>
</html>
EOT
fi

[ $RESTART_APACHE -ne 0 ] && service apache2 restart

# Make sure that we send directly to the Internet
# See also: http://www.sympa.org/faq/postfix_howto
echo '=> Configuring postfix'
postconf -e 'relayhost='
postconf -e "mydestination=listserv.$THISDOMAIN, localhost.$THISDOMAIN, localhost"
postconf -e "myhostname=listserv.$THISDOMAIN"
postconf -e 'alias_maps = hash:/etc/aliases,hash:/etc/mail/sympa/aliases'
postconf -e "relay_domains = $mydestination, listserv.$THISDOMAIN"
postconf -e 'sympa_destination_recipient_limit = 1'
postconf -e 'sympabounce_destination_recipient_limit = 1'
# virtual_alias_maps = regexp:/etc/sympa/virtual.regexp 
postconf -e 'recipient_delimiter = +'
postconf -e 'transport_maps = regexp:/etc/sympa/transport_regexp'

# Setup initial sympa aliases
[ -z "$(grep '^sympa:' /etc/mail/sympa/aliases)" ] && cat << EOT >> /etc/mail/sympa/aliases
sympa: "| /usr/lib/sympa/bin/queue listmaster@$THISDOMAIN"
listmaster: "| /usr/lib/sympa/bin/queue listmaster@$THISDOMAIN"
bounce+*: "| /usr/lib/sympa/bin/bouncequeue listmaster@$THISDOMAIN"
abuse-feedback-report: "| /usr/lib/sympa/bin/bouncequeue listmaster@$THISDOMAIN"
sympa-request: listmaster@$THISDOMAIN
sympa-owner: listmaster@$THISDOMAIN
EOT
postalias /etc/mail/sympa/aliases

# Setup transport table
[ -s /etc/sympa/transport_regexp ] || cat << EOT > /etc/sympa/transport_regexp
/^.*-owner\@listserv\.${THISDOMAIN//./\\\.}$/ sympabounce:
/^.*\@listserv\.${THISDOMAIN//./\\\.}$/       sympa:
EOT
postmap /etc/sympa/transport_regexp

# Finally install sympa services into postfix
PF_CD=$(postconf -h config_directory)
if [ -z "$(grep sympa $PF_CD/master.cf)" ]
then
    cat << EOT >> $PF_CD/master.cf

# Mailing list manager
sympa unix - n n - - pipe
  flags=R user=sympa argv=/usr/lib/sympa/bin/queue \${recipient}
sympabounce unix - n n - - pipe
  flags=R user=sympa argv=/usr/lib/sympa/bin/bouncequeue \${recipient}
EOT
    service postfix restart
fi
service sympa restart

# Make sure that Zimbra is configured correctly
cat << EOT

=> Ensure that Zimbra is setup correctly by checking on the Zimbra server:

1. The output of this command must include '$LOCALIP':
/opt/zimbra/bin/zmprov gs \$(/opt/zimbra/bin/zmhostname) zimbraMtaMyNetworks

2. Create a Zimbra user 'listmaster@$THISDOMAIN':
/usr/local/sbin/CreateZimbraAccount.sh listmaster Listserver Admin

3. The output of this command must show '$LOCALIP':
host listserv.$THISDOMAIN

4. The output of this command must show '$LOCALIP':
/opt/zimbra/bin/zmprov gd listserv.$THISDOMAIN zimbraMailTransport

5. Install this script on Zimbra server and run as cron (e.g. every 4 minutes):

=== cut here ===
#!/bin/bash

# Only "root" can run this script
[ "T\$EUID" = 'T0' ] || exit

#--------------------------------------------------------------------
# Set a sensible path for executables which includes Zimbra
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/opt/zimbra/bin
PROG=\${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/\$PROG.lock
if [ -f \$LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=\$(< \$LOCKFILE)
    [ -z "\$(ps h -p \$MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f \$LOCKFILE /tmp/\$\$*" EXIT
echo "\$\$" > \$LOCKFILE            

#--------------------------------------------------------------------
# Setups for mailing list server
host listserv.$THISDOMAIN &> /dev/null
if [ \$? -ne 0 ]
then
    echo "listserv      IN      A       209.217.203.211" >> /etc/bind/named/$THISDOMAIN
    service bind9 restart
fi
zmprov gd listserv.$THISDOMAIN &> /dev/null
if [ \$? -ne 0 ]
then
    su - zimbra -c "zmprov -l cd listserv.$THISDOMAIN zimbraMailTransport smtp:209.217.203.211 zimbraDomainType transport"
fi
sed -e 's/local/transport/' /opt/zimbra/conf/ldap-vmd.cf > /opt/zimbra/conf/ldap-vmd_transport.cf
if [[ ! \$(postconf -h virtual_mailbox_domains) =~ '_transport' ]]
then
    su - zimbra -c 'zmlocalconfig -e postfix_virtual_mailbox_domains=proxy:ldap:/opt/zimbra/conf/ldap-vmd.cf,proxy:ldap:/opt/zimbra/conf/ldap-vmd_transport.cf'
    postconf -e virtual_mailbox_domains='proxy:ldap:/opt/zimbra/conf/ldap-vmd.cf,proxy:ldap:/opt/zimbra/conf/ldap-vmd_transport.cf'
    postfix reload
fi
# See http://wiki.zimbra.com/wiki/ZCS_Sympa_integration
if [ ! -s /opt/zimbra/postfix/conf/listserv.virtual.regexp ]
then
    cat << EOLVR > /opt/zimbra/postfix/conf/listserv.virtual.regexp
/^(list-.*)@$THISDOMAIN/ \$1@listserv.$THISDOMAIN
/^(.*)@listserv.$THISDOMAIN/ \$1@listserv.$THISDOMAIN
EOLVR
    su - zimbra -c 'zmlocalconfig -e virtual_alias_maps=proxy:ldap:/opt/zimbra/conf/ldap-vam.cf,regexp:/opt/zimbra/postfix/conf/listserv.virtual.regexp'
    postconf -e virtual_alias_maps='proxy:ldap:/opt/zimbra/conf/ldap-vam.cf,regexp:/opt/zimbra/postfix/conf/listserv.virtual.regexp'
    postfix reload
fi

#--------------------------------------------------------------------
# We are done
exit 0
=== cut here ===
EOT

# We are done
exit 0
