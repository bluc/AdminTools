#!/bin/bash
# Version 20250116-080453 checked into repository
################################################################
# (c) Copyright 2022 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/SysMon.install.sh

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file and temp files at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE

#--------------------------------------------------------------------
# Can we use "curl"?
FP=$(command -v SysMon.install.sh)
if [ -z "$FP" ]
then
    if [ -s /usr/local/sbin/SysMon.install.sh ]
    then
        # Standard location
        FP=/tmp/SysMon.install.sh
    elif [ -s /tmp/SysMon.install.sh ]
    then
        # Used during "pkgupdate"
        FP=/tmp/SysMon.install.sh
    fi
fi
if [ -n "$FP" ]
then
    USER_AGENT=${PROG}'/'$(awk '/^# Version 2/{print $3}' $FP)
else
    USER_AGENT=${PROG}'/latest'
fi

HAVE_CURL=0
if [ -x /usr/bin/curl ]
then
    HAVE_CURL=1
    CURL_OPTS='-skL -m 60 --retry-connrefused --retry 3 --user-agent '"$USER_AGENT"
    [[ "$(curl --help)" =~ compressed ]] && CURL_OPTS="$CURL_OPTS --compressed"
fi
WGET_OPTS='-q --no-check-certificate -t 3 -T 60 --user-agent='"$USER_AGENT"
[[ "$(wget --help)" =~ compression= ]] && WGET_OPTS="$WGET_OPTS --compression=auto"

THISHOST=$(hostname)
[[ $THISHOST == *.* ]] || THISHOST=$(hostname -f)

#--------------------------------------------------------------------
# Get SysMon.pl
while [ ! -s /usr/local/sbin/SysMon.pl ]
do
    if [ $HAVE_CURL -eq 1 ]
    then
        curl $CURL_OPTS -o /usr/local/sbin/SysMon.pl \
         https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/SysMon.pl
    else
       wget $WGET_OPTS -O /usr/local/sbin/SysMon.pl \
         https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/SysMon.pl
    fi
done
chmod 744 /usr/local/sbin/SysMon.pl

# Get SysMon-Graphs.sh
while [ ! -s /usr/local/sbin/SysMon-Graphs.sh ]
do
    if [ $HAVE_CURL -eq 1 ]
    then
        curl $CURL_OPTS -o /usr/local/sbin/SysMon-Graphs.sh \
          https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/SysMon-Graphs.sh
     else
        wget $WGET_OPTS -O /usr/local/sbin/SysMon-Graphs.sh \
          https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/SysMon-Graphs.sh
     fi
done
chmod 744 /usr/local/sbin/SysMon-Graphs.sh

#--------------------------------------------------------------------
# Create an ingest configuration for rsyslog
cat << EOT > /tmp/$$
module(load="imfile" PollingInterval="1")

input(type="imfile"
      File="/var/log/SysMon.log"
      Tag="SysMon"
      Severity="info"
      Facility="local7")

# -> Suppress some messages
if (\$programname == 'SysMon') then {
  if (re_match(\$rawmsg,"INFO:")) then stop
}
EOT
diff -wu /tmp/$$ /etc/rsyslog.d/40-SysMon.conf &> /dev/null
if [ $? -ne 0 ]
then
    cat /tmp/$$ > /etc/rsyslog.d/40-SysMon.conf
    chmod o+r /var/log/SysMon.log
    if [ -x /bin/systemctl ]
    then
        systemctl restart rsyslog.service
    else
        service rsyslog restart
    fi
fi

#--------------------------------------------------------------------
# Create the start and check scripts
cat << EOSM > /usr/local/sbin/SysMon.start.sh
#!/bin/bash
#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=\${0##*/}

#--------------------------------------------------------------------
# Can we use "curl"?
USER_AGENT=\${PROG}'/'\$(awk '/^# Version 2/{print \$3}' \$(command -v SysMon.start.sh))

HAVE_CURL=0
if [ -x /usr/bin/curl ]
then
    HAVE_CURL=1
    CURL_OPTS='-skL -m 60 --retry-connrefused --retry 3 --user-agent '"\$USER_AGENT"
    [[ "\$(curl --help)" =~ compressed ]] && CURL_OPTS="\$CURL_OPTS --compressed"
fi
WGET_OPTS='-q --no-check-certificate -t 3 -T 60 --user-agent='"\$USER_AGENT"
[[ "\$(wget --help)" =~ compression= ]] && WGET_OPTS="\$WGET_OPTS --compression=auto"

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/\$PROG.lock
if [ -s \$LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=\$(< \$LOCKFILE)
    [ -z "\$(ps h -p \$MYPID)" ] || exit 0
fi

# Make sure we remove the lock file and temp files at exit
trap "rm -f \$LOCKFILE /tmp/\$\$*" EXIT
echo "\$\$" > \$LOCKFILE

#--------------------------------------------------------------------
# Update SysMon.pl if necessary
if [ \$HAVE_CURL -eq 1 ]
then
    curl \$CURL_OPTS -o /tmp/\$\$ https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/SysMon.pl
else
    wget \$WGET_OPTS -O /tmp/\$\$ https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/SysMon.pl
fi
diff -uw /tmp/\$\$ /usr/local/sbin/SysMon.pl &> /dev/null
if [ \$? -ne 0 ] && [ -s /tmp/\$\$ ]
then
    cat /tmp/\$\$ > /usr/local/sbin/SysMon.pl
    chmod 744 /usr/local/sbin/SysMon.pl
fi
rm -f /tmp/\$\$
exec \$(/usr/bin/awk -F\&\& '/SysMon/ {print \$2}' /etc/rc.local)
exit 0
EOSM
chmod 744 /usr/local/sbin/SysMon.start.sh

cat << EOSM > /usr/local/sbin/SysMon.check.sh
#!/bin/bash
#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=\${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/\$PROG.lock
if [ -s \$LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=\$(< \$LOCKFILE)
    [ -z "\$(ps h -p \$MYPID)" ] || exit 0
fi

# Make sure we remove the lock file and temp files at exit
trap "rm -f \$LOCKFILE /tmp/\$\$*" EXIT
echo "\$\$" > \$LOCKFILE

#--------------------------------------------------------------------
USER_AGENT=\${PROG}'/'\$(awk '/^# Version 2/{print \$3}' \$(command -v SysMon.check.sh))

HAVE_CURL=0
if [ -x /usr/bin/curl ]
then
    HAVE_CURL=1
    CURL_OPTS='-skL -m 60 --retry-connrefused --retry 3 --user-agent '"\$USER_AGENT"
    [[ "\$(curl --help)" =~ compressed ]] && CURL_OPTS="\$CURL_OPTS --compressed"
fi
WGET_OPTS='-q --no-check-certificate -t 3 -T 60 --user-agent='"\$USER_AGENT"
[[ "\$(wget --help)" =~ compression= ]] && WGET_OPTS="\$WGET_OPTS --compression=auto"

#--------------------------------------------------------------------
[[ \$- ==  *x* ]] || sleep \$((RANDOM % 120))
logger -p info -t \$PROG[\$\$] -- Trimming SysMon csv files

#--------------------------------------------------------------------
# Truncate csv files created by SysMon.pl to 50000 lines
for F in /var/tmp/*csv
do
    [ -s \$F ] || continue
    # Leave file with less than 50000 lines alone
    CURLINES=\$(sed -n '\$=' \$F)
    [ \$CURLINES -lt 50000 ] && continue

    # Truncate the file to 50000 lines
    LINES2DEL=\$((CURLINES - 50000))
    sed -i -e "1,\${LINES2DEL}d" \$F
done

#--------------------------------------------------------------------
# Rotate SysMon.log if it is bigger than 10MB
if [ -s /var/log/SysMon.log ]
then
    if [ \$(stat -c %s /var/log/SysMon.log) -gt $((1024 * 10240)) ]
    then
        mv -f /var/log/SysMon.log /var/log/SysMon.log.\$(date "+%FT%T")
        # Run the find command for up to 5 minutes
        timeout -k 300 270 find /var/log -type f -name SysMon.log.* -mtime +7 -delete
    fi
fi

#--------------------------------------------------------------------
# Restart SysMon if necessary
logger -p info -t \$PROG[\$\$] -- Checking SysMon.pl
RESTART_SYSMON=''

if [ \$HAVE_CURL -eq 1 ]
then
    curl \$CURL_OPTS -o /tmp/\$\$ https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/SysMon.pl
else
    wget \$WGET_OPTS -O /tmp/\$\$ https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/SysMon.pl
fi
if [ -n "\$(grep -E '(DOCTYPE|html)' /tmp/\$\$)" ]
then
    date "+%F %T: Download of SysMon.pl failed"
    exit 1
fi
diff -uw /tmp/\$\$ /usr/local/sbin/SysMon.pl &> /dev/null
if [ \$? -ne 0 ] && [ -s /tmp/\$\$ ]
then
    # There is a new version
    cat /tmp/\$\$ > /usr/local/sbin/SysMon.pl
    rm -f /tmp/\$\$
    chmod 744 /usr/local/sbin/SysMon.pl
    RESTART_SYSMON='newer version'
fi
if [ -z "\$RESTART_SYSMON" ]
then
    if [ \$(ps -wefH | grep -Fc 'Sys[M]on') -gt 1 ]
    then
        RESTART_SYSMON='more than 1 instance found'
    elif [ ! -f /var/tmp/CPU.csv ]
    then
        # No stat files
        RESTART_SYSMON='no stat file found'
    elif [ \$(date +%s -r /var/tmp/CPU.csv) -lt \$(date +%s -d '10 min ago') ]
    then
        # No updates to the stat files
        RESTART_SYSMON='stat file not updated'
    elif [ -z "\$(pidof -s -o %PPID SysMon.pl)" ]
    then
        # Not running
        RESTART_SYSMON='SysMon.pl not running'
    else
        # Restart SysMon.pl after 24 hours runtime
        SPID=\$(pidof -s -o %PPID SysMon.pl)
        if [ -n "\$SPID" ]
        then
            # Running for more than 24 hours
            SDATE=\$(date --date="\$(ps -p \$SPID -o lstart=)" '+%s')
            [ \$((EPOCHSECONDS - SDATE)) -gt 86400 ] && RESTART_SYSMON='running for more than 24 hours'
        fi
    fi
fi
if [ -n "\$RESTART_SYSMON" ]
then
    logger -p info -t \$PROG[\$\$] -- Restarting SysMon: \$RESTART_SYSMON
    SPID=\$(pidof -s -o %PPID SysMon.pl)
    [ -n "\$SPID')" ] && kill \$SPID &> /dev/null
    /bin/bash -$- /usr/local/sbin/SysMon.start.sh
fi

#--------------------------------------------------------------------
# Check for indicators of compromise and notate them in /tmp/IoC.report
# Inspired by https://github.com/Neo23x0/Fenrir/blob/master/fenrir.sh
rm -f /tmp/IoC.report
LSOF_OUT=\$(mktemp /tmp/\$\$XXXXXXXX)
timeout 10 lsof -i -n > \$LSOF_OUT
if [ -s \$LSOF_OUT ]
then
    rm -f /tmp/c2.txt
    wget -qO - https://feodotracker.abuse.ch/downloads/ipblocklist.txt | sed '/^#/d' > /tmp/\$\$.c2.txt
    wget -qO - https://sslbl.abuse.ch/blacklist/sslipblacklist.txt | sed '/^#/d' >> /tmp/\$\$.c2.txt
    if [ -s /tmp/\$\$.c2.txt ]
    then
        sort -u /tmp/\$\$.c2.txt > /tmp/c2.txt
        logger -p info -t \$PROG[\$\$] -- Checking for C2 connections
        date "+%F %T Possible backdoor connections:" >> /tmp/IoC.report
        grep -Ff /tmp/c2.txt \$LSOF_OUT >> /tmp/IoC.report 2>&1
    fi

    logger -p info -t \$PROG[\$\$] -- Checking for possible backdoor shells
    echo >> /tmp/IoC.report
    date "+%F %T Possible backdoor shells:" >> /tmp/IoC.report
    grep -wE '(sh|bash|[kc]sh)' \$LSOF_OUT | while read -r LINE
    do
        IS_PRIVATE=0
        grep -Eo '[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}:[0-9]+->[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}' <<< \$LINE | \\
          sed 's/:[0-9]*->/\\n/' > /tmp/\$\$.LINE
        for IP in \$(< /tmp/\$\$.LINE)
        do
            # Extract private IP addresses only
            ISP=\$(grep -Eo '^(10|127\\.0\\.0|192\\.168|172\\.1[6-9]|172\\.2[0-9]|172\\.3[01])\\.' <<< \$IP)
            [ -n "\$ISP" ] && IS_PRIVATE=\$((IS_PRIVATE+1))
        done
        rm -f /tmp/\$\$.LINE
        [ \$IS_PRIVATE -lt 2 ] && echo \$LINE
    done
fi
if [ -s /tmp/IoC.report ]
then
    HAVE_IOC=\$(grep -vc '^2' /tmp/IoC.report)
    if [ \$HAVE_IOC -gt 1 ]
    then
        logger -p info -t \$PROG[\$\$] -- Found \$((HAVE_IOC - 2)) backdoor connection
        cat /tmp/IoC.report
    fi
fi
rm -f \$LSOF_OUT

# Check for possible bad actions
# Inspired by https://blueteamblog.com/10-ways-to-detect-malicious-actions-within-a-linux-environment-using-siem
S_START=\$(date "+%m/%d/%Y %T" -d '15 minute ago')
CALOG=\$(mktemp /tmp/\$\$XXXXXXXX)
cat /var/log/audit/audit.log /var/log/audit/audit.log.[0-9] > \$CALOG 2> /dev/null
if [ -s \$CALOG ]
then
    date +"=> %F %T Checking for 'zeroing out files'" >> \$CALOG.out
    nice ausearch -if \$CALOG -ts \$S_START -c dd -i 2> /dev/null | grep -E 'EXECVE.*(null|zero)' >> \$CALOG.out

    date +"=> %F %T Checking 'changing immutable options on a file'" >> \$CALOG.out
    nice ausearch -if \$CALOG -ts \$S_START -c chattr -i 2> /dev/null | grep 'EXECVE.*\-i' >> \$CALOG.out

    date +"=> %F %T Checking 'system(d) starts'" >> \$CALOG.out
    nice ausearch -if \$CALOG -ts \$S_START -c systemctl -i 2> /dev/null | grep -E 'EXECVE.*(daemon.re|start)' >> \$CALOG.ou

# date +"=> Checking 'manipulate bash_history' (%F %T)"
#ausearch -ts \$S_START -w 'bash_history' -i 2> /dev/null 

    if [ \$(grep -vc '^=> ' \$CALOG.out) -gt 0 ]
    then
        echo "Possible 'bad actions' since \${S_START}:"
        cat \$CALOG.out
    fi
fi
rm -f \$CALOG*

#--------------------------------------------------------------------
# Check for some possible suspicious activity
rm -f /tmp/\$\$
# See https://www.sandflysecurity.com/blog/detecting-linux-memfd-create-fileless-malware-with-command-line-forensics/
ls -alR /proc/*/exe 2> /dev/null | grep 'memfd:.*\(deleted\)' > /tmp/\$\$
if [ -s /etc/debian_version ] && [ ! -s /etc/lsb-release ]
then
    # On a pure Debian system this is a bug, not an indicator of compromise:
    sed -i '/memfd:liblxc/d' /tmp/\$\$
fi
if [ -s /tmp/\$\$ ]
then
    date "+%F %T: [*] Fileless process(es) running on the server."
    cat /tmp/\$\$
fi
# More complicated way
#ps --ppid 2 -p 2 -p 1 --deselect -o uname,pid,ppid,rss,%cpu,command --sort -rss,-cpu | \
#  grep -vE '(^USER|/(usr|opt|sbin|lib|systemd)|sshd:|sd-pam|ppid|qmgr|pickup|bash|sudo|su -|php-fpm|nginx|ahavi-|fusedav|kde|gdrive|pve[pds-]|spiceproxy|dovecot|tlsmgr|ppolicyd|postgrey|smtpd|trivial-rewrite|anvil|showq|snap\/|redis|iperf|mdns-publisher|dhcpd)' > /tmp/\$\$

rm -f /tmp/\$\$
ls -sla /proc/*tinfo &> /tmp/\$\$
if [ -s /tmp/\$\$ ] && [ \$(grep -vc 'cannot access' /tmp/\$\$ 2> /dev/null) -ne 0 ]
then
    date "+%F %T: [*] Possible rootkit sysinitd running on the server."
    cat /tmp/\$\$
fi

#--------------------------------------------------------------------
# Suppress some chatty logs
echo '# Suppress some chatty logs' > /tmp/\$\$
echo '# Ex.: Suppress audit logs other then "forbidden" messages' >> /tmp/\$\$
echo 'if (\$programname == "audispd") and not (\$rawmsg contains "forbidden") then stop' >> /tmp/\$\$

# Adjust some existing logs
rm -f /etc/rsyslog.d/40-auditd.conf
[ -n "\$(grep -F 'logstash.crt' /etc/rsyslog.d/*)" ] && sed -i '/logstash.crt/d' /etc/rsyslog.d/*

# Restart rsyslog if necessary
diff -u /tmp/\$\$ /etc/rsyslog.d/10-auditd.conf &> /dev/null
if [ \$? -ne 0 ]
then
    cat /tmp/\$\$ > /etc/rsyslog.d/10-auditd.conf
    rsyslogd -N1 &> /dev/null
    if [ \$? -eq 0 ]
    then
        if [ -x /bin/systemctl ]
        then
            systemctl restart rsyslog
        else
            service rsyslog restart
        fi
    fi  
fi

#--------------------------------------------------------------------
# Actions once an hour
DO_HOURLY_ACTIONS=1
if [ -s /var/tmp/LastHourlyCheck.SysMon.check ]
then
    [ \$(date +%s -d '1 hours ago') -gt \$(date +%s -r /var/tmp/LastHourlyCheck.SysMon.check) ] || DO_HOURLY_ACTIONS=0
fi
if [ \$DO_HOURLY_ACTIONS -ne 0 ]
then
    #----------------------------------------------------------------
    # Check for a possible FrizFrog installation
    proc_names=( "nginx" "ifconfig" "libexec" "php-fpm" )
    malicious_proc=false
    listening_port=false
    for pn in "\${proc_names[@]}"
    do
        for exe_pid in \$(pidof \$pn)
        do
            exe_path=\$(ls -l /proc/\$exe_pid/exe 2>/dev/null | grep -F deleted)
            if [[ \$exe_path ]]
            then
                malicious_proc=true
                date "+%F %T: [*] Fileless process '\$pn' is running on the server."
            fi
        done
    done
    if [[ \$(netstat -ano | grep -P 'LISTEN .*\b1234\b') ]]
    then
        listening_port=true
        date "+%F %T: [*] Something is listening on port 1234"
    fi

    if \$malicious_proc || \$listening_port
    then
        date "+%F %T: [*] There is evidence of possible 'FritzFrog' malicious activity on this machine."
        read -r -t 10 -p 'Press ENTER to continue' YN
    fi

    #----------------------------------------------------------------
    # Check for a possible BitCoin miner
    BTC_MINER="\$(ps -wefH | grep 'yam[d]'; grep -E '(setup|tor):x' /etc/passwd)"
    if [ -n "\$BTC_MINER" ]
    then
        date "+%F %T: [*] A bitcoin miner might be running or be installed on this device: \$BTC_MINER"
        read -r -t 10 -p 'Press ENTER to continue' YN
    fi

    #----------------------------------------------------------------
    # Check for possible Drovorub infection
    touch /tmp/testfile
    echo "ASDFZXCV:hf:testfile" > /dev/zero
    if [ ! -e /tmp/testfile ]
    then
        date "+%F %T: [*] The device seems to have a 'Drovorub' infection."
        read -r -t 10 -p 'Press ENTER to continue' YN
    fi
    rm -f /tmp/testfile

    #----------------------------------------------------------------
    # Check for a possible Exaramel infection
    # see https://gist.github.com/humpalum/ce68317df2b740217ff2940f7479780b
    HAVE_EXARAMEL=0
    for F in /tmp/.applocktx /tmp/.applock /usr/local/centreon/www/search.php /usr/share/centreon/www/search.php /usr/share/centreon/www/modules/Discovery/include/DB−Drop.php /usr/share/centreon/www/htmlHeader.php
    do
        [ -e \$F ] || continue
        date "+%F %T: [*] The device might have had an 'Exaramel' infection: '\$F' exists."
        HAVE_EXARAMEL=1
    done
    [ \$HAVE_EXARAMEL -ne 0 ] && read -r -t 10 -p 'Press ENTER to continue' YN

    #----------------------------------------------------------------
    # Check for a possible FontOnLake infection
    # see https://www.welivesecurity.com/wp-content/uploads/2021/10/eset_fontonlake.pdf
    HAVE_FOL=0
    for F in /lib/modules/*/kernel/drivers/input/misc/ati_remote3.ko /etc/sysconfig/modules/ati_remote3.modules /proc/.dot3 /proc/.inl
    do
        [ -e \$F ] || continue
        date "+%F %T: [*] The device might have had a 'FontOnLake' infection: '\$F' exists."
        HAVE_FOL=1
    done
    [ \$HAVE_FOL -ne 0 ] && read -r -t 10 -p 'Press ENTER to continue' YN

    #----------------------------------------------------------------
    # Check for possible cronrat infection (two different ways)
    find /etc/cron.* /etc/crontab -type f -print0 | xargs -0r grep '52[[:space:]]*23[[:space:]]*31[[:space:]]*2[[:space:]]*3' > /tmp/testfile 2>/dev/null
    # See https://www.bleepingcomputer.com/news/security/new-malware-hides-as-legit-nginx-process-on-e-commerce-servers/
    grep -Fal LD_L1BRARY_PATH /proc/*/environ 2> /dev/null | grep -Fv self/ >> /tmp/testfile 2>/dev/null
    if [ -s /tmp/testfile ]
    then
        date "+%F %T: [*] The device seems to have a 'CronRat' infection."
        cat /tmp/testfile
        read -r -t 10 -p 'Press ENTER to continue' YN
    fi
    rm -f /tmp/testfile

    date > /var/tmp/LastHourlyCheck.SysMon.check
fi

#--------------------------------------------------------------------
# Actions once a day
DO_DAILY_ACTIONS=1
if [ -s /var/tmp/LastCheck.SysMon.check ]
then
    [ \$(date +%s -d '24 hours ago') -gt \$(date +%s -r /var/tmp/LastCheck.SysMon.check) ] || DO_DAILY_ACTIONS=0
fi
if [ \$DO_DAILY_ACTIONS -ne 0 ]
then

    # Check for possible log4shell attacks
    # Get all log directories (with some execptions)
    LOGDIRS=()
    unset dupes
    declare -A dupes
    timeout 120 lsof 2>/dev/null > /tmp/\$\$.lsofout
    for F in \$(awk '/\.log\$/{print \$NF}' /tmp/\$\$.lsofout | sort -u)
    do
        F_DIR="\${F%/*}"
        # Ignore some directories that take very long to scan
        [ "T\$F_DIR" = 'T/var/log' ] && continue
        [ "T\$F_DIR" = 'T/var/log/clamav' ] && continue
        [ "T\$F_DIR" = 'T/var/log/named' ] && continue
        [ "T\$F_DIR" = 'T/var/log/stunnel4' ] && continue
        [ "T\$F_DIR" = 'T/var/log/unattended-upgrades' ] && continue
        [ "T\$F_DIR" = 'T/var/cache/bind' ] && continue
        [ "T\$F_DIR" = 'T/opt/zimbra/log' ] && continue
        [ "T\$F_DIR" = 'T/opt/zextras/log' ] && continue
        [[ -z \${dupes["\$F_DIR"'/*log']} ]] && LOGDIRS+=("\$F_DIR"'/*log')
        dupes["\$F_DIR"'/*log']=1
    done
    rm -f /tmp/\$\$.lsofout
    unset dupes
    if [ "\${#LOGDIRS[@]}" -ge 1 ]
    then
        if [ \$HAVE_CURL -eq 1 ]
        then
            curl \$CURL_OPTS -o /tmp/log4shell.zip https://github.com/Neo23x0/log4shell-detector/archive/refs/heads/main.zip
        else
            wget \$WGET_OPTS -O /tmp/log4shell.zip https://github.com/Neo23x0/log4shell-detector/archive/refs/heads/main.zip
        fi
        (cd /tmp && unzip -oq /tmp/log4shell.zip)

        if [ -s /tmp/log4shell-detector-main/log4shell-detector.py ]
        then
            # We want to scan some Zimbra logs
            [ -s /opt/zimbra/log/nginx.log ] && LOGDIRS+=(/opt/zimbra/log/nginx.log)
            [ -s /opt/zimbra/log/nginx.access.log ] && LOGDIRS+=(/opt/zimbra/log/nginx.access.log)
            [ -s /opt/zimbra/log/audit.log ] && LOGDIRS+=(/opt/zimbra/log/audit.log)

            [ -s /opt/zextras/log/nginx.log ] && LOGDIRS+=(/opt/zextras/log/nginx.log)
            [ -s /opt/zextras/log/nginx.access.log ] && LOGDIRS+=(/opt/zextras/log/nginx.access.log)
            [ -s /opt/zextras/log/audit.log ] && LOGDIRS+=(/opt/zextras/log/audit.log)

            # Scan away :)
            python3 /tmp/log4shell-detector-main/log4shell-detector.py --quick -f \${LOGDIRS[@]} &> /tmp/l4s.report
            if [ -z "\$(grep -F 'No exploitation attempts detected in the scan' /tmp/l4s.report)" ]
            then
                # Some indicators of compromise was detected
                cat /tmp/l4s.report
            fi
        fi
    fi

    # Remember this check
    date > /var/tmp/LastCheck.SysMon.check
fi

#--------------------------------------------------------------------
# We are done
logger -p info -t \$PROG[\$\$] -- Done
exit 0
EOSM
chmod 744 /usr/local/sbin/SysMon.check.sh

#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Create the cron job
cat << EOSM > /etc/cron.d/SysMon
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/sbin:/sbin:/usr/local/bin:/usr/bin:/bin
#===============================================================
# Healthchecks
*/10 * * * * root [ -x /usr/local/sbin/SysMon.check.sh ] && /usr/local/sbin/SysMon.check.sh
# Daily statistics
20 6 * * *   root [ -x /usr/local/sbin/SysMon-Graphs.sh ] && /usr/local/sbin/SysMon-Graphs.sh -c
EOSM

#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# Create the systemd unit if needed
if [ -x /bin/systemctl ]
then
    RELOAD_SYSTEMD=0
    cat << EOSM > /tmp/$$
[Unit]
Description=Monitor system for possible issues
After=network.target

[Service]
Type=forking
PIDFile=SysMon.pid
#User=root
#WorkingDirectory=/home/nanodano
ExecStart=/bin/bash /usr/local/sbin/SysMon.start.sh
# Restart options
Restart=always
RestartSec=5s
# Other restart options: always, on-abort, etc

# The install section is needed to use
# 'systemctl enable' to start on boot
# For a user service that you want to enable
# and start automatically, use 'default.target'
# For system level services, use 'multi-user.target'
[Install]
WantedBy=multi-user.target
EOSM
    diff -wu /tmp/$$ /etc/systemd/system/SysMon.service &> /dev/null
    if [ $? -ne 0 ]
    then
        cat /tmp/$$ > /etc/systemd/system/SysMon.service
        chmod 644 /etc/systemd/system/SysMon.service
        systemd-analyze verify /etc/systemd/system/SysMon.service && RELOAD_SYSTEMD=1
        if [ $RELOAD_SYSTEMD -ne 0 ]
        then
            systemctl daemon-reload

            systemctl enable SysMon.service
            systemctl restart SysMon.service

            systemctl status SysMon.service
        fi
    fi
fi

#--------------------------------------------------------------------
# We are done
exit 0
