#!/bin/bash
# Version 20240822-044450 checked into repository
################################################################
# (c) Copyright 2012 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/Zimbra/zos-backup.sh

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
if [ -d /opt/zimbra/bin ]
then
    SERVER_TYPE='Z'
elif [ -d /opt/zextras/bin ]
then
    SERVER_TYPE='C'
else
    echo "ERROR: Neither a Zimbra nor a Carbonio server"
    exit 1
fi

#==============================================================================
# Don't run several instances
PROG=${0##*/}
LOCKFILE=/tmp/${PROG}.lock
if [ -s $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps -p $MYPID | grep $MYPID)" ] || exit 0
fi

# The process is not running (or no lockfile exists)
echo $$ > $LOCKFILE
trap "rm -f $LOCKFILE /tmp/$$*; exit 0" 1 2 3 15 EXIT

DEBUG=0
[[ $- = *x* ]] && DEBUG=1

# Get the full hostname
THISHOST=$(hostname)
[[ $THISHOST = *.* ]] || THISHOST=$(hostname -f)
if [ "T$SERVER_TYPE" = 'TZ' ]
then
    ZIMBRA_HOSTNAME=$(su - zimbra -c 'zmhostname')
else
    ZIMBRA_HOSTNAME=$(su - zextras -c 'zmhostname')
fi

# Run the whole script in "ionice" mode
ionice -c2 -n7 -p $$

# Use "nocache" if possible
NOCACHE=''
dpkg-query -S nocache &> /dev/null && NOCACHE='nocache'

# Determine whether we are on a "Network Edition" server or not
if [ "T$SERVER_TYPE" = 'TZ' ]
then
    if [ -x /opt/zimbra/bin/zmlicense  ] && [ -x /opt/zimbra/bin/zmrestore  ] && [ -s /opt/zimbra/backup/accounts.xml ]
    then
        ZIMBRA_NE=1
    else
        ZIMBRA_NE=0
    fi
else
    # Assume Carbonio CE at this time
    ZIMBRA_NE=0
fi

# Compute the max. CPU load
MAXLOAD=$(nproc --all)
[[  "$MAXLOAD" =~ ^[0-9]+$ ]] || MAXLOAD=1
if [ $MAXLOAD -gt 16 ]
then
    MAXLOAD=$((MAXLOAD / 4))
else
    MAXLOAD=$((MAXLOAD / 2))
fi

# Use the multi-core version of gzip if possible
GZIP='gzip'
[ -x /usr/bin/pigz ] && GZIP="pigz -p $MAXLOAD"

# The list of system config files
FILELIST='/etc/firehol/firehol.conf /etc/network/interfaces
          /etc/hosts /etc/hostname /etc/resolv.conf'
if [ "T$SERVER_TYPE" = 'TZ' ]
then
    FILELIST="$FILELIST /opt/zimbra/ssl/zimbra/commercial"
else
    FILELIST="$FILELIST /opt/zextras/ssl/carbonio/commercial"
fi

TODAY=$(date '+%F')

# Process options from the command line
OPTIONS=''
while getopts "ho:" OPTION
do
    case "${OPTION}" in
    o)  OPTIONS=${OPTARG}
        ;;
    *)
        cat << EOT
Usage: $0 [-o]

  Possible options (need to be seperated by a ',' [comma]:
  no-email  Backup only backup metadata (not emails)
EOT
        exit 1
        ;;
    esac
done
shift $((OPTIND-1))

# Create a directory for all backups
BKP_DIR='/backup/backups'
rm -rf $BKP_DIR
mkdir -p $BKP_DIR

LOGFILE="/backup/zos-backup.log"
LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
date "+%F %T $0 starting: ($LA)" > $LOGFILE

##################
# Subroutines
##################
function LoadCheck() {

    # Wait until the load average settled somewhat
    LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
    date "+%F %T Checking system load against $MAXLOAD ($LA)" >> $LOGFILE
    until [ $(awk '{print int($1 * 100)}' /proc/loadavg) -lt $((MAXLOAD * 100)) ]
    do
        sleep 4
    done

    LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
    date "+%F %T System load after check ($LA)" >> $LOGFILE
}

#-----------------------------------------------------------
function LimitCPULoad() {
    local ProcessID="$1"
    local ProcessCMD="$2"
    local Throttle=-1

    # This keeps the CPU load in check
    # It requires the process to run in the background
    # See https://unix.stackexchange.com/questions/39722/preventing-tar-from-using-too-much-cpu-and-disk-old-laptop-crashes-i
    while true
    do
        # We are done once the process is finished
        builtin kill -0 ${ProcessID} 2>/dev/null || break

        # Only throttle the process if the CPU is too busy
        if [ $(awk '{print int($1 * 100)}' /proc/loadavg) -lt $((MAXLOAD * 100)) ]
        then
            # Show the transition to full speed
            [ $Throttle -ne 0 ] && date "+%F %T Running $ProcessID at full speed ($ProcessCMD)" >> $LOGFILE
            Throttle=0
            sleep 0.5
        else
            # Show the transition to throttle
            [ $Throttle -ne 1 ] && date "+%F %T Throttling $ProcessID ($ProcessCMD)" >> $LOGFILE
            Throttle=1
            sleep 1.5  # Let it run for a while
            builtin kill -STOP ${ProcessID} 2>/dev/null || break
            sleep 1    # Pause it for a bit
            builtin kill -CONT ${ProcessID} 2>/dev/null || break
        fi
    done
}

#-----------------------------------------------------------
function BkpAccount() {

    # Local variables
    local BKP_DIR="$1"
    local ACCOUNT="$2"
    local TOTAL_USERS=$3
    local CUR_USER_NUM=$4

    local ZUSER=''
    local LA=65535

    if [ "T$SERVER_TYPE" = 'TZ' ]
    then
        ZUSER='zimbra'
    elif [ "T$SERVER_TYPE" = 'TC' ]
    then
        ZUSER='zextras'
    fi

    LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
    date "+%F %T Backing up account $ACCOUNT [$CUR_USER_NUM of $TOTAL_USERS] ($LA)" >> $LOGFILE

    # Save the account's metadata (incl. password)
    su - "${ZUSER}" -c "nice $NOCACHE zmprov -l ga $ACCOUNT" >> $BKP_DIR/account.$ACCOUNT 2>> $LOGFILE

    # Save the account's filters
    su - "${ZUSER}" -c "nice $NOCACHE zmmailbox -z -m $ACCOUNT getFilterRules" >> $BKP_DIR/account.filters.$ACCOUNT 2>> $LOGFILE

    if [[ $OPTIONS =~ no-email ]]
    then
        # We only need the meta data for an account
        return
    fi

    local LASTFULL=''
    if [ $ZIMBRA_NE -ne 0 ]
    then
        # TB: Special case - create full backups for all domains
        #      but a few to prepare for migration to new server
        if [[ ! $ACCOUNT =~ .*domain1.* ]] && [[ ! $ACCOUNT =~ .*domain2.* ]]
        then
            LASTFULL=''
        else

        # On "Network Edition" servers do an incremental backup
        #  since the last full regular backup
        if [ -d /opt/zimbra/backup/sessions ]
        then
            # Get the day BEFORE the last full backup
            local RFB=$(ls -1t /opt/zimbra/backup/sessions/ | grep -m 1 full-)
            [ -z "$RFB" ] || LASTFULL=$(date +%D -d "${RFB:5:8} - 1 day")
        fi
        fi # end TB: Special case
    else
        # Is there a full backup for that account already?
#        local BKPDAY=$(($(/sbin/ifconfig eth0 | sed -n "s/.*inet addr:\([0-9.]*\).*/\1/p" | awk -F. '{print $NF}') % 7))
        # Full backups happen on Sundays
        local BKPDAY=0
        if [ $(date +%w) -ne $BKPDAY ]
        then
            # Get the date of the last backup
            # Note: Run full backups on the backup day
            [ -s /var/tmp/${ACCOUNT}.lastbkp ] && LASTFULL=$(< /var/tmp/${ACCOUNT}.lastbkp)
        fi
    fi

    # Wait until the load average settled somewhat
    LoadCheck

    # Get the account's data
    if [ -z "$LASTFULL" ]
    then
        # Save everything
        LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
        date "+%F %T Full backup for account $ACCOUNT ($LA)" >> $LOGFILE
        su - "${ZUSER}" -c "nice $NOCACHE zmmailbox -z -m $ACCOUNT getRestURL '//?fmt=tgz'" > $BKP_DIR/account-data.${ACCOUNT}.f${TODAY}.tgz 2>> $LOGFILE
    else
        # Save only from a last backup date on (incremental)
        LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
        date "+%F %T Incremental backup for account $ACCOUNT ($LA)" >> $LOGFILE
        su - "${ZUSER}" -c "nice $NOCACHE zmmailbox -z -m $ACCOUNT getRestURL '//?fmt=tgz&query=after:$LASTFULL'" > $BKP_DIR/account-data.${ACCOUNT}.i${TODAY}.tgz 2>> $LOGFILE
    fi

    # Save the (previous) day of this backup
    date '+%D' > /var/tmp/${ACCOUNT}.lastbkp
}

#-----------------------------------------------------------
function BackupData() {

    # Local variables
    local BKP_DIR="$1"
    mkdir -p "$BKP_DIR"
    local ZUSER=''
    local RESTART_MAILBOXD=0
    local CD=''
    local LA=65535
    local INDEX=0

    if [ "T$SERVER_TYPE" = 'TZ' ]
    then
        ZUSER='zimbra'
    elif [ "T$SERVER_TYPE" = 'TC' ]
    then
        ZUSER='zextras'
    fi

    # Backup "localconfig"
    LoadCheck
    LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
    date "+%F %T Backing up 'localconfig' ($LA)" >> $LOGFILE
    echo "# Zimbra Local Configuration (incl. passwords)" > $BKP_DIR/localconfig
    su - ${ZUSER} -c "nice $NOCACHE zmlocalconfig -s" >> $BKP_DIR/localconfig 2>> $LOGFILE
    grep pass $BKP_DIR/localconfig > $BKP_DIR/passwords

    # Backup all databases
    LoadCheck
    LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
    date "+%F %T Backing up all databases ($LA)" >> $LOGFILE
    local ROOT_SQL_PASSWORD=$(awk '/^mysql_root_password/ {print $NF}' $BKP_DIR/localconfig)
    local MYSQLDUMP=''
    local MYSQLSOCK=''
    if [ -x /opt/${ZUSER}/common/bin/mysqldump ]
    then
        MYSQLDUMP=/opt/${ZUSER}/common/bin/mysqldump
        MYSQLSOCK=/opt/${ZUSER}/data/tmp/mysql/mysql.sock
    elif [ -x /opt/${ZUSER}/mysql/bin/mysqldump ]
    then
        MYSQLDUMP=/opt/${ZUSER}/mysql/bin/mysqldump
        MYSQLSOCK=/opt/${ZUSER}/db/mysql.sock
    fi
    if [ -x $MYSQLDUMP ]
    then
        nice $NOCACHE mysql -S $MYSQLSOCK \
          -u root --password=$ROOT_SQL_PASSWORD -NBe 'show databases' > /tmp/dbs &
        LimitCPULoad $! mysql
        grep -E '^(mbox|mysql|zimbra)' /tmp/dbs | while read -r DB
        do
            TRY=0
            while [ $TRY -lt 25 ]
            do
                nice $NOCACHE $MYSQLDUMP -S $MYSQLSOCK --add-drop-database \
                  -u root --password=$ROOT_SQL_PASSWORD $DB --result-file=$BKP_DIR/${DB}.sql 2>> $LOGFILE 
                if [ $? -eq 0 ]
                then
                    TRY=25
                    continue
                fi
                TRY=$((TRY+1))
                sleep 10
            done
        done
    fi
    nice $NOCACHE ${GZIP} $BKP_DIR/*.sql &
    LimitCPULoad $! "${GZIP%% *}"

    # LDAP database
    LoadCheck
    LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
    date "+%F %T Backing up LDAP configuration ($LA)" >> $LOGFILE
    local PW=$(awk '/^zimbra_ldap_password/{print $NF}' $BKP_DIR/passwords)
    local LDAP_URL=$(awk '/^ldap_url/{print $NF}' $BKP_DIR/localconfig)
    if [ -x /opt/${ZUSER}/common/bin/ldapsearch ]
    then
        $NOCACHE /opt/${ZUSER}/common/bin/ldapsearch -x -H $LDAP_URL -w $PW \
          -D uid=zimbra,cn=admins,cn=zimbra > $BKP_DIR/ldap.ldif
    elif [ -x /opt/${ZUSER}/bin/ldapsearch ]
    then
        $NOCACHE /opt/${ZUSER}/bin/ldapsearch -x -H $LDAP_URL -w $PW \
          -D uid=zimbra,cn=admins,cn=zimbra > $BKP_DIR/ldap.ldif
    else
        touch $BKP_DIR/ldap.ldif
    fi

    # Backups
    # All COS data (sorted by name)
    INDEX=0
    LoadCheck
    su - ${ZUSER} -c "nice $NOCACHE zmprov gac" | sort > /tmp/$$
    local TOTAL_COSS=$(sed -n '$=' /tmp/$$)
    local CUR_COS_NUM=1
    for COS in $(< /tmp/$$)
    do
        LoadCheck
        LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
        date "+%F %T Backing up COS '$COS' ($LA)" >> $LOGFILE
        echo "# Zimbra Class of Service: $COS" > $BKP_DIR/cos.$COS
        su - ${ZUSER} -c "nice $NOCACHE zmprov gc $COS" >> $BKP_DIR/cos.$COS 2>> $LOGFILE &
        CUR_COS_NUM=$(($CUR_COS_NUM + 1))
        INDEX=$(($INDEX + 1))
        if [ $INDEX -gt 1 ]
        then
            # Only two COSes in parallel at any time
            wait
            INDEX=0
        fi
    done
    wait

    # All servers (sorted by name)
    INDEX=0
    LoadCheck
    su - ${ZUSER} -c "nice $NOCACHE zmprov gas" | sort > /tmp/$$
    local TOTAL_SRVS=$(sed -n '$=' /tmp/$$)
    local CUR_SRV_NUM=1
    for SERVER in $(< /tmp/$$)
    do
        LoadCheck
        LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
        date "+%F %T Backing up server '$SERVER' [$CUR_SRV_NUM of $TOTAL_SRVS] ($LA)" >> $LOGFILE
        echo "# Zimbra Server: $SERVER" > $BKP_DIR/server.$SERVER
        su - ${ZUSER} -c "nice $NOCACHE zmprov gs $SERVER" >> $BKP_DIR/server.$SERVER 2>> $LOGFILE &
        CUR_SRV_NUM=$(($CUR_SRV_NUM + 1))
        INDEX=$(($INDEX + 1))
        if [ $INDEX -gt 1 ]
        then
            # Only two servers in parallel at any time
            wait
            INDEX=0
        fi
    done
    wait

    # All domains (sorted by name)
    INDEX=0
    LoadCheck
    su - ${ZUSER} -c "nice $NOCACHE zmprov gad" | sort > /tmp/$$
    local TOTAL_DOMS=$(sed -n '$=' /tmp/$$)
    local CUR_DOM_NUM=1
    for DOMAIN in $(< /tmp/$$)
    do
        # TB: Special case - ignore non-migrated domains
        [[ $DOMAIN =~ .*domain1.* ]] && continue
        [[ $DOMAIN =~ .*domain2.* ]] && continue

        LoadCheck
        LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
        date "+%F %T Backing up domain '$DOMAIN' [$CUR_DOM_NUM of $TOTAL_DOMS] ($LA)" >> $LOGFILE
        echo "# Zimbra Domain: $DOMAIN" > $BKP_DIR/domain.$DOMAIN
        su - ${ZUSER} -c "nice $NOCACHE zmprov gd $DOMAIN" >> $BKP_DIR/domain.$DOMAIN 2>> $LOGFILE &
        CUR_DOM_NUM=$(($CUR_DOM_NUM + 1))
        INDEX=$(($INDEX + 1))
        if [ $INDEX -gt 1 ]
        then
            # Only two domains in parallel at any time
            wait
            INDEX=0
        fi
    done
    wait

    # All accounts (sorted by size in descending order)
    INDEX=0
    LoadCheck
    su - ${ZUSER} -c "nice $NOCACHE zmprov gqu $ZIMBRA_HOSTNAME" | cut -d' ' -f1,3 | sort -k 2 -rn | cut -d' ' -f1 | \
      egrep -v '^(ham|spam|virus-quarantine|galsync|wiki)' > /tmp/$$
    local TOTAL_USERS=$(sed -n '$=' /tmp/$$)
    local CUR_USER_NUM=1
    for ACCOUNT in $(< /tmp/$$)
    do
        # TB: Special cases - ignore non-migrated domains
        [[ $ACCOUNT =~ .*domain1.* ]] && continue
        [[ $ACCOUNT =~ .*domain2.* ]] && continue

        BkpAccount $BKP_DIR $ACCOUNT $TOTAL_USERS $CUR_USER_NUM &
        CUR_USER_NUM=$(($CUR_USER_NUM + 1))
        INDEX=$(($INDEX + 1))
        if [ $INDEX -gt 1 ]
        then
            # Only two accounts in parallel at any time
            wait
            INDEX=0
        fi
    done
    wait

    # All distribution lists (sorted by name)
    INDEX=0
    LoadCheck
    su - ${ZUSER} -c "nice $NOCACHE zmprov gadl" | sort > /tmp/$$
    local TOTAL_LISTS=$(sed -n '$=' /tmp/$$)
    local CUR_LIST_NUM=1
    for LIST in $(< /tmp/$$)
    do
        # TB: Special case - ignore non-migrated domains
        [[ $LIST =~ .*domain1.* ]] && continue
        [[ $LIST =~ .*domain2.* ]] && continue

        LoadCheck
        LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
        date "+%F %T Backing up list '$LIST' [$CUR_LIST_NUM of $TOTAL_LISTS] ($LA)" >> $LOGFILE
        echo "# Zimbra Distribution List: $LIST" > $BKP_DIR/list.$LIST
        su - ${ZUSER} -c "nice $NOCACHE zmprov gdl $LIST" >> $BKP_DIR/list.$LIST 2>> $LOGFILE &
        CUR_LIST_NUM=$(($CUR_LIST_NUM + 1))
        INDEX=$(($INDEX + 1))
        if [ $INDEX -gt 1 ]
        then
            # Only two lists in parallel at any time
            wait
            INDEX=0
        fi
    done
    wait

    # All calendars (sorted by name)
    INDEX=0
    LoadCheck
    su - ${ZUSER} -c "nice $NOCACHE zmprov gacr" | sort > /tmp/$$
    local TOTAL_CALS=$(sed -n '$=' /tmp/$$)
    local CUR_CAL_NUM=1
    for CAL in $(< /tmp/$$)
    do
        # TB: Special case - ignore non-migrated domains
        [[ $CAL =~ .*domain1.* ]] && continue
        [[ $CAL =~ .*domain2.* ]] && continue

        LoadCheck
        LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
        date "+%F %T Backing up calendar '$CAL' [$CUR_CAL_NUM of $TOTAL_CALS] ($LA)" >> $LOGFILE
        echo "# Zimbra Calendar: $CAL" > $BKP_DIR/calendar.$CAL
        su - ${ZUSER} -c "nice $NOCACHE zmprov gcr $CAL" >> $BKP_DIR/calendar.$CAL 2>> $LOGFILE &
        CUR_CAL_NUM=$(($CUR_CAL_NUM + 1))
        INDEX=$(($INDEX + 1))
        if [ $INDEX -gt 1 ]
        then
            # Only two calendars in parallel at any time
            wait
            INDEX=0
        fi
    done
    wait

    # Some misc. files
    LoadCheck
    LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
    date "+%F %T Backing up misc. files ($LA)" >> $LOGFILE
    [ -f /usr/local/sbin/LocalHealthCheck.sh ] && cp /usr/local/sbin/LocalHealthCheck.sh $BKP_DIR
    [ -f /usr/local/etc/zos-MailRouting.cfg ] && cp /usr/local/etc/zos-MailRouting.cfg $BKP_DIR

    # Delete any backups older than 8 days
    LoadCheck
    LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
    date "+%F %T Deleting old backups ($LA)" >> $LOGFILE
    nice $NOCACHE find $BKP_DIR/.. -maxdepth 1 -type f \( -name "${THISHOST}.backup.*.tgz" -o -name "${THISHOST}.backup.*.tar" \) -mtime +7 -delete >> $LOGFILE 2>&1 &

    # Gzip all files not yet compressed
    LoadCheck
    LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
    date "+%F %T Compress backups ($LA)" >> $LOGFILE
    if [ -d /backup/opt/zextras ]
    then
        # Leave the ZFS snapshot alone
        nice $NOCACHE find ${BKP_DIR} -type f '!' -name "*gz" -exec ${GZIP} \-9f '{}' ';'
    else
        nice $NOCACHE find ${BKP_DIR}/.. -type f '!' -name "*gz"  -exec ${GZIP} \-9f '{}' ';'
    fi

    # Tar up the whole backup directory and delete it
    LoadCheck
    LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
    date "+%F %T Taring up backups ($LA)" >> $LOGFILE
    ARCHIVE=$BKP_DIR/../${THISHOST}'.backup.'$(date '+%Y-%m-%d')'.tar'
    nice $NOCACHE tar -cf "$ARCHIVE" $BKP_DIR >> $LOGFILE 2>&1 &
    LimitCPULoad $! tar
    if [ -s "$ARCHIVE" ]
    then
        LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
        date "+%F %T Removing $BKP_DIR ($LA)" >> $LOGFILE
        nice $NOCACHE rm -rf $BKP_DIR &
        LimitCPULoad $! rm
    #   if [ $DEBUG -eq 0 ]
    #   then
    #                su - ${ZUSER} -c 'zmcontrol stop'
    #                sleep 2
    #                [ -x /usr/local/sbin/KillOldZimbraProcs.pl ] && KillOldZimbraProcs.pl
    #                su - ${ZUSER} -c 'zmcontrol start'
    #        fi
    fi
    
    #==============================================================================
    # Create an archive of essential config files
    LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
    date "+%F %T Creating archive of essential config files ($LA)" >> $LOGFILE
    nice chown -R ${ZUSER}:${ZUSER} /backup &> /dev/null
    nice $NOCACHE tar -czf /backup/${THISHOST}.zosss.tgz $FILELIST >> $LOGFILE 2>&1 &
    LimitCPULoad $! tar

    #==============================================================================
    # Copy files to backup server (if possible)
    local DB1_BKP=
    if [ -n "$DB1_BKP" ]
    then
        ping -qc 3 $DB1_BKP &> /dev/null
        if [ $? -eq 0  ] && [ -x /usr/bin/pscp ]
        then
            U=  # Username for pscp
            P=  # Password for pscp
            LoadCheck
            $NOCACHE pscp -q -batch -4 -l $U -pw $P $BKP_DIR/../${THISHOST}.zosss.tgz $DB1_BKP:
        fi
    fi

    # Copy ${ZUSER} backups from this server to remote server
    # The SSH parameters
    local SSH_SERVER=''
    local SSH_USERID=''
    local SSH_PASSWD=''
    local LOCALMOUNT='/tmp/sshbkp'
    local REM_MOUNT='/backup/zimbra2'

    if [ -n "$SSH_SERVER"  ] && [ -n "$SSH_USERID"  ] && [ -n "$SSH_PASSWD" ]
    then
        # Install "sshfs" if necessary
        dpkg-query -s sshfs &> /dev/null
        [ $? -ne 0 ] && apt-get -qy sshfs
    
        # Create SSHFS mountpoint
        mkdir -p $LOCALMOUNT

        # Create the tunnel
        LDEBUG=''
        [ "T$DEBUG" = 'T-v' ] && LDEBUG='-o sshfs_debug'
        # As per http://www.admin-magazine.com/HPC/Articles/Sharing-Data-with-SSHFS
        echo "$SSH_PASSWD" | \
            sshfs ${SSH_USERID}@${SSH_SERVER}:$REM_MOUNT $LOCALMOUNT -o password_stdin \
             -o nonempty -o ServerAliveInterval=60 -o cache=yes \
             -o kernel_cache -o compression=no $LDEBUG

        # Delete really old backups
        LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
        date "+%F %T Deleting old backups on '$SSH_SERVER' ($LA)" >> $LOGFILE
        find /opt/${ZUSER}/backup $LOCALMOUNT -maxdepth 1 -type f -name "zimbrastore*tar" -mtime +2 -delete
        # Copy the remaining backups
        LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
        date "+%F %T Copying backups to '$SSH_SERVER' ($LA)" >> $LOGFILE
        $NOCACHE rsync -rlptD $DEBUG --log-file=/tmp/zimbra-backup.log --delete --size-only --no-g \
          /opt/${ZUSER}/backup/zimbrastore*tar $LOCALMOUNT
        LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
        date "+%F %T Copied backups to '$SSH_SERVER' ($LA)" >> $LOGFILE

        # Tear down the tunnel
        umount $LOCALMOUNT
    fi
}

#-----------------------------------------------------------
function Backup_Zimbra() {

    BackupData "$BKP_DIR"

    #==============================================================================
    # Add a random image from unsplash.com to Login screen
    if [ -s /opt/zimbra/jetty/webapps/zimbra/public/login.jsp ]
    then
        [ -z "$(grep 'source\.unsplash\.com' /opt/zimbra/jetty/webapps/zimbra/public/login.jsp)" ] && patch /opt/zimbra/jetty/webapps/zimbra/public/login.jsp << EOT
--- /opt/zimbra/jetty/webapps/zimbra/public/login.jsp.nosplash  2021-07-09 15:07:14.886242776 -0400
+++ /opt/zimbra/jetty/webapps/zimbra/public/login.jsp   2021-07-09 15:07:36.614061196 -0400
@@ -504,6 +504,15 @@
 
 </head>
 <c:set value="/img" var="iconPath" scope="request"/>
+<style>
+     .LoginScreen {
+         background-color    : #777 !important;
+         background-image    : url('https://source.unsplash.com/random/featured/?sky,water') !important;
+         background-position : center;
+         background-repeat   : no-repeat;
+         background-size     : cover;
+     }
+     </style>
 <body onload="onLoad();">
 
        <div class="LoginScreen">
EOT
    fi
}

#-----------------------------------------------------------
function Backup_Carbonio() {

    # Local variables
    local BKP_DIR='/backup'
    local BKP_TYPE='incremental'
    local ARCHIVE_NAME=''
    local USE=0
    local AVAIL=0

    # Make sure that the "zmmailbox .. getRestURL" command works
    RESTART_MAILBOXD=0
    su - zextras -c 'zmprov gad' > /tmp/$$.AllDomains
    [ -s /tmp/$$.AllDomains ] || return

    while read -r CD
    do
        su - zextras -c "zmprov gd $CD" | grep -E 'zimbraPublicService(Port|Protocol)' > /tmp/$$.$CD
        if [ ! "T$(awk '/^zimbraPublicServicePort/{print $NF}' /tmp/$$.$CD)" = 'T443' ]
        then
             su - zextras -c "zmprov md $CD zimbraPublicServicePort 443"
             RESTART_MAILBOXD=$((RESTART_MAILBOXD + 1))
        fi
        if [ ! "T$(awk '/^zimbraPublicServiceProtocol/{print $NF}' /tmp/$$.$CD)" = 'Thttps' ]
        then
             su - zextras -c "zmprov md $CD zimbraPublicServiceProtocol https"
             RESTART_MAILBOXD=$((RESTART_MAILBOXD + 1))
        fi
        rm -f /tmp/$$.$CD
    done < /tmp/$$.AllDomains
    if [ ! "T$(su - zextras -c 'zmprov gcf zimbraPublicServiceHostname' | awk '/^zimbraPublicServiceHostname/{print $NF}')" = 'Tmailtest.digitaltowpath.org' ]
    then
        su - zextras -c 'zmprov mcf zimbraPublicServiceHostname mailtest.digitaltowpath.org'
        RESTART_MAILBOXD=$((RESTART_MAILBOXD + 1))
    fi

    # Enable HTTP Strict Transport Security (HSTS) and disable search indexing of your server by web crawlers
    su - zextras -c 'zmprov gcf zimbraResponseHeader' > /tmp/$$
    if [ -z "$(grep '^zimbraResponseHeader: Strict-Transport-Security' /tmp/$$)" ]
    then
        cat << EOT >> /tmp/$$.newResponseHeaders
mcf +zimbraResponseHeader "Strict-Transport-Security: max-age=31536000; includeSubDomains"
EOT
        RESTART_MAILBOXD=$((RESTART_MAILBOXD + 1))
    fi
    if [ -z "$(grep '^zimbraResponseHeader: X-XSS-Protection' /tmp/$$)" ]
    then
        cat << EOT >> /tmp/$$.newResponseHeaders
mcf +zimbraResponseHeader "X-XSS-Protection: 1; mode=block"
EOT
        RESTART_MAILBOXD=$((RESTART_MAILBOXD + 1))
    fi
    if [ -z "$(grep '^zimbraResponseHeader: X-Content-Type-Options' /tmp/$$)" ]
    then
        cat << EOT >> /tmp/$$.newResponseHeaders
mcf +zimbraResponseHeader "X-Content-Type-Options: nosniff"
EOT
        RESTART_MAILBOXD=$((RESTART_MAILBOXD + 1))
    fi
    if [ -z "$(grep '^zimbraResponseHeader: X-Robots-Tag' /tmp/$$)" ]
    then
        cat << EOT >> /tmp/$$.newResponseHeaders
mcf +zimbraResponseHeader "X-Robots-Tag: noindex"
EOT
        RESTART_MAILBOXD=$((RESTART_MAILBOXD + 1))
    fi
    if [[ ! "$(su - zextras -c 'zmprov gcf zimbraMailKeepOutWebCrawlers')" =~ TRUE ]]
    then
        cat << EOT >> /tmp/$$.newResponseHeaders
mcf zimbraMailKeepOutWebCrawlers TRUE
EOT
        RESTART_MAILBOXD=$((RESTART_MAILBOXD + 1))
    fi
    if [ -s /tmp/$$.newResponseHeaders ]
    then
        chmod 644 /tmp/$$.newResponseHeaders
        su - zextras -c "zmprov < /tmp/$$.newResponseHeaders"
    fi
    [ $RESTART_MAILBOXD -ne 0 ] && su - zextras -c 'zmmailboxdctl restart'

    # Create the backup for individual items
    BackupData "${BKP_DIR}/backups"

    # Backup ZFS snapshot
    if [ -n "$(grep ' /backup zfs' /proc/mounts)" ]
    then
        if [ ! -d "${BKP_DIR}/opt/zextras" ]
        then
            echo "ERROR: Backup area does not exist"
            return
        fi    

        # Check available disk space in the backup area
        df -m ${BKP_DIR}/opt/zextras | awk '!/^Filesystem/{print $3" "int($4/3)}' | while read -r USE AVAIL
        do
            if [ $AVAIL -lt $USE ]
            then
                echo "ERROR: Not enough space left on ${BKP_DIR}"
                return
            fi
        done

        # Tar up the backup
        if [ $(date +%w) -eq 6 ] || [ ! -s ${BKP_DIR}/${THISHOST}.snar ]
        then
            # Do a full backup on Saturdays (or if it doesn't exist yet)
            rm -f ${BKP_DIR}/${THISHOST}.snar
            BKP_TYPE='full'
        fi
        LoadCheck
        LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
        date "+%F %T Creating archive of ZFS snapshot ($LA)" >> $LOGFILE
        ARCHIVE_NAME=$BKP_DIR/${THISHOST}'.zfs_'${BKP_TYPE}'_backup.'$(date '+%Y-%m-%d')'.tar'
        nice $NOCACHE tar -cf $ARCHIVE_NAME -g ${BKP_DIR}/${THISHOST}.snar \
          --warning=no-file-ignored ${BKP_DIR}/opt/zextras >> $LOGFILE 2>&1 &
        LimitCPULoad $! tar
        if [ ! -s $ARCHIVE_NAME ]
        then
            echo "ERROR: Backup archive could not be created"
            return
        fi

        # Compress the archive
        LoadCheck
        LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
        date "+%F %T Compressing ZFS snapshot archive ($LA)" >> $LOGFILE
        ${GZIP} -9f ${ARCHIVE_NAME} &
        LimitCPULoad $! ${GZIP}
        if [ ! -s ${ARCHIVE_NAME}.gz ]
        then
            echo "ERROR: Backup archive could not be compressed"
            return
        fi

        # Delete any backups older than 8 days
        LoadCheck
        LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
        date "+%F %T Deleting old backups ($LA)"
        nice find ${BKP_DIR} -maxdepth 1 -type f \( -name "${THISHOST}.backup.*.tar.gz" -o -name "${THISHOST}.backup.*.tar" \) -mtime +7 -delete &
        LimitCPULoad $! find
    fi
}

##################
# Main program
##################
if [ $DEBUG -eq 0 ]
then
    # Wait between 3 and 20 minutes
    ST=$(($RANDOM % 1200))
    [ $ST -lt 180 ] && ST=$(($ST + 180))
    LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
    date "+%F %T Waiting $ST seconds ($LA)" >> $LOGFILE
    sleep $ST
fi

if [ "T$SERVER_TYPE" = 'TC' ]
then
    # Copy from the ZFS backup area
    Backup_Carbonio
else
    # Copy specific items from Zimbra
    Backup_Zimbra
fi

#==============================================================================
# Restart zimbra services every other Sunday
if [ $(($(date +%-U) % 2)) -eq 0  ] && [ $(date +%w) -eq 0 ]
then
    LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
    date "+%F %T Restarting Zimbra services ($LA)" >> $LOGFILE
    if [ "T$SERVER_TYPE" = 'TZ' ]
    then
        su - zimbra -c 'zmcontrol restart'
    else
        su - zextras -c 'zmcontrol restart'
    fi
fi

#==============================================================================
# Upgrade OS and reboot if necessary
[ -x /usr/local/sbin/OSUpdate.sh ] && /usr/local/sbin/OSUpdate.sh

#==============================================================================
# We are done
LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
date "+%F %T $0: done ($LA)" >> $LOGFILE
exit 0
