#!/bin/bash
# Version 20230107-223717 checked into repository
# When the num of deleted docs are > 50,000, index compaction is recommended
# https://bugzilla.zimbra.com/show_bug.cgi?id=76414
# stsimb Sep 2015
# Original: https://raw.githubusercontent.com/stsimb/zimbra-scripts/master/compact-index.sh
# consult@btoy1.net Nov 2022
# Numerous enhancements
################################################################
# (c) Copyright 2022 B-LUC Consulting, Thomas Bullinger, and stsimb
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/Zimbra/ZimbraCompactIndeces.sh

export PATH="/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/sbin:/usr/sbin"
SCRIPT_NAME=${0##*/}
LOCKFILE="/tmp/${SCRIPT_NAME}.lock"
[ -f ${LOCKFILE} ] && logger "$0 already running......" && echo "Already running..." && exit 1
date > "${LOCKFILE}"

### REAL START SCRIPT #########################################################

THRESHOLD=50000

input=$(mktemp /tmp/$$XXXXXXXX)

# delete lock and temp file at the end
trap "rm -f $LOCKFILE $input" EXIT

zmprov='/opt/zimbra/bin/zmprov'
zmhostname='/opt/zimbra/bin/zmhostname'

# get all active accounts (might take a while)
nice $zmprov gqu $($zmhostname) | cut -d' ' -f1,3 | sort -k 2 -rn | cut -d' ' -f1 | \
  grep -Ev '^(ham|spam|virus-quarantine|galsync|wiki)' | sort > ${input}
# bail if we didn't find any accounts
[ $(sed -n '$=' ${input}) -eq 0 ] && exit 1

# process all accounts
for acct in $(< ${input})
do
    echo -n "$(date '+%F %T') ${acct}"

    # getIndexStats
    stats="$($zmprov getIndexStats $acct)"
    # skip any non-existing accounts
    if [ $? -ne 0 ]
    then
        echo " -> skip index compaction."
        continue
    fi
    echo -n " ${stats//:/ }"

    # compare with threshold or 10% mark
    set $(awk '{gsub(/:/," ");print $3" "$NF}' <<< $stats)
    maxDocs=$1
    numDeletedDocs=$2
    # skip accounts with unknown deleted items
    if [ -z "${numDeletedDocs}" ]
    then
        echo " -> skip index compaction."
        continue
    fi
    COMPACT=0
    if [ ${numDeletedDocs}  -ne 0 ]
    then
        if [ ${numDeletedDocs} -gt ${THRESHOLD} ] || [ $numDeletedDocs -gt $((maxDocs / 10)) ]
        then
            COMPACT=1
        fi
    fi
    if [ $COMPACT -ne 0 ]
    then
        # start compact job
        echo -n " => COMPACT INDEX "
        su - zimbra -c "$zmprov compactIndexMailbox $acct start"
    else
        # skip this account
        echo " -> skip index compaction."
    fi
done

### REAL END SCRIPT ###########################################################
### Please do not write below this line.

exit 0
