#!/bin/bash
# Version 20230508-034043 checked into repository
################################################################
# (c) Copyright 2022 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/Zimbra/carbonio.postinstall.sh

#cd /usr/local/src
#if [ ! -h zcs ]
#then
#    echo "Create a symbolic link for 'zcs' first"
#    exit 1
#fi

trap "rm -rf /tmp/$$*" EXIT

#if [ -x /usr/bin/dpkg ] 
#then
#    if [ "T$DISTRIB_CODENAME" = 'Ttrusty' ]
#    then
#        apt-get install libgmp10 pax sqlite3 sysstat libperl5.18 libaio1 unzip
#    else
#        apt-get install libgmp3c2 libgmp10 pax sqlite3 sysstat libperl5.14 libperl5.22 libperl5.26 libaio1 unzip nocache
#    fi
#fi

#---------------------------------------------------------------
# Set the correct hostname
#---------------------------------------------------------------
THISHOST=$(hostname)
[[ $THISHOST = *.* ]] || THISHOST=$(hostname -f)
read -p "Current hostname is '$THISHOST' - is this correct [Y/n] ? " YN
[ -z "$YN" ] && YN='Y'
if [ ! "T${YN^^}" = 'TY' ]
then
    NEWHN=''
    while [ -z "$NEWHN" ]
    do
        read -p 'New hostname (you must specify the fully qualified host name) : ' NEWHN
        [ -n "$NEWHN" ] && [[ $NEWHN = *.* ]] && break
        NEWHN=''
    done

    # Set the new hostname and delete the old one
    echo "$(hostname -I) $NEWHN" >> /etc/hosts
    sed -i "/$THISHOST/d" /etc/hosts
    hostnamectl set-hostname "$NEWHN"
    THISHOST="$NEWHN"
fi

#---------------------------------------------------------------
# Setup the repository
#---------------------------------------------------------------
echo "deb https://repo.zextras.io/release/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/zextras.list

# Get the GPG key
KEY='52FD40243E584A21'
gpg --verbose --keyserver hkp://keyserver.ubuntu.com:80 --receive-keys $KEY || exit 1
YN='Y'
if [ -s /etc/apt/trusted.gpg.d/zextras.asc ]
then
    # By default don't overwrite an existing file
    read -p "zextras key already exist - overwrite [y/N] ? " YN
    [ -z "$YN" ] && YN='N'
fi
# Install the GPG key and make it availabel for "apt"
[ "T${YN^^}" = 'TY' ] && gpg --output - --armor --export $KEY > /etc/apt/trusted.gpg.d/zextras.asc
[ -s /etc/apt/trusted.gpg.d/zextras.asc ] || exit 1 
chmod 644 /etc/apt/trusted.gpg.d/zextras.asc

# Delete the key from the GPG keyring
gpg --fingerprint --with-colons $KEY |\
  grep "^fpr" | sed -n 's/^fpr:::::::::\([[:alnum:]]\+\):/\1/p' |\
    xargs gpg --batch --delete-keys

#---------------------------------------------------------------
# Update, upgrade the system and install carbonio
#---------------------------------------------------------------
apt-get update && apt-get upgrade && \
 apt-get install service-discover-server \
  carbonio-directory-server \
  carbonio-proxy \
  carbonio-webui carbonio-files-ui \
  carbonio-mta \
  carbonio-appserver carbonio-logger \
  carbonio-user-management \
  carbonio-files-ce carbonio-files-db \
  carbonio-storages-ce \
  carbonio-preview-ce \
  carbonio-docs-connector-ce carbonio-docs-editor \
  carbonio-admin-ui carbonio-admin-console-ui \
  carbonio-admin-login-ui postgresql-12

#---------------------------------------------------------------
# Setup carbonio once
#---------------------------------------------------------------
if [ $(ls -l /opt/zextras/log/zmsetup*log | wc -l) -lt 1 ]
then
    DB_PASSWD=''
    DEF_PASSWD=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-16})
    while [ -z "$DB_PASSWD" ]
    do
        read -p 'Please type the main password for services (at least 8 characters - default is '$DEF_PASSWD'): ' DB_PASSWD
        if [ -z "$DB_PASSWD" ]
        then
            DB_PASSWD="$DEF_PASSWD"
        fi
        if [ ${#DB_PASSWD} -lt 8 ]
        then
            echo "ERROR: Password is too short"
            echo
            DB_PASSWD=''
        elif [[ "$DB_PASSWD" =~ ( |\') ]]
        then
            echo "ERROR: Password contains spaces or '|'"
            echo
            DB_PASSWD=''
        fi
    done
    echo "Use '$DB_PASSWD' all along the way"
    read -p 'Press ENTER to continue' C

    # Bootstrap the carbonio services
    carbonio-bootstrap || exit 1
    echo "The mesh password is stored in '/var/lib/service-discover/password'"

    # Setup the carbonio mesh
    service-discover setup-wizard
    # A workaround for a permission issue
    mkdir -p /etc/zextras/service-discover
    touch /etc/zextras/service-discover/carbonio-docs-editor.hcl
    chmod 644 /etc/zextras/service-discover/carbonio-docs-editor.hcl

    # Finish the carbonio mesh   
    pending-setups -a
    chmod a+r /etc/zextras/carbonio-mailbox/token

    # Setup the database
    su - postgres -c "psql --command=\"CREATE ROLE carbonio_adm WITH LOGIN SUPERUSER encrypted password '$DB_PASSWD';\""
    su - postgres -c "psql --command=\"CREATE DATABASE carbonio_adm owner carbonio_adm;\""
    PGPASSWORD=$DB_PASSWD carbonio-files-db-bootstrap carbonio_adm 127.0.0.1

    # Restart carbonio services
    su - zextras -c 'zmcontrol stop; zmcontrol start'

    # Set the password for the "zextras" email account
    ZEXTRAS_ACCT=$(su - zextras -c 'carbonio -l gaa' | grep -m 1 ^zextras)
    su - zextras -c "carbonio prov setpassword $ZEXTRAS_ACCT $DB_PASSWD"
fi

#---------------------------------------------------------------
# We are done
#---------------------------------------------------------------
exit 0
