#!/bin/bash
# Version 20241008-092539 checked into repository

# Don't run several instances
PROG=${0##*/}
LOCKFILE=/tmp/${PROG}.lock
if [ -s $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps -p $MYPID | grep $MYPID)" ] || exit 0
fi

# The process is not running (or no lockfile exists)
echo $$ > $LOCKFILE
trap "rm -rf $LOCKFILE /tmp/$$*; exit 0" 1 2 3 15 EXIT

# The directory for backups etc.
if [ -d /backup ]
then
    mkdir -p /backup/xfer && cd /backup/xfer || exit
elif [ -d /opt/zimbra/backup ]
then
    mkdir -p /opt/zimbra/backup/xfer && cd /opt/zimbra/backup/xfer || exit
else
    date "+%F %T: Can't find a backup directory"
    exit 1
fi
mkdir -p data
LOGFILE='BackupAll.log'
date "+%F %T: Starting backups for transfer"  >> $LOGFILE

# Get the number of physical CPUs in the system
MAXLOAD=$(grep physical.id /proc/cpuinfo | sort -u | wc -l)

function LoadCheck() {

    # Wait until the load average settled somewhat
    LA=$(uptime | cut -dl -f2-)
    date "+%F %T: Checking system load against $MAXLOAD (l$LA)" >> $LOGFILE
    until [ $(awk '{print int($1)}' /proc/loadavg) -lt $MAXLOAD ]
    do
        sleep 4
    done

    LA=$(uptime | cut -dl -f2-)
    date "+%F %T: System load after check (l$LA)" >> $LOGFILE
}

# Get the default socket timeout for Zimbra
DEFAULT_ST=$(/opt/zimbra/bin/zmlocalconfig socket_so_timeout | awk '{print $NF}')

# Create the list of all user accounts (if necessary)
if [ ! -s AllAccounts.txt ]
then
    date "+%F %T: Getting all accounts" >> $LOGFILE
    su - zimbra -c 'zmprov -l gaa' | grep -Ev '^((galsync|ham|spam)\.|virus-)' | sort > AllAccounts.txt
fi

# The Zimbra hostname
ZM_HOSTNAME=$(/opt/zimbra/bin/zmhostname)

while read -r ACCOUNT
do
    # Skip any disabled accounts
    [[ $ACCOUNT =~ disabled ]] && continue
    LoadCheck
    if [ -s data/account-data.${ACCOUNT}.xfer.tgz ]
    then
        date "+%F %T: Backup for account '$ACCOUNT' already exists - skipping" >> $LOGFILE
        continue
    fi
    # Allow for longer socket timeouts
    date "+%F %T: Backing up account '$ACCOUNT'" >> $LOGFILE
    A_STATUS=$(/opt/zimbra/bin/zmprov -l ga $ACCOUNT zimbraAccountStatus | awk '/zimbraAccountStatus/{print $NF}')
    if [ ! "T$A_STATUS" = 'Tactive' ]
    then
        date "+%F %T: Account '$ACCOUNT' not active ($A_STATUS) - skipping" >> $LOGFILE
        continue
    fi
    su - zimbra -c 'zmlocalconfig -e socket_so_timeout=3000000; zmlocalconfig --reload &> /dev/null'
    su - zimbra -c "nice nocache zmmailbox -u https://${ZM_HOSTNAME}:7071 -t 0 -z -m $ACCOUNT getRestURL -u https://${ZM_HOSTNAME}:8443 '//?fmt=tgz'" > data/account-data.${ACCOUNT}.xfer.tgz 2>> $LOGFILE
    if [ $? -eq 0 ]
    then
        date "+%F %T: Successfully backed up account '$ACCOUNT'" >> $LOGFILE
    else
        date "+%F %T: Unsuccessfully backed up account '$ACCOUNT' - check $LOGFILE for details" >> $LOGFILE
    fi
    # Restore standard socket timeouts
    su - zimbra -c "zmlocalconfig -e socket_so_timeout=${DEFAULT_ST}; zmlocalconfig --reload &> /dev/null"
    
    if [ $(df -m . | awk '/dev/{print $4}') -lt 100 ]
    then
        date "+%F %T: Less than 100 MB disk space left - stopping backups after account '$ACCOUNT'" >> $LOGFILE
        exit 1
    fi
    sleep 20
done < AllAccounts.txt

# We are done
date "+%F %T: Finished backups for transfer"  >> $LOGFILE
exit 0
