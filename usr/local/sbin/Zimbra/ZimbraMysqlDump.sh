#!/bin/bash
# Version 20241008-092611 checked into repository
################################################################
# (c) Copyright 2012 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/Zimbra/ZimbraMysqlDump.sh

export PATH=/opt/zimbra/bin:/opt/zimbra/common/lib/jvm/java/bin:/opt/zimbra/common/bin:/opt/zimbra/common/sbin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
if [ -d /opt/zimbra/bin ]
then
    SERVER_TYPE='Z'
elif [ -d /opt/zextras/bin ]
then
    SERVER_TYPE='C'
else
    echo "ERROR: Neither a Zimbra nor a Carbonio server"
    exit 1
fi

# This must run as the user "zimbra"
if [ -z "$(id | grep zimbra)" ]
then
    echo "You need be 'zimbra' to run this script!"
    exit 1
fi

#==============================================================================
# Don't run several instances
PROG=${0##*/}
LOCKFILE=/tmp/${PROG}.lock
if [ -s $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps -p $MYPID | grep $MYPID)" ] || exit 0
fi

# The process is not running (or no lockfile exists)
echo $$ > $LOCKFILE
trap "rm -f $LOCKFILE /tmp/$$*; exit 0" 1 2 3 15 EXIT

DEBUG=''
[[ $- ==  *x* ]] && DEBUG='-v'

trap "rm -f /tmp/$$*" EXIT

#==============================================================================
mysql -NBe 'SHOW DATABASES' | grep -Ev '(test|(information|performance)_schema)' > /tmp/$$.dbs
while read -r DB
do
    date "+%F %T: Backing up database $DB"
        mysql -NBe 'SHOW TABLES' $DB > /tmp/$$.${DB}.tables
        rm -f /tmp/$$.${DB}.save
        while read -r TABLE
        do
        [ "T$DEBUG" = 'T-v' ] && date "+%F %T: Backing up table $TABLE from database $DB"
                rm -f /tmp/${DB}.${TABLE}.csv
        cat << EOT >> /tmp/$$.${DB}.save
SELECT * from $TABLE
  INTO OUTFILE '/tmp/${DB}.${TABLE}.csv'
  FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
  LINES TERMINATED BY '\n';
EOT
    done < /tmp/$$.${DB}.tables
    mysql $DEBUG $DB < /tmp/$$.${DB}.save
    (cd /tmp/; rm -f /tmp/Zimbra_Mysql_Backup.${DB}.tgz && tar $DEBUG -czf /tmp/Zimbra_Mysql_Backup.${DB}.tgz ${DB}*.csv) && rm -f /tmp/${DB}*.csv
        echo
done < /tmp/$$.dbs

#==============================================================================
# We are done
exit 0
