#!/usr/bin/perl
# Version 20230129-081139 checked into repository
#--------------------------------------------------------------------
# (c) CopyRight 2023 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#--------------------------------------------------------------------
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/Zimbra/PopulateZimbraDomain.pl
#
# Read a list of accounts from a CSV file and create the necessary
#  commands for a "zmprov" run in Zimbra
# Recommended usage:
# $ PopulateZimbraDomain.pl <csv> > /tmp/z; chown zimbra /tmp/z
# $ su - zimbra -c 'zmprov' < /tmp/z
#
# The CSV file must have this format using 7 fields:
#
#"E-mail account","SA level","Auto respond","Forward","Quota","Max messages","Usage"
#All lines without a "@" in the "E-mail account" field are comments
#All lines with "No" in the "Forward" field specify real email accounts
#All lines with email address(es) in the "Forward" field are forwarding

#--------------------------------------------------------------------
# Common sense options:
use strict qw(vars subs);
no warnings;
use warnings qw(FATAL closed threads internal debugging pack
    portable prototype inplace io pipe unpack malloc
    glob digit printf layer reserved taint closure
    semicolon);
no warnings qw(exec newline unopened);

#--------------------------------------------------------------------
# Needed packages
#--------------------------------------------------------------------
use Text::CSV;
use POSIX qw(strftime);

#--------------------------------------------------------------------
# Globals
#--------------------------------------------------------------------
my @Accounts          = ();
my @DistributionLists = ();
my @Forwards          = ();

#--------------------------------------------------------------------
# Generate a random string with a specified length
#--------------------------------------------------------------------
# See https://www.codeproject.com/articles/3681/generating-quot-random-quot-strings-for-perl-based
sub generate_random_string
{
    my $stringsize   = shift;
    my @alphanumeric = ( 'a' .. 'z', 'A' .. 'Z', 0 .. 9, '_', '!' );
    my $randstring   = '';
    # We need at least one digit or punctuation character in the string!
    while ( $randstring !~ /(?:\d|_|!)/o )
    {
        $randstring = join( '',
            map $alphanumeric[ rand @alphanumeric ],
            0 .. $stringsize );
    } ## end while ( $randstring !~ /(?:\d|_|!)/o...)
    return $randstring;
} ## end sub generate_random_string

#--------------------------------------------------------------------
# Main program
#--------------------------------------------------------------------
# Get the input file as specified as the first argument
my $infile = $ARGV[0] or die "Need to get CSV file on the command line\n";
open my $fh, "<:encoding(utf8)", "$infile" or die "$infile: $!";

#--------------------------------------------------------------------
# Initialize the CSV parser
my $csv = Text::CSV->new(
    {   binary    => 1,
        auto_diag => 1,
        sep_char  => ','    # not really needed as this is the default
    }
) or die "$!";

#--------------------------------------------------------------------
# Main loop through the CSV file
#--------------------------------------------------------------------
while ( my $row = $csv->getline($fh) )
{
    # We need exactly 7 fields in each row
    die "ERROR: More than 7 fields in '$row->[0]'\n"
        if ( scalar( @{$row} ) > 7 );

    # Skip comments
    $row->[0] =~ m/\@/ or next;

    # We need the current date and time along
    #  with a password to create an account
    my $Now = strftime( "%F %T", localtime );
    my $pw  = generate_random_string(16);
    if ( $row->[3] =~ /No/ )
    {
        # We have a pure email account
        push( @Accounts,
            "ca $row->[0] $pw zimbraPasswordMustChange TRUE zimbraNotes 'Migrated $Now'"
        );
    } else
    {
        my (@F) = split( /,/, $row->[3] );
        if ( scalar(@F) > 1 )
        {
            # We have a distribution list
            push( @DistributionLists, "cdl $row->[0]" );
            push( @DistributionLists,
                "mdl $row->[0] zimbraNotes 'Migrated $Now'" );
            foreach (@F)
            {
                push( @DistributionLists, "adlm $row->[0] $_" );
            } ## end foreach (@F)
        } else
        {
            # We have an email account with forwarding
            push( @Accounts,
                "ca $row->[0] $pw zimbraPasswordMustChange TRUE zimbraNotes 'Migrated $Now'"
            );

            # Remember the forwarding
            my (@F) = split( /,/, $row->[3] );
            push( @Forwards,
                "ma $row->[0] zimbraPrefMailForwardingAddress $F[0]" );
        } ## end else [ if ( scalar(@F) > 1 ) ]
    } ## end else [ if ( $row->[3] =~ /No/...)]
} ## end while ( my $row = $csv->getline...)
if ( not $csv->eof )
{
    # Show potential CSV errors and exit
    $csv->error_diag();
} ## end if ( not $csv->eof )
close $fh;

#--------------------------------------------------------------------
# Show what we found
print "# Email ACCOUNTS:\n";
foreach (@Accounts)
{
    # First all accounts
    print "$_\n";
} ## end foreach (@Accounts)
print "# FORWARDS:\n";
foreach (@Forwards)
{
    # Last all forwards
    print "$_\n";
} ## end foreach (@Forwards)
print "# DISTRIBUTION LISTS:\n";
foreach (@DistributionLists)
{
    # Last all distribution lists
    print "$_\n";
} ## end foreach (@DistributionLists...)

#--------------------------------------------------------------------
# We are done
exit(0);
__END__
