#!/usr/bin/perl -Tw
#--------------------------------------------------------------------
# (c) CopyRight 2022 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#--------------------------------------------------------------------
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/Zimbra/ZimbraGetClients.pl
use 5.0010;

# Constants
my $ProgName = '';
( $ProgName = $0 ) =~ s%.*/%%;

# Common sense options:
use strict qw(vars subs);
no warnings;
use warnings qw(FATAL closed threads internal debugging pack
    portable prototype inplace io pipe unpack malloc
    glob digit printf layer reserved taint closure
    semicolon);
no warnings qw(exec newline unopened);

#--------------------------------------------------------------------
# Needed packages
#--------------------------------------------------------------------
use Sys::Hostname;

#--------------------------------------------------------------------
# Globals
#--------------------------------------------------------------------
local $ENV{PATH} = '/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin';

# Host to monitor
my $MHOST = hostname;
unless ( $MHOST =~ /\./ )
{
    if ( open( my $HOSTNAME, '-|', 'hostname -f' ) )
    {
        $MHOST = <$HOSTNAME>;
        close($HOSTNAME);
        chomp($MHOST);
    } ## end if ( open( my $HOSTNAME...))
} ## end unless ( $MHOST =~ /\./ )

#--------------------------------------------------------------------
# Main function
#--------------------------------------------------------------------
die "ERROR: Can't find yesterday's Zimbra log file"
    unless ( -s '/var/log/zimbra.log.1.gz' );

# Collect the information
if ( open( my $ZL, '-|', 'gunzip -c /var/log/zimbra.log.1.gz' ) )
{
    my %ClientList  = ();
    my %OSList      = ();
    my %SWList      = ();
    my %BrowserList = ();
    my $Total       = 0;
    my %BrowserName = (
        'FF' => 'Firefox',
        'GC' => 'Google Chrome',
        'IE' => 'Internet Explorer',
        'SA' => 'Safari'
    );
    while ( my $Line = <$ZL> )
    {
        if ( $Line =~ /ua=([^;]+)/o )
        {
            my $AccessClient = "$1";

            # Skip email addresses as clients
            next if ( $AccessClient =~ /\@/o );
            $Total++;

            # Get the specific client
            $ClientList{"$AccessClient"}++;

            # Get the OS
            if (   ( $Line =~ /Windows/io )
                || ( $Line =~ /Microsoft\s+Outlook/io ) )
            {
                $OSList{'Windows'}++;
            } elsif ( $Line =~ /Mac/io )
            {
                $OSList{'Mac'}++;
            } elsif ( $Line =~ /Android/io )
            {
                $OSList{'Android'}++;
            } elsif ( $Line =~ /iP(?:hone|ad)/io )
            {
                $OSList{'iPhone/iPad'}++;
            } ## end elsif ( $Line =~ /iP(?:hone|ad)/io...)

            # Get the software
            if ( $Line =~ /ZimbraWebClient - (\S\S)/io )
            {
                my $WebBrowser = $BrowserName{ uc("$1") };
                $WebBrowser = 'other' unless ( length($WebBrowser) );
                $BrowserList{"$WebBrowser"}++;

                $SWList{'ZimbraWebClient'}++;
            } elsif ( $Line =~ /Outlook/io )
            {
                $SWList{'Outlook'}++;
            } elsif ( $Line =~ /(?:X|iPhone|iPad)\s+Mail/io )
            {
                $SWList{'Apple Mail'}++;
            } ## end elsif ( $Line =~ /(?:X|iPhone|iPad)\s+Mail/io...)
        } ## end if ( $Line =~ /ua=([^;]+)/o...)
    } ## end while ( my $Line = <$ZL> ...)
    close($ZL);

    # Display the OS in descending order of connections
    print
        "Count       | Operating System\n=======================================================\n";
    foreach my $OS (
        sort { $OSList{$b} <=> $OSList{$a} }
        keys %OSList
        )
    {
        printf "%4d (%3d%%) | %s\n", $OSList{"$OS"},
            ( ( $OSList{"$OS"} / $Total ) * 100 ), $OS;
    } ## end foreach my $OS ( sort { $OSList...})

    # Display the software in descending order of connections
    print
        "\nCount       | Software\n=======================================================\n";
    foreach my $SW (
        sort { $SWList{$b} <=> $SWList{$a} }
        keys %SWList
        )
    {
        printf "%4d (%3d%%) | %s\n", $SWList{"$SW"},
            ( ( $SWList{"$SW"} / $Total ) * 100 ), $SW;
    } ## end foreach my $SW ( sort { $SWList...})

    # Display the browsers in descending order of connections
    print
        "\nCount       | Browser\n=======================================================\n";
    foreach my $Browser (
        sort { $BrowserList{$b} <=> $BrowserList{$a} }
        keys %BrowserList
        )
    {
        printf "%4d (%3d%%) | %s\n", $BrowserList{"$Browser"},
            ( ( $BrowserList{"$Browser"} / $Total ) * 100 ), $Browser;
    } ## end foreach my $Browser ( sort ...)

    # Display the individual clients in descending order of connections
    print
        "\nCount       | Client\n=======================================================\n";
    foreach my $Client (
        sort { $ClientList{$b} <=> $ClientList{$a} }
        keys %ClientList
        )
    {
        printf "%4d (%3d%%) | %s\n", $ClientList{"$Client"},
            ( ( $ClientList{"$Client"} / $Total ) * 100 ), $Client;
    } ## end foreach my $Client ( sort {...})
} ## end if ( open( my $ZL, '-|'...))

#--------------------------------------------------------------------
# We are done
exit 0;
__END__
