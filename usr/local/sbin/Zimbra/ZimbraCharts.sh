#!/bin/bash                                                     
################################################################
# (c) Copyright 2012 B-LUC Consulting and Thomas Bullinger          
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################ 

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# We must have a valid account 'zimbra'
[ -z "$(getent passwd zimbra)" ] && exit 0

ZIMBRA_HOME=$(awk -F: '/^zimbra/ {print $6}' /etc/passwd)
THISHOST=$(hostname)
[[ $THISHOST = *.* ]] || THISHOST=$(hostname -f)
THISDOMAIN=${THISHOST#*.}

# Create the charts for yesterday
YESTERDAY=$(date +%Y-%m-%d -d yesterday)
if [ -d $ZIMBRA_HOME/zmstat/$YESTERDAY ]
then                                    
        # Uncompress the saved csv files
        mkdir -p /tmp/zmstat
        cp $ZIMBRA_HOME/zmstat/$YESTERDAY/*gz /tmp/zmstat
        gunzip -f /tmp/zmstat/*gz
        chmod 0644 /tmp/zmstat/*.csv

        # Finally create the charts themselves
        mkdir -p $ZIMBRA_HOME/jetty/webapps/zimbra/charts/$YESTERDAY
        chown -R zimbra:zimbra $ZIMBRA_HOME/jetty/webapps/zimbra/charts/$YESTERDAY
        su - zimbra -c "zmstat-chart -s /tmp/zmstat -d $ZIMBRA_HOME/jetty/webapps/zimbra/charts/$YESTERDAY" > /dev/null
        chown -R zimbra:zimbra $ZIMBRA_HOME/jetty/webapps/zimbra/charts/$YESTERDAY
        rm -rf /tmp/zmstat

        cat << EOT | sendmail -t
From: admin@$THISDOMAIN
To: admin@$THISDOMAIN
Subject: Zimbra charts on $THISHOST
Date: `date -R`

The Zimbra performance charts for $YESTERDAY are now available:
https://${THISHOST}/charts/index.html
EOT
fi

# Delete directories older than one month
for D in zmstat jetty/webapps/zimbra/charts
do
        find ${ZIMBRA_HOME}/$D -type d -mtime +30 -print0 | xargs -0 rm -rf
done

# Recreate the index file for all charts
cd ${ZIMBRA_HOME}/jetty/webapps/zimbra/charts
cat << EOT > index.html
<!DOCTYPE html><html lang='en'>
<head>
 <title>Zimbra Performance Charts</title>
 <meta charset='utf-8' name='viewport' content='width=device-width, initial-scale=1.0'>
 <meta http-equiv='content-type' content='text/html; charset=UTF-8'>
 <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
 <meta http-equiv='refresh' content="$((12 * 60 * 60))">
 <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Fredericka+the+Great|Allura|Amatic+SC|Arizonia|Averia+Sans+Libre|Cabin+Sketch|Francois+One|Jacques+Francois+Shadow|Josefin+Slab|Kaushan+Script|Love+Ya+Like+A+Sister|Merriweather|Offside|Open+Sans|Open+Sans+Condensed|Oswald|Over+the+Rainbow|Pacifico|Romanesco|Sacramento|Seaweed+Script|Special+Elite'>
 <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans'>
 <link rel='stylesheet' type='text/css' href='site.css?v='>
 <!-- See https://www.devdevote.com/markup/css/html-css-centered-menu.html -->
 <style type="text/css" media="screen">
  body {
   margin: 0;
  }
  #menu-centered {
   padding: 10px;
   margin-bottom: 10px;
  }
  #menu-centered ul {
   margin: 0;
   padding: 0;
   text-align: center;
  }
  #menu-centered li {
   display: inline;
   list-style: none;
   padding: 10px 5px 10px 5px;
  }
  #menu-centered a {
   font: normal 14px/24px Arial;
   color: #fff;
   text-decoration: none;
   padding: 5px;
   background: #57a8d6;
  }
  #menu-centered a:hover {
   background: #5fb8eb;
  }
 </style>
</head>
<body>
<p style='text-align:center'>
Zimbra Performance stats exist for the days show below.  Please select the day you are interested in.
</p>
<div id='menu-centered'>
 <ul>
EOT
for D in 2*
do
        [ -d $D ] || continue
        HRD=$(date "+%a, %b %e %Y" -d "$D")
        echo "  <li><a href=${D}/index.html target='_blank' rel='noopener noreferrer'>$HRD</a>" >> index.html
done
cat << EOT >> index.html
 </ul>
</div>
</body>
</html>
EOT
chmod 0644 index.html

# We are done
exit 0
