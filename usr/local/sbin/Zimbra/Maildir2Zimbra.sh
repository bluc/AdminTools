#!/bin/bash
# Version 20230209-113703 checked into repository
################################################################
# (c) Copyright 2023 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/Zimbra/Maildir2Zimbra.sh
# See https://forums.zimbra.org/viewtopic.php?t=45564

# This script must be run as "root"
[ "T$(/usr/bin/id -u)" = 'T0' ] || exit

# Process options from the command line
i_domain=''
i_directory=$(pwd)
i_user='all'
i_what='all'
dryrun=''
ADMIN_ID='admin'
ADMIN_PW=''
while getopts "hd:D:u:w:nP:" OPTION
do
    case "${OPTION}" in
    d)  i_domain="${OPTARG}"
        ;;
    D)  i_directory="${OPTARG}"
        ;;
    u)  i_user="${OPTARG}"
        ;;
    w)  i_what="${OPTARG}"
        ;;
    P)  ADMIN_PW="${OPTARG}"
        ;;
    n)  dryrun='echo'
        ;;
    *)
        cat << EOT
Usage: $0 [ options]

  -d domain   Domain to import emails into [no default]
  -D path     Directory where to read the emails from [default=${i_directory}]
  -u user     User for whom to import emails [default=${i_user}']
  -w what     users|distributionlists|all (default=${i_what})
  -P password Specify password for "$ADMIN_ID"
  -n          Don't actually import or move any email [default=no]
EOT
        exit 1
        ;;
    esac
done
shift $((OPTIND-1))
if [ -z "$i_domain" ] || [ -z "$i_directory" ]
then
    echo 'ERROR: Not enough parameters'
    exit 1
fi
if [ ! -d "$i_directory" ]
then
    echo "ERROR: Path '$directory' not found"
    exit 1
fi
if [ -z "$(su - zimbra -c 'nice zmprov gd '$i_domain)" ]
then
    echo "ERROR: Domain '$i_domain' doesn't exist"
    exit 1
fi
if [ -z "$ADMIN_PW" ]
then
    echo "ERROR: No password specified for '$ADMIN_ID'"
    exit 1
fi
if [ -z "$i_user" ]
then
    echo "No user specified - assuming all users and distribution lists"
    read -p 'Is this what you want [y/N] ? ' YN
    [ -z "$YN" ] && YN='N'
    [ "T${YN^^}" = 'TY' ] || exit 1
fi
[ "T${i_user,,}" = 'Tall' ] && i_user=''
[ "T${i_what,,}" = 'Tall' ] && i_what='users distributionlists'

# Use "nocache" if possible
NOCACHE=''
dpkg-query -S nocache &> /dev/null && NOCACHE='nocache'

# Get imapdedup.py if necessary
HAVE_IMAPDEDUP="$(which imapdedup.py)"
if [ -z "$HAVE_IMAPDEDUP" ]
then
    wget -qO /usr/local/bin/imapdedup.py https://gitlab.com/bluc/AdminTools/raw/master/usr/local/bin/imapdedup.py
    chmod 755 /usr/local/bin/imapdedup.py
fi

# We need the perl "Email::Simple" package
perl -e 'use Email::Simple;' &> /dev/null
[ $? -ne 0 ] && /usr/bin/env DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::='--force-confdef' install libemail-simple-perl

# Create the perl script to extract the message ID from an email
cat << EOT > /tmp/GetMsgId.pl
use Email::Simple;
setpriority(0,\$\$,10);
my \$text = '';
while (<>) {
    last if (/^\$/o);    
    \$text .= "\$_";
}
my \$email = Email::Simple->new(\$text);
my \$from_header = \$email->header('Message-ID');
print "\$from_header";
exit (0);
EOT

# Compute the max. CPU load
MAXLOAD=$(nproc --all)
[[  "$MAXLOAD" =~ ^[0-9]+$ ]] || MAXLOAD=1
if [ $MAXLOAD -gt 16 ]
then
    MAXLOAD=$((MAXLOAD / 4))
else
    MAXLOAD=$((MAXLOAD / 2))
fi

# Get the max. upload size (- 128 bytes for safety)
MAX_UPLOAD_SIZE=$(nc localhost 25 << EOT | awk '/^250-SIZE/{print int($NF - 128)}'
ehlo test
quit
EOT
)

# Start the logfile
LogFile='/root/ImportLog.txt.'$(date "+%FT%T")
date "+%F %T: Starting import" &> $LogFile
date "+%F %T: Maximum allowed email size is ${MAX_UPLOAD_SIZE} bytes" &>> $LogFile

function LoadCheck() {

    # Wait until the load average settled somewhat
    LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
    date "+%F %T Checking system load against $MAXLOAD ($LA)" >> $LogFile
    until [ $(awk '{print int($1 * 100)}' /proc/loadavg) -lt $((MAXLOAD * 100)) ]
    do
        sleep 4
    done

    LA=$(awk '{print "load average: "$1", "$2", "$3}' /proc/loadavg)
    date "+%F %T System load after check ($LA)" >> $LogFile
}

# Remove temp files at exit
trap "rm -rf /tmp/$$*" EXIT

# The directory to preserve emails in
P_DIR1='/opt/zimbra/Email2Large2Import'
mkdir -p "${P_DIR1}"
cat << EOT > "${P_DIR1}/README1ST"
This directory contains emails that exceed the max. allowed
upload size for emails that were found while importing emails
from existing (3rd party) email archives.

Please inspect them manually and determine which emails can
be safely deleted and which ones need to be imported manually.
EOT

# Find any file that is too big to import and move it
#  to the preservation directory
LoadCheck
find $i_directory -type f -size +${MAX_UPLOAD_SIZE}c -print > /tmp/$$.toobig
while read -r EML
do
    date "+%F %T: Email '$EML' is too big to be imported" &>> $LogFile
    if [ -z "$dryrun" ]
    then
        LoadCheck
        date "+%F %T: Preserving '$EML' into '${P_DIR1}" &>> $LogFile
        nice $NOCACHE tar cf - "$EML" | (cd ${P_DIR1}; nice $NOCACHE tar xf -) &>> $LogFile
        nice diff "$EML" "${P_DIR1}${EML}" &>> $LogFile
        if [ $? -eq 0 ]
        then
            rm -f "$EML"
            date "+%F %T: Preserved '$EML' into '${P_DIR1}" &>> $LogFile
        fi
    fi
done < /tmp/$$.toobig
LoadCheck
chown -R zimbra: ${P_DIR1}

# Get all users for the domain
LoadCheck
su - zimbra -c 'nice zmprov -l gaa' | grep "@${i_domain}"'$' > /tmp/$$.i_users
if [ ! -s /tmp/$$.i_users ]
then
    echo "ERROR: No users defined for domain '$i_domain'"
    exit 1
fi
su - zimbra -c 'nice zmprov -l gadl' | grep "@${i_domain}"'$' > /tmp/$$.i_dls

RealUsers=()
DistLists=()
Others=()
for userDir in $(ls -d1 "$i_directory"/*)
do
    user=${userDir##*/}
    if [ -z "$(grep ^$user@$i_domain /tmp/$$.i_users)" ]
    then
        LoadCheck
        NoEmails=$(nice $NOCACHE find $userDir -maxdepth 10 -type d '(' -name cur -o -name new ')' -type f -print | wc -l)
        date "+%F %T: WARNING: User '$user@$i_domain' doesn't exist" &>> $LogFile
        if [ -z "$(grep ^$user@$i_domain /tmp/$$.i_dls)" ]
        then
            date "+%F %T:         '$user@$i_domain' also doesn't exist as a distribution list" &>> $LogFile
            date "+%F %T:         Create it first before continuing" &>> $LogFile
            Others+=("$user")
        else
            date "+%F %T:         '$user@$i_domain' exists as a distribution list" &>> $LogFile
            DistLists+=("$user")
        fi
    else
        # Remember this user
        RealUsers+=("$user")
    fi
done

if [[ ${i_what,,} =~ 'users' ]]
then
    # Process single users
    for user in "${RealUsers[@]}"
    do
        userDir="${i_directory}/${user}"
        # Ignore all but the specified user
        [ -n "$i_user" ] && [ ! "T$user" = "T$i_user" ] && continue
    
        echo &>> $LogFile
        date "+%F %T: Importing emails for user $user" &>> $LogFile
        echo  &>> $LogFile
        LoadCheck
        find $userDir -maxdepth 10 -type d '(' -name cur -o -name new ')' | while read -r line
        do
            fullPath="$(sed -e 's/^[0123456789]*[[:space:]]//' <<< $line)"
            # Exclude some folders altogether
            if [ $(grep -cE 'Maildir/\.(Junk|Trash|Sent|Drafts)/' <<< $fullPath) -ne 0 ]
            then
                continue
            fi
            folder="$(sed -e 's@^.*Maildir/@@' <<< $line)"
            newFolder=$(sed -e 's/^\.//' <<< $folder)
            # replace _ with a space (IMPORTANT! This makes the imported subfolders come out correctly!)
            newFolder="${newFolder//_/ }"
            if [ "T$newFolder" = 'Tcur' ] || [ "T$newFolder" = 'Tnew' ]
            then
                NoMsg=$(ls -1 "$fullPath" | wc -l)
                [ $NoMsg -lt 1 ] && continue

                LoadCheck
                date "+%F %T: Transferring $NoMsg email(s) in Inbox for ${user}@${i_domain}..." &>> $LogFile
                $dryrun su - zimbra -c "nice $NOCACHE zmmailbox -z -m $user@$i_domain addMessage 'Inbox' \"$fullPath\"" &>> $LogFile
                date "+%F %T: Finished transferring inbox." &>> $LogFile

                # Remove any duplicates in this folder
                LoadCheck
                date "+%F %T: Remove possible duplicates for '${user}/Inbox'" &>> $LogFile
                nice imapdedup.py -s localhost -p 143 -a "$ADMIN_ID" -w "${ADMIN_PW}" -u "$user@$i_domain" "Inbox" &>> $LogFile
                continue
            fi

            # Create new folder and import it
            NoMsg=$(ls -1 "$fullPath" | wc -l)
            [ $NoMsg -lt 1 ] && continue
            newFolder="${newFolder%/*}"
            date "+%F %T: Creating folder '/$newFolder' for ${user}@${i_domain}..." &>> $LogFile
            $dryrun su - zimbra -c "nice $NOCACHE zmmailbox -z -m $user@$i_domain createFolder /\"$newFolder\"" &>> $LogFile

            LoadCheck
            date "+%F %T: Transferring $NoMsg email(s) in folder '$folder'..." &>> $LogFile
            $dryrun su - zimbra -c "nice $NOCACHE zmmailbox -z -m $user@$i_domain addMessage \"/$newFolder\" \"$fullPath\"" &>> $LogFile
            date "+%F %T: Finished transferring folder '$folder'." &>> $LogFile

            # Remove any duplicates in this folder
            LoadCheck
            date "+%F %T: Remove possible duplicates for '${user}/${newFolder}'" &>> $LogFile
            nice imapdedup.py -s localhost -p 143 -a "$ADMIN_ID" -w "${ADMIN_PW}" \
              -m -c -u "$user@$i_domain" "${newFolder}" &>> $LogFile
        done

    done
fi

if [[ ${i_what,,} =~ 'distributionlists' ]]
then
    # Process distribution lists
    for DL in "${DistLists[@]}"
    do
        DLDir="${i_directory}/${DL}"
        # Ignore all but the specified user
        [ -n "$i_user" ] && [ ! "T$DL" = "T$i_user" ] && continue

        # Get the members for this distribution list
        su - zimbra -c "nice $NOCACHE zmprov gdl $DL@$i_domain" | sed '1,/^members$/d' > /tmp/$$.${DL}.members
        if [ ! -s /tmp/$$.${DL}.members ]
        then
            echo "WARNING: Empty distribution list '$DL@${i_domain}'" &>> $LogFile
            continue
        fi

        echo &>> $LogFile
        date "+%F %T: Importing emails for distribution list $DL" &>> $LogFile
        echo &>> $LogFile
        LoadCheck
        find $DLDir -maxdepth 10 -type d '(' -name cur -o -name new ')' | while read -r line
        do
            fullPath="$(sed -e 's/^[0123456789]*[[:space:]]//' <<< $line)"
            # Exclude some folders altogether
            if [ $(grep -cE 'Maildir/\.(Junk|Trash|Sent|Drafts)/' <<< $fullPath) -ne 0 ]
            then
                continue
            fi
            folder="$(sed -e 's@^.*Maildir/@@' <<< $line)"
            newFolder=$(sed -e 's/^\.//' <<< $folder)
            # replace _ with a space (IMPORTANT! This makes the imported subfolders come out correctly!)
            newFolder="${newFolder//_/ }"
            if [ "T$newFolder" = 'Tcur' ] || [ "T$newFolder" = 'Tnew' ]
            then
                NoMsg=$(ls -1 "$fullPath" | wc -l)
                [ $NoMsg -lt 1 ] && continue
                for member in $(< /tmp/$$.${DL}.members)
                do
                    if [[ "$member" =~ "@${i_domain}" ]]
                    then
                        LoadCheck
                        date "+%F %T: Transferring $NoMsg email(s) in Inbox to ${member}..." &>> $LogFile
                        $dryrun su - zimbra -c "nice $NOCACHE zmmailbox -z -m $member addMessage 'Inbox' \"$fullPath\"" &>> $LogFile
                        date "+%F %T: Finished transferring inbox." &>> $LogFile

                        # Remove any duplicates in this folder
                        LoadCheck
                        date "+%F %T: Remove possible duplicates for '${member}/Inbox'" &>> $LogFile
                        nice imapdedup.py -s localhost -p 143 -a "$ADMIN_ID" -w "${ADMIN_PW}" -u "$member" "Inbox" &>> $LogFile
                    else
                        date "+%F %T: Notice: Member '$member' is outside the domain '${i_domain}'" &>> $LogFile
                    fi
                done
                continue
            fi

            # Create new folder and import it (if it has any emails in it)
            NoMsg=$(ls -1 "$fullPath" | wc -l)
            [ $NoMsg -lt 1 ] && continue
            newFolder="${newFolder%/*}"
            for member in $(< /tmp/$$${DL}.members)
            do
                if [[ "$member" =~ "@${i_domain}" ]]
                then
                    LoadCheck
                    date "+%F %T: Creating folder '/$newFolder' for ${member}..." &>> $LogFile
                    $dryrun su - zimbra -c "nice $NOCACHE zmmailbox -z -m $member createFolder /\"$newFolder\"" &>> $LogFile
                    date "+%F %T: Transferring  $NoMsg email(s) in folder '$folder'..." &>> $LogFile
                    $dryrun su - zimbra -c "nice $NOCACHE zmmailbox -z -m $member addMessage \"/$newFolder\" \"$fullPath\"" &>> $LogFile
                    date "+%F %T: Finished transferring folder '$folder'." &>> $LogFile

                    # Remove any duplicates in this folder
                    LoadCheck
                    date "+%F %T: Remove possible duplicates for '${member}/${newFolder}'" &>> $LogFile
                    nice imapdedup.py -s localhost -p 143 -a "$ADMIN_ID" -w "${ADMIN_PW}" -u "$member" "${newFolder}" &>> $LogFile
                else
                    date "+%F %T: Notice: Member '$member' is outside the domain '${i_domain}'" &>> $LogFile
                fi
            done
        done
    done
fi

# Show what is potentially leftover
if [ ${#Others[@]} -gt 0 ]
then
    cat << EOT &>> $LogFile
$(date "+%F %T")
Other account that don't exist yet or
are neither users or distribution lists:
${Others[*]}
EOT
fi

# Show any errors that were discovered during the import
grep -Ei '(error|warning|notice):' $LogFile  | grep -v mail.ALREADY_EXISTS

# We are done
exit 0
