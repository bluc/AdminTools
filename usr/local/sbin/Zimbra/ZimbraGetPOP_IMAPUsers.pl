#!/usr/bin/perl -Tw
#--------------------------------------------------------------------
# (c) CopyRight 2022 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#--------------------------------------------------------------------
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/Zimbra/ZimbraGetPOP_IMAPUsers.pl
use 5.0010;

# Constants
my $ProgName = '';
( $ProgName = $0 ) =~ s%.*/%%;

# Common sense options:
use strict qw(vars subs);
no warnings;
use warnings qw(FATAL closed threads internal debugging pack
    portable prototype inplace io pipe unpack malloc
    glob digit printf layer reserved taint closure
    semicolon);
no warnings qw(exec newline unopened);

#--------------------------------------------------------------------
# Needed packages
#--------------------------------------------------------------------
use Sys::Hostname;

#--------------------------------------------------------------------
# Globals
#--------------------------------------------------------------------
local $ENV{PATH} = '/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin';

# Host to monitor
my $MHOST = hostname;
unless ( $MHOST =~ /\./ )
{
    if ( open( my $HOSTNAME, '-|', 'hostname -f' ) )
    {
        $MHOST = <$HOSTNAME>;
        close($HOSTNAME);
        chomp($MHOST);
    } ## end if ( open( my $HOSTNAME...))
} ## end unless ( $MHOST =~ /\./ )

my %Accounts;

if ( open( my $ZL, '<', '/var/log/zimbra.log' ) )
{
    while (<$ZL>)
    {
        chomp;
        # Only look at authentication logs
        if (/(Imap|Pop3)SSLServer.*authentication failed for \[([^]]+)\]/o)
        {
            $Accounts{"$2-$1"}++;
        } elsif (/(Imap|Pop3)SSLServer.*user (\S+) authenticated/o)
        {
            $Accounts{"$2-$1"}++;
        } ## end elsif (/(Imap|Pop3)SSLServer.*user (\S+) authenticated/o...)
    } ## end while (<$ZL>)
    close($ZL);
    foreach my $Account_Prot ( sort keys %Accounts )
    {
        my ( $Account, $Prot ) = split( /\-/, $Account_Prot );
        print "$MHOST: Account '$Account' uses '$Prot'\n";
    } ## end foreach my $Account_Prot ( ...)
} else
{
    die "ERROR: Can't open /var/log/zimbra.log\n";
} ## end else [ if ( open( my $ZL, '<'...))]

# We are done
exit 0;
__END__
