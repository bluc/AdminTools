#!/bin/bash
################################################################
# (c) Copyright 2013-2018 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/Zimbra/zimbramfa.postinstall.sh

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE

#--------------------------------------------------------------------
# Check for Zimbra installation
if [ ! -d /opt/zimbra/log ]
then 
    echo "ERROR: Not a Zimbra server"
    exit 1
fi

#--------------------------------------------------------------------
# Get the official installer script and execute it
wget -q https://raw.githubusercontent.com/Zimbra-Community/zimbra-foss-2fa/master/2fa-installer.sh \
  -O /usr/local/sbin/2fa-installer.sh
patch /usr/local/sbin/2fa-installer.sh << EOT
--- /usr/local/sbin/2fa-installer.sh.orig       2021-11-14 09:16:38.874242564 -0500
+++ /usr/local/sbin/2fa-installer.sh    2021-11-14 09:20:13.413375091 -0500
@@ -221,6 +221,15 @@
 cd /tmp
 wget https://github.com/Zimbra-Community/zimbra-foss-2fa/releases/download/0.0.2/tk_barrydegraaff_2fa.zip -O /tmp/tk_barrydegraaff_2fa.zip
 wget https://github.com/Zimbra-Community/zimbra-foss-2fa/releases/download/0.0.1/tk_barrydegraaff_2fa_admin.zip -O /tmp/tk_barrydegraaff_2fa_admin.zip
+# Apply patch for Google Authenticator and "smart phone"
+PATCHDIR=\$(mktemp -d)
+cd \$PATCHDIR
+unzip /tmp/tk_barrydegraaff_2fa.zip
+sed -i 's/install one/install Google Authenticator or one/;s/mobile:/smart phone:/g' tk_barrydegraaff_2fa.js
+zip -f /tmp/tk_barrydegraaff_2fa.zip tk_barrydegraaff_2fa.js
+cd /tmp
+rm -rf \$PATCHDIR
+# Install the zimlets
 su zimbra -c "/opt/zimbra/bin/zmzimletctl deploy /tmp/tk_barrydegraaff_2fa.zip"
 su zimbra -c "/opt/zimbra/bin/zmzimletctl deploy /tmp/tk_barrydegraaff_2fa_admin.zip"
EOT
bash /usr/local/sbin/2fa-installer.sh

#--------------------------------------------------------------------
# Always run this AFTER any update !!!!
grep -m1 -B 2 -A 200 'Zeta Alliance' 2fa-installer.sh > /tmp/$$
bash /tmp/$$

#--------------------------------------------------------------------
# Adjust the text for MFA/2FA in the login screen
sed -i 's@2FA:@MFA:@g;s@2FA.@MFA token.@g' /opt/zimbra/jetty/webapps/zimbra/public/login.jsp

#--------------------------------------------------------------------
# Add a random image from unsplash.com to Login screen
[ -z "$(grep 'source\.unsplash\.com' /opt/zimbra/jetty/webapps/zimbra/public/login.jsp)" ] && patch /opt/zimbra/jetty/webapps/zimbra/public/login.jsp << EOT 
--- /opt/zimbra/jetty/webapps/zimbra/public/login.jsp.nosplash  2021-07-09 15:07:14.886242776 -0400
+++ /opt/zimbra/jetty/webapps/zimbra/public/login.jsp   2021-07-09 15:07:36.614061196 -0400
@@ -504,6 +504,15 @@
 
 </head>
 <c:set value="/img" var="iconPath" scope="request"/>
+<style>
+     .LoginScreen {
+         background-color    : #777 !important;
+         background-image    : url('https://source.unsplash.com/random/featured/?sky,water') !important;
+         background-position : center;
+         background-repeat   : no-repeat;
+         background-size     : cover;
+     }
+     </style>
 <body onload="onLoad();">
 
        <div class="LoginScreen">
EOT

#--------------------------------------------------------------------
# We are done
exit 0
