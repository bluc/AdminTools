#!/bin/bash
################################################################
# (c) Copyright 2018 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/Zimbra/zinstall.sh

if [ ! -h zcs ]
then
    echo "Create a symbolic link for 'zcs' first"
    exit 1
fi

trap "rm -rf /tmp/$$*" EXIT

if [ -x /usr/bin/dpkg ] 
then
    source /etc/lsb-release
    if [ "T$DISTRIB_CODENAME" = 'Ttrusty' ]
    then
        apt-get install libgmp10 pax sqlite3 sysstat libperl5.18 libaio1 unzip
    else
        apt-get install libgmp3c2 libgmp10 pax sqlite3 sysstat libperl5.14 libperl5.22 libperl5.26 libaio1 unzip nocache
    fi
fi

# Remove possibly installed local postfix package
dpkg-query -l postfix 2> /dev/null
[ $? -ne 0 ] && $INSTALL_PROG --purge remove postfix

read -p 'Install or upgrade Zimbra now [y/N] ? ' YN
[ -z "$YN" ] && YN='n'
if [ "T${YN^^}" = 'TY' ]
then
    # Skip any activation check for license?
    ZI_OPTS=''
    read -p 'Skip activation check [Y/n] ? ' YN
    [ -z "$YN" ] && YN='y'
    [ "T${YN^^}" = 'TY' ] && ZI_OPTS='--skip-activation-check'

    cd zcs
    ./install.sh $ZI_OPTS "$@"
    cd /usr/local/src
fi
getent passwd zimbra
[ $? -ne 0 ] && exit 1

# Activate the license
if [ -x /opt/zimbra/bin/zmlicense ]
then
    [ -z "$(/opt/zimbra/bin/zmlicense -p | grep ^ActivationId)" ] && su - zimbra -c 'zmlicense -a'
fi

# Allow encrypted archives
# See http://bugzilla.zimbra.com/show_bug.cgi?id=18321
if [ -s /opt/zimbra/conf/clamd.conf.in ]
then
    perl -p -i -e 's/comment VAR:!zimbraVirusBlockEncryptedArchive/uncomment VAR:zimbraVirusBlockEncryptedArchive/' /opt/zimbra/conf/clamd.conf.in
    su - zimbra -c 'zmprov -l mcf zimbraVirusBlockEncryptedArchive FALSE'
fi

# Exclude insecure SSL ciphers and enable outgoing TLS
su - zimbra -c 'zmprov mcf zimbraSSLExcludeCipherSuites EXP zimbraSSLExcludeCipherSuites Low zimbraSSLExcludeCipherSuites SSLv2 zimbraSSLExcludeCipherSuites SSLv3 zimbraSSLExcludeCipherSuites DES zimbraSSLExcludeCipherSuites RC4'

ZIMBRA_MTA=0
if [ -x /usr/bin/dpkg ] 
then
    dpkg -l zimbra-mta 2> /dev/null
    [ $? -eq 0 ] && ZIMBRA_MTA=1
elif [ -s /etc/redhat-release ]
then
    [ -z "$(yum list | grep zimbra-mta)" ] || ZIMBRA_MTA=1
fi
if [ $ZIMBRA_MTA -ne 0 ]
then
    su - zimbra -c 'zmlocalconfig -e postfix_smtp_use_tls=yes'
fi

ZIMBRA_PROXY=0
if [ -x /usr/bin/dpkg ] 
then
    dpkg -l zimbra-proxy 2> /dev/null
    [ $? -eq 0 ] && ZIMBRA_PROXY=1
elif [ -s /etc/redhat-release ]
then
    [ -z "$(yum list | grep zimbra-proxy)" ] || ZIMBRA_PROXY=1
fi
if [ $ZIMBRA_PROXY -ne 0 ]
then
    # Show original connecting IP address in logs
    #  (needed for authentucation failure detection)
    if [ -z "$(/opt/zimbra/bin/zmprov gcf zimbraMailTrustedIP)" ]
    then
        LOCALIF=$(ip route get 8.8.8.8 | awk '/via/{print $5; exit}')
        LOCALIP=$(ifconfig $LOCALIF | sed -n "s/.*inet addr:\([0-9.]*\).*/\1/p")
        [ -z "$LOCALIP" ] && LOCALIP=$(ifconfig $LOCALIF | sed -n "s/.*inet \([0-9.]*\).*/\1/p")
        [ -z "$LOCALIP" ] && LOCALIP=$(ip addr list $LOCALIF | sed -n "s/.*inet \([0-9.]*\).*/\1/p")
        su - zimbra -c "zmprov mcf +zimbraMailTrustedIP 127.0.0.1 +zimbraMailTrustedIP $LOCALIP"
    fi
fi

# Ensure that the zimbra account is unlocked
Z_PASSWD=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-16})
echo 'zimbra:'"$Z_PASSWD" | chpasswd
passwd -u zimbra

# Increase the max. email size to 25MB and allow
#  clear text logins for IMAP and POP3 (globally)
cat << EOT > /tmp/$$
mcf zimbraFileUploadMaxSize 26214400
mcf zimbraMtaMaxMessageSize 26214400
mcf zimbraImapCleartextLoginEnabled TRUE
mcf zimbraPop3CleartextLoginEnabled TRUE
EOT
chmod 644 /tmp/$$
su - zimbra -c "zmprov < /tmp/$$"
rm -f /tmp/$$

# Switch to "redirect" http mode
su - zimbra -c 'zmtlsctl redirect; zmmailboxdctl restart'
sleep 5

# Make the redolog a bit smaller (default: 4GB)
su - zimbra -c "zmprov mcf zimbraRedoLogRolloverHardMaxFileSizeKB $((2 * 1024 * 1024))"

# The zimbraBatchedIndexingSize value depends on a number of factors including,
#  typical access methods, average size of messages/attachments and
#  CPU/IO capabilities of the server. However, the primary factor currently
#  looked at is the access method for retrieving/viewing email (HTTP vs IMAP vs POP).
#  A zimbraBatchedIndexingSize of 20 should be a good starting point for almost
#  any type of user access patterns, but when the usage for a particular COS is
#  strictly IMAP(S) and/or POP(S) the batch size can go even higher
#  (zimbraBatchedIndexingSize == 100, for example).
for cos in $(/opt/zimbra/bin/zmprov gac)
do
    if [ $(/opt/zimbra/bin/zmprov gc $cos zimbraBatchedIndexingSize | awk '/zimbraBatchedIndexingSize/ {print $2}') -lt 20 ]
    then
        su - zimbra -c "zmprov mc $cos zimbraBatchedIndexingSize 20"
    fi
done

# Set the correct permissions on the .ssh directory tree
su - zimbra -c 'chmod 700 .ssh; chmod 600 .ssh/authorized_keys'

# Make some postfix commands available
for C in mailq postfix postcat postconf postsuper sendmail
do
    if [ -x /opt/zimbra/postfix/sbin/$C ]
    then
        ln -fs /opt/zimbra/postfix/sbin/$C /usr/bin/$C
    elif [ -x /opt/zimbra/common/sbin/$C ]
    then
        ln -fs /opt/zimbra/common/sbin/$C /usr/bin/$C
    fi
done

# Correct the settings for public service access
# See https://wiki.zimbra.com/wiki/Enabling_Zimbra_Proxy_and_memcached
SERVER=$(/opt/zimbra/bin/zmhostname)
DOMAIN=$SERVER #${SERVER#[A-z]*.}

RESTART_ZIMBRA=0
/opt/zimbra/bin/zmprov gd $DOMAIN > /tmp/$$.$DOMAIN.zimbra

ZPS_PORT=$(awk '/zimbraPublicServicePort/ {print $NF}' /tmp/$$.$DOMAIN.zimbra)
if [ ! "T$ZPS_PORT" = 'T443' ]
then
    # Ensure that the public port is 443
    su - zimbra -c "/opt/zimbra/bin/zmprov md $DOMAIN zimbraPublicServicePort 443"
    RESTART_ZIMBRA=1
fi
ZPS_PROT=$(awk '/zimbraPublicServiceProtocol/ {print $NF}' /tmp/$$.$DOMAIN.zimbra)
if [ ! "T$ZPS_PROT" = 'Thttps' ]
then
    # Ensure that the public protocol is https
    su - zimbra -c "/opt/zimbra/bin/zmprov md $DOMAIN zimbraPublicServiceProtocol https"
    RESTART_ZIMBRA=1
fi
ZPS_HNAME=$(awk '/zimbraPublicServiceHostname/ {print $NF}' /tmp/$$.$DOMAIN.zimbra)
if [ ! "T$ZPS_HNAME" = "T$SERVER" ]
then
    # Ensure that the public hostname is this host
    su - zimbra -c "/opt/zimbra/bin/zmprov md $DOMAIN zimbraPublicServiceHostname $SERVER"
    RESTART_ZIMBRA=1
fi

# Adjust the default domain
if [ ! "T$(/opt/zimbra/bin/zmlocalconfig av_notify_domain | awk '{print $NF}')" = "T$DOMAIN" ]
then
    su - zimbra -c "/opt/zimbra/bin/zmlocalconfig -e av_notify_domain=$DOMAIN"
    RESTART_ZIMBRA=1
fi
if [ ! "T$(/opt/zimbra/bin/zmprov gs $SERVER zimbraMtaMyOrigin | awk '/^zimbraMtaMyOrigin/{print $NF}')" = "T$DOMAIN" ]
then
    su - zimbra -c "/opt/zimbra/bin/zmprov ms $SERVER zimbraMtaMyOrigin $DOMAIN"
    RESTART_ZIMBRA=1
fi

# Enable HTTP Strict Transport Security (HSTS) and disable search indexing of your server by web crawlers
su - zimbra -c 'zmprov gcf zimbraResponseHeader' > /tmp/$$
if [ -z "$(grep '^zimbraResponseHeader: Strict-Transport-Security' /tmp/$$)" ]
then
    cat << EOT >> /tmp/$$.newResponseHeaders
mcf +zimbraResponseHeader "Strict-Transport-Security: max-age=31536000; includeSubDomains"
EOT
    RESTART_ZIMBRA=1
fi
if [ -z "$(grep '^zimbraResponseHeader: X-XSS-Protection' /tmp/$$)" ]
then
    cat << EOT >> /tmp/$$.newResponseHeaders
mcf +zimbraResponseHeader "X-XSS-Protection: 1; mode=block"
EOT
    RESTART_ZIMBRA=1
fi
if [ -z "$(grep '^zimbraResponseHeader: X-Content-Type-Options' /tmp/$$)" ]
then
    cat << EOT >> /tmp/$$.newResponseHeaders
mcf +zimbraResponseHeader "X-Content-Type-Options: nosniff"
EOT
    RESTART_ZIMBRA=1
fi
if [ -z "$(grep '^zimbraResponseHeader: X-Robots-Tag' /tmp/$$)" ]
then
    cat << EOT >> /tmp/$$.newResponseHeaders
mcf +zimbraResponseHeader "X-Robots-Tag: noindex"
EOT
    RESTART_ZIMBRA=1
fi
if [[ ! "$(su - zimbra -c 'zmprov gcf zimbraMailKeepOutWebCrawlers')" =~ TRUE ]]
then
    cat << EOT >> /tmp/$$.newResponseHeaders
mcf zimbraMailKeepOutWebCrawlers TRUE
EOT
    RESTART_ZIMBRA=1
fi
if [ -s /tmp/$$.newResponseHeaders ]
then
    chmod 644 /tmp/$$.newResponseHeaders
    su - zimbra -c "zmprov < /tmp/$$.newResponseHeaders"
fi

[ $RESTART_ZIMBRA -ne 0 ] && su - zimbra -c 'zmmailboxdctl restart'
rm -rf /tmp/$$*

# Get the Zimbra to Syslog script
if [ -f /opt/zimbra/log/mailbox.log ]
then
    wget -q -O /usr/local/sbin/ZimbraLog2Syslog.pl \
      https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/Zimbra/ZimbraLog2Syslog.pl
    chmod 744 /usr/local/sbin/ZimbraLog2Syslog.pl
fi

# Add a random image from unsplash.com to Login screen
[ -z "$(grep -q 'source\.unsplash\.com' /opt/zimbra/jetty/webapps/zimbra/public/login.jsp)" ] && patch /opt/zimbra/jetty/webapps/zimbra/public/login.jsp << EOT 
--- /opt/zimbra/jetty/webapps/zimbra/public/login.jsp.nosplash  2021-07-09 15:07:14.886242776 -0400
+++ /opt/zimbra/jetty/webapps/zimbra/public/login.jsp   2021-07-09 15:07:36.614061196 -0400
@@ -504,6 +504,15 @@
 
 </head>
 <c:set value="/img" var="iconPath" scope="request"/>
+<style>
+     .LoginScreen {
+         background-color    : #777 !important;
+         background-image    : url('https://source.unsplash.com/random/featured/?nature') !important;
+         background-position : center;
+         background-repeat   : no-repeat;
+         background-size     : cover;
+     }
+     </style>
 <body onload="onLoad();">
 
        <div class="LoginScreen">
EOT

# Do not execute anything below - needs to be retested
exit 0
#========================================================================

# Install all available zimlets and show the installed ones
cd /opt/zimbra
for D in zimlets-*
do
        [ -d $D ] || continue
        [ "T$D" = 'Tzimlets-admin-extra' ] && continue
        cd /opt/zimbra/$D
        for F in *
        do
                [ -d $F ] && continue
                echo "Deploying zimlet ${D}/$F"
                su - zimbra -c "zmzimletctl deploy $(pwd)/$F"
        done
        cd /opt/zimbra
done
su - zimbra -c 'zmzimletctl listZimlets'


# Set the origin for email correctly
su - zimbra -c "zmprov ms $SERVER zimbraMtaMyOrigin $DOMAIN"
su - zimbra -c "zmprov md $DOMAIN zimbraMailCatchAllAddress @xwa zimbraMailCatchAllCanonicalAddress @$DOMAIN"

# Set the correct ports
su - zimbra -c "zmprov ms $SERVER -zimbraServiceEnabled imapproxy;zmprov ms $SERVER zimbraPop3BindPort 110 zimbraImapBindPort 143 zimbraPop3SSLBindPort 995 zimbraImapSSLBindPort 993 zimbraPop3ProxyBindPort 111 zimbraImapProxyBindPort 144 zimbraPop3SSLProxyBindPort 996 zimbraImapSSLProxyBindPort 994;zmcontrol stop; zmcontrol start"
# (Re)create SSL keys
#su - zimbra -c "cd /opt/zimbra/bin/;./zmsshkeygen;./zmupdateauthkeys;cd; echo '' > .ssh/authorized_keys; cat .ssh/zimbra_identity.pub > .ssh/authorized_keys; chmod 700 .ssh; chmod 600 .ssh/authorized_keys"

# Create new domain for the domain name of the host if necessary
su - zimbra -c "zmprov gd $DOMAIN" &> /dev/null
if [ $? -ne 0 ]
then
        read -p "Authentication for domain '$DOMAIN' [LOCAL|ad|ldap] ? " AUTH
        case "${AUTH^^}" in
        AD)     AUTH='ad'
                echo 'Setup the correct active directory parameters via the GUI'
                ;;
        LDAP)   AUTH='ldap'
                echo 'Setup the correct LDAP parameters via the GUI'
                ;;
        *)      AUTH='zimbra'
                ;;
        esac
        # Domain does not yet exist, create it with the chosen authentication
        su - zimbra -c "zmprov cd $DOMAIN zimbraAuthMech $AUTH"
fi

# Rename the admin account if necessary
su - zimbra -c "zmprov ga admin@$DOMAIN" &> /dev/null
if [ $? -ne 0 ]
then
        # Rename the account
        su - zimbra -c "zmprov ra admin@$SERVER admin@$DOMAIN"
fi

# Some optimizations as per https://wiki.zimbra.com/wiki/Performance_Tuning_Guidelines_for_Large_Deployments


# On a 8GB system, set Java heap size percent to 30 and mysql innodb buffer pool to 25% of system memory:
RAM=$(awk '/^MemTotal/ {print $2}' /proc/meminfo)
if [ $RAM -ge 8388608 ]
then

    # Allow for more threads for POP3 and IMAP
    POP3_THREADS=$(zmprov gs $SERVER zimbraPop3NumThreads | awk '/zimbraPop3NumThreads/ {print $NF}')
    if [ $POP3_THREADS -lt 300 ]
    then
        su - zimbra -c "zmprov ms $SERVER zimbraPop3NumThreads 300"
        RESTART_ZIMBRA=1
    fi
    IMAP_THREADS=$(zmprov gs $SERVER zimbraImapNumThreads | awk '/zimbraImapNumThreads/ {print $NF}')
    if [ $IMAP_THREADS -lt 500 ]
    then
        su - zimbra -c "zmprov ms $SERVER zimbraImapNumThreads 500"
        RESTART_ZIMBRA=1
    fi

    JHP=$(zmlocalconfig mailboxd_java_heap_size | awk '/mailboxd_java_heap_size/ {print $NF}')
    RAM25P=$(($RAM * 25 / 100))
    if [ $JHP -ne $(($RAM25P / 1024)) ]
    then
        su - zimbra -c 'zmlocalconfig -e mailboxd_java_heap_size='$(($RAM25P / 1024))
        RESTART_ZIMBRA=1
    fi
    JHP=$(zmlocalconfig mailboxd_java_heap_new_size_percent | awk '/mailboxd_java_heap_new_size_percent/ {print $NF}')
    if [ $JHP -ne 30 ]
    then
        su - zimbra -c 'zmlocalconfig -e mailboxd_java_heap_new_size_percent=30'
        RESTART_ZIMBRA=1
    fi
    MIBP=$(zmlocalconfig mysql_memory_percent | awk '/mysql_memory_percent/ {print $NF}')
    if [ $MIBP -ne 25 ]
    then
        su - zimbra -c 'zmlocalconfig -e mysql_memory_percent=25'
        RESTART_ZIMBRA=1
    fi
    CUR_INNOPOOL=$(awk '/^innodb_buffer_pool_size/ {print int($NF)}' /opt/zimbra/conf/my.cnf)
    RAMP25P_BYTES=$(($RAM25P * 1024))
    # Round the number up to the nearest GB value
    perl -e 'use Math::Round' &> /dev/null
    [ $? -eq 0 ] && RAM25P_BYTES=$(perl -e 'use Math::Round qw(nhimult);print nhimult(1073741824,$ARGV[0]),"\n"' $RAM25P_BYTES )
    if [ $CUR_INNOPOOL -ne $RAM25P_BYTES ]
    then
        sed -i "s/innodb_buffer_pool_size.*/innodb_buffer_pool_size = $RAM25P_BYTES/" /opt/zimbra/conf/my.cnf
        RESTART_ZIMBRA=1
    fi

    # See http://www.sfu.ca/~hillman/zimbra-hied-admins/msg00235.html
    CUR_THREADS=$(awk '/^thread_cache_size/ {print $NF}' /opt/zimbra/conf/my.cnf)
    if [ $CUR_THREADS -lt 150 ]
    then
        sed -i "s/thread_cache_size.*/thread_cache_size = 150/" /opt/zimbra/conf/my.cnf
        RESTART_ZIMBRA=1
    fi
    CUR_MAXCONN=$(awk '/^max_connections/ {print $NF}' /opt/zimbra/conf/my.cnf)
    if [ $CUR_MAXCONN -lt 220 ]
    then
        sed -i "s/max_connections.*/max_connections = 220/" /opt/zimbra/conf/my.cnf
        RESTART_ZIMBRA=1
    fi
                                                                                                                        
    # The default heap size reserved for classes and code is too small for the Zimbra application
    MJO_NOW=$(zmlocalconfig mailboxd_java_options | sed 's/mailboxd_java_options = //')
    MJO_OPTIM='-server -Djava.awt.headless=true -XX:+UseConcMarkSweepGC -XX:NewRatio=2 -XX:PermSize=196m -XX:MaxPermSize=350m -XX:SoftRefLRUPolicyMSPerMB=1 -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -XX:+PrintGCApplicationStoppedTime -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/opt/zimbra/log'
    if [ "T$MJO_NOW" != "T$MJO_OPTIM" ]
    then
        su - zimbra -c 'zmlocalconfig -e mailboxd_java_options='"'$MJO_OPTIM'"
        RESTART_ZIMBRA=1
    fi

    read -p 'Single server install [Y/n] ? ' YN
    [ -z "$YN" ] && YN='Y'
    if [ "T${YN^^}" = 'TY' ]
    then
        IPC_SSL=$(zmlocalconfig zimbra_require_interprocess_security | awk '/zimbra_require_interprocess_security/ {print $NF}')
        if [ $IPC_SSL -ne 0 ]
        then
            su - zimbra -c 'zmlocalconfig -e zimbra_require_interprocess_security=0'
            RESTART_ZIMBRA=1
        fi
        LDAP_SSL=$(zmlocalconfig ldap_starttls_supported | awk '/ldap_starttls_supported/ {print $NF}')
        if [ $LDAP_SSL -ne 0 ]
        then
            su - zimbra -c 'zmlocalconfig -e ldap_starttls_supported=0'
            RESTART_ZIMBRA=1
        fi
    fi
                                                        
    # If more than 100 domains are configured, you should adjust this to the lower of the number of domains you have configured and 30,000.
    # As per https://wiki.zimbra.com/wiki/OpenLDAP_Performance_Tuning_8.0#Mailbox_store_tuning_with_LDAP
    NOD=$(zmprov -l gad | wc -l)
    if [ $NOD -gt 100 ]
    then
        if [ $NOD -gt 30000 ]
        then
            # Restrict to max. 30,000
            NOD=30000
        else
            # Build in a small buffer
            NOD=$(($NOD + 10))
        fi
        LDAP_NOD=$(zmlocalconfig ldap_cache_domain_maxsize | awk '/ldap_cache_domain_maxsize/ {print $NF}')
        if [ $LDAP_NOD -lt $NOD ]
        then
            su - zimbra -c 'zmlocalconfig -e ldap_cache_domain_maxsize='$NOD
            RESTART_ZIMBRA=1
        fi
    fi
    [ $RESTART_ZIMBRA -ne 0 ] && su - zimbra -c 'zmcontrol restart'
fi

# We are done
exit 0
