#!/bin/bash
# Version 20231228-110948 checked into repository
################################################################
# (c) Copyright 2020 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/osticket.postinstall.sh

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=${0##*/}

# This needs to run as "root"
if [ ! "T$(/usr/bin/id -u)" = 'T0' ]
then
    echo "$PROG needs to run as 'root'"
    exit 1
fi

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            

#--------------------------------------------------------------------
# This host and domain
THISHOST=$(hostname)
[[ $THISHOST == *.* ]] || THISHOST=$(hostname -f)
THISDOMAIN=${THISHOST#*.}

# Get possible program options
UPDATE=0
while getopts u OPTION
do
    case ${OPTION} in
    u)  UPDATE=1
        ;;
    *)  echo "Usage: $0 [-u]"
        ;;
    esac
done
shift $((OPTIND - 1))

# Based on https://computingforgeeks.com/how-to-install-osticket-on-ubuntu-linux/
#  and https://www.howtoforge.com/how-to-install-osticket-on-ubuntu-1804/

# Setup local bashrc for "root"
cat << EOT > /root/.bashrc
# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
[ -z "\$PS1" ] && return

# don't put duplicate lines in the history. See bash(1) for more options
# ... or force ignoredups and ignorespace
HISTCONTROL=ignoreboth
HISTTIMEFORMAT="%F %T "

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "\$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "\$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=\$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "\$TERM" in
    xterm-color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "\$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
        # We have color support; assume it's compliant with Ecma-48
        # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
        # a case would tend to support setf rather than setaf.)
        color_prompt=yes
    else
        color_prompt=
    fi
fi

if [ "\$color_prompt" = yes ]; then
    PS1='\${debian_chroot:+(\$debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\\$ '
else
    PS1='\${debian_chroot:+(\$debian_chroot)}\u@\h:\w\\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "\$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;\${debian_chroot:+(\$debian_chroot)}\u@\h: \w\a\]\$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "\$(dircolors -b ~/.dircolors)" || eval "\$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
#if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
#    . /etc/bash_completion
#fi
EOT

# Setup aliases for "root"
cat << EOT > /root/.bash_aliases
# My own mysql connection
alias Mysql='mysql --defaults-file=/etc/mysql/debian.cnf'
alias Mysqlcheck='mysqlcheck --defaults-file=/etc/mysql/debian.cnf'
alias Mysqlrepair='mysqlrepair --defaults-file=/etc/mysql/debian.cnf'
alias Mysqladmin='mysqladmin --defaults-file=/etc/mysql/debian.cnf'
alias Mysqldump='mysqldump --defaults-file=/etc/mysql/debian.cnf'
EOT

# Adjust the firewall setting to allow web access
sed -i 's/server4 "ssh"/server "ssh http"/' /etc/firehol/firehol.conf

if [ $UPDATE -eq 0 ]
then
    # Activate repositories for newer versions
    UV=$(lsb_release -cs)
    [ -s /etc/apt/sources.list.d/ondrej-ubuntu-php-bionic.list ] || add-apt-repository -y ppa:ondrej/php
    [ -s /etc/apt/sources.list.d/ondrej-ubuntu-nginx-bionic.list ] || add-apt-repository -y ppa:ondrej/nginx
    [ -z "$(grep '^deb.*mariadb' /etc/apt/sources.list)" ] && add-apt-repository -y "deb http://mirror.its.dal.ca/mariadb/repo/10.4/ubuntu $UV main"

    apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0x14AA40EC0831756756D7F66C4F4EA0AAE5267A6C
    apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8

    apt-get update

    # Install mariadb-server if necessary
    [ -x /usr/sbin/mysqld ] || apt-get install mariadb-server

    # Setup database for "osticket"
    if [ -s /var/tmp/osticket.mysql ]
    then
        source /var/tmp/osticket.mysql
    else
        mysql_secure_installation
        OT_DB=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 12)
        OT_DBU=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 12)
        OT_DBPW=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 20)

        # Save the database parameters
        [ -z "$(grep '^OT_DBPW' /var/tmp/osticket.mysql)" ] && cat << EOT > /var/tmp/osticket.mysql
OT_DB=$OT_DB
OT_DBU=$OT_DBU
OT_DBPW=$OT_DBPW
EOT
        chmod 600 /var/tmp/osticket.mysql
    fi
    mysql --defaults-file=/etc/mysql/debian.cnf -v << EOT
CREATE DATABASE IF NOT EXISTS $OT_DB;
CREATE USER IF NOT EXISTS '$OT_DBU'@'localhost' IDENTIFIED BY '$OT_DBPW';
GRANT ALL PRIVILEGES ON $OT_DB.* TO ${OT_DBU}@localhost;
FLUSH PRIVILEGES;
EOT

    # Install php
    [ -x /usr/bin/php ] || apt-get install php php-mysql php-cgi php-fpm php-cli php-curl php-gd php-imap php-mbstring php-xml-util php-intl php-apcu php-common php-gettext php-bcmath php-zip
    apt-get purge apache2
    for F in /etc/php/*/fpm/php.ini
    do
        sed -i 's@^;cgi.fix_pathinfo=1@;cgi.fix_pathinfo=1\ncgi.fix_pathinfo=1@;
                s@^;date.timezone =@;date.timezone =\ndate.timezone = America/New_York@;
                s@^upload_max_filesize.*@upload_max_filesize = 2G@' $F
    done

    # Install nginx server
    [ -x /usr/sbin/nginx ] ||  apt-get install nginx
    systemctl enable nginx
    systemctl start nginx
    FPM_VERSION=$(dpkg -l | awk '/php7.*fpm/{print $2}')
    systemctl enable $FPM_VERSION
    systemctl restart $FPM_VERSION
fi

# Install OSTicket software itself
# Careful: Certain versions require specific PHP version to be installed
# osTicket 1.15 works with PHP 7.2-7.4
# osTicket 1.16 works with PHP 8
# You might want to download the zip file manually from https://osticket.com/download/
OST_URL=$(wget -qO - https://api.github.com/repos/osTicket/osTicket/releases/latest | grep browser_download_url | grep "browser_download_url" | cut -d '"' -f 4)
OST_ZIP=${OST_URL##*/}
if [ -s /usr/local/src/$OST_ZIP ]
then
    OST_VERS=$(sed 's/^.*v//;s/.zip//' <<< $OST_ZIP)
    if [ -z "$(grep v$OST_VERS /var/www/osTicket/upload/bootstrap.php)" ]
    then
       # Different version is installed
       UPDATE=2
    fi
else
    # This version is not yet downloaded
    UPDATE=2
fi

if [ $UPDATE -eq 2 ]
then
    wget -qO /usr/local/src/$OST_ZIP $OST_URL
    unzip -q /usr/local/src/$OST_ZIP -d /tmp/osTicket
    mv -f /tmp/osTicket /var/www
    mv /var/www/osTicket/upload/include/ost-sampleconfig.php /var/www/osTicket/upload/include/ost-config.php
    chown -R www-data:www-data /var/www/osTicket
    # This file is only relevant to IIS
    # See https://github.com/osTicket/osTicket/issues/5463
    find /var/www/osTicket -type f -name "web.config" -print0 | xargs -0rn 40 chown root:
    cat << EOT > /etc/nginx/sites-available/osticket.conf
# See https://www.nginx.com/resources/wiki/start/topics/recipes/osticket/
server {
        listen 80;
        server_name servicedesk.digitaltowpath.org;
        root /var/www/osTicket/upload;

        access_log /var/log/nginx/access.log;
        error_log /var/log/nginx/error.log;
        index index.php index.html index.htm;

        # Enable gzip
        gzip on;
        gzip_min_length 1000;
        gzip_types text/plain application/x-javascript text/xml text/css application/xml;

        keepalive_timeout 70;

        set \$path_info "";

        location ~ /include {
            deny all;
            return 403;
        }

        if (\$request_uri ~ "^/api(/[^\?]+)") {
            set \$path_info \$1;
        }

        location ~ ^/api/(?:tickets|tasks).*\$ {
            try_files \$uri \$uri/ /api/http.php?\$query_string;
        }

        if (\$request_uri ~ "^/scp/.*\.php(/[^\?]+)") {
            set \$path_info \$1;
        }

        if (\$request_uri ~ "^/.*\.php(/[^\?]+)") {
            set \$path_info \$1;
        }

        location ~ ^/scp/ajax.php/.*\$ {
            try_files \$uri \$uri/ /scp/ajax.php?\$query_string;
        }

        location ~ ^/ajax.php/.*\$ {
            try_files \$uri \$uri/ /ajax.php?\$query_string;
        }

        location / {
            try_files \$uri \$uri/ index.php;
        }

        location ~ \.php\$ {
            fastcgi_param  SCRIPT_FILENAME  \$document_root\$fastcgi_script_name;
            include        fastcgi_params;
            fastcgi_param  PATH_INFO        \$path_info;
            fastcgi_pass unix:/run/php/php7.4-fpm.sock;
        }
}
EOT
    ln -fs /etc/nginx/sites-available/osticket.conf /etc/nginx/sites-enabled/
    nginx -t && systemctl restart nginx
    chmod 0644 /var/www/osTicket/upload/include/ost-config.php

    # Go through the original setup
    source /var/tmp/osticket.mysql
    cat << EOT

Point your browser to http://$THISHOST and run through
the initial configuration for the help desk system.

Don't forget to remove the setup directory afterwards:
rm -rf /var/www/osTicket/upload/setup 

Use these credentials for the MySQL database:
Database name:     $OT_DB
Database user:     $OT_DBU
Database password: $OT_DBPW
EOT
fi

# Create backup script
cat << EOT > /usr/local/sbin/osticket-backup.sh
#!/bin/bash
# Based on https://raw.githubusercontent.com/wouterVE/OSticket-Backup-Restore/master/ostbackup.sh
NOW=\$(date +"%Y.%m.%d_%H:%M:%S")
# TODO: The directory where you store the OSticket backups
BKP_DIR="/var/tmp/osticket-backups"
# TODO: The directory of your OSticket installation (this is a directory under your web root)
BKP_FILES="/var/www/osTicket"
# TODO: If you have a separate attachment directory uncomment this line as well as the actual section to restore this directory below
#osticketDataDir="/var/OSattachments" 
source /var/tmp/osticket.mysql
# TODO: The maximum number of backups to keep (when set to 0, all backups are kept)
MAX_BKP=30

# Function for error messages
errorecho() { cat <<< "\$@" 1>&2; }

#
# Check for root
#
if [ "T\$(/usr/bin/id -u)" = 'T0' ]
then
    errorecho "ERROR: This script has to be run as root!"
    exit 1
fi

#
# Check if backup dir already exists
#
mkdir -p "\${BKP_DIR}"

#
# Backup file directory
#
date "+%F %T Creating backup of OSticket file directory" >> \${BKP_DIR}/osticket-backup.log
tar -cpzf "\${BKP_DIR}/OST_FILES.\${NOW}" -C "\${BKP_FILES}" .
date "+%F %T Done" >> \${BKP_DIR}/osticket-backup.log

#
# Backup attachment directory (if specified)
#
if [ ! -z "\${osticketDataDir}" ]
then
    date "+%F %T Creating backup of OSticket data directory" >> \${BKP_DIR}/osticket-backup.log
    tar -cpzf "\${BKP_DIR}/OST_DATA.\${NOW}"  -C "\${osticketDataDir}" .
    date "+%F %T Done" >> \${BKP_DIR}/osticket-backup.log
fi

#
# Backup DB
#
date "+%F %T Backup Database" >> \${BKP_DIR}/osticket-backup.log
mysqldump --defaults-file=/etc/mysql/debian.cnf --skip-opt --single-transaction -B "\${OT_DB}" "mysql" > "\${BKP_DIR}/OST_DB.\$NOW"
date "+%F %T Done" >> \${BKP_DIR}/osticket-backup.log

#
# Delete old backups
#
[ \$MAX_BKP -gt 0 ] && find \${BKP_DIR} -name "OST_*" -mtime +\${MAX_BKP} -ls

date "+%F %T Backups are in \${BKP_DIR}" >> \${BKP_DIR}/osticket-backup.log
ls -1 \${BKP_DIR}/*\$NOW  >> \${BKP_DIR}/osticket-backup.log
echo >> \${BKP_DIR}/osticket-backup.log

# Keep the log file at a reasonable size
CURLINES=\$(sed -n '\$=' \${BKP_DIR}/osticket-backup.log
if [ \$CURLINES -gt 10000 ]
then
    LINES2DEL=\$((\$CURLINES - 10000))
    sed -i -e "1,\${LINES2DEL}d" \${BKP_DIR}/osticket-backup.log
fi

EOT
chmod 744 /usr/local/sbin/osticket-backup.sh

# Create cron job for email fetching
[ -s /etc/cron.d/osticket ] || cat << EOT > /etc/cron.d/osticket
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/sbin:/sbin:/usr/local/bin:/usr/bin:/bin:/snap/bin
#MAILTO=<FILL IN THE CORRECT EMAIL ADDRESS>
#===============================================================
5 * * * * www-data /usr/bin/php /var/www/osTicket/upload/api/cron.php
#3 0 * * * root /usr/local/sbin/osticket-backup.sh
EOT

# We are done
exit 0
