#!/bin/bash
# Version 20240320-104527 checked into repository
################################################################
# (c) Copyright 2021 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/SysMon-Graphs.sh
# Run once a day, eg. around 3:00am

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

PROG=${0##*/}
ionice -c2 -n7 -p $$

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi
logger -p daemon.info -t ${PROG}[$$] -- Starting

# Make sure we remove the lock file at exit
TDIR=$(mktemp -d)
mkdir -p $TDIR
trap "rm -rf $LOCKFILE /tmp/$$* $TDIR" EXIT
echo "$$" > $LOCKFILE

# Display information about any detected errors
failure() {
    local exitcode=$1
    local lineno=$2
    local msg=$3
   
    # Ignore some false positives
    [[ $msg =~ diff ]] && return
    [[ $msg =~ wget ]] && return
    [[ $msg =~ EMAIL_SERVER= ]] && return
    echo "Detected error $exitcode in $PROG at line $lineno: $msg"
}
trap 'failure $? ${LINENO} "$BASH_COMMAND"' ERR

# Define the email address for the resulting image
EMAIL_TO=''
EMAIL_SERVER='localhost'

# Use "nocache" for find if possible
NOCACHE=''
dpkg-query -S nocache &> /dev/null && NOCACHE='nocache'

#--------------------------------------------------------------------
# Can we use "curl"?
USER_AGENT=${PROG}'/'$(awk '/^# Version 2/{print $3}' $(command -v SysMon-Graphs.sh))

HAVE_CURL=0
if [ -x /usr/bin/curl ]
then
    HAVE_CURL=1
    CURL_OPTS='-skL -m 60 --retry-connrefused --retry 3 --user-agent '"$USER_AGENT"
    [[ "$(curl --help)" =~ compressed ]] && CURL_OPTS="$CURL_OPTS --compressed"
fi
WGET_OPTS='-q --no-check-certificate -t 3 -T 60 --user-agent='"$USER_AGENT"
[[ "$(wget --help)" =~ compression= ]] && WGET_OPTS="$WGET_OPTS --compression=auto"

#--------------------------------------------------------------------
# The main loop
while true
do
    THISHOST=$(hostname 2> /dev/null)
    if [ -n "$THISHOST" ]
    then
        [[ $THISHOST == *.* ]] || THISHOST=$(hostname -f 2> /dev/null)
        [ -n "$THISHOST" ]  && break
    fi
    sleep 1
done
THISDOMAIN=${THISHOST#*.}

#--------------------------------------------------------------------
# Restart SysMon.pl if necessary
pidof -o %PPID SysMon.pl > /tmp/$$.SPIDS 2> /dev/null
if [ -s /tmp/$$.SPIDS ]
then
    RESTART_SYSMON=0
    SPID=''
    if [ $(awk '{ print NF }' /tmp/$$.SPIDS) -gt 1 ]
    then
        # We have more than one process running - kill them all
        kill $(< /tmp/$$.SPIDS) 2> /dev/null
        RESTART_SYSMON=1
    else
        # We have just one running - restart it after 24 hours
        SPID=$(< /tmp/$$.SPIDS)
        SDATE=$(date --date="$(ps -p $SPID -o lstart=)" '+%s')
        [ $((EPOCHSECONDS - SDATE)) -gt 86400 ] && RESTART_SYSMON=2
    fi
    if [ $RESTART_SYSMON -ne 0 ]
    then        
        SPID=$(pidof -s -o %PPID SysMon.pl)
        [ -n "$SPID" ] && kill $SPID &> /dev/null
        /bin/bash /usr/local/sbin/SysMon.start.sh
    fi
fi

#--------------------------------------------------------------------
# Install any necessary programs
export DEBIAN_FRONTEND=noninteractive
for P in ploticus sendemail gawk zip #webfs
do
    logger -p daemon.info -t ${PROG}[$$] -- Checking for presence of "$P"
    command -v "$P" &> /dev/null || dpkg-query -S $P &>/dev/null || apt-get -yq install $P
    command -v "$P" &> /dev/null
    if [ $? -ne 0 ]
    then
        if [ "T$P" = 'Tploticus' ]
        then
            if [ $HAVE_CURL -eq 1 ]
            then
                curl $CURL_OPTS -o /tmp/pl.tar.gz \
                  https://sourceforge.net/projects/ploticus/files/ploticus/2.42/ploticus242_linuxbin64.tar.gz/download
            else
               wget $WGET_OPTS -O /tmp/pl.tar.gz \
                  https://sourceforge.net/projects/ploticus/files/ploticus/2.42/ploticus242_linuxbin64.tar.gz/download
            fi
            if [ -s /tmp/pl.tar.gz ]
            then
                CWD=$(pwd)
                cd /tmp || exit
                rm -rf plo*
                $NOCACHE tar -xzf pl.tar.gz
                cp plo*/bin/pl /usr/bin/ploticus
                cd $CWD || exit
            fi
        fi
    fi
    command -v "$P" &> /dev/null
    if [ $? -ne 0 ]
    then
        echo "ERROR: '$P' is not installed locally"
        logger -p daemon.info -t ${PROG}[$$] -- "$P" is not installed locally
        exit 1
    fi
done

#--------------------------------------------------------------------
# Get possible program options
while getopts chm:t: OPTION
do
    case ${OPTION} in
    c)  # Run this script as a cronjob
        logger -p daemon.info -t ${PROG}[$$] -- Running as cron job
        if [ $HAVE_CURL -eq 1 ]
        then
            curl $CURL_OPTS -o /tmp/SysMon-Graphs.sh \
              https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/SysMon-Graphs.sh
        else
           wget $WGET_OPTS -O /tmp/SysMon-Graphs.sh \
             https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/SysMon-Graphs.sh
        fi
        if [ -s /usr/local/sbin/SysMon-Graphs.sh ]
        then
            diff /tmp/SysMon-Graphs.sh /usr/local/sbin/SysMon-Graphs.sh > /dev/null 2>&1
            [ $? -ne 0 ] && cat /tmp/SysMon-Graphs.sh > /usr/local/sbin/SysMon-Graphs.sh
        fi
        rm -f /tmp/SysMon-Graphs.sh
        if [ -x /usr/local/sbin/SysMon-Graphs.sh ]
        then
            grep -F 'SysMon.pl' /etc/rc.local > /tmp/$$ 2> /dev/null
            if [ -s /tmp/$$ ]
            then
                EMAIL_TO=$(grep -Po '\-a [\S]*' /tmp/$$ 2> /dev/null)
                [ -n "$EMAIL_TO" ] && EMAIL_TO=${EMAIL_TO//-a/-t}
                EMAIL_SERVER=$(grep -Po '\-m [\S]*' /tmp/$$ 2> /dev/null)
                rm -f $LOCKFILE
                bash -$- /usr/local/sbin/SysMon-Graphs.sh $EMAIL_TO $EMAIL_SERVER
            fi
        fi
        exit 0
        ;;
    m)  logger -p daemon.info -t ${PROG}[$$] -- Setting email server to "$OPTARG"
        EMAIL_SERVER="$OPTARG"
        ;;
    t)  logger -p daemon.info -t ${PROG}[$$] -- Setting email recipient to "$OPTARG"
        EMAIL_TO="$OPTARG"
        ;;
    *)  logger -p daemon.info -t ${PROG}[$$] -- Unknown option "$OPTION"
        cat << EOT
Usage: $PROG [options] [day]
       -c          Run this script as a cron job
       -t email    Send report to email address [default=don't send]
       -m IP/host  Specify email server to use [default=$EMAIL_SERVER]

       If you specify a day, stats will be created for that day.
        By default, stats are created for yesterday ($(date "+%Y-%m-%d" -d yesterday))
EOT
        exit 0
        ;;
    esac
done
shift $((OPTIND - 1))

# Debug or not?
DEBUG=''
ZIPQUIET='-q'
if [[ $- ==  *x* ]]
then
    DEBUG='-debug'
    ZIPQUIET=''
fi

# Number of CPU cores in the system
CPUs=$(grep -c '^proc' /proc/cpuinfo 2> /dev/null)
logger -p daemon.info -t ${PROG}[$$] -- Found $CPUs CPU cores

#--------------------------------------------------------------------
# Truncate csv file created by SysMon.pl to 50000 lines
for F in /var/tmp/*csv
do
    # Ignore some file(s)
    [[ $F =~ SpeedtestResults ]] && continue

    # Only process existing files
    [ -s $F ] || continue

    # Leave file with less than 50000 lines alone
    CURLINES=$(sed -n '$=' $F)
    [ $CURLINES -lt 50000 ] && continue

    # Truncate the file to 50000 lines
    LINES2DEL=$((CURLINES - 50000))
    sed -i -e "1,${LINES2DEL}d" $F
done

#--------------------------------------------------------------------
# Define the day and hour to collect stats for
if [ -z "$1" ]
then
    # Get the date and hour for 1 hour ago
    STATS_DAY=$(date "+%Y-%m-%d" -d yesterday)
else
    STATS_DAY=$(date "+%Y-%m-%d" -d "$1")
fi
ZIPFILE="/var/tmp/SysMon-Graphs-$STATS_DAY.zip"
logger -p daemon.info -t ${PROG}[$$] -- Results will be in "$ZIPFILE"

#--------------------------------------------------------------------
# Get the right data for the day
# For details on the values see /usr/local/sbin/SysMon.pl
echo > /tmp/SyslogSummary.txt
echo "Daily system monitoring summary on $THISHOST" >> /tmp/SyslogSummary.txt

# Get the list of possible virtual guests
if [ -x /usr/sbin/qm ]
then
    qm list | awk 'NR==1 {next};/[[:digit:]]*/{print $1" "$2}' > /tmp/$$.qms
fi

for F in CPU IO Memory $(grep -Eav '(^Inter|face|lo:)' /proc/net/dev 2> /dev/null | awk '{sub(/:/,"",$1);print $1}') $(awk '/ext[234]/{gsub(/\//,"#");print $1}' /proc/mounts) $(awk '!/Filename/{gsub(/\//," ");print $2}' /proc/swaps)
do
    # Check whether the csv file exists
    logger -p daemon.info -t ${PROG}[$$] -- Checking for $F data
    FNAME="/var/tmp/${F}.csv"
    [ -s $FNAME ] || continue
    logger -p daemon.info -t ${PROG}[$$] -- Processing "$FNAME"
    $NOCACHE grep -a "^${STATS_DAY}," $FNAME > $TDIR/${F}.csv 2> /dev/null

    case $F in
    #----------------------------------------------------------------
    CPU)
        # CPU load
        # Ex.: 2021-11-09,07:18:03,0.03
        [ -s $TDIR/${F}.csv ] || continue

        CPU_LOAD=$(awk -F, -v N=3 '{ sum += $N } END { if (NR > 0) print sum / NR }' $TDIR/${F}.csv)
        printf " Running process average (CPU usage) = %0.2f (of %d CPU cores)\n" $CPU_LOAD $CPUs >> /tmp/SyslogSummary.txt

        ploticus $DEBUG -png -o $TDIR/${F}.png -stdin << EOT
#proc page
#if @DEVICE in png,gif
   scale: 0.9
#endif

#proc getdata
file: $TDIR/${F}.csv
fieldnames: date time load
delim: comma

#proc areadef
   areaname: standard
   title: CPU load relative to number of CPU cores
   titledetails: size=14 align=C
   xscaletype: time hh:mm:ss
   xrange: 0:00 23:59
   yrange: 0 $((CPUs * 2))
   linebottom: yes
   lineside: yes

#proc xaxis
   stubs: inc 1 hour
   minorticinc: 30 minutes
   stubformat: hhA
   stubdetails: size=9
   grid: color=gray(0.9) width=0.1 style=2
   tics: yes

#proc yaxis:
   stubs: inc
   stubdetails: size=9
   stubformat: %0.0f
   grid: color=gray(0.9) width=0.1 style=2
   axisline: yes
   tics: yes
   gridskip: minmax
   nolimit: yes

#proc curvefit
  xfield: time
  yfield: load
  linedetails: color=purple width=.5
  legendlabel: CPU load
EOT
        [ -s $TDIR/${F}.png ] && (cd $TDIR && zip $ZIPQUIET "$ZIPFILE" ${F}.png)
        ;;

    #----------------------------------------------------------------
    IO)
        # I/O wait
        # Ex.: 2021-11-09,07:18:03,0.06
        [ -s $TDIR/${F}.csv ] || continue

        IO_WAIT=$(awk -F, -v N=3 '{ sum += $N } END { if (NR > 0) print sum / NR }' $TDIR/${F}.csv)
        printf " Blocked process average (I/O usage) = %0.2f (of %d CPU cores)\n" $IO_WAIT $CPUs >> /tmp/SyslogSummary.txt

        ploticus $DEBUG -png -o $TDIR/${F}.png -stdin << EOT
#proc page
#if @DEVICE in png,gif
   scale: 0.9
#endif

#proc getdata
file: $TDIR/${F}.csv
fieldnames: date time wait
delim: comma

#proc areadef
   areaname: standard
   title: I/O Wait relative to number of CPU cores
   titledetails: size=14 align=C
   xscaletype: time hh:mm:ss
   xrange: 0:00 23:59
   yrange: 0 $((CPUs * 2))
   linebottom: yes
   lineside: yes

#proc xaxis
   stubs: inc 1 hour
   stubformat: hh
   stubdetails: size=9
   grid: color=gray(0.9) width=0.1 style=2
   tics: yes

#proc yaxis:
   stubs: inc
   stubdetails: size=9
   stubformat: %0.0f
   grid: color=gray(0.9) width=0.1 style=2
   axisline: yes
   tics: yes
   gridskip: minmax
   nolimit: yes

#proc curvefit
  xfield: time
  yfield: wait
  linedetails: color=purple width=.5
  legendlabel: I/O Wait

#proc legend
  location: min+1 min-1
  textdetails: size=5
  format: across
  colortext: yes
EOT
        [ -s $TDIR/${F}.png ] && (cd $TDIR && zip $ZIPQUIET "$ZIPFILE" ${F}.png)
        ;;

    #----------------------------------------------------------------
    Memory)
        # Memory stats
        # Ex.: 2021-11-09,07:18:03,0.00
        [ -s $TDIR/${F}.csv ] || continue

        PAGING=$(awk -F, -v N=3 '{ sum += $N } END { if (NR > 0) print sum / NR }' $TDIR/${F}.csv)
        printf " Paged-in memory average (Mem usage) = %0.2f (of %d KB total)\n" $PAGING $(awk '/MemTotal/{print $2}' /proc/meminfo) >> /tmp/SyslogSummary.txt

        ploticus $DEBUG -png -o $TDIR/${F}.png -stdin << EOT
#proc page
#if @DEVICE in png,gif
   scale: 0.9
#endif

#proc getdata
file: $TDIR/${F}.csv
fieldnames: date time usage
delim: comma

#proc areadef
   areaname: standard
   title: Paging Activity over time
   titledetails: size=14 align=C
   xscaletype: time hh:mm:ss
   xrange: 0:00 23:59
   yautorange: datafield=usage
   linebottom: yes
   lineside: yes

#proc xaxis
   stubs: inc 1 hour
   stubformat: hh
   stubdetails: size=9
   grid: color=gray(0.9) width=0.1 style=2
   tics: yes

#proc yaxis:
   stubs: inc
   stubdetails: size=9
   stubformat: %3.0fKB
   grid: color=gray(0.9) width=0.1 style=2
   axisline: yes
   tics: yes
   gridskip: minmax
   nolimit: yes

#proc curvefit
  xfield: time
  yfield: usage
  linedetails: color=purple width=.5
  legendlabel: Paging activity

#proc legend
  location: min+1 min-1
  textdetails: size=5
  format: across
  colortext: yes
EOT
        [ -s $TDIR/${F}.png ] && (cd $TDIR && zip $ZIPQUIET "$ZIPFILE" ${F}.png)
        ;;

    #----------------------------------------------------------------
    \#dev*)
        # Disk stats
        # Ex.: 2021-11-09,07:15:03,34
        [ -s $TDIR/${F}.csv ] || continue

        echo >> /tmp/SyslogSummary.txt
        PU=$(awk -F, -v N=3 '{ sum += $N } END { if (NR > 0) print sum / NR }' $TDIR/${F}.csv)
        printf " Partition usage average for %s = %0.2f%%\n" ${F//#//} $PU >> /tmp/SyslogSummary.txt

        ploticus $DEBUG -png -o $TDIR/${F}.png -stdin << EOT
#proc page
#if @DEVICE in png,gif
   scale: 0.9
#endif

#proc getdata
file: $TDIR/${F}.csv
fieldnames: date time usage
delim: comma

#proc areadef
   areaname: standard
   title: Partition usage for ${F//#//} over time
   titledetails: size=14 align=C
   xscaletype: time hh:mm:ss
   xrange: 0:00 23:59
   yrange: 0 100
   linebottom: yes
   lineside: yes

#proc xaxis
   stubs: inc 1 hour
   stubformat: hh
   stubdetails: size=9
   grid: color=gray(0.9) width=0.1 style=2
   tics: yes

#proc yaxis:
   stubs: inc
   stubdetails: size=7
   grid: color=gray(0.9) width=0.1 style=2
   axisline: yes
   stubformat: %3.0f%%
   tics: yes

#proc curvefit
  xfield: time
  yfield: usage
  linedetails: color=purple width=.5
  legendlabel: Partition usage

#proc legend
  location: min+1 min-1
  textdetails: size=5
  format: across
  colortext: yes
EOT
        [ -s $TDIR/${F}.png ] && (cd $TDIR && zip $ZIPQUIET "$ZIPFILE" ${F}.png)

        # Handle Inode stats
        # Ex.: 2021-11-09,07:15:03,1
        FNAME="/var/tmp/Inodes_${F}.csv"
        [ -s $FNAME ] || continue
        $NOCACHE grep -a "^${STATS_DAY}," $FNAME > $TDIR/Inodes_${F}.csv 2> /dev/null
        [ -s $TDIR/Inodes_${F}.csv ] || continue

        IU=$(awk -F, -v N=3 '{ sum += $N } END { if (NR > 0) print sum / NR }' $TDIR/${F}.csv)
        printf " Inode usage average for %s = %0.2f%%\n" ${F//#//} $IU >> /tmp/SyslogSummary.txt

        ploticus $DEBUG -png -o $TDIR/Inodes_${F}.png -stdin << EOT
#proc page
#if @DEVICE in png,gif
   scale: 0.9
#endif

#proc getdata
file: $TDIR/Inodes_${F}.csv
fieldnames: date time usage
delim: comma

#proc areadef
   areaname: standard
   title: Inode usage for ${F//#//} over time
   titledetails: size=14 align=C
   xscaletype: time hh:mm:ss
   xrange: 0:00 23:59
   yrange: 0 100
   linebottom: yes
   lineside: yes

#proc xaxis
   stubs: inc 1 hour
   stubformat: hh
   stubdetails: size=9
   grid: color=gray(0.9) width=0.1 style=2
   tics: yes

#proc yaxis:
   stubs: inc
   stubdetails: size=7
   grid: color=gray(0.9) width=0.1 style=2
   axisline: yes
   stubformat: %3.0f%%
   tics: yes

#proc curvefit
  xfield: time
  yfield: usage
  linedetails: color=purple width=.5
  legendlabel: Inode usage

#proc legend
  location: min+1 min-1
  textdetails: size=5
  format: across
  colortext: yes
EOT
        [ -s $TDIR/Inodes_${F}.png ] && (cd $TDIR && zip $ZIPQUIET "$ZIPFILE" Inodes_${F}.png)

        # Handle partitions
        # Ex.: 2021-11-09,07:15:03,311890944,12267520
        FNAME="/var/tmp/${F##*#}.csv"
        [ -s $FNAME ] || continue
        $NOCACHE grep -a "^${STATS_DAY}," $FNAME 2> /dev/null | \
          awk -F, '

NR == 1 { 
 # We start with 0 deltas
 delta_read=0;prev_read=$3;
 delta_write=0;prev_write=$4;
 print $1","$2","delta_read","delta_write;
 next; }
NR > 1  {
 # Compute real deltas, but treat wraparound as 0 delta
 delta_read=(($3 > prev_read) ? (($3-prev_read)/1048576) : 0);prev_read=$3;
 delta_write=(($4 > prev_write) ? (($4-prev_write)/1048576) : 0);prev_write=$4;
 printf "%s,%s,%.2f,%.2f\n", $1,$2,delta_read,delta_write; }' > $TDIR/${F##*#}.csv

        echo >> /tmp/SyslogSummary.txt
        DR=$(awk -F, -v N=3 '{ sum += $N } END { if (NR > 0) print sum / NR }' $TDIR/${F}.csv)
        printf " Data reads average for %s = %0.2f\n" ${F//#//} $DR >> /tmp/SyslogSummary.txt
        DW=$(awk -F, -v N=4 '{ sum += $N } END { if (NR > 0) print sum / NR }' $TDIR/${F}.csv)
        printf " Data writes average for %s = %0.2f\n" ${F//#//} $DW >> /tmp/SyslogSummary.txt

        PART_NAME="${F//#//}"
        MP=$(grep "$PART_NAME" /proc/mounts | cut -d' ' -f2)
        if [ -n "$MP" ]
        then
            # Add the mount point to the disk partition
            PART_NAME="${F//#//} ($MP)"
        else
            # Get the mountpoint via major and minor device numbers
            DEV_NUM=$(ls -l /dev/${F//#//} 2> /dev/null | awk '{print int($5)":"$6}')
            if [ -n "$DEV_NUM" ]
            then
                MP=$(lsblk -l | awk "/${DEV_NUM}"'/{print $1}')
                if [ "T${MP:0:1}" = 'T/' ]
                then
                    # We have a valid mount point
                    PART_NAME="${F//#//} ($MP)"
                else
                    # Get the real mount point
                    MP=$(grep "/dev/mapper/$MP" /proc/mounts | cut -d' ' -f2)
                    if [ -n "$RMP" ]
                    then
                        # Use the real mount point
                        PART_NAME="${F//#//} ($RMP)"
                    else
                        # Use the logical volume name
                        PART_NAME="${F//#//} ($MP)"
                    fi
                fi
            fi
        fi

        ploticus $DEBUG -png -o $TDIR/${F##*#}.png -stdin << EOT
#proc page
#if @DEVICE in png,gif
   scale: 0.9
#endif

#proc getdata
file: $TDIR/${F##*#}.csv
fieldnames: date time reads writes
delim: comma

#proc areadef
   areaname: standard
   title: Disk Read and Writes for $PART_NAME over time
   titledetails: size=14 align=C
   xscaletype: time hh:mm:ss
   xrange: 0:00 23:59
   yautorange: datafield=reads,writes
   linebottom: yes
   lineside: yes

#proc xaxis
   stubs: inc 1 hour
   stubformat: hh
   stubdetails: size=9
   grid: color=gray(0.9) width=0.1 style=2
   tics: yes

#proc yaxis:
   stubs: inc
   stubdetails: size=9
   grid: color=gray(0.9) width=0.1 style=2
   axisline: yes
   tics: yes
   gridskip: minmax
   nolimit: yes

#proc curvefit
  xfield: time
  yfield: writes
  linedetails: color=purple width=.5
  legendlabel: Disk Writes in MB/s

#proc curvefit
  xfield: time
  yfield: reads
  linedetails: color=brightblue width=.5
  legendlabel: Disk Reads in MB/s

#proc legend
  location: min+1 min-1
  textdetails: size=5
  format: across
  colortext: yes
EOT
        [ -s $TDIR/${F##*#}.png ] && (cd $TDIR && zip $ZIPQUIET "$ZIPFILE" ${F##*#}.png)
        ;;

    #----------------------------------------------------------------
    [vhs]d[a-z][0-9]|nvme[0-9]n[0-9]|xvd[a-z][0-9]|dm-[0-9])
        # Disk partitions
        # Ex.: 2021-11-09,07:15:03,311890944,12267520
        [ -s $TDIR/${F}.csv ] || continue
        $NOCACHE grep -a "^${STATS_DAY}," $FNAME 2> /dev/null | \
          awk -F, '
NR == 1 { 
 # We start with 0 deltas
 delta_read=0;prev_read=$3;
 delta_write=0;prev_write=$4;
 print $1","$2","delta_read","delta_write;
 next; }
NR > 1  {
 # Compute real deltas, but treat wraparound as 0 delta
 delta_read=(($3 > prev_read) ? (($3-prev_read)/1048576) : 0);prev_read=$3;
 delta_write=(($4 > prev_write) ? (($4-prev_write)/1048576) : 0);prev_write=$4;
 printf "%s,%s,%.2f,%.2f\n", $1,$2,delta_read,delta_write; }' > $TDIR/${F##*#}.csv

        echo >> /tmp/SyslogSummary.txt
        DR=$(awk -F, -v N=3 '{ sum += $N } END { if (NR > 0) print sum / NR }' $TDIR/${F}.csv)
        printf " Data reads average for %s = %0.2f\n" ${F//#//} $DR >> /tmp/SyslogSummary.txt
        DW=$(awk -F, -v N=4 '{ sum += $N } END { if (NR > 0) print sum / NR }' $TDIR/${F}.csv)
        printf " Data writes average for %s = %0.2f\n" ${F//#//} $DW >> /tmp/SyslogSummary.txt

        PART_NAME="${F//#//}"
        MP=$(grep "$PART_NAME" /proc/mounts | cut -d' ' -f2)
        if [ -n "$MP" ]
        then
            # Add the mount point to the disk partition
            PART_NAME="${F//#//} ($MP)"
        else
            # Get the mountpoint via major and minor device numbers
            DEV_NUM=$(ls -l /dev/${F//#//} 2> /dev/null | awk '{print int($5)":"$6}')
            if [ -n "$DEV_NUM" ]
            then
                MP=$(lsblk -l | awk "/${DEV_NUM}"'/{print $1}')
                if [ "T${MP:0:1}" = 'T/' ]
                then
                    # We have a valid mount point
                    PART_NAME="${F//#//} ($MP)"
                else
                    # Get the real mount point
                    RMP=$(grep "/dev/mapper/$MP" /proc/mounts | cut -d' ' -f2)
                    if [ -n "$RMP" ]
                    then
                        # Use the real mount point
                        PART_NAME="${F//#//} ($RMP)"
                    else
                        # Use the logical volume name
                        PART_NAME="${F//#//} ($MP)"
                    fi
                fi
            fi
        fi

        ploticus $DEBUG -png -o $TDIR/${F##*#}.png -stdin << EOT
#proc page
#if @DEVICE in png,gif
   scale: 0.9
#endif

#proc getdata
file: $TDIR/${F##*#}.csv
fieldnames: date time reads writes
delim: comma

#proc areadef
   areaname: standard
   title: Disk Read and Writes for $PART_NAME over time
   titledetails: size=14 align=C
   xscaletype: time hh:mm:ss
   xrange: 0:00 23:59
   yautorange: datafield=reads,writes
   linebottom: yes
   lineside: yes

#proc xaxis
   stubs: inc 1 hour
   stubformat: hh
   stubdetails: size=9
   grid: color=gray(0.9) width=0.1 style=2
   tics: yes

#proc yaxis:
   stubs: inc
   stubdetails: size=9
   stubformat: %3.0f
   grid: color=gray(0.9) width=0.1 style=2
   axisline: yes
   tics: yes
   gridskip: minmax
   nolimit: yes

#proc curvefit
  xfield: time
  yfield: writes
  linedetails: color=purple width=.5
  legendlabel: Disk Writes in MB/s

#proc curvefit
  xfield: time
  yfield: reads
  linedetails: color=brightblue width=.5
  legendlabel: Disk Reads in MB/s

#proc legend
  location: min+1 min-1
  textdetails: size=5
  format: across
  colortext: yes
EOT
        [ -s $TDIR/${F##*#}.png ] && (cd $TDIR && zip $ZIPQUIET "$ZIPFILE" ${F##*#}.png)

        # Handle speeds
        # Ex.: 2021-11-09,07:15:03,311890944,12267520
        FNAME="/var/tmp/Speeds_${F}.csv"
        [ -s $FNAME ] || continue
        $NOCACHE grep -a "^${STATS_DAY}," $FNAME > $TDIR/${FNAME##*/} 2> /dev/null

        echo >> /tmp/SyslogSummary.txt
        DRS=$(awk -F, -v N=3 '{ sum += $N } END { if (NR > 0) print sum / NR }' $TDIR/${FNAME##*/})
        printf " Data read speed average for %s = %0.2f\n" ${F//#//} $DRS >> /tmp/SyslogSummary.txt
        DWS=$(awk -F, -v N=4 '{ sum += $N } END { if (NR > 0) print sum / NR }' $TDIR/${FNAME##*/})
        printf " Data write speed average for %s = %0.2f\n" ${F//#//} $DWS >> /tmp/SyslogSummary.txt

        ploticus $DEBUG -png -o $TDIR/${FNAME##*/}.png -stdin << EOT
#proc page
#if @DEVICE in png,gif
   scale: 0.9
#endif

#proc getdata
file: $TDIR/${FNAME##*/}
fieldnames: date time readspeed writespeed
delim: comma

#proc areadef
   areaname: standard
   title: Disk Read and Writes for ${F//#//} over time
   titledetails: size=14 align=C
   xscaletype: time hh:mm:ss
   xrange: 0:00 23:59
   yautorange: datafield=readspeed,writespeed
   linebottom: yes
   lineside: yes

#proc xaxis
   stubs: inc 1 hour
   stubformat: hh
   stubdetails: size=9
   grid: color=gray(0.9) width=0.1 style=2
   tics: yes

#proc yaxis:
   stubs: inc
   stubdetails: size=9
   grid: color=gray(0.9) width=0.1 style=2
   axisline: yes
   tics: yes
   gridskip: minmax
   nolimit: yes

#proc curvefit
   xfield: time
   yfield: readspeed
   linedetails: color=brightblue width=0.5
   legendlabel: Disk Reads in KB/s

#proc curvefit
  xfield: time
  yfield: writespeed
  linedetails: color=purple width=.5
  legendlabel: Disk Writes in KB/s

#proc legend
  location: min+1 min-1
  textdetails: size=5
  format: across
  colortext: yes
EOT
        [ -s $TDIR/${FNAME##*/}.png ] && (cd $TDIR && zip $ZIPQUIET "$ZIPFILE" ${FNAME##*/}.png)
        ;;

    #----------------------------------------------------------------
    *[0-9]|ovs-system|br-*)
        # Network interface stats
        # Ex.: 2021-11-09,07:15:03,521312526,1317617,0    ,206031,0     ,0      ,0     ,0      ,442736716,1054023,0    ,0     ,0     ,0      ,0      ,0
        #                          RXBytes  ,RXpkts ,RXerr,RXDrop,RXfifo,RXframe,RXcomp,RXmulti,TXBytes  ,TXpkts ,TXerr,TXDrop,TXfifo,TXframe,TXcomp,TXmulti
        $NOCACHE grep -a "^${STATS_DAY}," $FNAME 2> /dev/null | \
          awk -F, '
NR == 1 { 
 # We start with 0 deltas
 delta_rx_bytes=0;prev_rx_bytes=$3;
 delta_rx_errs=0;prev_rx_errs=$5;
 delta_tx_bytes=0;prev_tx_bytes=$11;
 delta_tx_errs=0;prev_tx_errs=$13;
 print $1","$2","delta_rx_bytes","delta_rx_errs","delta_tx_bytes","delta_tx_errs;
 next; }
NR > 1  {
 # Compute real deltas, but treat wraparound as 0 delta
 delta_rx_bytes=(($3 > prev_rx_bytes) ? (($3-prev_rx_bytes)/1048576) : 0);prev_rx_bytes=$3;
 delta_rx_errs=(($5 > prev_rx_errs) ? (($5-prev_rx_errs)/1048576) : 0);prev_rx_errs=$5;
 delta_tx_bytes=(($11 > prev_tx_bytes) ? (($11-prev_tx_bytes)/1048576) : 0);prev_tx_bytes=$11;
 delta_tx_errs=(($13 > prev_tx_errs) ? (($13-prev_tx_errs)/1048576) : 0);prev_tx_errs=$13;
 printf "%s,%s,%.2f,%.2f,%.2f,%.2f\n", $1,$2,delta_rx_bytes,delta_rx_errs,delta_tx_bytes,delta_tx_errs; }' > $TDIR/${F}.csv
        [ -s $TDIR/${F}.csv ] || continue

        echo >> /tmp/SyslogSummary.txt
        RXB=$(awk -F, -v N=3 '{ sum += $N } END { if (NR > 0) print sum / NR }' $TDIR/${F}.csv)
        printf " Receive bytes average for %s = %0.2f\n" ${F//#//} $RXB >> /tmp/SyslogSummary.txt
        RXE=$(awk -F, -v N=4 '{ sum += $N } END { if (NR > 0) print sum / NR }' $TDIR/${F}.csv)
        printf " Receive error average for %s = %0.2f\n" ${F//#//} $RXE >> /tmp/SyslogSummary.txt
        TXB=$(awk -F, -v N=5 '{ sum += $N } END { if (NR > 0) print sum / NR }' $TDIR/${F}.csv)
        printf " Transmit bytes average for %s = %0.2f\n" ${F//#//} $TXB >> /tmp/SyslogSummary.txt
        TXE=$(awk -F, -v N=6 '{ sum += $N } END { if (NR > 0) print sum / NR }' $TDIR/${F}.csv)
        printf " Transmit error average for %s = %0.2f\n" ${F//#//} $TXE >> /tmp/SyslogSummary.txt

        INTERFACE_NAME="$F"
        if [ -s /tmp/$$.qms ]
        then
            if [[ $F =~ tap ]]
            then
               # Interface for a virtual guest
               if [[ $F =~ ([[:digit:]]{1,}) ]]
               then
                   VMNAME=$(awk "/^${BASH_REMATCH[1]}"' /{print $NF}' /tmp/$$.qms)
                   logger -p daemon.info -t ${PROG}[$$] -- Found virtual guest name "$VMNAME" for "$F"
                   INTERFACE_NAME="$F ($VMNAME)"
               fi
            elif [[ $F =~ fw ]]
            then
               # A Proxmox firewall interface
               INTERFACE_NAME="$F (firewall)"
               logger -p daemon.info -t ${PROG}[$$] -- Found firewall interface "$F"
            fi
        else
            # Potentially a "normal" interface
            IPA=$(ip address show dev $F | awk '/inet /{print $2}')
            if [ -n "$IPA" ]
            then
                INTERFACE_NAME="$F ($IPA)"
                logger -p daemon.info -t ${PROG}[$$] -- Found IP address "$IPA" for "$F"
            fi
        fi

        ploticus $DEBUG -png -o $TDIR/${F}.png -stdin << EOT
#proc page
#if @DEVICE in png,gif
   scale: 0.9
#endif

#proc getdata
file: $TDIR/${F}.csv
fieldnames: date time rxb rxe txb txe
delim: comma

#proc areadef
   areaname: standard
   title: Network stats for ${INTERFACE_NAME} over time
   titledetails: size=14 align=C
   xscaletype: time hh:mm:ss
   xrange: 0:00 23:59
   yautorange: datafield=rxb,txb
   linebottom: yes
   lineside: yes

#proc xaxis
   stubs: inc 1 hour
   stubformat: hh
   stubdetails: size=9
   grid: color=gray(0.9) width=0.1 style=2
   tics: yes

#proc yaxis:
   stubs: inc
   stubdetails: size=7
   stubformat: %3.0f
   grid: color=gray(0.9) width=0.1 style=2
   axisline: yes
   tics: yes

#proc curvefit
  xfield: time
  yfield: rxb
  linedetails: color=purple width=.5
  legendlabel: Receive bytes

#proc curvefit
  xfield: time
  yfield: txb
  linedetails: color=brightblue width=.5
  legendlabel: Transmit bytes

#proc curvefit
  xfield: time
  yfield: rxe
  linedetails: color=drabgreen width=.5
  legendlabel: Receive errors

#proc curvefit
  xfield: time
  yfield: txe
  linedetails: color=powderblue width=.5
  legendlabel: Transmit errors

#proc legend
  location: min+1 min-1
  textdetails: size=5
  format: across
  colortext: yes
EOT
        [ -s $TDIR/${F}.png ] && (cd $TDIR && zip $ZIPQUIET "$ZIPFILE" ${F}.png)
        ;;

    #----------------------------------------------------------------
    *)
        # Anything else is unknown
        echo "WARNING: Unknown stats file '$F'"
        ;;
    esac
done

# Delete duplicate empty lines in the summary
# See http://sed.sourceforge.net/sed1line.txt
[ -s /tmp/SyslogSummary.txt ] && sed -i -e '$!N; /^\(.*\)\n\1$/!P; D' /tmp/SyslogSummary.txt

#--------------------------------------------------------------------
# Remove older graphs
logger -p daemon.info -t ${PROG}[$$] -- Deleting old zip files in /var/tmp
find /var/tmp -type f -name "SysMon-Graphs*zip" -mtime +14 -delete

#--------------------------------------------------------------------
# Send email if specified
if [ -n "$EMAIL_TO" ]
then
    logger -p daemon.info -t ${PROG}[$$] -- Sending email to "$EMAIL_TO" via "$EMAIL_SERVER"
    DEBUG='-q'
    [[ $- ==  *x* ]] && DEBUG='-v'
    cd $TDIR || exit
    echo 'For details graphs see the attached files' > /tmp/$$
    if [ -s /tmp/SyslogSummary.txt ]
    then
        echo >> /tmp/$$
        echo 'A summary is shown below:' >> /tmp/$$
        cat /tmp/SyslogSummary.txt >> /tmp/$$
    fi
    sendemail $DEBUG -f root@$THISDOMAIN -t $EMAIL_TO -s $EMAIL_SERVER \
      -u "System Monitoring graphs for $THISHOST" -o tls=no \
      -a $(echo "*png" | sed 's/ / -a /g' | sort) < /tmp/$$
fi

#--------------------------------------------------------------------
# We are done
logger -p daemon.info -t ${PROG}[$$] -- Done
exit 0
