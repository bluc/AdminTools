#!/bin/bash
################################################################
# (c) Copyright 2013 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            

#--------------------------------------------------------------------
# Can we use a popup dialog program for questions?
USE_DIALOG=0
[ -x /bin/whiptail ] && USE_DIALOG=1

#--------------------------------------------------------------------
# Remove the (old) debian/ubuntu package
PI=$(dpkg-query -s clamav-unofficial-sigs 2> /dev/null | grep 'Status.*installed')
[ -z "$PI" ] || apt-get -y purge clamav-unofficial-sigs

# Download the latest archive
TFILE=$(mktemp /tmp/$$XXXXXXXX)
wget --header='Accept-Encoding: gzip' -q --no-check-certificate -O $TFILE \
   https://github.com/extremeshok/clamav-unofficial-sigs/archive/master.zip
cmp $TFILE /tmp/clamav-unofficial-sigs.zip &> /dev/null
if [ $? -eq 0 ]
then
    echo "Already have latest clamav-unofficial-sigs code"
    exit 0
fi
mv $TFILE /tmp/clamav-unofficial-sigs.zip

# Uncompress/Unpackage the archive:
rm -rf /tmp/clamav-unofficial-sigs-master
unzip -qq -d /tmp /tmp/clamav-unofficial-sigs.zip

install -m 755 /tmp/clamav-unofficial-sigs-master/clamav-unofficial-sigs.sh /usr/local/bin/

#--------------------------------------------------------------------
# Install:
mkdir -p /usr/local/bin/
mkdir -p /var/log/clamav-unofficial-sigs/
mkdir -p /etc/clamav-unofficial-sigs/
cp -f /tmp/clamav-unofficial-sigs-master/clamav-unofficial-sigs.sh /usr/local/bin/clamav-unofficial-sigs.sh
chmod +x /usr/local/bin/clamav-unofficial-sigs.sh
cp -f /tmp/clamav-unofficial-sigs-master/config/*.* /etc/clamav-unofficial-sigs/

# Install Systemd configs:
cp -f /tmp/clamav-unofficial-sigs-master/systemd/*.* /etc/systemd/  

#--------------------------------------------------------------------
# Configure:
if [ -s /etc/lsb-release ]
then
    mv /etc/clamav-unofficial-sigs/os.ubuntu.conf /etc/clamav-unofficial-sigs/os.conf
elif [ -s /etc/debian_release ]
then
    # Need to find out what version of debian is running !!!
    mv /etc/clamav-unofficial-sigs/os.debian7.conf /etc/clamav-unofficial-sigs/os.conf
fi
# Edit the /etc/clamav-unofficial-sigs-master.conf, os.conf , user.conf files
if [ ! -z "$(grep YOUR-RECEIPT-NUMBER /etc/clamav-unofficial-sigs/master.conf)" ]
then
    if [ $USE_DIALOG -ne 0 ]
    then
        whiptail --title='Malwarepatrol receipt' --nocancel \
          --inputbox 'Receipt number for malwarepatrol.com: ' 9 40 2> $TFILE
        MP_RECEIPT=$(< $TFILE)
    else
        read -p 'Receipt number for malwarepatrol.com ? ' MP_RECEIPT
    fi
    [ -z "$MP_RECEIPT" ] || sed -i -e "s/YOUR-RECEIPT-NUMBER/$MP_RECEIPT/" /etc/clamav-unofficial-sigs/master.conf
fi
if [ ! -z "$(grep YOUR-SIGNATURE-NUMBER /etc/clamav-unofficial-sigs/master.conf)" ]
then
    if [ $USE_DIALOG -ne 0 ]
    then
        whiptail --title='SecurityInfo signature' --nocancel \
          --inputbox 'Signature number for securityinfo.com : ' 9 40 2> $TFILE
        SI_SIG=$(< $TFILE)
    else
        read -p 'Signature number for securityinfo.com ? ' SI_SIG
    fi
    [ -z "$SI_SIG" ] || sed -i -e "s/YOUR-SIGNATURE-NUMBER/$SI_SIG/" /etc/clamav-unofficial-sigs/master.conf
fi
sed -i -e 's/user_configuration_complete.*/user_configuration_complete="yes"/' /etc/clamav-unofficial-sigs/master.conf
  
# Edit the /etc/logrotate.d/clamav-unofficial-sigs file (if necessary)

# Edit the /etc/cron.d/clamav-unofficial-sigs file (if necessary)
   
# Install Cron configs:
/usr/local/bin/clamav-unofficial-sigs.sh --install-cron

# Install logrotate configs:: 
/usr/local/bin/clamav-unofficial-sigs.sh --install-logrotate

# Install man (help) file:
/usr/local/bin/clamav-unofficial-sigs.sh --install-man

# First Usage
/usr/local/bin/clamav-unofficial-sigs.sh

#--------------------------------------------------------------------
#To run at specific time intervals, either use the include cron file or edit the user crontab:
#   /etc/cron.d/clamav-unofficial-sigs
#To run manually:
#   /usr/local/bin/clamav-unofficial-sigs.sh
