#!/usr/bin/perl -w 
#--------------------------------------------------------------------
# (c) CopyRight 2018 Henrik K, B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#--------------------------------------------------------------------
# Based on http://spamassassin.1065346.n5.nabble.com/Bitcoin-rules-td153344.html
#--------------------------------------------------------------------
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/btcabuse.pl
exit 0; # Doesn't seem to work anymore

use 5.0010;

# Constants
my $ProgName = '';
( $ProgName = $0 ) =~ s%.*/%%;

# Common sense options:
use strict qw(vars subs);
no warnings;
use warnings qw(FATAL closed threads internal debugging pack
    portable prototype inplace io pipe unpack malloc
    glob digit printf layer reserved taint closure
    semicolon);
no warnings qw(exec newline unopened);

#--------------------------------------------------------------------
# Needed packages
#--------------------------------------------------------------------
use JSON::XS;          # requires libjson-xs-perl
use LWP::UserAgent;    # requires libwww-perl
use Getopt::Std;

#--------------------------------------------------------------------
# Globals
#--------------------------------------------------------------------
$ENV{PATH} = '/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin';
my $url = 'http://www.bitcoinabuse.com/api/reports/distinct';
my $ua  = LWP::UserAgent->new(
    keep_alive => 1,
    agent      => 'Wget/1.17.1 (linux-gnu)'
);
my $json;
my %ids;

# Program options
our $opt_m = 30;
our $opt_v = 0;
our $opt_h = 0;

#--------------------------------------------------------------------
# Parse one rule(=line) at a time
#--------------------------------------------------------------------
sub parse_rule
{
    my $id = $_[0]->{address};
    return if $id !~ /^\w{26,35}$/;
    return if defined $ids{$id};

    $ids{$id} = 1;
    my $len = 8;
    my $idshort = uc( substr( $id, 0, $len ) );
    while ( defined $ids{$idshort} )
    {
        $idshort = uc( substr( $id, 0, ++$len ) );
    } ## end while ( defined $ids{$idshort...})
    $ids{$idshort} = 1;
    warn "Creating rule 'BTC_$idshort'\n" if ($opt_v);
    print "\nbody BTC_$idshort /$id/i\n",
        "describe BTC_$idshort https://www.bitcoinabuse.com/reports/$id\n",
        "score BTC_$idshort 5\n",
        "priority BTC_$idshort 2\n";
} ## end sub parse_rule

#--------------------------------------------------------------------
# Display the usage
#--------------------------------------------------------------------
sub ShowUsage()
{

    print "Usage: $ProgName [options]\n",
        "       -m num  Specify max pages to download [default=$opt_m]\n",
        "       -h      Show this help [default=no]\n",
        "       -v      Show progress info on STDERR [default=no]\n";

    exit 0;
} ## end sub ShowUsage

#--------------------------------------------------------------------
# Main function
#--------------------------------------------------------------------
$|++;

# Get possible options
getopts('hvm:') or ShowUsage();
ShowUsage() if ($opt_h);

#--------------------------------------------------------------------
# Get all rules (the 29 most current pages)
#--------------------------------------------------------------------
for ( my $PageNo = 1; $PageNo < $opt_m; $PageNo++ )
{
    warn "Getting '$url'\n" if ($opt_v);
    my $r = $ua->get($url);
    die $r->status_line unless $r->is_success;

    eval { $json = decode_json( $r->decoded_content ); }
        or die "JSON parse failed: $@\n";
    die "No JSON Data\n" unless $json->{data};

    parse_rule($_) foreach ( @{ $json->{data} } );
    unless ( $json->{next_page_url} )
    {
        warn "No more pages left\n";
        last;
    } ## end unless ( $json->{next_page_url...})

    $url = $json->{next_page_url};
    sleep(1);
} ## end for ( my $PageNo = 1; $PageNo...)

#--------------------------------------------------------------------
# We are done
#--------------------------------------------------------------------
exit(0);
__END__
