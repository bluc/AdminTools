#!/bin/bash
# Version 20240401-035506 checked into repository
################################################################
# (c) Copyright 2021 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/netflow.postinstall.sh

#--------------------------------------------------------------------
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            

#--------------------------------------------------------------------
# Build nfsen (see https://github.com/mbolli/nfsen-ng/blob/master/INSTALL.md)
# add php repository
ls /etc/apt/sources.list.d/ondrej-*list &> /dev/null || DEBIAN_FRONTEND=noninteractive add-apt-repository -y ppa:ondrej/php

# install packages
DEBIAN_FRONTEND=noninteractive apt-get install \
  nfdump-sflow apache2 git pkg-config php8.3 php8.3-dev php8.3-mbstring libapache2-mod-php8.3 rrdtool librrd-dev jq

# Show current version of nfdump utilities
nfdump -V && nfpcapd -V && nfreplay -V && nfprofile -V && sfcapd -V
sleep 5

# Create a configuration to capture Mikrotik netflows
cat << EOT > /etc/nfdump/mikrotik.conf
# mikrotik configuration for nfcapd
# Options for this nfcapd instance, see nfcapd(1)
#
# Caveats:
# - You must define a base directory (-l or -M), this is *not* checked.
# - When using -l for the cache directory, you must give each instance
#   a separate directory, else all but the first instance will not
#   start.
# - Any cache_dir, user and group values defined above needs to be
#   repeated here in an according option (-l/-M, -u, -g).
#   Remember, shell expandsion will not work when using systemd.
# - Do not use the -D, -P, -l, and -I options, they are already set internally.
#
options='-4 -z -S 1 -T all -p 2055 -u daemon -g www-data -B 200000'
EOT

# Overwrite the default systemd file
cat << EOT > '/etc/systemd/system/nfdump@.service'
[Unit]
Description=netflow capture daemon, %I instance
Documentation=man:nfcapd(1)
After=network.target auditd.service
PartOf=nfdump.service

[Service]
Type=forking
EnvironmentFile=/etc/nfdump/%I.conf
ExecStart=/usr/bin/nfcapd -D -P /tmp/nfcapd.%I.pid -I %I -l /var/nfdump/profiles-data/live/%I \$options
ExecStartPre=-mkdir -p /var/nfdump/profiles-data/live/%I
ExecStartPre=chown -R daemon:www-data /var/nfdump
PIDFile=/tmp/nfcapd.%I.pid
KillMode=process
Restart=no

[Install]
WantedBy=multi-user.target
EOT
systemctl enable nfdump@mikrotik
systemctl daemon-reload

# Start the netflow capture daemon
systemctl restart nfdump@mikrotik || exit 1
#systemctl status nfdump@mikrotik

# enable apache modules
a2enmod rewrite deflate headers expires

# install rrd library for php
pecl install rrd

# create rrd library mod entry for php
PHPV=$(php -v | awk '/^PHP/{print $2}' | cut -d\. -f1,2)
if [ -d /etc/php/$PHPV/mods-available ]
then
    echo "extension=rrd.so" > /etc/php/$PHPV/mods-available/rrd.ini
else
    echo "ERROR: Can't find /etc/php/$PHPV/mods-available"
    exit 1
fi

# enable php mods
phpenmod rrd mbstring

# configure virtual host to read .htaccess files
vi /etc/apache2/apache2.conf # set AllowOverride All for /var/www directory

# restart apache web server
systemctl restart apache2

# install nfsen-ng
cd /var/www || exit 1 # or wherever, needs to be in the web root
git clone https://github.com/mbolli/nfsen-ng
rm -rf nfsen-ng/.git*
chown -R www-data:www-data .
chmod +x nfsen-ng/backend/cli.php

# install composer system-wide
cd /tmp && curl -sS https://getcomposer.org/installer | php
[ -s /tmp/composer.phar ] && install -m 0755 /tmp/composer.phar /usr/local/bin/composer
cd /var/www/nfsen-ng/ && composer install --no-dev

# Create configuration file from backend/settings/settings.php
cat << EOT > /var/www/nfsen-ng/backend/settings/settings.php
<?php
/**
 * config file for nfsen-ng.
 *
 * remarks:
 * * database name = datasource class name (case-sensitive)
 * * log priority should be one of the predefined core constants prefixed with LOG_
 */

\$nfsen_config = [
    'general' => [
        'ports' => [
            80, 22, 443, 53,
        ],
        'sources' => [
            'mikrotik',
        ],
        'db' => 'RRD',
        'processor' => 'NfDump',
    ],
    'frontend' => [
        'reload_interval' => 60,
        'defaults' => [
            'view' => 'graphs', // graphs, flows, statistics
            'graphs' => [
                'display' => 'sources', // sources, protocols, ports
                'datatype' => 'flows', // flows, packets, traffic
                'protocols' => ['any'], // any, tcp, udp, icmp, others (multiple possible if display=protocols)
            ],
            'flows' => [
                'limit' => 50,
            ],
            'statistics' => [
                'order_by' => 'bytes',
            ],
        ],
    ],
    'nfdump' => [
        'binary' => '$(command -v nfdump)',
        'profiles-data' => '/var/nfdump/profiles-data',
        'profile' => 'live',
        'max-processes' => 1, // maximum number of concurrently running nfdump processes
    ],
    'db' => [
        'RRD' => [],
    ],
    'log' => [
        'priority' => \\LOG_INFO, // LOG_DEBUG is very talkative!
    ],
];
EOT
chmod 644 /var/www/nfsen-ng/backend/settings/settings.php

# Create directory for RRD database
mkdir -p /var/www/nfsen-ng/backend/datasources/data \
  && chown www-data /var/www/nfsen-ng/backend/datasources/data

# Let PHP run a bit longer
find /etc/php/ -type f -name "php.ini" | while read -r PHPINI
do
   sed -i 's/^max_execution.*/max_execution_time = 300/' $PHPINI
done
service apache2 restart

#--------------------------------------------------------------------
# Start netflow capture daemon
#mkdir -p /var/nfdump/profiles_data && chown -R daemon: /var/nfdump
#nfpcapd -D -S 2 -w /var/nfdump/profiles_data -i ens18 -e 60,30 -u daemon -g daemon

# Start the netflow injest daemon for MikroTik
INSTANCE='mikrotik'
cat << EOT > /usr/local/sbin/netflow-${INSTANCE}.sh
#!/bin/bash
# Only start services if networking is up
[ "T\$(systemctl is-active systemd-networkd)" == 'Tactive' ] || exit 0

# Create the necessary data storage
mkdir -p /var/nfdump/profiles-data/${INSTANCE} && chown -R daemon:www-data /var/nfdump

# (re)start the nfsen collector
[ -z "\$(ps -wefH | grep 'php.*liste[n]')" ] && /var/www/nfsen-ng/backend/cli.php start

if [ -z "\$(pgrep nfcapd)" ]
then
    # (re)start the netflow collector
    systemctl restart nfdump@mikrotik || exit 1
fi
EOT
chmod 744 /usr/local/sbin/netflow-${INSTANCE}.sh
cat << EOT > /etc/cron.d/netflow
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/sbin:/sbin:/usr/local/bin:/usr/bin:/bin:/snap/bin
#MAILTO=<FILL IN THE CORRECT EMAIL ADDRESS>
#===============================================================
# (re)start nfcapd and nfsen collector
* * * * *       root    [ -x /usr/local/sbin/netflow-${INSTANCE}.sh ] && /usr/local/sbin/netflow-${INSTANCE}.sh
EOT

#--------------------------------------------------------------------
# Example for Mikrotik setup:
#/ip traffic-flow
#set active-flow-timeout=1m enabled=yes
#/ip traffic-flow target
#add dst-address=192.168.1.20 src-address=192.168.1.1 port=2055

# Example output for nfdump:
# Show the top 10 records sorted by number of bytes in extended format
#  where certain networks and hosts are included/excluded:
#nfdump -R /var/nfdump/profiles_data/mikrotik/2024 'net 192.168.1.0/24 and not host 192.168.1.13' -O bytes -o extended -n 10

#--------------------------------------------------------------------
# Get the current config
curl http://localhost/api/config | jq .

#--------------------------------------------------------------------
# We are done
exit 0
