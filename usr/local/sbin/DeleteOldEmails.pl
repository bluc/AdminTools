#!/usr/bin/env perl
# Version 20240713-125329 checked into repository
#--------------------------------------------------------------------
# (c) CopyRight 2023 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#--------------------------------------------------------------------
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/DeleteOldEmails.pl
# Delete emails from the postfix queue that are:
# 1. queue because the receiving mailbox is disabled
# 2. are in the queue for more than 3 days
use 5.0010;

# Common sense options:
use strict qw(vars subs);
no warnings;
use warnings qw(FATAL closed threads internal debugging pack
    portable prototype inplace io pipe unpack malloc
    glob digit printf layer reserved taint closure
    semicolon);
no warnings qw(exec newline unopened);

# Program options
delete @ENV{qw(IFS CDPATH ENV BASH_ENV)};
$ENV{'PATH'} = '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin';

# Required perl packages
use POSIX qw(strftime);
use Date::Parse;    # Debian package libtimedate-perl
use Text::Wrap;
use Getopt::Std;
require '/usr/local/include/loadcheck.pm';

# Constants
my $ProgName = '';
( $ProgName = $0 ) =~ s%.*/%%;

# Globals
my @Lines         = ();
my $LineNo        = 0;
my $Now           = strftime( "%s", localtime );
my @MailID2Delete = ();
my $OneDaySeconds = 24 * 60 * 60;

# Options
our $opt_h = 0;
our $opt_n = 0;
our $opt_d = 0;
our $opt_m = 3;

#--------------------------------------------------------------------
# Run this script only on servers with postfix
exit(0)
    unless ( -d '/etc/postfix' )
    or ( -d '/opt/zimbra' )
    or ( -d '/opt/zextras' );

#--------------------------------------------------------------------
# Display the usage
sub ShowUsage()
{

    print "Usage: $ProgName [options]\n",
        "       -m days  Specify days in queue for an email [default=$opt_m]\n",
        "       -h       Show this help [default=no]\n",
        "       -n       Do a 'dryrun'\n",
        "       -d       Show some debug info on STDERR [default=no]\n";

    exit 0;
} ## end sub ShowUsage

#--------------------------------------------------------------------
# The main function
# Get possible options
getopts('dhm:n') or ShowUsage();
ShowUsage() if ($opt_h);

# Requires "mailq" to be present in path
# Examples:  /usr/bin/mailq
# Can be linked to /opt/zimbra/common/sbin/mailq or
#                  /opt/zextras/common/sbin/mailq
LoadCheck();
open( my $Q, '-|', 'mailq' ) or die("Can not read from mailq command: $!");
while ( my $MLine = <$Q> )
{
    # Skip leading and trailing lines
    next if ( substr( $MLine, 0, 2 ) eq '-Q' );
    last
        if ( substr( $MLine, 0, 2 ) eq '--' )
        or ( $MLine =~ /^Mail queue is empty/o );

    $LineNo++;
    chomp($MLine);
    if ( length($MLine) )
    {
        # Add line to current queue item
        push( @Lines, "$MLine" );
    } else
    {
        # An empty line signals the end of an queue item
        if ( scalar @Lines )
        {
            my $QEntry = join( " ", @Lines );
            $QEntry =~ s/\s+/ /g;
            @Lines = ();

            # At this time we are only interested in emails that can't
            #  be delivered due to the recipient's mailbox being disabled
            next
                if ( $QEntry !~ /Mailbox disabled, not accepting messages/o );

# Extract relevant infos form the queue entry
# Example entry:
#4E25225ECA2F 23219 Thu Feb 2 12:42:13 messages@info.dot.gov (host mail.digitaltowpath.org[x.x.x.x] said: 450 4.2.1 Mailbox disabled, not accepting messages (in reply to RCPT TO command)) recipient@domain.org
            my (@field) = split( /\s+/, $QEntry );
            my $MailID = "$field[0]";
            my $MailDateTime  = "$field[3] $field[4] $field[5]";
            my $MailSender    = "$field[6]";
            my $MailRecipient = join( ' ', @field[ 7 .. $#field ] );

            # How many days has this message been in the queue?
            my $MailAge
                = ( $Now - str2time("$MailDateTime") ) / $OneDaySeconds;

            # Ignore entries that too young
            warn "DEBUG: MailID='$MailID' MailAge='$MailAge days'"
                . " From='$MailSender' To='$MailRecipient'\n"
                if ($opt_d);
            next if ( $MailAge < $opt_m );

            # Remove non-alphanumeric characters from mail ID
            $MailID =~ s/[^[:alnum:]]//g;
            # Remember the mail ID
            push( @MailID2Delete,
                "$MailID $MailAge $MailSender $MailRecipient" );
            warn "DEBUG: Deleting '$MailID'\n" if ($opt_d);
        } ## end if ( scalar @Lines )
    } ## end else [ if ( length($MLine) ) ]
} ## end while ( my $MLine = <$Q> ...)
close($Q);

#--------------------------------------------------------------------
# Actually delete emails (unless we are doing a 'dryrun')
my $NumberDeletes = scalar @MailID2Delete;
warn "DEBUG: Deleting $NumberDeletes emails\n" if ($opt_d);
if ( $NumberDeletes > 0 )
{
    # Keep track of actual number of deletes
    my $ActualDeletes = 0;

    # Create the temp file for the "postsuper" commands
    my $PSB = "/tmp/$$.psb";
    if ( open( my $PSB, '>', "$PSB" ) )
    {

        # Unbuffer STDOUT
        $|--;
        $Text::Wrap::columns = 72;
        my $Would = ( $opt_n ? "Would delete" : 'Deleting' );
        print wrap (
            '',
            '',
            "$Would $NumberDeletes email(s) from queue that are both undeliverable "
                . "due to the recipient's mailbox being disabled and have "
                . "been queued up for least $opt_m days."
            ),
            "\n\n";
        for my $QEntry (@MailID2Delete)
        {
            my (@field) = split( /\s+/, $QEntry );
            printf "%s %.2f day(s) old email '%s' from '%s' to '%s'\n\n",
                $Would, $field[1], $field[0], $field[2],
                join( ' ', @field[ 3 .. $#field ] );
            next if ($opt_n);

            # Notate the queue ID for later deletion
            print $PSB "postsuper -d $field[0]\n";
            $ActualDeletes++;
        } ## end for my $QEntry (@MailID2Delete...)
        close($PSB);
    } ## end if ( open( my $PSB, '>'...))

    LoadCheck();
    system( 'bash ' . ( ($opt_d) ? '-x' : '' ) . " $PSB" )
        if ($ActualDeletes);
    unlink("$PSB") or system("rm -f $PSB");
} ## end if ( $NumberDeletes > ...)

#--------------------------------------------------------------------
# We are done
exit(0);
__END__
