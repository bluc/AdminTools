#!/bin/bash
# Version 20240122-063348 checked into repository
################################################################
# (c) Copyright 2016 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

PROG=${0##*/}
ionice -c2 -n7 -p $$

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE

# Define the email address for the resulting image
EMAIL_TO=''

THISHOST=$(hostname)
[[ $THISHOST == *.* ]] || THISHOST=$(hostname -f)
THISDOMAIN=${THISHOST#*.}

#--------------------------------------------------------------------
# Install any necessary programs
for P in ploticus sendemail #webfs
do
    dpkg-query -W $P &>/dev/null
    [ $? -ne 0 ] && apt-get -y install $P
done

#--------------------------------------------------------------------
# Get possible program options
while getopts ht: OPTION
do
    case ${OPTION} in
        t)  EMAIL_TO="$OPTARG"
            ;;
        *)  cat << EOT
Usage: $PROG [options] [day hour]
       -t email  Send report to email address [default=don't send]
          Email is being sent at the end of the day plus
           at the start, middle and end of the work day

       The "day hour" must be specified in this format:
       'MMM DD HH', e.g. 'Sep  3 10', 'Jul 12 09'
       (days must NOT have a leading zero, but hours MUST have it)
EOT
            exit 0
            ;;
    esac
done
shift $((OPTIND - 1))

#--------------------------------------------------------------------
# Define the day and hour to collect stats for
if [ -z "$1" ]
then
    # Get the date and hour for 1 hour ago
    HR_AGO=$((EPOCHSECONDS - 3600))
    PF_1HR_AGO=$(date '+%b %e %H' -d @${HR_AGO})
else
    PF_1HR_AGO="$1"
fi
set $PF_1HR_AGO
DAY="$1-$2"
HOUR=${3##0}

#--------------------------------------------------------------------
# Extract the info for the desired day and hour
TFILE=$(mktemp /tmp/$$XXXXXXXX)
grep -a "^${PF_1HR_AGO}:" /var/log/mail.log > $TFILE

#--------------------------------------------------------------------
# Gather stats
TOTAL_CONN=$(grep -a ' connect from' $TFILE | grep -Eavc '\[(127\.|10\.|172\.1[6-9]\.|172\.2[0-9]\.|172\.3[0-1]\.|192\.168\.|localhost)')
REJECT=$(grep -ac  'NOQUEUE: reject' $TFILE)

VIRUS=$(grep -ac 'Blocked INFECTED' $TFILE)
SPAM=$(grep -ac 'Blocked SPAM' $TFILE)

DELIVERED=$(grep -Eac ':(587|825)' $TFILE)

# Write out the data into a file in this format:
#
#  Time      Total   Reject  Virus    Spam  Delivered
#00:00:00     208      42      15      25     88
#01:00:00     208      42      15      25     88
#02:00:00     208      42      15      25     88

# Remove any existing line for the hour
[ -s /var/tmp/PFStats.$DAY ] && sed -i -e "/^${HOUR}:00:00/d" /var/tmp/PFStats.$DAY

# Finally write the line
cat << EOT >> /var/tmp/PFStats.$DAY
${HOUR}:00:00 $TOTAL_CONN $REJECT $VIRUS $SPAM $DELIVERED
EOT

#--------------------------------------------------------------------
# We are done unless we have an email address to send the graph to
[ -z "$EMAIL_TO" ] && exit 0

#--------------------------------------------------------------------
# Plot the data
# See also http://ploticus.sourceforge.net/gallery/sar-cpu.htm
DEBUG=''
[[ $- ==  *x* ]] && DEBUG='-debug'
ploticus -png -o $TFILE.${DAY}.png -stdin $DEBUG << EOT
#proc page
#if @DEVICE in png,gif
   scale: 0.6
#endif

#proc getdata
  file: /var/tmp/PFStats.$DAY
  fieldnames: time total reject virus spam delivered
  delim: whitespace

#proc areadef
   title: Postfix statistics for $DAY
   titledetails: size=14  align=C
   rectangle: 1 1 8 4
   xscaletype: time hh:mm:ss
   xrange: 00:00:00 23:59:59
   yautorange: datafield=total nearest=10 lowfix=0

#proc xaxis
   stubs: inc 1 hours
   minorticinc: 30 minutes
   stubformat: hhA

#proc yaxis
  stubs: inc
  gridskip: minmax
  ticincrement: 100 1000
  grid: color=dullyellow width=0.1 style=2

#proc curvefit
  xfield: time
  yfield: total
  linedetails: color=purple width=.5
  legendlabel: total Internet conns

#proc curvefit
  xfield: time
  yfield: reject
  linedetails: color=red width=.5
  legendlabel: rejected

#proc curvefit
  xfield: time
  yfield: virus
  linedetails: color=blue width=.5
  legendlabel: virus

#proc curvefit
  xfield: time
  yfield: spam
  linedetails: color=orange width=.5
  legendlabel: spam

#proc curvefit
  xfield: time
  yfield: delivered
  linedetails: color=brightgreen width=.5
  legendlabel: delivered

#proc legend
  location: max-1 max
  seglen: 0.2
EOT

#--------------------------------------------------------------------
# Send the resulting image to "$EMAIL_TO"
if [ -s $TFILE.${DAY}.png ]
then
    SEND_EMAIL=0
    if [[ $- ==  *x* ]]
    then
        # In debug mode send now
        SEND_EMAIL=1
    else
        # Send at the end of the day, and at start/middle/end of work day
        case $HOUR in
            7|12|17|23)
                SEND_EMAIL=2
                ;;
        esac
    fi
    if [ $SEND_EMAIL -ne 0 ]
    then
        # Send the resulting image to "$EMAIL_TO"
        DEBUG='-q'
        [[ $- ==  *x* ]] && DEBUG='-v'
        echo "See attached picture" | \
            sendemail $DEBUG -f root@$THISDOMAIN -t $EMAIL_TO -a $TFILE.${DAY}.png \
            -s localhost -u "Hourly postfix statistics for $THISHOST" -o tls=no
    fi
fi

# We are done
exit 0
