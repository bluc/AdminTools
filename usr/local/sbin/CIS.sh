#!/bin/bash
# Version 20240320-104446 checked into repository
################################################################
# (c) Copyright 2020 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/CIS.sh

# Based on CIS Benchmarks for Ubuntu Linux 20.04 LTS, Version 1.0.0

#--------------------------------------------------------------------
# Sanity checks
if [ -s /etc/lsb-release ]
then
    source /etc/lsb-release
    if [ "T${DISTRIB_ID^^}" != 'TUBUNTU' ]
    then
        echo 'This is not an Ubuntu Server'
        exit 0
    fi
else
    echo 'This is not an Ubuntu Server'
    exit 0
fi

# Make sure we remove temp files at exit
trap "rm -f /tmp/$$*" EXIT

#--------------------------------------------------------------------
# Suppress reboot if requested
NO_REBOOT=0
[ "T$1" = 'Tnoreboot' ] && NO_REBOOT=1

#--------------------------------------------------------------------
# Server or Workstation?
dpkg-query -W -f='${binary:Package}\t${Status}\t${db:Status-Status}\n' xserver-xorg
SERVER=$?

#--------------------------------------------------------------------
# Virtual or hardware?
IS_VIRTUAL=0
if [ ! -z "\$(grep -m1 VMware /proc/scsi/scsi)" -o ! -z "\$(grep 'DMI:.*VMware' /var/log/dmesg)" ]
then
    IS_VIRTUAL=1
elif [ ! -z "\$(egrep 'KVM|QEMU' /proc/cpuinfo)" -o ! -z "\$(grep Bochs /sys/class/dmi/id/bios_vendor)" ]
then
    IS_VIRTUAL=2
elif [ ! -z "\$(grep '^flags[[:space:]]*.*hypervisor' /proc/cpuinfo)" ]
then
    IS_VIRTUAL=3
fi

#--------------------------------------------------------------------
# By default we don't need to reboot
REBOOT=0

#--------------------------------------------------------------------
# Disable unnecessary file systems
for FS in cramfs freevxfs jffs2 hfs hfsplus
do
    echo "install $FS /bin/true" > /etc/modprobe.d/${FS}.conf
    if [ ! -z "$(lsmod | grep $FS)" ]
    then
        rmmod $FS
        REBOOT=1
    fi
done
if [ $SERVER -ne 0 ]
then
    # Servers only
    for FS in udf usb-storage
    do
        echo "install $FS /bin/true" > /etc/modprobe.d/${FS}.conf
        if [ ! -z "$(lsmod | grep $FS)" ]
        then
            rmmod $FS
            REBOOT=1
        fi
    done

    # Remove packages not needed on servers
    for P in avahi-daemon cups autofs
    do
        dpkg-query -W -f='${binary:Package}\t${Status}\t${db:Status-Status}\n' $P
        [ $? -eq 0 ] && apt -y purge $P
    done

    # Disable wireless interfaces
    if command -v nmcli >/dev/null 2>&1
    then
        nmcli radio all off
    else
        if [ -n "$(find /sys/class/net/*/ -type d -name wireless)" ]
        then
            drivers=$(for driverdir in $(find /sys/class/net/*/ -type d -name wireless | xargs -0 dirname)
            do
                basename "$(readlink -f "$driverdir"/device/driver)"
            done | sort -u)
            for dm in $drivers;
            do
                echo "install $dm /bin/true" >> /etc/modprobe.d/disable_wireless.conf
            done
        fi
    fi

    # Disable bluetooth
    systemctl is-enabled bluetooth.service &> /dev/null
    if [ $? -eq 0 ]
    then
       systemctl stop bluetooth.service
       systemctl mask bluetooth.service
    fi

    # Disable some modules
    for M in ppdev lp parport_pc parport_pc parport psmouse pcspkr snd_pcm snd_timer snd soundcore snd_page_alloc
    do
        modprobe -n -v $M &> /dev/null
        if [ $? -eq 0 ]
        then
            echo "install $M /bin/false" > /etc/modprobe.d/blacklist-${M}.conf
            [ -z "$(grep ^$M'[[:space:]]' /proc/modules)" ] || modprobe -r $M &> /dev/null
        fi
    done
fi

#--------------------------------------------------------------------
# Harden /tmp
if [ -z "$(grep /tmp /etc/fstab)" ]
then
    echo 'tmpfs	/tmp	tmpfs     defaults,rw,nosuid,nodev,noatime,size=2G  0 0' >> /etc/fstab
    REBOOT=1
else
    TMP_MOUNTED="$(grep -E '/tmp' /proc/mounts)"
    if [ ! -z "$TMP_MOUNTED" ]
    then
        mount -o remount,nodev,exec,nosuid,rw /tmp
    fi
fi

#--------------------------------------------------------------------
# Harden /dev/shm
if [ -z "$(grep shm /proc/mounts)" ]
then
    mount tmpfs -o noexec,nodev,nosuid /dev/shm
else
    mount -o remount,noexec,nodev,nosuid /dev/shm
fi

#--------------------------------------------------------------------
# Enable logging for sudo
if [ -d /etc/sudoers.d ]
then
    cat << EOT > /etc/sudoers.d/logging 
# Enable session logging
Defaults        log_input
Defaults        log_output
Defaults        iolog_dir=/var/log/sudo-io/%{user}

# Be politically incorrect :)
Defaults	insults
EOT
fi

#--------------------------------------------------------------------
# Protect the boot loader
if [ -s /boot/grub/grub.cfg ]
then
    chown root:root /boot/grub/grub.cfg
    chmod u-x,go-rwx /boot/grub/grub.cfg
    stat -Lc '%n access: (%a/%A) uid: ( %u/ %U) gid: ( %g/ %G)' /boot/grub/grub.cfg
fi

#--------------------------------------------------------------------
# Randomly place virtual memory regions
[ $(sysctl -n kernel.randomize_va_space) -ne 2 ] && sysctl -w kernel.randomize_va_space=2

#--------------------------------------------------------------------
# Allow only "root" to trace commands
[ $(sysctl -n kernel.yama.ptrace_scope) -ne 1 ] && sysctl -w kernel.yama.ptrace_scope=1

#--------------------------------------------------------------------
# Disable "apport" crash reporting
sed -i 's/^enabled=1/enabled=0/'  /etc/default/apport
systemctl stop apport.service
systemctl --now disable apport.service

#--------------------------------------------------------------------
# Remove "prelink" (if necessary)
dpkg-query -W -f='${binary:Package}\t${Status}\t${db:Status-Status}\n' prelink
if [ $? -eq 0 ]
then
    prelink -ua
    apt -y purge prelink
fi

#--------------------------------------------------------------------
# Remove inetd and other packages
for P in xinetd openbsd-inetd rsh-client talk nis vsftp telnet
do
    dpkg-query -W -f='${binary:Package}\t${Status}\t${db:Status-Status}\n' $P
    if [ $? -eq 0 ]
    then
        apt -y purge $P
    fi
done

#--------------------------------------------------------------------
# Disable core dumps
if [ -z "$(grep -E '^(\*|\s).*hard.*core.*(\s+#.*)?$' /etc/security/limits.conf)" ]
then
    cat << EOT >> /etc/security/limits.conf
# Disable core dumps
* hard core 0
EOT
fi
[ $(sysctl -n fs.suid_dumpable) -ne 0 ] && sysctl -w fs.suid_dumpable=0

#--------------------------------------------------------------------
# Harden /etc/issue and /etc/issue.net
for F in /etc/issue /etc/issue.net
do
    chown root:root $(readlink -e $F)
    chmod u-x,go-wx $(readlink -e $F)
done

#--------------------------------------------------------------------
if [ $IS_VIRTUAL -ne 0 ]
then
    # Disable wireless interfaces
    if command -v nmcli >/dev/null 2>&1
    then
        nmcli radio all off
    else
        if [ -n "$(find /sys/class/net/*/ -type d -name wireless)" ]
        then
            drivers=$(for driverdir in $(find /sys/class/net/*/ -type d -name wireless | xargs -0 dirname)
            do
                basename "$(readlink -f "$driverdir"/device/driver)"
            done | sort -u)
            for dm in $drivers;
            do
                echo "install $dm /bin/true" >> /etc/modprobe.d/disable_wireless.conf
            done
        fi
    fi

    # Disable bluetooth
    systemctl is-enabled bluetooth.service &> /dev/null
    if [ $? -eq 0 ]
    then
       systemctl stop bluetooth.service
       systemctl mask bluetooth.service
    fi
fi

#--------------------------------------------------------------------
# Disable some modules
for M in dccp ipv6 floppy rds tipc
do
    modprobe -n -v $M &> /dev/null
    if [ $? -eq 0 ]
    then
        echo "install $M /bin/false" > /etc/modprobe.d/blacklist-${M}.conf
        [ -z "$(grep ^$M'[[:space:]]' /proc/modules)" ] || modprobe -r $M &> /dev/null
    fi
done

#--------------------------------------------------------------------
# Disable ICMP redirects
if [ $(sysctl -n net.ipv4.ip_forward) -eq 0 ]
then
    for P in net.ipv4.conf.all.send_redirects net.ipv4.conf.default.send_redirects
    do
        sysctl -w ${P}=0
    done
fi

#--------------------------------------------------------------------
# Enable some useful network security settings
for P in net.ipv4.icmp_echo_ignore_broadcasts net.ipv4.icmp_ignore_bogus_error_responses net.ipv4.conf.all.rp_filter net.ipv4.conf.default.rp_filter net.ipv4.route.flush
do
    sysctl -w ${P}=1
done

#--------------------------------------------------------------------
# Protect cron jobs
for FD in /etc/crontab /etc/cron.hourly /etc/cron.daily /etc/cron.weekly /etc/cron.monthly /etc/cron.d
do
    chown root:root $FD
    chmod og-rwx $FD
    stat -Lc '%n access: (%a/%A) uid: ( %u/ %U) gid: ( %g/ %G)' $FD
done

#--------------------------------------------------------------------
# Protect password files
for F in /etc/passwd /etc/passwd- /etc/group /etc/group- /etc/shells
do
    chmod u-x,go-wx $F
    chown root:root $F
    stat -Lc '%n access: (%a/%A) uid: ( %u/ %U) gid: ( %g/ %G)' $F
done
SG='root'
[ -n "$(grep shadow /etc/group)" ] && SG='shadow'
for F in /etc/shadow /etc/shadow- /etc/gshadow /etc/gshadow-
do 
    chmod u-x,go-wx $F
    chown root:$SG $F
    stat -Lc '%n access: (%a/%A) uid: ( %u/ %U) gid: ( %g/ %G)' $F
done
for F in /etc/security/opasswd /etc/security/opasswd.old
do
    if [ -f $F ]
    then
        chmod u-x,go-rwx $F
        chown root:root $F
        stat -Lc '%n access: (%a/%A) uid: ( %u/ %U) gid: ( %g/ %G)' $F
    fi
done

#--------------------------------------------------------------------
# Disallow "nologin" as a valid shell
[ -n "$(grep '/nologin\b' /etc/shells)" ] && sed -i '/nologin' /etc/shells

#--------------------------------------------------------------------
# Check for any accounts without a shadow password (there should be none)
echo "Accounts without shadow passwords (there should be none):"
getent passwd | awk -F: '($2 != "x" ) { print $1 " is not set to shadowed passwords "}'

#--------------------------------------------------------------------
# Check for any empty passwords (there should be none)
echo "Empty passwords (there should be none):"
getent shadow | awk -F: '($2 == "" ) { print $1 " does not have a password "}'

#--------------------------------------------------------------------
# Check for any unreferenced user groups (there should be none)
echo "Unreferenced user groups (there should be none):"
getent passwd | cut -s -d: -f4 | sort -u | while read -r g
do
    getent group |  grep -q -P "^.*?:[^:]*:$g:"
    if [ $? -ne 0 ]
    then
        echo "Group $g is referenced by /etc/passwd but does not exist in /etc/group"
    fi
done

#--------------------------------------------------------------------
# Check for users in the "shadow" group (there should be none)
echo "Users in the 'shadow' group (there should be none):"
getent passwd | awk -F: -v GID="$(awk -F: '($1=="shadow") {print $3}' /etc/group)" '($4==GID) {print $1}'
getent group | awk -F: '($1=="shadow") {print $NF}'

#--------------------------------------------------------------------
# Check for duplicate or shared users (there should be none)
echo "Duplicate users (there should be none):"
getent passwd | cut -f3 -d":" | sort -n | uniq -c | while read -r x
do
    [ -z "$x" ] && break
    set - $x
    if [ $1 -gt 1 ]
    then
        users=$(getent passwd | awk -F: '($3 == n) { print $1 }' n=$2 | xargs)
        echo "Duplicate UID ($2): $users"
    fi
done
echo "Shared user IDs (there should be none):"
getent passwd | cut -d: -f1 | sort | uniq -d | while read -r x
do
    echo "Duplicate login name $x in /etc/passwd"
done

#--------------------------------------------------------------------
# Check for duplicate groups (there should be none)
echo "Duplicate groups (there should be none):"
getent group | cut -d: -f3 | sort | uniq -d | while read -r g
do
    echo "Duplicate GID ($g) in /etc/group"
done
getent group | cut -d: -f1 | sort | uniq -d | while read -r g
do
    echo "Duplicate group name $g in /etc/group"
done

#--------------------------------------------------------------------
# Check for "." in root's PATH
echo "Find '.' in root's PATH (there should be none):"
grep -q "::" <<< $PATH && date "+%F %T: root's path contains a empty directory (::)"
grep -q ":$" <<< $PATH && date "+%F %T: root's path contains a trailing (:)"
for x in $(tr ':' ' ' <<< $PATH)
do
    if [ -d "$x" ]
    then
        ls -ldH "$x" | awk '$9 == "." {print "PATH contains current working directory (.)"}
        $3 != "root" {print $9, "is not owned by root"}
        substr($1,6,1) != "-" {print $9, "is group writable"}
        substr($1,9,1) != "-" {print $9, "is world writable"}'
     else
        echo "$x is not a directory"
   fi
done

#--------------------------------------------------------------------
# Check for directories with sticky bits set (there should be none)
echo "Non world-writable directories with sticky bits set (there should be none):"
df --local -P | awk '{if (NR!=1) print $6}' | \
  xargs -I '{}' find '{}' -xdev -type d \( -perm -0002 -a ! -perm -1000 \) 2>/dev/null
# Set sticky bit on world-writable directories
df --local -P | awk '{if (NR!=1) print $6}' | \
  xargs -I '{}' find '{}' -xdev -type d \( -perm -0002 -a ! -perm -1000 \) 2>/dev/null | \
  xargs -I '{}' chmod a+t '{}'

#--------------------------------------------------------------------
# Install and configure "apparmor"
dpkg-query -W -f='${binary:Package}\t${Status}\t${db:Status-Status}\n' apparmor
[ $? -ne 0 ] && apt install apparmor
if [[ ! $(grep '^GRUB_CMDLINE_LINUX=' /etc/default/grub | cut -d= -f2-) =~ apparmor ]]
then
    sed -i 's/^GRUB_CMDLINE_LINUX="/GRUB_CMDLINE_LINUX="apparmor=1 security=apparmor /' /etc/default/grub
    update-grub
    REBOOT=1
fi
echo "Checking apparmor profiles - there should be at least one:"
apparmor_status | grep -E '^[1-9].*profiles.*(enforce|complain)'
echo "Checking apparmor processes - there should be at least one:"
apparmor_status | grep -E '^[1-9].*processes.*(enforce|complain)'

#--------------------------------------------------------------------
# Show system accounts with executable shells
echo 'Root accounts - there should only be one:'
getent passwd | awk -F: '($3 == 0) { print $1 }'
echo 'System accounts with executable shells - there should be none:'
getent passwd | \
  awk -F: '($1!="root" && $1!="sync" && $1!="shutdown" && $1!="halt" && $1!~/^\+/ && $3<'"$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)"' && $7!="'"$(command -v nologin)"'" && $7!="/bin/false") {print "shell for system account "$1": "$7}'
echo 'Accounts without a password - there should be none:'
getent shadow | awk -F: '($2 == "" ) { print $1 " does not have a password "}'

#--------------------------------------------------------------------
# Protect the "root" account and the local password files
usermod -g 0 -u 0 root

chown root:root /etc/passwd* /etc/group*
chmod 0644 /etc/passwd* /etc/group*

chown root:shadow /etc/shadow* /etc/gshadow*
chmod 0640 /etc/shadow* /etc/gshadow*

#--------------------------------------------------------------------
# Check for possible dangerous files
TEMP_FILE=$(mktemp /tmp/$$XXXXXXXX)
df --local -P -t ext4 | awk '{if (NR!=1) print $6}' > $TEMP_FILE
echo 'Checking for world-writable files - there should be none:'
xargs -I '{}' find '{}' -xdev -type f -perm -0002 < $TEMP_FILE
echo 'Checking for unowned files - there should be none:'
xargs -I '{}' find '{}' -xdev -nouser < $TEMP_FILE
xargs -I '{}' find '{}' -xdev -nogroup < $TEMP_FILE

echo 'Checking for SUID and GUID files - there should be some:'
xargs -I '{}' find '{}' -xdev -type f -perm -4000 -ls < $TEMP_FILE
xargs -I '{}' find '{}' -xdev -type f -perm -2000 -ls < $TEMP_FILE
rm -f $TEMP_FILE

#--------------------------------------------------------------------
# Check for possible dangerous PATH components
echo 'Checking PATH components - there should be no dangerous ones:'
if [[ $PATH =~ :: ]]
then 
    echo "Empty Directory in PATH (::)"
fi  
if [[ $PATH =~ :$ ]]
then 
    echo "Trailing : in PATH" 
fi  
for x in ${PATH//:/ }
do
    if [ -d "$x" ]
    then
        ls -ldH "$x" | awk '
$9 == "." {print "PATH contains current working directory (.)"}
$3 != "root" {print $9, "is not owned by root"}
substr($1,6,1) != "-" {print $9, "is group writable"}
substr($1,9,1) != "-" {print $9, "is world writable"}
'
    else
        echo "PATH component '$x' is not a directory or doesn't exist"
    fi
done

#--------------------------------------------------------------------
# Check home directories for users
SH_NOLOGIN=$(command -v nologin) 
SH_FALSE=$(command -v false)
TEMP_FILE=$(mktemp /tmp/$$XXXXXXXX)
getent passwd | grep -E -v '^(halt|sync|shutdown)' | \
  awk -F: '($7 != "'"$SH_NOLOGIN"'" && $7 != "'$SH_FALSE'") { print $1 " " $6 }' > $TEMP_FILE

echo 'Checking home directories - there should be no missing ones:'
while read -r user dir
do
    [ -d "$dir" ] || echo "The home directory ($dir) of user $user does not exist."
done < $TEMP_FILE

echo 'Checking home directory permissions:'
while read user dir
do
  if [ ! -d "$dir" ]
  then
    echo "The home directory ($dir) of user $user does not exist."
  else
    dirperm=$(ls -ld $dir | cut -f1 -d" ")
    if [ $(echo $dirperm | cut -c6) != "-" ]
    then
      echo "Group Write permission set on the home directory ($dir) of user $user"
    fi
    if [ $(cut -c8 <<< $dirperm) != "-" ]
    then
      echo "Other Read permission set on the home directory ($dir) of user $user"
    fi
    if [ $(cut -c9 <<< $dirperm) != "-" ]
    then
      echo "Other Write permission set on the home directory ($dir) of user $user"
    fi
    if [ $(cut -c10  <<< $dirperm) != "-" ]
    then
      echo "Other Execute permission set on the home directory ($dir) of user $user"
    fi
  fi
done < $TEMP_FILE

echo 'Checking home directory ownerships:'
while read user dir
do
    if [ ! -d "$dir" ]
    then
        echo "The home directory ($dir) of user $user does not exist."
    else
        owner=$(stat -L -c "%U" "$dir")
        if [ "$owner" != "$user" ]
        then
            echo "The home directory ($dir) of user $user is owned by $owner."
        fi
    fi
done < $TEMP_FILE

echo 'Checking permissions on .dot files in home directories:'
while read user dir
do
    if [ ! -d "$dir" ]
    then
        echo "The home directory ($dir) of user $user does not exist."
    else
        for file in $dir/.[A-Za-z0-9]*
        do
            if [ ! -h "$file" -a -f "$file" ]
            then
                fileperm=$(ls -ld $file | cut -f1 -d" ")

                if [ $(cut -c6 <<< $fileperm)  != "-" ]
                then
                    echo "Group Write permission set on file $file"
                fi
                if [ $(cut -c9 <<< $fileperm)  != "-" ]
                then
                    echo "Other Write permission set on file $file"
                fi
            fi
        done
    fi
done < $TEMP_FILE

echo 'Checking for .netrc and .rhosts in home directories:'
while read user dir
do
    if [ ! -d "$dir" ]
    then
        echo "The home directory ($dir) of user $user does not exist."
    else
        [ ! -h "$dir/.netrc" -a -f "$dir/.netrc" ] && echo ".netrc file $dir/.netrc exists"
        [ ! -h "$dir/.rhosts" -a -f "$dir/.rhosts" ] && echo ".rhosts file $dir/.rhosts exists"
    fi
done < $TEMP_FILE

rm -f $TEMP_FILE

echo 'Checking for missing groups in group file:'
for i in $(getent passwd | cut -s -d: -f4 | sort -u )
do
    getent group | grep -q -P "^.*?:[^:]*:$i:"
    if [ $? -ne 0 ]
    then
        echo "Group $i is referenced by password file but does not exist in group file"
    fi
done

echo 'Checking for duplicate user IDs:'
getent passwd | cut -f3 -d":" | sort -n | uniq -c | \
while read x 
do
    [ -z "$x" ] && break
    set - $x
    if [ $1 -gt 1 ]
    then
        users=$(awk -F: '($3 == n) { print $1 }' n=$2 /etc/passwd | xargs)
        echo "Duplicate UID ($2): $users"
    fi
done

echo 'Checking for duplicate user names:'
getent passwd | cut -d: -f1 | sort | uniq -d | \
while read x
do
    echo "Duplicate user name ${x} in password file"
done

echo 'Checking for duplicate group IDs:'
getent group | cut -d: -f3 | sort | uniq -d | \
while read x
do
    echo "Duplicate GID ($x) in group file"
done

echo 'Checking for duplicate group names:'
getent group | cut -d: -f1 | sort | uniq -d | \
while read x
do
    echo "Duplicate group name ${x} in group file"
done

#--------------------------------------------------------------------
# Check for possible bad "umask"
if [ ! -z "$(grep ^UMASK /etc/login.defs)" ]
then
    sed -i 's/^UMASK.*/UMASK 027/g' /etc/login.defs
else
    echo 'UMASK 027' >> /etc/login.defs
fi
UMASK_OVERRIDE=0
grep -Eiq '^\s*UMASK\s+(0[0-7][2-7]7|[0-7][2-7]7)\b' /etc/login.defs && grep -Eq '^\s*session\s+(optional|requisite|required)\s+pam_umask\.so\b' /etc/pam.d/common-session && UMASK_OVERRIDE=1
grep -REiq '^\s*UMASK\s+\s*(0[0-7][2-7]7|[0-7][2-7]7|u=(r?|w?|x?)(r?|w?|x?)(r?|w?|x?),g=(r?x?|x?r?),o=)\b' /etc/profile* /etc/bash.bashrc* && UMASK_OVERRIDE=1
[ $UMASK_OVERRIDE -ne 0 ] && echo 'Possible issue: Default user umask is overwritten'

#--------------------------------------------------------------------
# Force the use of "su" (needs testing)
#[ -z "$(getent group sugroup)" ] && groupadd sugroup
#[ -z "$(grep '^auth required pam_wheel.so use_uid group=sugroup' /etc/pam.d/su)" ] && echo 'auth required pam_wheel.so use_uid group=sugroup' >> /etc/pam.d/su

#--------------------------------------------------------------------
# Auditing and logging
for P in auditd audispd-plugins
do
    dpkg-query -W -f='${binary:Package}\t${Status}\t${db:Status-Status}\n' $P
    [ $? -ne 0 ] && apt install $P
done
[[ $(systemctl is-enabled auditd) =~ enabled ]] || systemctl --now enable auditd
echo "Checking audit time change rules - there should be at least one:"
grep time-change /etc/audit/rules.d/*.rules | grep -v '#'
auditctl -l | grep time-change
echo "Checking audit identity change rules - there should be at least one:"
grep identity /etc/audit/rules.d/*.rules | grep -v '#'
auditctl -l | grep identity
echo "Checking audit system change rules - there should be at least one:"
grep system-locale /etc/audit/rules.d/*.rules | grep -v '#'
auditctl -l | grep system-locale
echo "Checking audit MAC change rules - there should be at least one:"
grep MAC-policy /etc/audit/rules.d/*.rules | grep -v '#'
auditctl -l | grep MAC-policy
echo "Checking audit permission change rules - there should be at least one:"
grep perm_mod /etc/audit/rules.d/*.rules | grep -v '#'
auditctl -l | grep perm_mod
echo "Checking audit access rules - there should be at least one:"
grep access /etc/audit/rules.d/*.rules | grep -v '#'
auditctl -l | grep access
echo "Checking audit sudoers rules - there should be at least one:"
grep sudoers /etc/audit/rules.d/*.rules | grep -v '#'
auditctl -l | grep sudoers
echo "Checking audit module load/unload rules - there should be at least one:"
grep modules /etc/audit/rules.d/*.rules | grep -v '#'
auditctl -l | grep modules

# Monitor privileged programs
find / -xdev \( -perm -4000 -o -perm -2000 \) -type f | \
  awk '{print "-a always,exit -F path=" $1 " -F perm=x -F auid>='"$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)"' -F auid!=4294967295 -k privileged" }' >> /etc/audit/rules.d/32-privileged.rules

# Monitor mounts
cat << EOT > /etc/audit/rules.d/33-mounts.rules
-a always,exit -F arch=b64 -S mount -F auid>=1000 -F auid!=4294967295 -k mounts
-a always,exit -F arch=b32 -S mount -F auid>=1000 -F auid!=4294967295 -k mounts
EOT

# Monitor deletions
cat << EOT > /etc/audit/rules.d/33-deletions.rules
-a always,exit -F arch=b64 -S unlink -S unlinkat -S rename -S renameat -F auid>=1000 -F auid!=4294967295 -k delete
-a always,exit -F arch=b32 -S unlink -S unlinkat -S rename -S renameat -F auid>=1000 -F auid!=4294967295 -k delete
EOT

# Make configuration immutable
sed -i 's/^#-e/-e/' /etc/audit/rules.d/99-finalize.rules

#--------------------------------------------------------------------
# Send "journald" messages to rsyslog
if [ -z "$(grep ^ForwardToSyslog /etc/systemd/journald.conf)" ]
then
    echo 'ForwardToSyslog=yes' >> /etc/systemd/journald.conf
    systemctl restart systemd-journald
fi
# Let "journald" compress large logs
if [ -z "$(grep ^Compress /etc/systemd/journald.conf)" ]
then
    echo 'Compress=yes' >> /etc/systemd/journald.conf
    systemctl restart systemd-journald
fi
# Have "journald" write to disk
if [ -z "$(grep ^Storage /etc/systemd/journald.conf)" ]
then
    echo 'Storage=yes' >> /etc/systemd/journald.conf
    systemctl restart systemd-journald
fi

# Protect logs (only privileged users can access)
chown root:root /etc/cron.{hourly,daily,weekly,monthly,d}
find /var/log -type f -exec chmod g-wx,o-rwx "{}" \; &
find /var/log -type d -exec chmod g-w,o-rwx "{}" \; &
wait
chmod 0755 /var/log

#--------------------------------------------------------------------
# Protect system-wide ssh keys etc.
find /etc/ssh -xdev -type f -name 'ssh_host_*_key*' | xargs chown root:root &
find /etc/ssh -xdev -type f -name 'ssh_host_*_key' | xargs chmod 0600 &
find /etc/ssh -xdev -type f -name 'ssh_host_*_key.pub' | xargs chmod 0644 &
wait

# Restrict the number of ssh authentication attempts
if [ $(sshd -T 2> /dev/null | awk '/maxauthtries/{print $NF}') -gt 3 ]
then
    sed -i 's/#MaxAuthTries/MaxAuthTries/;s/MaxAuthTries.*/MaxAuthTries 3/' /etc/ssh/sshd_config
    service ssh restart
fi
# Enable timeout for SSH sessions
if [ $(sshd -T 2> /dev/null | awk '/clientalivecountmax/{print $NF}') -ne 3 ]
then
    sed -i 's/#ClientAliveCountMax/ClientAliveCountMax/;s/ClientAliveCountMax.*/ClientAliveCountMax 3/' /etc/ssh/sshd_config
    service ssh restart
fi
if [ $(sshd -T 2> /dev/null | awk '/clientaliveinterval/{print $NF}') -ne 300 ]
then
    sed -i 's/#ClientAliveInterval/ClientAliveInterval/;s/ClientAliveInterval.*/ClientAliveInterval 300/' /etc/ssh/sshd_config
    service ssh restart
fi
# Enable IgnoreRhosts for ssh
if [ ! "T$(sshd -T 2> /dev/null | awk '/ignorerhosts/{print $NF}')" = 'Tyes' ]
then
    sed -i 's/#IgnoreRhosts/IgnoreRhosts/;s/IgnoreRhosts.*/IgnoreRhosts yes/' /etc/ssh/sshd_config
    service ssh restart
fi
# Disable host based authentication for ssh
if [ ! "T$(sshd -T 2> /dev/null | awk '/hostbasedauthentication /{print $NF}')" = 'Tno' ]
then
    sed -i 's/#HostbasedAuthentication/;s/HostbasedAuthentication.*/HostbasedAuthentication no/' /etc/ssh/sshd_config
    service ssh restart
fi
# Disable authentication for ssh with an empty password
if [ ! "T$(sshd -T 2> /dev/null | awk '/permitemptypasswords/{print $NF}')" = 'Tno' ]
then
    sed -i 's/#PermitEmptyPasswords/;s/PermitEmptyPasswords.*/PermitEmptyPasswords no/' /etc/ssh/sshd_config
    service ssh restart
fi
# Disable custom environment for ssh
if [ ! "T$(sshd -T 2> /dev/null | awk '/permituserenvironment/{print $NF}')" = 'Tno' ]
then
    sed -i 's/#PermitUserEnvironment/;s/PermitUserEnvironment.*/PermitUserEnvironment no/' /etc/ssh/sshd_config
    service ssh restart
fi
# Enable root login ssh only with SSH key
if [ ! "T$(sshd -T 2> /dev/null | awk '/permitrootlogin/{print $NF}')" = 'Twithout-password' ]
then
    sed -i 's/#PermitRootLogin/PermitRootLogin/;s/PermitRootLogin .*/PermitRootLogin without-password/' /etc/ssh/sshd_config
    service ssh restart
fi
# Strong, secure ciphers and keys in "/etc/ssh/sshd_config" (for openssh => 6.x):
[ -z "$(grep '^KexAlgo' /etc/ssh/sshd_config)" ] && echo 'KexAlgorithms curve25519-sha256@libssh.org,ecdh-sha2-nistp521,ecdh-sha2-nistp384,ecdh-sha2-nistp256,diffie-hellman-group-exchange-sha256' >> /etc/ssh/sshd_config
[ -z "$(grep '^Ciphers' /etc/ssh/sshd_config)" ] &&  echo 'Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr' >> /etc/ssh/sshd_config
[ -z "$(grep '^MACs' /etc/ssh/sshd_config)" ] && echo 'MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,umac-128@openssh.com' >> /etc/ssh/sshd_config

#--------------------------------------------------------------------
# Check files for any modifications from defaults
read -t 10 -p 'Check modifications of default file settings (this takes a while) [y/N] ? ' YN
if [ "T${YN^^}" = 'TY' ]
then
    for P in $(dpkg -l | awk '/^i/{print $2}')
    do
        dpkg --verify $P
    done
fi

#--------------------------------------------------------------------
# Reboot if necessary
[ $NO_REBOOT -eq 0 -a $REBOOT -ne 0 ] && shutdown -r +1 Rebooting after CIS hardening

#--------------------------------------------------------------------
# We are done
exit 0
