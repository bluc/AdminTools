#!/usr/bin/perl -Tw
# Version 20240215-121716 checked into repository
#--------------------------------------------------------------------
# (c) CopyRight 2019 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#--------------------------------------------------------------------
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/CheckZimbraClamd.pl
use 5.0010;

# Constants
my $ProgName = '';
( $ProgName = $0 ) =~ s%.*/%%;

# Common sense options:
use strict qw(vars subs);
no warnings;
use warnings qw(FATAL closed threads internal debugging pack
    portable prototype inplace io pipe unpack malloc
    glob digit printf layer reserved taint closure
    semicolon);
no warnings qw(exec newline unopened);

#--------------------------------------------------------------------
# Needed packages
#--------------------------------------------------------------------
use Getopt::Std;
use Sys::Syslog qw(:macros :standard);

#--------------------------------------------------------------------
# Globals
#--------------------------------------------------------------------
$ENV{PATH} = '/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin';

# Program options
our $opt_d = 0;
our $opt_h = 0;

# Other internal globals
my $LogFile;
my %File2Delete  = ();
my $RestartClamd = 0;

#--------------------------------------------------------------------
# Display the usage
#--------------------------------------------------------------------
sub ShowUsage()
{

    print "Usage: $ProgName [options]\n",
        "       -h   Show this help [default=no]\n",
        "       -d   Show some debug info on STDERR [default=no]\n";
    exit 0;
} ## end sub ShowUsage

#--------------------------------------------------------------------
# Get the program options
getopts('dh') or ShowUsage();
ShowUsage() if ($opt_h);
if ( $> != 0 )
{
    print "$ProgName must run as 'root'\n";
    ShowUsage();
} ## end if ( $> != 0 )

#--------------------------------------------------------------------
# No action necessary if this is NOT a Zimbra server
die "ERROR: Not a Zimbra server\n" unless ( -d '/opt/zimbra/log' );

openlog "$ProgName", 'ndelay,pid', 'mail';

# No action necessary if clamd is running
my @lines = ();
if ( open( my $ClamdRunning, '-|', 'ps -wefH' ) )
{
    chomp( @lines = grep {/clamd/} <$ClamdRunning> );
    close($ClamdRunning);
} ## end if ( open( my $ClamdRunning...))
if ( scalar(@lines) )
{
    syslog 5, 'clamd is already running';
    warn "'clamd' is already running\n" if ($opt_d);
    my $rc
        = system(
        '/opt/zimbra/common/bin/clamdscan /etc/passwd > /dev/null 2>&1');
    exit 0 if ( $rc == 0 );
    syslog 5, 'clamd is not running well ... keep checking';
    warn "'clamd' is not running well ... keep checking\n"
        if ($opt_d);
} ## end if ( scalar(@lines) )

#--------------------------------------------------------------------
# Find virus definition files that cause problems
@lines = ();
if (open(
        $LogFile,
        '-|',
        'grep "Warning.*failed to parse" /opt/zimbra/log/clamd.log | tail -n 200'
    )
    )
{
    chomp( @lines = <$LogFile> );
    close($LogFile);
    foreach my $Line (@lines)
    {
        warn "DEBUG: Warning line = '$Line'\n" if ($opt_d);
        if ( $Line =~ m@from\s+file\s(\S+),@o )
        {
            my $DelFile = "$1";
            unless ( $File2Delete{"$DelFile"} )
            {
                warn "DEBUG: File to delete: $DelFile\n" if ($opt_d);
                syslog 5, "AV File to delete %s", $DelFile;
                $File2Delete{"$DelFile"}++;
            } ## end unless ( $File2Delete{"$DelFile"...})
            next;
        } ## end if ( $Line =~ m@from\s+file\s(\S+),@o...)
        if ( $Line =~ m@The\s+(\S+)\s+database\s+is\s+older@o )
        {
            my $DelFile = "$1";
            unless ( $File2Delete{"$DelFile"} )
            {
                warn "DEBUG: File to delete: $DelFile\n" if ($opt_d);
                syslog 5, "AV File to delete %s", $DelFile;
                $File2Delete{"$DelFile"}++;
            } ## end unless ( $File2Delete{"$DelFile"...})
            next;
        } ## end if ( $Line =~ m@The\s+(\S+)\s+database\s+is\s+older@o...)
    } ## end foreach my $Line (@lines)
} ## end if ( open( $LogFile, '-|'...))

#--------------------------------------------------------------------
# Now delete all files that we found (if any)
foreach my $DelFile ( keys %File2Delete )
{
    warn "DEBUG: Delete file '$DelFile'\n" if ($opt_d);
    system("rm -f $DelFile") and syslog 5, "Delete AV File %s", $DelFile;
    $RestartClamd++;
} ## end foreach my $DelFile ( keys ...)

#--------------------------------------------------------------------
# Ensure that the config file has the right permissions
chmod 0644, '/opt/zimbra/common/etc/clamd.conf';

#--------------------------------------------------------------------
# Finally restart clamd (if necessary)
if ($RestartClamd)
{
    my $RestartCmd = "su - zimbra -c '/opt/zimbra/bin/zmclamdctl restart'";
    warn "DEBUG: Restarting 'clamd' via '$RestartCmd'\n" if ($opt_d);
    syslog 5, 'Restarting clamd';
    system("$RestartCmd");
} ## end if ($RestartClamd)

#--------------------------------------------------------------------
# We are done
closelog;
exit 0;
__END__
