#!/bin/bash
################################################################
# (c) Copyright 2013-2018 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/MirrorBootDrive.sh

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Also don't run more than one instance of "ddrescue"
pgrep ddrescue &> /dev/null && exit 0

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            

TFILE=$(mktemp /tmp/$$XXXXXXXX)

QUIET='-q'
[[ $- ==  *i* ]] && QUIET=''

# Adapt the source and destination
# (and make sure that the destination drive is
#  the same size or bigger than the source drive)
SRC_DRIVE=${1-sda}
if [ ! -b /dev/$SRC_DRIVE ]
then
    echo "Source drive '$SRC_DRIVE' doesn't exist"
    exit 1
fi
DEST_DRIVE=${2-sdb}
if [ ! -b /dev/$DEST_DRIVE ]
then
    echo "Destination drive '$DEST_DRIVE' doesn't exist"
    exit 1
fi
if [ "T$SRC_DRIVE" = "T$DEST_DRIVE" ]
then
    echo "Source and destination drives are the same"
    exit 1
fi

# Install "ddrescue" if necessary
dpkg-query -S gddrescue &> /dev/null
[ $? -ne 0 ] && apt-get -y install gddrescue

# Finally mirror the drive ...
ddrescue $QUIET -d -f -c 2048 -r 3 /dev/$SRC_DRIVE /dev/$DEST_DRIVE 2> $TFILE
RETCODE=$?
if [ $RETCODE -eq 0 ]
then
    echo "Successfully copied /dev/$SRC_DRIVE to /dev/$DEST_DRIVE"
else
    echo "Problem copying /dev/$SRC_DRIVE to /dev/$DEST_DRIVE ($RETCODE):"
    cat $TFILE
fi
echo "Current partition table on /dev/$DEST_DRIVE:"
fdisk -l /dev/$DEST_DRIVE

# We are done
exit $RETCODE
