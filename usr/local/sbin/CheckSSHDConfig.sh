#!/bin/bash
# Version 20240119-085648 checked into repository
#--------------------------------------------------------------------
# (c) CopyRight 2017 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#--------------------------------------------------------------------

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# This needs to run as "root"
if [ ! "T$(/usr/bin/id -u)" = 'T0' ]
then
    echo "$PROG needs to run as 'root'"
    exit 1
fi

# Make sure we remove temporary file at exit
trap "rm -f /tmp/$$* $HOME/$$*" EXIT

#--------------------------------------------------------------------
# Globals
SSHD_CONF=/tmp/$$.sshd_config
sshd -T > $SSHD_CONF

#--------------------------------------------------------------------
# Check SSHD for some reasonable settings
if [ ! -z "$(grep -i '^Protocol' $SSHD_CONF | grep 1)" ]
then
    echo "Disable protocol '1' in $SSHD_CONF"
fi
if [ ! -z "$(grep -i '^PermitRootLogin' $SSHD_CONF | grep -i yes)" ]
then
    echo "Allow root logins only 'with-password'"
fi
if [ -z "$(grep -i '^Allow' $SSHD_CONF)" ]
then
    echo "Create a new group 'sshusers' and allow only its members SSH access"
fi
SKB=$(awk 'tolower($0) ~ /^ServerKeyBits/ {print $NF}' $SSHD_CONF)
if [ -z $SKB ]
then
    echo "Set the key strength for 'ServerKeyBits' to 1024 or higher"
elif [ $SKB -lt 1024 ]
then
    echo "Increase the key strength for 'ServerKeyBits' to 1024 or higher"
fi
cat << EOT

This requires recreating existing host keys and restarting sshd:

rm -f /etc/ssh/ssh*host*key*
ssh-keygen -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa
ssh-keygen -f /etc/ssh/ssh_host_dsa_key -N '' -t dsa

/etc/init.d/ssh restart - or: systemctl restart ssh

EOT
if [ -z "$(grep -i '^IgnoreRhosts' $SSHD_CONF | grep -i yes)" ]
then
    echo "Enable 'IgnoreHosts'"
fi
for P in RhostsRSAAuthentication HostbasedAuthentication PermitEmptyPasswords
do
    if [ -z "$(grep -i ^$P $SSHD_CONF | grep -i no)" ]
    then
        echo "Disable '$P'"
    fi
done

#--------------------------------------------------------------------
# We are done
exit 0
