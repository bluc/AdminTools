#!/bin/bash
# Version 20221116-075907 checked into repository
################################################################
# (c) Copyright 2022 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/concretecms.postinstall.sh

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -rf $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE

DEBUG='-q'
[[ $- ==  *x* ]] && DEBUG='-v'

#--------------------------------------------------------------------
# We need this for custom repositories
[ -x /usr/bin/apt-add-repository ] || apt-get install software-properties-common
UBUNTU_REL=$(lsb_release -cs)

#--------------------------------------------------------------------
# Install mariadb
[ -z "$(apt-key list 2> /dev/null | grep Maria)" ] && apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
[ -z "$(grep -m1 mariadb /etc/apt/sources.list)" ] && add-apt-repository -y "deb [arch=amd64] http://mirror.its.dal.ca/mariadb/repo/10.6/ubuntu $UBUNTU_REL main"
[ -x /usr/bin/mariadb ] || apt-get install mariadb-server

#--------------------------------------------------------------------
# Install php (also installs apache2)
[ -s /etc/apt/sources.list.d/ondrej-ubuntu-apache2-${UBUNTU_REL}.list ] || LC_ALL=C.UTF-8 add-apt-repository -y ppa:ondrej/apache2
[ -s /etc/apt/sources.list.d/ondrej-ubuntu-php-${UBUNTU_REL}.list ] || LC_ALL=C.UTF-8 add-apt-repository -y ppa:ondrej/php

[ -x /usr/bin/php ] || apt-get install php php-mysql php-gd php-pecl-http php-zip php-xml php-intl php-mbstring php-curl php-raphf

#TBD## Install PHP composer
#TBD#curl -sS https://getcomposer.org/installer | php
#TBD#cmp composer.phar /usr/local/bin/composer &> /dev/null
#TBD#[ $? -ne 0 ] && mv composer.phar /usr/local/bin/composer
#TBD#chmod +x /usr/local/bin/composer
#TBD#composer self-update

#TBD##--------------------------------------------------------------------
#TBD## Setup the database for Concrete CMS
#TBD#if [ -s /var/tmp/cms_dbpass ]
#TBD#then
#TBD#    CMS_DBPASS=$(< /var/tmp/cms_dbpass)
#TBD#else
#TBD#    CMS_DBPASS=$(LC_ALL=C tr -dc 'A-Za-z0-9!' </dev/urandom | head -c 16)
#TBD#    echo "$CMS_DBPASS" > /var/tmp/cms_dbpass
#TBD#    chmod 644 /var/tmp/cms_dbpass
#TBD#fi
#TBD#mysql << EOT
#TBD#CREATE DATABASE IF NOT EXISTS concretedb;
#TBD#GRANT ALL PRIVILEGES ON concretedb.* TO 'concreteuser'@'localhost' IDENTIFIED BY '$CMS_DBPASS';
#TBD#FLUSH PRIVILEGES;
#TBD#EOT

#TBD##--------------------------------------------------------------------
#TBD## Install Concrete CMS
#TBD#mkdir -p /tmp/$$
#TBD#cd /tmp/$$
#TBD#git clone https://github.com/concretecms/concretecms.git
#TBD#cd concretecms/
#TBD#composer install

#TBD## Archive the relevant files
#TBD#tar -cv --exclude-vcs --exclude-vcs-ignore -f /tmp/ccms.tar \
#TBD#  application/ concrete/ packages/ updates/ \
#TBD#  composer.lock composer.json robots.txt LICENSE.TXT index.php
#TBD## Save the current archive
#TBD#[ -s /usr/local/ccms.tar ] && mv /usr/local/ccms.tar /usr/local/ccms.tar.$(date +%s)
#TBD#cp /tmp/ccms.tar /usr/local/src

#TBD## Don't overwrite the current vesion unless explicetely allowed
#TBD#INSTALL_CMS=0
#TBD#if [ -d /var/www/html/concrete ]
#TBD#then
#TBD#    read -p 'Overwrite existing installation [y/N] ?' YN
#TBD#    [ -z "$YN" ] && YN='N'
#TBD#    [ "${YN^^}" = 'Y' ] && INSTALL_CMS=1
#TBD#else
#TBD#    INSTALL_CMS=1
#TBD#fi
#TBD#if [ $INSTALL_CMS -ne 0 ]
#TBD#then
#TBD#    # Extract the relevant files from the archive
#TBD#    mkdir -p /var/www/ccms
#TBD#    tar --directory=/var/www/cms -xvf /usr/local/ccms.tar
#TBD#fi

#TBD## Setup the virtual host for apache
#TBD#VH=''
#TBD#while [ -z "$VH" ]
#TBD#do
#TBD#    read -p 'Name of the virtual host ? ' VH
#TBD#done
#TBD#cat << EOT > /etc/apache2/sites-available/concrete.conf
#TBD#<VirtualHost *:80>
#TBD#     # Adapt this:
#TBD#     ServerAdmin ateam@digitaltowpath.org
#TBD#     DocumentRoot /var/www/html/cms
#TBD#     ServerName $VH
#TBD#
#TBD#     <Directory /var/www/html/cmd>
#TBD#          Options FollowSymlinks
#TBD#          AllowOverride All
#TBD#          Require all granted
#TBD#     </Directory>
#TBD#
#TBD#     ErrorLog ${APACHE_LOG_DIR}/${VH}_error.log
#TBD#     CustomLog ${APACHE_LOG_DIR}/${VH}_access.log combined
#TBD#
#TBD#</VirtualHost>
#TBD#EOT
#TBD#chown -R www-data:www-data /var/www/html/cms
#TBD#chmod -R 755 /var/www/html/cms
#TBD#a2ensite concrete
#TBD#a2enmod rewrite
#TBD#systemctl restart apache2
#TBD#
#TBD#if [ $INSTALL_CMS -ne 0 ]
#TBD#then
#TBD#    LOCALIF=$(ip route get 8.8.8.8 | awk '/via/{print $5; exit}')
#TBD#    DEFAULT_GW=$(ip route get 8.8.8.8 | awk '/via/{print $3}')
#TBD#    LOCALIP=$(ifconfig $LOCALIF | sed -n "s/.*inet addr:\([0-9.]*\).*/\1/p")
#TBD#    [ -z "$LOCALIP" ] && LOCALIP=$(ifconfig $LOCALIF | sed -n "s/.*inet \([0-9.]*\).*/\1/p")
#TBD#    [ -z "$LOCALIP" ] && LOCALIP=$(ip addr list $LOCALIF | sed -n "s/.*inet \([0-9.]*\).*/\1/p")
#TBD#
#TBD#    cat << EOT
#TBD#Now configure the Concrete CMS system by opening a browser window to
#TBD#http://$VH
#TBD#
#TBD#PS: This only works if you have a DNS entry for "$VH" pointing to this IP address:
#TBD#    $LOCALIP
#TBD#EOT
#TBD#fi

#--------------------------------------------------------------------
# We are done
exit 0
