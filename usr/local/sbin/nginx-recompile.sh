#!/bin/bash
# Version 20240208-112029 checked into repository
################################################################
# (c) Copyright 2018 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/nginx-recompile.sh
# Based on https://www.howtoforge.com/tutorial/how-to-install-nginx-with-brotli-compression-on-ubuntu-1804/

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE

TFILE=$(mktemp /tmp/$$XXXXXXXX)

#--------------------------------------------------------------------
# Process options
FORCE_BUILD=0
while getopts f OPTION
do
    case ${OPTION} in
    f)  FORCE_BUILD=1
        ;;
    *)  echo "Usage: $0 [-f]"
        echo "       -f Force a build"
        ;;
    esac
done
shift $((OPTIND - 1))

#--------------------------------------------------------------------
# Check the currently installed version
if [ $FORCE_BUILD -eq 0 ]
then
    BUILD_NGINX=0
    nginx -V &> $TFILE
    if [ -z "$(grep ngx_brotli $TFILE)" ]
    then
        # Current version doesn't have "brotli" installed
        BUILD_NGINX=1
    elif [ -z "$(grep geoip2 $TFILE)" ]
    then
        # Current version doesn't have "geoip2" installed
        BUILD_NGINX=1
    elif [ -z "$(grep headers-more $TFILE)" ]
    then
        # Current version doesn't have "headers-more" installed
        BUILD_NGINX=1
    elif [ -z "$(grep ModSecurity $TFILE)" ]
    then
        # Current version doesn't have "modsecurity" installed
        BUILD_NGINX=1
    else
        CUR_VERSION=$(awk '/^nginx/{print $NF}' $TFILE)
        DN_VERSION=$(awk '/define NGINX_VERSION/{gsub(/"/,"");print "nginx/"$NF; exit}' /usr/local/src/nginx-*/src/core/nginx.h)
        # Current version is different (= older) than the downloaded one
        [ "T$CUR_VERSION" = "T$DN_VERSION" ] || BUILD_NGINX=1
    fi
    [ $BUILD_NGINX -eq 0 ] && exit 0
fi

#--------------------------------------------------------------------
# Get the necessary utilities for a build
apt-get -y install dpkg-dev build-essential zlib1g-dev libpcre3 libpcre3-dev \
  unzip git libgeoip-dev libtool autoconf cmake

#--------------------------------------------------------------------
# Use the official nginx repository
REL=$(lsb_release -sc)
if [ -s /etc/apt/sources.list.d/nginx.list ]
then
    [ -z "$(grep ^deb-src /etc/apt/sources.list.d/nginx.list)" ] && echo "deb-src http://nginx.org/packages/mainline/ubuntu/ ${REL} nginx" >> /etc/apt/sources.list.d/nginx.list
else
    wget --header='Accept-Encoding: gzip' https://nginx.org/keys/nginx_signing.key | sudo apt-key add -
    cat << EOT > /etc/apt/sources.list.d/nginx.list
deb http://nginx.org/packages/mainline/ubuntu/ ${REL} nginx
deb-src http://nginx.org/packages/mainline/ubuntu/ ${REL} nginx
EOT
fi
apt-get -y update

#--------------------------------------------------------------------
# Download the nginx source
cd /usr/local/src || exit
rm -rf nginx*
# For now stick with version 1.24 for compatibility with modsecurity
#apt-get -y source nginx=1.21.4-1
apt-get -y source nginx

#--------------------------------------------------------------------
# Build nginx dependencies
apt-get -y build-dep nginx

#--------------------------------------------------------------------
# Get the "nginx_cookie_flag_module" module
# See https://github.com/AirisX/nginx_cookie_flag_module
wget -O /tmp/nginx_cookie_flag_module.zip \
  https://github.com/AirisX/nginx_cookie_flag_module/archive/refs/heads/master.zip
unzip -od /usr/local/src /tmp/nginx_cookie_flag_module.zip
mv -f /usr/local/src/nginx_cookie_flag_module-master /usr/local/src/nginx_cookie_flag_module

#--------------------------------------------------------------------
# Get the "geoip2" module
if [ -z "$(grep maxmind /etc/apt/sources.list.d/*list)" ]
then
    add-apt-repository ppa:maxmind/ppa
    apt-get -y update
fi
apt-get -y install libmaxminddb0 libmaxminddb-dev mmdb-bin

cd /usr/local/src || exit
rm -rf ngx_http_geoip2*
git clone --recursive https://github.com/leev/ngx_http_geoip2_module

#--------------------------------------------------------------------
# Get the "modsecurity" library and connector
cd /usr/local/src || exit
rm -rf ModSecurity*
git clone --depth 1 -b v3/master --single-branch https://github.com/SpiderLabs/ModSecurity
git clone --depth 1 --single-branch https://github.com/SpiderLabs/ModSecurity-nginx.git

# Actually build the library (this takes a while)
cd ModSecurity || exit
git submodule init
git submodule update
./build.sh
./configure
make
make install

# Get the CRS rule set for modsecurity
cd /usr/local/src || exit
rm -rf coreruleset
git clone --depth 1 --single-branch https://github.com/coreruleset/coreruleset.git

# Create the configuration for the log rotation
if [ -d /etc/logrotate.d ]
then
    if [ ! -s /etc/logrotate.d/modsec ]
    then
        cat << EOT > /etc/logrotate.d/modsec
/var/log/modsec*.log {
        daily
        missingok
        rotate 52
        compress
        delaycompress
        notifempty
        create 644 root root
        sharedscripts
        postrotate
                if [ -f /var/run/nginx.pid ]; then
                        kill -USR1 \`cat /var/run/nginx.pid\`
                fi
        endscript
}
EOT
    fi
fi

#--------------------------------------------------------------------
# Get the "brotli" module
cd /usr/local/src || exit
rm -rf ngx_brotli*
git clone --recursive https://github.com/google/ngx_brotli.git

# From the README file:
cd ngx_brotli/deps/brotli || exit
mkdir out && cd out || exit
cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_SHARED_LIBS=OFF -DCMAKE_C_FLAGS="-Ofast -m64 -march=native -mtune=native -flto -funroll-loops -ffunction-sections -fdata-sections -Wl,--gc-sections" -DCMAKE_CXX_FLAGS="-Ofast -m64 -march=native -mtune=native -flto -funroll-loops -ffunction-sections -fdata-sections -Wl,--gc-sections" -DCMAKE_INSTALL_PREFIX=./installed ..
cmake --build . --config Release --target brotlienc

#--------------------------------------------------------------------
# Get the "headers_more" module
cd /usr/local/src || exit
rm -rf headers-more-nginx-module
git clone --recursive https://github.com/openresty/headers-more-nginx-module.git

#--------------------------------------------------------------------
# Adapt the debian "rules" file
find /usr/local/src/nginx-[0-9]* -type f -name "rules" | while read -r RF
do
    # "nginx_cookie_flag_module" module
    [ -z "$(grep nginx_cookie_flag_module $RF)" ] && sed -i 's@--with-cc-opt@--add-module=/usr/local/src/nginx_cookie_flag_module --with-cc-opt@g' $RF
    # "geoip2" module
    [ -z "$(grep geoip2 $RF)" ] && sed -i 's@--with-cc-opt@--add-module=/usr/local/src/ngx_http_geoip2_module --with-cc-opt@g' $RF
    # "brotli" module
    [ -z "$(grep ngx_brotli $RF)" ] && sed -i 's@--with-cc-opt@--add-module=/usr/local/src/ngx_brotli --with-cc-opt@g' $RF
    # "headers_more" module
    [ -z "$(grep headers-more-nginx-module $RF)" ] && sed -i 's@--with-cc-opt@--add-module=/usr/local/src/headers-more-nginx-module --with-cc-opt@g' $RF
    # "stub_status" module
    [ -z "$(grep http_stub_status_module $RF)" ] && sed -i 's@--with-cc-opt@--with-http_stub_status_module --with-cc-opt@g' $RF
    # "modsecurity" module
    [ -z "$(grep modsecurity $RF)" ] && sed -i 's@--with-cc-opt@--add-module=/usr/local/src/ModSecurity-nginx --with-cc-opt@g' $RF
    [ -z "$(grep ^override_dh_shlibdeps: $RF)" ] && cat << EOT >> $RF
override_dh_shlibdeps:
	dh_shlibdeps --dpkg-shlibdeps-params=--ignore-missing-info
EOT
done

#--------------------------------------------------------------------
# Finally build nginx (incl. extra modules)
cd /usr/local/src/nginx-[0-9]* || exit
CA=''
dpkg-buildpackage -b -uc -us

# Check whether nginx executable works and install it
if [ -s ./debian/nginx/usr/sbin/nginx ]
then
    cmp ./debian/nginx/usr/sbin/nginx /usr/sbin/nginx &> /dev/null
    if [ $? -ne 0 ]
    then
        ./debian/nginx/usr/sbin/nginx -t
        if [ $? -eq 0 ]
        then
            service nginx stop
            cat ./debian/nginx/usr/sbin/nginx > /usr/sbin/nginx
            service nginx start
        else
            read -r -p 'Inspect error messages and press ENTER to continue or CTRL-C to abort' CA
        fi
    fi
else
    echo 'Inspect build error messages'
    exit 1
fi

#--------------------------------------------------------------------
# Enable the "geoip2" module (if necessary)

# First remove the old "geoip" module (if present)
dpkg-query -S nginx-module-geoip &> /dev/null
[ $? -eq 0 ] && apt-get -y purge nginx-module-geoip
[ -s /etc/nginx/modules-enabled/geoip.conf ] && rm -f /etc/nginx/modules-enabled/geoip.conf
[ -s /etc/nginx/conf.d/geoip.conf ] && rm -rf /etc/nginx/conf.d/geoip.conf

# Now write the config file for the "geoip2" module
mkdir -p /etc/nginx/conf.d
if [ ! -s /etc/nginx/conf.d/geoip2.conf ]
then
    cat << EOT > /etc/nginx/conf.d/geoip2.conf
# Based on http://www.treselle.com/blog/nginx-with-geoip2-maxmind-database-to-fetch-user-geo-location-data/

geoip2 /etc/nginx/geoip2/GeoLite2-Country.mmdb {
 \$geoip_city_name city names en;
 \$geoip_postal_code postal code;
 \$geoip_latitude location latitude;
 \$geoip_longitude location longitude;
 \$geoip_state_name subdivisions 0 names en;
 \$geoip_state_code subdivisions 0 iso_code;
 }

geoip2 /etc/nginx/geoip2/GeoLite2-City.mmdb {
 \$geoip_country_code default=US country iso_code;
 \$geoip_country_name country names en;
 }
EOT
fi

# Get/update the GeoLite2 databases
[ -x /usr/local/sbin/geoipupdate-btoy1.sh ] && /usr/local/sbin/geoipupdate-btoy1.sh
for F in GeoLite2-City.mmdb GeoLite2-Country.mmdb
do
    if [ -f /var/lib/GeoIP/$F ]
    then
        cp -u /var/lib/GeoIP/$F /etc/nginx/geoip2
elif [ -f /usr/share/GeoIP/$F ]
    then
        cp -u /usr/share/GeoIP/$F /etc/nginx/geoip2
    fi
done

#--------------------------------------------------------------------
# Enable the "brotli" module (if necessary)
if [ ! -s /etc/nginx/conf.d/brotli.conf ]
then
    cat << EOT > /etc/nginx/conf.d/brotli.conf
brotli on;
brotli_comp_level 6;
brotli_static on;
brotli_types text/plain text/css application/javascript application/x-javascript text/xml application/xml application/xml+rss text/javascript image/x-icon image/vnd.microsoft.icon image/bmp image/svg+xml;
EOT
fi

#--------------------------------------------------------------------
# Enable the "modsecurity" module (if necessary)
# See https://kifarunix.com/configure-libmodsecurity-with-nginx-on-centos-8/
mkdir -p /etc/nginx/modsecurity/rules
cp -u /usr/local/src/ModSecurity/modsecurity.conf-recommended /etc/nginx/modsecurity/modsecurity.conf
cp -u /usr/local/src/ModSecurity/unicode.mapping /etc/nginx/modsecurity/unicode.mapping
cat << EOT > /etc/nginx/modsecurity/main.conf
# From https://github.com/SpiderLabs/ModSecurity/blob/master/
# modsecurity.conf-recommended
#
# Edit to set SecRuleEngine On
Include "/etc/nginx/modsecurity/modsecurity.conf"

Include "/etc/nginx/modsecurity/crs-setup.conf"
$(find /etc/nginx/modsecurity/rules/ -type f - name "*.conf" | awk '{print "Include \""$1"\""}')

# Basic test rule
SecRule ARGS:testparam "@contains test" "id:1234,deny,status:403"
EOT

# Copy the CRS rule set
[ -s /etc/nginx/modsecurity/crs-setup.conf ] || cp -u /usr/local/src/coreruleset/crs-setup.conf.example /etc/nginx/modsecurity/crs-setup.conf
cp -u /usr/local/src/coreruleset/rules/* /etc/nginx/modsecurity/rules

# Add a custom rule for "log4shell" vulnerability
if [ ! -s /etc/nginx/modsecurity/rules/log4shell.conf ]
then
    # See https://robert.penz.name/1602/modsecurity-rule-to-filter-cve-2021-44228-logjam-log4shell/
    cat << EOT > /etc/nginx/modsecurity/rules/log4shell.conf
SecRule \
  ARGS|REQUEST_HEADERS|REQUEST_URI|REQUEST_BODY|REQUEST_COOKIES|REQUEST_LINE|QUERY_STRING "jndi:ldap:|jndi:dns:|jndi:rmi:|jndi:rni:|\${jndi:" \
  "phase:1, \
  id:751001, \
  t:none, \
  deny, \
  status:403, \
  log, \
  auditlog, \
  msg:'DVT: CVE-2021-44228 - phase 1 - deny known \"jndi:\" pattern', \
  severity:'5', \
  rev:1, \
  tag:'no_ar'"
  
SecRule \
  ARGS|REQUEST_HEADERS|REQUEST_URI|REQUEST_BODY|REQUEST_COOKIES|REQUEST_LINE|QUERY_STRING "jndi:ldap:|jndi:dns:|jndi:rmi:|jndi:rni:|\${jndi:" \
  "phase:2, \
  id:751002, \
  t:none, \
  deny, \
  status:403, \
  log, \
  auditlog, \
  msg:'DVT: CVE-2021-44228 - phase 2 - deny known \"jndi:\" pattern', \
  severity:'5', \
  rev:1, \
  tag:'no_ar'
EOT
fi

for F in /etc/nginx/sites-enabled/*vhost /etc/nginx/sites-enabled/*site
do
    [ -f $F ] || continue
    if [ "$(grep 'modsecurity on' $F)" ]
    then
        cat << EOT

Add these directives to '$F' - right after the 'server_name' statement:

        # Enable "modsecurity"
        modsecurity  on;
        modsecurity_rules_file  /etc/nginx/modsecurity/main.conf;
EOT
    fi
done

# Remove duplicate ruleset file
rm -f /etc/nginx/modsecurity/rules/REQUEST-934-APPLICATION-ATTACK-NODEJS.conf

# Finally restart the rebuild nginx version
RESTART_NGINX=0
nginx -t &> /dev/null
[ $? -eq 0 ] && RESTART_NGINX=1

#--------------------------------------------------------------------
# Give nginx more files to work with
for F in system user
do
    if [ -z "$(grep ^DefaultLimitNOFILE /etc/systemd/$F.conf)" ]
    then
        echo "DefaultLimitNOFILE=65536" >> /etc/systemd/$F.conf
        RESTART_NGINX=1
    fi
done
if [ -z "$(grep ^LimitNOFILE /etc/systemd/system/nginx.service.d/nginx.conf)" ]
then
    mkdir -p /etc/systemd/system/nginx.service.d
    cat <<EOT > /etc/systemd/system/nginx.service.d/nginx.conf
[Service]
LimitNOFILE=65536
EOT
    systemctl daemon-reload
    RESTART_NGINX=1
fi
[ $RESTART_NGINX -ne 0 ] && systemctl restart nginx.service

#--------------------------------------------------------------------
# We are done
exit 0
