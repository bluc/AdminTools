#!/bin/bash
################################################################
# (c) Copyright 2021 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/tailscale.postinstall.sh

#--------------------------------------------------------------------
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            

#--------------------------------------------------------------------
# Specifying "-x" to the bash invocation = DEBUG
DEBUG=''
[[ $- ==  *x* ]] && DEBUG='-x'

#--------------------------------------------------------------------
apt remove iperf3 libiperf0

# Get the packages
cd /usr/local/src
apt install libsctp1
wget -q -O libiperf0_3.9-1_amd64.deb https://iperf.fr/download/ubuntu/libiperf0_3.9-1_amd64.deb
wget -q -O iperf3_3.9-1_amd64.deb https://iperf.fr/download/ubuntu/iperf3_3.9-1_amd64.deb
# Install the downloaded packages
dpkg -i libiperf0_3.9-1_amd64.deb iperf3_3.9-1_amd64.deb

#--------------------------------------------------------------------
# We are done
exit 0
