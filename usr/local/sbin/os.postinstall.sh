#!/bin/bash
# Version 20240826-184617 checked into repository
################################################################
# (c) Copyright 2023 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/os.postinstall.sh

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE

# Display information about any detected errors
failure() {
    local exitcode=$1
    local lineno=$2
    local msg=$3
   
    # Ignore some false positives
    [[ $msg =~ diff ]] && return
    [[ $msg =~ wget ]] && return
    echo "Detected error $exitcode in $PROG at line $lineno: $msg"
}
trap 'failure $? ${LINENO} "$BASH_COMMAND"' ERR

#--------------------------------------------------------------------
# This host and domain
THISHOST=$(hostname)
[[ $THISHOST == *.* ]] || THISHOST=$(hostname -f)

#--------------------------------------------------------------------
# Can we use a popup dialog program for questions?
USE_DIALOG=0
[ -x /bin/whiptail ] && USE_DIALOG=1

TFILE=$(mktemp /tmp/$$XXXXXXXX)

#--------------------------------------------------------------------
# Get possible program options
FORCE_INSTALL=''
DISABLE_IPV6=1
while getopts hy6 OPTION
do
    case ${OPTION} in
        y)  FORCE_INSTALL='-y'
            ;;
        6)  DISABLE_IPV6=0
            rm -f /etc/apt.conf.d/99-force-ipv4
            ;;
        h|*)  echo "Usage: $PROG [-y|-6]"
              echo "       -y Force installation of packages [default=no]"
              echo "       -6 Enable IPv6 [default=no]"
              exit 0
            ;;
    esac
done
shift $((OPTIND - 1))

# Use IPv4 throughout this script (unless overwritten by option)
[ $DISABLE_IPV6 -ne 0 ] && echo 'Acquire::ForceIPv4 "true";' > /etc/apt/apt.conf.d/99-force-ipv4

#--------------------------------------------------------------------
# Function to convert netmasks into CIDR notation and back
# See: https://forums.gentoo.org/viewtopic-t-888736-start-0.html
function mask2cdr ()
{
    # Assumes there's no "255." after a non-255 byte in the mask
    local x=${1##*255.}
    set -- 0^^^128^192^224^240^248^252^254^ $(( (${#1} - ${#x})*2 )) ${x%%.*}
    x=${1%%$3*}
    echo $(( $2 + (${#x}/4) ))
}

#--------------------------------------------------------------------
# Disable IPv6 (unless overridden by option "6")
[ $DISABLE_IPV6 -eq 1 ] && sysctl -e net.ipv6.conf.$(ip route get 8.8.8.8 | awk '/via/{print $5; exit}').disable_ipv6=1

#--------------------------------------------------------------------
# Determine the Linux distribution
LINUX_DIST=''
INSTALL_PROG=''
if [ -s /etc/debian_version ]
then
    LINUX_DIST='DEBIAN'
    INSTALL_PROG="/usr/bin/env DEBIAN_FRONTEND=noninteractive apt-get -y $FORCE_INSTALL"
    # For Ubuntu choose the US mirror repositories
    sed -i 's@http://security.ubuntu.com@http://us.archive.ubuntu.com@g' /etc/apt/sources.list
elif [ -s /etc/redhat-release ]
then
    LINUX_DIST='REDHAT'
    INSTALL_PROG="yum $FORCE_INSTALL"

    # Install the necessary redhat packages
    yum list > /tmp/redhat.packages.list
    SRV_ARCH=$(uname -i)
    if [ -z "$(grep '^rpmforge-release.'$SRV_ARCH /tmp/redhat.packages.list)" ]
    then
        # Get "rpmforge" repository and install it
        curl -L 'http://pkgs.repoforge.org/rpmforge-release/rpmforge-release-0.5.3-1.el6.rf.x86_64.rpm' \
            > /tmp/rpmforge-release-0.5.3-1.el6.rf.x86_64.rpm
        rpm --import http://apt.sw.be/RPM-GPG-KEY.dag.txt
        $INSTALL_PROG install /tmp/rpmforge-release-0.5.3-1.el6.rf.x86_64.rpm
        # Reget the list of install packages
        yum list > /tmp/redhat.packages.list
    fi
else
    echo "Unsupported Linux distribution"
    exit 1
fi

#--------------------------------------------------------------------
# Remove some packages on a virtual guest
IS_VIRTUAL=0
if [ -n "$(grep -m1 VMware /proc/scsi/scsi)" ]
then
    IS_VIRTUAL=1
elif [ -n "$(grep QEMU /proc/cpuinfo /sys/class/dmi/id/sys_vendor)" ] && [ -n "$(grep Bochs /sys/class/dmi/id/bios_vendor)" ]
then
    IS_VIRTUAL=2
elif [ -n "$(grep '^flags[[:space:]]*.*hypervisor' /proc/cpuinfo)" ]
then
    IS_VIRTUAL=3
fi
if [ $IS_VIRTUAL -ne 0 ]
then
    $INSTALL_PROG -y purge irqbalance
    $INSTALL_PROG -y install qemu-guest-agent

    if [ -n "$(grep -m 1 linode /etc/systemd/network/*)" ]
    then
        # We are on a Linode host
        if [ -z "$(grep swapfile /etc/fstab)" ]
        then
            # Create and install a swapfile
            read -rp 'Size of additional swap space in GB [default=2] ? ' SS
            [ -z "$SS" ] && SS=2
            case $SS in
                *[!0-9]*)
                    # Not a number
                    ;;
                *)
                    echo 'Creating additional swap space'
                    dd if=/dev/zero of=/swapfile bs=1024 count=$((SS * 1024 * 1024))
                    mkswap /swapfile
                    chmod 600 /swapfile

                    echo 'Activating additional swap space'
                    swapon -p -2 /swapfile
                    SWAP_PART="$(awk '/^\/dev.*swap/{$4="sw,pri=9";print}' /etc/fstab)"
                    grep -v swap /etc/fstab > $TFILE
                    cat << EOT >> $TFILE
# Dedicated swap partition
$SWAP_PART
# Local swap file to complement swap partition
/swapfile none swap sw,pri=-2 0 0
EOT
                    diff $TFILE /etc/fstab &> /dev/null
                    [ $? -ne 0 ] && cat $TFILE > /etc/fstab
                    ;;
            esac
        fi
    fi
fi

#--------------------------------------------------------------------
# Ensure Entropy Pools are Populated, prevents slowdowns while waiting for entropy
if [ -n "$(< /sys/devices/virtual/misc/hw_random/rng_available)" ]
then
    # On devices with a RNG try "rngd" first and fall back to "haveged"
    $INSTALL_PROG -o Dpkg::Options::='--force-confdef' install rng-tools
    if [ -z "$(ps -wefH | grep rng[d])" ]
    then
        $INSTALL_PROG purge rng-tools
        $INSTALL_PROG -o Dpkg::Options::='--force-confdef' install haveged
        ## Net optimising
        cat <<EOF > /etc/default/haveged
# eXtremeSHOK.com
#   -w sets low entropy watermark (in bits)
DAEMON_ARGS="-w 1024"
EOF
        systemctl daemon-reload && systemctl enable haveged && systemctl restart haveged
    fi
else
    # On devices without RNG just install "haveged"
    $INSTALL_PROG -o Dpkg::Options::='--force-confdef' install haveged
    ## Net optimising
    cat <<EOF > /etc/default/haveged
# eXtremeSHOK.com
#   -w sets low entropy watermark (in bits)
DAEMON_ARGS="-w 1024"
EOF
    systemctl daemon-reload && systemctl enable haveged && systemctl restart haveged
fi

#--------------------------------------------------------------------
# On Sun hardware start grub in console mode (and don't display a splash screen)
if [ -n "$(dmidecode | grep -m1 'Manufacturer.*Sun Microsystems')" ]
then
    if [ -z "$(grep 'SUN devices' /etc/default/grub)" ]
    then
        cat << EOT > /etc/default/grub

# For SUN devices work in a simple console
GRUB_GFXMODE=640x480
GRUB_GFXPAYLOAD_LINUX=keep
EOT
        update-grub
    fi
fi

#--------------------------------------------------------------------
# Remove the PPP packages
if [ "T$LINUX_DIST" = 'TDEBIAN' ]
then
    $INSTALL_PROG --purge landscape-common
    dpkg-query -l lubuntu-desktop 2> /dev/null
    [ $? -ne 0 ] && $INSTALL_PROG --purge remove ppp pppconfig pppoeconf
else
    $INSTALL_PROG erase ppp pppconfig pppoeconf
fi

#--------------------------------------------------------------------
# Install specific packages
if [ "T$LINUX_DIST" = 'TDEBIAN' ]
then
    $INSTALL_PROG install sudo firehol iprange joe ethtool linuxlogo libunix-syslog-perl chrony libio-socket-ssl-perl sendemail chkrootkit perltidy xz-utils net-tools
    if [ -s /etc/lsb-release ]
    then
        $INSTALL_PROG install xtables-addons-dkms
        source /etc/lsb-release
        if [ ${DISTRIB_RELEASE%.*} -lt 14 ]
        then
            $INSTALL_PROG install python-software-properties
        else
            $INSTALL_PROG install software-properties-common
        fi

        # Get some updated packages from 3rd party
        wget http://neuro.debian.net/lists/${DISTRIB_CODENAME}.us-nh.full -O /etc/apt/sources.list.d/neurodebian.sources.list
        if [ -z "$(apt-key list | grep -i neuro)" ]
        then
            apt-key adv --recv-keys --keyserver hkp://pgp.mit.edu:80 0xA5D32F012649A5A9
            [ $? -ne 0 ] && wget -q -O - http://neuro.debian.net/_static/neuro.debian.net.asc | apt-key add -
        fi
    fi

    # Update chkrootkit
    cat << EOT > /etc/cron.daily/0chkrootkit-update
#!/bin/sh
    # Update chkrootkit
    wget -q https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/chkrootkit.postinstall.sh -O /usr/local/sbin/chkrootkit.postinstall.sh
    if [ \$? -eq 0] && [ -s /usr/local/sbin/chkrootkit.postinstall.sh ]
    then
        chmod 0744 /usr/local/sbin/chkrootkit.postinstall.sh
        /usr/local/sbin/chkrootkit.postinstall.sh
    fi
EOT
    chmod 744 /etc/cron.daily/0chkrootkit-update
    /etc/cron.daily/0chkrootkit-update

    # Install "shellcheck"
    wget -O /tmp/shellcheck.tar.xz \
        https://github.com/koalaman/shellcheck/releases/download/stable/shellcheck-stable.linux.x86_64.tar.xz
    if [ -s /tmp/shellcheck.tar.xz ]
    then
        (cd /tmp && xz -d shellcheck.tar.xz && tar xf shellcheck.tar && cp shellcheck-stable/shellcheck /usr/local/bin)
    fi

    # Remove "You do not have a valid subscription for this server….” from Proxmox Virtual Environment
    if [ -s /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js ] && [ -n "$(grep 'data.status' /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js)" ]
    then
        if [ $(pveversion | grep -c 'pve-manager/7') -ge 1 ]
        then
            if [ -z "$(grep 'NoMoreNag' /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js)" ]
            then
                echo "DPkg::Post-Invoke { \"dpkg -V proxmox-widget-toolkit | grep -q '/proxmoxlib\.js$'; if [ \$? -eq 1 ]; then { echo 'Removing subscription nag from UI...'; sed -i '/data.status/{s/\!//;s/active/NoMoreNagging/}' /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js; }; fi\"; };" >/etc/apt/apt.conf.d/no-nag-script
                apt --reinstall install proxmox-widget-toolkit &>/dev/null
            fi
        else
            sed -i.backup "s/data.status !== 'Active'/false/g" /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js && systemctl restart pveproxy.service
        fi

        # Proxmox optimizations
        # Based on https://raw.githubusercontent.com/extremeshok/xshok-proxmox/master/install-post.sh
        RAM_SIZE_GB=$(awk '/^MemTotal.*kB$/ { print int($2/1024/1000); }' /proc/meminfo)

        ## Detect AMD EPYC CPU and Apply Fixes
        if [ "$(grep -i -m 1 'model name' /proc/cpuinfo | grep -i 'EPYC')" != "" ]; then
          echo "AMD EPYC detected"
          #Apply EPYC fix to kernel : Fixes random crashing and instability
          if ! grep "GRUB_CMDLINE_LINUX_DEFAULT" /etc/default/grub | grep -q "idle=nomwait" ; then
            echo "Setting kernel idle=nomwait"
            sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT="/GRUB_CMDLINE_LINUX_DEFAULT="idle=nomwait /g' /etc/default/grub
            update-grub
          fi
        fi
        if [ "$(grep -i -m 1 'model name' /proc/cpuinfo | grep -i 'EPYC')" != '' ] || [ "$(grep -i -m 1 'model name' /proc/cpuinfo | grep -i 'Ryzen')" != '' ]; then
          ## Add msrs ignore to fix Windows guest on EPIC/Ryzen host
          echo "options kvm ignore_msrs=Y" >> /etc/modprobe.d/kvm.conf
          echo "options kvm report_ignored_msrs=N" >> /etc/modprobe.d/kvm.conf
        fi

        ## Ensure ksmtuned (ksm-control-daemon) is enabled and optimise according to ram size
        $INSTALL_PROG -o Dpkg::Options::='--force-confdef' install ksm-control-daemon
        if [[ RAM_SIZE_GB -le 16 ]] ; then
            # start at 50% full
            KSM_THRES_COEF=50
            KSM_SLEEP_MSEC=80
        elif [[ RAM_SIZE_GB -le 32 ]] ; then
            # start at 60% full
            KSM_THRES_COEF=40
            KSM_SLEEP_MSEC=60
        elif [[ RAM_SIZE_GB -le 64 ]] ; then
            # start at 70% full
            KSM_THRES_COEF=30
            KSM_SLEEP_MSEC=40
        elif [[ RAM_SIZE_GB -le 128 ]] ; then
            # start at 80% full
            KSM_THRES_COEF=20
            KSM_SLEEP_MSEC=20
        else
            # start at 90% full
            KSM_THRES_COEF=10
            KSM_SLEEP_MSEC=10
        fi
        sed -i -e "s/\# KSM_THRES_COEF=.*/KSM_THRES_COEF=${KSM_THRES_COEF}/g" /etc/ksmtuned.conf
        sed -i -e "s/\# KSM_SLEEP_MSEC=.*/KSM_SLEEP_MSEC=${KSM_SLEEP_MSEC}/g" /etc/ksmtuned.conf
        systemctl enable ksmtuned
        systemctl restart ksmtuned

        ## Limit the size and optimise journald
        cat <<EOF > /etc/systemd/journald.conf
# eXtremeSHOK.com
[Journal]
# Store on disk
Storage=persistent
# Don't split Journald logs by user
SplitMode=none
# Disable rate limits
RateLimitInterval=0
RateLimitIntervalSec=0
RateLimitBurst=0
# Disable Journald forwarding to syslog
ForwardToSyslog=no
# Journald forwarding to wall /var/log/kern.log
ForwardToWall=yes
# Disable signing of the logs, save cpu resources.
Seal=no
Compress=yes
# Fix the log size
SystemMaxUse=64M
RuntimeMaxUse=60M
# Optimise the logging and speed up tasks
MaxLevelStore=warning
MaxLevelSyslog=warning
MaxLevelKMsg=warning
MaxLevelConsole=notice
MaxLevelWall=crit
EOF
        systemctl restart systemd-journald.service
        journalctl --vacuum-size=64M --vacuum-time=1d;
        journalctl --rotate &

        ## Optimise Memory
        cat <<EOF > /etc/sysctl.d/99-xs-memory.conf
# eXtremeSHOK.com
# Memory Optimising
## Bugfix: reserve 512MB memory for system
vm.min_free_kbytes=524288
vm.nr_hugepages=72
# (Redis/MongoDB)
vm.max_map_count=262144
vm.overcommit_memory = 1
## Enable TCP BBR congestion control
# TCP BBR congestion control
net.core.default_qdisc=fq_codel
net.ipv4.tcp_congestion_control=bbr
EOF
        sysctl -p /etc/sysctl.d/99-xs-memory.conf

        ## Increase max FD limit / ulimit
        cat <<EOF >> /etc/security/limits.d/99-xs-limits.conf
# eXtremeSHOK.com
# Increase max FD limit / ulimit
* soft     nproc          256000
* hard     nproc          256000
* soft     nofile         256000
* hard     nofile         256000
root soft     nproc          256000
root hard     nproc          256000
root soft     nofile         256000
root hard     nofile         256000
EOF
        ## Increase kernel max Key limit
        cat <<EOF > /etc/sysctl.d/99-xs-maxkeys.conf
# eXtremeSHOK.com
# Increase kernel max Key limit
kernel.keys.root_maxkeys=1000000
kernel.keys.maxkeys=1000000
EOF
        sysctl -p /etc/sysctl.d/99-xs-maxkeys.conf
        ## Set systemd ulimits
        echo "DefaultLimitNOFILE=256000" >> /etc/systemd/system.conf
        echo "DefaultLimitNOFILE=256000" >> /etc/systemd/user.conf

        echo 'session required pam_limits.so' >> /etc/pam.d/common-session
        echo 'session required pam_limits.so' >> /etc/pam.d/runuser-l

        ## Set ulimit for the shell user
        echo "ulimit -n 256000" >> /root/.profile

    fi

    # Create a command for package updates
    if [ ! -s /usr/local/sbin/pkgupdate ]
    then
        wget --no-check-certificate -O /usr/local/sbin/pkgupdate \
            https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/pkgupdate
        if [ $? -ne 0 ] || [ ! -s /usr/local/sbin/pkgupdate ]
        then
            cat << EOT > /usr/local/sbin/pkgupdate
#!/bin/bash
dpkg -l | awk '/^rc/ {print \$2}' | xargs -r dpkg -P
apt-get update
apt-get -y autoremove
apt-get dist-upgrade
EOT
        fi
    fi
    chmod 0744 /usr/local/sbin/pkgupdate
else
    $INSTALL_PROG install sudo vim-minimal ethtool perltidy system-config-network-tui system-config-firewall-tui
fi

#--------------------------------------------------------------------
# Activate the HPN patched SSH (no longer exists :( )
#HPN#[ "T$LINUX_DIST" = 'TDEBIAN' ] && apt-add-repository ppa:w-rouesnel/openssh-hpn

if [ $(grep -c '^processor' /proc/cpuinfo) -gt 1 ]
then
    # Install multi-core versions of gzip and bzip2
    $INSTALL_PROG install pigz pbzip2
fi

# Update and upgrade
$INSTALL_PROG update
if [ "T$LINUX_DIST" = 'TDEBIAN' ]
then
    $INSTALL_PROG autoremove
    $INSTALL_PROG dist-upgrade
fi

#--------------------------------------------------------------------
# Adapt SSH configs (for security)
# See https://wiki.mozilla.org/Security/Guidelines/OpenSSH

# Disable password-based root logins
sed -i 's/#PermitRootLogin/PermitRootLogin/;s/PermitRootLogin .*/PermitRootLogin without-password/' /etc/ssh/sshd_config

# Restrict the number of ssh authentication attempts
sed -i 's/#MaxAuthTries/MaxAuthTries/;s/MaxAuthTries.*/MaxAuthTries 3/' /etc/ssh/sshd_config
# Enable IgnoreRhosts for ssh
sed -i 's/#IgnoreRhosts/IgnoreRhosts/;s/IgnoreRhosts.*/IgnoreRhosts yes/' /etc/ssh/sshd_config
# Disable host based authentication for ssh
sed -i 's/#HostbasedAuthentication/;s/HostbasedAuthentication.*/HostbasedAuthentication no/' /etc/ssh/sshd_conf
# Disable authentication for ssh with an empty password
sed -i 's/#PermitEmptyPasswords/;s/PermitEmptyPasswords.*/PermitEmptyPasswords no/' /etc/ssh/sshd_config
# Disable custom environment for ssh
sed -i 's/#PermitUserEnvironment/;s/PermitUserEnvironment.*/PermitUserEnvironment no/' /etc/ssh/sshd_config

# Strong Diffie-Hellman moduli (suitable for openssh 6 and above)
awk '$5 >= 3071' /etc/ssh/moduli > /etc/ssh/moduli.tmp && mv /etc/ssh/moduli.tmp /etc/ssh/moduli

# Strong, secure ciphers and keys in "/etc/ssh/sshd_config" (for openssh => 6.x):
[ -z "$(grep '^KexAlgo' /etc/ssh/sshd_config)" ] && echo 'KexAlgorithms curve25519-sha256@libssh.org,ecdh-sha2-nistp521,ecdh-sha2-nistp384,ecdh-sha2-nistp256,diffie-hellman-group-exchange-sha256' >> /etc/ssh/sshd_config
[ -z "$(grep '^Ciphers' /etc/ssh/sshd_config)" ] &&  echo 'Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr' >> /etc/ssh/sshd_config
[ -z "$(grep '^MACs' /etc/ssh/sshd_config)" ] && echo 'MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,umac-128@openssh.com' >> /etc/ssh/sshd_config

# Only use SSH protocol 2 to avoid man-in-middle attacks
sed -i 's/^[[:space:]]*Protocol.*/Protocol 2/' /etc/ssh/ssh_config

#--------------------------------------------------------------------
# Adjust the mount options for any ext{3|4} partition
# See http://www.howtoforge.com/reducing-disk-io-by-mounting-partitions-with-noatime
awk 'substr($3,1,3) != "ext" {print}' /etc/fstab > $TFILE.fstab
awk 'substr($3,1,3) == "ext" {print $1" "$2" "$3" noatime,errors=remount-ro "$5" "$6}' /etc/fstab >> $TFILE.fstab
diff -u $TFILE.fstab /etc/fstab &> /dev/null
if [ $? -ne 0 ]
then
    [ -s /tmp/fstab.orig ] || cp /etc/fstab /etc/fstab.orig
    cat $TFILE.fstab > /etc/fstab
fi
rm -f $TFILE.fstab

#--------------------------------------------------------------------
# Adapt firehol.conf
if [ -d /etc/firehol ]
then
    # Adapt firehol config
    if [ -z "$(grep bellow /etc/firehol/firehol.conf)" ]
    then
        LOCALIF=$(ip route get 8.8.8.8 | awk '/via/{print $5; exit}')
        LOCALIP=$(ifconfig $LOCALIF | sed -n "s/.*inet addr:\([0-9.]*\).*/\1/p")
        [ -z "$LOCALIP" ] && LOCALIP=$(ifconfig $LOCALIF | sed -n "s/.*inet \([0-9.]*\).*/\1/p")
        [ -z "$LOCALIP" ] && LOCALIP=$(ip addr list $LOCALIF | sed -n "s/.*inet \([0-9.]*\).*/\1/p")
        LOCALMASK=$(ifconfig $LOCALIF | sed -n -e 's/.*Mask:\(.*\)$/\1/p')
        [ -z "$LOCALMASK" ] && LOCALMASK=$(ifconfig $LOCALIF | awk '/netmask/ {print $4}')
        # From: http://www.routertech.org/viewtopic.php?t=1609
        l="${LOCALIP%.*}";r="${LOCALIP#*.}";n="${LOCALMASK%.*}";m="${LOCALMASK#*.}"
        LOCALNET=$((${LOCALIP%%.*}&${LOCALMASK%%.*})).$((${r%%.*}&${m%%.*})).$((${l##*.}&${n##*.})).$((${LOCALIP##*.}&${LOCALMASK##*.}))
        CIDRMASK=$(mask2cdr $LOCALMASK)

        # Install packages for Geo-based firewall rules
        $INSTALL_PROG install xtables-addons-dkms libtext-csv-perl unzip

        # Get the IP address for B-LUC
        BLUC_CL1=$(dig +short cl1.btoy1.net)
        BLUC_CL2=$(dig +short cl2.btoy1.net)
        BLUC_MX=$(dig +short btoy1.mooo.com)
        cat << EOT > /etc/firehol/firehol.conf
#!/sbin/firehol
# : firehol.sh,v 1.273 2008/07/31 00:46:41 ktsaou Exp $
#
# This config will have the same effect as NO PROTECTION!
# Everything that found to be running, is allowed.
# YOU SHOULD NEVER USE THIS CONFIG AS-IS.
#
# Date: Fri Oct 15 07:38:13 EDT 2010 on host linux2
#
# IMPORTANT:
# The TODOs bellow, are *YOUR* to-dos!
#

bluc='$BLUC_CL1 $BLUC_CL2 $BLUC_MX'
home_net='${LOCALNET}/${CIDRMASK}'

# Private server ports
#server_xwaadmin_ports="tcp/7071"
#client_xwaadmin_ports="default"

# Fix some TOS values
EOT
        if [ -z "$(grep -m 1 '^tosfix(' /usr/sbin/firehol)" ]
        then
            cat << EOT >> /etc/firehol/firehol.conf
# See: http://www.docum.org/docum.org/faq/cache/49.html
# and: https://github.com/ktsaou/firehol/blob/master/sbin/firehol.in
iptables -t mangle -N ackfix
iptables -t mangle -A ackfix -m tos ! --tos Normal-Service -j RETURN
iptables -t mangle -A ackfix -p tcp -m length --length 0:128 -j TOS --set-tos Minimize-Delay
iptables -t mangle -A ackfix -p tcp -m length --length 128: -j TOS --set-tos Maximize-Throughput
iptables -t mangle -A ackfix -j RETURN
iptables -t mangle -I POSTROUTING -p tcp -m tcp --tcp-flags SYN,RST,ACK ACK -j ackfix

iptables -t mangle -N tosfix
iptables -t mangle -A tosfix -p tcp -m length --length 0:512 -j RETURN
iptables -t mangle -A tosfix -m limit --limit 2/s --limit-burst 10 -j RETURN
iptables -t mangle -A tosfix -j TOS --set-tos Maximize-Throughput
iptables -t mangle -A tosfix -j RETURN
iptables -t mangle -I POSTROUTING -p tcp -m tos --tos Minimize-Delay -j tosfix
EOT
        else
            echo 'tosfix' >> /etc/firehol/firehol.conf
        fi

        cat << EOT >> /etc/firehol/firehol.conf

# Interface No 1a - frontend (public).
# The purpose of this interface is to control the traffic
# on the $LOCALIF interface with IP ${LOCALIP} (net: "${LOCALNET}/${CIDRMASK}").
interface $LOCALIF internal_1 src "\${home_net}" dst ${LOCALIP}

        # The default policy is DROP. You can be more polite with REJECT.
        # Prefer to be polite on your own clients to prevent timeouts.
        policy drop

        # Don't trust the clients behind ${LOCALIF} (net "${LOCALNET}/${CIDRMASK}")
        protection strong 75/sec 50

        # Here are the services listening on $LOCALIF.
        server "ssh" accept src "\${home_net}"
        #server "smtp imaps smtps https" accept
        server ping accept

        # The following means that this machine can REQUEST anything via $LOCALIF.
        client all accept

# Interface No 1b - frontend (public).
# The purpose of this interface is to control the traffic
# from/to unknown networks behind the default gateway
interface $LOCALIF external_1 src not "\${home_net}" dst ${LOCALIP}

        # The default policy is DROP. You can be more polite with REJECT.
        policy drop

        # Don't trust the clients behind $LOCALIF (net not "\${UNROUTABLE_IPS} ${LOCALNET}/${CIDRMASK}")
        protection strong 75/sec 50

        # Here are the services listening on $LOCALIF.
        # TODO: Normally, you will have to remove those not needed.
        server4 any drop src "\${PRIVATE_IPS} \${MULTICAST_IPV4}"
        server "ssh" accept src "\${bluc}"
        #server "smtp imaps smtps http https" accept
        server ping accept

        # Portscan defense
        iptables -A in_external_1 -m psd -j LOG --log-prefix 'IN-ISP-Portscan'
        iptables -A in_external_1 -m psd -j DROP

        # The following means that this machine can REQUEST anything via $LOCALIF.
        # TODO: On production servers, avoid this and allow only the
        #       client services you really need.
        client all accept
EOT

        if [ $(dpkg -l firehol | awk '/^ii/ {print int($3)}') -ge 2 ]
        then
            # Use explicit ipv4 commands in newer versions
            sed -i 's/interface /interface4 /g;s/server /server4 /g;s/client /client4 /g' /etc/firehol/firehol.conf
        fi
    fi
    sed -ie 's/^[[:space:]]*START_FIREHOL.*/START_FIREHOL=YES/' /etc/default/firehol

    # Get the scripts to update firewall blocklists
    echo "Updating firewall blocklists - that takes a while"
    read -rt 10 -p 'Press ENTER to continue or wait 10 seconds' E
    for F in UpdateBlocklists.sh UpdateBlocklists-Firehol.sh
    do
        wget --no-check-certificate -O /usr/local/sbin/$F \
            https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/$F
        chmod 744 /usr/local/sbin/$F
    done

    # Update the local firewall's block lists and ban traffic
    #  to/from China, Russia, Vietnam, North Korea and Ukraine
    UpdateBlocklists-Firehol.sh -L 1 -C cn,ru,vn,kp,ua

elif [ -x /usr/bin/system-config-firewall-tui ]
then
    system-config-firewall-tui
fi

#--------------------------------------------------------------------
if [ "T$LINUX_DIST" = 'TDEBIAN' ]
then
    # Install the audit daemon
    # (configuration - see /etc/rc.local)
    $INSTALL_PROG install auditd

    # Enable syslog via rsyslog
    if [ -s /etc/audisp/plugins.d/syslog.conf ]
    then
        # Use the native syslog module for auditd
        #  (secure enough since we use rsyslog)
        sed -i -e 's/^active.*/active = yes/' /etc/audisp/plugins.d/syslog.conf

        service auditd restart
    fi
    if [ -d /etc/rsyslog.d ]
    then
        cat << EOT > /etc/rsyslog.d/40-auditd.conf
# Ex.: Suppress anything but "forbidden" messages
#if (\$programname contains 'audispd') and (not (\$rawmsg contains ' forbidden')) then ~
EOT
    fi
fi

#OPTIONAL #--------------------------------------------------------------------
#OPTIONAL if [ -z "$(grep opa /etc/passwd)" ]
#OPTIONAL then
#OPTIONAL     # Create the operator account
#OPTIONAL     useradd -s /bin/bash -m -c 'Linux Operator' opa
#OPTIONAL     passwd opa
#OPTIONAL else
#OPTIONAL     # Give the "opa" account a meaningful full name
#OPTIONAL     [ -z "$(getent passwd opa)" ] || chfn -f 'Linux Operator' opa
#OPTIONAL fi
#OPTIONAL if [ -d /etc/sudoers.d ]
#OPTIONAL then
#OPTIONAL     # Make sure that "opa" can execute "sudo"
#OPTIONAL     cat << EOT > /etc/sudoers.d/opa
#OPTIONAL ## Allow opa to run any commands anywhere
#OPTIONAL opa    ALL=(ALL)       ALL
#OPTIONAL EOT
#OPTIONAL     chmod 440 /etc/sudoers.d/opa
#OPTIONAL fi

#--------------------------------------------------------------------
# Tweak system for security and performance
if [ -z "$(grep IS_VIRTUAL /etc/rc.local)" ]
then
    # Make sure that rc.local runs in "bash"
    if [ -s /etc/rc.local ]
    then
        sed -i -e 's@bin/sh -e@bin/bash@;/^exit/d' /etc/rc.local
    else
        echo '#!/bin/bash' > /etc/rc.local
    fi

    # Expand /etc/rc.local
    cat << EORC >> /etc/rc.local
#-------------------------------------------------------------------------
# Allow for larger PID numbers on 64bit systems (default is 32738)
[ "T\$(uname -m)" = 'Tx86_64' ] && echo \$((128 * 1024)) > /proc/sys/kernel/pid_max

#-------------------------------------------------------------------------
# See: https://klaver.it/linux/sysctl.conf
echo '# Dynamically created sysctl.conf' > /tmp/sysctl.conf

#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Improve system memory management
echo '# Increase size of file handles and inode cache' >> /tmp/sysctl.conf
[ \$(sysctl -n fs.file-max) -ge 209708 ] && echo -n '# ' >> /tmp/sysctl.conf
echo 'fs.file-max = 209708' >> /tmp/sysctl.conf
echo '' >> /tmp/sysctl.conf

cat << EOSC >> /tmp/sysctl.conf
EOSC

if [ \$(awk '/^MemTotal/{print int(\$2 / 1024)}' /proc/meminfo) -ge 1024 ]
then
    ## See https://antmeetspenguin.blogspot.com/2011/01/high-performance-linux-router.html
    BUCKET_SIZE=\$((256 * 1024))
    HASH_SIZE=\$((\$BUCKET_SIZE / 4))

    cat << EOSC /tmp/sysctl.conf
net.netfilter.nf_conntrack_max = \$BUCKET_SIZE
# See https://antmeetspenguin.blogspot.com/2011/01/high-performance-linux-router.html
net.netfilter.nf_conntrack_tcp_timeout_established = 86400
EOSC
    echo \$HASH_SIZE > /sys/module/nf_conntrack/parameters/hashsize
fi

EORC
    if [ "T$LINUX_DIST" = 'TDEBIAN' ]
    then
        echo 'dpkg-query -l xserver-xorg > /dev/null 2>&1' >> /etc/rc.local
    else
        echo 'rpm -q xorg-x11-server-Xorg >/dev/null 2>&1' >> /etc/rc.local
    fi
    cat << EORC >> /etc/rc.local
if [ \$? -ne 0 ]
then
    # Disable bluetooth on servers
    echo 'alias net-pf-31 off' > /etc/modprobe.d/blacklist-bluetooth.conf
    update-rc.d bluetooth remove

    cat << EOSC >> /tmp/sysctl.conf
# Tune the kernel scheduler for a server
# See: http://people.redhat.com/jeder/presentations/customer_convergence/2012-04-jeder_customer_convergence.pdf
kernel.sched_min_granularity_ns = 10000000
kernel.sched_wakeup_granularity_ns = 15000000
kernel.sched_migration_cost = 1000000

# Suppress swapping unless absolutely necessary
vm.swappiness = 1
EOSC
else
    cat << EOSC >> /tmp/sysctl.conf
# Do less swapping
vm.swappiness = 10
EOSC
fi
cat << EOSC >> /tmp/sysctl.conf
vm.watermark_scale_factor = 100

# Adjust disk write buffers
EOSC
SYSTEM_RAM=\$(awk '/MemTotal/ {print \$2}' /proc/meminfo)
if [ \$SYSTEM_RAM -lt 2097152 ]
then
    cat << EOSC >> /tmp/sysctl.conf
# 60% disk cache under 2GB RAM
vm.dirty_ratio = 40
# Start writing at 10%
vm.dirty_background_ratio = 10
EOSC
elif [ \$SYSTEM_RAM -lt 8388608 ]
then
    cat << EOSC >> /tmp/sysctl.conf
# 30% disk cache under 4GB RAM
vm.dirty_ratio = 30
# Start writing at 7%
vm.dirty_background_ratio = 7
EOSC
else
   cat << EOSC >> /tmp/sysctl.conf
# Hold up to 600MB in disk cache
vm.dirty_bytes = 629145600
# Start writing at 300MB
vm.dirty_background_bytes = 314572800
EOSC
fi

cat << EOSC >> /tmp/sysctl.conf
# Protect bottom 64k of memory from mmap to prevent NULL-dereference
# attacks against potential future kernel security vulnerabilities.
vm.mmap_min_addr = 65536

# Keep at least 64MB of free RAM space available
vm.min_free_kbytes = 65536

# Increase max user watches
fs.inotify.max_user_watches=1048576
fs.inotify.max_user_instances=1048576
fs.inotify.max_queued_events=1048576

EOSC

#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Tune overall security settings
cat << EOSC >> /tmp/sysctl.conf
# Disable core dumps
fs.suid_dumpable = 0

# Randomly place virtual memory regions
kernel.randomize_va_space = 2

# Enable /proc/\$pid/maps privacy so that memory relocations are not
# visible to other users.
kernel.maps_protect = 1

# Controls the System Request debugging functionality of the kernel
kernel.sysrq = 1

# Controls whether core dumps will append the PID to the core filename.
# Useful for debugging multi-threaded applications.
kernel.core_uses_pid = 1

# The contents of /proc/<pid>/maps and smaps files are only visible to
# readers that are allowed to ptrace() the process
kernel.maps_protect = 1

# Controls the maximum size of a message, in bytes
kernel.msgmnb = 65536

# Controls the default maxmimum size of a message queue
kernel.msgmax = 65536

# Automatic reboot
vm.panic_on_oom = 1
kernel.panic_on_oops = 1
kernel.unknown_nmi_panic = 1
kernel.panic_on_unrecovered_nmi = 1
kernel.panic = 60

# Stop low-level messages on console
kernel.printk = 4 4 1 7

EOSC

#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Tune the network stack for security
cat << EOSC >> /tmp/sysctl.conf
# Prevent SYN attack, enable SYNcookies (they will kick-in when the max_syn_backlog reached)
net.ipv4.tcp_syncookies = 1
net.ipv4.tcp_syn_retries = 5
net.ipv4.tcp_synack_retries = 2
net.ipv4.tcp_max_syn_backlog = 4096

# Disables packet forwarding
net.ipv4.ip_forward = 0
net.ipv4.conf.all.forwarding = 0
net.ipv4.conf.default.forwarding = 0
net.ipv6.conf.all.forwarding = 0
net.ipv6.conf.default.forwarding = 0

# Disable IPv6
net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1
net.ipv6.conf.lo.disable_ipv6 = 1

# Disables IP source routing
net.ipv4.conf.all.accept_source_route = 0
net.ipv4.conf.default.accept_source_route = 0
net.ipv6.conf.all.accept_source_route = 0
net.ipv6.conf.default.accept_source_route = 0

# Enable IP spoofing protection, turn on source route verification
net.ipv4.conf.all.rp_filter = 1
net.ipv4.conf.default.rp_filter = 1

# Enable Log Spoofed Packets, Source Routed Packets, Redirect Packets
net.ipv4.conf.all.log_martians = 1
net.ipv4.conf.default.log_martians = 1

# Decrease the time default value for tcp_fin_timeout connection
net.ipv4.tcp_fin_timeout = 30

# Decrease the default values for connections to keep alive
net.ipv4.tcp_keepalive_probes = 5
net.ipv4.tcp_keepalive_intvl = 15
net.ipv4.tcp_keepalive_time = 900

# Don't relay bootp
net.ipv4.conf.all.bootp_relay = 0

# Don't proxy arp for anyone
net.ipv4.conf.all.proxy_arp = 0

# Turn on SACK
net.ipv4.tcp_dsack = 1
net.ipv4.tcp_sack = 1
net.ipv4.tcp_fack = 1

# Don't ignore directed pings
net.ipv4.icmp_echo_ignore_all = 0

# Disable timestamps
net.ipv4.tcp_timestamps = 0

# Enable ignoring broadcasts request
net.ipv4.icmp_echo_ignore_broadcasts = 1

# Enable bad error message Protection
net.ipv4.icmp_ignore_bogus_error_responses = 1

# Allowed local port range
net.ipv4.ip_local_port_range = 32769 60416

# Enable a fix for RFC1337 - time-wait assassination hazards in TCP
net.ipv4.tcp_rfc1337 = 1

EOSC

dpkg -l 'pve-kernel-*' 2> /dev/null
if [ \$? -eq 0 ]
then
    cat << EOSC >> /tmp/sysctl.conf
# Recommendations as per https://gist.github.com/sergey-dryabzhinsky/bcc1a15cb7d06f3d4606823fcc834824
net.core.somaxconn = 256000
net.ipv4.tcp_max_syn_backlog = 40000
net.core.netdev_max_backlog = 50000
net.ipv4.tcp_challenge_ack_limit = 9999
net.ipv4.tcp_low_latency = 1
net.ipv4.neigh.default.gc_thresh1 = 1024
net.ipv4.neigh.default.gc_thresh2 = 2048
net.ipv4.neigh.default.gc_thresh3 = 4096
vm.nr_hugepages = 1
net.unix.max_dgram_qlen = 1024
kernel.sched_autogroup_enabled = 0
EOSC
fi

#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Tune the network stack for performance (in order of preference)
CANDO_BBR=0
if [ -s /lib/modules/\$(uname -r)/kernel/net/ipv4/tcp_bbr.ko ]
then
    CANDO_BBR=1
    CALG=bbr
elif [ -f /lib/modules/\$(uname -r)/kernel/net/ipv4/tcp_htcp.ko ]
then
    CALG=htcp
elif [ -f /lib/modules/\$(uname -r)/kernel/net/ipv4/tcp_cubic.ko ]
then
    CALG=cubic
fi
modprobe tcp_\$CALG
cat << EOSC >> /tmp/sysctl.conf
# Use modern congestion control algorithm
net.ipv4.tcp_congestion_control = \$CALG

# Setup default Ethernet queueing
net.core.default_qdisc = fq_codel

# No slowness for idle connections
net.ipv4.tcp_slow_start_after_idle = 0

# Turn on the tcp_window_scaling
net.ipv4.tcp_window_scaling = 1

# Increase the maximum total buffer-space allocatable
net.ipv4.tcp_mem = 8388608 12582912 16777216
net.ipv4.udp_mem = 8388608 12582912 16777216

# Increase the maximum read-buffer space allocatable
net.ipv4.tcp_rmem = 8192 256960 16777216
net.ipv4.udp_rmem_min = 16384

# Increase the maximum write-buffer-space allocatable
net.ipv4.tcp_wmem = 8192 256960 16777216
net.ipv4.udp_wmem_min = 16384

# Increase the maximum and default receive socket buffer size
net.core.rmem_max=16777216
net.core.rmem_default=256960

# Increase the maximum and default send socket buffer size
net.core.wmem_max=16777216
net.core.wmem_default=256960

# Increase the tcp-time-wait buckets pool size
net.ipv4.tcp_max_tw_buckets = 1440000
net.ipv4.tcp_max_orphans = 1440000
net.ipv4.tcp_tw_reuse = 1
# This is gone as of kernel 4.12 and above
net.ipv4.tcp_tw_recycle = 1

# Increase the maximum memory used to reassemble IP fragments
net.ipv4.ipfrag_high_thresh = 512000
net.ipv4.ipfrag_low_thresh = 446464

# Increase the maximum amount of option memory buffers
net.core.optmem_max = 65536

# Increase the maximum number of skb-heads to be cached
#net.core.hot_list_length = 1024

# don't cache ssthresh from previous connection
net.ipv4.tcp_no_metrics_save = 1
net.ipv4.tcp_moderate_rcvbuf = 1

# Increase RPC slots
sunrpc.tcp_slot_table_entries = 128
sunrpc.udp_slot_table_entries = 128

# Increase size of RPC datagram queue length
net.unix.max_dgram_qlen = 50

# Don't allow the arp table to become bigger than this
net.ipv4.neigh.default.gc_thresh3 = 2048

# Tell the gc when to become aggressive with arp table cleaning.
# Adjust this based on size of the LAN. 1024 is suitable for most /24
# networks
net.ipv4.neigh.default.gc_thresh2 = 1024

# Adjust where the gc will leave arp table alone - set to 32.
net.ipv4.neigh.default.gc_thresh1 = 32

# Adjust to arp table gc to clean-up more often
net.ipv4.neigh.default.gc_interval = 30

# Increase TCP queue length
net.ipv4.neigh.default.proxy_qlen = 96
net.ipv4.neigh.default.unres_qlen = 6

# Enable Explicit Congestion Notification (RFC 3168), disable it if it
# doesn't work for you
net.ipv4.tcp_ecn = 1
net.ipv4.tcp_reordering = 3

# How many times to retry killing an alive TCP connection
net.ipv4.tcp_retries2 = 15
net.ipv4.tcp_retries1 = 3

# Increase number of incoming connections
net.core.somaxconn = 32768

# Increase number of incoming connections backlog
net.core.netdev_max_backlog = 4096
net.core.dev_weight = 64

# This will ensure that immediatly subsequent connections use the new values
net.ipv4.route.flush = 1
net.ipv6.route.flush = 1

EOSC

if [ -d /etc/sysctl.d ]
then
    rm -f /etc/sysctl.d/90-bluc.conf
    cat /tmp/sysctl.conf > /etc/sysctl.d/zz-bluc.conf
elif [ -z "\$(grep 'Dynamically created' /etc/sysctl.conf)" ]
then
    cat /tmp/sysctl.conf >> /etc/sysctl.conf
fi
sysctl --system -e

#-------------------------------------------------------------------------
# Enable large ring buffers for Ethernet interfaces
for ETH in \$(awk -F: '/:/ && !/lo|tap|ovs|docker|veth|virbr|ifb|tun|dummy|teql|gre|ip6|sit|span|ip_|tailscale/{print \$1}' /proc/net/dev)
    do
    RING=\$(ethtool -g \$ETH 2> /dev/null | awk 'BEGIN{i=0};/^[RT]X:/{printf"%d",\$2;i++;if(i)exit}')
    case \$RING in
    ''|*[!0-9]*)
        :
    ;;
    *)
        [ \$(ethtool -g \$ETH | grep -c "[RT]X:.*\$RING") -ne 4 ] && (ethtool -G \$ETH rx \$RING tx \$RING 2> /dev/null; sleep 5)
    ;;
    esac

    [ \$CANDO_BBR -ne 0 ] && [ ! "T\$(sysctl -nq net.ipv4.tcp_congestion_control)" = 'Tbbr' ] && sysctl -qw net.ipv4.tcp_congestion_control=bbr
    [ ! "T$(sysctl -nq net.core.default_qdisc)" = 'Tfq_codel' ] && sysctl -qw net.core.default_qdisc=fq_codel
    [ -z "\$(tc -s qdisc show dev \$ETH | grep -Eaw '(mq|fq_codel)')" ] && tc qdisc delete root dev \$ETH 2> /dev/null

    # A larger transmit queue
    ifconfig \$ETH txqueuelen 2048
    # Disable wake on line
    /sbin/ethtool -s \$ETH wol d &> /dev/null
done

# Increase initial window for TCP on ALL routes
# See http://www.cablelabs.com/wp-content/uploads/2014/05/Analysis_of_Google_SPDY_TCP.pdf
ip route show | grep eth | while read L
do
    if [[ ! \$L =~ init.wnd ]]
    then
        ip route change \$L initcwnd 10 initrwnd 10
    fi
done

#-------------------------------------------------------------------------
# Set readahead for disks
# See http://linuxmantra.com/2013/11/disk-read-ahead-in-linux.html
#     http://michael.otacoo.com/postgresql-2/tuning-disks-and-linux-for-postgres/
SECTORS=16384
MD_SECTORS=\$SECTORS
# MegaRaid controller:
if [ -x /opt/MegaRAID/MegaCli/MegaCli64 ]
then
    SECTORS=\$(/opt/MegaRAID/MegaCli/MegaCli64 -AdpAllInfo -aAll -NoLog | awk '/^Max Data Transfer Size/ {print \$(NF-1)}')
fi
if [ -e /proc/mdstat ]
then
    for DEV in \$(awk '/^md/ {gsub(/\[[0-9]\]/,"");\$2="";\$3="";\$4="";print \$0}' /proc/mdstat)
    do
        RA=\$(blockdev --getra /dev/\$DEV)
        case \$DEV in
        md*)
            [ \$RA -lt 62000 ] && blockdev --setra 65535 /dev/\$DEV
            ;;
        *)
            [ \$RA -ne \$SECTORS ] && blockdev --setra \$SECTORS /dev/\$DEV
            ;;
        esac
    done
fi
for DEV in \$( ls /dev/[vhs]d[a-z][0-9] /dev/nvme[0-9]n[0-9] 2> /dev/null | awk '{sub(/\/dev\//,"");printf "%s ",\$1}')
do
    RA=\$(blockdev --getra /dev/\$DEV)
    [ \$RA -ne \$MD_SECTORS ] && blockdev --setra \$MD_SECTORS /dev/\$DEV
done
for DEV in \$( ls /dev/xvd[a-z][0-9] 2> /dev/null | awk '{sub(/\/dev\//,"");printf "%s ",\$1}')
do
    RA=\$(blockdev --getra /dev/\$DEV)
    [ \$RA -ne \$SECTORS ] && blockdev --setra \$SECTORS /dev/\$DEV
done
for DEV in \$( ls /dev/mapper/* 2> /dev/null | awk '{sub(/\/dev\/mapper\//,"");printf "%s ",\$1}')
do
    [ "T\$DEV" = 'Tcontrol' ] && continue
    RA=\$(blockdev --getra /dev/mapper/\$DEV)
    [ \$RA -ne \$SECTORS ] && blockdev --setra \$SECTORS /dev/mapper/\$DEV
done
echo "You can ignore any errors concerning 'cciss' devices"
for DEV in \$( ls /dev/cciss/c[0-9]d[0-9] 2> /dev/null | awk '{sub(/\/dev\/cciss\//,"");printf "%s ",\$1}')
do
    [ "T\$DEV" = 'Tcontrol' ] && continue
    RA=\$(blockdev --getra /dev/cciss/\$DEV)
    [ \$RA -ne \$SECTORS ] && blockdev --setra \$SECTORS /dev/cciss/\$DEV
done
if [ -d /dev/etherd ]
then
    for DEV in \$(ls /dev/etherd/e[0-9].[0-9]p[0-9] 2> /dev/null | awk'{sub(/\/dev\/etherd\//,"");printf "%s ",\$1}')
    do
        [ "T\$DEV" = 'Tcontrol' ] && continue
        RA=\$(blockdev --getra /dev/etherd/\$DEV)
        [ \$RA -ne \$SECTORS ] && blockdev --setra \$SECTORS /dev/etherd/\$DEV
    done
fi

# Filesystem errors force a reboot
for FS in \$(awk '/ext[234]/ {print \$1}' /proc/mounts)
do
    tune2fs -e panic -c 5 -i 1m \$FS
    mount -o remount,errors=panic \$FS
done

#-------------------------------------------------------------------------
# Set the correct I/O scheduler
IS_VIRTUAL=0
if [ -n "\$(grep -m1 VMware /proc/scsi/scsi)" ] || [ -n "\$(grep 'DMI:.*VMware' /var/log/dmesg)" ]
then
    IS_VIRTUAL=1
elif [ -n "\$(grep -Ea 'KVM|QEMU' /proc/cpuinfo /sys/class/dmi/id/sys_vendor)" ] || [ -n "\$(grep Bochs /sys/class/dmi/id/bios_vendor)" ]
then
    IS_VIRTUAL=2
elif [ -n "\$(grep '^flags[[:space:]]*.*hypervisor' /proc/cpuinfo)" ]
then
    IS_VIRTUAL=3
fi
IS_INTERACTIVE=0
if [ "T\$LINUX_DIST" = 'TDEBIAN' ]
then
    dpkg-query -l xserver-xorg > /dev/null 2>&1
    [ \$? -eq 0 ] && IS_INTERACTIVE=1
else
    rpm -q xorg-x11-server-Xorg >/dev/null 2>&1
    [ \$? -eq 0 ] && IS_INTERACTIVE=1
fi
cd /sys/block
for DEV in [vhs]d? xvd? cciss\!c[0-9]d[0-9] nvme*
do
    [ -d \$DEV ] || continue

    # See: http://oss.oetiker.ch/rrdtool-trac/wiki/TuningRRD
    [ -w \${DEV}/queue/nr_requests ] && echo 512 > \${DEV}/queue/nr_requests
    # See https://serverfault.com/questions/373563/linux-real-world-hardware-raid-controller-tuning-scsi-and-cciss
    [ -w \${DEV}/queue/max_sectors_kb ] && echo \$(< \${DEV}/queue/max_hw_sectors_kb) > \${DEV}/queue/max_sectors_kb

    # Set up modern CPU IRQ affinity
    [ -w \${DEV}/queue/rq_affinity ] && echo 2 > \${DEV}/queue/rq_affinity

    # Set disk entropy based on disk type (0 = SSD, 1 = HDD)
    cat \${DEV}/queue/rotational > \${DEV}/queue/add_random

    if [ -w \${DEV}/queue/read_ahead_kb ]
    then
        [ \$(< \${DEV}/queue/read_ahead_kb) -lt 2048 ] && echo 2048 > \${DEV}/queue/read_ahead_kb
    fi
    if [ -f \${DEV}/device/vendor ] || [ -f \${DEV}/device/model ]
    then
        if [ -n "\$(grep -Ea '(DELL|3ware|Areca|HP)' \${DEV}/device/vendor \${DEV}/device/model)" ]
        then
            # Use "noop/none" for 3ware/Dell/Areca/HP (Raid) units
            [ -w \${DEV}/queue/scheduler ] && echo noop > \${DEV}/queue/scheduler 2>/dev/null || echo none > \${DEV}/queue/scheduler
            [ \$? -eq 0 ] || echo none > \${DEV}/queue/scheduler
            [ \$? -eq 0 ] && continue
        elif [ -n "\$(grep -Ea '(RS3DC080|SSD)' \${DEV}/device/vendor \${DEV}/device/model)" ]
        then
            # Use "noop/none" for Intel Raid units and SSD drives
            [ -w \${DEV}/queue/scheduler ] && echo noop > \${DEV}/queue/scheduler 2>/dev/null || echo none > \${DEV}/queue/scheduler
            [ \$? -eq 0 ] || echo none > \${DEV}/queue/scheduler
            [ \$? -eq 0 ] && continue
        fi
    fi
    if [[ \$DEV =~ cciss || \$DEV =~ dm- || \$DEV =~ nvme ]]
    then
        # Use "noop/none" for HP (Raid) units and NVME drives
        [ -w \${DEV}/queue/scheduler ] && echo noop > \${DEV}/queue/scheduler 2>/dev/null || echo none > \${DEV}/queue/scheduler
        [ \$? -eq 0 ] || echo none > \${DEV}/queue/scheduler
        [ \$? -eq 0 ] && continue
    fi

    if [ \$IS_VIRTUAL -eq 0 ]
    then
        if [ -s /lib/modules/`uname -r`/kernel/block/bfq.ko ]
        then
            # Try the "budget fair queueing" scheduler
            # See https://unix.stackexchange.com/questions/30286/can-i-configure-my-linux-system-for-more-aggressive-file-system-caching#41831
            modprobe bfq
            if [ \$? -eq 0 ]
            then
                [ -w \${DEV}/queue/scheduler ] && echo bfq > \${DEV}/queue/scheduler
                [ -w \${DEV}/queue/nr_requests ] && echo 4 > \${DEV}/queue/nr_requests
                [ -w \${DEV}/queue/osched/back_seek_max ] && echo 32000 > \${DEV}/queue/iosched/back_seek_max
                [ -w \${DEV}/queue/iosched/back_seek_penalty ] && echo 3 > \${DEV}/queue/iosched/back_seek_penalty
                [ -w \${DEV}/queue/iosched/fifo_expire_sync ] && echo 80 > \${DEV}/queue/iosched/fifo_expire_sync
                [ -w \${DEV}/queue/iosched/fifo_expire_async ] && echo 1000 > \${DEV}/queue/iosched/fifo_expire_async
                [ -w \${DEV}/queue/iosched/slice_idle_us ] && echo 5300 > \${DEV}/queue/iosched/slice_idle_us
                [ -w \${DEV}/queue/iosched/low_latency ] && echo 1 > \${DEV}/queue/iosched/low_latency
                [ -w \${DEV}/queue/iosched/timeout_sync ] && echo 200 > \${DEV}/queue/iosched/timeout_sync
                [ -w \${DEV}/queue/iosched/max_budget ] && echo 0 > \${DEV}/queue/iosched/max_budget
                [ -w \${DEV}/queue/iosched/strict_guarantees ] && echo 1 > \${DEV}/queue/iosched/strict_guarantees
                continue
            fi
        fi
        if [ -s /lib/modules/\$(uname -r)/kernel/block/mq-deadline.ko ]
        then
            # Try the multi-queue scheduler
            [ -w \${DEV}/queue/scheduler ] && echo mq-deadline > \${DEV}/queue/scheduler
            continue
        fi
        if [ \$IS_INTERACTIVE -eq 0 ]
        then
            # Try the "completely fair queuing" scheduler
            [ -w \${DEV}/queue/scheduler ] && echo cfq > \${DEV}/queue/scheduler
            if [ \$? -eq 0 ]
            then
                [ -w \${DEV}/device/queue_depth ] && echo 1 > \${DEV}/device/queue_depth
                # See: http://www.nextre.it/oracledocs/ioscheduler_03.html
                [ -w \${DEV}/queue/iosched/slice_idle ] && echo 0 > \${DEV}/queue/iosched/slice_idle
                [ -w \${DEV}/queue/iosched/max_depth ] && echo 64 > \${DEV}/queue/iosched/max_depth
                [ -w \${DEV}/queue/iosched/queued ] && echo 8 > \${DEV}/queue/iosched/queued
                # See: http://www.linux-mag.com/id/7572/2
                [ -w \${DEV}/queue/iosched/quantum ] && echo 32 > \${DEV}/queue/iosched/quantum
                # See: http://lkml.indiana.edu/hypermail/linux/kernel/0906.3/02344.html
                # (favors writes over reads)
                [ -w \${DEV}/queue/iosched/slice_async] && echo 10 > \${DEV}/queue/iosched/slice_async
                [ -w \${DEV}/queue/iosched/slice_async_rq ] && cat \${DEV}/queue/iosched/slice_async > \${DEV}/queue/iosched/slice_async_rq
                [ -w \${DEV}/queue/iosched/slice_sync] && echo 100 > \${DEV}/queue/iosched/slice_sync
            fi
        else
            # Use the "deadline" scheduler
            [ -w \${DEV}/queue/scheduler ] && echo deadline > \${DEV}/queue/scheduler
        fi
    else
        # Use "noop" for VMware and KVM guests
        [ -w \${DEV}/queue/scheduler ] && echo noop > \${DEV}/queue/scheduler 2>/dev/null || echo none > \${DEV}/queue/scheduler
        [ \$? -eq 0 ] || echo none > \${DEV}/queue/scheduler
        # As per http://kb.vmware.com/selfservice/microsites/search.do?language=en_US&cmd=displayKC&externalId=1009465
        [ -w \${DEV}/device/timeout ] && echo 180 > \${DEV}/device/timeout
    fi
done

#-------------------------------------------------------------------------
# Watch system performance (adapt email address "-a")
[ -x /usr/local/sbin/SysMon.pl ] && /usr/local/sbin/SysMon.pl -D -a root -o /var/tmp

#-------------------------------------------------------------------------
# Setup auditing (only if "auditd" is installed)
# See /usr/share/doc/auditd/examples/rules/README-rules
if [ -d /etc/audit/rules.d ]
then
    rm -f /etc/audit/rules.d/*.rules
    wget -T 30 -t 2 -q https://raw.githubusercontent.com/Neo23x0/auditd/master/audit.rules -O /etc/audit/rules.d/00-neo.rules
    if [ \$? -eq 0 ] && a[ -s /etc/audit/rules.d/00-neo.rules ]
    then
        # Add the URL for the rules
        sed -i '1 s@^@# See https://raw.githubusercontent.com/Neo23x0/auditd/master/audit.rules\n@' /etc/audit/rules.d/00-neo.rules

        # Adjust the account name for "chronyd"
        CNAME=\$(getent passwd | awk -F: '/chrony/{print \$1}')
        sed -i 's/uid=chrony/uid='\$CNAME'/g' /etc/audit/rules.d/00-neo.rules

        # Make configuration immutable
        echo '-e 2' >> /etc/audit/rules.d/00-neo.rules
     else
        cp /usr/share/doc/auditd/examples/rules/*.rules* /etc/audit/rules.d
        gunzip /etc/audit/rules.d/*.gz
        # Monitor privileged commands
        find / -xdev \( -perm -4000 -o -perm -2000 \) -type f | \\
          awk '{print "-a always,exit -F path=" \$1 " -F perm=x -F auid>='"\$(awk '/^\s*UID_MIN/{print \$2}' /etc/login.defs)"' -F auid!=4294967295 -k privileged" }' | \\
          sed 's/=chrony/=_chrony/g'  >> /etc/audit/rules.d/32-privileged.rules
        # Monitor mounts
        cat << EOR > /etc/audit/rules.d/33-mounts.rules
-a always,exit -F arch=b64 -S mount -F auid>=1000 -F auid!=4294967295 -k mounts
-a always,exit -F arch=b32 -S mount -F auid>=1000 -F auid!=4294967295 -k mounts
EOR
        # Monitor deletions
        cat << EOR > /etc/audit/rules.d/33-deletions.rules
-a always,exit -F arch=b64 -S unlink -S unlinkat -S rename -S renameat -F auid>=1000 -F auid!=4294967295 -k delete
-a always,exit -F arch=b32 -S unlink -S unlinkat -S rename -S renameat -F auid>=1000 -F auid!=4294967295 -k delete
EOR
        # Make configuration immutable
        sed -i 's/^#-e/-e/' /etc/audit/rules.d/99-finalize.rules
    fi
    augenrules --load &> /var/log/augenrules.log.\$(date +%FT%T)
    find /var/log -type f -name augenrules.log* -mtime +1 -delete
fi

# Send "journald" message to rsyslog
if [ -z "\$(grep -Ea '(shok|B.LUC)' /etc/systemd/journald.conf)" ]
then
    cat << EOJC > /etc/systemd/journald.conf
# B-LUC adjustments
# Store on disk
Storage=persistent
# Don't split Journald logs by user
SplitMode=none
# Disable rate limits
RateLimitInterval=0
RateLimitIntervalSec=0
RateLimitBurst=0
ForwardToSyslog=no
# Journald forwarding to wall /var/log/kern.log
ForwardToWall=yes
# Disable signing of the logs, save cpu resources.
Seal=no
Compress=yes
# Fix the log size
SystemMaxUse=64M
RuntimeMaxUse=60M
# Optimise the logging and speed up tasks
MaxLevelStore=warning
MaxLevelSyslog=warning
MaxLevelKMsg=warning
MaxLevelConsole=notice
MaxLevelWall=crit
EOJC
    systemctl restart systemd-journald.service
    journalctl --vacuum-size=64M --vacuum-time=1d;
    journalctl --rotate &
fi

#-------------------------------------------------------------------------
# We are done
exit 0
EORC
    chmod 744 /etc/rc.local

    if [ $USE_DIALOG -ne 0 ]
    then
        whiptail --title='Review Email' --nocancel --ok-button 'Continue' \
            --msgbox "Review/correct the '-a root' email address in /etc/rc.local" 9 40
    else
        read -rp "Review/correct the '-a root' email address in /etc/rc.local" -t 30 REPLY
    fi
    vi +/SysMon /etc/rc.local

    # Get the newest version of some files
    for F in RemoveOldKernel.sh pkgupdate
    do
        wget -q -O $TFILE https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/$F
        diff -uw $TFILE /usr/local/sbin/$F &> /dev/null
        [ $? -ne 0 ] && cat $TFILE > /usr/local/sbin/$F
        chmod 744 /usr/local/sbin/$F
    done
    for F in CIS.sh SysMon.install.sh
    do
        wget -q -O $TFILE https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/$F
        diff -uw $TFILE /usr/local/sbin/$F &> /dev/null
        [ $? -ne 0 ] && cat $TFILE > /usr/local/sbin/$F
        chmod 700 /usr/local/sbin/$F
    done
    for F in loadcheck.sm loadcheck.pm loadcheck.py
    do
        wget -q -O $TFILE https://gitlab.com/bluc/AdminTools/raw/master/usr/local/include/$F
        diff -uw $TFILE /usr/local/include/$F &> /dev/null
        [ $? -ne 0 ] && cat $TFILE > /usr/local/include/$F
        chmod 644 /usr/local/include/$F
    done

    # Create a suitable local HealthCheck.sh file (if necessary)
    if [ ! -s /usr/local/sbin/HealthCheck.sh ]
    then
        cat << EOT > /usr/local/sbin/HealthCheck.sh
#!/bin/bash
#--------------------------------------------------------------------
# (c) CopyRight 2015 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#--------------------------------------------------------------------

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=\${0##*/}

# Only run in multi-user mode
runlevel > /tmp/runlevel 2> /dev/null
[ -z "\$(grep '^[A-Z0-9]' /tmp/runlevel)" ] && exit 0
[ \$(who -r | awk '{print \$2}') -ge 2 ] || exit 0

#-------------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/\$PROG.lock
if [ -f \$LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=\$(< \$LOCKFILE)
    [ -z "\$(ps h -p \$MYPID)" ] || exit 0
fi
trap "rm -f \$LOCKFILE /tmp/\$\$*" EXIT
echo "\$\$" > \$LOCKFILE

DEBUG='-q'
[[ \$- ==  *x* ]] && DEBUG='-v'

# Globals
set \$(date "+%_H %_M")
CURHOUR=\$1
CURMIN=\$2
UPTIME=\$(cut -d. -f1 < /proc/uptime)

THISHOST=\$(hostname)
[[ \$THISHOST == *.* ]] || THISHOST=\$(hostname -f)
THISDOMAIN=\${THISHOST#*.}

#--------------------------------------------------------------------
# Can we use "curl"?
USER_AGENT=${PROG}'/'\$(awk '/^# Version 2/{print \$3}' \$(command -v os.postinstall.sh))

HAVE_CURL=0
if [ -x /usr/bin/curl ]
then
    HAVE_CURL=1
    CURL_OPTS='-skL -m 60 --retry-connrefused --retry 3 --user-agent '"\$USER_AGENT"
    [[ "\$(curl --help)" =~ compressed ]] && CURL_OPTS="\$CURL_OPTS --compressed"
fi
WGET_OPTS='-q --no-check-certificate -t 3 -T 60 --user-agent='"\$USER_AGENT"
[[ "\$(wget --help)" =~ compression ]] && WGET_OPTS="\$WGET_OPTS --compression=auto"

#--------------------------------------------------------------------
if [ \$((\$CURMIN % 5)) -eq 2 ]
then
    # Disable MOST of the motd updater
    cd /etc/update-motd.d
    find . -type f -print0 | xargs -0rn 40 chmod 0644
    # Create our own footer
    cat << EOM > 99-footer
#!/bin/bash
if [ -x /usr/bin/linux_logo ]
then
    OS_LOGO=11
    if [ -s /etc/lsb-release ]
    then
        source /etc/lsb-release
        OS_LOGO=\\\$(linux_logo -L list | awk '/Ubuntu/{print \\\$1}' | tail -n 1)
    elif [ -s /etc/debian_version ]
    then
        OS_LOGO=\\\$(linux_logo -L list | awk '/Debian/{print \\\$1}' | tail -n 1)
    fi
    linux_logo -ys -L \\\$OS_LOGO -F "#O Version #V, Compiled #C\n#N #M #T #P, #R RAM\n#H\n"
else
    echo -n 'Linux version '; uname -r
    echo -n 'Compiled '; uname -v
    echo \\\$(grep -c 'core id' /proc/cpuinfo)\\\$(awk -F: '/^model name/{print \\\$2;exit}' /proc/cpuinfo)
    awk '/^MemTotal/{printf "%.2fGB RAM\\\n",\\\$2/1024/1024}' /proc/meminfo
fi
EOM
    chmod +x 99*

    # Don't let RAID resync bog down the system
    if [ \$(grep -c ^md /proc/mdstat) -gt 0 ]
    then
        CURDAY=\$(date +%w)
        # This speed is the absolute minimum
        MIN_SPEED=2000
        if [ \$CURDAY -eq 0 ] || [ \$CURDAY -eq 6 ] || [ \$CURHOUR -le 6 ] || [ \$CURHOUR -ge 19 ]
        then
            # Higher speeds are allow on weekends or non-working hours
            MIN_SPEED=\$((MIN_SPEED * 10))
        fi
        MAX_SPEED=\$((2 * MIN_SPEED))
        [ \$(sysctl -n dev.raid.speed_limit_min) -ne \$MIN_SPEED ] && sysctl -qw dev.raid.speed_limit_min=\$MIN_SPEED
        [ \$(sysctl -n dev.raid.speed_limit_max) -ne \$MAX_SPEED ] && sysctl -qw dev.raid.speed_limit_max=\$MAX_SPEED
    
        for MD in \$(awk '/^md/{print \$1}' /proc/mdstat)
        do
            mdadm --detail /dev/\$MD > /tmp/\$\$
            if [ -z "\$(grep 'Rebuild Status' /tmp/\$\$)" ]
            then
                # No bitmap needed in a stable state
                [ -z "\$(grep Bitmap /tmp/\$\$)" ] || mdadm --grow --bitmap=none /dev/\$MD
            else
                # Enable bitmap while rebuilding
                [ -z "\$(grep Bitmap /tmp/\$\$)" ] && mdadm --grow --bitmap=internal /dev/\$MD
            fi
        done
    fi
fi

#--------------------------------------------------------------------
if [ \$UPTIME -gt 500 ]
then
    # Make sure we have some sensible values for wget
    sed '/##BTOY1 START/,/##BTOY1 END/d' /etc/wgetrc > /tmp/\$\$.wget
    if [[ "\$(wget --help)" =~ compression= ]]
    then
        WC='compression = auto'
    else
        WC=''
    fi
    cat << EOWG >> /tmp/\$\$.wget
##BTOY1 START
timeout = 120
prefer-family = IPv4
tries = 5
\$WC
user_agent = BTOY1 $THISHOST $PROG
##BTOY1 END
EOWG
    diff /tmp/\$\$.wget /etc/wgetrc &>/dev/null
    [ \$? -ne 0 ] && cat /tmp/\$\$.wget > /etc/wgetrc

    if [ -z "\$(grep -m 1 'flags.*hypervisor' /proc/cpuinfo)" ]
    then
        # Decrease swapping (not on virtual guests, though)
        if [ ! -s /usr/local/sbin/deswappify_auto.py ]
        then
            if [ \$HAVE_CURL -eq 1 ]
            then
                curl \$CURL_OPTS -o /usr/local/sbin/deswappify_auto.py \\
                  http://cl1.btoy1.net/deswappify_auto.py
            else
                wget \$WGET_OPTS -o /usr/local/sbin/deswappify_auto.py \\
                  http://cl1.btoy1.net/deswappify_auto.py
            fi
        fi
        if [ ! -s /usr/local/sbin/deswappify_auto.py ]
        then
            if [ \$HAVE_CURL -eq 1 ]
            then
                curl \$CURL_OPTS -o /usr/local/sbin/deswappify_auto.py \\
                  https://raw.githubusercontent.com/wiedemannc/deswappify-auto/master/deswappify_auto.py
            else
                wget \$WGET_OPTS -O /usr/local/sbin/deswappify_auto.py \\
                  https://raw.githubusercontent.com/wiedemannc/deswappify-auto/master/deswappify_auto.py
            fi
        fi
        if [ ! -s /etc/systemd/system/deswappify.service ]
        then
            NUM_CORE=\$(awk '/^cpu cores/{print \$NF;exit}' /proc/cpuinfo)
            if [ \$NUM_CORE -lt 4 ]
            then
                MAX_JOBS=\$((\$NUM_CORE / 2))
                [ \$MAX_JOBS -eq 0 ] && MAX_JOBS=1
            else
                MAX_JOBS=4
            fi
            wget -q -O - https://raw.githubusercontent.com/wiedemannc/deswappify-auto/master/deswappify.service | \\
              sed "s@/usr/local/bin.*@/usr/local/sbin/deswappify_auto.py -e -j \$MAX_JOBS -t 120 -v info@g" > /etc/systemd/system/deswappify.service
            systemctl enable deswappify
        fi
        if [ -s /usr/local/sbin/deswappify_auto.py ] && [ -s /etc/systemd/system/deswappify.service ]
        then
            chmod 744 /usr/local/sbin/deswappify_auto.py
            [ -z "\$(ps -wefH | grep 'deswappif[y]')" ] && systemctl start deswappify
        fi
    fi

    if [ \$CURMIN -eq 3 ]
    then
        # Optimize TCP/IP receive windows
        [ "T\$(sysctl -n net.ipv4.tcp_rmem)" = 'T8192	262144	16777216' ] || sysctl -q -w 'net.ipv4.tcp_rmem=8192 262144 16777216'
        # The default window sizes depend on the amount of RAM in the system
        WIN_DEFAULT='1048576'
        if [ \$(awk '/MemTotal:/{print \$2}' /proc/meminfo) -le 2097152 ]
        then
            # At most 2GB RAM - use smaller window sizes
            WIN_DEFAULT='262144'
        fi
        [ "T\$(sysctl -n net.core.rmem_max)" = 'T16777216' ] || sysctl -q -w 'net.core.rmem_max=16777216'
        [ "T\$(sysctl -n net.core.rmem_default)" = "T\$WIN_DEFAULT" ] || sysctl -q -w "net.core.rmem_default=\$WIN_DEFAULT"
        [ "T\$(sysctl -n net.core.wmem_max)" = 'T16777216' ] || sysctl -q -w 'net.core.wmem_max=16777216'
        [ "T\$(sysctl -n net.core.wmem_default)" = "T\$WIN_DEFAULT" ] || sysctl -q -w "net.core.wmem_default=\$WIN_DEFAULT"
    fi

    #-------------------------------------------------------------------------
    if [ "T\$DEBUG" = 'T-v' ] || [ \$CURHOUR -gt 2 ] && [ \$CURHOUR -lt 6 ]
    then

        YESTERDAY=\$(date +%s -d 'yesterday')
        R_AGE=\$(date +%s -r /tmp/SyslogSummary.txt 2> /dev/null)
        [ -z "\$R_AGE" ] && R_AGE=0
        if [ \$YESTERDAY -gt \$R_AGE ]
        then
            # Get the daily system monitoring data
            zgrep -h "\$(date '+%b %_d' -d yesterday)"'.*SysMon.*DAILY' /var/log/syslog* | \\
              awk -F: '{print \$5}' > /tmp/\$\$
            if [ -s /tmp/\$\$ ]
            then

                echo > /tmp/SyslogSummary.txt
                date +"Daily system monitoring summary on \$THISHOST for %F:" -d yesterday >> /tmp/SyslogSummary.txt

                echo >> /tmp/SyslogSummary.txt
                grep average /tmp/\$\$ >> /tmp/SyslogSummary.txt
                echo >> /tmp/SyslogSummary.txt

                for DEV in \$(awk -F: '/:/ {gsub(/ /,"",\$1);print \$1}' /proc/net/dev)
                do
                    grep -Ea "(Receive|Transmit).*for \$DEV" /tmp/\$\$ >> /tmp/SyslogSummary.txt
                    echo >> /tmp/SyslogSummary.txt
                done

                for DEV in \$(awk '/[0-9]\$/{print \$NF}' /proc/partitions)
                do
                    grep -Ea "Disk (read|write).*for \$DEV" /tmp/\$\$ >> /tmp/SyslogSummary.txt
                    echo >> /tmp/SyslogSummary.txt
                done

                grep 'Mysql' /tmp/\$\$ >> /tmp/SyslogSummary.txt

                rm -f /tmp/\$\$

                # Delete duplicate empty lines
                # See http://sed.sourceforge.net/sed1line.txt
                sed -i -e '\$!N; /^\(.*\)\n\1\$/!P; D' /tmp/SyslogSummary.txt
            fi
        fi

        R_AGE=\$(date +%s -r /tmp/DiskLayoutReport.txt 2> /dev/null)
        [ -z "\$R_AGE" ] && R_AGE=0
        if [ \$YESTERDAY -gt \$R_AGE ]
        then
            # Create a disk layout report
            rm -f /tmp/DiskLayoutReport.txt
            echo >> /tmp/DiskLayoutReport.txt
            echo "=> Filesystems:" >> /tmp/DiskLayoutReport.txt
            df -Ph >> /tmp/DiskLayoutReport.txt

            if [ -x /sbin/lvdisplay ]
            then
                echo >> /tmp/DiskLayoutReport.txt
                echo "=> LVM2 volumes:" >> /tmp/DiskLayoutReport.txt
                lvdisplay >> /tmp/DiskLayoutReport.txt
                vgdisplay >> /tmp/DiskLayoutReport.txt
                pvdisplay >> /tmp/DiskLayoutReport.txt
            fi

            if [ -f /proc/mdstat ]
            then
                echo >> /tmp/DiskLayoutReport.txt
                echo "=> Softraid volumes:" >> /tmp/DiskLayoutReport.txt
                grep -A 1 md /proc/mdstat >> /tmp/DiskLayoutReport.txt
            fi

            echo >> /tmp/DiskLayoutReport.txt
            echo "=> Disks:" >> /tmp/DiskLayoutReport.txt
            fdisk -l 2>/dev/null | grep 'Disk.*GiB' >> /tmp/DiskLayoutReport.txt
        fi
    fi
fi

#-------------------------------------------------------------------------
# We are done
exit 0
EOT
        chmod 744 /usr/local/sbin/HealthCheck.sh
    fi

    #-------------------------------------------------------------------------
    # Keep timestamps in bash history
    sed -i 's/HISTCONTROL.*/HISTCONTROL=ignoreboth\nHISTTIMEFORMAT="%F %T "/' /etc/skel/.bashrc
    for D in $(getent passwd | cut -d: -f6)
    do
        [ -s $D/.bashrc ] || continue
        [ -z "$(grep ^HISTTIMEFORMAT $D/.bashrc)" ] && sed -i 's/HISTCONTROL.*/HISTCONTROL=ignoreboth\nHISTTIMEFORMAT="%F %T "/' $D/.bashrc
    done

    #-------------------------------------------------------------------------
    # Configure chrony
    if [ -d /etc/chrony ]
    then
        grep -Eav '^(# CUSTOM|pool|allow)' /etc/chrony/chrony.conf > $TFILE
        cat << EOT >> $TFILE
# CUSTOM: Use US based NTP servers
pool us.pool.ntp.org        iburst maxsources 4
pool 0.us.pool.ntp.org iburst maxsources 1
pool 1.us.pool.ntp.org iburst maxsources 1
pool 2.us.pool.ntp.org iburst maxsources 2
# CUSTOM: Provide NTP services to the local network
# CUSTOM: - remove the leading "#" to enable
# CUSTOM: - adapt the IP address range if necessary
#allow ${LOCALNET}/${CIDRMASK}
EOT
        diff $TFILE /etc/chrony/chrony.conf &> /dev/null
        if [ $? -ne 0 ]
        then
            cat $TFILE > /etc/chrony/chrony.conf
            service chrony restart
        fi
    fi

    # Create a suitable cron job (if necessary)
    if [ ! -s /etc/cron.d/bluc ]
    then
        cat << EOT > /etc/cron.d/bluc
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/sbin:/sbin:/usr/local/bin:/usr/bin:/bin:/snap/bin
#MAILTO=<FILL IN THE CORRECT EMAIL ADDRESS>
#===============================================================
# Healthchecks
* * * * *       root    [ -x /usr/local/sbin/HealthCheck.sh ] && /usr/local/sbin/HealthCheck.sh
# The nightly cleanups, updates and backup
12 1 * * *      root    [ -x /usr/local/sbin/LiSysCo.sh ] && /usr/local/sbin/LiSysCo.sh
12 1 * * *      root    [ -x /usr/local/sbin/LiHome.sh ] && /usr/local/sbin/LiHome.sh
11 0 * * *      root    [ -x /usr/local/sbin/VMDump.sh ] && /usr/local/sbin/VMDump.sh
EOT
    fi
fi

#--------------------------------------------------------------------
if [ "T$LINUX_DIST" = 'TDEBIAN' ]
then
    # NFS server/client
    if [ $USE_DIALOG -ne 0 ]
    then
        whiptail --title='NFS Server' --nocancel \
            --defaultno --yesno 'Is this a NFS server ?' 7 40
        if [ $? -eq 0 ]
        then
            ANSWER='Y'
        else
            ANSWER='n'
        fi
    else
        read -rp 'Is this a NFS server [y|N] ? ' ANSWER
    fi
    if [ "T${ANSWER^^}" = 'TY' ]
    then
        $INSTALL_PROG install nfs-kernel-server
        LOCALIF=$(route -n | awk '/^0.0.0.0/{print $NF}')
        [ -z "$LOCALIP" ] && LOCALIP=$(ifconfig $LOCALIF | sed -n "s/.*inet \([0-9.]*\).*/\1/p")
        [ -z "$LOCALIP" ] && LOCALIP=$(ip addr list $LOCALIF | sed -n "s/.*inet \([0-9.]*\).*/\1/p")
        LOCALMASK=$(ifconfig $LOCALIF | sed -n -e 's/.*Mask:\(.*\)$/\1/p')
        [ -z "$LOCALMASK" ] && LOCALMASK=$(ifconfig $LOCALIF | awk '/netmask/ {print $4}')
        # From: http://www.routertech.org/viewtopic.php?t=1609
        l="${LOCALIP%.*}";r="${LOCALIP#*.}";n="${LOCALMASK%.*}";m="${LOCALMASK#*.}"
        LOCALNET=$((${LOCALIP%%.*}&${LOCALMASK%%.*})).$((${r%%.*}&${m%%.*})).$((${l##*.}&${n##*.})).$((${LOCALIP##*.}&${LOCALMASK##*.}))
        if [ $USE_DIALOG -ne 0 ]
        then
            whiptail --title='NFS Clients' --nocancel \
                --inputbox 'Name or IP of NFS client(s): ' 9 40 "$LOCALNET/$LOCALMASK" 2> $TFILE
            NFS_CLIENT=$(< $TFILE)
        else
            read -rp "Name or IP of NFS client(s) [ default=$LOCALNET/$LOCALMASK]: " NFS_CLIENT
        fi
        [ -z "$NFS_CLIENT" ] && NFS_CLIENT="$LOCALNET/$LOCALMASK"
        if [ $USE_DIALOG -ne 0 ]
        then
            whiptail --title='NFS Share' --nocancel \
                --inputbox 'Name of NFS share: ' 9 40 "/export" 2> $TFILE
            NFS_SHARE=$(< $TFILE)
        else
            read -rp 'Name of NFS share on server: ' NFS_SHARE
        fi
        if [ -n "$NFS_SHARE" ]
        then
            # Finally - we have all the necessary infos
            cat << EOT >> /etc/exports
$NFS_SHARE  $NFS_CLIENT(rw,async,subtree_check,no_root_squash)
EOT
            exportfs -a

            cat << EOT > /etc/default/nfs-common
# If you do not set values for the NEED_ options, they will be attempted
# autodetected; this should be sufficient for most people. Valid alternatives
# for the NEED_ options are "yes" and "no".

# Do you want to start the statd daemon? It is not needed for NFSv4.
NEED_STATD=

# Options for rpc.statd.
#   Should rpc.statd listen on a specific port? This is especially useful
#   when you have a port-based firewall. To use a fixed port, set this
#   this variable to a statd argument like: "--port 4000 --outgoing-port 4001".
#   For more information, see rpc.statd(8) or http://wiki.debian.org/?SecuringNFS
STATDOPTS="--port 32765 --outgoing-port 32766"

# Do you want to start the idmapd daemon? It is only needed for NFSv4.
NEED_IDMAPD=

# Do you want to start the gssd daemon? It is required for Kerberos mounts.
NEED_GSSD=
EOT
            service portmap restart
        fi
    else
        if [ $USE_DIALOG -ne 0 ]
        then
            whiptail --title='NFS Client' --nocancel \
                --defaultno --yesno 'Is this a NFS client ?' 7 40
            if [ $? -eq 0 ]
            then
                ANSWER='Y'
            else
                ANSWER='n'
            fi
        else
            read -rp 'Is this a NFS client [y|N] ? ' ANSWER
        fi
        if [ "T${ANSWER^^}" = 'TY' ]
        then
            $INSTALL_PROG install nfs-common

            if [ $USE_DIALOG -ne 0 ]
            then
                whiptail --title='NFS Server' --nocancel \
                    --inputbox 'Name or IP of NFS Server: ' 9 40 2> $TFILE
                NFS_SRV=$(< $TFILE)
            else
                read -rp 'Name or IP of NFS server: ' NFS_SRV
            fi
            if [ -n "$NFS_SRV" ]
            then
                if [ $USE_DIALOG -ne 0 ]
                then
                    whiptail --title='NFS Share' --nocancel \
                        --inputbox "Name of share on NFS Server '$NFS_SRV': " 9 40 2> $TFILE
                    NFS_SHARE=$(< $TFILE)
                else
                    read -rp "Name of NFS share on server on '$NFS_SRV': " NFS_SHARE
                fi
                if [ -n "$NFS_SHARE" ]
                then
                    if [ $USE_DIALOG -ne 0 ]
                    then
                        whiptail --title='NFS Mountpoint' --nocancel \
                            --inputbox "Name of mount point for share '$NFS_SHARE' on this NFS client: " 9 40 2> $TFILE
                        NFS_MP=$(< $TFILE)
                    else
                        read -rp "Mount point of NFS share '$NFS_SHARE' on this NFS client: " NFS_MP
                    fi
                    if [ -n "$NFS_MP" ]
                    then
                        # Finally - we have all the necessary infos
                        mkdir -p $NFS_MP
                        if [ -z "$(grep ^$NFS_SRV:$NFS_SHARE /etc/fstab)" ]
                        then
                            cat << EOT >> /etc/fstab
$NFS_SRV:$NFS_SHARE $NFS_MP nfs rw,vers=3,rsize=524288,wsize=524288,hard,proto=tcp,timeo=600,retrans=2,sec=sys,addr=$NFS_SRV 0 0
EOT
                        fi

                        # NFS write performance
                        # See https://docs.datafabric.hpe.com/62/AdministratorGuide/ConfigureNFSWrite.html
                        for P in tcp_slot_table_entries tcp_max_slot_table_entries udp_slot_table_entries
                        do
                            [ -z "$(grep $P /etc/modprobe.d/sunrpc.conf)" ] && echo "options sunrpc ${P}=128" >> /etc/modprobe.d/sunrpc.conf
                            echo 128 > /proc/sys/sunrpc/$P
                            sysctl -w "sunrpc.${P}=128"
                        done
                    fi
                fi
            fi
        fi
    fi
fi

# Run the CIS hardening script (without automatic reboot)
/usr/local/sbin/CIS.sh noreboot noreboot
