#!/bin/bash
#--------------------------------------------------------------------
# (c) CopyRight 2015 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#--------------------------------------------------------------------
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/FWStats.sh

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=${0##*/}

# Make sure we remove temporary files at exit
trap "rm -f /tmp/$$*" EXIT

DESIRED_DAY=${1-yesterday}

# First extract the logs for desired day
YESTERDAY=$(date +'%b %_d' -d "$DESIRED_DAY")
zegrep -a "^$YESTERDAY .*IN=.*OUT=" /var/log/kern.log* > $TFILE

# Get numbers
TOTAL=$(sed -n '$=' $TFILE)
GEO_REJ=$(egrep -c '(Geo-based rejection|Geo rejection| not from |C_NETS_)' $TFILE)
declare -A BLOCKED
BLOCKED_GEN=$(grep -c '] BLOCKED:' $TFILE)
BLOCKED_ALL=$BLOCKED_GEN
iptables -nvL | awk '/BL_.*BLOCKED:/{gsub(/"/,"");print $24}' | sort -u > $TFILE.BL
for BL in $(< $TFILE.BL)
do
    B=$(grep -c "$BL"' BLOCKED:' $TFILE)
    BLOCKED_ALL=$((BLOCKED_ALL + B))
    BLOCKED["$BL"]=$B
done
FLOOD=$(grep -c 'FLOOD' $TFILE)
MALFORMED=$(grep -c 'MALFORMED ' $TFILE)
FRAGMENTS=$(grep -c 'FRAGMENTS ' $TFILE)
NOSYN=$(grep -c 'NEW TCP w/o SYN' $TFILE)
PORTSCAN=$(grep -c 'Portscan' $TFILE)
FAIL2BAN=$(egrep -c 'f(ail)?2b(an)?-' $TFILE)
GEN_ISP_REJ=$(egrep -c 'IN-(external|ISP):' $TFILE)
GEN_LAN_REJ=$(egrep -c 'IN-(internal|LAN):' $TFILE)
GEN_DMZ_REJ=$(grep -c 'IN-DMZ:' $TFILE)
GEN_REJ=$(egrep '(Rejected (TCP|UDP)|(IN|OUT)-)' $TFILE | egrep -cv '\-((in|ex)ternal|DMZ|Portscan)')
OTHER=$((TOTAL - GEO_REJ - BLOCKED_ALL - FLOOD - MALFORMED - FRAGMENTS - NOSYN - PORTSCAN - -FAIL2BAN - GEN_ISP_REJ - GEN_LAN_REJ - GEN_DMZ_REJ - GEN_REJ))
[ $OTHER -lt 0 ] && OTHER=0

# Nicely report the numbers
cat << EOT > $TFILE.out
 Firewall rejection statistics for $(date +%F -d "$DESIRED_DAY")

 Total number of log entries : $(echo $TOTAL | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta')

 Blocked by geography        : $(echo $GEO_REJ | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta') ($(echo $TOTAL $GEO_REJ | awk '{ printf("%.2f%%\n", $2/($1/100)) }'))
EOT
if [ $GEO_REJ -gt 0 ]
then
    # Show the actual countries being blocked (along with their count)
    grep -o ' rejection [A-Z][A-Z] ' $TFILE | sed -e 's/[a-z ]//g' | sort | uniq -c | sort -bnr | \
      awk '{printf "  Blocked from %-14s: %d\n",$2,$1}' | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta' >> $TFILE.out
fi
cat << EOT >> $TFILE.out
 Blocked by blocklists       : $(echo $BLOCKED_ALL | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta') ($(echo $TOTAL $BLOCKED_ALL | awk '{ printf("%.2f%%\n", $2/($1/100)) }'))
EOT
for BL in $(< $TFILE.BL)
do
  printf "  Blocked by %-15s : " $BL >> $TFILE.out
  echo $(sed ':a;s/\B[0-9]\{3\}\>/,&/;ta' <<< ${BLOCKED["$BL"]})" ("$(echo $TOTAL ${BLOCKED["$BL"]} | awk '{ printf("%.2f%%\n", $2/($1/100)) }')")" >> $TFILE.out
done
cat << EOT >> $TFILE.out
  Blocked generically        : $(echo $BLOCKED_GEN | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta') ($(echo $TOTAL $BLOCKED_GEN | awk '{ printf("%.2f%%\n", $2/($1/100)) }'))
 Blocked malformed traffic   : $(echo $MALFORMED | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta') ($(echo $TOTAL $MALFORMED | awk '{ printf("%.2f%%\n", $2/($1/100)) }'))
 Blocked fragmented traffic  : $(echo $FRAGMENTS | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta') ($(echo $TOTAL $FRAGMENTS | awk '{ printf("%.2f%%\n", $2/($1/100)) }'))
 Blocked by flood protections: $(echo $FLOOD | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta') ($(echo $TOTAL $FLOOD | awk '{ printf("%.2f%%\n", $2/($1/100)) }'))
 Blocked by TCP w/o SYN      : $(echo $NOSYN | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta') ($(echo $TOTAL $NOSYN | awk '{ printf("%.2f%%\n", $2/($1/100)) }'))
 Blocked port scan           : $(echo $PORTSCAN | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta') ($(echo $TOTAL $PORTSCAN | awk '{ printf("%.2f%%\n", $2/($1/100)) }'))
 Blocked by 'fail2ban'       : $(echo $FAIL2BAN | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta') ($(echo $TOTAL $FAIL2BAN | awk '{ printf("%.2f%%\n", $2/($1/100)) }'))
 Blocked generically from ISP: $(echo $GEN_ISP_REJ | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta') ($(echo $TOTAL $GEN_ISP_REJ | awk '{ printf("%.2f%%\n", $2/($1/100)) }'))
 Blocked generically from LAN: $(echo $GEN_LAN_REJ | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta') ($(echo $TOTAL $GEN_LAN_REJ | awk '{ printf("%.2f%%\n", $2/($1/100)) }'))
 Blocked generically from DMZ: $(echo $GEN_DMZ_REJ | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta') ($(echo $TOTAL $GEN_DMZ_REJ | awk '{ printf("%.2f%%\n", $2/($1/100)) }'))
 Blocked generically         : $(echo $GEN_REJ | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta') ($(echo $TOTAL $GEN_REJ | awk '{ printf("%.2f%%\n", $2/($1/100)) }'))
 Other                       : $(echo $OTHER | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta') ($(echo $TOTAL $OTHER | awk '{ printf("%.2f%%\n", $2/($1/100)) }'))

EOT

# Show the top 10 source IP addresses
echo '  Top 10 source IP addresses' >> $TFILE.out
grep -o ' SRC=[0-9\.]* ' $TFILE | sed -e 's/[A-Z= ]//g' | sort | uniq -c | sort -bnr | head -n 10 | \
  awk '{printf " Blocked from %-15s: %d\n",$2,$1}' | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta' >> $TFILE.out
echo >> $TFILE.out

# Show the top 10 destination IP addresses
echo '  Top 10 destination IP addresses' >> $TFILE.out
grep -o ' DST=[0-9\.]* ' $TFILE | sed -e 's/[A-Z= ]//g' | sort | uniq -c | sort -bnr | head -n 10 | \
  awk '{printf " Blocked to %-17s: %d\n",$2,$1}' | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta' >> $TFILE.out
echo >> $TFILE.out

# Show the top 10 source ports
echo '  Top 10 source ports' >> $TFILE.out
echo '  (name for port: https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml)' >> $TFILE.out
grep -o ' SPT=[0-9\.]* ' $TFILE | sed -e 's/[A-Z= ]//g' | sort | uniq -c | sort -bnr | head -n 10 | \
  awk '{printf " Blocked from %-15s: %d\n",$2,$1}' >> $TFILE.out
echo >> $TFILE.out

# Show the top 10 destination ports
echo '  Top 10 destination ports' >> $TFILE.out
echo '  (name for port: https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml)' >> $TFILE.out
grep -o ' DPT=[0-9\.]* ' $TFILE | sed -e 's/[A-Z= ]//g' | sort | uniq -c | sort -bnr | head -n 10 | \
  awk '{printf " Blocked to %-17s: %d\n",$2,$1}' >> $TFILE.out
echo >> $TFILE.out

cat $TFILE.out

# We are done
exit 0
