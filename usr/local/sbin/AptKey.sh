#!/bin/bash
# Version 20240112-194143 checked into repository
################################################################
# (c) Copyright 2013-2018 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/AptKey.sh
# A quick script to emulate "apt-key" utility

# This must run as "root"
if [ $(/usr/bin/id -u) -ne 0 ]
then
    echo 'You must be root to continue'
    exit 0
fi

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE

#--------------------------------------------------------------------
# Get the key ID to install
# One way to find it is to run "apt-get update" and copy
#  the ID that is shown as "NO_PUBKEY"
if [ $# -ne 0 ]
then
    KEY="$1"
else
    read -r -p 'Key to install ? ' KEY
fi
[ -z "$KEY" ] && exit 1

# Get the key into the gpg keyring
gpg --verbose --keyserver keyserver.ubuntu.com --receive-keys $KEY || exit 1

# Export the key into /etc/apt/trusted.gpg.d
read -r -p 'Name for exported keys file ? ' KEY_NAME
if [ -s /etc/apt/trusted.gpg.d/${KEY_NAME}.asc ]
then
    # By default don't overwrite an existing file
    read -r -p "$KEY_NAME already exist - overwrite [y/N] ? " YN
    [ -z "$YN" ] && YN='N'
    [ "T${YN^^}" = 'TY' ] || exit 1
fi
gpg --output - --armor --export $KEY > /etc/apt/trusted.gpg.d/${KEY_NAME}.asc || exit 1
chmod 644 /etc/apt/trusted.gpg.d/${KEY_NAME}.asc

# Lastly delete the key in the gpg keyring
gpg --fingerprint --with-colons $KEY |\
  grep "^fpr" | sed -n 's/^fpr:::::::::\([[:alnum:]]\+\):/\1/p' |\
    xargs gpg --batch --delete-keys

#--------------------------------------------------------------------
# We are done
exit 0
