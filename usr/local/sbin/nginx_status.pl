#!/usr/bin/perl
#--------------------------------------------------------------------
# (c) CopyRight 2017 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#--------------------------------------------------------------------
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/nginx_status.pl
use 5.0010;

# Constants
my $ProgName = '';
( $ProgName = $0 ) =~ s%.*/%%;
my $CopyRight = "(c) CopyRight 2017 B-LUC Consulting Thomas Bullinger";

# Common sense options:
use strict qw(vars subs);
no warnings;
use warnings qw(FATAL closed threads internal debugging pack
    portable prototype inplace io pipe unpack malloc
    glob digit printf layer reserved taint closure
    semicolon);
no warnings qw(exec newline unopened);

#--------------------------------------------------------------------
# Needed packages
#--------------------------------------------------------------------
#use LWP::Simple;
use POSIX qw(setsid strftime);
use Getopt::Std;
use Sys::Syslog qw(:macros :standard);

#--------------------------------------------------------------------
# Globals
#--------------------------------------------------------------------
local $ENV{PATH} = '/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin';

# Program options
our $opt_d = 0;
our $opt_h = 0;
our $opt_D = 0;
our $opt_U = 'http://localhost/nginx_status';

#--------------------------------------------------------------------
# Become a daemon process
#--------------------------------------------------------------------
sub Daemonize()
{

    # pretty command line in ps
    $0 = join( ' ', $0, @ARGV ) unless ($opt_d);

    chdir '/' or die "Can't chdir to '/': $!";

    # Redirect STDIN and STDOUT
    open( STDIN,  '<', '/dev/null' ) or die "Can't read '/dev/null': $!";
    open( STDOUT, '>', '/dev/null' ) or die "Can't write '/dev/null': $!";
    defined( my $pid = fork ) or die "Can't fork: $!";

    if ($pid)
    {

        # The parent can die now
        print "DEBUG: Parent dies" if ($opt_d);
        exit;
    } ## end if ($pid)

    setsid or die "Can't start new session: $!";
    open STDERR, '>&STDOUT' or die "Can't duplicate stdout: $!";

    return (0);
} ## end sub Daemonize

#--------------------------------------------------------------------
# Get the status from NGINX, massage the output and write a log entry
#--------------------------------------------------------------------
sub GetStatus()
{
    my $LogName = '/var/log/nginx_status.log';

    my $status = '';
    my $Now = strftime( "%F,%T,%s", localtime );
    warn "Now = $Now" if ($opt_d);

    # Get the current load average
    my $Load = -1;
    if ( open( my $LA, '<', '/proc/loadavg' ) )
    {
        if ( <$LA> =~ /^(\S+)/o )
        {
            $Load = $1;
        } ## end if ( <$LA> =~ /^(\S+)/o...)
        close($LA);
    } ## end if ( open( my $LA, '<'...))
    warn "Load = $Load" if ($opt_d);

    # Get the status from NGINX (stub_status)
    my $content = get $opt_U;
    unless ( defined $content )
    {
        if ( open( my $GET, '-|', "wget -q --no-check-certificate -O - $opt_U"))
        {
            $content = join("\n", <$GET>);
            close ($GET);
        }
    }
    unless ( defined $content )
    {
        warn "Couldn't get $opt_U";
    } else
    {
        warn "content = $content" if ($opt_d);
        $status = 'ActiveConnections:';
        if ( $content =~ /^Active connections:\s+(\d+)/ )
        {
            $status .= "$1";
        } ## end if ( $content =~ /^Active connections:\s+(\d+)/...)
        $status .= ',';
        if ( $content =~ /(\d+)\s+(\d+)\s+(\d+)/ )
        {
            my $Accepts  = $1;
            my $Handled  = $2;
            my $Requests = $3;
            $status
                .= sprintf(
                "Accepts:%d,Handled:%d,Requests:%d,RequestsPerConnection:%.2f",
                $Accepts, $Handled, $Requests, $Requests / $Handled );

            # Warn about a possible overload
            syslog 4, 'Warning - Possible overload'
                if ( $Handled < ( $Accepts * 0.9 ) );
        } else
        {
            $status
                .= "Accepts:-1,Handled:-1,Requests:-1,RequestsPerConnection:-1";
        } ## end else [ if ( $content =~ /(\d+)\s+(\d+)\s+(\d+)/...)]

        # Get the number of max. possible connections (if possible)
        my $maxconn = 1024;    # A VERY conservative assumption
        if ( -f '/etc/nginx/nginx.conf' )
        {
            open(
                my $MC,
                '-|',
                "awk '/worker_processes/{mp=int(\$2)};/worker_connections/{mc=int(\$2)}END{print int(mp*mc)}' /etc/nginx/nginx.conf"
            );
            $maxconn = <$MC>;
            chomp($maxconn);
            close($MC);
        } ## end if ( -f '/etc/nginx/nginx.conf'...)
        $status .= ",MaxConnections:$maxconn";

        warn "status = $status" if ($opt_d);
    } ## end else

    # Log the stats
    open my $LF, '>>', $LogName or die "$@";
    print $LF "$Now,Load:$Load,$status\n";
    close($LF);

    # Rotate the log file if it is larger than 1 GB
    my $LogSize = (stat $LogName)[7];
    if (($LogSize / (1024 * 1024)) > 1024 )
    {
        # Rotate the log file
        rename $LogName, $LogName . '.old';
    }

    syslog 6, "Load:%.2f,%s", $Load, $status;
    return ("$Now,Load:$Load,$status");
} ## end sub GetStatus

#--------------------------------------------------------------------
# Display the usage
#--------------------------------------------------------------------
sub ShowUsage()
{

    print "Usage: $ProgName [options]\n",
        "       -D                 Run as daemon [default=no]\n",
        "       -U <URL>           Specify NGINX server to monitor [default='$opt_U']\n",
        "       -h                 Show this help [default=no]\n",
        "       -d                 Show some debug info on STDERR [default=no]\n";

    exit 0;
} ## end sub ShowUsage

#--------------------------------------------------------------------
# Main function
#--------------------------------------------------------------------
$|++;
print "$ProgName\n$CopyRight\n\n";

# Get possible options
getopts('dDhU:') or ShowUsage();
ShowUsage() if ($opt_h);

# Become a daemon process (if specified)
Daemonize if ($opt_D);

# Write the PID
if ( open( my $PF, '>', '/var/run/nginx_status.pid' ) )
{
    print $PF "$$\n";
    close($PF);
} ## end if ( open( my $PF, '>'...))

# Open the connection to the local syslog daemon
openlog "$ProgName", LOG_PID, LOG_DAEMON;

# Eternal loop
while (1)
{
    # Get the current status
    my $Stat = GetStatus();
    warn "Stat = $Stat" if ($opt_d);

    # We are done unless we are running in daemon mode
    last unless ($opt_D);

    # Determine the correct sleep time
    my ( undef, undef, $Stat_time ) = split( ',', $Stat );
    my $Sleep_time = 60 - ( time - $Stat_time );
    warn "Stat_time = $Stat_time; Sleep_time = $Sleep_time" if ($opt_d);
    sleep $Sleep_time;
} ## end while (1)
closelog;

# We are done
warn "We are done" if ($opt_d);
exit(0);
__END__
