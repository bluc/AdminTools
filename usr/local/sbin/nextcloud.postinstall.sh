#!/bin/bash
# Version 20230402-201846 checked into repository
################################################################
# (c) Copyright 2018 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/elk-docker.postinstall.sh
# Based on https://blog.ssdnodes.com/blog/installing-nextcloud-docker/

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi
# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE

#--------------------------------------------------------------------
# Specifying "-x" to the bash invocation = DEBUG
DEBUG=''
[[ $- ==  *x* ]] && DEBUG='-x'

#--------------------------------------------------------------------
# This host and domain
THISHOST=$(hostname)
[[ $THISHOST == *.* ]] || THISHOST=$(hostname -f)
THISDOMAIN=${THISHOST#*.}

if [ ! -x /usr/bin/joe ]
then
    echo "ERROR: Download and run 'os.postinstall.sh' first"
    exit 1
fi

#--------------------------------------------------------------------
# Install "nextcloud" snap package
[ -x /usr/bin/snap ] || apt-get install snap snapd
snap install nextcloud
LOCALIF=$(ip route get 8.8.8.8 | awk '/via/{print $5; exit}')
if [ -x /sbin/ip ]
then
    LOCALIP=$(ip addr list $LOCALIF | sed -n "s/.*inet \([0-9.]*\).*/\1/p")
elif [ -x /sbin/ifconfig ]
then
    LOCALIP=$(ifconfig $LOCALIF | sed -n "s/.*inet addr:\([0-9.]*\).*/\1/p")
    [ -z "$LOCALIP" ] && LOCALIP=$(ifconfig $LOCALIF | sed -n "s/.*inet \([0-9.]*\).*/\1/p")
fi
nextcloud.occ config:system:set trusted_domains 1 --value=nextcloud.$THISDOMAIN
[ -z "$LOCALIP" ] || nextcloud.occ config:system:set trusted_domains 2 --value=$LOCALIP
if [ $? -ne 0 ]
then
    # Work around a possible bug
    if [ -z "$(grep trusted_domains /var/snap/nextcloud/current/nextcloud/config/config.php)" ]
    then
        sed -i "s/^);/ trusted_domains => [ 'nextcloud.$THISDOMAIN' ],\n);/" /var/snap/nextcloud/current/nextcloud/config/config.php
        vim.tiny +/trusted_domains /var/snap/nextcloud/current/nextcloud/config/config.php
    fi
fi
cat << EOT
Add something like this to the file /var/snap/nextcloud/current/nextcloud/config/config.php
 if you want to enable user authentication via an IMAP server (only if you have the
 corresponding APP installed):

  'user_backends' => 
  array (
    0 => 
    array (
      'class' => 'OC_User_IMAP',
      'arguments' => 
      array (
        0 => 'ip.add.re.ss', # IP of the IMAP server
        1 => port,  # 143 or 993
        2 => NULL,  # Set to 'ssl' if using port 443
        3 => 'dom.ain',  # The domain for IMAP users
        4 => true,
        5 => false,
      ),
    ),
  ),
Reference: https://github.com/nextcloud/user_external#readme
EOT
read -t 10 -p 'Press ENTER to continue or wait 10 seconds' E
snap restart nextcloud

#--------------------------------------------------------------------
# Adapt firewall
vim.tiny +/http /etc/firehol/firehol.conf

#--------------------------------------------------------------------
# Install mail client if necessary
#-------------------------------------------------------------------------
# Install MTA if none present
if [ ! -s /usr/sbin/sendmail ]
then
    apt-get install ssmtp
    MH=$(dig +short mail 2> /dev/null)
    read -p "IP address of mail host [default="$MH"] ? " MR
    [ -z "$MR" ] && MR="$MH"
    cat > /etc/ssmtp/ssmtp.conf << EOT
# DO NOT DELETE THIS LINE: nextcloud email config
# The person who gets all mail for userids < 1000
# Make this empty to disable rewriting.
root=postmaster
mailhub=$MR
#UseSTARTTLS=NO
UseTLS=NO
#AuthUser=username
#AuthPass=password
#AuthMethod=LOGIN

# Where will the mail seem to come from?
rewriteDomain=$THISDOMAIN

# The full hostname
hostname=$THISHOST

# Are users allowed to set their own From: address?
# YES - Allow the user to specify their own From: address
# NO - Use the system generated From: address
FromLineOverride=YES
EOT
    vim.tiny /etc/ssmtp/ssmtp.conf
fi

#--------------------------------------------------------------------
# We are done
exit 0
