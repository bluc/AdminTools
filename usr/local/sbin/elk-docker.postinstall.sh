#!/bin/bash
################################################################
# (c) Copyright 2018 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/elk-docker.postinstall.sh
# Based on https://logz.io/learn/complete-guide-elk-stack/

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi
# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE

#--------------------------------------------------------------------
# This host and domain
THISHOST=$(hostname)
[[ $THISHOST == *.* ]] || THISHOST=$(hostname -f)
THISDOMAIN=${THISHOST#*.}

#--------------------------------------------------------------------
function InstallServer ()
{
    #--------------------------------------------------------------------
    # Sanity checks
    if [ -s /etc/lsb-release ]
    then
        source /etc/lsb-release
        if [ "T${DISTRIB_ID^^}" != 'TUBUNTU' ]
        then
            echo 'This is not an Ubuntu Server'
            exit 0
        fi
    else
        echo 'This is not an Ubuntu Server'
        exit 0
    fi

    #--------------------------------------------------------------------
    # Remove local firehol (docker will manage the local firewall)
    apt-get purge firehol
    rm -f /usr/local/sbin/UpdateBlocklists*

    #--------------------------------------------------------------------
    # Check installed RAM
    if [ $(awk '/MemTotal/{print $2}' /proc/meminfo) -lt $((4 * 1024 * 1024)) ]
    then
        echo "WARNING: This server has less than 4GB RAM"
        read -rp 'Press ENTER to continue' E
    fi

    #--------------------------------------------------------------------
    # Install the most current docker utilities
    dpkg -S docker.io &> /dev/null
    HAVE_DOCKER=$?
    dpkg -S docker-compose &> /dev/null
    HAVE_DOCKER=$((HAVE_DOCKER + $?))
    if [ $HAVE_DOCKER -ne 0 ]
    then
        # Docker itself
        curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
        echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" > /etc/apt/sources.list.d/docker.list
        # AquaSecurity's trivy tool
        # See https://github.com/aquasecurity/trivy
        apt-get -y install apt-transport-https gnupg lsb-release
        wget -qO - https://aquasecurity.github.io/trivy-repo/deb/public.key | apt-key add -
        echo "deb https://aquasecurity.github.io/trivy-repo/deb $(lsb_release -sc) main" > /etc/apt/sources.list.d/trivy.list
        apt-get update
        apt-get install docker-compose trivy
    fi

    #--------------------------------------------------------------------
    # Install JSON pretty print processor
    for P in jq
    do
        dpkg-query -S $P &> /dev/null
        [ $? -ne 0 ] || apt-get -y install $P
    done

    while true
    do
        read -rp 'Use [O]pensearch or [E]asticsearch as ELK Stack ? ' OE
        case ${OE^^} in
        O|E) break
            ;;
        *)  echo 'Your must select the ELK stack to install'
            continue
            ;;
        esac
    done
    
    mkdir -p /usr/local/elk
    if [ "T${OE^^}" = 'TE' ]
    then
        ### *** Elasticsearch *** ###

        #--------------------------------------------------------------------
        # Create the script for "docker-compose"
        # Copied from https://www.elastic.co/guide/en/elastic-stack-get-started/current/get-started-docker.html
        cat << EOT > /usr/local/elk/docker-compose.yml
version: '2.2'
services:
  es01:
    image: docker.elastic.co/elasticsearch/elasticsearch-oss:7.9.1-amd64
    container_name: es01
    environment:
      - node.name=es01
      - cluster.name=es-docker-cluster
      - discovery.seed_hosts=es02,es03
      - cluster.initial_master_nodes=es01,es02,es03
      - bootstrap.memory_lock=true
      - "ES_JAVA_OPTS=-Xms2g -Xmx2g -Dlog4j2.formatMsgNoLookups=true"
    ulimits:
      memlock:
        soft: -1
        hard: -1
    volumes:
      - data_es01:/usr/share/elasticsearch/data
      - modules_es01:/usr/share/elasticsearch/modules
      - config_es01:/usr/share/elasticsearch/config
    ports:
      - 9200:9200
    networks:
      - elastic
    logging:
      driver: syslog
      options:
        syslog-address: "tcp://127.0.0.1:5000"
        tag: "docker-es"

  es02:
    image: docker.elastic.co/elasticsearch/elasticsearch-oss:7.9.1-amd64
    container_name: es02
    environment:
      - node.name=es02
      - cluster.name=es-docker-cluster
      - discovery.seed_hosts=es01,es03
      - cluster.initial_master_nodes=es01,es02,es03
      - bootstrap.memory_lock=true
      - "ES_JAVA_OPTS=-Xms2g -Xmx2g -Dlog4j2.formatMsgNoLookups=true"
    ulimits:
      memlock:
        soft: -1
        hard: -1
    volumes:
      - data_es02:/usr/share/elasticsearch/data
      - modules_es02:/usr/share/elasticsearch/modules
      - config_es02:/usr/share/elasticsearch/config
    ports:
      - 9201:9201
    networks:
      - elastic
    logging:
      driver: syslog
      options:
        syslog-address: "tcp://127.0.0.1:5000"
        tag: "docker-es"

  es03:
    image: docker.elastic.co/elasticsearch/elasticsearch-oss:7.9.1-amd64
    container_name: es03
    environment:
      - node.name=es03
      - cluster.name=es-docker-cluster
      - discovery.seed_hosts=es01,es02
      - cluster.initial_master_nodes=es01,es02,es03
      - bootstrap.memory_lock=true
      - "ES_JAVA_OPTS=-Xms2g -Xmx2g -Dlog4j2.formatMsgNoLookups=true"
    ulimits:
      memlock:
        soft: -1
        hard: -1
    volumes:
      - data_es03:/usr/share/elasticsearch/data
      - modules_es03:/usr/share/elasticsearch/modules
      - config_es03:/usr/share/elasticsearch/config
    ports:
      - 9202:9202
    networks:
      - elastic
    logging:
      driver: syslog
      options:
        syslog-address: "tcp://127.0.0.1:5000"
        tag: "docker-es"

  kib01:
    image: docker.elastic.co/kibana/kibana-oss:7.9.1
    container_name: kib01
    volumes:
      - config_kib01:/usr/share/kibana/config
    ports:
      - 5601:5601
    environment:
      ELASTICSEARCH_URL: http://es01:9200
      ELASTICSEARCH_HOSTS: http://es01:9200
    networks:
      - elastic
    logging:
      driver: syslog
      options:
        syslog-address: "tcp://127.0.0.1:5000"
        tag: "docker-kib"

volumes:
  data_es01:
    driver: local
  modules_es01:
    driver: local
  config_es01:
    driver: local
  data_es02:
    driver: local
  modules_es02:
    driver: local
  config_es02:
    driver: local
  data_es03:
    driver: local
  modules_es03:
    driver: local
  config_es03:
    driver: local
  config_kib01:
    driver: local

networks:
  elastic:
    driver: bridge
EOT

        # Create the start script for the ELK stack
        cat << EOT > /usr/local/sbin/elk.sh
#!/bin/bash
################################################################
# (c) Copyright 2020 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/os.postinstall.sh

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=\${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/\$PROG.lock
if [ -f \$LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=\$(< \$LOCKFILE)
    [ -z "\$(ps h -p \$MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f \$LOCKFILE /tmp/\$\$*" EXIT
echo "\$\$" > \$LOCKFILE            

#--------------------------------------------------------------------
# Specifying "-x" to the bash invocation = DEBUG
DEBUG=''
[[ \$- ==  *x* ]] && DEBUG='-x'

#--------------------------------------------------------------------
# Ensure that there is enough memory for ElasticSearch
[ \$(sysctl -n vm.max_map_count) -lt 262144 ] && sysctl -qw vm.max_map_count=262144

#--------------------------------------------------------------------
# Get the firewall right
date "+%F %T \$PROG Restarting docker service" >> /var/log/docker.log
service docker restart

#--------------------------------------------------------------------
# See https://stackoverflow.com/questions/42695004/importerror-no-module-named-ssl-match-hostname-when-importing-the-docker-sdk-fo
[ -e /usr/lib/python2.7/dist-packages/backports/__init__.py ] || pycompile -p python-backports.ssl-match-hostname

#--------------------------------------------------------------------
# Start the ELK containers
cd /usr/local/elk
docker-compose up -d >> /var/log/docker.log 2>&1

#--------------------------------------------------------------------
# Increase available buffer size for ElasticSearch indeces
# See https://sematext.com/blog/elasticsearch-refresh-interval-vs-indexing-performance/
cat << EOCF > /tmp/elasticsearch.yml
cluster.name: "docker-cluster"
network.host: 0.0.0.0
indices.memory.index_buffer_size: 30%
EOCF
RESTART_ELK=0
for F in /var/lib/docker/volumes/elk_config_es0*/_data/elasticsearch.yml
do
    [ -s \$F ] || continue
    diff /tmp/elasticsearch.yml \$F &>/dev/null
    if [ \$? -ne 0 ]
    then
        cat /tmp/elasticsearch.yml > \$F
        RESTART_ELK=1
    fi
done
if [ \$RESTART_ELK -ne 0 ]
then
   sleep 120
   docker-compose down >> /var/log/docker.log 2>&1
   sleep 30
   docker-compose up -d >> /var/log/docker.log 2>&1
fi

#--------------------------------------------------------------------
# Define pipeline for geoip processing
date "+%F %T \$PROG Waiting for ELK stack to be ready" >> /var/log/docker.log
sleep 120
date "+%F %T \$PROG Adding Kibana prepocessing" >> /var/log/docker.log
while true
do
    curl -s -X PUT "localhost:9200/_ingest/pipeline/geoip?pretty" -H 'Content-Type: application/json' -d'
{
  "description" : "Preprocess JSON and add geoip info",
  "processors" : [
    {
      "json" : {
        "field" : "all-json",
        "add_to_root" : true,
        "on_failure" : [
          {
            "set" : {
              "field" : "json_error_message",
              "value" : "{{ _ingest.on_failure_message }}"
            }
          }
        ]
      }
    },
    {
      "set": {
        "field": "event.tags",
        "value": "unprocessed",
        "override": false,
        "on_failure" : [
          {
            "set" : {
              "field" : "set_error_message",
              "value" : "{{ _ingest.on_failure_message }}"
            }
          }
        ]
      }
    },
    {
      "set": {
        "field": "geoip.country_iso_code",
        "value": "local",
        "override": false,
        "on_failure" : [
          {
            "set" : {
              "field" : "set_error_message",
              "value" : "{{ _ingest.on_failure_message }}"
            }
          }
        ]
      }
    },
    {
      "set": {
        "field": "src_country",
        "value": "local",
        "override": false,
        "on_failure" : [
          {
            "set" : {
              "field" : "set_error_message",
              "value" : "{{ _ingest.on_failure_message }}"
            }
          }
        ]
      }
    },
    {
      "set": {
        "field": "dst_country",
        "value": "local",
        "override": false,
        "on_failure" : [
          {
            "set" : {
              "field" : "set_error_message",
              "value" : "{{ _ingest.on_failure_message }}"
            }
          }
        ]
      }
    },
    {
      "geoip" : {
        "field" : "ip",
        "on_failure" : [
          {
            "set" : {
              "field" : "geoip_error_message",
              "value" : "{{ _ingest.on_failure_message }}"
            }
          }
        ]
      }
    }
  ]
}
' &> /dev/null
    [ \$? -eq 0 ] && break
    sleep 2
done

#--------------------------------------------------------------------
# Increase index ingestion performance
# See https://sematext.com/blog/elasticsearch-refresh-interval-vs-indexing-performance/
curl -X PUT localhost:9200/_settings/?pretty -H 'Content-Type: application/json' \\
  -d'{"index":{"refresh_interval":"5s"}}' &> /dev/null

#--------------------------------------------------------------------
# You can check the installed images using this script:
#for DI in \$(docker images | awk '!/^REPOSITORY/{print \$1":"\$2}')
#do
#    trivy -d image -f json -o \${DI//\//_}.trivy-results.json \$DI
#done

#--------------------------------------------------------------------
# We are done
date "+%F %T \$PROG ELK stack ready" >> /var/log/docker.log
echo >> /var/log/docker.log
exit 0
EOT
        chmod 744 /usr/local/sbin/elk.sh

        # Create the backup script for the ELK stack
        cat << EOT > /usr/local/sbin/elk-backup.sh
#!/bin/bash
################################################################
# (c) Copyright 2020 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/os.postinstall.sh

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=\${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/\$PROG.lock
if [ -f \$LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=\$(< \$LOCKFILE)
    [ -z "\$(ps h -p \$MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f \$LOCKFILE /tmp/\$\$*" EXIT
echo "\$\$" > \$LOCKFILE            

#--------------------------------------------------------------------
# Specifying "-x" to the bash invocation = DEBUG
DEBUG=''
[[ \$- ==  *x* ]] && DEBUG='-x'

#--------------------------------------------------------------------
# The number of CPU cores in the system
NUM_CPU=\$(grep -c 'core id' /proc/cpuinfo)
if [ \$NUM_CPU -ge 4 ]
then
    CONCURRENT=\$((\$NUM_CPU / 2))
else
    CONCURRENT=1
fi

#--------------------------------------------------------------------
# Save elasticsearch indeces into /var/lib/docker/snapshots
ESBKP_DIR='/var/lib/docker/snapshots'
mkdir -p \$ESBKP_DIR

# The list of current indeces
wget -qO - 'http://127.0.0.1:9200/_cat/indices/' | awk '{print \$3}' > \${ESBKP_DIR}/es_indeces

# The IP address for the ElasticSearch cluster
ES_IP=\$(docker inspect  elk_elastic | awk '/Gateway/{gsub(/"/,"");print \$NF}')

#--------------------------------------------------------------------
# Always save the Kibana settings
INDEX='.kibana_1'
rm -f \${ESBKP_DIR}/\${INDEX}.*

if [ -s \${ESBKP_DIR}/\${INDEX}.bz2 ]
then
    TZ='UTC' date "+%a, %d %b %Y %T GMT | Preserving \${ESBKP_DIR}/\${INDEX}.bz2" >> \${ESBKP_DIR}/\${INDEX}.log
    rm -f \${ESBKP_DIR}/\${INDEX}.old
    mv -f \${ESBKP_DIR}/\${INDEX} \${ESBKP_DIR}/\${INDEX}.old
fi

# Save the index
TZ='UTC' date "+%a, %d %b %Y %T GMT | Saving \$INDEX to \$ESBKP_DIR" >> \${ESBKP_DIR}/\${INDEX}.log
docker run --rm -v \${ESBKP_DIR}:\${ESBKP_DIR} elasticdump/elasticsearch-dump \\
  --input=http://\${ES_IP}:9200/\$INDEX --output=\${ESBKP_DIR}/\${INDEX} \\
  --overwrite --limit 1000 --concurrency \$CONCURRENT >> \${ESBKP_DIR}/\${INDEX}.log 2>&1
echo >> \${ESBKP_DIR}/\${INDEX}.log

# Remove the older backup
if [ -s \${ESBKP_DIR}/\${INDEX} ]
then
    TZ='UTC' date "+%a, %d %b %Y %T GMT | Compressing \${ESBKP_DIR}/\$INDEX" >> \${ESBKP_DIR}/\${INDEX}.log
    bzip2 -9 \${ESBKP_DIR}/\${INDEX}
    [ \$? -eq 0 ] && rm -f \${ESBKP_DIR}/\${INDEX}.old
fi

# Keep the kibana log file at a reasonable size
CURLINES=\$(sed -n '\$=' \${ESBKP_DIR}/\${INDEX}.log)
if [ \$CURLINES -gt 10000 ]
then
    LINES2DEL=\$((\$CURLINES - 10000))
    sed -i -e "1,\${LINES2DEL}d" \${ESBKP_DIR}/\${INDEX}.log
fi

#--------------------------------------------------------------------
# Save yesterday's index
INDEX='rsyslog-'\$(date "+%Y.%m.%d" -d yesterday)
if [ ! -z "\$(grep \$INDEX \${ESBKP_DIR}/es_indeces)" ]
then
    for TYPE in settings analyzer data mapping alias template component_template index_template
    do
        # Save any existing backup
        if [ -s \${ESBKP_DIR}/\${INDEX}.\${TYPE}.bz2 ]
        then
            TZ='UTC' date "+%a, %d %b %Y %T GMT | Preserving \${ESBKP_DIR}/\${INDEX}.\${TYPE}.bz2" >> \${ESBKP_DIR}/\${INDEX}.log
            [ -s \${ESBKP_DIR}/\${INDEX}.\${TYPE}.bz2.old ] && rm -f \${ESBKP_DIR}/\${INDEX}.\${TYPE}.bz2.old
            mv -f \${ESBKP_DIR}/\${INDEX}.\${TYPE}.bz2 \${ESBKP_DIR}/\${INDEX}.\${TYPE}.bz2.old
        fi

        # Save the index (based on type)
        TZ='UTC' date "+%a, %d %b %Y %T GMT | Saving \${INDEX}.\${TYPE} to \$ESBKP_DIR" >> \${ESBKP_DIR}/\${INDEX}.log
        docker run --rm -v \${ESBKP_DIR}:\${ESBKP_DIR} elasticdump/elasticsearch-dump \\
          --input=http://\${ES_IP}:9200/\$INDEX --output=\${ESBKP_DIR}/\${INDEX}.\${TYPE} \\
          --type=\$TYPE --overwrite --limit 1000 --concurrency \$CONCURRENT >> \${ESBKP_DIR}/\${INDEX}.log 2>&1

        if [ -s \${ESBKP_DIR}/\${INDEX}.\${TYPE} ]
        then
            TZ='UTC' date "+%a, %d %b %Y %T GMT | Compressing \${ESBKP_DIR}/\${INDEX}.\${TYPE}" >> \${ESBKP_DIR}/\${INDEX}.log
            bzip2 -9 \${ESBKP_DIR}/\${INDEX}.\${TYPE}
            [ -s \${ESBKP_DIR}/\${INDEX}.\${TYPE}.bz2 ] && rm -f \${ESBKP_DIR}/\${INDEX}.\${TYPE}.bz2.old
        fi
    done
fi

#--------------------------------------------------------------------
# Get all saved elasticsearch and kibana configurations
curl -s 'http://localhost:9200/.kibana/_search?size=10000&pretty' > \${ESBKP_DIR}/Kibana-Backup.json 2> /dev/null
[ -s \${ESBKP_DIR}/Kibana-Backup.json ] && bzip2 -f \${ESBKP_DIR}/Kibana-Backup.json
for KTYPE in 'config' 'map' 'canvas-workpad' 'canvas-element' 'index-pattern' 'visualization' 'search' 'dashboard' 'url'
do
    curl -s -X POST "http://localhost:5601/api/saved_objects/_export" -H 'kbn-xsrf: true' \\
      -H 'Content-Type: application/json' -d" { \"type\": \"\${KTYPE}\", \"excludeExportDetails\": true } " |\\
      jq -r '.' > \${ESBKP_DIR}/Kibana-Backup.\${KTYPE}.json && bzip2 -f \${ESBKP_DIR}/Kibana-Backup.\${KTYPE}.json
    TZ='UTC' date "+%a, %d %b %Y %T GMT | Saved \$KTYPE configuration" >> \${ESBKP_DIR}/config.log
done
echo >> \${ESBKP_DIR}/config.log

# Keep the config log file at a reasonable size
CURLINES=\$(sed -n '\$=' \${ESBKP_DIR}/config.log)
if [ \$CURLINES -gt 10000 ]
then
    LINES2DEL=\$((\$CURLINES - 10000))
    sed -i -e "1,\${LINES2DEL}d" \${ESBKP_DIR}/config.log
fi

#--------------------------------------------------------------------
cat << EORM > \${ESBKP_DIR}/Kibana-Backup.README
# Restore the saved data using this script:
cd \$ESBKP_DIR
for F in Kibana-Backup*.json.bz2
do
   [ -s \\\$F ] && bunzip2 -f \\\$F
done
for F in Kibana-Backup*.json
do
   [ -s \\\$F ] && jq -c '.' \\\$F > \\\${F}.ndjson
done
for KTYPE in 'config' 'index-pattern' 'visualization' 'search' 'map' 'canvas-workpad' 'canvas-element' 'dashboard' 'url'
do
    if [ -s Kibana-Backup.\\\${KTYPE}.json.ndjson ]
    then
        curl --fail -X POST "http://localhost:5601/api/saved_objects/_import?overwrite=true" \\\\
          -H 'kbn-xsrf: true' \\\\
          --form file=@Kibana-Backup.\\\${KTYPE}.json.ndjson
    fi
done
EORM

#--------------------------------------------------------------------
# Remove any saved data older than a week
find \$ESBKP_DIR -type f -mtime +7 -delete

#--------------------------------------------------------------------
# We are done
exit 0
EOT
        chmod 744 /usr/local/sbin/elk-backup.sh
    else
        ### *** Opensearch *** ###

        #--------------------------------------------------------------------
        # Create the script for "docker-compose"
        # See https://opensearch.org/versions/opensearch-1-0-0.html#
        wget -qO /usr/local/elk/docker-compose.yml 'https://opensearch.org/samples/docker-compose.yml'

        # Create the start script for the ELK stack
        cat << EOT > /usr/local/sbin/elk.sh
#!/bin/bash
################################################################
# (c) Copyright 2020 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/os.postinstall.sh

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=\${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/\$PROG.lock
if [ -f \$LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=\$(< \$LOCKFILE)
    [ -z "\$(ps h -p \$MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f \$LOCKFILE /tmp/\$\$*" EXIT
echo "\$\$" > \$LOCKFILE            

#--------------------------------------------------------------------
# Specifying "-x" to the bash invocation = DEBUG
DEBUG=''
[[ \$- ==  *x* ]] && DEBUG='-x'

#--------------------------------------------------------------------
# Ensure that there is enough memory for ElasticSearch
[ \$(sysctl -n vm.max_map_count) -lt 262144 ] && sysctl -qw vm.max_map_count=262144

#--------------------------------------------------------------------
# Get the firewall right
date "+%F %T \$PROG Restarting docker service" >> /var/log/docker.log
service docker restart

#--------------------------------------------------------------------
# See https://stackoverflow.com/questions/42695004/importerror-no-module-named-ssl-match-hostname-when-importing-the-docker-sdk-fo
[ -d /usr/lib/python3 ] || [ -e /usr/lib/python2.7/dist-packages/backports/__init__.py ] || pycompile -p python-backports.ssl-match-hostname

#--------------------------------------------------------------------
# Start the ELK containers
cd /usr/local/elk
docker-compose up -d >> /var/log/docker.log 2>&1

#--------------------------------------------------------------------
# Get the docker ID for the first node
DNID=\$(docker ps | awk '/node1/{print \$1}')
if [ -n "\$DNID" ]
then
    docker cp \${DNID}:/usr/share/opensearch/config/root-ca.pem /etc/rsyslog.d/opensearch.root-ca.pem
fi

#--------------------------------------------------------------------
# You can check the installed images using this script:
#for DI in \$(docker images | awk '!/^REPOSITORY/{print \$1":"\$2}')
#do
#    trivy -d image -f json -o \${DI//\//_}.trivy-results.json \$DI
#done

#--------------------------------------------------------------------
# We are done
date "+%F %T \$PROG ELK stack ready" >> /var/log/docker.log
echo >> /var/log/docker.log
exit 0
EOT
        chmod 744 /usr/local/sbin/elk.sh
    fi

    # Create a suitable cron job for ELK
    if [ -z "$(grep -h '^@reboot.*elk\.sh$' /etc/cron.d/*)" ]
    then
        cat << EOCJ > /etc/cron.d/elk
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/sbin:/sbin:/usr/local/bin:/usr/bin:/bin:/snap/bin
#MAILTO=<FILL IN THE CORRECT EMAIL ADDRESS>
#===============================================================
# Start the ELK stack at boot time
@reboot root    /usr/local/sbin/elk.sh
# Restart it when needed
EOCJ
        if [ "T${OE^^}" = 'TE' ]
        then
            cat << EOCJ >> /etc/cron.d/elk
*/2 * * * * root [ \$(docker ps | grep -c elastic) -lt 4 ] && /usr/local/sbin/elk.sh
EOCJ
        else
            cat << EOCJ >> /etc/cron.d/elk
*/2 * * * * root [ \$(docker ps | grep -c opensearch-node) -lt 2 ] && /usr/local/sbin/elk.sh
EOCJ
        fi
    fi

# Doesn't work with ElasticSearch version 7.9
#TB    #--------------------------------------------------------------------
#TB    # Install elastalert
#TB    for P in elastalert
#TB    do
#TB        dpkg-query -S $P &> /dev/null
#TB        [ $? -ne 0 ] && apt-get -y install $P
#TB    done
#TB    mkdir -p /etc/elastalert/rules
#TB    cat << EOT > /etc/elastalert/config.yaml
#TB# This is the folder that contains the rule yaml files
#TB# Any .yaml file will be loaded as a rule
#TBrules_folder: /etc/elastalert/rules
#TB
#TB# How often ElastAlert will query Elasticsearch
#TB# The unit can be anything from weeks to seconds
#TBrun_every:
#TB  minutes: 1
#TB
#TB# ElastAlert will buffer results from the most recent
#TB# period of time, in case some log sources are not in real time
#TBbuffer_time:
#TB  minutes: 15
#TB
#TB# The Elasticsearch hostname for metadata writeback
#TB# Note that every rule can have its own Elasticsearch host
#TBes_host: localhost
#TB
#TB# The Elasticsearch port
#TBes_port: 9200
#TB
#TB# The AWS region to use. Set this when using AWS-managed elasticsearch
#TB#aws_region: us-east-1
#TB
#TB# The AWS profile to use. Use this if you are using an aws-cli profile.
#TB# See http://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html
#TB# for details
#TB#profile: test
#TB
#TB# Optional URL prefix for Elasticsearch
#TB#es_url_prefix: elasticsearch
#TB
#TB# Connect with TLS to Elasticsearch
#TB#use_ssl: True
#TB
#TB# Verify TLS certificates
#TB#verify_certs: True
#TB
#TB# GET request with body is the default option for Elasticsearch.
#TB# If it fails for some reason, you can pass 'GET', 'POST' or 'source'.
#TB# See http://elasticsearch-py.readthedocs.io/en/master/connection.html?highlight=send_get_body_as#transport
#TB# for details
#TB#es_send_get_body_as: GET
#TB
#TB# Option basic-auth username and password for Elasticsearch
#TB#es_username: someusername
#TB#es_password: somepassword
#TB
#TB# Use SSL authentication with client certificates client_cert must be
#TB# a pem file containing both cert and key for client
#TB#verify_certs: True
#TB#ca_certs: /path/to/cacert.pem
#TB#client_cert: /path/to/client_cert.pem
#TB#client_key: /path/to/client_key.key
#TB
#TB# The index on es_host which is used for metadata storage
#TB# This can be a unmapped index, but it is recommended that you run
#TB# elastalert-create-index to set a mapping
#TBwriteback_index: elastalert_status
#TB
#TB# If an alert fails for some reason, ElastAlert will retry
#TB# sending the alert until this time period has elapsed
#TBalert_time_limit:
#TB  days: 2
#TBEOT
#TB    # Create the index for the elastalert utility
#TB    elastalert-create-index --config /etc/elastalert/config.yaml
#TB
#TB    # Create rules for elastalert
#TB
#TB    # Keep elastalert running via cron
#TB    grep -m 3 '^[SPM]' /etc/cron.d/bluc > /etc/cron.d/elastalert
#TB    cat << EOT >> /etc/cron.d/elastalert
#TB#===============================================================
#TB# (re)start elastalert
#TB* * * * *       root    [ -x /usr/bin/elastalert -a -z "\$(ps -e | grep elastalert)" ] && elastalert --config /etc/elastalert/config.yaml &> /var/log/elastalert.log &
#TBEOT

    #--------------------------------------------------------------------
    # Install rsyslog and configure it
    # Install the newest version from Adiscon's repository
    if [ -s /etc/lsb-release ]
    then
        source /etc/lsb-release
        [ -s /etc/apt/sources.list.d/adiscon-ubuntu-v8-stable-${DISTRIB_CODENAME}.list ] || add-apt-repository ppa:adiscon/v8-stable
    elif [ -s /etc/debian_version ]
    then
        # Use an US based mirror of "download.opensuse.org"
        echo 'deb http://provo-mirror.opensuse.org/repositories/home:/rgerhards/Debian_9.0/ /' > /etc/apt/sources.list.d/home:rgerhards.list
        wget -nv https://provo-mirror.opensuse.org/repositories/home:rgerhards/Debian_9.0/Release.key -O /tmp/Release.key
        apt-key add - < Release.key
    fi
    apt-get update
    for P in rsyslog-gnutls rsyslog-mmrm1stspace rsyslog-elasticsearch rsyslog-mmnormalize rsyslog-mmjsonparse liblognorm-utils
    do
        dpkg-query -S $P &> /dev/null
        [ $? -ne 0 ] && apt-get -y install $P
    done
    [ -z "$(dpkg -l rsyslog | grep adiscon)" ] && apt-get -y install rsyslog

    # Download and install local configuration files for rsyslog
    while true
    do
        TFILE=$(mktemp /tmp/$$XXXXXXXX)
        wget -q https://gitlab.com/bluc/AdminTools/raw/master/etc/rsyslog.d/elk.rb -O $TFILE
        [ -s $TFILE ] && break
    done
    diff $TFILE /etc/rsyslog.d/elk.rb &> /dev/null
    [ $? -eq 0 ] || cat $TFILE > /etc/rsyslog.d/elk.rb

    while true
    do
        TFILE=$(mktemp /tmp/$$XXXXXXXX)
        wget -q https://gitlab.com/bluc/AdminTools/raw/master/etc/rsyslog.d/20-input.conf -O $TFILE
        [ -s $TFILE ] && break
    done
    diff $TFILE /etc/rsyslog.d/20-input.conf &> /dev/null
    [ $? -eq 0 ] || cat $TFILE > /etc/rsyslog.d/20-input.conf

    while true
    do
        TFILE=$(mktemp /tmp/$$XXXXXXXX)
        wget -q https://gitlab.com/bluc/AdminTools/raw/master/etc/rsyslog.d/55-impstats.conf -O $TFILE
        [ -s $TFILE ] && break
    done
    diff $TFILE /etc/rsyslog.d/55-impstats.conf &> /dev/null
    [ $? -eq 0 ] || cat $TFILE > /etc/rsyslog.d/55-impstats.conf

    while true
    do
        TFILE=$(mktemp /tmp/$$XXXXXXXX)
        wget -q https://gitlab.com/bluc/AdminTools/raw/master/etc/rsyslog.d/60-out-templates.conf -O $TFILE
        [ -s $TFILE ] && break
    done
    diff $TFILE /etc/rsyslog.d/60-out-templates.conf &> /dev/null
    [ $? -eq 0 ] || cat $TFILE > /etc/rsyslog.d/60-out-templates.conf

    while true
    do
        TFILE=$(mktemp /tmp/$$XXXXXXXX)
        wget -q https://gitlab.com/bluc/AdminTools/raw/master/etc/rsyslog.d/61-out-rules.conf -O $TFILE
        [ -s $TFILE ] && break
    done
    if [ "T${OE^^}" = 'TO' ]
    then
        # Opensearch uses SSL connections
        sed -i 's@server="localhost:9200"@server="https://localhost:9200"\n         usehttps="on"\n         allowUnsignedCerts="on"@' $TFILE
    fi
    diff $TFILE /etc/rsyslog.d/61-out-rules.conf &> /dev/null
    [ $? -eq 0 ] || cat $TFILE > /etc/rsyslog.d/61-out-rules.conf

    # Determine the correct python version to use
    PYTHON_EXE='python'
    [ -d /usr/lib/python3 ] && PYTHON_EXE='python3'

    # Create the external plugin for rsyslog
    PYTHON_EXE='python'
    [ -d /usr/lib/python3 ] && PYTHON_EXE='python3'
    cat << EOT > /usr/local/bin/rsyslog-elk-sysmon.py
#!/usr/bin/env $PYTHON_EXE

"""A message modification plugin to anonymize credit card numbers.

   Copyright (C) 2014 by Adiscon GmbH and Peter Slavov

   This file is part of rsyslog.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0
         -or-
         see COPYING.ASL20 in the source distribution

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import sys
import re
import json

# skeleton config parameters
# currently none

# App logic global variables

def onInit():
        """ Do everything that is needed to initialize processing (e.g.
            open files, create handles, connect to systems...)
        """
def onReceive(msg):
        """This is the entry point where actual work needs to be done. It receives
           the message from rsyslog and now needs to examine it, do any processing
           necessary. The to-be-modified properties (one or many) need to be pushed
           back to stdout, in JSON format, with no interim line breaks and a line
           break at the end of the JSON. If no field is to be modified, empty
           json ("{}") needs to be emitted.
           Note that no batching takes place (contrary to the output module skeleton)
           and so each message needs to be fully processed (rsyslog will wait for the
           reply before the next message is pushed to this module).
        """
        global rc

        # Remove the leading "info" (or similar) string
        rc = re.search('^\s{0,}\S+\s+(.+)',msg)
        if rc:
                print json.dumps({'msg':rc.group(1)})
        else:
                print json.dumps({})

def onExit():
        """ Do everything that is needed to finish processing (e.g.
            close files, handles, disconnect from systems...). This is
            being called immediately before exiting.
        """
        # most often, nothing to do here


"""
-------------------------------------------------------
This is plumbing that DOES NOT need to be CHANGED
-------------------------------------------------------
Implementor's note: Python seems to very agressively
buffer stdouot. The end result was that rsyslog does not
receive the script's messages in a timely manner (sometimes
even never, probably due to races). To prevent this, we
flush stdout after we have done processing. This is especially
important once we get to the point where the plugin does
two-way conversations with rsyslog. Do NOT change this!
See also: https://github.com/rsyslog/rsyslog/issues/22
"""
onInit()
keepRunning = 1
while keepRunning == 1:
        msg = sys.stdin.readline()
        if msg:
                msg = msg[:-1] # remove LF
                onReceive(msg)
                sys.stdout.flush() # very important, Python buffers far too much!
        else: # an empty line means stdin has been closed
                keepRunning = 0
onExit()
sys.stdout.flush() # very important, Python buffers far too much!
EOT
    chmod 755 /usr/local/bin/rsyslog-elk-sysmon.py

    cat << EOT > /usr/local/bin/rsyslog-elk-amavis.py
#!/usr/bin/env $PYTHON_EXE

"""A message modification plugin to anonymize credit card numbers.

   Copyright (C) 2014 by Adiscon GmbH and Peter Slavov

   This file is part of rsyslog.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0
         -or-
         see COPYING.ASL20 in the source distribution

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import sys
import re
import json

# skeleton config parameters
# currently none

# App logic global variables

def onInit():
        """ Do everything that is needed to initialize processing (e.g.
            open files, create handles, connect to systems...)
        """
def onReceive(msg):
        """This is the entry point where actual work needs to be done. It receives
           the message from rsyslog and now needs to examine it, do any processing
           necessary. The to-be-modified properties (one or many) need to be pushed
           back to stdout, in JSON format, with no interim line breaks and a line
           break at the end of the JSON. If no field is to be modified, empty
           json ("{}") needs to be emitted.
           Note that no batching takes place (contrary to the output module skeleton)
           and so each message needs to be fully processed (rsyslog will wait for the
           reply before the next message is pushed to this module).
        """
        global rc

        # Replace ">,<" in amavis messages
        newmsg = re.sub('>,<', ',', msg)
        if newmsg == msg:
            print json.dumps({})
        else:
            print json.dumps({'msg': newmsg})

def onExit():
        """ Do everything that is needed to finish processing (e.g.
            close files, handles, disconnect from systems...). This is
            being called immediately before exiting.
        """
        # most often, nothing to do here


"""
-------------------------------------------------------
This is plumbing that DOES NOT need to be CHANGED
-------------------------------------------------------
Implementor's note: Python seems to very agressively
buffer stdouot. The end result was that rsyslog does not
receive the script's messages in a timely manner (sometimes
even never, probably due to races). To prevent this, we
flush stdout after we have done processing. This is especially
important once we get to the point where the plugin does
two-way conversations with rsyslog. Do NOT change this!
See also: https://github.com/rsyslog/rsyslog/issues/22
"""
onInit()
keepRunning = 1
while keepRunning == 1:
        msg = sys.stdin.readline()
        if msg:
                msg = msg[:-1] # remove LF
                onReceive(msg)
                sys.stdout.flush() # very important, Python buffers far too much!
        else: # an empty line means stdin has been closed
                keepRunning = 0
onExit()
sys.stdout.flush() # very important, Python buffers far too much!
EOT
    chmod 755 /usr/local/bin/rsyslog-elk-amavis.py

    read -rp 'Ensure that /etc/hosts lists the full host name first' YN
    vim.tiny /etc/hosts
    rsyslogd -N1 && service rsyslog restart
}

#--------------------------------------------------------------------
function InstallClient ()
{
    # Install the newest version from Adiscon's repository
    if [ -s /etc/lsb-release ]
    then
        source /etc/lsb-release
        [ -s /etc/apt/sources.list.d/adiscon-ubuntu-v8-stable-${DISTRIB_CODENAME}.list ] || add-apt-repository ppa:adiscon/v8-stable
    elif [ -s /etc/debian_version ]
    then
        # Use an US based mirror of "download.opensuse.org"
        echo 'deb http://provo-mirror.opensuse.org/repositories/home:/rgerhards/Debian_9.0/ /' > /etc/apt/sources.list.d/home:rgerhards.list
        wget -nv https://provo-mirror.opensuse.org/repositories/home:rgerhards/Debian_9.0/Release.key -O /tmp/Release.key
        apt-key add - < Release.key
    fi
    apt-get update
    for P in rsyslog-gnutls
    do
        dpkg-query -S $P &> /dev/null
        [ $? -ne 0 ] && apt-get -y install $P
    done
    [ -z "$(dpkg -l rsyslog | grep adiscon)" ] && apt-get -y install rsyslog

    cat << EOT > /etc/rsyslog.d/60-output.conf
\$PreserveFQDN on
# The main queue settings
main_queue (
        queue.type="LinkedList"      
        queue.highwatermark="40000"  
        queue.spoolDirectory="/var/spool/rsyslog"
        queue.filename="mainQueue"
        queue.lowwatermark="5000" 
        queue.maxdiskspace="100m" 
        queue.size="50000"    
        queue.dequeuebatchsize="1000" 
        queue.workerthreads="2"
        queue.saveonshutdown="on"
)

#RsyslogGnuTLS
\$DefaultNetstreamDriverCAFile /etc/rsyslog.d/keys/ca.d/logstash.crt

# Only works for version 8.25.x and above
global(parser.permitSlashInProgramName="on")

if \$programname == ["kibana", "logstash"]
then {
  # Ignore some internal "elk" messages
  stop
} else {
  # Suppress some Zimbra messages
  if (\$programname == ["slapd", "zmconfigd", "zimbramon", "ldapsearch"])
  then {
    stop
  }
  if ((\$programname == "ZimbraLog2Syslog.pl") and (re_match(\$msg, "misc - ")))
  then {
    stop
  }
  # Suppress some sshd messages
  if ((\$programname == "sshd") and (re_match(\$msg, "Received disconnect from|Deprecated")))
  then {
    stop
  }
  # Suppress all but two ppolicyd messages
  if ((\$programname == "sshd") and (re_match(\$msg, "X-PPolicyd|blocked ")))
  then {
    stop
  }
  # Suppress some clamd messages
  if ((\$programname == "clamd") and (re_match(\$msg, "Database correctly reloaded|SelfCheck:|Reading databases from")))
  then {
    stop
  }
  # Suppress some postfix messages
  if ((re_match(\$programname, "smtpd")) and (re_match(\$msg, "discarding [A-Z]+ keywords|table .+ has changed|lost connection |too many errors |SSL_accept|TLS library problem|Issuing session ticket|TLS engine")))
  then {
    stop
  }
  if ((re_match(\$programname, "smtp")) and (re_match(\$msg, "^connect to "))) 
  then {
    stop
  }
  if (re_match(\$programname, "postfix-script|scache"))
  then {
    stop
  }
  # Supress some nullmailer messages
  if ((\$programname == "nullmailer") and (re_match(\$msg, "Starting delivery,|Delivery complete|Rescanning queue|Sending failed:")))
  then {
    stop
  }
  # Suppress some amavis messages
  if (re_match(\$programname, "amavis"))
  then {
    if not (\$msg startswith '(')
    then {
      stop
    }
    if (re_match(\$msg, "SA info|mangling | p0[0-9][0-9] | TIMING|_WARN:|truncating a message|Requesting process rundown|Starting child process|RUSAGE|..."))
    then {
      stop
    }
  }

  # Send messages to central server over TCP using the
  # standard template.  All logs are sent in clear text,
  # so use a SSL or SSH tunnel to protect the stream when
  # sending over untrusted networks (e.g. Internet)
  # Adjust the IP address of the central server
  action(type="omfwd" protocol="tcp" target="192.168.20.233" port="5000" template="RSYSLOG_TraditionalForwardFormat")
}
EOT
    # Have all the changes take effect
    service rsyslog restart
}

#--------------------------------------------------------------------
# Server or client?
while true
do
    read -rp 'Install ELK server or client [S/C] ? ' SC
    SC=${SC^^}
    [ "T$SC" = 'TS' ] && break
    [ "T$SC" = 'TC' ] && break
done

case $SC in
S)  InstallServer
    ;;
C)  InstallClient
    ;;
esac

#--------------------------------------------------------------------
# We are done
exit 0
