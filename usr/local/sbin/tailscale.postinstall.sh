#!/bin/bash
################################################################
# (c) Copyright 2021 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/sbin/tailscale.postinstall.sh

#--------------------------------------------------------------------
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            

#--------------------------------------------------------------------
# Specifying "-x" to the bash invocation = DEBUG
DEBUG=''
[[ $- ==  *x* ]] && DEBUG='-x'

#--------------------------------------------------------------------
# Global variables
CODENAME=$(lsb_release -sc)

#--------------------------------------------------------------------
# Get the signing key and custom repository
wget -q -O - https://pkgs.tailscale.com/stable/ubuntu/${CODENAME}.gpg | apt-key add -
wget -q https://pkgs.tailscale.com/stable/ubuntu/${CODENAME}.list \
  -O /etc/apt/sources.list.d/tailscale.list

#--------------------------------------------------------------------
# Install the software
apt-get update
apt-get install tailscale

#--------------------------------------------------------------------
# Fire up the daemon once
tailscale up

#--------------------------------------------------------------------
# Adapt local firewall
if [ -z "$(grep tailscale /etc/firehol/firehol.conf)" ]
then
    cat << EOT >> /etc/firehol/firehol.conf

interface4 tailscale0 tailscale
        masquerade
        #-----------------------------------------------------------------
        # Protect against attacks as best as we can
        policy reject
        protection strong 10/sec 10
        protection reverse strong
        #-----------------------------------------------------------------
        # Allow everything
        client all accept
        server all accept
EOT
    systemctl restart firehol
fi

#--------------------------------------------------------------------
# We are done
exit 0
