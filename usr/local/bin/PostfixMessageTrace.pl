#!/usr/bin/env perl
################################################################
# (c) Copyright 2018 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/bin/PostfixMessageTrace.pl

# Trace individual messages through postfix, ppolicyd, PFCF, and clamsmtpd
# Invoke it like this: PostfixMessageTrace.pl < logfile
#        OR like this: grep 'Jan  7' logfile | PostfixMessageTrace.pl
use 5.0010;

# Constants
my $ProgName = '';
( $ProgName = $0 ) =~ s%.*/%%;

# Common sense options:
use strict qw(vars subs);
no warnings;
use warnings qw(FATAL closed threads internal debugging pack
    portable prototype inplace io pipe unpack malloc
    glob digit printf layer reserved taint closure
    semicolon);
no warnings qw(exec newline unopened);

#--------------------------------------------------------------------
# Needed packages
#--------------------------------------------------------------------
use Getopt::Std;
use Data::Dumper qw(Dumper);
# These require the appropriate non-core modules to be installed
use JSON::XS;

#--------------------------------------------------------------------
# Globals
#--------------------------------------------------------------------
$ENV{PATH} = '/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin';

# The hash containing all data
my %Logs;

# Program options
our $opt_f = 'none';
our $opt_v = 0;
our $opt_h = 0;

#--------------------------------------------------------------------
# Display the usage
#--------------------------------------------------------------------
sub ShowUsage()
{
    print "Usage: $ProgName [options]\n",
        "       -f format  Define output format [default=$opt_f]\n",
        "          Possible formats: raw, json, none\n",
        "       -v         Show logs in STDOUT [default=no]\n",
        "       -h         Show this help\n";

    exit 0;
} ## end sub ShowUsage

#--------------------------------------------------------------------
# Main function
#--------------------------------------------------------------------
$|++;

# Get possible options
getopts('f:vh') or ShowUsage();
ShowUsage() if ($opt_h);
my $OutputFormat = lc($opt_f);
ShowUsage() unless ( $opt_f =~ /^(?:raw|json|none)/ );

# All input is on STDIN
while ( my $Line = <> )
{
    chomp($Line);

    # Extract common data and shorten the line
    $Line =~ s@^(.{15})\s\S+\s([^\[]+)(?:\[(\d+)\])?:\s@@;

    my $DateTime = "$1";
    my $Process  = lc("$2");
    my $PID      = $3;

    $Line =~ s@(?:\[ID\s\S+\s\S+\s)@@;

    if ( $Process =~ m@postfix/@o )
    {
        #--------------------------------------------------------------------
        # Postfix log messages
        #--------------------------------------------------------------------

        if ( $Line
            =~ m@([^:]*):\sclient=[^\[]*\[(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})@o
            )
        {
            # First connection

            my $PostfixID      = "$1";
            my $SMTPClient     = "$2";
            my $ConnectionType = 'remote';
            $ConnectionType = 'local' if ( $SMTPClient eq '127.0.0.1' );
            $Logs{'postfix'}{"$PostfixID"}{"$ConnectionType SMTP connection"}
                {'Timestamp'} = $DateTime;
            $Logs{'postfix'}{"$ConnectionType SMTP connection"}{'SMTPClient'}
                = $SMTPClient;
            printf "%-36s: %s\n", "Postfix $ConnectionType SMTPD connection",
                "$DateTime $PostfixID $SMTPClient"
                if ($opt_v);
            next;
        } ## end if ( $Line =~ ...)
        if ( $Line =~ m@([^:]*):\smessage-id=(\S+)@o )
        {
            # Get the email's message ID (postfix)
            my $PostfixID = "$1";
            my $MessageID = "$2";
            $Logs{'postfix'}{"$PostfixID"}{'Message-ID'}{'Timestamp'}
                = $DateTime;
            $Logs{'postfix'}{"$PostfixID"}{'Message-ID'}{'MessageID'}
                = $MessageID;
            printf " %-35s: %s\n", 'Postfix message-id',
                "$DateTime $PostfixID $MessageID"
                if ($opt_v);
            next;
        } ## end if ( $Line =~ m@([^:]*):\smessage-id=(\S+)@o...)
        if ( $Line =~ m@([^:]*):\sfrom=([^,]*)@o )
        {
            # Get the email sender
            my $PostfixID = "$1";
            my $Sender    = "$2";
            $Logs{'postfix'}{"$PostfixID"}{'Email sender'}{'Timestamp'}
                = $DateTime;
            $Logs{'postfix'}{"$PostfixID"}{'Email sender'}{'Sender'}
                = $Sender;
            printf " %-35s: %s\n", 'Postfix email sender',
                "$DateTime $PostfixID $Sender"
                if ($opt_v);
            next;
        } ## end if ( $Line =~ m@([^:]*):\sfrom=([^,]*)@o...)
        if ( $Line =~ m@lmtp@o )
        {
            if ( $Line =~ m@([^:]*):\s.+\srelay=([^,]+),.+\sstatus=(.+)@o )
            {
                # Final mail delivery status (lmtp)
                my $PostfixID  = "$1";
                my $LMTPRelay  = "$2";
                my $LMTPStatus = "$3";
                $Logs{'postfix'}{"$PostfixID"}{'LMTP status'}{'Timestamp'}
                    = $DateTime;
                $Logs{'postfix'}{"$PostfixID"}{'LMTP status'}{'Relay'}
                    = $LMTPRelay;
                $Logs{'postfix'}{"$PostfixID"}{'LMTP status'}{'Status'}
                    = $LMTPStatus;
                printf "%-36s: %s\n", 'Postfix LMTP status',
                    "$DateTime $PostfixID $LMTPRelay $LMTPStatus"
                    if ($opt_v);
                next;
            } ## end if ( $Line =~ ...)
            if ( $Line
                =~ m@^([^:]*):\s.+\s(\d+-\d+)\s(250\smessage\s[qrp].+|[45]51\s.+)@o
                )
            {
                # PFCF is done
                my $PostfixID = "$1";
                my $PFCFID    = "$2";
                my $PFCFMsg   = "$3";
                $Logs{'postfix'}{"$PostfixID"}{'PFCF done'}{'Timestamp'}
                    = $DateTime;
                $Logs{'postfix'}{"$PostfixID"}{'PFCF done'}{'Message'}
                    = $PFCFMsg;
                $Logs{'postfix'}{"$PostfixID"}{'PFCF done'}{'PFCF ID'}
                    = $PFCFID;
                printf "%-36s: %s\n", 'Postfix PFCF done',
                    "$DateTime $PostfixID $PFCFMsg (PFCF: $PFCFID)"
                    if ($opt_v);
                next;
            } ## end if ( $Line =~ ...)
        } ## end if ( $Line =~ m@lmtp@o...)
        if ( $Line =~ m@smtp@o )
        {
            if ( $Line =~ m@([^:]*):\s.+\srelay=([^,]+),.+\sstatus=(.+)@o )
            {
                # Final mail delivery status (smtp)
                my $PostfixID  = "$1";
                my $SMTPRelay  = "$2";
                my $SMTPStatus = "$3";
                $Logs{'postfix'}{"$PostfixID"}{'SMTP status'}{'Timestamp'}
                    = $DateTime;
                $Logs{'postfix'}{"$PostfixID"}{'SMTP status'}{'Relay'}
                    = $SMTPRelay;
                $Logs{'postfix'}{"$PostfixID"}{'SMTP status'}{'Status'}
                    = $SMTPStatus;
                printf "%-36s: %s\n", 'Postfix SMTP status',
                    "$DateTime $PostfixID $SMTPRelay $SMTPStatus"
                    if ($opt_v);
                next;
            } ## end if ( $Line =~ ...)
        } ## end if ( $Line =~ m@lmtp@o...)
        next;
    } ## end if ( $Process =~ m@postfix/@o...)

    if ( $Process =~ m@ppolicyd@o )
    {
        #--------------------------------------------------------------------
        # PPolicyd log messages
        #--------------------------------------------------------------------
        $Line =~ s@(\d+_\d+)\s@@;
        my $PPolicyID = "$1";

        if ( $Line =~ m@original IP (\S+)@o )
        {
            # Get the original connecting IP address
            my $OriginalIP = "$1";
            $Logs{'ppolicyd'}{"$PPolicyID"}{'Original IP'}{'Timestamp'}
                = $DateTime;
            $Logs{'ppolicyd'}{"$PPolicyID"}{'Original IP'}{'Address'}
                = $OriginalIP;
            printf "%-36s: %s\n",
                'PPolicyd original IP', "$DateTime $OriginalIP"
                if ($opt_v);
            next;
        } ## end if ( $Line =~ m@original IP (\S+)@o...)
        if ( $Line =~ m@X-PPolicyd:\s(.*)@o )
        {
            # Get the original connecting IP address
            my $Result = "$1";
            $Logs{'ppolicyd'}{"$PPolicyID"}{'Result'}{'Timestamp'}
                = $DateTime;
            $Logs{'ppolicyd'}{"$PPolicyID"}{'Result'}{'Result'} = $Result;
            printf "%-36s: %s\n", 'PPolicyd result', "$DateTime $Result"
                if ($opt_v);
            next;
        } ## end if ( $Line =~ m@X-PPolicyd:\s(.*)@o...)
        next;
    } ## end if ( $Process =~ m@ppolicyd@o...)

    if ( $Process =~ m@clamsmtpd@o )
    {
        #--------------------------------------------------------------------
        # ClamSmtpd log messages
        #--------------------------------------------------------------------
        $Line =~ s@:\s(\d+):@@;
        my $clamsmtpdID = "$1";

        if ( $Line =~ m@from=([^,]*),\sto=([^,]*),\sstatus=(.*)@o )
        {
            # Get to and from along with the status
            my $Sender    = "$1";
            my $Recipient = "$2";
            my $Status    = "$3";
            $Logs{'clamsmtpd'}{"$clamsmtpdID"}{'Status'}{'Timestamp'}
                = $DateTime;
            $Logs{'clamsmtpd'}{"$clamsmtpdID"}{'Status'}{'Sender'} = $Sender;
            $Logs{'clamsmtpd'}{"$clamsmtpdID"}{'Status'}{'Recipient'}
                = $Recipient;
            $Logs{'clamsmtpd'}{"$clamsmtpdID"}{'Status'}{'Status'} = $Status;
            printf "%-36s: %s\n", 'Clamsmtpd sender/recipient/status',
                "$DateTime $Sender $Recipient $Status"
                if ($opt_v);
            next;
        } ## end if ( $Line =~ m@from=([^,]*),\sto=([^,]*),\sstatus=(.*)@o...)
        next;
    } ## end if ( $Process =~ m@clamsmtpd@o...)

    if ( $Process =~ m@amavis@o )
    {
        #--------------------------------------------------------------------
        # Amavis log messages
        #--------------------------------------------------------------------
        $Line =~ s@\((\d+-\d+)\)@@;
        my $AmavisID = "$1";

        if ( $Line
            =~ m@(\S+)\s->\s(\S+)\sSIZE=(\d+)\s(?:BODY=\S+\s)?Received:\sfrom\s\S+\s\(\[[^\]]+@o
            )
        {
            # Get initial connection to amavis
            my $Sender      = "$1";
            my $Recipient   = "$2";
            my $EmailSize   = $3;
            my $ConnectFrom = "$4";
            $Logs{'amavis'}{"$AmavisID"}{'Initial connection'}{'Timestamp'}
                = $DateTime;
            $Logs{'amavis'}{"$AmavisID"}{'Initial connection'}{'Sender'}
                = $Sender;
            $Logs{'amavis'}{"$AmavisID"}{'Initial connection'}{'Recipient'}
                = $Recipient;
            $Logs{'amavis'}{"$AmavisID"}{'Initial connection'}{'EmailSize'}
                = $EmailSize;
            $Logs{'amavis'}{"$AmavisID"}{'Initial connection'}{'ConnectFrom'}
                = $ConnectFrom;
            printf "%-36s: %s\n", 'Amavis initial connection',
                "$DateTime $Sender $Recipient $EmailSize $ConnectFrom"
                if ($opt_v);
            next;
        } ## end if ( $Line =~ ...)
        if ( $Line =~ m@spam-tag,\s(\S+)\s->\s([^,]+),\s([^,]+)@o )
        {
            # Get spam results
            my $Sender    = "$1";
            my $Recipient = "$2";
            my $SpamYesNo = $3;
            $Logs{'amavis'}{"$AmavisID"}{'spam result'}{'Timestamp'}
                = $DateTime;
            $Logs{'amavis'}{"$AmavisID"}{'spam result'}{'Sender'} = $Sender;
            $Logs{'amavis'}{"$AmavisID"}{'spam result'}{'Recipient'}
                = $Recipient;
            $Logs{'amavis'}{"$AmavisID"}{'spam result'}{'YesNo'} = $SpamYesNo;
            printf "%-36s: %s\n", 'Amavis spam result',
                "$DateTime $Sender $Recipient $SpamYesNo"
                if ($opt_v);
            next;
        } ## end if ( $Line =~ m@spam-tag,\s(\S+)\s->\s([^,]+),\s([^,]+)@o...)
        if ( $Line =~ m@virus_scan:\s\(([^\)]+)@o )
        {
            # Get virus name
            my $VirusName = "$1";
            $Logs{'amavis'}{"$AmavisID"}{'virus found'}{'Timestamp'}
                = $DateTime;
            $Logs{'amavis'}{"$AmavisID"}{'virus found'}{'Virus name'}
                = $VirusName;
            printf "%-36s: %s\n", 'Amavis found virus',
                "$DateTime $VirusName"
                if ($opt_v);
            next;
        } ## end if ( $Line =~ m@virus_scan:\s\(([^\)]+)@o...([(])))
        if ( $Line
            =~ m@(\S+)\s->\s([^,]+),.+\sMessage-ID:\s([^,]+),.+\ssize:\s(\d+),\squeued_as:\s([^,]+)@o
            )
        {
            # Get reinjection data
            my $Sender    = "$1";
            my $Recipient = "$2";
            my $MessageID = "$3";
            my $EmailSize = $4;
            my $PostfixID = "$5";
            $Logs{'amavis'}{"$AmavisID"}{'reinjected email'}{'Timestamp'}
                = $DateTime;
            $Logs{'amavis'}{"$AmavisID"}{'reinjected email'}{'Sender'}
                = $Sender;
            $Logs{'amavis'}{"$AmavisID"}{'reinjected email'}{'Recipient'}
                = $Recipient;
            $Logs{'amavis'}{"$AmavisID"}{'reinjected email'}{'Message-ID'}
                = $MessageID;
            $Logs{'amavis'}{"$AmavisID"}{'reinjected email'}{'PostfixID'}
                = $PostfixID;
            printf "%-36s: %s\n", 'Amavis reinjected email',
                "$DateTime $Sender $Recipient $MessageID $EmailSize $PostfixID"
                if ($opt_v);
            next;
        } ## end if ( $Line =~ ...)
        next;
    } ## end if ( $Process =~ m@amavis@o...)

    if ( $Process =~ m@pfcf@o )
    {
        #--------------------------------------------------------------------
        # PFCF log messages
        #--------------------------------------------------------------------
        $Line =~ s@(\d+_\d+)\s@@;
        my $PFCFID = "$1";

        if ( $Line =~ m@Email\smessage-id:\s(\S+)@o )
        {
            # Get the email's message ID (PFCF)
            my $MessageID = "$1";
            $Logs{'pfcf'}{"$PFCFID"}{'MessageID'}{'Timestamp'} = $DateTime;
            $Logs{'pfcf'}{"$PFCFID"}{'MessageID'}{'MessageID'} = $MessageID;
            printf " %-35s: %s\n", 'PFCF message-id',
                "$DateTime $PFCFID $MessageID"
                if ($opt_v);
            next;
        } ## end if ( $Line =~ m@Email\smessage-id:\s(\S+)@o...)
        if ( $Line =~ m@Reinjecting\semail@o )
        {
            # Reinjected email
            $Logs{'pfcf'}{"$PFCFID"}{'Reinjected email'}{'Timestamp'}
                = $DateTime;
            printf "%-36s: %s\n", 'PFCF reinject', "$DateTime $PFCFID"
                if ($opt_v);
            next;
        } ## end if ( $Line =~ m@Reinjecting\semail@o...)
        if ( $Line =~ m@(FTP\sthe\semail|Transferred\s)@o )
        {
            # FTPed the email body
            $Logs{'pfcf'}{"$PFCFID"}{'Transferred email body'}{'Timestamp'}
                = $DateTime;
            printf "%-36s: %s\n", 'PFCF FTP email', "$DateTime $PFCFID"
                if ($opt_v);
            next;
        } ## end if ( $Line =~ m@(FTP\sthe\semail|Transferred\s)@o...)
        if ( $Line =~ m@Save\sthe\semail@o )
        {
            # Save the email body locally
            $Logs{'pfcf'}{"$PFCFID"}{'Saved email body locally'}{'Timestamp'}
                = $DateTime;
            printf "%-36s: %s\n", 'PFCF Save email locally',
                "$DateTime $PFCFID"
                if ($opt_v);
            next;
        } ## end if ( $Line =~ m@Save\sthe\semail@o...)
        if ( $Line =~ m@Sending\sresponse@o )
        {
            # Sending response to sender
            $Logs{'pfcf'}{"$PFCFID"}{'Sending response'}{'Timestamp'}
                = $DateTime;
            printf "%-36s: %s\n", 'PFCF Sending response', "$DateTime $PFCFID"
                if ($opt_v);
            next;
        } ## end if ( $Line =~ m@Sending\sresponse@o...)
    } ## end if ( $Process =~ m@pfcf@o...)
} ## end while ( my $Line = <> )

# Print what we have via "Dumper"
if ( $OutputFormat =~ /raw/ )
{
    $Data::Dumper::Varname = 'Logs';
    print Dumper \%Logs;
} elsif ( $OutputFormat =~ /json/ )
{
   print JSON::XS->new->pretty(1)->encode (\%Logs);
} ## end elsif ( $OutputFormat =~ ...)

#--------------------------------------------------------------------
# We are done
exit(0);
__END__
