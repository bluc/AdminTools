#!/usr/bin/perl -w
# Last checked into repository on 2022-04-16 08:50:02 EDT
################################################################
# (c) Copyright 2021 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# This works for a Technicolor TC8717T Modem

use 5.0010;

# Constants
my $ProgName = '';
( $ProgName = $0 ) =~ s%.*/%%;
my $CopyRight = "(c) CopyRight 2020 B-LUC Consulting Thomas Bullinger";

# Common sense options:
use strict qw(vars subs);
no warnings;
use warnings qw(FATAL closed threads internal debugging pack
    portable prototype inplace io pipe unpack malloc
    glob digit printf layer reserved taint closure
    semicolon);
no warnings qw(exec newline unopened);

#--------------------------------------------------------------------
# Needed packages
#--------------------------------------------------------------------
use Getopt::Std;
use HTTP::Tiny;    # Requires debian package libhttp-tiny-perl

use Data::Dumper;

#--------------------------------------------------------------------
# Globals
#--------------------------------------------------------------------
# Program options
our $opt_h = 0;
our $opt_d = 0;
our $opt_n = 0;
our $opt_v = 0;

# Program variables
# Adapt these values if necessary:
my $UserID = 'admin';
my $Passwd = 'password';
my $URL="http://192.168.0.1";

#--------------------------------------------------------------------
# Display the usage
#--------------------------------------------------------------------
sub ShowUsage()
{

    print "Usage: $ProgName [options]\n",
        "       -h      Show this help [default=no]\n",
        "       -n      Don't actually reboot [default=no]\n",
        "       -v      Show some progress [default=no]\n",
        "       -d      Show some debug info on STDERR [default=no]\n";

    exit 0;
} ## end sub ShowUsage

#--------------------------------------------------------------------
# Main function
#--------------------------------------------------------------------
$|++;
print "$ProgName\n$CopyRight\n\n";

# Get possible options
getopts('dhnv') or ShowUsage();
ShowUsage() if ($opt_h);

#--------------------------------------------------------------------
# Connect to the modem
my $agent = HTTP::Tiny->new(timeout => 20);
warn "D: URL='$URL'\n" if ($opt_d);
my $response = $agent->get( "$URL");
warn "D: response = ", Dumper($response) if ($opt_d);
die "ERROR: Couldn't connect to modem: " . $response->{response} . "\n" if ( $response->{status} > 399 );
if ($opt_v)
{
    my $CurDate = localtime();
    print "NOTICE: Connected to modem at '$URL' ($CurDate)\n";
}

# Login
$response = $agent->post_form("$URL/goform/home_loggedout", {loginUsername=>"$UserID", loginPassword=>"$Passwd"});
warn "D: response = ", Dumper($response) if ($opt_d);
die "ERROR: Couldn't login to modem: ".$response->{response} . "\n" if ( $response->{status} > 399 );
if ($opt_v)
{
    my $CurDate = localtime();
    print "NOTICE: Logged in to modem ($CurDate)\n";
}

if ($opt_n)
{
    my $CurDate = localtime();
    print "NOTICE: Not actually rebooting the modem ($CurDate)\n";
    exit (0);
}

# Reboot
$response = $agent->post_form("$URL/goform/restore_reboot", {resetbt=>1});
warn "D: response = ", Dumper($response) if ($opt_d);
die "ERROR: Couldn't reset modem: ".$response->{response} . "\n" if ( $response->{status} > 399 );
if ($opt_n)
{
    my $CurDate = localtime();
    print "NOTICE: Issued reboot command\n";
}

my $ReturnDate = localtime(time + 120);
print "SUCCESS: Modem is being rebooted and should be back by $ReturnDate\n";

#--------------------------------------------------------------------
# We are done
exit(0);
__END__
