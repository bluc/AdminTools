#!/usr/bin/perl -w
# Version 20240628-074944 checked into repository

# Copyright (c) 2004, Matthew R. McEachen

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# <http://matthew.mceachen.us/geek/gigasync/>

# Changes made by Noor Dawod, https://github.com/noordawod/gigasync

# Additional changes made my Thomas Bullinger (consult@btoy1.net):
# Create log file '/tmp/gigasync.log' when running
# Be more verbose about found and excluded files
# Added more default options for rsync:
#  Require at least perl 5
#  Be specific which "strict" options to use
#  Always use IPv4
#  Always skip compression for certain extensions

use 5.0010;
# Common sense options:
use strict qw(vars subs);
no warnings;
use warnings qw(FATAL closed threads internal debugging pack
    portable prototype inplace io pipe unpack malloc
    glob digit printf layer reserved taint closure
    semicolon);
no warnings qw(exec newline unopened);

use Env;
use File::Basename;
use File::Find;
use File::Path;
use File::Temp;
use File::stat;
use Getopt::Long;
use Pod::Usage;
use POSIX qw(strftime);

my $man         = 0;
my $help        = 0;
my $run_size_mb = 128;    # Mb
my $exclude_pattern;
my $verbose;

# DON'T INCLUDE ANY OPTIONS THAT INFER --recusive (like --archive)!
# ALSO, THE SCRIPT WRITE THE FILES THAT WILL BE COPIED INTO A LOG
# FILE SO NO NEED TO INCLUDE -v (VERBOSE) MODE.

my $rsync_options
    = '-lptgoD --no-implied-dirs --log-format="%t %f (%l bytes)" --skip-compress=3g2/3gp/3gpp/3mf/7z/aac/ace/amr/apk/appx/appxbundle/arc/arj/asf/avi/br/bz2/cab/crypt5/crypt7/crypt8/deb/dmg/drc/ear/gz/flac/flv/gpg/h264/h265/heif/iso/jar/jp2/jpg/jpeg/lz/lz4/lzma/lzo/m4a/m4p/m4v/mkv/msi/mov/mp3/mp4/mpeg/mpg/mpv/oga/ogg/ogv/opus/pack/png/qt/rar/rpm/rzip/s7z/sfx/svgz/tbz/tgz/tlz/txz/vob/vma/webm/webp/wim/wma/wmv/xz/z/zip/zst --ipv4';
$rsync_options .= $ENV{RSYNC_OPTIONS} if $ENV{RSYNC_OPTIONS};

GetOptions(
    'help|?|h'          => \$help,
    'man'               => \$man,
    'verbose'           => \$verbose,
    'run-size:i'        => \$run_size_mb,
    'exclude_pattern:s' => \$exclude_pattern
) or pod2usage(2);
pod2usage(1) if $help;
pod2usage( -exitstatus => 0, -verbose => 2 ) if $man;

my $src_dir  = shift;    # first arg is source directory
my $dest_dir = shift;    # second arg is dest directory

# run size in bytes
my $run_size = 1024 * 1024 * $run_size_mb;

if ( !-d $src_dir )
{
    print "SRC '$src_dir' is not a directory.";
    pod2usage(2);
} ## end if ( !-d $src_dir )

my $batchsize;
my $OUT;

my $LOG;
unless ( open( $LOG, '>', '/tmp/gigasync.log' ) )
{
    die "Can't create log file.\n";
} ## end unless ( open( $LOG, '>', ...))
print $LOG strftime( "%Y-%m-%d %H:%M:%S", localtime ) . " Starting\n";

sub new_srclist()
{
    $batchsize = 0;
    # UNLINK on exit is by default.
    $OUT = new File::Temp( UNLINK => 1 ) or die "Can't open tempfile: $!";
} ## end sub new_srclist

sub run_rsync()
{
    my $retries = 5;
    while ( $retries-- )
    {
        close $OUT;
        my $rsync_cmd = join( '',
            'rsync ', $rsync_options, ' --files-from=',
            $OUT->filename, ' ', $src_dir, ' ', $dest_dir );
        print $LOG strftime( "%Y-%m-%d %H:%M:%S", localtime )
            . " Running '$rsync_cmd'\n";
        my $rsync_status = system($rsync_cmd);
        $rsync_status >>= 8;
        return if ( $rsync_status == 0 );
        if ( $rsync_status == 12 )
        {
            print "Network issues. I'll retry in a bit.\n" if $verbose;
            sleep(90);
        } else
        {
            die "rsync returned $rsync_status: $!" if ( $rsync_status != 0 );
        } ## end else [ if ( $rsync_status == ...)]
    } ## end while ( $retries-- )
    die "Failed to transfer, even after 5 retries";
} ## end sub run_rsync

new_srclist();

File::Find::find( \&do, $src_dir );

run_rsync() if ( $batchsize > 0 );

sub do
{
    return if ( $File::Find::name eq $src_dir );
    return if ( !-f $File::Find::name );
    if ( ($exclude_pattern) && ( $File::Find::name =~ $exclude_pattern ) )
    {
        print $LOG strftime( "%Y-%m-%d %H:%M:%S", localtime )
            . " Excluding '$File::Find::name'\n";
        return;
    } ## end if ( ($exclude_pattern...))
    my $st = stat($File::Find::name);
    if ( !$st )
    {
        # File is missing (?! emacs temp file?)
        return;
    } ## end if ( !$st )
    my $rel_file = $File::Find::name;
    $rel_file =~ s:^\Q$src_dir/::;
    print $OUT $rel_file . "\n";
    print $LOG strftime( "%Y-%m-%d %H:%M:%S", localtime )
        . " Found local file '$rel_file'\n";
    if ( $st->size + $batchsize >= $run_size )
    {
        run_rsync();
        new_srclist();
        $batchsize = 0;
    } else
    {
        $batchsize += $st->size;
    } ## end else [ if ( $st->size + $batchsize...)]
} ## end sub do

# We are done
print $LOG strftime( "%Y-%m-%d %H:%M:%S", localtime ) . " Ending\n";
exit(0);

__END__

=head1 NAME

gigasync - Tool that enables rsync to mirror enormous directory trees.

L<http://samba.org/rsync/>) has a couple issues with mirroring large
(> 100K) directory trees.

=over

=item * rsync's memory usage is directly proportional to the number of
files in a tree. Large directories take a large amount of RAM.

=item * rsync can recover from previous failures, but always
determines the files to transfer up-front. If the connection fails
before that determination can be made, no forward progress in the
mirror can occur.

=back

The solution? Chop up the workload by using perl to recurse the
directory tree, building smallish lists of files to transfer with
rsync. Most of the time these small lists of files transfer over fine,
but if they fail, this script can look for that specific failure and
retry that set a couple times before giving up.

=head1 SYNOPSIS

gigasync [OPTION]... SRCDIR DESTHOST

=head1 OPTIONS

=over 8

=item B<--run-size>

The minimum number of bytes to transfer per batch.

=item B<--exclude_pattern>

As this script will be recursing through the source directory, if you
want to add exclude patterns in this option it means rsync won't be
run against file lists that it will immediately ignore.

=item B<--help, -h>

Print a brief help message and exits.

=item B<--man>

Prints the manual page and exits.

=back

=head1 PARAMETERS

=over 8

=item B<SRCDIR>

The source directory to recurse. Note that the source directory should
NOT be included in the [RSYNC] section that follows.

=item B<DESTHOST>

The destination host to mirror to. Note that the destination directory
will exactly match the source's directory heirarchy. If that's not
appropriate, change the script.

=back

=head1 ENVIRONMENT

=over 8

=item B<RSYNC_OPTIONS>

The environment variable that will be transparently passed along with
all rsync incantations. C<--bwlimit=20> limits the bandwidth used by
rsync to be ~ 20KB/s, for example. Note that you B<connot> use C<-a>
or C<--recurse> (as that breaks the whole point of this
script). Worse, those options B<are not checked for>, so bad things
happen if you include those in this environment variable. See the
script for more details.

=back

=cut
