-- Prosody IM
-- Copyright (C) 2017 B-LUC Consulting and Thomas Bullinger
--
-- This project is MIT/X11 licensed. Please see the
-- COPYING file in the source package for more information.
--

local new_sasl = require "util.sasl".new;
local pop3 = require "pop3"

local host = module.host;
local log = require "util.logger".init("auth_zimbra");

local zimbra_auth_host = module:get_option_string("zimbra_auth_host", "localhost");
local zimbra_domain = module:get_option_string("zimbra_domain");
local zimbra_pop3_port = module:get_option_number("zimbra_pop3_port", "110");
if zimbra_domain == "" then error("zimbra_domain required") end

local provider = {};

local function execute_command(command)
	local exit = os.execute(command .. ' > /dev/null 2> /dev/null')
	return exit
end

function provider.set_password(username, password)
        local zimbra_username
        if username:match("@") then zimbra_username = username else zimbra_username = username .. '@' .. zimbra_domain end
        module:log("debug", "Setting password for " .. zimbra_username);
	local SSHCmd = 'ssh -i /var/tmp/zimbra_key -o StrictHostKeyChecking=no zimbra@' .. zimbra_auth_host .. ' zmprov sp ' .. zimbra_username .. ' ' .. password .. ' >/dev/null 2>/dev/null'
	local rc = execute_command(SSHCmd)
        module:log("debug", "SSH return code = " .. rc);
	if rc == 0 then return true else return false end
end

function provider.create_user(username, password)
	return nil, "Account creation/modification unavailable for "..name;
end

function provider.get_password(username)
	return nil, "Getting passwords unavailable for "..name;
end

function provider.user_exists(username)
        local zimbra_username
        if username:match("@") then zimbra_username = username else zimbra_username = username .. '@' .. zimbra_domain end
        module:log("debug", "checking for " .. zimbra_username);
	local SSHCmd = 'ssh -i /var/tmp/zimbra_key -o StrictHostKeyChecking=no zimbra@' .. zimbra_auth_host .. ' zmprov -l ga ' .. zimbra_username .. ' >/dev/null 2>/dev/null'
	local rc = execute_command(SSHCmd)
        module:log("debug", "SSH return code = " .. rc);
	if rc == 0 then return true else return false end
end

function provider.users(host)
	return nil, "Getting user list unavailable for "..name;
end

local function test_password(username, password)
	local mbox = pop3.new()
	mbox:open(zimbra_auth_host, zimbra_pop3_port or '110')
        if username:match("@") then zimbra_username = username else zimbra_username = username .. '@' .. zimbra_domain end
       	module:log("debug", "test_password for " .. zimbra_username);
	mbox:auth(zimbra_username, password);
	return mbox:is_auth()
end

function provider.test_password(username, password)
	return test_password(username, password);
end

function provider.get_sasl_handler()
	return new_sasl(module.host, {
		plain_test = function(sasl, username, password)
			return provider.test_password(username, password), true;
		end
	});
end

module:provides("auth", provider);
