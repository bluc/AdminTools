#!/bin/bash
# Version 20240113-013352 checked into repository
################################################################
# (c) Copyright 2017 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            

#--------------------------------------------------------------------
# This host and domain
THISHOST=$(hostname)
[[ $THISHOST = *.* ]] || THISHOST=$(hostname -f)
THISDOMAIN=${THISHOST#*.}

# Global variables
SERVER='localhost'
PORT=25

#--------------------------------------------------------------------
# Can we use a popup dialog program for questions?
USE_DIALOG=0
[ -x /bin/whiptail ] && USE_DIALOG=1

function TLSCheck ()
{
    GetServer

    case ${PORT} in
    25) PROTOCOL='ESMTP --tlso'
        ;;
    465) PROTOCOL='SSMTP'
        ;;
    esac
    swaks -q RCPT -t test@test.com -f test@$THISDOMAIN -s $SERVER -p $PORT \
      --protocol $PROTOCOL -tls-get-peer-cert /tmp/$$.cert &> /tmp/$$
    if [ -s /tmp/$$.cert ]
    then
        echo >> /tmp/$$
        openssl x509 -text -in /tmp/$$.cert >> /tmp/$$ 2>&1
        rm -f /tmp/$$.cert
    fi

    if [ $USE_DIALOG -ne 0 ]
    then
        whiptail --title='Test results' --textbox /tmp/$$ --scrolltext 20 80
    else
        less /tmp/$$
    fi
}

function SMTPCapabilities ()
{
    GetServer

    case ${PORT} in
    25) PROTOCOL='ESMTP'
        ;;
    465) PROTOCOL='SSMTP'
        ;;
    esac
    swaks -q EHLO -t test@test.com -f test@$THISDOMAIN -s $SERVER -p $PORT &> /tmp/$$
    if [ $USE_DIALOG -ne 0 ]
    then
        whiptail --title='Test results' --textbox /tmp/$$ --scrolltext 20 80
    else
        less /tmp/$$
    fi
}

function GetServer ()
{
    if [ $USE_DIALOG -ne 0 ]
    then
        whiptail --title='Server name or address' --nocancel \
          --inputbox "Name or IP address of SMTP server: " 9 40 $SERVER 2> /tmp/$$
        N_SERVER=$(< /tmp/$$)
    else
        read -r -p "Name or IP address of SMTP server: " N_SERVER
    fi
    [ -z "$N_SERVER" ] || SERVER="$N_SERVER"    

    if [ $USE_DIALOG -ne 0 ]
    then
        whiptail --title='Server port' --nocancel \
          --inputbox "Port on SMTP server: " 9 40 $PORT 2> /tmp/$$
        N_PORT=$(< /tmp/$$)
    else
        read -r -p "Port on SMTP server: " N_PORT
    fi
    [ -z "$N_PORT" ] || PORT="$N_PORT"    
}

function MainMenu ()
{
    while [ true ]
    do
        if [ $USE_DIALOG -ne 0 ]
        then
            whiptail --title='SMTP Tests' --nocancel \
              --menu "Port on SMTP server: " 25 78 16 \
              "0" "Exit" \
              "1" "Get SMTP capabilities" \
              "2" "TLS Check" 2> /tmp/$$
            ACTION=$(< /tmp/$$)
        else
            clear
            cat << EOT

            S M T P   T e s t s
 0 - Exit
 1 - Get SMTP capabilities    2 - TLS Check

EOT
            read -r -p "Select an action from the menu above: " ACTION
        fi

        case ${ACTION} in
        0)  exit 0
            ;;
        1)  SMTPCapabilities
            ;;
        2)  TLSCheck
            ;;
        esac
    done
}

#--------------------------------------------------------------------
# Show the main menu and let the user pick an action
MainMenu

#--------------------------------------------------------------------
# We are done
exit 0
