#!/bin/bash
# Version 20230130-072730 checked into repository
################################################################
# (c) Copyright 2023 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/bin/ShowDebContents.sh
# Based on https://superuser.com/questions/82923/how-to-list-files-of-a-debian-package-without-install

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
PROG=${0##*/}


#--------------------------------------------------------------------
# Get the package contents from the corret download site
function deb_list ()
{
    curl -s $(lsb_release -si | \
      sed -e 's Ubuntu https://packages.ubuntu.com ' -e 's Debian https://packages.debian.org '
      )/$(lsb_release -sc)/all/$1/filelist |\
       sed -n -e '/<pre>/,/<\/pre>/p' | \
       sed -e 's/<[^>]\+>//g' -e '/^$/d';
}

#--------------------------------------------------------------------
# We need the package name
if [ "$#" -ne 1 ]
then
    echo "Usage: $PROG package-name"
    exit 1
fi

#--------------------------------------------------------------------
# Show the contents of a debian package
deb_list "$1"

#--------------------------------------------------------------------
# We are done
exit 0
