#!/bin/bash
# Version 20240901-054535 checked into repository
################################################################
# (c) Copyright 2024 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/bin/SFTP.sh

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

#--------------------------------------------------------------------
# The server to connect to
ACCT_SERVER="$1"
[ -z "$ACCT_SERVER" ] && read -rp 'Which account and server to connect to (e.g. "opa@mx") ? ' ACCT_SERVER
[ -z "$ACCT_SERVER" ] && exit 0
IP=$(grep -E -o '(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)' <<< $ACCT_SERVER)
if [ -z "$IP" ]
then
    IP=${ACCT_SERVER#*@}
    [[ $IP =~ : ]] && IP=${IP%:*}
fi

#--------------------------------------------------------------------
# Determine which cipher to use
CIPHER=''
if [[ $(ssh -Q ciphers) =~ aes256-gcm@openssh.com ]]
then
    CIPHER='-c aes256-gcm@openssh.com'
fi

#--------------------------------------------------------------------
while true
do
  date
  ping -c 2 $IP && sftp -R 128 -B $((64 * 1024)) $CIPHER -o RekeyLimit=40G ${ACCT_SERVER} && break
  sleep 5
  echo
done

#--------------------------------------------------------------------
# We are done
exit 0
