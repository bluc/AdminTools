#!/bin/bash
# Version 20240113-013155 checked into repository
################################################################
# (c) Copyright 2022 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
trap "rm -rf /tmp/$$*" EXIT

GIT_ROOT=$(git rev-parse --show-toplevel) 

if [ $# -eq 0 ]
then
    # Show what needs to be checked in
    echo 'Files that need to be checked in:'
    git diff HEAD  --name-only
    exit 1
fi

if [ "T$1" = 'T-a' ]
then
    # Get the names of all files that need to be checked in
    git diff HEAD  --name-only > /tmp/$$.filelist
else
    # Save the specified list of files
    echo "$*" > /tmp/$$.filelist
fi
for F in $(< /tmp/$$.filelist)
do
    if [[ "$F" =~ / ]]
    then
        THISFILE="$GIT_ROOT/$F"
    else
        THISFILE=$(pwd)/"$F"
    fi

    # Add a version block into a script file
    if [ -n "$(file $THISFILE | grep script)" ]
    then
        VERSION=$(date +%Y%m%d-%H%M%S)
        sed -i '0,/checked into repository/{/checked into repository/d};
                1 a # Version '"$VERSION"' checked into repository' $THISFILE
    fi

    git diff $THISFILE
    read -r -p 'Press ENTER to continue' ENTER
    git ci $THISFILE
done

# We are done
exit 0
