-- Prosody IM
-- Copyright (C) 2017 B-LUC Consulting and Thomas Bullinger
--
-- This project is MIT/X11 licensed. Please see the
-- COPYING file in the source package for more information.
--

local new_sasl = require "util.sasl".new;
local pop3 = require "pop3"

local host = module.host;
local log = require "util.logger".init("auth_pop3");

local pop3_host = module:get_option_string("pop3_auth_host", "localhost");
local pop3_port = module:get_option_number("pop3_auth_port", "110");

local provider = {};

function provider.set_password(username, password)
	return nil, "Setting passwords unavailable for "..name;
end
function provider.create_user(username, password)
	return nil, "Acccount creation/modification unavailable for "..name;
end

function provider.get_password(username)
	return nil, "Getting passwords unavailable for "..name;
end

function provider.user_exists(username)
	-- FIXME
	return true
end

local function test_password(username, password)
	local mbox = pop3.new()
	mbox:open(pop3_host, pop3_port or '110')
	mbox:auth(username, password);
	return mbox:is_auth()
end

function provider.test_password(username, password)
	return test_password(username, password);
end

function provider.get_sasl_handler()
	return new_sasl(module.host, {
		plain_test = function(sasl, username, password)
			return provider.test_password(username, password), true;
		end
	});
end

module:provides("auth", provider);
