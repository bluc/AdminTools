#!/bin/bash
# Version 20240113-013559 checked into repository
################################################################
# (c) Copyright 2022 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/usr/local/bin/ZimbraCheck.sh

# Run this script every five minutes, e.g.:
# */5 * * * * zimbra [ -x /usr/local/bin/ZimbraCheck.sh ] && /usr/local/bin/ZimbraCheck.sh

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/opt/zimbra/bin
PROG=${0##*/}

# This needs to run as "zimbra"
if [ ! "T$(/usr/bin/id -un)" = 'Tzimbra' ]
then
    echo "$PROG needs to run as 'zimbra'"
    exit 1
fi

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f "$LOCKFILE" ]
then
    # The file exists so read the PID
    MYPID=$(< "$LOCKFILE")
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > "$LOCKFILE"

#--------------------------------------------------------------------
# Requirements for the checks
# The server needs to be up and running for at least 15 minutes
[ $(cut -d. -f1 < /proc/uptime) -lt 900 ] && exit 2

# No backups and no zmcontrol are running
if [ -n "$(ps -eo pid,cmd | grep -E '(zo[s]|zmcontro[l])' | grep -v rsyn[c])" ]
then
    logger -p info -t ${PROG}[$$] -- Either backup or service restart in progress
    date "+%F %T either backup or service restart in progress" >> /tmp/ZimbraStatus.txt
    exit 0
fi

#--------------------------------------------------------------------
# Do the actual checks
if [ -s /tmp/ZimbraStatus.txt ]
then
    if [ $(tail -3 /tmp/ZimbraStatus.txt | grep -c 'restarting') -eq 3 ]
    then
        logger -p info -t ${PROG}[$$] -- Tried restarting Zimbra at least 3 times
        shutdown -r +1 Zimbra services are down hard
        exit 0
    fi

    # Truncate status file to 3 lines
    CURLINES=$(sed -n '$=' /tmp/ZimbraStatus.txt)
    if [ $CURLINES -gt 3 ]
    then
        LINES2DEL=$((CURLINES - 3))
        sed -i -e "1,${LINES2DEL}d" /tmp/ZimbraStatus.txt
    fi
fi

RETCODE=0
DOWN_SERVICES=0
if [ -z "$(lsof -n -i | grep -Em 1 ':(443|https).*LISTEN')" ]
then
    logger -p info -t ${PROG}[$$] -- Restarting Zimbra due to web service being down
    date "+%F %T restarting Zimbra due to web service being down" >> /tmp/ZimbraStatus.txt
    zmcontrol restart
    RETCODE=$?
else
    zmcontrol status &> /tmp/$$.status
    [[ $- = *x* ]] && cat /tmp/$$.status
    if [ -n "$(grep 'Unable to determine enabled services from ldap' /tmp/$$.status)" ]
    then
        logger -p info -t ${PROG}[$$] -- Restarting Zimbra due to LDAP being down
        date "+%F %T restarting Zimbra due to LDAP being down" >> /tmp/ZimbraStatus.txt
        zmcontrol restart
        RETCODE=$?
    else
        DOWN_SERVICES=$(grep -c 'is not running' /tmp/$$.status)
        if [ $DOWN_SERVICES -gt 2 ]
        then
            logger -p info -t ${PROG}[$$] -- Restarting Zimbra due to at least three services being down
            date "+%F %T restarting Zimbra due to $DOWN_SERVICES service(s) being down" >> /tmp/ZimbraStatus.txt
            zmcontrol restart
            RETCODE=$?
        fi
    fi
fi

if [ $RETCODE -eq 0 ]
then
    logger -p info -t ${PROG}[$$] -- 'Zimbra runs well - '$DOWN_SERVICES' down service(s)'
    date "+%F %T Zimbra runs well - $DOWN_SERVICES down service(s)" >> /tmp/ZimbraStatus.txt
fi

# Check whether mailboxd had a "lock" problem
LOCK_ERR=$(grep -B 1 "Couldn't lock rootDir: /opt/zimbra/data/mailboxd" /opt/zimbra/log/mailbox.log | awk -F, '/^[0-9]/{print $1}' | tail -n 1)
if [ -n "$LOCK_ERR" ]
then
    ZM_PID=$(ps -eo pid,cmd | awk '/zmmailbox[d]mgr.*start/{print $1}')
    if [ $(date +%s -d "$LOCK_ERR") -gt $(date +%s -r /proc/${ZM_PID}/comm) ]
    then
        logger -p info -t ${PROG}[$$] -- Restarting Zimbra due to mailboxd lock problem
        date "+%F %T restarting Zimbra due to mailboxd lock problem" >> /tmp/ZimbraStatus.txt
        cd /opt/zimbra/data || exit
        mv -f mailboxd mailboxd.old
        mkdir mailboxd
        chmod 755 /opt/zimbra/data/mailboxd
        chown zimbra:zimbra /opt/zimbra/data/mailboxd
        zmcontrol restart
    fi
fi

#--------------------------------------------------------------------
# Run some integrity checks once a day
if [ $RETCODE -eq 0 ] && [ $(date +%k) -eq 22 ]
then
    RUN_IC=1
    if [ -s /tmp/ZimbraIntegrityCheck.txt ]
    then
        # Force an integrity check if it hasn't run in the past day
        [ $(date +%s -r /tmp/ZimbraIntegrityCheck.txt) -lt $(date +%s -d '1 day ago') ] || RUN_IC=0
    fi

    if [ $RUN_IC -ne 0 ]
    then
        NOCACHE=''
        [ -x /usr/bin/nocache ] && NOCACHE='nocache'
        # Correct any blobs that have an incorrect revision
        nice $NOCACHE zmblobchk --incorrect-revision-rename-file start &> /tmp/ZimbraIntegrityCheck.txt
        # Delete any message with missing blobs
        nice $NOCACHE zmblobchk --missing-blob-delete-item --no-export start >> /tmp/ZimbraIntegrityCheck.txt 2>&1
        # Find any orphan blobs
        nice $NOCACHE zmblobchk --unexpected-blob-list /tmp/zbl start >> /tmp/ZimbraIntegrityCheck.txt 2>&1
        if [ -s /tmp/zbl ]
        then
            # Delete the orphaned blobs
            sed -i -e 's/^/nice '$NOCACHE' rm -f /g' /tmp/zbl
            bash /tmp/zbl
        fi
        # Check the database integrity
        [ -z "$(zmlocalconfig | grep integ)" ] && zmlocalconfig -e zmdbintegrityreport_disabled=TRUE
        nice $NOCACHE /opt/zimbra/libexec/zmdbintegrityreport -o -v >> /tmp/ZimbraIntegrityCheck.txt 2>&1

        # Finally show the results of the integrity checks
        if [ -s /tmp/ZimbraIntegrityCheck.txt ]
        then
            if [ $(grep -vcE '(Setting mysql connector property|^Checking mailbox|^No errors found)' /tmp/ZimbraIntegrityCheck.txt) -gt 0 ]
            then
                date "+%F %T Results of the Zimbra integrity checks:%n"
                cat /tmp/ZimbraIntegrityCheck.txt
                cat << EOT

If there is any mention of "no such account: xxxxxxxx", use this to start debugging:
su - zimbra
mysql zimbra -e 'select id,group_id,comment from mailbox where account_id="xxxxxxxx"\\G'

# Delete the orphaned account - replace \$GROUP_ID and \$MID by the corresponding numbers
#  in the "mysql" output (see also https://wiki.zimbra.com/wiki/King0770-Notes-Remove-Orphaned-Account):
mysql mboxgroup\$GROUP_ID -e "delete from appointment where mailbox_id='\$MID';"
mysql mboxgroup\$GROUP_ID -e "delete from appointment_dumpster where mailbox_id='\$MID';"
mysql mboxgroup\$GROUP_ID -e "delete from data_source_item where mailbox_id='\$MID';"
mysql mboxgroup\$GROUP_ID -e "delete from imap_folder where mailbox_id='\$MID';"
mysql mboxgroup\$GROUP_ID -e "delete from imap_message where mailbox_id='\$MID';"
mysql mboxgroup\$GROUP_ID -e "SET FOREIGN_KEY_CHECKS=0; delete from mail_item where mailbox_id='\$MID';"
mysql mboxgroup\$GROUP_ID -e "delete from mail_item_dumpster where mailbox_id='\$MID';"
mysql mboxgroup\$GROUP_ID -e "delete from open_conversation where mailbox_id='\$MID';"
mysql mboxgroup\$GROUP_ID -e "delete from pop3_message where mailbox_id='\$MID';"
mysql mboxgroup\$GROUP_ID -e "delete from purged_conversations where mailbox_id='\$MID';"
mysql mboxgroup\$GROUP_ID -e "delete from purged_messages where mailbox_id='\$MID';"
mysql mboxgroup\$GROUP_ID -e "delete from revision where mailbox_id='\$MID';"
mysql mboxgroup\$GROUP_ID -e "delete from revision_dumpster where mailbox_id='\$MID';"
mysql mboxgroup\$GROUP_ID -e "delete from tag where mailbox_id='\$MID';"
mysql mboxgroup\$GROUP_ID -e "delete from tagged_item where mailbox_id='\$MID';"
mysql mboxgroup\$GROUP_ID -e "delete from tombstone where mailbox_id='\$MID';"
mysql zimbra -e "delete from mailbox where id='\$MID';"
mysql zimbra -e "delete from mailbox_metadata where mailbox_id='\$MID';"
mysql zimbra -e "delete from mobile_devices where mailbox_id='\$MID';"
mysql zimbra -e "delete from out_of_office where mailbox_id='\$MID';"
mysql zimbra -e "delete from pending_acl_push where mailbox_id='\$MID';"
mysql zimbra -e "delete from scheduled_task where mailbox_id='\$MID';"
mysql zimbra -e "delete from zmg_devices where mailbox_id='\$MID';"

[ -d /opt/zimbra/store/0/\$MID ] && rm -rf /opt/zimbra/store/0/\$MID
[ -d /opt/zimbra/index/0/\$MID ] && rm -rf /opt/zimbra/index/0/\$MID

zmmailboxdctl restart
EOT
            fi
        fi
    fi
fi

#--------------------------------------------------------------------
# We are done
exit $RETCODE
