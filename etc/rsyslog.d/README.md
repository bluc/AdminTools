rs1.ndjson
==========

* An importable file that corresponds with the configuration files in this directory

To edit it do the following:
- expand the fields via "jq '.' rs1.ndjson > rs1.json"
- add a new field at the end of line 2 using this template:
{\"name\":\"FIELDNAME\",\"type\":\"string\",\"esTypes\":[\"text\"],\"count\":0,\"scripted\":false,\"searchable\":true,\"aggregatable\":false,\"readFromDocValues\":false},{\"name\":\"FIELDNAME.keyword\",\"type\":\"string\",\"esTypes\":[\"keyword\"],\"count\":0,\"scripted\":false,\"searchable\":true,\"aggregatable\":true,\"readFromDocValues\":true,\"subType\":{\"multi\":{\"parent\":\"FIELDNAME\"}}}
- repack the edited file via "jq -c '.' rs1.json  > rs1.ndjson"

In the Kibana interface, import the updated "rs1.ndjson" file
via the "Stack Management -> Save Objects" interface.

conf files
==========

* Custom configrations for rsyslog working with the ELK server installed via elk-docker.postinstall.sh and using an index pattern named "rs*"

elk.rb
======

* Custom rules for the rsyslog "mmnormalize" module referenced in 61-out-rules.conf
