################################################################
# (c) Copyright 2020 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/etc/rsyslog.d/20-input.conf

# Enable UDP syslog reception
module(load="imudp")
input(type="imudp" port="6000")

# Enable TCP syslog reception
module(load="imtcp" MaxListeners="256")
input(type="imtcp" port="5000")

$PreserveFQDN on
# The main queue settings
main_queue (
	queue.type="LinkedList"      
	queue.highwatermark="40000"  
	queue.spoolDirectory="/var/spool/rsyslog"
	queue.filename="mainQueue"
	queue.lowwatermark="5000" 
	queue.maxdiskspace="100m" 
	queue.size="50000"    
	queue.dequeuebatchsize="1000" 
	queue.workerthreads="2"
	queue.saveonshutdown="on"
)

# Enable on-demand debugging
# https://techpunch.co.uk/development/how-to-ship-logs-with-rsyslog-and-logstash
$DebugFile /var/log/rsyslog-debug.log
$DebugLevel 1
