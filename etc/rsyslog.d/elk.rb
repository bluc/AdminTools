version=2
################################################################
# (c) Copyright 2020 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://gitlab.com/bluc/AdminTools/raw/master/etc/rsyslog.d/elk.rb
# See also https://www.liblognorm.com/files/manual/configuration.html

#---------------------------------------------------------------
# rsyslog stats
rule=rsyslog,queuestats:%
 object:char-to{"extradata":":"}
 %: origin=%
 origin:char-to{"extradata":" "}
 % size=%
 size:number
 % enqueued=%
 enqueued:number
 % full=%
 full:number
 % discarded.full=%
 discarded_full:number
 % discarded.nf=%
 discarded_nf:number
 % maxqsize=%
 maxqsize:number
 %%-:rest%

rule=rsyslog,actionstats:action-%
 -:number
 %-%
 object:char-to{"extradata":" "}
 % origin=%
 origin:char-to{"extradata":" "}
 % processed=%
 processed:number
 % failed=%
 failed:number
 % suspended=%
 suspended:number
 % suspended.duration=%
 suspended_duration:number
 % resumed=%
 resumed:number
 %%-:rest%

rule=rsyslog,action:action '%
 object:char-to{"extradata":"'"}
 %' %
 action:word
 % %rest:rest%

rule=rsyslog,globals:%
 object:char-to{"extradata":" "}
 % origin=%
 origin:word
 %%-:rest%

rule=rsyslog,modulestats:%
 object:char-to{"extradata":" "}
 % origin=%
 origin:word
 % %
 p1:char-to{"extradata":"="}
 %=%
 submitted:number
 %%rest:rest%

rule=rsyslog,resourcestats:%
 object:char-to{"extradata":" "}
 % origin=%
 origin:word
 % utime=%
 utime:number
 % stime=%
 stime:number
 % maxrss=%
 maxrss:number
 % minflt=%
 minflt:number
 % majflt=%
 majflt:number
 % inblock=%
 inblock:number
 % oublock=%
 outblock:number
 % nvcsw=%
 nvcsw:number
 % nivcsw=%
 nivcsw:number
 % openfiles=%
 openfiles:number
 %%-:rest%

#---------------------------------------------------------------
# bind9/named logs
rule=bind,named:client %
 ip:char-to{"extradata":"#"}
 %%-:rest%

#---------------------------------------------------------------
# IPTABLES logs - outbound rejections
rule=iptables,outbound,reject:%
 -:kernel-timestamp
 % OUT-e%
 -:char-sep:=
 %= OUT=%
 int_out:word
 % SRC=%
 src_ip:ipv4
 % DST=%
 ip:ipv4
 % LEN=%
 len:number
 % TOS=%
 tos:word
 % PREC=%
 prec:word
 % TTL=%
 ttl:number
 % ID=%
 ipt_id:number
 % %
 -:char-sep:=
 %=%
 proto:word
 % SPT=%
 src_port:number
 % DPT=%
 dst_port:number
 %%-:rest%

rule=iptables,outbound,reject:%
 -:kernel-timestamp
 % OUT-e%
 -:char-sep:=
 %= OUT=%
 int_out:word
 % SRC=%
 src_ip:ipv4
 % DST=%
 ip:ipv4
 % LEN=%
 len:number
 % TOS=%
 tos:word
 % PREC=%
 prec:word
 % TTL=%
 ttl:number
 % ID=%
 ipt_id:number
 % %
 -:char-sep:=
 %=%
 proto:word
 % TYPE=%
 -:rest%

rule=iptables,reject:%
 -:kernel-timestamp
 % not from US IN= OUT=%
 int_out:word
 % SRC=%
 src_ip:ipv4
 % DST=%
 ip:ipv4
 % LEN=%
 len:number
 % TOS=%
 tos:word
 % PREC=%
 prec:word
 % TTL=%
 ttl:number
 % ID=%
 ipt_id:number
 % %
 -:char-sep:=
 %=%
 proto:word
 % SPT=%
 src_port:number
 % DPT=%
 dst_port:number
 %%-:rest%

rule=iptables,inbound,reject:%
 -:kernel-timestamp
 % %
 -:word
 % BLOCKED%
 {"type":"alternative","parser":[
	{"type":"literal", "text":": "},
	{"type":"literal", "text":":"}
 ]}
 %IN=%
 int_in:word
 % OUT= MAC=%
 mac:word
 % SRC=%
 ip:ipv4
 % DST=%
 dst_ip:ipv4
 % LEN=%
 len:number
 % TOS=%
 tos:word
 % PREC=%
 prec:word
 % TTL=%
 ttl:number
 % ID=%
 ipt_id:number
 % %
 -:char-to{"extradata":"="}
 %=%
 proto:word
 % SPT=%
 src_port:number
 % DPT=%
 dst_port:number
 %%-:rest%

rule=iptables,reject:%
 -:kernel-timestamp
 % SSDP rejection IN= OUT=%
 int_out:word
 % SRC=%
 src_ip:ipv4
 % DST=%
 ip:ipv4
 % LEN=%
 len:number
 % TOS=%
 tos:word
 % PREC=%
 prec:word
 % TTL=%
 ttl:number
 % ID=%
 ipt_id:number
 % %
 -:char-to{"extradata":"="}
 %=%
 proto:word
 % SPT=%
 src_port:number
 % DPT=%
 dst_port:number
 %%-:rest%

#---------------------------------------------------------------
# IPTABLES logs - inbound rejections
rule=iptables,inbound,reject:%
 -:kernel-timestamp
 % IN-%
 -:char-to{"extradata":"="}
 %=%
 int_in:word
 % OUT= MAC=%
 mac:word
 % SRC=%
 ip:ipv4
 % DST=%
 dst_ip:ipv4
 % LEN=%
 len:number
 % TOS=%
 tos:word
 % PREC=%
 prec:word
 % TTL=%
 ttl:number
 % ID=%
 ipt_id:number
 % %
 -:char-to{"extradata":"="}
 %=%
 proto:word
 % SPT=%
 src_port:number
 % DPT=%
 dst_port:number
 %%-:rest%

rule=iptables,inbound,reject:%
 -:kernel-timestamp
 % %
 jail:char-to:\x20
 % IN=%
 int_in:word
 % OUT= MAC=%
 mac:word
 % SRC=%
 ip:ipv4
 % DST=%
 dst_ip:ipv4
 % LEN=%
 len:number
 % TOS=%
 tos:word
 % PREC=%
 prec:word
 % TTL=%
 ttl:number
 % ID=%
 ipt_id:number
 % %
 -:char-to{"extradata":"="}
 %=%
 proto:word
 % SPT=%
 src_port:number
 % DPT=%
 dst_port:number
 %%-:rest%

rule=iptables,inbound,reject:%
 -:kernel-timestamp
 % not from US IN=%
 int_in:word
 % OUT= MAC=%
 mac:word
 % SRC=%
 ip:ipv4
 % DST=%
 dst_ip:ipv4
 % LEN=%
 len:number
 % TOS=%
 tos:word
 % PREC=%
 prec:word
 % TTL=%
 ttl:number
 % ID=%
 ipt_id:number
 % %
 -:char-to{"extradata":"="}
 %=%
 proto:word
 % SPT=%
 src_port:number
 % DPT=%
 dst_port:number
 %%-:rest%

rule=iptables,inbound,reject:%
 -:kernel-timestamp
 % SSDP rejection IN=%
 int_in:word
 % OUT= MAC=%
 mac:word
 % SRC=%
 ip:ipv4
 % DST=%
 dst_ip:ipv4
 % LEN=%
 len:number
 % TOS=%
 tos:word
 % PREC=%
 prec:word
 % TTL=%
 ttl:number
 % ID=%
 ipt_id:number
 % %
 -:char-to{"extradata":"="}
 %=%
 proto:word
 % SPT=%
 src_port:number
 % DPT=%
 dst_port:number
 %%-:rest%

#---------------------------------------------------------------
# IPTABLES logs - outbound rejections
rule=iptables,outbound,reject:%
 -:kernel-timestamp
 % OUT-%
 -:char-to{"extradata":"="}
 %= %
 % OUT=%
 out_int:word
 % SRC=%
 ip:ipv4
 % DST=%
 dst_ip:ipv4
 % LEN=%
 len:number
 % TOS=%
 tos:word
 % PREC=%
 prec:word
 % TTL=%
 ttl:number
 % ID=%
 ipt_id:number
 % %
 -:char-to{"extradata":"="}
 %=%
 proto:word
 % SPT=%
 src_port:number
 % DPT=%
 dst_port:number
 %%-:rest%

#---------------------------------------------------------------
# Fail2Ban logs
rule=fail2ban:%
 -:char-to{"extradata":" "}
 % %
 -:char-to{"extradata":" "}
 % %
 -:char-to{"extradata":" "}
 % %
 host:char-to{"extradata":" "}
 % %
 -:char-to{"extradata":"]"}
 %]:%
 -:char-to{"extradata":"["}
 %%
 jail:string{"quoting.char.begin":"[", "quoting.char.end":"]"}
 % %
 action:word
 % %
 ip:ipv4
 %%-:rest%

rule=fail2ban:%
 -:char-to{"extradata":" "}
 % %
 -:char-to{"extradata":" "}
 % %
 -:char-to{"extradata":" "}
 % %
 host:char-to{"extradata":" "}
 % %
 -:char-to{"extradata":"]"}
 %]:%
 -:char-to{"extradata":"["}
 %%
 jail:string{"quoting.char.begin":"[", "quoting.char.end":"]"}
 % %
 ip:ipv4
 % %
 action:rest
 %

rule=fail2ban:%
 -:char-to{"extradata":"]"}
 %]: ERROR %
 rest:rest%

#---------------------------------------------------------------
# sshd logs
rule=sshd:Accepted %
 method:word
 % for %
 user:word
 % from %
 ip:ipv4
 % port %
 port:number
 %%-:rest%

rule=sshd:Disconnected from %
 ip:ipv4
 % port %
 port:number
 %

rule=sshd:Disconnected from user %
 user:word
 % %
 ip:ipv4
 % port %
 port:number
 %

rule=sshd,fail:Failed password for %
 user:word
 % from %
 ip:ipv4
 % port %
 port:number
 %%
 -:rest%

#---------------------------------------------------------------
# sudo, su, and systemd-login logs
rule=sudo,session:pam_unix(sudo:session): session %
 action:word
 % for user %
 user:word
 % %-:rest%

rule=sudo,session:pam_unix(sudo:session): session %
 action:word
 % for user %
 user:word
 %

rule=sudo,command: %
 user:word
 % : TTY=%
 tty:char-to{"extradata":" "}
 % ; PWD=%
 pwd:char-to{"extradata":" "}
 % ; USER=%
 -:char-to{"extradata":" "}
 % ; COMMAND=%
 command:rest%

rule=sudo:%
 user:word
 % : TTY=%
 tty:char-to{"extradata":" "}
 % ; PWD=%
 pwd:char-to{"extradata":" "}
 % ; USER=%
 -:char-to{"extradata":" "}
 % ; COMMAND=%
 command:rest
 %

rule=su,session:pam_unix(su:session): session %
 action:word
 % for user %
 user:word
 %

rule=su,session:pam_unix(su:session): session %
 action:word
 % for user %
 user:word
 % %-:rest%

rule=su,session:%
 action:word
 % su for %
 destuser:char-to{"extradata":" "}
 % by %
 user:rest
 %

rule=logind,session:New session %
 sessionid:number
 % of user %
 user:char-to{"extradata":"."}
 %%-:rest%

rule=logind,session:Removed session %
 sessionid:number
 %%-:rest%

#---------------------------------------------------------------
# postgrey logs
rule=postgrey:%
 -:char-to{"extradata":"="}
 %=%
 pp_score:char-to{"extradata":","}
 %, reason=%
 reason:char-to{"extradata":","}
 %, client_name=%
 client_name:char-to{"extradata":","}
 %, client_address=%
 ip:char-to{"extradata":"/"}
 %%
 -:char-to{"extradata":","}
 %, sender=%
 from:char-to{"extradata":","}
 %, recipient=%
 to:rest
 %

#---------------------------------------------------------------
# ppolicyd logs
rule=ppolicyd:X-PPolicyd: %
 check:char-to{"extradata":"M"}
 %MAIL FROM check %
 -:word
 % for %
 from:char-to{"extradata":";"}
 %; result code = %
 pp_score:char-to{"extradata":":"}
 %: %
 host:char-to{"extradata":":"}
 %: domain of %
 -:char-to{"extradata":" "}
 % designates %
 ip:ipv4
 % %-:rest%

rule=ppolicyd:%
 -:char-to{"extradata":" "}
 % X-PPolicyd: %
 -:char-to{"extradata":"M"}
 %MAIL FROM check %
 -:word
 % for %
 from:char-to{"extradata":";"}
 %; result code = %
 pp_score:char-to{"extradata":":"}
 %: %
 host:char-to{"extradata":":"}
 %: domain of %
 n3:char-to{"extradata":" "}
 % designates %
 ip:ipv4
 %%-:rest%

rule=ppolicyd:%
 -:char-to{"extradata":" "}
 % X-PPolicyd: %
 -:char-to{"extradata":"M"}
 %MAIL FROM check %
 -:word
 % for %
 from:char-to{"extradata":";"}
 %; result code = %
 pp_score:char-to{"extradata":":"}
 %: %
 host:char-to{"extradata":":"}
 %: domain of %
 n3:char-to{"extradata":" "}
 % does not designate%-:rest%

rule=ppolicyd:%
 -:char-to{"extradata":" "}
 % X-PPolicyd: %
 -:char-to{"extradata":"M"}
 %MAIL FROM check %
 -:word
 % for %
 from:char-to{"extradata":";"}
 %; result code = %
 pp_score:char-to{"extradata":":"}
 %: %
 host:char-to{"extradata":":"}
 %: %
 -:char-to{"extradata":"d"}
 %domain of %
 n3:char-to{"extradata":" "}
 % does not designate%-:rest%

rule=ppolicyd,reject:%
 -:char-to{"extradata":"C"}
 %Client host %
 ip:string{"quoting.char.begin":"[", "quoting.char.end":"]"}
 % blocked%-:rest%

#---------------------------------------------------------------
# Reverse Proxy logs
rule=nginx,access-denied:%
 -:number
 %#%
 -:number
 %: %
 -:char-to{"extradata":" "}
 % access forbidden by rule, client: %
 ip:char-to:,
 %, server: %
 host:char-to:,
 %, request: "%
 verb:word
 % %
 request:char-to{"extradata":" "}
 % HTTP/%
 httpversion:float
 %%-:rest%

rule=proxy:%
 ip:ipv4
 % %
 ident:word
 % %
 auth:word
 % %
 ts:string{"quoting.char.begin":"[", "quoting.char.end":"]"}
 % %
 verb:word
 % %
 request:char-to{"extradata":" "}
 % HTTP/%
 httpversion:float
 %" %
 response:number
 % %
 bytes:number
 % %
 host:string{"quoting.char.begin":'"', "quoting.char.end":'"'}
 % %
 referrer:string{"quoting.char.begin":'"', "quoting.char.end":'"'}
 % %
 agent:string{"quoting.char.begin":'"', "quoting.char.end":'"'}
 % country=%
 country:word
 % rt=%
 request_time:float
 % uct=%
 upstream_connect_time:float
 % uht=%
 upstream_header_time:float
 % urt=%
 upstream_response_time:float
 % reqlen=%
 request_len:number
 % ucs=%
 cache_hit:rest
 %

rule=nginx:%
 ip:ipv4
 % %
 ident:word
 % %
 auth:word
 % %
 ts:string{"quoting.char.begin":"[", "quoting.char.end":"]"}
 % %
 verb:word
 % %
 request:char-to{"extradata":" "}
 % HTTP/%
 httpversion:float
 %" %
 response:number
 % %
 bytes:number
 % %
 host:string{"quoting.char.begin":'"', "quoting.char.end":'"'}
 % %
 agent:string{"quoting.char.begin":'"', "quoting.char.end":'"'}
 % %
 forwarded_for:string{"quoting.char.begin":'"', "quoting.char.end":'"'}
 %

#rule=nginx:message repeated %
# -:number
# % times: [%
# ip:word
# % %
# ident:word
# % %
# auth:word
# % %
# -:char-to:"
# %"%
# verb:word
# % %
# request:word
# % HTTP/%
# httpversion:float
# %" %
# response:number
# % %
# bytes:number
# % "%
# referrer:char-to:"
# %" "%
# -:char-to:"
# %" "%
# agent:char-to:"
# %" %-:rest%

rule=nginx,modsecurity:%
 -:number
 %#%
 -:number
 %: ModSecurity%
 modsec:rest
 %

rule=nginx,status:Load:%
 load:float
 %,ActiveConnections:%
 ActiveConnections:float
 %,Accepts:%
 Accepts:float
 %,Handled:%
 Handled:float
 %,Requests:%
 Requests:float
 %,RequestsPerConnection:%
 RequestsPerConnection:float
 %,MaxConnections:%
 MaxConnections:float
 %

#---------------------------------------------------------------
# SysMon logs
rule=sysmon:info %
 -:char-to{"extradata":"-"}
 %- LS_%
 what:char-to{"extradata":"="}
 %=%
 sysmon:float
 %=%
 par2:char-to{"extradata":"="}
 %=%
 par3:rest
 %

rule=sysmon:%
 -:char-to{"extradata":"-"}
 %- LS_%
 what:char-to{"extradata":"="}
 %=%
 sysmon:float
 %=%
 par2:char-to{"extradata":"="}
 %=%
 par3:rest
 %

#---------------------------------------------------------------
# stunnel logs
rule=stunnel,close:LOG5[%
 -:number
 %]: Connection closed: %
 bytes1:number
 % byte(s) sent to %
 dest1:char-to{"extradata":","}
 %, %
 bytes2:number
 % byte(s) sent to %
 dest2:rest
 %

rule=stunnel,accept:LOG5[%
 -:number
 %]: Service [%
 service:char-to{"extradata":"]"}
 %] accepted connection from %
 ip:ipv4
 %:%
 port:number
 %

rule=stunnel,connect:LOG5[%
 -:number
 %]: Service [%
 service:char-to{"extradata":"]"}
 %] connected remote server from %
 ip:ipv4
 %:%
 port:number
 %

rule=stunnel,connect:LOG5[%
 -:number
 %]: s_connect: connected %
 ip:ipv4
 %:%
 port:number
 %

#---------------------------------------------------------------
# mail logs
rule=synclea:SyncLEA%
 -:rest%

# nullmailer
rule=nullmailer,fail:smtp: Failed: %
 reason:rest%

rule=nullmailer:From: <%
 from:char-to{"extradata":">"}
 %> to: <%
 to:char-to{"extradata":">"}
 %%-:rest%

rule=nullmailer:Message-Id: <%
 msgid:char-to{"extradata":">"}
 %%-:rest%

rule=nullmailer:Starting delivery: protocol: %
 protocol:char-to{"extradata":" "}
 % host: %
 ip:ipv4
 %%-:rest%

rule=nullmailer:smtp: Succeeded: %
 -:number
 % %
 -:char-to{"extradata":":"}
 %: queued as %
 postfix_id:rest%

# postfix postscreen
rule=postscreen:%
 {"type":"alternative","parser":[
	{"type":"literal", "text":"CONNECT from"},
	{"type":"literal", "text":"WHITELISTED"}
 ]}
 % [%
 ip:ipv4
 %%-:rest%

# postfix smtpd
rule=smtpd,warning:warning: %
 rest:rest%

rule=smtpd,warning:%
 postfix_id:char-to{"extradata":":"}
 %: warning: %
 rest:rest%

rule=smtpd:connect from %
 -:char-to:[
 %[%
 ip:ipv4
 %%-:rest%

rule=smtpd:disconnect from %
 -:char-to:[
 %[%
 ip:ipv4
 %%-:rest%

rule=smtpd:%
 -:word
 % %
 -:word
 % connection established from %
 client:char-to:[
 %[%
 ip:ipv4
 %%-:rest%

rule=smtpd,SSL:setting up TLS connection from %
 -:char-to{"extradata":"["}
 %[%
 ip:ipv4
 %%-:rest%

rule=smtpd,SSL:%
 -:char-to{"extradata":"["}
 %[%
 ip:ipv4
 %]: TLS cipher list %
 ciphers:rest
 %

rule=smtpd,fail:%
 -:char-to:{"extradata":":"}
 %: reject: RCPT from %
 client:char-to:[
 %[%
 ip:ipv4
 %]: %
 smtp_code:number
 % %
 -:char-to:;
 %; from=<%
 from:char-to:>
 %> to=<%
 to:char-to:>
 %>%
 -:rest
 %

rule=smtpd,fail:%
 -:char-to:{"extradata":":"}
 %: reject: RCPT from %
 client:char-to:[
 %[%
 ip:ipv4
 %]: %
 smtp_code:number
 % %
 -:char-to:;
 %; from=<> to=<%
 to:char-to:>
 %>%
 -:rest
 %

rule=smtpd,fail:%
 -:char-to:{"extradata":":"}
 %: reject: RCPT from %
 client:char-to:[
 %[%
 ip:ipv4
 %]: %
 smtp_code:number
 % %
 -:char-to:;
 %; %
 -:char-to:;
 %; %
 -:char-to:;
 %; from=<%
 from:char-to:>
 %> to=<%
 to:char-to:>
 %>%
 -:rest
 %

rule=smtpd,fail:%
 -:char-to:{"extradata":":"}
 %: reject: RCPT from %
 client:char-to:[
 %[%
 ip:ipv4
 %]: %
 smtp_code:number
 % %
 -:char-to:;
 %; %
 -:char-to:;
 %; %
 -:char-to:;
 %; from=<> to=<%
 to:char-to:>
 %>%
 -:rest
 %

rule=smtpd,fail:%
 -:char-to:{"extradata":":"}
 %: reject: RCPT from %
 client:char-to:[
 %[%
 ip:ipv4
 %]: %
 smtp_code:number
 % %
 -:char-to:<
 % <%
 to:char-to:>
 %>: Recipient %
 -:rest
 %

rule=smtpd,fail:%
 -:char-to:{"extradata":":"}
 %: reject: RCPT from %
 client:char-to:[
 %[%
 ip:ipv4
 %]: %
 smtp_code:number
 % %
 -:rest
 %

rule=smtpd,filter:%
 postfix_id:char-to{"extradata":":"}
 %: filter: %
 -:char-to{"extradata":"["}
 %[%
 ip:ipv4
 %]%
 -:char-to{"extradata":"<"}
 -:char-to{"extradata":"<"}
 %<%
 -:char-to{"extradata":">"}
 %>: %
 -:char-to{"extradata":";"}
 %; from=<%
 from:char-to{"extradata":">"}
 %> to=<%
 to:char-to:>
 %>%-:rest%

rule=smtpd:%
postfix_id:char-to::
 %: client=%
 -:char-to:[
 %[%
 ip:char-to:]
 %]

rule=smtpd,login:%
postfix_id:char-to::
 %: client=%
 -:char-to:[
 %[%
 ip:char-to:]
 %], sasl_method=%
 how:char-to{"extradata":","}
 %, sasl_username=%
 from:rest
 %

# postfix qmgr
rule=qmgr:%
 postfix_id:char-to::
 %: message-id=<%
 msgid:char-to:>
 %%-:rest%

rule=qmgr:%
 postfix_id:char-to::
 %: from=<%
 from:char-to:>
 %%-:rest%

rule=qmgr:%
 postfix_id:char-to::
 %: from=<>%
 -:rest%

rule=qmgr:%
 postfix_id:char-to::
 %: removed

rule=qmgr:%
 postfix_id:char-to::
 %: skipped%
 -:rest
 %

# postfix smtp
rule=smtp:setting up %
 -:char-to:[
 %[%
 ip:char-to:]
 %%-:rest%

rule=smtp:%
 postfix_id:char-to{"extradata":":"}
 %: to=<%
 from:char-to{"extradata":">"}
 %>, relay=%
 client:char-to{"extradata":"["}
 %[%
 ip:ipv4
 %]:%
 dst_port:number
 %%-:char-to{"extradata":"d"}
 %delay=%
 delay:float
 %, delays=%
 delays:char-to{"extradata":","}
 %, dsn=%
 dsn:char-to{"extradata":","}
 %, status=%
 action:word
 %%-:rest%

rule=smtp:%
 postfix_id:char-to{"extradata":":"}
 %: to=<%
 from:char-to{"extradata":">"}
 %>, relay=%
 client:char-to{"extradata":","}
 %, delay=%
 delay:float
 %, delays=%
 delays:char-to{"extradata":","}
 %, dsn=%
 dsn:char-to{"extradata":","}
 %, status=%
 action:word
 %%-:rest%

rule=smtp:%
 postfix_id:char-to::
 %: to=<%
 to:char-to:>
 %>, orig_to=<%
 orig_to:char-to:>
 %>, relay=%
 client:char-to:[
 %[%
 ip:ipv4
 %]:%
 dst_port:number
 %, delay=%
 delay:float
 %, delays=%
 delays:char-to:,
 %, dsn=%
 dsn:char-to:,
 %, status=%
 action:word
 %%-:rest%

rule=smtp:%
 -:word
 % %
 -:word
 % connection established to %
 client:char-to:[
 %[%
 ip:ipv4
 %]:%
 smtp_port:number
 %:%-:rest%

# postfix cleanup
rule=cleanup:%
 postfix_id:char-to::
 %: message-id=<%
 msgid:char-to:>
 %>

# postfix cleanup
rule=bounce:%
 postfix_id:char-to::
 %: sender %
 rest:rest%

#---------------------------------------------------------------
# dovecot logs
rule=dovecot,auth:%
 service:char-to{"extradata":"-"}
 %-login: Login: user=<%
 user:char-to{"extradata":">"}
 %>, method=%
 method:char-to{"extradata":","}
 %, rip=%
 ip:ipv4
 %, %
 -:char-to{"extradata":"<"}
 %<%
 session-id:char-to{"extradata":">"}
 %%-:rest%

rule=dovecot,close:%
 service:char-to{"extradata":"-"}
 %-login: Disconnected (%
 reason:char-to{"extradata":")"}
 %): user=<>, rip=%
 ip:ipv4
 %, %
 -:char-to{"extradata":"<"}
 %<%
 session-id:char-to{"extradata":">"}
 %%-:rest%

rule=dovecot,close:%
 service:char-to{"extradata":"("}
 %(%
 user:char-to{"extradata":")"}
 %)<%
 -:char-to{"extradata":"<"}
 %<%
 session-id:char-to{"extradata":">"}
 %>: %
 {"type":"alternative","parser":[
	{"type":"literal", "text":"Connection closed"},
	{"type":"literal", "text":"Logged out"}
 ]}
 %%-:rest%

rule=dovecot,delivery:%
 service:char-to{"extradata":"("}
 %(%
 user:char-to{"extradata":")"}
 %)<%
 -:char-to{"extradata":"<"}
 %<%
 session-id:char-to{"extradata":">"}
 %>: save: box=%
 folder:char-to{"extradata":","}
 %%-:char-to{"extradata":"m"}
 %msgid=<%
 msgid:char-to{"extradata":">"}
 %%-:rest%

rule=dovecot,filter:%
 service:char-to{"extradata":"("}
 %(%
 user:char-to{"extradata":")"}
 %)<%
 -:char-to{"extradata":"<"}
 %<%
 session-id:char-to{"extradata":">"}
 %>: sieve: msgid=%
 msgid:char-to{"extradata":">"}
 %>: %
 action:char-to{"extradata":":"}
 %%-:rest%

#---------------------------------------------------------------
# openkdim logs
rule=opendkim:%
 postfix_id:char-to{"extradata":":"}
 %: no signing table match for '%
 from:char-to{"extradata":"'"}
 %%-:rest%

#---------------------------------------------------------------
# amavis logs
rule=amavis:(%
 amavis_id:char-to{"extradata":")"}
 %) spam-tag, <%
 from:char-to{"extradata":">"}
 %> -> <%
 to:char-to{"extradata":">"}
 %>, %
 am_score:char-to{"extradata":","}
 %,%-:rest%

rule=amavis:(%
 amavis_id:char-to{"extradata":")"}
 %) %
 -:word
 % FWD from <%
 from:char-to{"extradata":">"}
 %> -> <%
 to:char-to{"extradata":">"}
 %%-:rest%

rule=amavis:(%
 amavis_id:char-to{"extradata":")"}
 %) dkim: %
 dkim_result:word
 %%
 -:char-to{"extradata":"<"}
 %<%
 from:char-to{"extradata":">"}
 %%-:rest%

rule=amavis:(%
 amavis_id:char-to{"extradata":")"}
 %) Checking: %
 -:char-to{"extradata":"["}
 %[%
 ip:ipv4
 %] <%
 from:char-to{"extradata":">"}
 %> -> <%
 to:char-to{"extradata":">"}
 %%-:rest%

rule=amavis:(%
 amavis_id:char-to{"extradata":")"}
 %) %
 passblock:word
 % %
 pbhow:word
 % {%
 tagged:char-to{"extradata":"}"}
 %},%
 -:char-to{"extradata":"["}
 %[%
 ip:char-to{"extradata":"]"}
 %]%
 -:char-to{"extradata":"<"}
 %<%
 from:char-to{"extradata":">"}
 %> -> <%
 to:char-to{"extradata":">"}
 %%
 -:char-to{"extradata":"Q"}
 %Queue-ID: %
 postfix_id:char-to{"extradata":","}
 %, Message-ID: <%
 msgid:char-to{"extradata":">"}
 %%-:rest%

rule=amavis:(%
 amavis_id:char-to{"extradata":")"}
 %) ESMTP :%
 -:number
 % %
 -:char-to{"extradata":"<"}
 %<%
 from:char-to{"extradata":">"}
 %> -> <%
 to:char-to{"extradata":">"}
 %%-:rest%

rule=amavis:(%
 amavis_id:char-to{"extradata":")"}
 %) %
 passblock:word
 % %
 pbhow:word
 % {%
 tagged:char-to{"extradata":"}"}
 %}%
 -:char-to{"extradata":"["}
 %[%
 ip:char-to{"extradata":"]"}
 %]%
 -:char-to{"extradata":"<"}
 %<>%
 -:char-to{"extradata":">"}
 %<%
 to:char-to{"extradata":">"}
 %%
 -:char-to{"extradata":"Q"}
 % Queue-ID: %
 postfix_id:char-to{"extradata":","}
 %, Message-ID: <%
 msgid:char-to{"extradata":">"}
 %%-:rest%

rule=amavis:(%
 amavis_id:char-to{"extradata":")"}
 %) %
 passblock:word
 % %
 pbhow:word
 % {%
 tagged:char-to{"extradata":"}"}
 %%
 -:char-to{"extradata":"["}
 %%
 ip:char-to{"extradata":"]"}
 %%
 -:char-to{"extradata":"<"}
 %%
 from:char-to{"extradata":">"}
 %%
 -:char-to:{"extradata":"<"}
 %%
 to:char-to{"extradata":">"}
 %%
 -:char-to{"extradata":" "}
 % quarantine: %
 -:char-to{"extradata":","}"}
 %, Queue-ID: %
 postfix_id:char-to{"extradata":","}
 %, Message-ID: <%
 msgid:char-to{"extradata":">"}
 %%-:rest%

rule=amavis:(%
 amavis_id:char-to{"extradata":")"}
 %) %
 smtp_flavor:word
 % :%
 smtp_port:number
 % %
 file:char-to::
 %: <%
 from:char-to:>
 %>%
 -:char-to:<
 %<%
 to:char-to:>
 %>%
 -:char-to::
 %: from %
 -:char-to:[
 %[%
 ip:char-to:]
 %%-:rest%

#---------------------------------------------------------------
# Zimbra logs
rule=zimbra,auth:auth_zimbra: %
 user:char-to{"extradata":" "}
 %%-:rest%

rule=zimbra,delivery:MAILBOX [%
 component:char-to{"extradata":"]"}
 %] [ip=%
 ip:char-to{"extradata":";"}
 %;] lmtp - Delivering message: size=%
 -:number
 % bytes, nrcpts=%
 -:number
 %, sender=%
 from:char-to{"extradata":","}
 %, msgid=<%
 msgid:char-to{"extradata":">"}
 %%-:rest%

rule=zimbra,auth:MAILBOX [%
 component:char-to{"extradata":"]"}
 %] [name=%
 user:char-to{"extradata":";"}
 %;ip=%
 -:char-to{"extradata":";"}
 %;oip=%
 ip:char-to{"extradata":";"}
 %;%
 -:char-to{"extradata":"]"}
 %] %
 proto:word
 % - user %
 -:word
 % authenticated%-:rest%

rule=zimbra,auth:MAILBOX [%
 component:char-to{"extradata":"]"}
 %] [name=%
 user:char-to{"extradata":";"}
 %;ip=%
 -:char-to{"extradata":";"}
 %;%
 -:char-to{"extradata":"]"}
 %] %
 proto:word
 % - user %
 -:word
 % authenticated%-:rest%

rule=zimbra,mailop,add:MAILBOX [%
 component:char-to{"extradata":"]"}
 %] [name=%
 user:char-to{"extradata":";"}
 %;%
 -:char-to{"extradata":";"}
 %;%
 -:char-to{"extradata":"p"}
 %p=%
 ip:char-to{"extradata":";"}
 %%
 -:char-to{"extradata":"]"}
 %] mailop - Adding Message: id=%
 -:number
 %, Message-ID=<%
 msgid:char-to{"extradata":">"}
 %>, parentId=%
 -:char-to{"extradata":","}
 %, folderId=%
 -:char-to{"extradata":","}
 %, folderName=%
 foldername:rest
 %

rule=zimbra,mailop:MAILBOX [%
 component:char-to{"extradata":"]"}
 %] [name=%
 user:char-to{"extradata":";"}
 %;%
 -:char-to{"extradata":";"}
 %;%
 -:char-to{"extradata":"p"}
 %p=%
 ip:char-to{"extradata":";"}
 %%
 -:char-to{"extradata":"]"}
 %] mailop - %
 action:rest
 %

rule=zimbra,filter:MAILBOX [%
 component:char-to{"extradata":"]"}
 %] [name=%
 user:char-to{"extradata":";"}
 %;%
 -:char-to{"extradata":";"}
 %;ip=%
 ip:char-to{"extradata":";"}
 %;] filter - %
 action:rest
 %

rule=zimbra,filter,discard:MAILBOX [%
 component:char-to{"extradata":"]"}
 %] [name=%
 user:char-to{"extradata":";"}
 %;%
 -:char-to{"extradata":";"}
 %;ip=%
 ip:char-to{"extradata":";"}
 %;] filter - Discarding message with Message-ID <%
 msgid:char-to{"extradata":">"}
 %> from %
 -:char-to{"extradata":"<"}
 %<%
 from:char-to{"extradata":">"}
 %%-:rest%

rule=zimbra,tempfail:MAILBOX [%
 component:char-to{"extradata":"]"}
 %] [ip=%
 ip:char-to{"extradata":";"}
 %;] lmtp - try again for address %
 to:char-to{"extradata":":"}
 %: %reason:rest%

rule=zimbra,fail:MAILBOX [%
 component:char-to{"extradata":"]"}
 %] [ip=%
 ip:char-to{"extradata":";"}
 %;] lmtp - S:%
 -:char-to{"extradata":"<"}
 %<%
 to:char-to{"extradata":">"}
 %%-:rest%

rule=zimbra,fail:MAILBOX [%
 component:char-to{"extradata":"]"}
 %] [ip=%
 ip:char-to{"extradata":";"}
 %;] lmtp - S: %
 -:number
 %%-:rest%

rule=zimbra,delete:MAILBOX [%
 component:char-to{"extradata":"]"}
 %] [name=%
 from:char-to{"extradata":";"}
 %;%
 -:char-to{"extradata":"]"}
 %] mailop - Deleting %
 rest:rest%

# Zimbra authentication failures
rule=zimbra,auth,fail:%
 -:char-to{"extradata":":"}
 %: %
 -:char-to:{"extradata":"["}
 %[%
 ip:ipv4
 %]: %
 -:word
 % LOGIN authentication failed: %
 -:rest%

rule=zimbra,auth,fail:%
 -:word
 % [%
 component:char-to{"extradata":"]"}
 %] [ip=%
 ip:char-to{"extradata":";"}
 %%
 -:char-to{"extradata":"-"}
 %- authentication failed for %
 user:char-to{"extradata":"("}
 %(%
 reason:char-to{"extradata":")"}
 %%-:rest%

rule=zimbra,auth,fail:AUDIT [%
 -:char-to{"extradata":"]"}
 %] [name=%
 user:char-to{"extradata":";"}
 %;oip=%
 ip:char-to{"extradata":";"}
 %;%
 -:char-to{"extradata":"-"}
 %- cmd=Auth%
 -:rest%

#---------------------------------------------------------------
# clamd logs
rule=clamd:%
 {"type":"alternative","parser":[
	{"type":"literal", "text":"Mon"},
	{"type":"literal", "text":"Tue"},
	{"type":"literal", "text":"Wed"},
	{"type":"literal", "text":"Thu"},
	{"type":"literal", "text":"Fri"},
	{"type":"literal", "text":"Sat"},
	{"type":"literal", "text":"Sun"}
 ]}
 % %
 -:word
 % %
 -:number
 % %
 -:time-24hr
 % %
 -:number
 % -> %
 -:char-to{"extradata":":"}
 %: %
 signature:char-to{"extradata":" "}
 % %-:rest%

rule=clamd:/%
 -:char-to{"extradata":":"}
 %: %
 signature:char-to{"extradata":" "}
 % %-:rest%

#---------------------------------------------------------------
# audit logs
rule=audit:node=%
 host:char-to{"extradata":" "}
 % type=%
 type:char-to{"extradata":" "}
 % msg=audit(%
 msg:char-to{"extradata":")"}
 %):%rest:rest%


#---------------------------------------------------------------
# MailqWarning.sh logs
rule=mailq:TOTAL=%
 q_total:number
 % MAILDROP=%
 q_maildrop:number
 % DEFERRED=%
 q_deferred:number
 % INCOMING=%
 q_incoming:number
 % ACTIVE=%
 q_active:number
 %

#---------------------------------------------------------------
# Sophos logs
rule=sophos:%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%src_ip:ipv4% message="%reason:char-to:"%%-:rest%

rule=sophos:%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%= src_ip=%src_ip:ipv4% src_country_code=%src_country:word
 % dst_ip=%dst_ip:ipv4% dst_country_code=%dst_country:word% protocol="%proto:char-to:"
 %" src_port=%src_port:number% dst_port=%dst_port:number%%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%="%reason:char-to:"%"%-:rest%

rule=sophos:%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%= src_ip=%src_ip:ipv4% src_country_code= dst_ip=%dst_ip:ipv4
 % dst_country_code=%dst_country:word% protocol="%proto:char-to:"%" src_port=%src_port:number
 % dst_port=%dst_port:number%%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%="%
 reason:char-to:"%"%-:rest%

rule=sophos:%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%= src_ip=%src_ip:ipv4% src_country_code=%src_country:word
 % dst_ip=%dst_ip:ipv4% dst_country_code= protocol="%proto:char-to:"%" src_port=%
 src_port:number% dst_port=%dst_port:number%%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%-:char-to{"extradata":"="}%=%
 -:char-to{"extradata":"="}%="%reason:char-to:"%"%-:rest%

#---------------------------------------------------------------
# Cloud logs
rule=cloud:id=%
 ltype:char-to:_
 %_%
 eventno:char-to:,
 %,action=%
 action:char-to:,
 %,status=%
 status:char-to:,
 %,created=%
 date:char-to:,
 %,duration=%
 duration:char-to:,
 %,elabel=%
 elabel:char-to:,
 %,etype=%
 etype:rest%

rule=cloud:id=%
 ltype:char-to:_
 %_%
 eventno:char-to:,
 %,user=%
 user:char-to:,
 %,ip=%
 ip:char-to:,
 %,date=%
 date:rest%
