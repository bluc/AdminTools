AdminTools
==========

* Several administrative tools for Linux (Ubuntu) and Zimbra/Carbonio Collaboration Suites
* Several installation scripts for specific applications or server types

Note: If you are using "apt-cacher-ng" in your environment, create the file
"/etc/apt/apt.conf.d/01Proxy" on each client:
~~~
Acquire::http::Proxy "http://<proxy-IP>:3142"
Acquire::http::Proxy { download.oracle.com DIRECT; }
~~~
